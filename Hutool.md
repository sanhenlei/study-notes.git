---
link: https://juejin.cn/post/6844903962265518093
title: 提示 Hutool 指南 API
description: 介绍一款超厉害的国产Java工具——Hutool。Hutool是一个Java工具包类库，对文件、流、加密解密、转码、正则、线程、XML等JDK方法进行封装，组成各种Util工具类。适用于很多项目以及Web开发，并且与其他框架没有耦合性。 1. Hutool之时间工具——Date…
keywords: Java
author: 首页 首页 沸点 小册 活动 搜索历史 清空 写文章 发布沸点 登录
date: 2019-10-12T08:59:06.000Z
publisher: 掘金
stats: paragraph=362 sentences=408, words=5943
---
介绍一款超厉害的国产 **Java工具——Hutool**。Hutool是一个Java工具包类库，对文件、流、加密解密、转码、正则、线程、XML等JDK方法进行封装，组成各种Util工具类。适用于很多项目以及Web开发，并且与其他框架没有耦合性。

=========================================================

## 1. Hutool之时间工具——DateUtil

* `DateUtil` 针对日期时间操作提供一系列静态方法
* `DateTime` 提供类似于Joda-Time中日期时间对象的封装，继承Date
* `FastDateFormat` 提供线程安全的针对Date对象的格式化和日期字符串解析支持。此对象在实际使用中并不需要感知，相关操作已封装在DateUtil和DateTime的相关方法中。
* `DateBetween` 计算两个时间间隔的类，除了通过构造新对象使用外，相关操作也已封装在DateUtil和DateTime的相关方法中。
* `TimeInterval` 一个简单的计时器类，常用于计算某段代码的执行时间，提供包括毫秒、秒、分、时、天、周等各种单位的花费时长计算，对象的静态构造已封装在DateUtil中。
* `DatePattern` 提供常用的日期格式化模式，包括String和FastDateFormat两种类型。

主要需要了解DateUtil类，DateTime类以及DatePattern类就可以应对大多数时间日期的操作。

DateUtil中都是静态方法, 下面是一些 **简单方法**;

```
now():String
today():String

date():DateTime

lastWeek():DateTime
lastMonth():DateTime
nextWeek():DateTime
nextMonth():DateTime
yesterday():DateTime
tomorrow():DateTime

currentSeconds():<span class="hljs-keyword">long</span>

thisYear():<span class="hljs-keyword">int</span>
thisMonth():<span class="hljs-keyword">int</span>
thisWeekOfMonth():<span class="hljs-keyword">int</span>
thisWeekOfYear():<span class="hljs-keyword">int</span>
thisDayOfMonth():<span class="hljs-keyword">int</span>
thisDayOfWeek():<span class="hljs-keyword">int</span>
thisHour(<span class="hljs-keyword">boolean</span> is24HourClock):<span class="hljs-keyword">int</span>
thisMinute():<span class="hljs-keyword">int</span>
thisSecond():<span class="hljs-keyword">int</span>
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

将一些固定格式的字符串-->Date对象：

```
yyyy-MM-dd hh:mm:ss
yyyy-MM-dd
hh:mm:ss
yyyy-MM-dd hh:mm

&#x5982;&#x679C;&#x4F60;&#x7684;&#x65E5;&#x671F;&#x683C;&#x5F0F;&#x4E0D;&#x662F;&#x8FD9;&#x51E0;&#x79CD;&#x683C;&#x5F0F;&#xFF0C;&#x5219;&#x9700;&#x8981;&#x6307;&#x5B9A;&#x65E5;&#x671F;&#x683C;&#x5F0F;&#xFF0C;&#x5BF9;&#x4E8E;&#x4EE5;&#x4E0A;&#x683C;&#x5F0F;&#x8FD8;&#x6709;&#x4E13;&#x95E8;&#x7684;&#x65B9;&#x6CD5;&#x5BF9;&#x5E94;&#xFF1A;
    parseDateTime parseDate ParseTime

DateUtil.parse()
DateUtil.parse(String,String)
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
&#x9700;&#x8981;&#x5C06;&#x65E5;&#x671F;&#x65F6;&#x95F4;&#x683C;&#x5F0F;&#x5316;&#x8F93;&#x51FA;&#xFF0C;Hutool&#x63D0;&#x4F9B;&#x4E86;&#x4E00;&#x4E9B;&#x65B9;&#x6CD5;&#x5B9E;&#x73B0;&#xFF1A;
    DateUtil.formatDateTime(Date date):String
    DateUtil.formatDate(Date date):String
    DateUtil.formatTime(Date date):String
    DateUtil.format(Date,String):String
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

Hutool可以很方便的获得某天/某月/某年的开始时刻和结束时刻：

```
beginOfDay(Date):Date &#x4E00;&#x5929;&#x5F00;&#x59CB;&#x65F6;&#x523B;
endOfDay(Date):Date &#x4E00;&#x5929;&#x7ED3;&#x675F;&#x65F6;&#x523B;

beginOfMonth(Date):<span class="hljs-function">Date
<span class="hljs-title">endOfMonth</span><span class="hljs-params">(Date)</span>:Date

<span class="hljs-title">beginOfYear</span><span class="hljs-params">(Date)</span>:Date
<span class="hljs-title">endOfYear</span><span class="hljs-params">(Date)</span>:Date

getBeginTimeOfDay &#x83B7;&#x5F97;&#x7ED9;&#x5B9A;&#x65E5;&#x671F;&#x5F53;&#x5929;&#x7684;&#x5F00;&#x59CB;&#x65F6;&#x95F4;&#xFF0C;&#x5F00;&#x59CB;&#x65F6;&#x95F4;&#x662F;00:00
getEndTimeOfDay &#x83B7;&#x5F97;&#x7ED9;&#x5B9A;&#x65E5;&#x671F;&#x5F53;&#x5929;&#x7684;&#x7ED3;&#x675F;&#x65F6;&#x95F4;&#xFF0C;&#x7ED3;&#x675F;&#x65F6;&#x95F4;&#x662F;23:59&#x3002;
</span><span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

需要指定日期做偏移，则使用offsiteDay、offsiteWeek、offsiteMonth来获得指定 **日期偏移**天、偏移周、偏移月，指定的偏移量正数向未来偏移，负数向历史偏移。
如果以上还不能满足偏移要求，则使用offsiteDate偏移制定量，其中参数calendarField为偏移的粒度大小（小时、天、月等）使用Calendar类中的常数。

```
<span class="hljs-number">1</span>.between&#x65B9;&#x6CD5;
    Hutool&#x53EF;&#x4EE5;&#x65B9;&#x4FBF;&#x7684;&#x8BA1;&#x7B97;&#x65F6;&#x95F4;&#x95F4;&#x9694;&#xFF0C;&#x4F7F;&#x7528; DateUtil.between(Date begin,Date end,DateUnit):<span class="hljs-keyword">long</span>
&#x8BA1;&#x7B97;&#x95F4;&#x9694;&#xFF0C;&#x65B9;&#x6CD5;&#x6709;&#x4E09;&#x4E2A;&#x53C2;&#x6570;&#xFF0C;&#x524D;&#x4E24;&#x4E2A;&#x5206;&#x522B;&#x662F;&#x5F00;&#x59CB;&#x65F6;&#x95F4;&#x548C;&#x7ED3;&#x675F;&#x65F6;&#x95F4;&#xFF0C;&#x7B2C;&#x4E09;&#x4E2A;&#x53C2;&#x6570;&#x662F;DateUnit&#x7684;&#x679A;&#x4E3E;&#xFF0C;
&#x8868;&#x793A;&#x5DEE;&#x503C;&#x4EE5;&#x4EC0;&#x4E48;&#x4E3A;&#x5355;&#x4F4D;&#x3002;

    DateUnit&#x7684;&#x53D6;&#x503C;&#x53EF;&#x4EE5;&#x662F;DateUnit.DAY&#xFF08;&#x5929;&#xFF09;,DateUnit.HOUR&#xFF08;&#x5C0F;&#x65F6;&#xFF09;,DateUnit.WEEK&#xFF08;&#x5468;&#xFF09;,
DateUnit.SECOND&#xFF08;&#x79D2;&#xFF09;,DateUnit.MINUTE(&#x5206;&#x949F;&#xFF09;&#xFF1B;

<span class="hljs-number">2</span>.formatBetween&#x65B9;&#x6CD5;
    &#x4E5F;&#x53EF;&#x4EE5;&#x5C06;&#x5DEE;&#x503C;&#x8F6C;&#x4E3A;&#x6307;&#x5B9A;&#x5177;&#x4F53;&#x7684;&#x683C;&#x5F0F;&#xFF0C;&#x6BD4;&#x5982; XX&#x5929;XX&#x5C0F;&#x65F6;XX&#x5206;&#x949F;XX&#x79D2; &#x8FD9;&#x6837;&#x7684;&#x683C;&#x5F0F;&#xFF0C;&#x53EF;&#x4EE5;&#x4F7F;&#x7528;
DateUtil.formatBetween(Date begin,Date end,Level):String  &#x65B9;&#x6CD5;&#xFF0C;&#x6709;&#x4E09;&#x4E2A;&#x53C2;&#x6570;&#xFF0C;
&#x524D;&#x4E24;&#x4E2A;&#x4F9D;&#x7136;&#x662F;&#x5F00;&#x59CB;&#x548C;&#x7ED3;&#x675F;&#x65F6;&#x95F4;&#xFF0C;&#x7B2C;&#x4E09;&#x4E2A;&#x53C2;&#x6570;&#x8868;&#x793A;&#x7CBE;&#x786E;&#x5EA6;&#xFF0C;&#x6BD4;&#x5982;Level.SECOND&#x8868;&#x793A;&#x7CBE;&#x786E;&#x5230;&#x79D2;&#xFF0C;
&#x5373;XX&#x5929;XX&#x5C0F;&#x65F6;XX&#x5206;&#x949F;XX&#x79D2;&#x7684;&#x683C;&#x5F0F;&#x3002;

<span class="hljs-number">3</span>.diff&#x65B9;&#x6CD5;
    &#x8FD4;&#x56DE;&#x4E24;&#x4E2A;&#x65E5;&#x671F;&#x7684;&#x65F6;&#x95F4;&#x5DEE;&#xFF0C;&#x53C2;&#x6570;diffField&#x5B9A;&#x4E49;&#x4E86;&#x8FD9;&#x4E2A;&#x5DEE;&#x7684;&#x5355;&#x4F4D;&#xFF0C;&#x5355;&#x4F4D;&#x7684;&#x5B9A;&#x4E49;&#x5728;DateUtil&#x7684;&#x5E38;&#x91CF;&#x4E2D;&#xFF0C;
&#x4F8B;&#x5982;DateUtil.SECOND_MS&#x8868;&#x793A;&#x4E24;&#x4E2A;&#x65E5;&#x671F;&#x76F8;&#x5DEE;&#x7684;&#x79D2;&#x6570;&#x3002;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

用了Hutool可以瞬间计算年龄，你还需要担心什么虚岁周岁吗？不需要，Hutool说多少就是多少。我们可以使用如下方法：

```
age(Date begin,Date end):<span class="hljs-keyword">int</span> &#x51FA;&#x751F;&#x548C;&#x53BB;&#x4E16;&#x65E5;&#x671F;&#x8BA1;&#x7B97;&#x5E74;&#x9F84;
ageOfNow(String birthday):<span class="hljs-keyword">int</span> &#x8BA1;&#x7B97;&#x5230;&#x5F53;&#x524D;&#x65E5;&#x671F;&#x7684;&#x5E74;&#x9F84;
ageOfNow(Date birthday):<span class="hljs-keyword">int</span> &#x8BA1;&#x7B97;&#x5230;&#x5F53;&#x524D;&#x65E5;&#x671F;&#x7684;&#x5E74;&#x9F84;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

使用 **DateUtil.isLeapYear(in year)**:boolean判断是否为闰年。

了解了DateUtil,再来看DateTime应该是很简单的，因为DateTime里面的大多数方法和DateUtil是类似的。DateTime类继承自java.util.Date，完全可以替代Date的使用并且还有其他的实用方法！

首先来了解一下DateTime的构造方法：

```
&#x65B9;&#x6CD5;&#x4E00;:&#x4F7F;&#x7528;<span class="hljs-keyword">new</span>
DateTime dt=<span class="hljs-keyword">new</span> DateTime();
DateTime dt=<span class="hljs-keyword">new</span> DateTime(Date date);
DateTime dt=<span class="hljs-keyword">new</span> DateTime(<span class="hljs-keyword">long</span> timeMillis);

&#x65B9;&#x6CD5;&#x4E8C;:&#x4F7F;&#x7528;of()&#x65B9;&#x6CD5;
DateTime dt=DateTime.of();

&#x65B9;&#x6CD5;&#x4E09;:&#x4F7F;&#x7528;now()&#x65B9;&#x6CD5;
DateTime dt=DateTime.now();

DateTime&#x91CC;&#x9762;&#x8FD8;&#x6709;&#x4E24;&#x4E2A;&#x975E;&#x5E38;&#x5B9E;&#x7528;&#x7684;&#x65B9;&#x6CD5;&#xFF0C;&#x5C31;&#x662F;before(Date when):<span class="hljs-keyword">boolean</span> &#x548C; after(Date when):<span class="hljs-keyword">boolean</span>&#xFF0C;
&#x5B83;&#x4EEC;&#x53EF;&#x4EE5;&#x5224;&#x65AD;&#x65F6;&#x95F4;&#x7684;&#x5148;&#x540E;&#x3002;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

最后了解一下DatePattern类，这个类主要是提供了一些 **指定的时间日期格式(都是String类型）**，包括地区的区别表示:

```
&#x683C;&#x5F0F;&#x5316;&#x6A21;&#x677F;:
    DatePattern.CHINESE_DATE_PATTERN
    DatePattern.NORM_DATE_PATTERN
    DatePattern.NORM_TIME_PATTERN
    DatePattern.NORM_DATETIME_PATTERN
    DatePattern.UTC_PATTERN
    DatePattern.PURE_DATE_PATTERN
    DatePattern.PURE_TIME_PATTERN
    DatePattern.PURE_DATETIME_PATTERN
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
&#x4F5C;&#x8005;&#xFF1A;&#x300C;612&#x661F;&#x7403;&#x7684;&#x4E00;&#x53EA;&#x5929;&#x624D;&#x732A;&#x300D;&#x8DEF;&#x5C0F;&#x78CA;
&#x539F;&#x6587;&#x94FE;&#x63A5;&#xFF1A;https://blog.csdn.net/tianc_pig/article/details/87826810
          https://my.oschina.net/looly/blog/268552
&#x6765;&#x6E90;&#xFF1A;CSDN oschina
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

=========================================================

## 2. Hutool之字符串工具——StrUtil

字符串工具指 `cn.hutool.core.util.StrUtil`类，其中对String的多种方法进行了封装并且提供了其他方便实用的方法。StrUtil中的方法是静态方法。

这里的空有两层含义：一是null或者""(空串)，二是不可见字符构成的字符串(不可见字符串)，比如由空格构成的字符串(" ")。针对两种情况Hutool提供了不同的方法来解决。
类似于 `Apache Commons Lang`中的StringUtil，之所以使用StrUtil而不是使用StringUtil是因为前者更短，而且Str这个简写我想已经深入人心。常用的方法, 例如isBlank、isNotBlank、isEmpty、isNotEmpty这些就不做介绍了，判断字符串是否为空，下面我说几个比较好用的功能。

常用的方法如下表所示：

```
isBlank(CharSequence arg0):<span class="hljs-keyword">boolean</span>
isEmpty(charSequence arg0):<span class="hljs-keyword">boolean</span>
hasBlank(CharSequence&#x2026;arg0):<span class="hljs-keyword">boolean</span>
hasEmpty(CharSequence&#x2026;arg0):<span class="hljs-keyword">boolean</span>

&#x8868;&#x5355;&#x767B;&#x5F55;&#x65F6;&#xFF0C;&#x5E38;&#x7528;hasEmpty()&#x65B9;&#x6CD5;&#x3002;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

sub()方法相当于subString()方法；
有三个参数：第一个是截取的字符串，后两个是首尾位置。sub()方法有一个最大的特点就是容错率非常高，并且-1表示最后一个字符，-2表示倒数第二个字符，以此类推。 并且首尾位置也可以颠倒。

```
String testStr=<span class="hljs-string">"my name is smartPig"</span>;
String result01=StrUtil.sub(testStr, <span class="hljs-number">0</span>, <span class="hljs-number">4</span>);

String result02=StrUtil.sub(testStr, <span class="hljs-number">4</span>, <span class="hljs-number">0</span>);
String result03=StrUtil.sub(testStr, -<span class="hljs-number">1</span>, <span class="hljs-number">3</span>);
String result04=StrUtil.sub(testStr, -<span class="hljs-number">4</span>, <span class="hljs-number">3</span>);

<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
format&#x65B9;&#x6CD5;&#x7C7B;&#x4F3C;&#x4E8E;JDBC&#x4E2D;PreparedStatement&#x4E2D;&#x7684;?&#x5360;&#x4F4D;&#x7B26;&#xFF0C;&#x53EF;&#x4EE5;&#x5728;&#x5B57;&#x7B26;&#x4E32;&#x4E2D;&#x4F7F;&#x7528;&#x201D;{}&#x201D;&#x4F5C;&#x4E3A;&#x5360;&#x4F4D;&#x7B26;&#x3002;
String testStr=<span class="hljs-string">"{} name is smart{}"</span>;
String result=StrUtil.format(testStr, <span class="hljs-string">"my"</span>,<span class="hljs-string">"Pig"</span>);

<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

Hutool可以去除字符串的前缀/后缀，这个特点非常适用于去除文件的后缀名。相关方法如下表：

```
removeSuffix(CharSequence str, CharSequence suffix)
removeSuffixIgnoreCase(CharSequence str, CharSequence suffix)

removePrefix(CharSequence str, CharSequence suffix)
removePrefixIgnoreCase(CharSequence str, CharSequence suffix)
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
 Hutool&#x5B9A;&#x4E49;&#x4E86;&#x4E00;&#x4E9B;&#x5B57;&#x7B26;&#x5E38;&#x91CF;&#xFF0C;&#x53EF;&#x4EE5;&#x7075;&#x6D3B;&#x4F7F;&#x7528;&#x3002;&#x90E8;&#x5206;&#x5E38;&#x91CF;&#x5982;&#x4E0B;&#x6240;&#x793A;&#xFF1A;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
StrUtil.DOT
StrUtil.DOUBLE_DOT
StrUtil.UNDERLINE
StrUtil.EMPTY
StrUtil.BACKSLASH
StrUtil.DASHED
StrUtil.BRACKET_END
StrUtil.BRACKET_START
StrUtil.COLON
StrUtil.COMMA
StrUtil.DELIM_END
StrUtil.DELIM_START
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
  Hutool&#x53EF;&#x4EE5;&#x5F88;&#x5BB9;&#x6613;&#x7684;&#x5B9E;&#x73B0;&#x5B57;&#x7B26;&#x4E32;&#x9006;&#x5E8F;&#x3002;&#x53EF;&#x4EE5;&#x4F7F;&#x7528;StrUtil.reverse(String str):String&#x65B9;&#x6CD5;&#x3002;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
&#x628A;String.getByte(String charsetName)&#x65B9;&#x6CD5;&#x5C01;&#x88C5;&#x5728;&#x8FD9;&#x91CC;&#x4E86;&#xFF0C;&#x539F;&#x751F;&#x7684;String.getByte()&#x8FD9;&#x4E2A;&#x65B9;&#x6CD5;&#x592A;&#x5751;&#x4E86;&#xFF0C;&#x4F7F;&#x7528;&#x7CFB;&#x7EDF;&#x7F16;&#x7801;&#xFF0C;
&#x7ECF;&#x5E38;&#x4F1A;&#x6709;&#x4EBA;&#x8DF3;&#x8FDB;&#x6765;&#x5BFC;&#x81F4;&#x4E71;&#x7801;&#x95EE;&#x9898;&#xFF0C;&#x6240;&#x4EE5;&#x5C31;&#x52A0;&#x4E86;&#x8FD9;&#x4E24;&#x4E2A;&#x65B9;&#x6CD5;&#x5F3A;&#x5236;&#x6307;&#x5B9A;&#x5B57;&#x7B26;&#x96C6;&#x4E86;&#xFF0C;&#x5305;&#x4E86;&#x4E2A;try&#x629B;&#x51FA;&#x4E00;&#x4E2A;&#x8FD0;&#x884C;&#x65F6;&#x5F02;&#x5E38;&#xFF0C;
&#x7701;&#x7684;&#x6211;&#x5F97;&#x5728;&#x4E1A;&#x52A1;&#x4EE3;&#x7801;&#x91CC;&#x5904;&#x7406;&#x90A3;&#x4E2A;&#x6076;&#x5FC3;&#x7684;UnsupportedEncodingException&#x3002;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
&#x4F5C;&#x8005;&#xFF1A;&#x300C;612&#x661F;&#x7403;&#x7684;&#x4E00;&#x53EA;&#x5929;&#x624D;&#x732A;&#x300D;&#x8DEF;&#x5C0F;&#x78CA;
&#x539F;&#x6587;&#x94FE;&#x63A5;&#xFF1A;https://blog.csdn.net/tianc_pig/article/details/87944463
          https://my.oschina.net/looly/blog/262775
&#x6765;&#x6E90;&#xFF1A;CSDN oschina
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

=========================================================

## 3. Hutool之随机工具——RandomUtil

随机工具的主要方法如下：

```
RandomUtil.randomInt &#x83B7;&#x5F97;&#x6307;&#x5B9A;&#x8303;&#x56F4;&#x5185;&#x7684;&#x968F;&#x673A;&#x6570;
RandomUtil.randomEle &#x968F;&#x673A;&#x83B7;&#x5F97;&#x5217;&#x8868;&#x4E2D;&#x7684;&#x5143;&#x7D20;
RandomUtil.randomString &#x83B7;&#x5F97;&#x4E00;&#x4E2A;&#x968F;&#x673A;&#x7684;&#x5B57;&#x7B26;&#x4E32;&#xFF08;&#x53EA;&#x5305;&#x542B;&#x6570;&#x5B57;&#x548C;&#x5B57;&#x7B26;&#xFF09;
RandomUtil.randomNumbers &#x83B7;&#x5F97;&#x4E00;&#x4E2A;&#x53EA;&#x5305;&#x542B;&#x6570;&#x5B57;&#x7684;&#x5B57;&#x7B26;&#x4E32;
RandomUtil.randomNumber &#x83B7;&#x5F97;&#x4E00;&#x4E2A;&#x968F;&#x673A;&#x6570;
RandomUtil.randomChar &#x83B7;&#x5F97;&#x968F;&#x673A;&#x5B57;&#x7B26;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

这些方法都有一些重载方法，如下表：

```
randomInt():<span class="hljs-keyword">int</span>
randomInt(<span class="hljs-keyword">int</span> limit):<span class="hljs-keyword">int</span>
randomInt(<span class="hljs-keyword">int</span> min,<span class="hljs-keyword">int</span> max):<span class="hljs-keyword">int</span>

randomChar():<span class="hljs-keyword">char</span>
randomChar(Strint str):<span class="hljs-keyword">char</span>

randomNumber():<span class="hljs-keyword">int</span>
randomNumbers(<span class="hljs-keyword">int</span> length):String

randomString(<span class="hljs-keyword">int</span> length):String
randomString(String str,<span class="hljs-keyword">int</span> length):String

randomEle(T array[]):T
randomEle(List<t> list):T
randomEle(List<t> list,<span class="hljs-keyword">int</span> limit):T
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span></t></t>
```

```
&#x4F5C;&#x8005;&#xFF1A;&#x300C;612&#x661F;&#x7403;&#x7684;&#x4E00;&#x53EA;&#x5929;&#x624D;&#x732A;&#x300D;
&#x539F;&#x6587;&#x94FE;&#x63A5;&#xFF1A;https://blog.csdn.net/tianc_pig/article/details/88034976
&#x6765;&#x6E90;&#xFF1A;CSDN
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

=========================================================

## 4. Hutool之正则工具——ReUtil

```
&#x4F7F;&#x7528; isMatch(String regex,CharSequence content):boolean
&#x7B2C;&#x4E00;&#x4E2A;&#x53C2;&#x6570;&#x662F;&#x6B63;&#x5219;&#xFF0C;&#x7B2C;&#x4E8C;&#x4E2A;&#x662F;&#x5339;&#x914D;&#x7684;&#x5185;&#x5BB9;&#x3002;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
delFirst(String regex,CharSequence content):String

delAll(String regex,CharSequence content):String
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
&#x4F7F;&#x7528;findAll(String regex, CharSequence content, int group): List<string>
&#x53EF;&#x4EE5;&#x83B7;&#x5F97;&#x6240;&#x6709;&#x5339;&#x914D;&#x7684;&#x5185;&#x5BB9;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span></string>
```

```
&#x4F7F;&#x7528;replaceAll(CharSequence content, String regex, String replacementTemplate):String
&#x53EF;&#x4EE5;&#x66FF;&#x6362;&#x5339;&#x914D;&#x7684;&#x5185;&#x5BB9;&#x3002;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
&#x4F7F;&#x7528;escape(CharSequence arg0):String
&#x53EF;&#x4EE5;&#x5C06;&#x7528;&#x4E8E;&#x6B63;&#x5219;&#x8868;&#x8FBE;&#x5F0F;&#x4E2D;&#x7684;&#x67D0;&#x4E9B;&#x7279;&#x6B8A;&#x5B57;&#x7B26;&#x8FDB;&#x884C;&#x8F6C;&#x4E49;&#xFF0C;&#x53D8;&#x6210;&#x8F6C;&#x4E49;&#x5B57;&#x7B26;&#x3002;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
&#x4F5C;&#x8005;&#xFF1A;&#x300C;612&#x661F;&#x7403;&#x7684;&#x4E00;&#x53EA;&#x5929;&#x624D;&#x732A;&#x300D;
&#x539F;&#x6587;&#x94FE;&#x63A5;&#xFF1A;https://blog.csdn.net/tianc_pig/article/details/88134882
&#x6765;&#x6E90;&#xFF1A;CSDN
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

=========================================================

## 5. Hutool之分页工具——PageUtil

```
PageUtil&#x7684;&#x9759;&#x6001;&#x65B9;&#x6CD5;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
transToStartEnd(<span class="hljs-keyword">int</span>,<span class="hljs-keyword">int</span>):<span class="hljs-keyword">int</span>[]

totalPage(<span class="hljs-keyword">int</span>,<span class="hljs-keyword">int</span>):<span class="hljs-keyword">int</span>

&#x5F69;&#x8679;&#x5206;&#x9875;&#x7B97;&#x6CD5;
    rainbow(<span class="hljs-keyword">int</span>,<span class="hljs-keyword">int</span>):<span class="hljs-keyword">int</span>[]

    rainbow(<span class="hljs-keyword">int</span>,<span class="hljs-keyword">int</span>,<span class="hljs-keyword">int</span>):<span class="hljs-keyword">int</span>[]

<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

彩虹分页算法应用于网页中需要经常显示【上一页】1 2 3 4 5 【下一页】这种页码的情况，一般都根据当前的页码动态的更新所显示出来的页码，因为每次所显示的页码的数量是固定的。

```

<span class="hljs-function"><span class="hljs-keyword">public</span> <span class="hljs-keyword">static</span> String <span class="hljs-title">rainbowPage</span><span class="hljs-params">(<span class="hljs-keyword">int</span> curPage,<span class="hljs-keyword">int</span> totalPage)</span> </span>{
	<span class="hljs-keyword">int</span> rainbow[]=PageUtil.rainbow(curPage,totalPage);
	<span class="hljs-keyword">return</span> Arrays.toString(rainbow);
}

<span class="hljs-function"><span class="hljs-keyword">public</span> <span class="hljs-keyword">static</span> String <span class="hljs-title">rainbowPage</span><span class="hljs-params">(<span class="hljs-keyword">int</span> curPage,<span class="hljs-keyword">int</span> totalPage,<span class="hljs-keyword">int</span> size)</span> </span>{
	<span class="hljs-keyword">int</span> rainbow[]=PageUtil.rainbow(curPage,totalPage,size);
	<span class="hljs-keyword">return</span> Arrays.toString(rainbow);
}
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

MyBatis就提供了分页工具，并且支持插件分页

```
&#x4F5C;&#x8005;&#xFF1A;&#x300C;612&#x661F;&#x7403;&#x7684;&#x4E00;&#x53EA;&#x5929;&#x624D;&#x732A;&#x300D;
&#x539F;&#x6587;&#x94FE;&#x63A5;&#xFF1A;https://blog.csdn.net/tianc_pig/article/details/88628323
&#x6765;&#x6E90;&#xFF1A;CSDN
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

=========================================================

## 6. Hutool之集合工具——CollectionUtil

主要增加了对数组、集合类的操作。

将集合转换为字符串，这个方法还是挺常用，是StrUtil.split的反方法。这个方法的参数支持各种类型对象的集合，最后连接每个对象时候调用其toString()方法。如下：

```
String[] col= <span class="hljs-keyword">new</span> String[]{a,b,c,d,e};
String str = CollectionUtil.join(col, <span class="hljs-string">"#"</span>);
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

功能是：将给定的多个集合放到一个列表（List）中，根据给定的Comparator对象排序，然后分页取数据。这个方法非常类似于数据库多表查询后排序分页，这个方法存在的意义也是在此。sortPageAll2功能和sortPageAll的使用方式和结果是 一样的，区别是sortPageAll2使用了BoundedPriorityQueue这个类来存储组合后的列表，不知道哪种性能更好一些，所以就都保留了。如下：

```
Comparator<integer> comparator = <span class="hljs-keyword">new</span> Comparator<integer>(){

	<span class="hljs-function"><span class="hljs-keyword">public</span> <span class="hljs-keyword">int</span> <span class="hljs-title">compare</span><span class="hljs-params">(Integer o1, Integer o2)</span> </span>{
		<span class="hljs-keyword">return</span> o1.compareTo(o2);
	}
};

List<integer> list1 = CollectionUtil.newArrayList(<span class="hljs-number">1</span>, <span class="hljs-number">2</span>, <span class="hljs-number">3</span>);
List<integer> list2 = CollectionUtil.newArrayList(<span class="hljs-number">4</span>, <span class="hljs-number">5</span>, <span class="hljs-number">6</span>);
List<integer> list3 = CollectionUtil.newArrayList(<span class="hljs-number">7</span>, <span class="hljs-number">8</span>, <span class="hljs-number">9</span>);

(<span class="hljs-string">"unchecked"</span>)
List<integer> result = CollectionUtil.sortPageAll(<span class="hljs-number">0</span>, <span class="hljs-number">2</span>, comparator, list1, list2, list3);
System.out.println(result);
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span></integer></integer></integer></integer></integer></integer>
```

主要是对 `Entry<long, long></long,>`按照Value的值做排序，使用局限性较大

这个方法传入一个栈对象，然后弹出指定数目的元素对象，弹出是指pop()方法，会从原栈中删掉。

这些方法是新建相应的数据结构，数据结构元素的类型取决于你变量的类型，如下：

```
HashMap<string, string> map = CollectionUtil.newHashMap();
HashSet<string> set = CollectionUtil.newHashSet();
ArrayList<string> list = CollectionUtil.newArrayList();
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span></string></string></string,>
```

在给定数组里末尾加一个元素，其实List.add()也是这么实现的，这个方法存在的意义是只有少量的添加元素时使用，因为内部使用了System.arraycopy,每调用一次就要拷贝数组一次。这个方法也是为了在某些只能使用数组的情况下使用，省去了先要转成List，添加元素，再转成Array。

重新调整数据的大小，如果调整后的大小比原来小，截断，如果比原来大，则多出的位置空着。（貌似List在扩充的时候会用到类似的方法）

将多个数据合并成一个数组

这个方法来源于Python的一个语法糖，给定开始和结尾以及步进，就会生成一个等差数列(列表)

```
<span class="hljs-keyword">int</span>[] a1 = CollectionUtil.range(<span class="hljs-number">6</span>);
<span class="hljs-keyword">int</span>[] a2 = CollectionUtil.range(<span class="hljs-number">4</span>, <span class="hljs-number">7</span>);
<span class="hljs-keyword">int</span>[] a3 = CollectionUtil.range(<span class="hljs-number">4</span>, <span class="hljs-number">9</span>, <span class="hljs-number">2</span>);
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

对集合切片，其他类型的集合会转换成List，封装List.subList方法，自动修正越界等问题，完全避免IndexOutOfBoundsException异常。

判断集合是否为空（包括null和没有元素的集合）。

此方法也是来源于Python的一个语法糖，给定两个集合，然后两个集合中的元素一一对应，成为一个Map。此方法还有一个重载方法，可以传字符，然后给定分隔符，字符串会被split成列表。

```
String[] keys = <span class="hljs-keyword">new</span> String[]{<span class="hljs-string">"a"</span>, <span class="hljs-string">"b"</span>, <span class="hljs-string">"c"</span>};
Integer[] values = <span class="hljs-keyword">new</span> Integer[]{<span class="hljs-number">1</span>, <span class="hljs-number">2</span>, <span class="hljs-number">3</span>};
Map<string, integer> map = CollectionUtil.zip(keys,values);
System.out.println(map);

String a = <span class="hljs-string">"a,b,c"</span>;
String b = <span class="hljs-string">"1,2,3"</span>;
Map<string, string> map2 = CollectionUtil.zip(a,b, <span class="hljs-string">","</span>);
System.out.println(map2);
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span></string,></string,>
```

```
&#x4F5C;&#x8005;&#xFF1A;&#x8DEF;&#x5C0F;&#x78CA;
&#x539F;&#x6587;&#x94FE;&#x63A5;&#xFF1A;https://my.oschina.net/looly/blog/262786
&#x6765;&#x6E90;&#xFF1A;oschina
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

=========================================================

## 7. Hutool之类处理工具——ClassUtil

主要是封装了一些反射的方法，而这个类中最有用的方法是scanPackage方法，这个方法会扫描classpath下所有类，这个在Spring中是特性之一，主要为Hulu框架中类扫描的一个基础

此方法唯一的参数是包的名称，返回结果为此包以及子包下所有的类。方法使用很简单，但是过程复杂一些，包扫面首先会调用 getClassPaths方法获得ClassPath，然后扫描ClassPath，如果是目录，扫描目录下的类文件，或者jar文件。如果是jar包，则直接从jar包中获取类名。这个方法的作用显而易见，就是要找出所有的类，在Spring中用于依赖注入，在Hulu中则用于找到Action类。当然，也可以传一个ClassFilter对象，用于过滤不需要的类。

此方法同Class对象的·getMethods·方法，只不过只返回方法的名称（字符串），封装非常简单。

此方法是获得当前线程的ClassPath，核心是Thread.currentThread().getContextClassLoader().getResources的调用。

此方法用于获得java的系统变量定义的ClassPath。

此方法封装了强制类型转换，首先会调用对象本身的cast方法，失败则尝试是否为基本类型（int,long,double,float等），再失败则尝试日期、数字和字节流，总之这是一个包容性较好的类型转换方法，省去我们在不知道类型的情况下多次尝试的繁琐。

此方法被parse方法调用，专门用于将字符集串转换为基本类型的对象(Integer,Double等等)。可以说这是一个一站式的转换方法，JDK的方法名太别扭了，例如你要转换成Long，你得调用Long.parseLong方法，直接Long.parse不就行了......真搞不懂，所以才有了这个方法。

这个方法比较别扭，就是把例如Integer类变成int.class，貌似没啥用处，忘了哪里用了，如果你能用到，就太好了。

后者只是获得当前线程的ClassLoader，前者在获取失败的时候获取ClassUtil这个类的ClassLoader。

实例化对象，封装了Class.forName(clazz).newInstance()方法。

克隆对象。对于有些对象没有实现Cloneable接口的对象想克隆下真是费劲，例如封装Redis客户端的时候，配置对象想克隆下基本不可能，于是写了这个方法，原理是使用ObjectOutputStream复制对象流。

```
&#x4F5C;&#x8005;&#xFF1A;&#x8DEF;&#x5C0F;&#x78CA;
&#x539F;&#x6587;&#x94FE;&#x63A5;&#xFF1A;https://my.oschina.net/looly/blog/268087
&#x6765;&#x6E90;&#xFF1A;oschina
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

=========================================================

## 8. Hutool之类文件工具——FileUtil

在Java工具中，文件操作应该也是使用相当频繁的，但是Java对文件的操作由于牵涉到流，所以较为繁琐，各种Stream也是眼花缭乱，因此大部分项目里的util包中我想都有一个FileUtil的类，而本类就是对众多FileUtil的总结。

这些方法都是按照Linux命令来命名的，方便熟悉Linux的用户见名知意，例如：

```
ls &#x8FD4;&#x56DE;&#x7ED9;&#x5B9A;&#x76EE;&#x5F55;&#x7684;&#x6240;&#x6709;&#x6587;&#x4EF6;&#x5BF9;&#x8C61;&#x5217;&#x8868;&#xFF0C;&#x8DEF;&#x5F84;&#x53EF;&#x4EE5;&#x662F;&#x76F8;&#x5BF9;ClassPath&#x8DEF;&#x5F84;&#x6216;&#x8005;&#x7EDD;&#x5BF9;&#x8DEF;&#x5F84;&#xFF0C;&#x4E0D;&#x53EF;&#x4EE5;&#x662F;&#x538B;&#x7F29;&#x5305;&#x91CC;&#x7684;&#x8DEF;&#x5F84;&#x3002;
listFileNames &#x5219;&#x662F;&#x8FD4;&#x56DE;&#x6307;&#x5B9A;&#x76EE;&#x5F55;&#x4E0B;&#x7684;&#x6240;&#x6709;&#x6587;&#x4EF6;&#x540D;&#xFF0C;&#x652F;&#x6301;jar&#x7B49;&#x538B;&#x7F29;&#x5305;&#x3002;
touch &#x521B;&#x5EFA;&#x6587;&#x4EF6;&#xFF0C;&#x5982;&#x679C;&#x7ED9;&#x5B9A;&#x8DEF;&#x5F84;&#x7236;&#x76EE;&#x5F55;&#x4E0D;&#x5B58;&#x5728;&#xFF0C;&#x4E5F;&#x4E00;&#x540C;&#x521B;&#x5EFA;&#x3002;
del &#x5220;&#x9664;&#x6587;&#x4EF6;&#x6216;&#x8005;&#x76EE;&#x5F55;&#xFF0C;&#x76EE;&#x5F55;&#x4E0B;&#x6709;&#x5D4C;&#x5957;&#x76EE;&#x5F55;&#x6216;&#x8005;&#x6587;&#x4EF6;&#x4F1A;&#x4E00;&#x8D77;&#x5220;&#x9664;&#x3002;
mkdir &#x521B;&#x5EFA;&#x76EE;&#x5F55;&#xFF0C;&#x7236;&#x76EE;&#x5F55;&#x4E0D;&#x5B58;&#x5728;&#x81EA;&#x52A8;&#x521B;&#x5EFA;&#x3002;
createTempFile &#x521B;&#x5EFA;&#x4E34;&#x65F6;&#x6587;&#x4EF6;&#xFF0C;&#x5728;&#x7A0B;&#x5E8F;&#x8FD0;&#x884C;&#x5B8C;&#x6BD5;&#x7684;&#x65F6;&#x5019;&#xFF0C;&#x8FD9;&#x4E2A;&#x6587;&#x4EF6;&#x4F1A;&#x88AB;&#x5220;&#x9664;&#x3002;
copy &#x590D;&#x5236;&#x6587;&#x4EF6;&#x6216;&#x76EE;&#x5F55;&#xFF0C;&#x76EE;&#x6807;&#x6587;&#x4EF6;&#x5BF9;&#x8C61;&#x53EF;&#x4EE5;&#x662F;&#x76EE;&#x5F55;&#xFF0C;&#x81EA;&#x52A8;&#x7528;&#x539F;&#x6587;&#x4EF6;&#x540D;&#xFF0C;&#x53EF;&#x4EE5;&#x9009;&#x62E9;&#x662F;&#x5426;&#x8986;&#x76D6;&#x76EE;&#x6807;&#x6587;&#x4EF6;&#x3002;
move &#x79FB;&#x52A8;&#x6587;&#x4EF6;&#x6216;&#x76EE;&#x5F55;&#xFF0C;&#x539F;&#x7406;&#x662F;&#x5148;&#x590D;&#x5236;&#xFF0C;&#x518D;&#x5220;&#x9664;&#x539F;&#x6587;&#x4EF6;&#x6216;&#x76EE;&#x5F55;
isExist &#x6587;&#x4EF6;&#x6216;&#x8005;&#x76EE;&#x5F55;&#x662F;&#x5426;&#x5B58;&#x5728;&#x3002;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
getAbsolutePath &#x83B7;&#x5F97;&#x7EDD;&#x5BF9;&#x8DEF;&#x5F84;&#xFF0C;&#x5982;&#x679C;&#x7ED9;&#x5B9A;&#x8DEF;&#x52B2;&#x5DF2;&#x7ECF;&#x662F;&#x7EDD;&#x5BF9;&#x8DEF;&#x5F84;&#xFF0C;&#x8FD4;&#x56DE;&#x539F;&#x8DEF;&#x5F84;&#xFF0C;&#x5426;&#x5219;&#x6839;&#x636E;ClassPath
        &#x6216;&#x8005;&#x7ED9;&#x5B9A;&#x7C7B;&#x7684;&#x76F8;&#x5BF9;&#x4F4D;&#x7F6E;&#x83B7;&#x5F97;&#x5176;&#x7EDD;&#x5BF9;&#x4F4D;&#x7F6E;
close &#x5BF9;&#x4E8E;&#x5B9E;&#x73B0;&#x4E86;Closeable&#x63A5;&#x53E3;&#x7684;&#x5BF9;&#x8C61;&#xFF0C;&#x53EF;&#x4EE5;&#x76F4;&#x63A5;&#x8C03;&#x7528;&#x6B64;&#x65B9;&#x6CD5;&#x5173;&#x95ED;&#xFF0C;&#x4E14;&#x662F;&#x9759;&#x9ED8;&#x5173;&#x95ED;&#xFF0C;&#x5173;&#x95ED;&#x51FA;&#x9519;&#x5C06;&#x4E0D;
        &#x4F1A;&#x6709;&#x4EFB;&#x4F55;&#x8C03;&#x8BD5;&#x4FE1;&#x606F;&#x3002;&#x8FD9;&#x4E2A;&#x65B9;&#x6CD5;&#x4E5F;&#x662F;&#x4F7F;&#x7528;&#x975E;&#x5E38;&#x9891;&#x7E41;&#x7684;&#xFF0C;&#x4F8B;&#x5982;&#x6587;&#x4EF6;&#x6D41;&#x7684;&#x5173;&#x95ED;&#x7B49;&#x7B49;&#x3002;
equals &#x6BD4;&#x8F83;&#x4E24;&#x4E2A;&#x6587;&#x4EF6;&#x662F;&#x5426;&#x76F8;&#x540C;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
getBufferedWriter &#x83B7;&#x5F97;&#x5E26;&#x7F13;&#x5B58;&#x7684;&#x5199;&#x5165;&#x5BF9;&#x8C61;&#xFF0C;&#x53EF;&#x4EE5;&#x5199;&#x5B57;&#x7B26;&#x4E32;&#x7B49;&#x3002;
getPrintWriter &#x5BF9; getBufferedWriter&#x7684;&#x5305;&#x88C5;&#xFF0C;&#x53EF;&#x4EE5;&#x6709;println&#x7B49;&#x65B9;&#x6CD5;&#x6309;&#x7167;&#x884C;&#x5199;&#x51FA;&#x3002;
getOutputStream &#x4F1A;&#x7684;&#x6587;&#x4EF6;&#x7684;&#x5199;&#x51FA;&#x6D41;&#x60F3;&#x5BF9;&#x8C61;&#x3002;
writeString&#x76F4;&#x63A5;&#x5199;&#x5B57;&#x7B26;&#x4E32;&#x5230;&#x6587;&#x4EF6;&#xFF0C;&#x4F1A;&#x8986;&#x76D6;&#x4E4B;&#x524D;&#x7684;&#x5185;&#x5BB9;&#x3002;
appendString &#x8FFD;&#x52A0;&#x5B57;&#x7B26;&#x4E32;&#x5230;&#x6587;&#x672C;&#x3002;
writeLines appendLines &#x8986;&#x76D6;&#x5199;&#x5165;&#x548C;&#x8FFD;&#x52A0;&#x6587;&#x672C;&#x5217;&#x8868;&#xFF0C;&#x6BCF;&#x4E2A;&#x5143;&#x7D20;&#x90FD;&#x662F;&#x4E00;&#x884C;&#x3002;
writeBytes &#x5199;&#x5B57;&#x8282;&#x7801;&#x3002;
writeStream &#x5199;&#x6D41;&#x4E2D;&#x7684;&#x5185;&#x5BB9;&#x5230;&#x6587;&#x4EF6;&#x91CC;&#x3002;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
getReader &#x83B7;&#x5F97;&#x5E26;&#x7F13;&#x5B58;&#x7684;Reader&#x5BF9;&#x8C61;&#x3002;
readLines &#x6309;&#x884C;&#x8BFB;&#x53D6;&#x6587;&#x4EF6;&#x4E2D;&#x7684;&#x6570;&#x636E;&#xFF0C;&#x8FD4;&#x56DE;List&#xFF0C;&#x6BCF;&#x4E00;&#x4E2A;&#x5143;&#x7D20;&#x90FD;&#x662F;&#x4E00;&#x884C;&#x6587;&#x672C;&#x3002;
load &#x6309;&#x7167;&#x7ED9;&#x5B9A;&#x7684;ReaderHandler&#x5BF9;&#x8C61;&#x8BFB;&#x53D6;&#x6587;&#x4EF6;&#x4E2D;&#x7684;&#x6570;&#x636E;&#xFF0C;ReaderHandler&#x662F;&#x4E00;&#x4E2A;&#x501F;&#x53E3;&#xFF0C;&#x5B9E;&#x73B0;&#x540E;&#x5C31;&#x53EF;&#x4EE5;&#x64CD;&#x4F5C;Reader&#x5BF9;&#x8C61;&#x4E86;&#xFF0C;&#x8FD9;&#x4E2A;&#x65B9;&#x6CD5;&#x5B58;&#x5728;&#x662F;&#x4E3A;&#x4E86;&#x907F;&#x514D;&#x7528;&#x6237;&#x624B;&#x52A8;&#x8C03;&#x7528;close&#x65B9;&#x6CD5;&#x3002;
readString &#x76F4;&#x63A5;&#x8BFB;&#x51FA;&#x6587;&#x4EF6;&#x4E2D;&#x7684;&#x6240;&#x6709;&#x6587;&#x672C;&#x3002;
readBytes &#x8BFB;&#x5B57;&#x8282;&#x7801;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
isModifed &#x6587;&#x4EF6;&#x662F;&#x5426;&#x88AB;&#x4FEE;&#x6539;&#x8FC7;&#xFF0C;&#x9700;&#x8981;&#x4F20;&#x5165;&#x4E00;&#x4E2A;&#x65F6;&#x95F4;&#x6233;&#xFF0C;&#x7528;&#x6765;&#x6BD4;&#x5BF9;&#x6700;&#x540E;&#x4FEE;&#x6539;&#x65F6;&#x95F4;&#x3002;
getExtension &#x83B7;&#x5F97;&#x6587;&#x4EF6;&#x7684;&#x6269;&#x5C55;&#x540D;&#x3002;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
&#x4F5C;&#x8005;&#xFF1A;&#x8DEF;&#x5C0F;&#x78CA;
&#x539F;&#x6587;&#x94FE;&#x63A5;&#xFF1A;https://my.oschina.net/looly/blog/288525
&#x6765;&#x6E90;&#xFF1A;oschina
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

=========================================================

## 9. Hutool之有界优先队列-BoundedPriorityQueue

举个例子: 我有一个用户表，这个表根据用户名被Hash到不同的数据库实例上，我要找出这些用户中最热门的5个，怎么做？我是这么做的：

```
&#x5728;&#x6BCF;&#x4E2A;&#x6570;&#x636E;&#x5E93;&#x5B9E;&#x4F8B;&#x4E0A;&#x627E;&#x51FA;&#x6700;&#x70ED;&#x95E8;&#x7684;5&#x4E2A;
&#x5C06;&#x6BCF;&#x4E2A;&#x6570;&#x636E;&#x5E93;&#x5B9E;&#x4F8B;&#x4E0A;&#x7684;&#x8FD9;5&#x6761;&#x6570;&#x636E;&#x6309;&#x7167;&#x70ED;&#x95E8;&#x7A0B;&#x5EA6;&#x6392;&#x5E8F;&#xFF0C;&#x6700;&#x540E;&#x53D6;&#x51FA;&#x524D;5&#x6761;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

这个过程看似简单，但是你应用服务器上的代码要写不少。首先需要Query N个列表，加入到一个新列表中，排序，再取前5。这个过程不但代码繁琐，而且牵涉到多个列表，非常浪费空间。于是， `BoundedPriorityQueu`e应运而生。

```

<span class="hljs-keyword">public</span> <span class="hljs-class"><span class="hljs-keyword">class</span> <span class="hljs-title">BoundedPriorityQueueDemo</span> </span>{
	<span class="hljs-function"><span class="hljs-keyword">public</span> <span class="hljs-keyword">static</span> <span class="hljs-keyword">void</span> <span class="hljs-title">main</span><span class="hljs-params">(String[] args)</span> </span>{

		BoundedPriorityQueue<integer> queue = <span class="hljs-keyword">new</span> BoundedPriorityQueue<integer>(<span class="hljs-number">5</span>);

		queue = <span class="hljs-keyword">new</span> BoundedPriorityQueue<>(<span class="hljs-number">5</span>, <span class="hljs-keyword">new</span> Comparator<integer>(){

			<span class="hljs-function"><span class="hljs-keyword">public</span> <span class="hljs-keyword">int</span> <span class="hljs-title">compare</span><span class="hljs-params">(Integer o1, Integer o2)</span> </span>{
				<span class="hljs-keyword">return</span> o1.compareTo(o2);
			}
		});

		<span class="hljs-keyword">int</span>[] array = <span class="hljs-keyword">new</span> <span class="hljs-keyword">int</span>[]{<span class="hljs-number">5</span>,<span class="hljs-number">7</span>,<span class="hljs-number">9</span>,<span class="hljs-number">2</span>,<span class="hljs-number">3</span>,<span class="hljs-number">8</span>};
		<span class="hljs-keyword">for</span> (<span class="hljs-keyword">int</span> i : array) {
		    queue.offer(i);
		}

		ArrayList<integer> list = queue.toList();

		System.out.println(queue);
	}
}

&#x539F;&#x7406;: &#x8BBE;&#x5B9A;&#x597D;&#x961F;&#x5217;&#x7684;&#x5BB9;&#x91CF;&#xFF0C;&#x7136;&#x540E;&#x628A;&#x6240;&#x6709;&#x7684;&#x6570;&#x636E;add&#x6216;&#x8005;offer&#x8FDB;&#x53BB;&#xFF08;&#x4E24;&#x4E2A;&#x65B9;&#x6CD5;&#x76F8;&#x540C;&#xFF09;&#xFF0C;&#x5C31;&#x4F1A;&#x5F97;&#x5230;&#x524D;<span class="hljs-number">5</span>&#x6761;&#x6570;&#x636E;&#x3002;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span></integer></integer></integer></integer>
```

```
&#x4F5C;&#x8005;&#xFF1A;&#x8DEF;&#x5C0F;&#x78CA;
&#x539F;&#x6587;&#x94FE;&#x63A5;&#xFF1A;https://my.oschina.net/looly/blog/389940
&#x6765;&#x6E90;&#xFF1A;oschina
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

=========================================================

## 10. Hutool之定时任务工具-CronUtil

Java中定时任务使用的最多的就是 **quartz**了，但是这个框架太过庞大，而且也不需要用到这么多东西，使用方法也是比较复杂。于是便寻找新的框架代替。用过Linux的 **crontab**的人都知道，使用其定时的表达式可以非常灵活的定义定时任务的时间以及频率(Linux的crontab精确到分，而quaeta的精确到秒，不过对来说精确到分已经够用了，精确到秒的可以使用Timer可以搞定)，然后就是crontab的配置文件，可以把定时任务很清晰的罗列出来。(记得当时Spring整合quartz的时候那XML看的眼都花了)。于是我便找到了一个轻量调度框架——cron4j

为了隐藏这个框架里面的东西，对其做了封装，所谓封装，就是把任务调度放在一个配置文件里，然后启动即可（与Linux的crontab非常像）。

对于Maven项目，首先在src/main/resources/config下放入cron4j.setting文件（默认是这个路径的这个文件），然后在文件中放入定时规则，规则如下：

```
#&#x6CE8;&#x91CA;
[&#x5305;&#x540D;]
TestJob = */10 * * * *
TestJob2 = */10 * * * *

&#x7B2C;&#x4E8C;&#x884C;&#x7B49;&#x53F7;&#x524D;&#x9762;&#x662F;&#x8981;&#x6267;&#x884C;&#x7684;&#x5B9A;&#x65F6;&#x4EFB;&#x52A1;&#x7C7B;&#x540D;&#xFF0C;&#x7B49;&#x53F7;&#x540E;&#x9762;&#x662F;&#x5B9A;&#x65F6;&#x8868;&#x8FBE;&#x5F0F;&#x3002;
TestJob&#x662F;&#x4E00;&#x4E2A;&#x5B9E;&#x73B0;&#x4E86;Runnable&#x63A5;&#x53E3;&#x7684;&#x7C7B;&#xFF0C;&#x5728;start()&#x65B9;&#x6CD5;&#x91CC;&#x5C31;&#x53EF;&#x4EE5;&#x52A0;&#x903B;&#x8F91;&#x3002;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

调用CronUtil.start()既可启动定时任务服务，CrontabUtil.stop()关闭服务。想动态的添加定时任务，使用CronUtil.schedule(String schedulingPattern, Runnable task)方法即可(使用此方法加入的定时任务不会被写入到配置文件)。

```
&#x4F5C;&#x8005;&#xFF1A;&#x8DEF;&#x5C0F;&#x78CA;
&#x539F;&#x6587;&#x94FE;&#x63A5;&#xFF1A;https://my.oschina.net/looly/blog/379677
&#x6765;&#x6E90;&#xFF1A;oschina
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

=========================================================

## 11. Hutool之类型转换类-Convert

在Java开发中会面对各种各样的类型转换问题，尤其是从命令行获取的用户参数、从HttpRequest获取的Parameter等等，这些参数类型多种多样，怎么去转换他们呢？常用的办法是先整成String，然后调用xxx. **parse**xxx方法，还要承受转换失败的风险，不得不加一层try catch，这个小小的过程混迹在业务代码中会显得非常难看和臃肿，于是把这种类型转换的任务封装在了Conver类中。

```
1. toStr&#x3001;toInt&#x3001;toLong&#x3001;toDouble&#x3001;toBool&#x65B9;&#x6CD5;
    &#x8FD9;&#x51E0;&#x4E2A;&#x65B9;&#x6CD5;&#x57FA;&#x672C;&#x4EE3;&#x66FF;&#x4E86;JDK&#x7684;XXX.parseXXX&#x65B9;&#x6CD5;&#xFF0C;&#x4F20;&#x5165;&#x4E24;&#x4E2A;&#x53C2;&#x6570;&#xFF0C;&#x7B2C;&#x4E00;&#x4E2A;&#x662F;Object&#x7C7B;&#x578B;&#x7684;&#x88AB;&#x8F6C;&#x6362;&#x7684;&#x503C;&#xFF0C;
    &#x7B2C;&#x4E8C;&#x4E2A;&#x53C2;&#x6570;&#x662F;&#x9ED8;&#x8BA4;&#x503C;&#x3002;&#x8FD9;&#x4E9B;&#x65B9;&#x6CD5;&#x505A;&#x8F6C;&#x6362;&#x5E76;&#x4E0D;&#x629B;&#x51FA;&#x5F02;&#x5E38;&#xFF0C;&#x5F53;&#x8F6C;&#x6362;&#x5931;&#x8D25;&#x6216;&#x8005;&#x63D0;&#x4F9B;&#x7684;&#x503C;&#x4E3A;null&#x65F6;&#xFF0C;
    &#x53EA;&#x4F1A;&#x8FD4;&#x56DE;&#x9ED8;&#x8BA4;&#x503C;&#xFF0C;&#x8FD4;&#x56DE;&#x7684;&#x7C7B;&#x578B;&#x5168;&#x90E8;&#x4F7F;&#x7528;&#x4E86;&#x5305;&#x88C5;&#x7C7B;&#xFF0C;&#x65B9;&#x4FBF;&#x6211;&#x4EEC;&#x9700;&#x8981;null&#x7684;&#x60C5;&#x51B5;&#x3002;

2. &#x534A;&#x89D2;&#x8F6C;&#x5168;&#x89D2;toSBC&#x548C;&#x5168;&#x89D2;&#x8F6C;&#x534A;&#x89D2;toDBC
    &#x5728;&#x5F88;&#x591A;&#x6587;&#x672C;&#x7684;&#x7EDF;&#x4E00;&#x5316;&#x4E2D;&#x8FD9;&#x4E24;&#x4E2A;&#x65B9;&#x6CD5;&#x975E;&#x5E38;&#x6709;&#x7528;&#xFF0C;&#x4E3B;&#x8981;&#x5BF9;&#x6807;&#x70B9;&#x7B26;&#x53F7;&#x7684;&#x5168;&#x89D2;&#x534A;&#x89D2;&#x8F6C;&#x6362;&#x3002;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
&#x4F5C;&#x8005;&#xFF1A;&#x8DEF;&#x5C0F;&#x78CA;
&#x539F;&#x6587;&#x94FE;&#x63A5;&#xFF1A;https://my.oschina.net/looly/blog/270829
&#x6765;&#x6E90;&#xFF1A;oschina
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

=========================================================

## 12. Hutool之单例池——Singleton

使用单例不外乎两种方式：

```
&#x5728;&#x5BF9;&#x8C61;&#x91CC;&#x52A0;&#x4E2A;&#x9759;&#x6001;&#x65B9;&#x6CD5;getInstance()&#x6765;&#x83B7;&#x53D6;&#x3002;
&#x6B64;&#x65B9;&#x5F0F;&#x53EF;&#x4EE5;&#x53C2;&#x8003; &#x3010;&#x8F6C;&#x3011;&#x7EBF;&#x7A0B;&#x5B89;&#x5168;&#x7684;&#x5355;&#x4F8B;&#x6A21;&#x5F0F;[https://my.oschina.net/looly/blog/152865] &#x8FD9;&#x7BC7;&#x535A;&#x5BA2;&#xFF0C;
&#x53EF;&#x5206;&#x4E3A;&#x997F;&#x6C49;&#x548C;&#x9971;&#x6C49;&#x6A21;&#x5F0F;&#x3002;

&#x901A;&#x8FC7;Spring&#x8FD9;&#x7C7B;&#x5BB9;&#x5668;&#x7EDF;&#x4E00;&#x7BA1;&#x7406;&#x5BF9;&#x8C61;&#xFF0C;&#x7528;&#x7684;&#x65F6;&#x5019;&#x53BB;&#x5BF9;&#x8C61;&#x6C60;&#x4E2D;&#x62FF;&#x3002;Spring&#x4E5F;&#x53EF;&#x4EE5;&#x901A;&#x8FC7;&#x914D;&#x7F6E;&#x51B3;&#x5B9A;&#x61D2;&#x6C49;&#x6216;&#x8005;&#x997F;&#x6C49;&#x6A21;&#x5F0F;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

说实话更倾向于第二种，但是Spring更对的的注入，而不是拿，于是想做Singleton这个类，维护一个单例的池，用这个单例对象的时候直接来拿就可以，这里用的懒汉模式。只是想把单例的管理方式换一种思路，希望管理单例的是一个容器工具，而不是一个大大的框架，这样能大大减少单例使用的复杂性。

```
<span class="hljs-keyword">import</span> com.xiaoleilu.hutool.Singleton;

<span class="hljs-keyword">public</span> <span class="hljs-class"><span class="hljs-keyword">class</span> <span class="hljs-title">SingletonDemo</span> </span>{

	<span class="hljs-keyword">public</span> <span class="hljs-keyword">static</span> <span class="hljs-class"><span class="hljs-keyword">interface</span> <span class="hljs-title">Animal</span></span>{
		<span class="hljs-function"><span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">say</span><span class="hljs-params">()</span></span>;
	}

	<span class="hljs-keyword">public</span> <span class="hljs-keyword">static</span> <span class="hljs-class"><span class="hljs-keyword">class</span> <span class="hljs-title">Dog</span> <span class="hljs-keyword">implements</span> <span class="hljs-title">Animal</span></span>{

		<span class="hljs-function"><span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">say</span><span class="hljs-params">()</span> </span>{
			System.out.println(<span class="hljs-string">"&#x6C6A;&#x6C6A;"</span>);
		}
	}

	<span class="hljs-keyword">public</span> <span class="hljs-keyword">static</span> <span class="hljs-class"><span class="hljs-keyword">class</span> <span class="hljs-title">Cat</span> <span class="hljs-keyword">implements</span> <span class="hljs-title">Animal</span></span>{

		<span class="hljs-function"><span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">say</span><span class="hljs-params">()</span> </span>{
			System.out.println(<span class="hljs-string">"&#x55B5;&#x55B5;"</span>);
		}
	}

	<span class="hljs-function"><span class="hljs-keyword">public</span> <span class="hljs-keyword">static</span> <span class="hljs-keyword">void</span> <span class="hljs-title">main</span><span class="hljs-params">(String[] args)</span> </span>{
		Animal dog = Singleton.get(Dog.class);
		Animal cat = Singleton.get(Cat.class);

		System.out.println(dog == Singleton.get(Dog.class));
		System.out.println(cat == Singleton.get(Cat.class));

		dog.say();
		cat.say();
	}
}
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

如有兴趣可以看下这个类，实现非常简单，一个HashMap用于做为单例对象池，通过newInstance()实例化对象（不支持带参数的构造方法），无论取还是创建对象都是线程安全的（在单例对象数量非常庞大且单例对象实例化非常耗时时可能会出现瓶颈），考虑到在get的时候使双重检查锁，但是并不是线程安全的，故直接加了synchronized做为修饰符，欢迎在此给出建议。

```
&#x4F5C;&#x8005;&#xFF1A;&#x8DEF;&#x5C0F;&#x78CA;
&#x539F;&#x6587;&#x94FE;&#x63A5;&#xFF1A;https://my.oschina.net/looly/blog/284922
&#x6765;&#x6E90;&#xFF1A;oschina
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

=========================================================

## 13. Hutool之Properties替代——Setting

对于JDK自带的Properties读取的Properties文件，有很多限制，首先是ISO8859-1编码导致没法加中文的value和注释（用日本的那个插件在Eclipse里可以读写，放到服务器上读就费劲了），再就是不支持变量分组等功能，因此有了Setting类。

配置文件中使用变量这个需求由来已久，在Spring中PropertyPlaceholderConfigurer类就用于在ApplicationContext.xml中使用Properties文件中的变量替换。 分组的概念我第一次在Linux的rsync的/etc/rsyncd.conf配置文件中有所了解，发现特别实用具体大家可以百度。

而这两种功能后来在jodd的Props才有所发现，它的这个配置文件扩展类十分强大，甚至支持多行等等功能，本来想直接使用，避免重复造轮子，可是发现很多特性完全用不到，而且没有需要的便捷功能，于是便造了Setting这个轮子。

配置文件格式example.setting

```
<!-- lang: shell -->
<span class="bash"> -------------------------------------------------------------</span>
<span class="bash"> ----- Setting File with UTF8-----</span>
<span class="bash"> ----- &#x6570;&#x636E;&#x5E93;&#x914D;&#x7F6E;&#x6587;&#x4EF6; -----</span>
<span class="bash"> -------------------------------------------------------------</span>
<span class="bash">&#x4E2D;&#x62EC;&#x8868;&#x793A;&#x4E00;&#x4E2A;&#x5206;&#x7EC4;&#xFF0C;&#x5176;&#x4E0B;&#x9762;&#x7684;&#x6240;&#x6709;&#x5C5E;&#x6027;&#x5F52;&#x5C5E;&#x4E8E;&#x8FD9;&#x4E2A;&#x5206;&#x7EC4;&#xFF0C;&#x5728;&#x6B64;&#x5206;&#x7EC4;&#x540D;&#x4E3A;demo&#xFF0C;&#x4E5F;&#x53EF;&#x4EE5;&#x6CA1;&#x6709;&#x5206;&#x7EC4;</span>
[demo]
<span class="bash">&#x81EA;&#x5B9A;&#x4E49;&#x6570;&#x636E;&#x6E90;&#x8BBE;&#x7F6E;&#x6587;&#x4EF6;&#xFF0C;&#x8FD9;&#x4E2A;&#x6587;&#x4EF6;&#x4F1A;&#x9488;&#x5BF9;&#x5F53;&#x524D;&#x5206;&#x7EC4;&#x751F;&#x6548;&#xFF0C;&#x7528;&#x4E8E;&#x7ED9;&#x5F53;&#x524D;&#x5206;&#x7EC4;&#x914D;&#x7F6E;&#x5355;&#x72EC;&#x7684;&#x6570;&#x636E;&#x5E93;&#x8FDE;&#x63A5;&#x6C60;&#x53C2;&#x6570;&#xFF0C;</span>
<span class="bash">&#x6CA1;&#x6709;&#x5219;&#x4F7F;&#x7528;&#x5168;&#x5C40;&#x7684;&#x914D;&#x7F6E;</span>
ds.setting.path = config/other.setting
<span class="bash">&#x6570;&#x636E;&#x5E93;&#x9A71;&#x52A8;&#x540D;&#xFF0C;&#x5982;&#x679C;&#x4E0D;&#x6307;&#x5B9A;&#xFF0C;&#x5219;&#x4F1A;&#x6839;&#x636E;url&#x81EA;&#x52A8;&#x5224;&#x5B9A;</span>
driver = com.mysql.jdbc.Driver
<span class="bash">JDBC url&#xFF0C;&#x5FC5;&#x987B;</span>
url = jdbc:mysql://fedora.vmware:3306/extractor
<span class="bash">&#x7528;&#x6237;&#x540D;&#xFF0C;&#x5FC5;&#x987B;</span>
user = root${driver}
<span class="bash">&#x5BC6;&#x7801;&#xFF0C;&#x5FC5;&#x987B;&#xFF0C;&#x5982;&#x679C;&#x5BC6;&#x7801;&#x4E3A;&#x7A7A;&#xFF0C;&#x8BF7;&#x586B;&#x5199; pass = </span>
pass = 123456
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

配置文件可以放在任意位置，具体Setting类如何寻在在构造方法中提供了多种读取方式，具体稍后介绍。现在说下配置文件的具体格式, Setting配置文件类似于Properties文件，规则如下：

* 注释用#开头表示，只支持单行注释，空行和无法正常被识别的键值对也会被忽略，可作为注释，但是建议显式指定注释。
* 键值对使用key = value 表示，key和value在读取时会trim掉空格，所以不用担心空格。
* 分组为中括号括起来的内容（例如配置文件中的[demo]），中括号以下的行都为此分组的内容，无分组相当于空字符分组，即[]。若某个key是name，分组是group，加上分组后的key相当于group.name。
* 支持变量，默认变量命名为![](https://juejin.im/equation?tex=%7B%E5%8F%98%E9%87%8F%E5%90%8D%7D%EF%BC%8C%E5%8F%98%E9%87%8F%E5%8F%AA%E8%83%BD%E8%AF%86%E5%88%AB%E8%AF%BB%E5%85%A5%E8%A1%8C%E7%9A%84%E5%8F%98%E9%87%8F%EF%BC%8C%E4%BE%8B%E5%A6%82%E7%AC%AC6%E8%A1%8C%E7%9A%84%E5%8F%98%E9%87%8F%E5%9C%A8%E7%AC%AC%E4%B8%89%E8%A1%8C%E6%97%A0%E6%B3%95%E8%AF%BB%E5%8F%96%EF%BC%8C%E4%BE%8B%E5%A6%82%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6%E4%B8%AD%E7%9A%84){driver}会被替换为com.mysql.jdbc.Driver，为了性能，Setting创建的时候构造方法会指定是否开启变量替换，默认不开启。

```
<!-- lang: java -->
<span class="hljs-keyword">import</span> java.io.IOException;

<span class="hljs-keyword">import</span> com.xiaoleilu.hutool.CharsetUtil;
<span class="hljs-keyword">import</span> com.xiaoleilu.hutool.FileUtil;
<span class="hljs-keyword">import</span> com.xiaoleilu.hutool.Setting;

<span class="hljs-keyword">public</span> <span class="hljs-class"><span class="hljs-keyword">class</span> <span class="hljs-title">SettingDemo</span> </span>{
	<span class="hljs-function"><span class="hljs-keyword">public</span> <span class="hljs-keyword">static</span> <span class="hljs-keyword">void</span> <span class="hljs-title">main</span><span class="hljs-params">(String[] args)</span> <span class="hljs-keyword">throws</span> IOException </span>{

		Setting setting = <span class="hljs-keyword">new</span> Setting(<span class="hljs-string">"XXX.setting"</span>);

		setting = <span class="hljs-keyword">new</span> Setting(<span class="hljs-string">"config/XXX.setting"</span>);

		setting = <span class="hljs-keyword">new</span> Setting(FileUtil.touch(<span class="hljs-string">"/home/looly/XXX.setting"</span>), CharsetUtil.UTF_8, <span class="hljs-keyword">true</span>);

		setting = <span class="hljs-keyword">new</span> Setting(<span class="hljs-string">"XXX.setting"</span>, SettingDemo.class, CharsetUtil.UTF_8, <span class="hljs-keyword">true</span>);

		setting.getString(<span class="hljs-string">"name"</span>);

		setting.getString(<span class="hljs-string">"name"</span>, <span class="hljs-string">"group1"</span>);

		setting.getStringWithDefault(<span class="hljs-string">"name"</span>, <span class="hljs-string">"&#x9ED8;&#x8BA4;&#x503C;"</span>);

		setting.getStringWithDefault(<span class="hljs-string">"name"</span>, <span class="hljs-string">"group1"</span>, <span class="hljs-string">"&#x9ED8;&#x8BA4;&#x503C;"</span>);

		setting.getWithLog(<span class="hljs-string">"name"</span>);
		setting.getWithLog(<span class="hljs-string">"name"</span>, <span class="hljs-string">"group1"</span>);

		setting.reload();

		setting.setSetting(<span class="hljs-string">"name1"</span>, <span class="hljs-string">"value"</span>);
		setting.store(<span class="hljs-string">"/home/looly/XXX.setting"</span>);

		setting.getGroups();

		setting.setVarRegex(<span class="hljs-string">"\\$\\{(.*?)\\}"</span>);
	}
}
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

对Properties的简单封装Props(版本2.0.0开始提供)

对于Properties的广泛使用使我也无能为力，有时候遇到Properties文件又想方便的读写也不容易，于是对Properties做了简单的封装，提供了方便的构造方法（与Setting一致），并提供了与Setting一致的getXXX方法来扩展Properties类，Props类继承自Properties，所以可以兼容Properties类，具体不再做介绍，有兴趣可以看下 `cn.hutool.setting.dialect.Props`

```
&#x4F5C;&#x8005;&#xFF1A;&#x8DEF;&#x5C0F;&#x78CA;
&#x539F;&#x6587;&#x94FE;&#x63A5;&#xFF1A;https://my.oschina.net/looly/blog/302407
&#x6765;&#x6E90;&#xFF1A;oschina
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

=========================================================

## 14. Hutool-crypto模块

AES和DES同属对称加密算法，数据发信方将明文（原始数据）和加密密钥一起经过特殊加密算法处理后，使其变成复杂的加密密文发送出去。收信方收到密文后，若想解读原文，则需要使用加密用过的密钥及相同算法的逆算法对密文进行解密，才能使其恢复成可读明文。在对称加密算法中，使用的密钥只有一个，发收信双方都使用这个密钥对数据进行加密和解密，这就要求解密方事先必须知道加密密钥。

在Java世界中，AES、DES加密解密需要使用 **Cipher对象**构建加密解密系统，Hutool中对这一对象做再包装，简化了加密解密过程。

```
<dependency>
    <groupid>cn.hutool</groupid>
    <artifactid>hutool-all</artifactid>
    <version>4.1.2</version>
</dependency>
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
** AES&#x52A0;&#x5BC6;&#x89E3;&#x5BC6;

String content = <span class="hljs-string">"test&#x4E2D;&#x6587;"</span>;
byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();//&#x968F;&#x673A;&#x751F;&#x6210;&#x5BC6;&#x94A5;
AES aes = SecureUtil.aes(key);//&#x6784;&#x5EFA;
byte[] encrypt = aes.encrypt(content);//&#x52A0;&#x5BC6;
byte[] decrypt = aes.decrypt(encrypt);//&#x89E3;&#x5BC6;

String encryptHex = aes.encryptHex(content);//&#x52A0;&#x5BC6;&#x4E3A;16&#x8FDB;&#x5236;&#x8868;&#x793A;
String decryptStr = aes.decryptStr(encryptHex);//&#x89E3;&#x5BC6;&#x4E3A;&#x539F;&#x5B57;&#x7B26;&#x4E32;

** DES&#x52A0;&#x5BC6;&#x89E3;&#x5BC6;, DES&#x7684;&#x4F7F;&#x7528;&#x65B9;&#x5F0F;&#x4E0E;AES&#x57FA;&#x672C;&#x4E00;&#x81F4;

String content = <span class="hljs-string">"test&#x4E2D;&#x6587;"</span>;
byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.DES.getValue()).getEncoded();//&#x968F;&#x673A;&#x751F;&#x6210;&#x5BC6;&#x94A5;
DES des = SecureUtil.des(key);//&#x6784;&#x5EFA;

byte[] encrypt = des.encrypt(content);//&#x52A0;&#x5BC6;&#x89E3;&#x5BC6;
byte[] decrypt = des.decrypt(encrypt);

String encryptHex = des.encryptHex(content);//&#x52A0;&#x5BC6;&#x4E3A;16&#x8FDB;&#x5236;&#xFF0C;&#x89E3;&#x5BC6;&#x4E3A;&#x539F;&#x5B57;&#x7B26;&#x4E32;
String decryptStr = des.decryptStr(encryptHex);
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

Hutool中针对JDK支持的所有对称加密算法做了封装，封装为 **SymmetricCrypto**类，AES和DES两个类是此类的简化表示。通过实例化这个类传入相应的算法枚举即可使用相同方法加密解密字符串或对象。

Hutool支持的对称加密算法枚举有：

```
AES
ARCFOUR
Blowfish
DES
DESede
RC2
PBEWithMD5AndDES
PBEWithSHA1AndDESede
PBEWithSHA1AndRC2_40
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

这些枚举全部在 **SymmetricAlgorithm**中被列举

```
&#x5BF9;&#x79F0;&#x52A0;&#x5BC6;&#x5BF9;&#x8C61;&#x7684;&#x4F7F;&#x7528;&#x4E5F;&#x975E;&#x5E38;&#x7B80;&#x5355;&#xFF1A;

String content = <span class="hljs-string">"test&#x4E2D;&#x6587;"</span>;

<span class="hljs-keyword">byte</span>[] key = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();

SymmetricCrypto aes = <span class="hljs-keyword">new</span> SymmetricCrypto(SymmetricAlgorithm.AES, key);

<span class="hljs-keyword">byte</span>[] encrypt = aes.encrypt(content);
<span class="hljs-keyword">byte</span>[] decrypt = aes.decrypt(encrypt);

String encryptHex = aes.encryptHex(content);
String decryptStr = aes.decryptStr(encryptHex);
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
&#x4F5C;&#x8005;&#xFF1A;&#x8DEF;&#x5C0F;&#x78CA;
&#x539F;&#x6587;&#x94FE;&#x63A5;&#xFF1A;https://my.oschina.net/looly/blog/1504160
&#x6765;&#x6E90;&#xFF1A;oschina
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

Hutool工具是一个国产开源Java工具集，旨在简化Java开发中繁琐的过程，Hutool-crypto模块便是针对JDK加密解密做了大大简化。
此文主要介绍利用Hutool-crypto简化非对称加密解密。
对于非对称加密，最常用的就是RSA和DSA，在Hutool中使用AsymmetricCrypto对象来负责加密解密。
非对称加密有公钥和私钥两个概念，私钥自己拥有，不能给别人，公钥公开。根据应用的不同，我们可以选择使用不同的密钥加密：

```
&#x7B7E;&#x540D;&#xFF1A;&#x4F7F;&#x7528;&#x79C1;&#x94A5;&#x52A0;&#x5BC6;&#xFF0C;&#x516C;&#x94A5;&#x89E3;&#x5BC6;&#x3002;&#x7528;&#x4E8E;&#x8BA9;&#x6240;&#x6709;&#x516C;&#x94A5;&#x6240;&#x6709;&#x8005;&#x9A8C;&#x8BC1;&#x79C1;&#x94A5;&#x6240;&#x6709;&#x8005;&#x7684;&#x8EAB;&#x4EFD;&#x5E76;&#x4E14;&#x7528;&#x6765;&#x9632;&#x6B62;&#x79C1;&#x94A5;&#x6240;&#x6709;&#x8005;&#x53D1;&#x5E03;&#x7684;&#x5185;&#x5BB9;&#x88AB;&#x7BE1;&#x6539;&#xFF0C;
      &#x4F46;&#x662F;&#x4E0D;&#x7528;&#x6765;&#x4FDD;&#x8BC1;&#x5185;&#x5BB9;&#x4E0D;&#x88AB;&#x4ED6;&#x4EBA;&#x83B7;&#x5F97;&#x3002;

&#x52A0;&#x5BC6;&#xFF1A;&#x7528;&#x516C;&#x94A5;&#x52A0;&#x5BC6;&#xFF0C;&#x79C1;&#x94A5;&#x89E3;&#x5BC6;&#x3002;&#x7528;&#x4E8E;&#x5411;&#x516C;&#x94A5;&#x6240;&#x6709;&#x8005;&#x53D1;&#x5E03;&#x4FE1;&#x606F;,&#x8FD9;&#x4E2A;&#x4FE1;&#x606F;&#x53EF;&#x80FD;&#x88AB;&#x4ED6;&#x4EBA;&#x7BE1;&#x6539;,&#x4F46;&#x662F;&#x65E0;&#x6CD5;&#x88AB;&#x4ED6;&#x4EBA;&#x83B7;&#x5F97;&#x3002;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
<dependency>
    <groupid>cn.hutool</groupid>
    <artifactid>hutool-all</artifactid>
    <version>4.1.2</version>
</dependency>
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

在非对称加密中，我们可以通过AsymmetricCrypto(AsymmetricAlgorithm algorithm)构造方法，通过传入不同的算法枚举，获得其加密解密器。
当然，为了方便，我们针对最常用的RSA和DSA算法构建了单独的对象：RSA和DSA。

以RSA为例，介绍使用RSA加密和解密
在构建RSA对象时，可以传入公钥或私钥，当使用无参构造方法时，Hutool将自动生成随机的公钥私钥密钥对：

```
RSA rsa = <span class="hljs-keyword">new</span> RSA();

rsa.getPrivateKey()
rsa.getPrivateKeyBase64()

rsa.getPublicKey()
rsa.getPublicKeyBase64()

<span class="hljs-keyword">byte</span>[] encrypt = rsa.encrypt(StrUtil.bytes(<span class="hljs-string">"&#x6211;&#x662F;&#x4E00;&#x6BB5;&#x6D4B;&#x8BD5;aaaa"</span>, CharsetUtil.CHARSET_UTF_8),
            KeyType.PublicKey);
<span class="hljs-keyword">byte</span>[] decrypt = rsa.decrypt(encrypt, KeyType.PrivateKey);
Assert.assertEquals(<span class="hljs-string">"&#x6211;&#x662F;&#x4E00;&#x6BB5;&#x6D4B;&#x8BD5;aaaa"</span>, StrUtil.str(decrypt, CharsetUtil.CHARSET_UTF_8));

<span class="hljs-keyword">byte</span>[] encrypt2 = rsa.encrypt(StrUtil.bytes(<span class="hljs-string">"&#x6211;&#x662F;&#x4E00;&#x6BB5;&#x6D4B;&#x8BD5;aaaa"</span>, CharsetUtil.CHARSET_UTF_8), KeyType.PrivateKey);
<span class="hljs-keyword">byte</span>[] decrypt2 = rsa.decrypt(encrypt2, KeyType.PublicKey);
Assert.assertEquals(<span class="hljs-string">"&#x6211;&#x662F;&#x4E00;&#x6BB5;&#x6D4B;&#x8BD5;aaaa"</span>, StrUtil.str(decrypt2, CharsetUtil.CHARSET_UTF_8));
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

对于加密和解密可以完全分开，对于RSA对象，如果只使用公钥或私钥，另一个参数可以为null

```
&#x6709;&#x65F6;&#x5019;&#x60F3;&#x81EA;&#x52A9;&#x751F;&#x6210;&#x5BC6;&#x94A5;&#x5BF9;&#x53EF;&#x4EE5;&#xFF1A;
KeyPair pair = SecureUtil.generateKeyPair(<span class="hljs-string">"RSA"</span>);
pair.getPrivate();
pair.getPublic();
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

自助生成的密钥对是byte[]形式，我们可以使用Base64.encode方法转为Base64，便于存储为文本。
当然，如果使用RSA对象，也可以使用encryptStr和decryptStr加密解密为字符串

```
&#x5DF2;&#x77E5;&#x79C1;&#x94A5;&#x548C;&#x5BC6;&#x6587;&#xFF0C;&#x5982;&#x4F55;&#x89E3;&#x5BC6;&#x5BC6;&#x6587;&#xFF1F;

String PRIVATE_KEY = <span class="hljs-string">"MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAIL7pbQ+5KKGYRhw7jE31hmA"</span>
		+ <span class="hljs-string">"f8Q60ybd+xZuRmuO5kOFBRqXGxKTQ9TfQI+aMW+0lw/kibKzaD/EKV91107xE384qOy6IcuBfaR5lv39OcoqNZ"</span>
		+ <span class="hljs-string">"5l+Dah5ABGnVkBP9fKOFhPgghBknTRo0/rZFGI6Q1UHXb+4atP++LNFlDymJcPAgMBAAECgYBammGb1alndta"</span>
		+ <span class="hljs-string">"xBmTtLLdveoBmp14p04D8mhkiC33iFKBcLUvvxGg2Vpuc+cbagyu/NZG+R/WDrlgEDUp6861M5BeFN0L9O4hz"</span>
		+ <span class="hljs-string">"GAEn8xyTE96f8sh4VlRmBOvVdwZqRO+ilkOM96+KL88A9RKdp8V2tna7TM6oI3LHDyf/JBoXaQJBAMcVN7fKlYP"</span>
		+ <span class="hljs-string">"Skzfh/yZzW2fmC0ZNg/qaW8Oa/wfDxlWjgnS0p/EKWZ8BxjR/d199L3i/KMaGdfpaWbYZLvYENqUCQQCobjsuCW"</span>
		+ <span class="hljs-string">"nlZhcWajjzpsSuy8/bICVEpUax1fUZ58Mq69CQXfaZemD9Ar4omzuEAAs2/uee3kt3AvCBaeq05NyjAkBme8SwB0iK"</span>
		+ <span class="hljs-string">"kLcaeGuJlq7CQIkjSrobIqUEf+CzVZPe+AorG+isS+Cw2w/2bHu+G0p5xSYvdH59P0+ZT0N+f9LFAkA6v3Ae56OrI"</span>
		+ <span class="hljs-string">"wfMhrJksfeKbIaMjNLS9b8JynIaXg9iCiyOHmgkMl5gAbPoH/ULXqSKwzBw5mJ2GW1gBlyaSfV3AkA/RJC+adIjsRGg"</span>
		+ <span class="hljs-string">"JOkiRjSmPpGv3FOhl9fsBPjupZBEIuoMWOC8GXK/73DHxwmfNmN7C9+sIi4RBcjEeQ5F5FHZ"</span>;

RSA rsa = <span class="hljs-keyword">new</span> RSA(PRIVATE_KEY, <span class="hljs-keyword">null</span>);

String a = <span class="hljs-string">"2707F9FD4288CEF302C972058712F24A5F3EC62C5A14AD2FC59DAB93503AA0FA17113A020EE4EA35EB53F"</span>
		+ <span class="hljs-string">"75F36564BA1DABAA20F3B90FD39315C30E68FE8A1803B36C29029B23EB612C06ACF3A34BE815074F5EB5AA3A"</span>
		+ <span class="hljs-string">"C0C8832EC42DA725B4E1C38EF4EA1B85904F8B10B2D62EA782B813229F9090E6F7394E42E6F44494BB8"</span>;

<span class="hljs-keyword">byte</span>[] aByte = HexUtil.decodeHex(a);
<span class="hljs-keyword">byte</span>[] decrypt = rsa.decrypt(aByte, KeyType.PrivateKey);
Assert.assertEquals(<span class="hljs-string">"&#x864E;&#x5934;&#x95EF;&#x676D;&#x5DDE;,&#x591A;&#x62AC;&#x5934;&#x770B;&#x5929;,&#x5207;&#x52FF;&#x53EA;&#x7BA1;&#x79CD;&#x5730;"</span>, StrUtil.str(decrypt, CharsetUtil.CHARSET_UTF_8));
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
&#x4F5C;&#x8005;&#xFF1A;&#x8DEF;&#x5C0F;&#x78CA;
&#x539F;&#x6587;&#x94FE;&#x63A5;&#xFF1A;https://my.oschina.net/looly/blog/1523094
&#x6765;&#x6E90;&#xFF1A;oschina
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

=========================================================

## 15. Hutool-core模块

`cn.hutool.core.bean`包下的DynaBean、BeanDesc、BeanDesc.PropDesc、BeanPath、BeanUtil

```
&#x4F5C;&#x8005;&#xFF1A;&#x8C62;&#x9F99;&#x5148;&#x751F;
&#x539F;&#x6587;&#x94FE;&#x63A5;&#xFF1A;https://blog.csdn.net/sanjun333/article/details/90645420
&#x6765;&#x6E90;&#xFF1A;CSDN
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

=========================================================

## 16. Hutool-http模块

使用Hutool爬取开源中国的开源资讯, 演示Hutool-http的http请求功能.

1.打开oschina的主页，我们找到最显眼的开源资讯模块，然后点击"更多"，打开"开源资讯"板块。

2.打开F12调试器，点击快捷键F12打开Chrome的调试器，点击"Network"选项卡，然后在页面上点击"全部资讯"。

3.由于oschina的列表页是通过下拉翻页的，因此下拉到底部会触发第二页的加载，此时我们下拉到底部，然后观察调试器中是否有新的请求出现。如图，我们发现第二个请求是列表页的第二页。

4.我们打开这个请求地址，可以看到纯纯的内容。红框所指地址为第二页的内容，很明显p参数代表了页码page。

5.我们右键点击后查看源码，可以看到源码。

6.找到标题部门的HTML源码，然后搜索这个包围这个标题的HTML部分，看是否可以定位标题。

至此分析完毕，我们拿到了列表页的地址，也拿到了可以定位标题的相关字符（在后面用正则提取标题用），就可以开始使用Hutool编码了。

```
&#x4F7F;&#x7528;Hutool-http&#x914D;&#x5408;ReUtil&#x8BF7;&#x6C42;&#x5E76;&#x63D0;&#x53D6;&#x9875;&#x9762;&#x5185;&#x5BB9;&#x975E;&#x5E38;&#x7B80;&#x5355;&#xFF0C;&#x4EE3;&#x7801;&#x5982;&#x4E0B;&#xFF1A;

//&#x8BF7;&#x6C42;&#x5217;&#x8868;&#x9875;
String listContent = HttpUtil.get("http://www.oschina.net/action/ajax/get_more_news_list?newsType=&p=2");
//&#x4F7F;&#x7528;&#x6B63;&#x5219;&#x83B7;&#x53D6;&#x6240;&#x6709;&#x6807;&#x9898;
List<string> titles = ReUtil.findAll("<span class="\"text-ellipsis\"">(.*?)</span>", listContent, 1);
for (String title : titles) {
	Console.log(title);//&#x6253;&#x5370;&#x6807;&#x9898;
}

&#x7B2C;&#x4E00;&#x884C;&#x8BF7;&#x6C42;&#x9875;&#x9762;&#x5185;&#x5BB9;&#xFF0C;&#x7B2C;&#x4E8C;&#x884C;&#x6B63;&#x5219;&#x5B9A;&#x4F4D;&#x6240;&#x6709;&#x6807;&#x9898;&#x884C;&#x5E76;&#x63D0;&#x53D6;&#x6807;&#x9898;&#x90E8;&#x5206;&#x3002;

&#x8FD9;&#x91CC;&#x89E3;&#x91CA;&#x4E0B;&#x6B63;&#x5219;&#x90E8;&#x5206;&#xFF1A;
ReUtil.findAll&#x65B9;&#x6CD5;&#x7528;&#x4E8E;&#x67E5;&#x627E;&#x6240;&#x6709;&#x5339;&#x914D;&#x6B63;&#x5219;&#x8868;&#x8FBE;&#x5F0F;&#x7684;&#x5185;&#x5BB9;&#x90E8;&#x5206;&#xFF0C;&#x7B2C;&#x4E8C;&#x4E2A;&#x53C2;&#x6570;1&#x8868;&#x793A;&#x63D0;&#x53D6;&#x7B2C;&#x4E00;&#x4E2A;&#x62EC;&#x53F7;&#xFF08;&#x5206;&#x7EC4;&#xFF09;&#x4E2D;&#x7684;&#x5185;&#x5BB9;&#xFF0C;
0&#x8868;&#x793A;&#x63D0;&#x53D6;&#x6240;&#x6709;&#x6B63;&#x5219;&#x5339;&#x914D;&#x5230;&#x7684;&#x5185;&#x5BB9;&#x3002;&#x8FD9;&#x4E2A;&#x65B9;&#x6CD5;&#x53EF;&#x4EE5;&#x770B;&#x4E0B;core&#x6A21;&#x5757;&#x4E2D;ReUtil&#x7AE0;&#x8282;&#x4E86;&#x89E3;&#x8BE6;&#x60C5;&#x3002;

<span class="\"text-ellipsis\"">(.*?)</span>&#x8FD9;&#x4E2A;&#x6B63;&#x5219;&#x5C31;&#x662F;&#x6211;&#x4EEC;&#x4E0A;&#x9762;&#x5206;&#x6790;&#x9875;&#x9762;&#x6E90;&#x7801;&#x540E;&#x5F97;&#x5230;&#x7684;&#x6B63;&#x5219;&#xFF0C;
&#x5176;&#x4E2D;(.*?)&#x8868;&#x793A;&#x6211;&#x4EEC;&#x9700;&#x8981;&#x7684;&#x5185;&#x5BB9;&#xFF0C;.&#x8868;&#x793A;&#x4EFB;&#x610F;&#x5B57;&#x7B26;&#xFF0C;*&#x8868;&#x793A;0&#x4E2A;&#x6216;&#x591A;&#x4E2A;&#xFF0C;?&#x8868;&#x793A;&#x6700;&#x77ED;&#x5339;&#x914D;&#xFF0C;&#x6574;&#x4E2A;&#x6B63;&#x5219;&#x7684;&#x610F;&#x601D;&#x5C31;&#x662F;&#x3002;
&#xFF0C;&#x4EE5;<span class="\"text-ellipsis\"">&#x5F00;&#x5934;&#xFF0C;</span>&#x7ED3;&#x5C3E;&#x7684;&#x4E2D;&#x95F4;&#x6240;&#x6709;&#x5B57;&#x7B26;&#xFF0C;&#x4E2D;&#x95F4;&#x7684;&#x5B57;&#x7B26;&#x8981;&#x8FBE;&#x5230;&#x6700;&#x77ED;&#x3002;
?&#x7684;&#x4F5C;&#x7528;&#x5176;&#x5B9E;&#x5C31;&#x662F;&#x5C06;&#x8303;&#x56F4;&#x9650;&#x5236;&#x5230;&#x6700;&#x5C0F;&#xFF0C;&#x4E0D;&#x7136;&#x5F88;&#x53EF;&#x80FD;&#x5339;&#x914D;&#x5230;&#x540E;&#x9762;&#x53BB;&#x4E86;&#x3002;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span></string>
```

不得不说，抓取本身并不困难，尤其配合Hutool会让这项工作变得更加简单快速，而其中的难点便是分析页面和定位我们需要的内容。

真正的内容抓取分为连个部分：

* 找到列表页（很多网站都没有一个总的列表页）
* 请求列表页，获取详情页地址
* 请求详情页并使用正则匹配我们需要的内容
* 入库或将内容保存为文件

而且在抓取过程中我们也会遇到各种问题，包括但不限于：

* 封IP
* 对请求Header有特殊要求
* 对Cookie有特殊要求
* 验证码

```
&#x4F5C;&#x8005;&#xFF1A;&#x8DEF;&#x5C0F;&#x78CA;
&#x539F;&#x6587;&#x94FE;&#x63A5;&#xFF1A;https://my.oschina.net/looly/blog/1575851
&#x6765;&#x6E90;&#xFF1A;oschina
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

=========================================================

## 17. Hutool之验证码工具——CaptchaUtil

使用Hutool [生成和验证] 图形验证码

随着攻击防护做的越来越全面，而图形验证码又是一种简单有效的防攻击和防抓取手段，因此应用越来越广。而Hutool中抽象了验证码的实现，也提供了几个简单的验证码实现，从而大大减少服务端开发成本。
由于对验证码需求量巨大，且我之前项目中有所积累，因此在Hutool中加入验证码生成和校验功能。

```
&#x4E86;&#x89E3;Hutool&#x7684;&#x66F4;&#x591A;&#x4FE1;&#x606F;&#x8BF7;&#x8BBF;&#x95EE;&#xFF1A;http://hutool.cn/

<dependency>
    <groupid>com.xiaoleilu</groupid>
    <artifactid>hutool-all</artifactid>
    <version>3.2.3</version>
</dependency>
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

验证码功能位于 `com.hutool.captcha`包中，核心接口为ICaptcha，此接口定义了以下方法：

```
    createCode &#x521B;&#x5EFA;&#x9A8C;&#x8BC1;&#x7801;&#xFF0C;&#x5B9E;&#x73B0;&#x7C7B;&#x9700;&#x540C;&#x65F6;&#x751F;&#x6210;&#x968F;&#x673A;&#x9A8C;&#x8BC1;&#x7801;&#x5B57;&#x7B26;&#x4E32;&#x548C;&#x9A8C;&#x8BC1;&#x7801;&#x56FE;&#x7247;
    getCode &#x83B7;&#x53D6;&#x9A8C;&#x8BC1;&#x7801;&#x7684;&#x6587;&#x5B57;&#x5185;&#x5BB9;
    verify &#x9A8C;&#x8BC1;&#x9A8C;&#x8BC1;&#x7801;&#x662F;&#x5426;&#x6B63;&#x786E;&#xFF0C;&#x5EFA;&#x8BAE;&#x5FFD;&#x7565;&#x5927;&#x5C0F;&#x5199;
    write &#x5C06;&#x9A8C;&#x8BC1;&#x7801;&#x5199;&#x51FA;&#x5230;&#x76EE;&#x6807;&#x6D41;&#x4E2D;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

其中write方法只有一个OutputStream，ICaptcha实现类可以根据这个方法封装写出到文件等方法。
AbstractCaptcha为一个ICaptcha抽象实现类，此类实现了验证码文本生成、非大小写敏感的验证、写出到流和文件等方法，通过继承此抽象类只需实现createImage方法定义图形生成规则即可。

* LineCaptcha 线段干扰的验证码, 生成效果大致如下：

```

LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(<span class="hljs-number">200</span>, <span class="hljs-number">100</span>);

lineCaptcha.write(<span class="hljs-string">"d:/line.png"</span>);

lineCaptcha.verify(<span class="hljs-string">"1234"</span>);
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

* CircleCaptcha 圆圈干扰验证码

```

CircleCaptcha captcha = CaptchaUtil.createCircleCaptcha(<span class="hljs-number">200</span>, <span class="hljs-number">100</span>, <span class="hljs-number">4</span>, <span class="hljs-number">20</span>);

captcha.write(<span class="hljs-string">"d:/circle.png"</span>);

captcha.verify(<span class="hljs-string">"1234"</span>);
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

* ShearCaptcha 扭曲干扰验证码

```

ShearCaptcha captcha = CaptchaUtil.createShearCaptcha(<span class="hljs-number">200</span>, <span class="hljs-number">100</span>, <span class="hljs-number">4</span>, <span class="hljs-number">4</span>);

captcha.write(<span class="hljs-string">"d:/shear.png"</span>);

captcha.verify(<span class="hljs-string">"1234"</span>);
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

* 写出到浏览器（Servlet输出）

```
ICaptcha captcha = ...;
captcha.write(response.getOutputStream());

<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
&#x4F5C;&#x8005;&#xFF1A;&#x8DEF;&#x5C0F;&#x78CA;
&#x539F;&#x6587;&#x94FE;&#x63A5;&#xFF1A;https://my.oschina.net/looly/blog/1591079
&#x6765;&#x6E90;&#xFF1A;oschina
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

=========================================================

## 18. 使用Hutool发送工作日报

```
cron&#x6A21;&#x5757;&#xFF0C;&#x7528;&#x4E8E;&#x5B9A;&#x65F6;&#x53D1;&#x9001;&#x90AE;&#x4EF6;
extra&#x6A21;&#x5757;&#xFF0C;MailUtil&#xFF0C;&#x7528;&#x4E8E;&#x53D1;&#x9001;&#x90AE;&#x4EF6;
poi&#x6A21;&#x5757;&#xFF0C;WordWriter&#xFF0C;&#x7528;&#x4E8E;&#x751F;&#x6210;&#x65E5;&#x62A5;&#x7684;word
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
demo&#x9879;&#x76EE;&#x53EF;&#x4EE5;&#x8BBF;&#x95EE;&#x7801;&#x4E91;&#x5730;&#x5740;&#x83B7;&#x53D6;&#xFF1A;https://gitee.com/loolly_admin/daily-work

src/main/java
    cn/hutool/example/dailyWork/
        DailyWorkGenerator.java ---- &#x65E5;&#x62A5;&#x751F;&#x6210;&#x5668;&#xFF0C;&#x7528;&#x4E8E;&#x751F;&#x6210;Word&#x6587;&#x6863;
        MailSendTask.java       ---- &#x90AE;&#x4EF6;&#x53D1;&#x9001;&#x4EFB;&#x52A1;&#xFF0C;&#x7528;&#x4E8E;&#x53D1;&#x9001;&#x90AE;&#x4EF6;
        DailyWorkMain.java      ---- &#x5B9A;&#x65F6;&#x4EFB;&#x52A1;&#x4E3B;&#x7A0B;&#x5E8F;&#xFF0C;&#x7528;&#x4E8E;&#x542F;&#x52A8;&#x5B9A;&#x65F6;&#x4EFB;&#x52A1;

src/main/resources
    config/
        cron.setting            ---- &#x5B9A;&#x65F6;&#x4EFB;&#x52A1;&#x914D;&#x7F6E;&#x6587;&#x4EF6;
        mail.setting            ---- &#x90AE;&#x7BB1;&#x914D;&#x7F6E;&#x6587;&#x4EF6;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

1.生成日报Word

```

<span class="hljs-keyword">private</span> <span class="hljs-keyword">static</span> <span class="hljs-keyword">final</span> Font TITLE_FONT = <span class="hljs-keyword">new</span> Font(<span class="hljs-string">"&#x9ED1;&#x4F53;"</span>, Font.PLAIN, <span class="hljs-number">22</span>);

<span class="hljs-keyword">private</span> <span class="hljs-keyword">static</span> <span class="hljs-keyword">final</span> Font MAIN_FONT = <span class="hljs-keyword">new</span> Font(<span class="hljs-string">"&#x5B8B;&#x4F53;"</span>, Font.PLAIN, <span class="hljs-number">14</span>);

<span class="hljs-function"><span class="hljs-keyword">public</span> <span class="hljs-keyword">static</span> File <span class="hljs-title">generate</span><span class="hljs-params">()</span> </span>{

	File wordFile = FileUtil.file(StrUtil.format(<span class="hljs-string">"&#x6BCF;&#x65E5;&#x5DE5;&#x4F5C;&#x6C47;&#x62A5;_{}.docx"</span>, DateUtil.today()));
	<span class="hljs-keyword">if</span>(FileUtil.exist(wordFile)) {

		wordFile.delete();
	}

	Word07Writer writer = <span class="hljs-keyword">new</span> Word07Writer(wordFile);
	writer.addText(ParagraphAlignment.CENTER, TITLE_FONT, <span class="hljs-string">"&#x5DE5;&#x4F5C;&#x65E5;&#x62A5;"</span>);
	writer.addText(MAIN_FONT, <span class="hljs-string">""</span>);
	writer.addText(MAIN_FONT, <span class="hljs-string">"&#x5C0A;&#x656C;&#x7684;&#x9886;&#x5BFC;&#xFF1A;"</span>);
	writer.addText(MAIN_FONT, <span class="hljs-string">"    &#x4ECA;&#x5929;&#x6211;&#x5728;Hutool&#x7FA4;&#x91CC;&#x6478;&#x9C7C;&#xFF0C;&#x4EC0;&#x4E48;&#x5DE5;&#x4F5C;&#x4E5F;&#x6CA1;&#x505A;&#x3002;"</span>);

	writer.close();
	<span class="hljs-keyword">return</span> wordFile;
}
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

2.发送邮件在mail.setting中配置发件箱信息

```
# &#x53D1;&#x4EF6;&#x4EBA;&#xFF08;&#x5FC5;&#x987B;&#x6B63;&#x786E;&#xFF0C;&#x5426;&#x5219;&#x53D1;&#x9001;&#x5931;&#x8D25;&#xFF09;
from = Hutool<hutool@yeah.net>
# &#x7528;&#x6237;&#x540D;&#xFF08;&#x6CE8;&#x610F;&#xFF1A;&#x5982;&#x679C;&#x4F7F;&#x7528;foxmail&#x90AE;&#x7BB1;&#xFF0C;&#x6B64;&#x5904;user&#x4E3A;qq&#x53F7;&#xFF09;
user = hutool
# &#x5BC6;&#x7801;
pass = XXXX

#&#x4F7F;&#x7528; STARTTLS&#x5B89;&#x5168;&#x8FDE;&#x63A5;
startttlsEnable = true
#&#x4F7F;&#x7528; SSL&#x5B89;&#x5168;&#x8FDE;&#x63A5;
sslEnable = true

=========================================================================
// &#x4ECA;&#x5929;&#x7684;&#x65E5;&#x671F;&#xFF0C;&#x683C;&#x5F0F;&#x7C7B;&#x4F3C;&#xFF1A;2019-06-20
String today = DateUtil.today();

// &#x751F;&#x6210;&#x6C47;&#x62A5;Word
File dailyWorkDoc = DailyWorkGenerator.generate();
// &#x53D1;&#x9001;&#x90AE;&#x4EF6;
MailUtil.sendHtml("hutool@foxmail.com", StrUtil.format("{} &#x5DE5;&#x4F5C;&#x65E5;&#x62A5;", today), "&#x8BF7;&#x89C1;&#x9644;&#x4EF6;&#x3002;", dailyWorkDoc);

StaticLog.debug("{} &#x5DE5;&#x4F5C;&#x65E5;&#x62A5;&#x5DF2;&#x53D1;&#x9001;&#x7ED9;&#x9886;&#x5BFC;&#xFF01;", today);
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span></hutool@yeah.net>
```

3.定时发送,将刚才的发送邮件作为定时任务加入到配置文件：

```
[cn.hutool.example.dailyWork]
# &#x6BCF;&#x5929;&#x4E0B;&#x5348;6&#x70B9;&#x5B9A;&#x65F6;&#x53D1;&#x9001;
MailSendTask.execute = 00 00 18 * * *
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

4.启动定时任务

```

CronUtil.setMatchSecond(<span class="hljs-keyword">true</span>);

CronUtil.start();
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

5.结果

```
&#x4F5C;&#x8005;&#xFF1A;&#x8DEF;&#x5C0F;&#x78CA;
&#x539F;&#x6587;&#x94FE;&#x63A5;&#xFF1A;https://my.oschina.net/looly/blog/3064203
&#x6765;&#x6E90;&#xFF1A;oschina
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

=========================================================

## 19. Hutool-log模块

```
* Slf4j
    &#x5168;&#x79F0;Simple Logging Facade <span class="hljs-keyword">for</span> JAVA&#xFF0C;&#x771F;&#x6B63;&#x7684;&#x65E5;&#x5FD7;&#x95E8;&#x9762;&#xFF0C;&#x53EA;&#x63D0;&#x4F9B;&#x63A5;&#x53E3;&#x65B9;&#x6CD5;&#xFF0C;&#x5F53;&#x914D;&#x5408;&#x7279;&#x5B9A;&#x7684;&#x65E5;&#x5FD7;&#x5B9E;&#x73B0;&#x65F6;&#xFF0C;
    &#x9700;&#x8981;&#x5F15;&#x5165;&#x76F8;&#x5E94;&#x7684;&#x6865;&#x63A5;&#x5305;

* Common-logging
    Apache&#x63D0;&#x4F9B;&#x7684;&#x4E00;&#x4E2A;&#x901A;&#x7528;&#x7684;&#x65E5;&#x5FD7;&#x63A5;&#x53E3;&#xFF0C;common-logging&#x4F1A;&#x901A;&#x8FC7;&#x52A8;&#x6001;&#x67E5;&#x627E;&#x7684;&#x673A;&#x5236;&#xFF0C;&#x5728;&#x7A0B;&#x5E8F;&#x8FD0;&#x884C;&#x65F6;&#x81EA;&#x52A8;&#x627E;&#x51FA;&#x771F;&#x6B63;
    &#x4F7F;&#x7528;&#x7684;&#x65E5;&#x5FD7;&#x5E93;&#xFF0C;&#x81EA;&#x5DF1;&#x4E5F;&#x81EA;&#x5E26;&#x4E00;&#x4E2A;&#x529F;&#x80FD;&#x5F88;&#x5F31;&#x7684;&#x65E5;&#x5FD7;&#x5B9E;&#x73B0;&#x3002;

* &#x5DEE;&#x522B;&#xFF1A;
    Common-logging &#x52A8;&#x6001;&#x67E5;&#x627E;&#x65E5;&#x5FD7;&#x5B9E;&#x73B0;&#xFF08;&#x7A0B;&#x5E8F;&#x8FD0;&#x884C;&#x65F6;&#x627E;&#x51FA;&#x65E5;&#x5FD7;&#x5B9E;&#x73B0;&#xFF09;&#xFF0C;
    Slf4j &#x5219;&#x662F;&#x9759;&#x6001;&#x7ED1;&#x5B9A;&#xFF08;&#x7F16;&#x8BD1;&#x65F6;&#x627E;&#x5230;&#x5B9E;&#x73B0;&#xFF09;&#xFF0C;&#x52A8;&#x6001;&#x7ED1;&#x5B9A;&#x56E0;&#x4E3A;&#x4F9D;&#x8D56;ClassLoader&#x5BFB;&#x627E;&#x548C;&#x8F7D;&#x5165;&#x65E5;&#x5FD7;&#x5B9E;&#x73B0;&#xFF0C;&#x56E0;&#x6B64;&#x7C7B;&#x4F3C;&#x4E8E;
        OSGI&#x90A3;&#x79CD;&#x4F7F;&#x7528;&#x72EC;&#x7ACB;ClassLoader&#x5C31;&#x4F1A;&#x9020;&#x6210;&#x65E0;&#x6CD5;&#x4F7F;&#x7528;&#x7684;&#x60C5;&#x51B5;&#x3002;

    Slf4j &#x652F;&#x6301;&#x53C2;&#x6570;&#x5316;&#x7684;<span class="hljs-built_in">log</span>&#x5B57;&#x7B26;&#x4E32;&#xFF0C;&#x907F;&#x514D;&#x4E86;&#x4E4B;&#x524D;&#x4E3A;&#x4E86;&#x51CF;&#x5C11;&#x5B57;&#x7B26;&#x4E32;&#x62FC;&#x63A5;&#x7684;&#x6027;&#x80FD;&#x635F;&#x8017;&#x800C;&#x4E0D;&#x5F97;&#x4E0D;&#x5199;&#x7684;
        <span class="hljs-keyword">if</span>(logger.isDebugEnable())&#xFF0C;&#x73B0;&#x5728;&#x53EF;&#x4EE5;&#x76F4;&#x63A5;&#x5199;&#xFF1A;logger.debug(&#x201C;current user is: {}&#x201D;, user)&#x3002;
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

* Log4j
Log4j可能是Java世界里最出名的日志框架了，支持各种目的地各种级别的日志输出。
* LogBack
Log4j作者的又一力作（听说是受不了收费文档搞了个开源的，不需要桥接包完美适配Slf4j），感觉迄今为止最棒的日志框架了，一直都在用，配置文件够简洁，性能足够好。

> JDK Logging 从JDK1.4开始引入，不得不说，你去Google下这个JDK自带的日志组件，并不如Log4j和LogBack之类好用，没有配置文件，日志级别不好理解，想顺心的用估计还得自己封装下，总之大家已经被Log4j惯坏了，JDK的设计并不能被大家认同，唯一的优点想就是不用引入新额jar包。

看了以上介绍，如果你不是混迹（深陷）Java多年的老手，估计会蒙圈儿了吧，那你肯定会问，要门面干嘛。有了手机就有手机壳、手机膜，框架也一样，门面的作用更多的还是三个字：解耦合。说白了，加入一个项目用了一个日志框架，想换咋整啊？那就一行一行的找日志改呗，想想都是噩梦。于是，门面出来了，门面说啦， 你用我的格式写日志，把日志想写哪儿写哪儿，例如Slf4j-api加上后，想换日志框架，直接把桥接包一换就行。方便极了。
说实话，现在Slf4j基本可以是Java日志的一个标准了，按照它写基本可以实现所有日志实现通吃，但是就有人不服，还写了门面的门面（没错，这个人就是我）。

如果看过Netty的源码，推荐你看下io.netty.util.internal.logging这个包里内容，会发现Netty又对日志封装了一层。

```
&#x65E0;&#x8BBA;&#x662F;Netty&#x7684;&#x65E5;&#x5FD7;&#x6A21;&#x5757;&#x8FD8;&#x662F;Hutool-log&#x6A21;&#x5757;&#xFF0C;&#x601D;&#x60F3;&#x7C7B;&#x4F3C;&#x4E8E;Common Logging&#xFF0C;&#x505A;&#x52A8;&#x6001;&#x65E5;&#x5FD7;&#x5B9E;&#x73B0;&#x67E5;&#x627E;&#xFF0C;
&#x7136;&#x540E;&#x627E;&#x5230;&#x76F8;&#x5E94;&#x7684;&#x65E5;&#x5FD7;&#x5B9E;&#x73B0;&#x6765;&#x5199;&#x5165;&#x65E5;&#x5FD7;&#xFF0C;&#x6838;&#x5FC3;&#x4EE3;&#x7801;&#x5982;&#x4E0B;&#xFF1A;

<span class="hljs-keyword">public</span> <span class="hljs-keyword">static</span> Class<? extends AbstractLog> detectLog(){
	List<class<? extends abstractlog>> logClassList = Arrays.asList(
			Slf4jLog.class,
			Log4jLog.class,
			Log4j2Log.class,
			ApacheCommonsLog.class,
			JdkLog.class
	);

	<span class="hljs-keyword">for</span> (Class<? extends AbstractLog> clazz : logClassList) {
		<span class="hljs-keyword">try</span> {
			clazz.getConstructor(Class.class).newInstance(LogFactory.class).info(<span class="hljs-string">"Use Log Framework: [{}]"</span>, clazz.getSimpleName());
			<span class="hljs-keyword">return</span> clazz;
		} <span class="hljs-keyword">catch</span> (Error | Exception e) {
			<span class="hljs-keyword">continue</span>;
		}
	}
	<span class="hljs-keyword">return</span> JdkLog.class;
}
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span></class<?>
```

按顺序实例化相应的日志实现，如果实例化失败（一般是ClassNotFoundException），说明jar不存在，那实例化下一个，通过不停的尝试，最终如果没有引入日志框架，那使用JDK Logging（这个肯定会有的），当然这种方式也和Common-logging存在类似问题，不过不用到跨ClassLoader还是很好用的。

对于JDK Logging，也做了一些适配，使之可以与Slf4j的日志级别做对应，这样就将各个日志框架差异化降到最小。另一方面，如果你看过我的这篇日志，那你一定了解了我的类名自动识别功能，这样大家在复制类名的时候，就不用修改日志的那一行代码了，在所有类中，日志的初始化只有这一句：

```
Log log = LogFactory.get();

<span class="hljs-function"><span class="hljs-keyword">public</span> <span class="hljs-keyword">static</span> Log <span class="hljs-title">get</span><span class="hljs-params">()</span> </span>{
	<span class="hljs-keyword">return</span> getLog(<span class="hljs-keyword">new</span> Exception().getStackTrace()[<span class="hljs-number">1</span>].getClassName());
}

<span class="hljs-keyword">public</span> <span class="hljs-class"><span class="hljs-keyword">interface</span> <span class="hljs-title">Log</span> <span class="hljs-keyword">extends</span> <span class="hljs-title">TraceLog</span>, <span class="hljs-title">DebugLog</span>, <span class="hljs-title">InfoLog</span>, <span class="hljs-title">WarnLog</span>, <span class="hljs-title">ErrorLog</span>

&#x8FD9;&#x6837;&#x5C31;&#x5B9E;&#x73B0;&#x4E86;&#x5355;&#x4E00;&#x4F7F;&#x7528;&#xFF0C;&#x5404;&#x4E2A;&#x65E5;&#x5FD7;&#x6846;&#x67B6;&#x7075;&#x6D3B;&#x5F15;&#x7528;&#x7684;&#x4F5C;&#x7528;&#x4E86;&#x3002;
</span><span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

```
&#x4F5C;&#x8005;&#xFF1A;&#x8DEF;&#x5C0F;&#x78CA;
&#x539F;&#x6587;&#x94FE;&#x63A5;&#xFF1A;https://my.oschina.net/looly/blog/543000
&#x6765;&#x6E90;&#xFF1A;oschina
<span class="copy-code-btn">&#x590D;&#x5236;&#x4EE3;&#x7801;</span>
```

=========================================================

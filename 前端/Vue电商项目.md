# 项目概述

##  电商项目基本业务概述

根据不同场景，电商系统一般都提供了PC端、移动APP、移动Web、微信小程序等多种终端访问方式。

![image-20200311153612952](image\Vue电商项目\image-20200311153612952.png)

## 电商后台管理系统的功能

电商后台管理系统鱼鱼管理用户账号、商品分类、商品信息、订单、数据统计等业务功能。

```mermaid
graph TD
	A[电商后台管理系统]-->B1[用户登陆]
	A-->B2[退出登陆]
	A-->B3[用户管理]
	A-->B4[权限管理]
	A-->B5[商品管理]
	A-->B6[订单管理]
	A-->B7[数据统计]
	B4-->C1[角色列表]
	B4-->C2[权限列表]
	B5-->C3[商品列表]
	B5-->C4[分类管理]
	B5-->C5[参数管理]
	
```

## 开发模式（前后端分离）

![image-20200311162258697](image\Vue电商项目\image-20200311162258697.png)

## 技术选型

1、前端项目技术栈

* Vue
* Vue-router
* Element-UI
* Axios
* Echarts

2、后端项目技术栈

* Node.js

* Express

* Jwt

* Mysql

* Sequelize

  

# 项目初始化

## 前端项目初始化步骤

1. 安装Vue脚手架
2. 通过Vue脚手架创建项目
3. 配置Vue路由
4. 配置Element-UI组件库
5. 配置axios库
6. 初始化git远程仓库
7. 将本地项目脱管到gitee中

## 后台项目的环境安装配置

1. 安装MySQL数据库

2. 安装Node.js环境

3. 配置项目相关信息

4. 启动项目

   - cmd 进入`vue_api_server`文件夹下
   - 先`npm install`
   - 再修改`vue_api_server/config/default.json`里`db_config`的myslq用户名密码
   - `node app.js`启动项目

5. 使用Postman测试后台项目接口是否正常

   - 测试连接http://127.0.0.1:8888/api/private/v1/login  body内容username:admin,password:12345

     ![image-20200311204205956](image\Vue电商项目\image-20200311204205956.png)

# 登陆、退出功能

## 登陆概述

### 登陆流程

1. 在登陆页面输入用户名和密码
2. 调用后台接口进行验证
3. 通过验证后，根据后台的响应状态跳转到项目主页

## 登陆业务的相关技术点

* http是无状态的
* 通过cookie在客户端记录状态
* 通过session在服务器端记录状态
* 通过token方式维持状态

> 如果不存在前后端跨域问题使用cooki和session，如果存在使用token

### 登陆——token原理

![image-20200311205128580](image\Vue电商项目\image-20200311205128580.png)

## 登陆功能实现

### 登陆页面的布局

通过Element-UI组件实现布局

* el-form
* el-form-item
* el-input
* el-button
* 字体图标

![image-20200311205350053](image\Vue电商项目\image-20200311205350053.png)

### 路由挂载守卫控制访问权限

如果用户没有登录，但是直接通过URL访问特定页面，需要重新导航到登陆页面

```js
// 挂载路由导航守卫
router.beforeEach((to, from, next) => {
  // to 将要访问的路径
  // from 代表从哪个路径跳转而来
  // next 是一个函数，表示放行
  if (to.path === '/login') return next()
  // 获取token
  const tokenStr = window.sessionStorage.getItem('token')
  if (!tokenStr) return next('login')
  next()
})
```



# 主页布局

## 整体布局

![image-20200312103852991](image\Vue电商项目\image-20200312103852991.png)

## 左侧菜单布局

![image-20200312202401938](image\Vue电商项目\image-20200312202401938.png)

## 通过接口获取菜单数据

在main.js里，通过axios请求拦截器添加token，保证拥有获取数据的权限。

```js
// axios请求拦截
axios.interceptors.request.use(config => {
  // 为请求头对象，添加Token验证的Authorization字段
  config.headers.Authorization = window.sessionStorage.getItem('token')
  return config
})
```

## 菜单美化

1. 只保留两层结果，先加载数据，用双层v-for绑定数据
2. 修改点击颜色，绑定图标
3. 添加折叠菜单按钮和设置折叠展开样式
4. 侧边栏路由连接改造!!Hn@nsn123



# 用户管理模块

![image-20200314163830534](image\Vue电商项目\image-20200314163830534.png)

# 权限管理模块

## 权限管理业务分析

![image-20200314205940091](image\Vue电商项目\image-20200314205940091.png)

![image-20200314205836832](image\Vue电商项目\image-20200314205836832.png)

# 分类管理模块

## 商品分类概述

商品分类用户购物时，快速找到所要购买的商品，可以通过电商平台主页直观的看到

![image-20200315104622081](image\Vue电商项目\image-20200315104622081.png)

![image-20200315105104859](image\Vue电商项目\image-20200315105104859.png)

# 参数管理模块

商品参数属于显示商品的固定的特征信息，可以通过电商平台商品详情页面直观的看到

![image-20200315144359667](image\Vue电商项目\image-20200315144359667.png)

![image-20200315144509690](image\Vue电商项目\image-20200315144509690.png)

# 商品列表模块

# 订单管理模块

# 数据统计模块

# 项目优化

## 优化策略

1. 生成打包报告
2. 第三方库启用CDN
3. Element-UI组件按需加载
4. 路由依赖加载
5. 首页内容定制

## 生成打包报告

打包时，为了直观的发现项目中存在的问题，可以在打包时生成报告。生成报告的方式有两种：

1. 通过命令行参数的形式生成报

   ```shell
   # 通过vue-cli的命令选项可以生成打包报告
   # --report选项可以生成report.html 以帮助分析包内容
   vue-cli-service build --report
   ```

2. 通过可视化的UI面板直接查看报告（`推荐`)

   在可视化的UI面板中，通过控制台和分析面板，可以方便的看到项目中所存在的问题。

## 通过vue.config.js修改webpack的默认配置

通过vue-cli 工具生成的项目，`默认隐藏了所有webpack的配置项`，目的是为了屏蔽项目的配置过程，让程序员把工作重心放到具体功能和业务逻辑的实现上。

如果要修改webpack默认配置的需求，可以在项目根目录中，按需创建`vue.config.js`这个配置文件，从而对项目的打包发布过程做自定义的配置（具体配置参考：http://cli.vuejs.org/zh/config/#vue-config-js）

```shell
// vue.config.js
module.exports = {
  // 选项...
}
```

## 为开发模式与发布模式指定不同的打包入口

默认情况下，Vue项目的`开发模式`与`发布模式`，共用同一个打包的入口文件（即src/main.js）。为了将项目的开发过程与发布过程分离，可以为两个模式，各自指定打包的入口文件。即：

1. 开发模式的入口文件为`src/main-dev.js`
2. 发布模式的入口文件为`src/main-prod.js`

### chainWebpack打包方式：

1. 把main.js重命名为main-prod.js，复制这个文件命名为main-dev.js
2. 在vue.config.js导出的配置对象中，新增configureWebpack或chainWebpack节点，来自定义webpack的打包配置

具体配置代码如下：

```js
  chainWebpack: config => {
    // eslint-disable-next-line no-unused-expressions
    config.when(process.env.NODE_ENV === 'production', config => {
      config.entry('app').clear().add('./src/main-prod.js')
    })

    config.when(process.env.NODE_ENV === 'development', config => {
      config.entry('app').clear().add('./src/main-dev.js')
    })
  }
```

## 通过externals加载外部CDN资源

默认情况下，通过import语法导入的第三方依赖包，最终会被打包合并到同一个文件中，从而导致打包成功后，单文件体积过大的问题。

为解决上述问题，可以通过webpack的externals节点，来配置并加载外部的CDN资源。凡是声明在externals中的第三方依赖包，都不会被打包。

具体配置代码如下：

​	1、在vue.config.js如下添加代码

```js
    // 发布模式
    config.when(process.env.NODE_ENV === 'production', config => {
      config.entry('app').clear().add('./src/main-prod.js')

      config.set('externals', {
        vue: 'Vue',
        'vue-router': 'VueRouter',
        axios: 'axios',
        echarts: 'echarts',
        nprogress: 'NProgress',
        'vue-quill-editor': 'VueQuillEditor'
      })
    })
```

​	2、删除main-prod.js里引用的css

​	3、在public/index.html文件头部，添加如下CDN资源引用

```html
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link rel="icon" href="<%= BASE_URL %>favicon.ico">
    <title><%= htmlWebpackPlugin.options.title %></title>
        <% if(htmlWebpackPlugin.options.isProd) {%>
          
        <!-- nprogress 的样式表 -->
        <link rel="stylesheet" href="https://cdn.staticfile.org/nprogress/0.2.0/nprogress.min.css">
        <!-- 富文本编辑器 的样式表文件 -->
        <link rel="stylesheet" href="https://cdn.staticfile.org/quill/1.3.4/quill.core.min.css">
        <link rel="stylesheet" href="https://cdn.staticfile.org/quill/1.3.4/quill.snow.min.css">
        <link rel="stylesheet" href="https://cdn.staticfile.org/quill/1.3.4/quill.bubble.min.css">
        <!-- element-ui样式 -->
        <link rel="stylesheet" href="https://cdn.staticfile.org/element-ui/2.8.2/theme-chalk/index.css">
        
        <script src="https://cdn.staticfile.org/vue/2.5.22/vue.min.js"></script>
        <script src="https://cdn.staticfile.org/vue-router/3.0.1/vue-router.min.js"></script>
        <script src="https://cdn.staticfile.org/axios/0.18.0/axios.min.js"></script>
        <script src="https://cdn.staticfile.org/echarts/4.1.0/echarts.min.js"></script>
        <script src="https://cdn.staticfile.org/nprogress/0.2.0/nprogress.min.js"></script>
        <script src="https://cdn.staticfile.org/quill/1.3.4/quill.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/vue-quill-editor@3.0.4/dist/vue-quill-editor.js"></script>
        <script src="https://cdn.staticfile.org/element-ui/2.8.2/index.js"></script>
        
        <%}%>
  </head>
```



## 通过CDN优化element-ui的打包

如果项目调用的element-ui的组件比较多，即使按需加载，体积也会很大。

具体操作流程：

1. 在main-prod.js中，注释掉element-ui按需加载的代码
2. 在index.html的头部区域中，通过CDN加载element-ui的js和css样式

```html
<link rel="stylesheet" href="https://cdn.staticfile.org/element-ui/2.8.2/theme-chalk/index.css">
<script src="https://cdn.staticfile.org/element-ui/2.8.2/index.js"></script>
```

## 首页内容定制

不同的打包环境下，首页内容可能会有所不同。我们可以通过插件的方式进行定制，插件配置如下：

```js
chainWebpack: config => {
    config.when(process.env.NODE_ENV === 'production', config => {
        config.plugin('html').tap(args => {
            args[0].isProd = true
            return args
        })
    })
    config.when(process.env.NODE_ENV === 'development', config => {
        config.plugin('html').tap(args => {
            args[0].isProd = false
            return args
        })
    })
}
```

在public/index.html 首页中，可以根据isProd的值，来决定如何渲染页面结构：

```html
    <!-- 按需渲染页面的标题 -->
    <title><%= htmlWebpackPlugin.options.isProd ? '' : 'dev - '%>电商后台管理系统</title>
    
    <!-- 如果是true,则加载你们的link和script -->
    <% if(htmlWebpackPlugin.options.isProd) {%>
    <link rel="stylesheet" href="https://cdn.staticfile.org/element-ui/2.8.2/theme-chalk/index.css">
    <script src="https://cdn.staticfile.org/element-ui/2.8.2/index.js"></script>
    <%}%>
```

## 路由懒加载

当打包构建项目时,JavaScript包会变得非常大,影响页面加载.如果我们能把不同路由对应的组件分割成不同的代码块,然后当路由被访问的时候才加载对应组件,这样就更加高效了.

具体需要3步:

1. 安装`@babel/plugin-syntax-dynamic-import`开发依赖包
2. 在`babel.config.js`配置文件中声明该插件
3. 将路由改成按需加载的形式，示例代码如下：

```js
// webpackChunkName是分组的名字，会把一组的组件打包在一个js里
const Foo = () => import(/* webpackChunkName: "group-foo" */ './Foo.vue')
const Bar = () => import(/* webpackChunkName: "group-foo" */ './Bar.vue')
const Baz = () => import(/* webpackChunkName: "group-boo" */ './Baz.vue')
```



# 项目上线

## 上线相关配置

1. 通过node创建web服务器

   创建node项目，并按照express，通过express快速创建web服务器，将vue打包生成的dist文件夹，托管为静态资源即可，关键代码如下：

   先创建一个新的项目文件夹，用cmd进入到这个目录

   初始化node：`npm init -y`

   安装express:：`npm i express -S`

   把vue build创建的项目dist文件夹拷过来

   在新建一个app.js文件，写入下面代码

   ```js
   const express = require('express')
   // 创建web服务器
   const app = express()
   
   // 托管静态资源
   app.use(express.static('./dist'))
   
   // 启动web服务器
   app.listen(80, () => {
       console.log('web server running at http://127.0.0.1')
   })
   ```

   

2. 开启gzip配置

   使用gzip可以减小文件体积，是传输速度更快。

   可以通过服务端使用Express做gzip压缩，其配置如下：

   ```js
   // 安装相应包
   npm i compression -S
   
   // 导入包
   const compression = require('compression');
   
   // 启用中间件
   app.use(compression())
   ```

   

3. 配置HTTPS服务

   **为什么要启用HTTPS服务？**

   * 传统的HTTP协议传输的数据都是明文，不安全
   * 采用HTTPS协议对传输的数据进行了加密处理，可以防止数据被中间人窃取，使用更安全

   **申请SSL证书（https//freessl.org）**

   - 进入https://freessl.cn官网，输入要申请的域名并选择品牌
   - 输入自己的邮箱并选择相关选项
   - 验证DNS（在域名管理后台添加TXT记录）
   - 验证通过之后，下载SSL证书（full_chain.pem公钥; private.key私钥）

   **在后台项目中导入证书**

   ```js
   const https = require('https')
   const fs = require('fs')
   const options = {
       cert: fs.readFileSync('./full_chain.pem'),
       key: fs.readFileSync('./private.key')
   }
   https.createServer(options, app).listen(443)
   ```

4. 使用pm2管理应用

   - 在服务器中安装pm2：`npm i pm2 -g`
   - 启动项目：pm2 start 脚本  --name 自定义名称
   - 查看运行项目：pm2 ls
   - 重启项目：pm2 restart 自定义名称
   - 停止项目：pm2 stop 自定义名称
   - 删除项目：pm2 delete 自定义名称

   


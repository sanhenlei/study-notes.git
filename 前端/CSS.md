# CSS 简介

## 什么是 CSS?

- CSS 指层叠样式表 ( **C**ascading **S**tyle **S**heets )
- 样式定义**如何显示** HTML 元素
- 样式通常存储在**样式表**中
- 把样式添加到 HTML 4.0 中，是为了**解决内容与表现分离的问题**
- **外部样式表**可以极大提高工作效率
- 外部样式表通常存储在 **CSS 文件**中
- 多个样式定义可**层叠**为一
- 样式对网页中元素位置的排版进行像素级精确控制

## 多页面应用同一个样式

通常保存在外部的独立的 .CSS 文件（该文件不属于任何页面文件）可以在多个页面中使用同一个 CSS 样式表。通过在任何的页面文件中引用 .CSS 文件，你可以设置具有一致风格的多个页面。

## 样式层叠

样式层叠就是对一个元素多次设置同一个样式，这将使用最后一次设置的属性值。

### 样式层叠次序

当同一个 HTML 元素定义了多个样式时，应该使用哪个样式？

一般而言，所有的样式会根据下面的规则层叠于一个新的虚拟样式表中，其中数字 4 拥有最高的优先权。

1. 浏览器缺省设置
2. 外部样式表
3. 内部样式表（位于 `<head>` 标签内部）
4. 内联样式（在 HTML 元素内部）

因此，内联样式（在 HTML 元素内部）拥有最高的优先权，这意味着它将优先于以下的样式声明： 标签中的样式声明，外部样式表中的样式声明，或者浏览器中的样式声明（缺省值）。

# CSS 语法

## CSS 实例

CSS 规则由两个主要的部分构成：选择器，以及一条或多条声明:

![img](image\CSS\selector.gif)

选择器通常是您需要改变样式的 HTML 元素。

每条声明由一个属性和一个值组成。

属性（property）是您希望设置的样式属性（style attribute）。每个属性有一个值。属性和值被冒号分开。

## CSS 实例

CSS 声明总是以分号 (` ;` ) 结束，声明组以大括号 (`{ }`) 括起来:

```css
p {color:red;text-align:center;}
```

为了让 CSS 可读性更强，你可以每行只描述一个属性.

### CSS 颜色值的写法

在描述颜色的时候，除了使用英文单词 red，我们还可以使用十六进制的颜色值 #ff0000： 

```css
p { color: #ff0000; }

为了节约字节，我们可以使用 CSS 的缩写形式： 
p { color: #f00; }

我们还可以通过两种方法使用 RGB 值：
p { color: rgb(255,0,0); } p { color: rgb(100%,0%,0%); }
```

**提示：**当使用 RGB 百分比时，即使当值为 0 时也要写百分比符号。但是在其他的情况下就不需要这么做了。比如说，当尺寸为 0 像素时，0 之后不需要使用 px 单位。

## CSS 注释

注释是用来解释你的代码，并且可以随意编辑它，浏览器会忽略它。

CSS注释以 "`/*`" 开始, 以 "`*/`" 结束, 实例如下:

```css
/*这是个注释*/        
p        
{       
text-align:center;       
/*这是另一个注释*/     
color:black;     
font-family:arial;       
}
```

# CSS Id 和 Class选择器

如果你要在 HTML 元素中设置 CSS 样式，你需要在元素中设置 "id" 和 "class" 选择器。

------

## id 选择器

id 选择器可以为标有特定 id 的 HTML 元素指定特定的样式。

HTML 元素以 id 属性来设置 id 选择器,CSS 中 id 选择器以 `# `来定义。

以下的样式规则应用于元素属性 id = "para1":

```css
#para1
{ text-align:center;
color:red; }
```

![Remark](image\CSS\lamp.gif) ID 属性不要以数字开头，数字开头的 ID 在 Mozilla/Firefox 浏览器中不起作用。

![Remark](image\CSS\lamp.gif) ID 属性只能在每个 HTML 文档中出现一次。

## class 类选择器

class 选择器用于描述一组元素的样式，class 选择器有别于 id 选择器，class 可以在多个元素中使用。

class 选择器在 HTML 中以 class 属性表示, 在 CSS 中，类选择器以一个点"`.`"号显示：

在以下的例子中，所有拥有 center 类的 HTML 元素均为居中。

```css
.center {text-align:center;}
```

你也可以指定特定的 HTML 元素使用 class。

在以下实例中, 所有的 p 元素使用 class="center" 让该元素的文本居中:

```css
p.center {text-align:center;}
```

![Remark](image\CSS\lamp.gif) 类名的第一个字符不能使用数字！它无法在 Mozilla 或 Firefox 中起作用

## 类选择器 

除了提到的 id 和 class 选择器外，第三种选择器为标签选择器，即以 HTML 标签作为 CSS 修饰所用的选择器。

```html
<style>
h3{color:red;}
</style>
<h3>W3cschool教程</h3>
```

## 内联选择器 

第四种内联选择器即直接在标签内部写 CSS 代码。

```html
<h3 style="color:red;">W3cschool教程</h3>
```

这四种 CS 选择器有修饰上的优先级，即：

> 内联选择器 > id选择器 > 类选择器 > 标签选择器

# CSS 创建

当读到一个样式表时，浏览器会根据它来格式化 [HTML](https://www.w3cschool.cn/html/html-intro.html) 文档。

------

## 如何插入样式表

插入样式表的方法有三种:

- 外部样式表
- 内部样式表
- 内联样式

------

## 外部样式表

当样式需要应用于很多页面时，外部样式表将是理想的选择。在使用外部样式表的情况下，你可以通过改变一个文件来改变整个站点的外观。每个页面使用 标签链接到样式表。 标签在（文档的）头部

```html
<head> <link rel="stylesheet" type="text/css" href="mystyle.css"> </head>
```

浏览器会从文件 mystyle.css 中读到样式声明，并根据它来格式文档。

外部样式表可以在任何文本编辑器中进行编辑。文件不能包含任何的 html 标签。样式表应该以 .CSS 扩展名进行保存。下面是一个样式表文件的例子：

```css
hr {color:sienna;}            
p {margin-left:20px;}            
body {background-image:url(/images/back40.gif);}
```

![Remark](image\CSS\lamp.gif) 不要在属性值与单位之间留有空格。假如你使用 "margin-left: 20 px" 而不是 "margin-left: 20px" ，它仅在 IE 6 中有效，但是在 Mozilla/Firefox 或 Netscape 中却无法正常工作。

## 内部样式表

当单个文档需要特殊的样式时，就应该使用内部样式表。你可以使用 `<style> `标签在文档头部定义内部样式表，就像这样:

```html
<head>
<style>
hr {color:sienna;}
p {margin-left:20px;}
body {background-image:url("images/back40.gif");}
</style>
</head>
```

## 内联样式

由于要将表现和内容混杂在一起，内联样式会损失掉样式表的许多优势。请慎用这种方法，例如当样式仅需要在一个元素上应用一次时。

要使用内联样式，你需要在相关的标签内使用样式（style）属性。Style 属性可以包含任何 CSS 属性。本例展示如何改变段落的颜色和左外边距：

```css
<p style="color:sienna;margin-left:20px">这是一个段落。</p>
```

## 多重样式将层叠为一个

样式表允许以多种方式规定样式信息。样式可以规定在单个的 HTML 元素中，在 HTML 页的头元素中，或在一个外部的 CSS 文件中。甚至可以在同一个 HTML 文档内部引用多个外部样式表。

### 层叠次序

当同一个 HTML 元素被不止一个样式定义时，会使用哪个样式呢？

一般而言，所有的样式会根据下面的规则层叠于一个新的虚拟样式表中，其中数字 4 拥有最高的优先权。

1. 浏览器缺省设置
2. 外部样式表
3. 内部样式表（位于 head 标签内部）
4. 内联样式（在 HTML 元素内部）

因此，内联样式（在 HTML 元素内部）拥有最高的优先权，这意味着它将优先于以下的样式声明： 标签中的样式声明，外部样式表中的样式声明，或者浏览器中的样式声明（缺省值）。

![Remark](image\CSS\lamp.gif) **提示:**如果你使用了外部文件的样式在内部样式中也定义了该样式，则内部样式表会取代外部文件的样式。

## 多重样式优先级深入理解

优先级用于浏览器是通过判断哪些属性值与元素最相关以决定并应用到该元素上的。

优先级仅由选择器组成的匹配规则决定的。

优先级就是分配给指定的CSS声明的一个权重，它由匹配的选择器中的每一种选择器类型的数值决定。

### 多重样式优先级顺序

下列是一份优先级逐级增加的选择器列表，其中数字 7 拥有最高的优先权：

1. 通用选择器（*）
2. 元素(类型)选择器
3. 类选择器
4. 属性选择器
5. 伪类
6. ID 选择器
7. 内联样式

### !important 规则例外

当 !important 规则被应用在一个样式声明中时，该样式声明会覆盖 CSS 中任何其他的声明，无论它处在声明列表中的哪里。尽管如此，!important 规则还是与优先级毫无关系。使用 !important 不是一个好习惯，因为它改变了你样式表本来的级联规则，从而使其难以调试。

一些经验法则：

- Always 要优化考虑使用样式规则的优先级来解决问题而不是 !important
- Only 只在需要覆盖全站或外部 css（例如引用的 ExtJs 或者 YUI ）的特定页面中使用 !important
- Never 永远不要在全站范围的 CSS 上使用 !important
- Never 永远不要在你的插件中使用 !important

### 权重计算:

![201712181559548677](image\CSS\1513584051697589.png)

以下是对于上图的解释：

- 内联样式表的权值最高 1000
- ID 选择器的权值为 100
- Class 类选择器的权值为 10
- HTML 标签选择器的权值为 1

```html
<!DOCTYPE html>
<html>

<head>
    <style type="text/css">
        #redP p {
            /* 权值 = 100+1=101 */
            color:#F00;  /* 红色 */
        }
        #redP .red em {
            /* 权值 = 100+10+1=111 */
            color:#00F; /* 蓝色 */
        }
        #redP p span em {
            /* 权值 = 100+1+1+1=103 */
            color:#FF0;/*黄色*/
        }
    </style>
</head>

<body>
<div id="redP">
    <p class="red">red
        <span><em>em red</em></span>
    </p>
    <p>red</p>
</div>
</body>
</html>
```

### CSS 优先级法则：

-  A 选择器都有一个权值，权值越大越优先；
-  B 当权值相等时，后出现的样式表设置要优于先出现的样式表设置；
-  C 创作者的规则高于浏览者：即网页编写者设置的 CSS 样式的优先权高于浏览器所设置的样式；
-  D 继承的 CSS 样式不如后来指定的 CSS 样式；
-  E 在同一组属性设置中标有 "!important" 规则的优先级最大；

# CSS Backgrounds (背景)

## CSS 背景



CSS 背景属性用于定义 HTML 元素的背景。

CSS 属性定义背景效果：

- `background-color`
- `background-image`
- `background-repeat`
- `background-attachment`
- `background-position`

## 背景颜色

`background-color` 属性定义了元素的背景颜色。

页面的背景颜色使用在` body` 的选择器中:

```css
body {background-color:#b0c4de;}
```

CSS 中，颜色值通常以以下方式定义:

- 十六进制 - 如："#ff0000"
- RGB - 如："rgb(255,0,0)"
- 颜色名称 - 如："red"

以下实例中, `h1`, `p`, 和 `div`元素拥有不同的背景颜色:

```CSS
h1 {background-color:#6495ed;}
p {background-color:#e0ffff;}
div {background-color:#b0c4de;}
```

**提示：**你可以为所有元素设置背景色，包括` body` 一直到` em` 和 `a` 等行内元素。

**提示：**`background-color` 不能继承，其默认值是` transparent`。如果一个元素没有指定背景色，那么背景就是透明的，这样其父元素的背景才可见。

## 背景图像

`background-image` 属性描述了元素的背景图像.

默认情况下，背景图像进行平铺重复显示，以覆盖整个元素实体。

页面背景图片设置实例:

```CSS
body {background-image:url('paper.gif');}
```

## 背景图像 - 水平或垂直平铺

如果需要在 HTML 页面上对背景图像进行平铺，可以使用 [`background-repeat`](https://www.w3cschool.cn/cssref/pr-background-repeat.html) 属性。

默认情况下 `background-image` 属性会在页面的水平或者垂直方向平铺。

如果图像只在水平方向平铺` (repeat-x) `, 页面背景会更好些:

```CSS
body
{background-image:url('gradient2.png');
background-repeat:repeat-x;}
```

## 背景图像- 设置定位与不平铺

![Remark](image\CSS\lamp.gif) 让背景图像不影响文本的排版

如果你不想让图像平铺，你可以使用 `background-repeat` 属性:

```CSS
body
{background-image:url('img_tree.png');
background-repeat:no-repeat;}
```

以上实例中，背景图像与文本显示在同一个位置，为了让页面排版更加合理，不影响文本的阅读，我们可以改变图像的位置。

可以利用 `background-position` 属性改变图像在背景中的位置:

```CSS
body
{background-image:url('img_tree.png');
background-repeat:no-repeat;
background-position:right top
```

**提示：**为 `background-position` 属性提供值有很多方法。首先，可以使用一些关键字：`top`、`bottom`、`left`、`right` 和 `center`；其次，可以使用长度值，如 100px 或 5cm；最后也可以使用百分数值。不同类型的值对于背景图像的放置稍有差异。

### 关键字

图像放置关键字最容易理解的作用就像其名字的意义。例如，`top` `left` 使图像放置在元素内边距区的左上角。

只要保证不超过两个关键字：一个对应水平方向，另一个对应垂直方向，那么你可以设置位置关键字以任何顺序出现。

如果只有一个关键字，则会默认另一个关键字为 `center`。

所以，如果希望每个段落的中部上方出现一个图像，只需声明如下：

```CSS
p        
{background-image:url('img_tree.png');        
background-repeat:no-repeat;            
background-position:top;}
```

# CSS Text(文本)

## CSS Text 文本格式

通过 CSS 的 Text 属性，你可以改变页面中文本的颜色、字符间距、对齐文本、装饰文本、对文本进行缩进等等，你可以观察下述的一段简单的应用了 CSS 文本格式的段落。

## Text Color

颜色属性被用来设置文字的颜色。

颜色是通过CSS最经常的指定：

- 十六进制值 - 如"＃FF0000"
- 一个RGB值 - "RGB（255,0,0）"
- 颜色的名称 - 如"红"

一个网页的背景颜色是指在主体内的选择：

```css
body {color:blue;}
h1 {color:#00ff00;}
h2 {color:rgb(255,0,0);}
```

![Remark](image\CSS\lamp.gif) 对于 W3C 标准的 CSS：如果你定义了颜色属性，你还必须定义背景色属性。

## 文本的对齐方式

文本排列属性是用来设置文本的水平对齐方式。

文本可居中或对齐到左或右,两端对齐.

当 `text-align `设置为`justify`，每一行被展开为宽度相等，左，右外边距是对齐（如杂志和报纸）。

```css
h1 {text-align:center;}
p.date {text-align:right;}
p.main {text-align:justify;}
```

**提示：**如果想把一个行内元素的第一行“缩进”，可以用左内边距或外边距创造这种效果。

## 文本修饰

`text-decoration`属性用来设置或删除文本的装饰。

从设计的角度看 `text-decoration` 属性主要是用来删除链接的下划线：

```css
a {text-decoration:none;}
```

也可以这样装饰文字：

``` css
h1 {text-decoration:overline;}
h2 {text-decoration:line-through;}
h3 {text-decoration:underline;}
```

![Remark](image\CSS\lamp.gif)我们不建议强调指出不是链接的文本，因为这常常混淆用户。

## 文本转换

文本转换属性是用来指定在一个文本中的大写和小写字母。

可用于所有字句变成大写或小写字母，或每个单词的首字母大写。

```css
p.uppercase {text-transform:uppercase;}
p.lowercase {text-transform:lowercase;}
p.capitalize {text-transform:capitalize;}
```

## 文本缩进

文本缩进属性是用来指定文本的第一行的缩进。

CSS 提供了 `text-indent` 属性，该属性可以方便地实现文本缩进。

通过使用 `text-indent` 属性，所有元素的第一行都可以缩进一个给定的长度。

```css
p {text-indent:50px;}
```

## 文本间隔

`word-spacing` 属性可以改变字（单词）之间的标准间隔。其默认值 `normal `与设置值为 0 是一样的。

```css
p {word-spacing:30px;}
```

## 所有CSS文本属性

| 属性                                                         | 描述                     |
| :----------------------------------------------------------- | :----------------------- |
| [color](https://www.w3cschool.cn/cssref/pr-text-color.html)  | 设置文本颜色             |
| [direction](https://www.w3cschool.cn/cssref/pr-text-direction.html) | 设置文本方向。           |
| [letter-spacing](https://www.w3cschool.cn/cssref/pr-text-letter-spacing.html) | 设置字符间距             |
| [line-height](https://www.w3cschool.cn/cssref/pr-dim-line-height.html) | 设置行高                 |
| [text-align](https://www.w3cschool.cn/cssref/pr-text-text-align.html) | 对齐元素中的文本         |
| [text-decoration](https://www.w3cschool.cn/cssref/pr-text-text-decoration.html) | 向文本添加修饰           |
| [text-indent](https://www.w3cschool.cn/cssref/pr-text-text-indent.html) | 缩进元素中文本的首行     |
| [text-shadow](https://www.w3cschool.cn/cssref/css3-pr-text-shadow.html) | 设置文本阴影             |
| [text-transform](https://www.w3cschool.cn/cssref/pr-text-text-transform.html) | 控制元素中的字母         |
| [unicode-bidi](https://www.w3cschool.cn/cssref/pr-text-unicode-bidi.html) | 设置或返回文本是否被重写 |
| [vertical-align](https://www.w3cschool.cn/cssref/pr-pos-vertical-align.html) | 设置元素的垂直对齐       |
| [white-space](https://www.w3cschool.cn/cssref/pr-text-white-space.html) | 设置元素中空白的处理方式 |
| [word-spacing](https://www.w3cschool.cn/cssref/pr-text-word-spacing.html) | 设置字间距               |



# CSS3选择器归类整理（附CSS优先级要点）

CSS是用于网页设计可用的最强大的工具之一。使用它我们可以在几分钟内改变一个网站的界面，而不用改变页面的标签。在深入研究CSS选择器之前，我们应该先搞懂CSS优先级是如何工作的。比如给一个p标签增加一个类（class），可是执行后该class中的有些属性并没有起作用。通过Firebug查看，发现没有起作用的属性被覆盖了。这个时候CSS选择器优先级的问题了。



当我们写CSS的时候需要注意有些选择器在级联(cascade)上会高于其它选择器，我们写在最后面的选择器将不一定会覆盖前面我们写在同一个元素的样式。那么你如何计算指定选择器的优先级？如果你考虑到将优先级表示为用逗号隔开的四个数字就会相当简单，比如：1, 1, 1, 1 或0, 2, 0, 1



第一个数字(a)通常就是0，除非在标签上使用style属性；
第二个数字(b)是该选择器上的id的数量的总和；
第三个数字(c)是用在该选择器上的其它属性选择器和伪类的总和。这里包括class (.example) 和属性选择器(比如 li[id=red])；
第四个数字(d)计算元素(就像table、p、div等等)和伪元素(就像:first-line等)；
通用选择器(*)是0优先级；
如果两个选择器有同样的优先级，在样式表中后面的那个起作用。
让我们看几个例子，这样或许比较容易理解些：

\#sidebar h2 — 0, 1, 0, 1
h2.title — 0, 0, 1, 1
h2 + p — 0, 0, 0, 2
\#sidebar p:first-line — 0, 1, 0, 2
在下面的例子中，第一个将会起作用，因为它比第二个优先级高：

\#sidebar p#first { color: red; } — 0, 2, 0, 1
\#sidebar p:first-line { color: blue; } — 0, 1, 0, 2
至少基本理解优先级是如何工作的是很重要的，但是一些工具比如Firebug，在我们审查指定元素的时候，按照选择器的优先级列出所有的css选择器对让我们知道在指定元素上哪个选择器是有效的是很有用的，让你非常容易的看到那个选择器作用于一个元素上了。



![css选择器](image\CSS\201611221051344550.png)



## CSS3 选择器分类

CSS3选择器分类如下图所示：

![选择器分类](image\CSS\201611221054464406.png)



## 选择器的语法

1，基本选择器语法

| **选择器**          | **类型**   | **功能描述**                                         |
| ------------------- | ---------- | ---------------------------------------------------- |
| *                   | 通配选择器 | 选择文档中所有HTML元素                               |
| E                   | 元素选择器 | 选择指定类型的HTML元素                               |
| #id                 | ID选择器   | 选择指定ID属性值为“id”的任意类型元素                 |
| .class              | 类选择器   | 选择指定class属性值为“class”的任意类型的任意多个元素 |
| selector1,selectorN | 群组选择器 | 将每一个选择器匹配的元素集合并                       |

 

2，层次选择器语法

| **选择器** | **类型**                 | **功能描述**                                          |
| ---------- | ------------------------ | ----------------------------------------------------- |
| E  F       | 后代选择器（包含选择器） | 选择匹配的F元素，且匹配的F元素被包含在匹配的E元素内   |
| E>F        | 子选择器                 | 选择匹配的F元素，且匹配的F元素所匹配的E元素的子元素   |
| E+F        | 相邻兄弟选择器           | 选择匹配的F元素，且匹配的F元素紧位于匹配的E元素的后面 |
| E~F        | 通用选择器               | 选择匹配的F元素，且位于匹配的E元素后的所有匹配的F元素 |

 

3，动态伪类选择器语法

| **选择器** | **类型**       | **功能描述**                                                 |
| ---------- | -------------- | ------------------------------------------------------------ |
| E:link     | 链接伪类选择器 | 选择匹配的E元素，而且匹配元素被定义了超链接并未被访问过。常用于链接描点上 |
| E:visited  | 链接伪类选择器 | 选择匹配的E元素，而且匹配元素被定义了超链接并已被访问过。常用于链接描点上 |
| E:active   | 用户行为选择器 | 选择匹配的E元素，且匹配元素被激活。常用于链接描点和按钮上    |
| E:hover    | 用户行为选择器 | 选择匹配的E元素，且用户鼠标停留在元素E上。IE6及以下浏览器仅支持a:hover |
| E:focus    | 用户行为选择器 | 选择匹配的E元素，而且匹配元素获取焦点                        |

 **注意：** 在CSS定义中，a:hover 必须被置于 a:link 和 a:visited 之后，才是有效的。

**注意：** 在 CSS 定义中，a:active 必须被置于 a:hover 之后，才是有效的。

**注意：**伪类的名称不区分大小写。



4，目标伪类选择器

| **选择器** | **功能描述**                                 |
| ---------- | -------------------------------------------- |
| E:target   | 选择匹配E的所有元素，且匹配元素被相关URL指向 |

 

5，UI元素状态伪类选择器语法

| **选择器** | **类型**             | **功能描述**                           |
| ---------- | -------------------- | -------------------------------------- |
| E:checked  | 选中状态伪类选择器   | 匹配选中的复选按钮或者单选按钮表单元素 |
| E:enabled  | 启用状态伪类选择器   | 匹配所有启用的表单元素                 |
| E:disabled | 不可用状态伪类选择器 | 匹配所有禁用的表单元素                 |



 6，结构伪类选择器使用语法

| **选择器**            | **功能描述**                                                 |
| --------------------- | ------------------------------------------------------------ |
| E:first-child         | 作为父元素的第一个子元素的元素E。与E:nth-child(1)等同        |
| E:last-child          | 作为父元素的最后一个子元素的元素E。与E:nth-last-child(1)等同 |
| E:root                | 选择匹配元素E所在文档的根元素。在HTML文档中，根元素始终是html，此时该选择器与html类型选择器匹配的内容相同 |
| E F:nth-child(n)      | 选择父元素E的第n个子元素F。其中n可以是整数（1，2，3）、关键字（even，odd）、可以是公式（2n+1）,而且n值起始值为1，而不是0. |
| E F:nth-last-child(n) | 选择父元素E的倒数第n个子元素F。此选择器与E:nth-child(n)选择器计算顺序刚好相反，但使用方法都是一样的，其中：nth-last-child(1)始终匹配最后一个元素，与last-child等同 |
| E:nth-of-type(n)      | 选择父元素内具有指定类型的第n个E元素                         |
| E:nth-last-of-type(n) | 选择父元素内具有指定类型的倒数第n个E元素                     |
| E:first-of-type       | 选择父元素内具有指定类型的第一个E元素，与E:nth-of-type(1)等同 |
| E:last-of-type        | 选择父元素内具有指定类型的最后一个E元素，与E:nth-last-of-type(1)等同 |
| E:only-child          | 选择父元素只包含一个子元素，且该子元素匹配E元素              |
| E:only-of-type        | 选择父元素只包含一个同类型子元素，且该子元素匹配E元素        |
| E:empty               | 选择没有子元素的元素，而且该元素也不包含任何文本节点         |

注：（1），“ul>li:nth-child(3)”表达的并不是一定选择列表ul元素中的第3个子元素li，仅有列表ul中第3个li元素前不存在其他的元素，命题才有意义，否则不会改变列表第3个li元素的样式。

（2），:nth-child(n)  中参数只能是n，不可以用其他字母代替。

（3），:nth-child(odd) 选择的是奇数项，而使用:nth-last-child(odd) 选择的却是偶数项



7，否定伪类选择器

| **选择器** | **功能描述**             |
| ---------- | ------------------------ |
| E:not(F)   | 匹配所有除元素F外的E元素 |

8，伪元素

| 选择器                                                       | 示例           | 示例说明                                        |
| :----------------------------------------------------------- | :------------- | :---------------------------------------------- |
| [:link](https://www.runoob.com/cssref/sel-link.html)         | a:link         | 选择所有未访问链接                              |
| [:visited](https://www.runoob.com/cssref/sel-visited.html)   | a:visited      | 选择所有访问过的链接                            |
| [:active](https://www.runoob.com/cssref/sel-active.html)     | a:active       | 选择正在活动链接                                |
| [:hover](https://www.runoob.com/cssref/sel-hover.html)       | a:hover        | 把鼠标放在链接上的状态                          |
| [:focus](https://www.runoob.com/cssref/sel-focus.html)       | input:focus    | 选择元素输入后具有焦点                          |
| [:first-letter](https://www.runoob.com/cssref/sel-firstletter.html) | p:first-letter | 选择每个<p> 元素的第一个字母                    |
| [:first-line](https://www.runoob.com/cssref/sel-firstline.html) | p:first-line   | 选择每个<p> 元素的第一行                        |
| [:first-child](https://www.runoob.com/cssref/sel-firstchild.html) | p:first-child  | 选择器匹配属于任意元素的第一个子元素的 <p> 元素 |
| [:before](https://www.runoob.com/cssref/sel-before.html)     | p:before       | 在每个<p>元素之前插入内容                       |
| [:after](https://www.runoob.com/cssref/sel-after.html)       | p:after        | 在每个<p>元素之后插入内容                       |
| [:lang(*language*)](https://www.runoob.com/cssref/sel-lang.html) | p:lang(it)     | 为<p>元素的lang属性选择一个开始值               |



9，属性选择器语法

| **选择器**              | **功能描述**                                                 |
| ----------------------- | ------------------------------------------------------------ |
| [*attribute*]           | 用于选取带有指定属性的元素。                                 |
| [*attribute*=*value*]   | 用于选取带有指定属性和值的元素。                             |
| [*attribute*~=*value*]  | 用于选取属性值中包含指定词汇的元素。                         |
| [*attribute*\|=*value*] | 用于选取带有以指定值开头的属性值的元素，该值必须是整个单词。 |
| [*attribute*^=*value*]  | 匹配属性值以指定值开头的每个元素。                           |
| [*attribute*$=*value*]  | 匹配属性值以指定值结尾的每个元素。                           |
| [*attribute**=*value*]  | 匹配属性值中包含指定值的每个元素。                           |

注：例<div class="links item"></div>其中a[class="links"]{……} 是找不到匹配元素，只有a[class="links item"]{……}才匹配



# CSS3 渐变

CSS3 渐变（gradients）可以让你在两个或多个指定的颜色之间显示平稳的过渡。

以前，你必须使用图像来实现这些效果。但是，通过使用 CSS3 渐变（gradients），你可以减少下载的事件和宽带的使用。此外，渐变效果的元素在放大时看起来效果更好，因为渐变（gradient）是由浏览器生成的。

CSS3 定义了两种类型的渐变（gradients）：

- **线性渐变（Linear Gradients）- 向下/向上/向左/向右/对角方向**
- **径向渐变（Radial Gradients）- 由它们的中心定义**

## CSS3 线性渐变

为了创建一个线性渐变，你必须至少定义两种颜色结点。颜色结点即你想要呈现平稳过渡的颜色。同时，你也可以设置一个起点和一个方向（或一个角度）。

![线性渐变](image\CSS\1456318715707086.png)

### 语法

> background: linear-gradient(direction, color-stop1, color-stop2, ...);                            

**线性渐变 - 从上到下（默认情况下）**

下面的实例演示了从顶部开始的线性渐变。起点是红色，慢慢过渡到蓝色：

```css
#grad {
  background: -webkit-linear-gradient(red, blue); /* Safari 5.1 - 6.0 */
  background: -o-linear-gradient(red, blue); /* Opera 11.1 - 12.0 */
  background: -moz-linear-gradient(red, blue); /* Firefox 3.6 - 15 */
  background: linear-gradient(red, blue); /* 标准的语法 */
}
```

**线性渐变 - 从左到右**

下面的实例演示了从左边开始的线性渐变。起点是红色，慢慢过渡到蓝色：

```css
#grad {
  background: -webkit-linear-gradient(left, red , blue); /* Safari 5.1 - 6.0 */
  background: -o-linear-gradient(right, red, blue); /* Opera 11.1 - 12.0 */
  background: -moz-linear-gradient(right, red, blue); /* Firefox 3.6 - 15 */
  background: linear-gradient(to right, red , blue); /* 标准的语法 */
}
```

**线性渐变 - 对角**

你可以通过指定水平和垂直的起始位置来制作一个对角渐变。

下面的实例演示了从左上角开始（到右下角）的线性渐变。起点是红色，慢慢过渡到蓝色：

```css
#grad {
  background: -webkit-linear-gradient(left top, red , blue); /* Safari 5.1 - 6.0 */
  background: -o-linear-gradient(bottom right, red, blue); /* Opera 11.1 - 12.0 */
  background: -moz-linear-gradient(bottom right, red, blue); /* Firefox 3.6 - 15 */
  background: linear-gradient(to bottom right, red , blue); /* 标准的语法 */
}
```

## 使用角度

如果你想要在渐变的方向上做更多的控制，你可以定义一个角度，而不用预定义方向（to bottom、to top、to right、to left、to bottom right，等等）。

### 语法

>  background: linear-gradient(angle, color-stop1, color-stop2);                                

角度是指水平线和渐变线之间的角度，逆时针方向计算。换句话说，0deg 将创建一个从下到上的渐变，90deg 将创建一个从左到右的渐变。

<img src="image\CSS\201611251408471469.jpg" alt="img" style="zoom:50%;" /> 

但是，请注意很多浏览器(Chrome,Safari,fiefox等)的使用了旧的标准，即 0deg 将创建一个从下到上的渐变，90deg 将创建一个从左到右的渐变。换算公式 **90 - x = y** 其中 x 为标准角度，y为非标准角度。

下面的实例演示了如何在线性渐变上使用角度：

```css
#grad {
  background: -webkit-linear-gradient(180deg, red, blue); /* Safari 5.1 - 6.0 */
  background: -o-linear-gradient(180deg, red, blue); /* Opera 11.1 - 12.0 */
  background: -moz-linear-gradient(180deg, red, blue); /* Firefox 3.6 - 15 */
  background: linear-gradient(180deg, red, blue); /* 标准的语法 */
}
```

## 使用多个颜色结点

下面的实例演示了如何设置多个颜色结点：

```css
#grad {
  background: -webkit-linear-gradient(red, green, blue); /* Safari 5.1 - 6.0 */
  background: -o-linear-gradient(red, green, blue); /* Opera 11.1 - 12.0 */
  background: -moz-linear-gradient(red, green, blue); /* Firefox 3.6 - 15 */
  background: linear-gradient(red, green, blue); /* 标准的语法 */
}
```

## 使用透明度（Transparency）

CSS3 渐变也支持透明度（transparency），可用于创建减弱变淡的效果。

为了添加透明度，我们使用 rgba() 函数来定义颜色结点。rgba() 函数中的最后一个参数可以是从 0 到 1 的值，它定义了颜色的透明度：0 表示完全透明，1 表示完全不透明。

下面的实例演示了从左边开始的线性渐变。起点是完全透明，慢慢过渡到完全不透明的红色：

```css
#grad {
  background: -webkit-linear-gradient(left,rgba(255,0,0,0),rgba(255,0,0,1)); /* Safari 5.1 - 6 */
  background: -o-linear-gradient(right,rgba(255,0,0,0),rgba(255,0,0,1)); /* Opera 11.1 - 12*/
  background: -moz-linear-gradient(right,rgba(255,0,0,0),rgba(255,0,0,1)); /* Firefox 3.6 - 15*/
  background: linear-gradient(to right, rgba(255,0,0,0), rgba(255,0,0,1)); /* 标准的语法 */
}
```

## 重复的线性渐变

repeating-linear-gradient() 函数用于重复线性渐变：

```css
#grad {
  /* Safari 5.1 - 6.0 */
  background: -webkit-repeating-linear-gradient(red, yellow 10%, green 20%);
  /* Opera 11.1 - 12.0 */
  background: -o-repeating-linear-gradient(red, yellow 10%, green 20%);
  /* Firefox 3.6 - 15 */
  background: -moz-repeating-linear-gradient(red, yellow 10%, green 20%);
  /* 标准的语法 */
  background: repeating-linear-gradient(red, yellow 10%, green 20%);
}
```

## CSS3 径向渐变

径向渐变由它的中心定义。

为了创建一个径向渐变，你也必须至少定义两种颜色结点。颜色结点即你想要呈现平稳过渡的颜色。同时，你也可以指定渐变的中心、形状（圆形或椭圆形）、大小。默认情况下，渐变的中心是 center（表示在中心点），渐变的形状是 ellipse（表示椭圆形），渐变的大小是 farthest-corner（表示到最远的角落）。

**径向渐变的实例：**

![Radial gradient](image\CSS\1456318715769924.jpg)

### 语法

>  background: radial-gradient(center, shape size, start-color, ..., last-color);                                

**径向渐变 - 颜色结点均匀分布（默认情况下）**

```css
#grad {
  background: -webkit-radial-gradient(red, green, blue); /* Safari 5.1 - 6.0 */
  background: -o-radial-gradient(red, green, blue); /* Opera 11.6 - 12.0 */
  background: -moz-radial-gradient(red, green, blue); /* Firefox 3.6 - 15 */
  background: radial-gradient(red, green, blue); /* 标准的语法 */
}
```

**径向渐变 - 颜色结点不均匀分布**

```css
#grad {
  background: -webkit-radial-gradient(red 5%, green 15%, blue 60%); /* Safari 5.1 - 6.0 */
  background: -o-radial-gradient(red 5%, green 15%, blue 60%); /* Opera 11.6 - 12.0 */
  background: -moz-radial-gradient(red 5%, green 15%, blue 60%); /* Firefox 3.6 - 15 */
  background: radial-gradient(red 5%, green 15%, blue 60%); /* 标准的语法 */
}

```

## 设置形状

shape 参数定义了形状。它可以是值 circle 或 ellipse。其中，circle 表示圆形，ellipse 表示椭圆形。默认值是 ellipse。

```css
#grad {
  background: -webkit-radial-gradient(circle, red, yellow, green); /* Safari 5.1 - 6.0 */
  background: -o-radial-gradient(circle, red, yellow, green); /* Opera 11.6 - 12.0 */
  background: -moz-radial-gradient(circle, red, yellow, green); /* Firefox 3.6 - 15 */
  background: radial-gradient(circle, red, yellow, green); /* 标准的语法 */
}
```

## 不同尺寸大小关键字的使用

size 参数定义了渐变的大小。它可以是以下四个值：

- **closest-side**
- **farthest-side**
- **closest-corner**
- **farthest-corner**

```html
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"> 
<title>W3Cschool教程(w3cschool.cn)</title> 
<style>
#grad1 {
    height: 150px;
    width: 150px;
    background: -webkit-radial-gradient(60% 55%, closest-side,blue,green,yellow,black); /* Safari 5.1 - 6.0 */
    background: -o-radial-gradient(60% 55%, closest-side,blue,green,yellow,black); /* Opera 11.6 - 12.0 */
    background: -moz-radial-gradient(60% 55%, closest-side,blue,green,yellow,black); /* Firefox 3.6 - 15 */
    background: radial-gradient(60% 55%, closest-side,blue,green,yellow,black); /* 标准的语法（必须放在最后） */
}

#grad2 {
    height: 150px;
    width: 150px;
    background: -webkit-radial-gradient(60% 55%, farthest-side,blue,green,yellow,black); /* Safari 5.1 - 6.0 */
    background: -o-radial-gradient(60% 55%, farthest-side,blue,green,yellow,black); /* Opera 11.6 - 12.0 */
    background: -moz-radial-gradient(60% 55%, farthest-side,blue,green,yellow,black); /* Firefox 3.6 - 15 */
    background: radial-gradient(60% 55%, farthest-side,blue,green,yellow,black); /* 标准的语法（必须放在最后） */
}

#grad3 {
    height: 150px;
    width: 150px;
    background: -webkit-radial-gradient(60% 55%, closest-corner,blue,green,yellow,black); /* Safari 5.1 - 6.0 */
    background: -o-radial-gradient(60% 55%, closest-corner,blue,green,yellow,black); /* Opera 11.6 - 12.0 */
    background: -moz-radial-gradient(60% 55%, closest-corner,blue,green,yellow,black); /* Firefox 3.6 - 15 */
    background: radial-gradient(60% 55%, closest-corner,blue,green,yellow,black); /* 标准的语法（必须放在最后） */
}

#grad4 {
    height: 150px;
    width: 150px;
    background: -webkit-radial-gradient(60% 55%, farthest-corner,blue,green,yellow,black); /* Safari 5.1 - 6.0 */
    background: -o-radial-gradient(60% 55%, farthest-corner,blue,green,yellow,black); /* Opera 11.6 - 12.0 */
    background: -moz-radial-gradient(60% 55%, farthest-corner,blue,green,yellow,black); /* Firefox 3.6 - 15 */
    background: radial-gradient(60% 55%, farthest-corner,blue,green,yellow,black); /* 标准的语法（必须放在最后） */
}
</style>
</head>
<body>

<h3>径向渐变 - 不同尺寸大小关键字的使用</h3>

<p><strong>closest-side：</strong></p>
<div id="grad1"></div>

<p><strong>farthest-side：</strong></p>
<div id="grad2"></div>

<p><strong>closest-corner：</strong></p>
<div id="grad3"></div>

<p><strong>farthest-corner（默认）：</strong></p>
<div id="grad4"></div>

<p><strong>注意：</strong> Internet Explorer 9 及之前的版本不支持渐变。</p>

</body>
</html>
```

## 重复的径向渐变

repeating-radial-gradient() 函数用于重复径向渐变：

```css
#grad {
  /* Safari 5.1 - 6.0 */
  background: -webkit-repeating-radial-gradient(red, yellow 10%, green 15%);
  /* Opera 11.6 - 12.0 */
  background: -o-repeating-radial-gradient(red, yellow 10%, green 15%);
  /* Firefox 3.6 - 15 */
  background: -moz-repeating-radial-gradient(red, yellow 10%, green 15%);
  /* 标准的语法 */
  background: repeating-radial-gradient(red, yellow 10%, green 15%);
}
```



# CSS3 过渡

## CSS3 过渡

CSS3中，我们为了添加某种效果可以从一种样式转变到另一个的时候，无需使用Flash动画或JavaScript。用鼠标移过下面的元素：

## 览器支持

表格中的数字表示支持该属性的第一个浏览器版本号。

紧跟在 -webkit-, -ms- 或 -moz- 前的数字为支持该前缀属性的第一个浏览器版本号。

## 它是如何工作？

CSS3 过渡是元素从一种样式逐渐改变为另一种的效果。

要实现这一点，必须规定两项内容：

- 指定要添加效果的CSS属性
- 指定效果的持续时间。

应用于宽度属性的过渡效果，时长为 2 秒：

```css
div
{
transition: width 2s;
-webkit-transition: width 2s; /* Safari */
} 
```

**注意：** 如果未指定的期限，transition将没有任何效果，因为默认值是0。

指定的CSS属性的值更改时效果会发生变化。一个典型CSS属性的变化是用户鼠标放在一个元素上时：

规定当鼠标指针悬浮(:hover)于 <div>元素上时：

```css
div:hover
{
width:300px;
} 
```

**注意：** 当鼠标光标移动到该元素时，它逐渐改变它原有样式。

## 多项改变

要添加多个样式的变换效果，添加的属性由逗号分隔：

```css
div
{
transition: width 2s, height 2s, transform 2s;
-webkit-transition: width 2s, height 2s, -webkit-transform 2s;
}  
```

## 过渡属性

下表列出了所有的过渡属性:

| 属性                                                         | 描述                                         | CSS  |
| :----------------------------------------------------------- | :------------------------------------------- | :--- |
| [transition](https://www.w3cschool.cn/cssref/css3-pr-transition.html) | 简写属性，用于在一个属性中设置四个过渡属性。 | 3    |
| [transition-property](https://www.w3cschool.cn/cssref/css3-pr-transition-property.html) | 规定应用过渡的 CSS 属性的名称。              | 3    |
| [transition-duration](https://www.w3cschool.cn/cssref/css3-pr-transition-duration.html) | 定义过渡效果花费的时间。默认是 0。           | 3    |
| [transition-timing-function](https://www.w3cschool.cn/cssref/css3-pr-transition-timing-function.html) | 规定过渡效果的时间曲线。默认是 "ease"。      | 3    |
| [transition-delay](https://www.w3cschool.cn/cssref/css3-pr-transition-delay.html) | 规定过渡效果何时开始。默认是 0。             | 3    |

在一个例子中使用所有过渡属性：

```css
div
{
transition-property: width;
transition-duration: 1s;
transition-timing-function: linear;
transition-delay: 2s;
/* Safari */
-webkit-transition-property:width;
-webkit-transition-duration:1s;
-webkit-transition-timing-function:linear;
-webkit-transition-delay:2s;
}
```



# CSS3 多列

通过 CSS3，您能够创建多个列来对文本进行布局 - 就像报纸那样，您可以收藏文章，需要用到的时候看一看。

在本章中，您将学习如下多列属性：

- column-count
- column-gap
- column-rule
- column-rule-color
- column-rule-style
- column-rule-width
- column-width

## CSS3创建多列

划分成三列的div元素的文本：

```css
div{
    -moz-column-count:3; /* Firefox */
    -webkit-column-count:3; /* Safari and Chrome */
    column-count:3;
}
```

## CSS3列规则

column-rule属性设置列之间的宽度，样式和颜色。

```html
div{
-moz-column-rule:3px outset #ff00ff; /* Firefox */
-webkit-column-rule:3px outset #ff00ff; /* Safari and Chrome */
column-rule:3px outset #ff00ff;
}
```

## CSS3 新多列属性

下表列出了所有 CSS3 的新多列属性，点击属性可以查看更多内容以及用法：

| 属性                                                         | 说明                             | CSS  |
| :----------------------------------------------------------- | :------------------------------- | :--- |
| [column-count](https://www.w3cschool.cn/cssref/css3-pr-column-count.html) | 指定元素应分为的列数             | 3    |
| [column-fill](https://www.w3cschool.cn/cssref/css3-pr-column-fill.html) | 指定如何填充列                   | 3    |
| [column-gap](https://www.w3cschool.cn/cssref/css3-pr-column-gap.html) | 指定列之间差距                   | 3    |
| [column-rule](https://www.w3cschool.cn/cssref/css3-pr-column-rule.html) | 一个用于设置所有列规则的简写属性 | 3    |
| [column-rule-color](https://www.w3cschool.cn/cssref/css3-pr-column-rule-color.html) | 指定的列之间颜色规则             | 3    |
| [column-rule-style](https://www.w3cschool.cn/cssref/css3-pr-column-rule-style.html) | 指定的列之间的样式规则           | 3    |
| [column-rule-width](https://www.w3cschool.cn/cssref/css3-pr-column-rule-width.html) | 指定的列之间的宽度规则           | 3    |
| [column-span](https://www.w3cschool.cn/cssref/css3-pr-column-span.html) | 指定一个元素应该横跨多少列       | 3    |
| [column-width](https://www.w3cschool.cn/cssref/css3-pr-column-width.html) | 指定列的宽度                     | 3    |
| [columns](https://www.w3cschool.cn/cssref/css3-pr-columns.html) | 缩写属性设置列宽和列数           | 3    |

 

## CSS3的指定列之间的差距

column-gap属性指定的列之间的差距：

```css
div{
    -moz-column-gap:40px; /* Firefox */
    -webkit-column-gap:40px; /* Safari and Chrome */
    column-gap:40px;
}
```



# CSS3动画

## 动画属性

下面的表格列出了 @keyframes 规则和所有动画属性：

| 属性                                                         | 描述                                                     | CSS  |
| :----------------------------------------------------------- | :------------------------------------------------------- | :--- |
| [@keyframes](https://www.w3cschool.cn/cssref/css3-pr-animation-keyframes.html) | 规定动画。                                               | 3    |
| [animation](https://www.w3cschool.cn/cssref/css3-pr-animation.html) | 所有动画属性的简写属性，除了 animation-play-state 属性。 | 3    |
| [animation-name](https://www.w3cschool.cn/cssref/css3-pr-animation-name.html) | 规定 @keyframes 动画的名称。                             | 3    |
| [animation-duration](https://www.w3cschool.cn/cssref/css3-pr-animation-duration.html) | 规定动画完成一个周期所花费的秒或毫秒。默认是 0。         | 3    |
| [animation-timing-function](https://www.w3cschool.cn/cssref/css3-pr-animation-timing-function.html) | 规定动画的速度曲线。默认是 "ease"。                      | 3    |
| [animation-delay](https://www.w3cschool.cn/cssref/css3-pr-animation-delay.html) | 规定动画何时开始。默认是 0。                             | 3    |
| [animation-iteration-count](https://www.w3cschool.cn/cssref/css3-pr-animation-iteration-count.html) | 规定动画被播放的次数。默认是 1。                         | 3    |
| [animation-direction](https://www.w3cschool.cn/cssref/css3-pr-animation-direction.html) | 规定动画是否在下一周期逆向地播放。默认是 "normal"。      | 3    |
| [animation-play-state](https://www.w3cschool.cn/cssref/css3-pr-animation-play-state.html) | 规定动画是否正在运行或暂停。默认是 "running"。           | 3    |

### animation-duration 

**定义和用法**

animation-duration：定义动画完成一个周期需要多少秒或毫秒。

| 默认值:          | 0                                          |
| ---------------- | ------------------------------------------ |
| 继承:            | no                                         |
| 版本:            | CSS3                                       |
| JavaScript 语法: | object object.style.animationDuration="3s" |

**语法**

```
animation-duration: time;
```

| **值 ** | **说明**                                                     |
| ------- | ------------------------------------------------------------ |
| time    | 设置一个动画周期的时间间隔（以秒或毫秒为单位）。 默认值为0，表示不会有动画 |

### animation-timing-function 

**定义和用法**

animation-timing-function：指定动画速度曲线。

速度曲线定义动画从一套 CSS 样式变为另一套所用的时间。

速度曲线用于使变化更为平滑。

| 默认值:          | ease                                                 |
| :--------------- | ---------------------------------------------------- |
| 继承:            | no                                                   |
| 版本:            | CSS3                                                 |
| JavaScript 语法: | object object.style.animationTimingFunction="linear" |

**语法**

```
animation-timing-function: linear|ease|ease-in|ease-out|cubic-bezier(n,n,n,n);
```

| 值                            | 描述                                                        | 测试                                                         |
| :---------------------------- | :---------------------------------------------------------- | :----------------------------------------------------------- |
| linear                        | 动画从开始到结束具有相同的速度。                            | [测试](https://www.w3cschool.cn/statics/demosource/Playit/Playit2.html#linear) |
| ease                          | 动画有一个缓慢的开始，然后快，结束慢。                      | [测试](https://www.w3cschool.cn/statics/demosource/Playit/Playit2.html#ease) |
| ease-in                       | 动画有一个缓慢的开始。                                      | [测试](https://www.w3cschool.cn/statics/demosource/Playit/Playit2.html#ease-in) |
| ease-out                      | 动画结束缓慢。                                              | [测试](https://www.w3cschool.cn/statics/demosource/Playit/Playit2.html#ease-out) |
| ease-in-out                   | 动画具有缓慢的开始和慢的结束。                              | [测试](https://www.w3cschool.cn/statics/demosource/Playit/Playit2.html#ease-in-out) |
| cubic-bezier(*n*,*n*,*n*,*n*) | 在立方贝塞尔函数中定义速度函数。 可能的值是从0到1的数字值。 |                                                              |



### animation-delay

**标签定义及使用说明**

animation-delay 属性定义动画什么时候开始。

animation-delay 值单位可以是秒（s）或毫秒（ms）。

**提示:** 允许负值，-2s 使动画马上开始，但跳过 2 秒进入动画。

| 默认值:          | 0                                         |
| :--------------- | ----------------------------------------- |
| 继承:            | no                                        |
| 版本:            | CSS3                                      |
| JavaScript 语法: | *object* object.style.animationDelay="2s" |

**语法**

animation-delay: *time*;

| 值     | 描述                                                    | 测试                                                         |
| :----- | :------------------------------------------------------ | :----------------------------------------------------------- |
| *time* | 可选。定义动画开始前等待的时间，以秒或毫秒计。默认值为0 | [测试 »](https://www.w3cschool.cn/statics/demosource/Playit/Playit2.html#preval=1s) |



### animation-iteration-count

**定义和用法**

animation-iteration-count属性定义动画应该播放多少次。

| 默认值:          | 1                                             |
| ---------------- | --------------------------------------------- |
| 继承:            | no                                            |
| 版本:            | CSS3                                          |
| JavaScript 语法: | object object.style.animationIterationCount=3 |

**语法**

animation-iteration-count: *value*;

| 值       | 描述                           | 测试                                                         |
| -------- | ------------------------------ | ------------------------------------------------------------ |
| *n*      | 定义播放动画多少次。 默认值为1 | [测试 »](https://www.w3cschool.cn/statics/demosource/Playit/Playit2.html#preval=1) |
| infinite | 指定动画应该播放无限次（永远） | [测试 »](https://www.w3cschool.cn/statics/demosource/Playit/Playit2.html#infinite) |

### animation-direction

**定义和用法**

animation-direction 属性定义是否循环交替反向播放动画。

**注意：**如果动画被设置为只播放一次，该属性将不起作用。

| 默认值：          | normal                                                       |
| :---------------- | ------------------------------------------------------------ |
| 继承：            | 否                                                           |
| 可动画化：        | 否。请参阅 [*可动画化（animatable）*](https://www.w3cschool.cn/cssref/css-animatable.html)。 |
| 版本：            | CSS3                                                         |
| JavaScript 语法： | *object*.style.animationDirection="reverse"[尝试一下](https://www.w3cschool.cn/tryrun/showhtml/trycss3_js_animation-direction) |

**CSS 语法**

animation-direction: normal|reverse|alternate|alternate-reverse|initial|inherit;

**属性值**

| 值                | 描述                                                         | 测试                                                         |
| :---------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| normal            | 默认值。动画按正常播放。                                     | [测试 »](https://www.w3cschool.cn/statics/demosource/Playit/Playit2.html#normal) |
| reverse           | 动画反向播放。                                               | [测试 »](https://www.w3cschool.cn/statics/demosource/Playit/Playit2.html#reverse) |
| alternate         | 动画在奇数次（1、3、5...）正向播放，在偶数次（2、4、6...）反向播放。 | [测试 »](https://www.w3cschool.cn/statics/demosource/Playit/Playit2.html#alternate) |
| alternate-reverse | 动画在奇数次（1、3、5...）反向播放，在偶数次（2、4、6...）正向播放。 | [测试 »](https://www.w3cschool.cn/statics/demosource/Playit/Playit2.html#alternate-reverse) |
| initial           | 设置该属性为它的默认值。请参阅 [*initial*](https://www.w3cschool.cn/cssref/css-initial.html)。 |                                                              |
| inherit           | 从父元素继承该属性。请参阅 [*inherit*](https://www.w3cschool.cn/cssref/css-inherit.html)。 |                                                              |

### animation-play-state

**标签定义及使用说明**

animation--play-state属性指定动画是否正在运行或已暂停。

**注意：**在JavaScript中使用此属性在一个周期中暂停动画。

| 默认值:          | running                                           |
| :--------------- | ------------------------------------------------- |
| 继承:            | no                                                |
| 版本:            | CSS3                                              |
| JavaScript 语法: | *object* object.style.animationPlayState="paused" |

**语法**

animation-play-state: paused|running;

| 值      | 描述               | 测试                                                         |
| :------ | :----------------- | :----------------------------------------------------------- |
| paused  | 指定暂停动画       | [测试 »](https://www.w3cschool.cn/statics/demosource/Playit/Playit2.html#paused) |
| running | 指定正在运行的动画 | [测试 »](https://www.w3cschool.cn/statics/demosource/Playit/Playit2.html#running) |

## 案例：位移

```html
    <div class="model">
    </div>
    <style>
        .model {
            width: 50px;
            height: 50px;
            background-color: aqua;
            position: relative;
            animation: zoom 2s infinite;
        }
        
        @keyframes zoom {
            from { top: 0px; }
            to { top: 200px }
        }
    </style>
```



## 案例：放大缩小

```html
<body>
    <div class="model">
    </div>
    <style>
        .model {
            width: 500px;
            height: 500px;
            background-color: aqua;
            border-radius: 250px;
            position: relative;
            animation: zoom 1s infinite;
        }
        
        @keyframes zoom {
            0% {
                transform: scale(0.5);
                opacity: 0.4
            }
            50% {
                transform: scale(1);
                opacity: 1
            }
            100% {
                transform: scale(0.5);
                opacity: 0.4
            }
        }
    </style>
</body>
```


## 案例：颜色渐变

```html
<body>
    <div class="model">
    </div>
    <style>
        .model {
            width: 50px;
            height: 50px;
            background-color: aqua;
            position: relative;
            animation: mymove 5s infinite;
        }
        
        @keyframes mymove {
            0% {
                top: 0px;
                background: red;
                width: 100px;
            }
            100% {
                top: 200px;
                background: yellow;
                width: 300px;
            }
        }
    </style>
</body>
```



# CSS3的长度单位

   在CSS3中，新增了很多长度单位，今天就来谈谈：

- vw、vh、vmin、vmax
- rem与em

**1、vw、vh、vmin、vmax**

vh、`vw`、`vmin`、`vmax`这四个单位都是基于视口的，含义如下：

**（1）vw、vh**

vw是相对视口（viewport）的宽度而定的，长度等于视口宽度的`1/100`。

假如浏览器的宽度为200px，那么1vw就等于2px（200px/100）。

vh是相对视口（viewport）的高度而定的，长度等于视口高度的`1/100`。

假如浏览器的高度为500px，那么1vh就等于5px（500px/100）。

**（2）vmin、vmax**

vmin和`vmax`是相对于视口的高度和宽度两者之间的`最小值`或`最大值`。

如果浏览器的高为300px、宽为500px，那么1vmin就是3px，1vmax就是5px；如果浏览器的高为800px，宽为1080px，那么1vmin也是8px，1vmax也是10.8px。

兼容性：[兼容性](http://caniuse.com/#feat=viewport-units)

**2、rem**

rem是相对于`根元素（html）`的字体大小（font-size）来计算的长度单位。

```
html{font-size: 62.5%}  /* 10 ÷ 16 × 100% = 62.5% */
body{font-size: 1.4rem;} /* 1.4 × 10px = 14px */
```

如果你没有设置html的字体大小，就会以浏览器默认字体大小，一般是16px。

注意：由于浏览器**默认最小字体大小**的限制，如果设置的`根元素字体大小`小于`默认最小字体大小`，那么就会以默认最小字体大小设置根元素。比如：在chrome中，最小字体大小是12px，如果你设置的字体大小小于12px，还是会以12px设置：

```
html{font-size: 62.5%}  /* 16px * 62.5% = 10px */
body{font-size: 1.4rem;} /* 1.4rem * 12px = 16.8px */
```

本意设置body的字体大小为14px，可是实际上却是16.8px，就是因为10px小于12px，所以采取了12px。

所以，我一般都是这样设置：

```
html{font-size: 20px;}
body{font-size: 0.7rem;}  /* 0.7 * 20px = 14px */
```

说到`rem`，我们还会想到`em`，两者都是相对单位，由浏览器转换为像素值，那两者有什么区别？使用哪个更好呢？

rem与`em`的区别：

- rem是相对于根元素（html）的字体大小，而em是相对于其父元素的字体大小
- em最多取到小数点的后三位

```
<style>  html{ font-size: 20px; }  body{     font-size: 1.4rem;  /* 1rem = 28px */    padding: 0.7rem;  /* 0.7rem = 14px */  }   div{    padding: 1em;  /* 1em = 28px */  }  span{    font-size:1rem;  /* 1rem = 20px */    padding: 0.9em;  /* 1em = 18px */  }</style>
<html>  <body>    <div>         <span></span>      </div>  </body></html>
```

在上面的代码中，我们将根元素（html）的字体大小font-size设为`20px`，body的字体大小设为`1rem`，那么转换为像素就是`28px`（20 × 1.4），接着我们又将div的padding设为`1em`，由于其基于父元素，所以转换为像素是`28px` ( 28 × 1），然后我们又将span的字体大小设为`1rem`，也就是`20px`，由于其自身设置了字体大小，所以padding设为`1em`，转换为像素是`20px`(20 × 1），而不是乘以其父元素的字体大小`28px`(28 × 1）。

注意：当元素自身设置了字体大小，那么如果它的其他css属性也使用em单位，则会基于它自身的字体大小。（就像上面例子的span的padding一样）

基于上面这些原因，个人更倾向于使用`rem`，因为`em`使用不当的话，当出现多层继承时，会很容易混淆，比如：

```
<style>  html{ font-size: 20px; }  body{     font-size: 0.9em;  /* 1rem = 18px */  }   div{    font-size: 0.8em;  /* 1em = 14.4px */  }  span{    font-size: 0.9em;  /* 1rem = 12.96px */  }</style>
<html>  <body>    <div>         <span></span>      </div>  </body></html>
```

看到没有，使用`em`时，如果其祖先元素都是用了`em`，那么就会像上面一样，body继承其父元素html的字体大小，而div又继承其父元素body的字体大小，而span又继承其父元素div的字体大小，最终span的字体大小最终是12.96px（20 × 0.9 ×0.8 × 0.9）。

而rem总是相对于根元素（html）的，也就是说，不管哪里使用了rem单位，都是根元素的字体大小 × 数字，由浏览器转为像素值。

对于rem的兼容性，无须担心，点击查看：[兼容性](http://caniuse.com/#search=rem)

当然，em和rem各有优缺点，简单的使用规则：

- 如果这个属性根据它的font-size进行测量，则使用`em`
- 其他的一切事物属性均使用rem.

两者都不使用的情况：

- 多列布局，一般使用百分比%

这里提供了一个px、em、rem单位的转换工具：http://pxtoem.com/

![img](image\CSS\t_remem.png)



# 响应式 Web 设计 – Viewport

## 什么是 Viewport?

`viewport `是用户网页的可视区域。

`viewport` 翻译为中文可以叫做"视区"。

手机浏览器是把页面放在一个虚拟的"窗口"`（viewport）`中，通常这个虚拟的"窗口"`（viewport）`比屏幕宽，这样就不用把每个网页挤到很小的窗口中（这样会破坏没有针对手机浏览器优化的网页的布局），用户可以通过平移和缩放来看网页的不同部分。

------

## 设置 Viewport

一个常用的针对移动网页优化过的页面的 `viewport meta` 标签大致如下：

- `width`：控制 `viewport` 的大小，可以指定的一个值，如果 600，或者特殊的值，如` device-width` 为设备的宽度（单位为缩放为 100% 时的 CSS 的像素）。
- `height`：和 `width` 相对应，指定高度。
- `initial-scale`：初始缩放比例，也即是当页面第一次 `load` 的时候缩放比例。
- `maximum-scale`：允许用户缩放到的最大比例。
- `minimum-scale`：允许用户缩放到的最小比例。
- `user-scalable`：用户是否可以手动缩放。

### 实例1、没有添加 `viewport`：[点击查看](https://www.w3cschool.cn/statics/demosource/example_withoutviewport.html)

![img](https://atts.w3cschool.cn/attachments/uploads/2015/06/img_viewport1.png)

### 实例2、添加 viewport：[点击查看](https://www.w3cschool.cn/statics/demosource/example_withviewport.html)

![img](https://atts.w3cschool.cn/attachments/uploads/2015/06/img_viewport2.png)

如果你在平板电脑或手机上访问，可以直接点击查看效果。

# 响应式 Web 设计 – 网格视图

## 什么是网格视图?

很多网页都是基于网格设计的，这说明网页是按列来布局的。

使用网格视图有助于我们设计网页。这让我们向网页添加元素变的更简单。

响应式网格视图通常是 12 列，宽度为100%，在浏览器窗口大小调整时会自动伸缩。

[响应式网格视图](https://www.w3cschool.cn/statics/demosource/tryresponsive_grid.htm)

------

## 创建响应式网格视图

接下来我们来创建一个响应式网格视图。

首先确保所有的 HTML 元素都有 **box-sizing** 属性且设置为 **border-box**。

确保边距和边框包含在元素的宽度和高度间。

添加如下代码：

```
* {
    box-sizing: border-box;
}
```

查看更多 box-sizing 内容请点击：[CSS3 box-sizing 属性 ](https://www.w3cschool.cn/cssref/css3-pr-box-sizing.html)。

以下实例演示了简单的响应式网页，包含两列：

```css
.menu {
width: 25%;
float: left;}
.main {
width: 75%;
float: left;}
```

以上实例包含两列。

12 列的网格系统可以更好的控制响应式网页。

首先我们可以计算每列的百分比: 100% / 12 列 = 8.33%。

在每列中指定 class， **class="col-"** 用于定义每列有几个 span ：

```css
.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}
```

所有的列向左浮动，间距(padding) 为 15px：

```css
[class*="col-"] {
float: left;
padding: 15px;
border: 1px solid red;}
```

每一行使用 <div> 包裹。所有列数加起来应为 12：

```html
<div class="row">
<div class="col-3">...</div>
<div class="col-9">...</div>
</div>
```

列中行为左浮动，并添加清除浮动：

```css
.row:after {
content: "";
clear: both;
display: block;}
```

我们可以添加一些样式和颜色，让其更好看：

```html
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
    * {
        box-sizing: border-box;
    }
    .row:after {
        content: "";
        clear: both;
        display: block;
    }
    [class*="col-"] {
        float: left;
        padding: 15px;
    }
    .col-1 {width: 8.33%;}
    .col-2 {width: 16.66%;}
    .col-3 {width: 25%;}
    .col-4 {width: 33.33%;}
    .col-5 {width: 41.66%;}
    .col-6 {width: 50%;}
    .col-7 {width: 58.33%;}
    .col-8 {width: 66.66%;}
    .col-9 {width: 75%;}
    .col-10 {width: 83.33%;}
    .col-11 {width: 91.66%;}
    .col-12 {width: 100%;}
    html {
        font-family: "Lucida Sans", sans-serif;
    }
    .header {
        background-color: #9933cc;
        color: #ffffff;
        padding: 15px;
    }
    .menu ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
    }
    .menu li {
        padding: 8px;
        margin-bottom: 7px;
        background-color :#33b5e5;
        color: #ffffff;
        box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
    }
    .menu li:hover {
        background-color: #0099cc;
    }
</style>
</head>
<body>

    <div class="header">
        <h1>查尼亚</h1>
    </div>
    
    <div class="row">
    
    <div class="col-3 menu">
    <ul>
        <li>航班</li>
        <li>城市</li>
        <li>岛屿</li>
        <li>食物</li>
    </ul>
    </div>
    
    <div class="col-9">
        <h1>The City</h1>
        <p>查尼亚是克里特岛查尼亚地区的首府. 这个城市可以分为两部分，老城区和现代城区.</p>
        <p>调整浏览器窗口的大小，以查看内容如何响应调整大小.</p>
    </div>

    </div>
</body>
</html>
```

# 响应式 Web 设计 - 媒体查询

------

媒体(media)查询在 CSS3 上有介绍：[CSS3 @media 查询](https://www.w3cschool.cn/cssref/css3-pr-mediaquery.html)。

使用 @media 查询，你可以针对不同的媒体类型定义不同的样式。

如果浏览器窗口小于 500px, 背景将变为浅蓝色：

```css
@media only screen and (max-width: 500px) {
body { background-color: lightblue;}}
```

## 添加断点

在先前的教程中我们使用行和列来制作网页，它是响应式的，但在小屏幕上并不能友好的展示。

媒体查询可以帮我们解决这个问题。我们可以在设计稿的中间添加断点，不同的断点有不同的效果。

使用媒体查询在 768px 添加断点：

当屏幕 (浏览器窗口) 小于 768px, 每一列的宽度是 100%:

```css
/* For desktop: */
.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}
@media only screen and (max-width: 768px) {  /* For mobile phones: */
[class*="col-"] {width: 100%;}}
```

## 为移动端优先设计

移动端优先意味着在设计桌面和其他设备时优先考虑移动端的设计。

这就意味着我们必须对 CSS 做一些改变。

我们在屏幕小于 768px 进行样式修改，同样在屏幕宽度大于 768px 时也需要修改样式。以下是移动端优先实例：

```css
/* 为移动端设计: */
[class*="col-"] {width: 100%;}
@media only screen and (min-width: 768px) {  /* For desktop: */
.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}}
```

## 方向：横屏/竖屏

结合 CSS 媒体查询,可以创建适应不同设备的方向(横屏 landscape、竖屏 portrait 等)的布局。

### 语法：

orientation：portrait | landscape

- **portrait：**指定输出设备中的页面可见区域高度大于或等于宽度
- **landscape：** 除 portrait 值情况外，都是 landscape

```html
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
    body {
        background-color: lightgreen;
    }
    
    @media only screen and (orientation: landscape) {
        body {
            background-color: lightblue;
        }
    }
</style>
</head>
<body>
    <p>重置浏览器大小，当文档的宽度大于高度时，背景会变为浅蓝色。否则为浅绿色。</p>
</body>
</html>
```

# 响应式 Web 设计 – 图片

## 使用 width 属性

如果 width 属性设置为 100%，图片会根据上下范围实现响应式功能：

```css
img {
width: 100%;
height: auto; }
```

注意在以上实例中，图片会比它的原始图片大。我们可以使用 `**max-width**` 属性很好的解决这个问题。

## 使用 `max-width` 属性

如果 `max-width` 属性设置为 100%, 图片永远不会大于其原始大小：

```css
img {
max-width: 100%;
height: auto; }
```

**提示：**关于`max-width` 属性的更多内容，请参考本站 CSS 参考手册中的 [CSS max-width属性](https://www.w3cschool.cn/cssref/pr-dim-max-width.html)部分。

## 背景图片

背景图片可以响应调整大小或缩放。

以下是三个不同的方法：

1. 如果 `background-size` 属性设置为 `contain`, 背景图片将按比例自适应内容区域。图片保持其比例不变：

   ```css
   div { 
   background-repeat: no-repeat;
   background-size: contain; }
   ```

2. 如果 `background-size` 属性设置为 "100% 100%" ，背景图片将延展覆盖整个区域：

   ```css
   div {
   background-size: cover;
   border: 1px solid red;}
   ```

## 不同设备显示不同图片

大尺寸图片可以显示在大屏幕上，但在小屏幕上确不能很好显示。我们没有必要在小屏幕上去加载大图片，这样很影响加载速度。所以我们可以使用媒体查询，根据不同的设备显示不同的图片。

以下大图片和小图片将显示在不同设备上：

```css
/* For width smaller than 400px: */
body {
background-image: url('img_smallflower.jpg');
}
/* For width 400px and larger: */
@media only screen and (min-width: 400px) {
body {
background-image: url('img_flowers.jpg'); } }
```

你可以使用媒体查询的 `min-device-width` 替代` min-width` 属性，它将检测的是设备宽度而不是浏览器宽度。浏览器大小重置时，图片大小不会改变。

```css
/* 设备小于 400px: */
body {
background-image: url('img_smallflower.jpg'); }
/* 设备大于 400px (也等于): */
@media only screen and (min-device-width: 400px) {
body {
background-image: url('img_flowers.jpg'); } }
```

## HTML5 <picture> 元素

HTML5 的 <picture>元素可以设置多张图片。

<picture> 元素类似于`` 和 `` 元素。可以设备不同的资源，第一个设置的资源为首选使用的：

```css
<picture>
<source srcset="img_smallflower.jpg" media="(max-width: 400px)">
<source srcset="img_flowers.jpg">
<img src="img_flowers.jpg" alt="Flowers">
</picture>
```

`srcset` 属性的必须的，定义了图片资源。

`media` 属性是可选的，可以在媒体查询的 [CSS @media 规则](https://www.w3cschool.cn/cssref/css3-pr-mediaquery.html) 查看详情。

对于不支持<picture> 元素的浏览器你也可以定义``元素来替代。

## 扩展阅读

[《响应式图片101》](https://www.w3cschool.cn/responsive_images_101/)：介绍为什么需要响应式图片以及如何选择正确的响应式图片解决方案。


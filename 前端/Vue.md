

## 1、认识Vuejs

### 为什么学习Vuejs

作为前端框架越来越流行，前端必备技能之一。

目前市场招聘中前端要求Vue技能占7成以上。

在Vue中，一个核心的概念，就是让用户不再操作DOM元素，解放了用户的双手，让程序员可以更多的时间去关注业务逻辑。

### 初始Vue

Vue读音/ju:/，类似于view

vue是一个渐进式的框架，什么是渐进式呢？

* 渐进式意味着你可以将Vue作为你应用的一部分嵌入其中，带来更丰富的交互体验。
* 或者如果你希望将更多的业务逻辑使用Vue实现，那么Vue的核心库以及其生态系统。
* 比如Core+Vue-router+Vuex，可以满足你各种各样的需求

Vue特点

* 解耦视图和数据
* 可复用的组件
* 前端路由技术
* 状态管理
* 虚拟DOM

## 2、Vuejs安装



# 二、Vue基础语法

## 1、常用方法

### 1.1 基本用法

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- 1. 导入Vue的js包 -->
    <script src="./lib/vue.js"></script>
</head>
<body>
    <!-- 将来new的Vue实例，会控制这个元素中的所有内容 -->
    <!-- Vue实例锁控制的这个元素区域，就是我们的v -->
    <div id="app">
        <p>{{ msg }}</p>
    </div>
        
    <script>
        //2. 创建一个Vue的实例
        // 当我们导入包后，在浏览器内存中，就多了一个Vue构造函数
        // 注意：我们new出来的这个vm对象，就是我们mvvm中的vm调度者
        // 在vm中如果要访问data和methods中的内容，必须带this
        var vm = new Vue({
            el: '#app',     // 表示当前我们new的这个Vue实例，要控制页面上的哪个区域
            // 这里的data就是mvvm中的m，专门用来保存数据
            data: {
                msg: '欢迎学习Vue'
            },
            methods:{
                func: function(){}
            },
            computed: {},	// 计算属性
            filters: {},	// 过滤
            template: {},   // 制作模板
            components: {},	// 自定义标签
            directives: {},
            router: {},
            watch: {
                '$route.path': function(newVal, oldVal){
                    console.log(newVal + ' --- ' + oldVal)
                }
            }
        })
    </script>
</body>
</html>
```



### 1.2 插值

#### 文本

数据绑定最常见的形式就是使用“Mustache”语法（双大括号）的文本插值：

```html
<sapn>Message: {{ msg }}</sapn>
```

Mustache标签将会被代替为对应数据对象上msg属性的值。无论何时，绑定的数据对象上`msg`属性发生了改变。插值处的内容都会更新。

通过使用 [v-once指令]("")，你也能执行一次性地插值，当数据改变时，插值处的内容不会更新。但请留心这会影响到该节点上的其它数据绑定：

```html
<span v-once>这个将不会改变：{{ msg }}</span>
```

#### 原始HTML

双大括号会将数据解释为普通文本，而非HTML代码。为了输出真正的HTML，你需要使用v-html指令：

```html
<p>Using mustaches: {{ rawHtml }}</p>
<p>Using v-html directive: <span v-html="rawHtml"></span></p>
```

![image-20200223175718384](image\Vue\image-20200223175718384.png)

这个`span`的内容将会被替换为属性值`rawHtml`，直接作为HTML——会忽略解析属性值中的数据绑定。注意，你不能使用`v-html`来复合局部模板，因为Vue不是基于字符串的模板引擎。反之，对于用户界面（UI）,组件更适合作为可重用和可组合的基本单位。

#### Attribute

Mustache语法不能作用在HTML attribute上，遇到这种情况应该使用`v-bind`指令：

```html
<div v-bind:id="dynamicId"></div>
```

对于布尔attribute（它们只能存在就意味着值为`true`），`v-bind`工作起来略有不同，在这个例子中: 

```html
<button v-bind:disabled="isButtonDisabled">Button</button>
```

如果`isButtonDisabled`的值是`null`、`undefined`或`false`，则`disabled`attribute甚至不会被包含在渲染出来的`<button>`元素中。

#### 使用JavaScript表达式

对于所有的数据绑定，Vue.js都提供了完全的JavaScript表达式支持。

```HTML
{{ number + 1}}
{{ ok ? 'YES' : 'NO'}}
{{ message.split('').reverse().join() }}
<div v-bind:id="'list-' + id"></div>
```

这些表达式会在所属Vue实例的数据作用域下作为JavaScript被解析。有个限制就是，每个绑定都只能包含单个表达式。下面的例子都不会生效

```html
<!-- 这个语句，不是表达式 -->
{{ var a = 1 }}

<!-- 流控制也不会生效，请使用三元表达式 -->
{{ if (OK) {return message} }}
```



### v-bind 绑定属性

v-bind：是vue中，提供用于绑定属性的指令，可以简写为一个冒号

```html
<body>
    <div class="app">
        <!-- v-bind：是vue中，提供用于绑定属性的指令，可以简写为一个冒号 -->
        <input type="button" value="按钮1" v-bind:title="mytitle">  
        <input type="button" value="按钮2" :title="mytitle + '123'">  
    </div>
    
    <script>
        var vm = new Vue({
            el: '.app',
            data:{
                mytitle: '这是一个按钮'
            }
        })
    </script>
</body>
```

### v-on  绑定事件

用来绑定事件方法，缩写是@符号

事件修饰符：

* .stop 阻止冒泡
* .prevent 阻止默认事件
* .capture 添加事件监听器时使用事件捕获模式
* .self 只当事件在该元素本身触发时回调
* .once 事件只触发一次

```html
<body>
    <div class="app">
        <!-- v-on进行事件绑定 -->
        <input type="button" value="按钮1" v-bind:title="mytitle" v-on:click="show"> 
    </div>
    
    <script>
        var vm = new Vue({
            el: '.app',
            data:{
                mytitle: '这是一个按钮'
            },
            methods: {  //定义当前vue实例所有可用的方法
                show: function(){
                    alert('hello')
                }
            }
        })
    </script>
</body>
```



###  v-model  双向数据绑定

**v-model** 指令用来在 input、select、textarea、checkbox、radio 等表单控件元素上创建双向数据绑定，根据表单上的值，自动更新绑定的元素的值。

```html
<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    <title>简易计算机</title>
    <script src="./lib/vue.js"></script>
    </head>
    <body>
        <div id="app">
            <input type="text" v-model="n1">
            <select name="" id="" v-model="opt">
                <option value="+">+</option>
                <option value="-">-</option>
                <option value="*">*</option>
                <option value="/">/</option>
            </select>
            <input type="text" v-model="n2">
            <input type="button" value="=" v-on:click="calc">
            <input type="text" v-model="result">
            
        </div>
        <script>
            var vm = new Vue({
                el: '#app',
                data: {
                    n1: 0,
                    n2: 0,
                    result: 0,
                    opt: "+"
                },
                methods:{
                    calc: function (){
                        // switch (this.opt) {
                        //     case '+':
                        //         this.result = parseInt(this.n1) + parseInt(this.n2)
                        //         break;
                        //     case '-':
                        //         this.result = parseInt(this.n1) - parseInt(this.n2)
                        //         break;
                        //     case '*':
                        //         this.result = parseInt(this.n1) * parseInt(this.n2)
                        //         break;
                        //     case '/':
                        //         this.result = parseInt(this.n1) / parseInt(this.n2)
                        //         break;
                        //     default:
                        //         break;
                        // }

                        var codestr = 'parseInt(this.n1) ' + this.opt + ' parseInt(this.n2)'
                        this.result = eval(codestr)
                    }
                }
            })
        </script>
    </body>
</html>
```

###  v-for

1、迭代数组

```html
<p v-for="(item, i) in list">索引值{{ i }}---{{ item }}</p>
```

2、迭代对象中的属性

```html
<p v-for="(user, i) in listMap">索引值{{ i }}---id: {{ user.id }}, name: {{ user.name }}</p>
<p v-for="(val, key, i) in obj">索引值{{ i }}---值：{{ val }} --- 键：{{ key }}</p>
```

3、迭代数字

```html
<!-- 遍历数字, 注意：如果迭代数字，count从1开始 -->
<p v-for="count in 10">这是第{{ count }}次循环</p>
```

```html
		<script>
            var vm = new Vue({
                el: '#app',
                data: {
                    list: ['a', 'b', 'c', 'd', 'e', 'f'],
                    listMap:[
                        {id: 1, name: 'zs1'},
                        {id: 2, name: 'zs2'},
                        {id: 3, name: 'zs3'},
                        {id: 4, name: 'zs4'},
                    ],
                    obj: {id: 1, name: 'zhangsan', sex: '男'}
                },
                methods:{}
            })
        </script>
```

![image-20200221230203287](image\Vue\image-20200221230203287.png)

###  v-if和v-show

两个都是控制显隐的：

v-if：有更高的切换消耗，适用于不怎么改变情况

v-show：有更高的初始渲染消耗，适用于不频繁切换

![image-20200221231210870](image\Vue\image-20200221231210870.png)

当 `v-if` 与 `v-for` 一起使用时，`v-for` 具有比 `v-if` 更高的优先级，不推荐同时使用。

### v-else

可以使用`v-else`指令来表示`v-if`的”else块“：

```html
<div v-if="Math.random() > 0.5">
    Now you see me
</div>
<div v-else>
    Now you don't
</div>
```

`v-else`元素必须紧跟在带`v-if`或`v-else-if`的元素的后面，否则它将不会被识别。

### v-else-if

> 2.1.0新增

充当`v-if`的"else-if块"，可以连续使用：

```html
<div v-if="type ==== 'a'">
    a
</div>
<div v-else-if="type === 'b'">
    b
</div>
<div v-else>
    Not a/b
</div>
```



### v-cloak/v-text/v-html

v-cloak 遮盖，v-html 使用html输出

```html
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        [v-cloak] {
            display: none;
        }
    </style>

</head>
<body>
    <div class="app">
        <!-- 使用v-cloak可以解决插值表达式闪烁的问题 -->
        <p v-cloak>{{msg}}</p>
        <h4 v-text="msg"></h4>      
        <!-- v-text可以防止先闪烁出{{msg}} -->

        <div>{{msg2}}</div>
        <div v-text="msg2"></div>
        <div v-html="msg2"></div>
    </div>
    <script src="./lib/vue.js"></script>
    <script>
        var vm = new Vue({
            el: '.app',
            data:{
                msg: '123',
                msg2: '<h1>这是一个大大的h1标签</h1>'
            }
        })
    </script>
</body>
</html>
```

## 2、样式使用

### 2.1 使用class样式

1. 直接在元素上通过`:class`的形式，使用数组书写类

   ```html
   <h1 v-bind:class="['red', 'thin']">this is a h1.</h1>
   ```

2. 数组中使用三元表达式

   ```html
   <h1 v-bind:class="['red', 'thin', flag ? 'active' : '']">this is a h1.</h1>
   ```

3. 数组中嵌套对象

   ```html
   <h1 v-bind:class="['red', 'thin', {'active': flag}]">this is a h1.</h1>
   ```

4. 直接使用对象

   ```html
   <h1 :class="{red: true, thin: true, active: false, italic: true}">this is a h1.</h1>
   ```

```html
<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    <title>vue html demo</title>
    <script src="./lib/vue.js"></script>
    <style>
        .red {
            color: red;
        }
        .thin {
            font-weight: 200;
        }
        .italic {
            font-style: italic;
        }
        .active {
            letter-spacing: 0.5em;
        }

    </style>
    </head>
    <body>
        <div id="app">
            <!-- 第一种使用方法，直接传递一个数组，注意这里的class需要使用v-bind做数据绑定 -->
            <!-- 在数组中使用三元表达式 -->
            <h1 v-bind:class="['red', 'thin', flag ? 'active' : '']">this is a h1.</h1>
            <!-- 可以使用对象来代替三元表达式，提高可读性 -->
            <h1 v-bind:class="['red', 'thin', {'active': flag}]">this is a h1.</h1>
            <!-- 直接使用对象, 对象的属性可以不带引号 -->
            <h1 :class="{red: true, thin: true, active: false, italic: true}">this is a h1.</h1>

            <h1 :class="classObj">this is a h1.</h1>
        </div>
        <script>
            var vm = new Vue({
                el: '#app',
                data: {
                    flag: true,
                    classObj: {red: true, thin: true, active: false, italic: false}
                },
                methods:{}
            })
        </script>
    </body>
</html>
```

### 2.2 使用内联样式

1. 直接在元素上通过`:style`的形式，书写样式对象

   ```html
   <h1 :style="{color: 'red', 'font-size': '40px'}">这是一个h1标签</h1>
   ```

2. 将样式对象，定义到data中，并直接引用到`:style=“h1StyleObj"`中

   ```html
   data:{
   	h1StyleObj: {color: 'red', 'font-size': '40px', 'font-weight': '200'}
   }
   ```

3. 在`:style=“[h1StyleObj,h1StyleObj2]"`中通过数组，引用多个`data`上的样式对象

   ```html
   data:{
   	h1StyleObj: {color: 'red', 'font-size': '40px', 'font-weight': '200'},
   	h1StyleObj2: {fontStyle: 'italic'},
   }
   ```

   

### 2.3 filter过滤器

*只能用在{{}}和v-bind*

如果全局和私有名称一样，优先调用私有过滤器

```html
<div id="app">
    <p>{{ msg | timeFormat('') }}</p>
</div>

<script>
    	// 全局过滤器
		Vue.filter('timeFormat', function(data, pattern) {
            var day = new Date(data)
            var y = day.getFullYear();
            var m = (day.getMonth() + 1).toString().padStart(2, '0');
            var d = day.getDate().toString().padStart(2, '0');
            var hh = day.getHours().toString().padStart(2, '0');
            var mm = day.getMinutes().toString().padStart(2, '0');
            var ss = day.getSeconds().toString().padStart(2, '0');
            if (pattern.toLowerCase() === 'yyy-mm-dd') {
                return `${y}-${m}-${d}`
            } else {
                return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
            }
            // return data.toLocaleString()
        })
    
    	var vm = new Vue({
            el: '#app',
            data:{},
            methods:{
                msg: new Date()
            },
            filters:{	// 私有过滤器
                
            }
        })
</script>
```

### 2.4 键盘的键值修饰符

在监听键盘事件时使用

```html
<input v-on:keyup.13="submit">
```

记住所有的keyCode比较困难，所以Vue提供了常用按键的别名：

> .enter
>
> .tab
>
> .delete (捕获“删除”和“退格”键)
>
> .esc
>
> .space
>
> .up
>
> .down
>
> .left
>
> .right

可以通过全局`congfig.keyCode`对象自定义键值修饰符别名：

> Vue.config.keyCodes.f2 = 113

[js里面的键盘事件对应的键码]: "https://www.cnblogs.com/wuhua1/p/6686237.html"

## 3、计算属性和侦听器

### 3.1 计算属性

模板内的表达式非常便利，但是设计他们的初衷是用于简单运算。在模板中放入太多的逻辑会让模板过重且难以维护。例如：

```html
<div id="example">
  {{ message.split('').reverse().join('') }}
</div>
```

这这个地方模板不是简单的声明逻辑。你必须看一段时间才能意识到，这里是想要显示变量`message`的翻转字符串。当你想要在模板中多次引用此处的翻转字符串时，就会更加难以处理。

所以，对于任何复杂逻辑，你都应当使用**计算属性**。

#### 基础例子

```html
<div id="example">
    <p>Original message: "{{ message }}"</p>
    <p>Computed reversed message: "{{ reversedMessage }}"</p>
</div>
```

```js
var vm = new Vue({
    el: 'example',
    date: {
        message: 'Hello'
    },
    computed: {
        // 计算属性的getter
        reversedMessage: function () {
            // `this` 指向vm实例
            return this.message.split(''),reverse().join('')
        }
    }
})
```

结果：

Original message: "Hello"

Computed reversed message: "olleH"

这里我们声明了一个计算机属性`reversedMesssage`。我们提供的函数将用作属`vm.reversedMessage`的getter函数：

```js
console.log(vm.reversedMessage) // => 'olleH'
vm.message = 'Goodbye'
console.log(vm.reversedMessage) // => 'eybdooG'
```

你可以打开浏览器的控制台，自行修改例子中的 vm。`vm.reversedMessage` 的值始终取决于 `vm.message` 的值。

#### 计算属性缓存vs方法

我们可以将同一个函数定义为一个方法而不是一个计算属性：

```html
<p>Reversed message: "{{ reversedMessage() }}"</p>
```

```js
// 在组件中
methods: {
    reversedMessage: function () {
        return this.message.split('').reverse().join('')
    }
}
```

两种方式的最终结果确实完全相同。然而，不同的是**计算属性是基于他们的响应式依赖进行缓存的**。只在相关响应式依赖发生改变时它们才会重新求值。这就意味着只要`message`还没有发生改变，多次访问reversedMessage计算属性会立即返回之前的计算结果，而不必再次执行函数

我们为什么需要缓存？假设我们有一个性能开销比较大的计算属性A，它需要遍历一个巨大的数组并做大量的计算。然后我们可能有其他的计算属性依赖于A。如果没有缓存，我们将不可避免的多次执行A的getter！如果你不希望有缓存，请用方法来代替。

#### 计算属性vs侦听属性

Vue提供了一种更通用的方式来观察和响应Vue实例上的数据变动：**侦听属性**。当你有一些数据需要随着其它数据变动而变动时，你很容易滥用`watch`----特别是如果你之前使用过AngularJS。然而，通常更好的做法是使用计算属性而不是命令式的`watch`回调。如下例子：

```html
<div id="demo">{{ fullName }}</div>
```

```js
var vm = new Vue({
    el: '#demo',
    data: {
        firstName: 'Foo',
        lastName: 'Bar',
        fullName: 'Foo Bar'
    },
    watch: {
        firstName: function (val) {
            this.fullName = val + ' ' + this.lastName        
        },
        lastName: function (val) {
            this.fullName = this.firstName + ' ' + val
        }
    }
})
```

上面的代码是命令式且重复的，将它与计算属性的版本进行比较：

```js
var vm = new Vue({
    el: '#demo',
    data: {
        firstName: 'Foo',
        lastName: 'Bar',
    },
    computed: {
        fullName: function () {
            return this.firstName + ' ' + this.lastName
        }
    }
})
```

#### 计算属性的setter

计算属性默认只有getter，不过在需要时可以提供一个setter：

```js
computed: {
    fullName: {
        // getter
        get: function () {
            return this.firstName + ' ' + this.lastName
        }
        // setter
        set: function (newValue) {
            var names = newValue.split(' ')
            this.firstName = names[0]
            this.lastName = names[names.length - 1]
        }
    }
}
```

现在再运行 `vm.fullName = 'John Doe'` 时，setter 会被调用，`vm.firstName` 和 `vm.lastName` 也会相应地被更新。

### 3.2 侦听器

虽然计算属性在大多数情况下更合适，但有时也需要一个自定义的侦听器。这就是为什么Vue通过`watch`选项提供了一个更通用的方法，来响应数据的变化。当需要在数据变化时执行异步或开销较大的操作时，这个方式是最有用的。

例如：

```html
<div id="watch-example">
    <p>
        Ask a yes/no question:
        <input v-model="question">
    </p>
    <p>{{ answer }}</p>
</div>
```

```js
<!-- 因为 AJAX 库和通用工具的生态已经相当丰富，Vue 核心代码没有重复 -->
<!-- 提供这些功能以保持精简。这也可以让你自由选择自己更熟悉的工具。 -->
<script src="https://cdn.jsdelivr.net/npm/axios@0.12.0/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/lodash@4.13.1/lodash.min.js"></script>
<script>
var watchExampleVM = new Vue({
  el: '#watch-example',
  data: {
    question: '',
    answer: 'I cannot give you an answer until you ask a question!'
  },
  watch: {
    // 如果 `question` 发生改变，这个函数就会运行
    question: function (newQuestion, oldQuestion) {
      this.answer = 'Waiting for you to stop typing...'
      this.debouncedGetAnswer()
    }
  },
  created: function () {
    // `_.debounce` 是一个通过 Lodash 限制操作频率的函数。
    // 在这个例子中，我们希望限制访问 yesno.wtf/api 的频率
    // AJAX 请求直到用户输入完毕才会发出。想要了解更多关于
    // `_.debounce` 函数 (及其近亲 `_.throttle`) 的知识，
    // 请参考：https://lodash.com/docs#debounce
    this.debouncedGetAnswer = _.debounce(this.getAnswer, 500)
  },
  methods: {
    getAnswer: function () {
      if (this.question.indexOf('?') === -1) {
        this.answer = 'Questions usually contain a question mark. ;-)'
        return
      }
      this.answer = 'Thinking...'
      var vm = this
      axios.get('https://yesno.wtf/api')
        .then(function (response) {
          vm.answer = _.capitalize(response.data.answer)
        })
        .catch(function (error) {
          vm.answer = 'Error! Could not reach the API. ' + error
        })
    }
  }
})
</script>
```

## 4、生命周期

### 4.1 生命周期图示

下图展示了实例的生命周期。你不需要立马弄明白所有的东西，不过随着你的不断学习和使用，它的参考价值会越来越高。

![Vue 实例生命周期](image\Vue\lifecycle.png)

**实例**

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>vue html demo</title>
    <script src="./lib/vue.js"></script>
</head>

<body>
    <div id="app">
        <input type="button" value="修改msg" @click="msg='no'">
        <h3 id="h"> {{ msg }}</h3>
    </div>
    <script>
        var vm = new Vue({
            el: '#app',
            data: {
                msg: 'ok'
            },
            methods: {
                show() {
                    console.log('执行了show方法')
                }
            },
            beforeCreate() { // 实例创建之前会执行它
                // 下面两个都还没有加载，无法显示
                // console.log(this.msg)
                // this.show()
            },
            created() {
                // 这时data、methos都已经被初始化好了
                console.log(this.msg)
                this.show()
            },
            beforeMount() {
                // 表示模板已经编译完了，但尚未把模板渲染到页面中
                // console.log(document.getElementById('h3').innerText)
            },
            mounted() {
                // 表示内存中的模板已经真实的挂载到了页面中，用户可以看到渲染的页面
                // console.log('h3里的值：' + document.getElementById('h').innerText)
            },

            // 运行中的两个事件
            beforeUpdate() {
                // 表示我们的界面还没有被更新，但是数据被更新了
                // console.log('界面上h3里的值：' + document.getElementById('h').innerText)
                // console.log("data里的msg值：" + this.msg)
            },
            updated() {
                console.log('界面上h3里的值：' + document.getElementById('h').innerText)
                console.log("data里的msg值：" + this.msg)
            },

            // 销毁的两个事件，不怎么用
            beforeDestroy() {

            },
            destroyed() {

            }
        })
    </script>
</body>

</html>
```

## 5、vue-resource

### 用get、post、jsonp实现请求

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>vue html demo</title>
    <script src="./lib/vue.js"></script>
    <!-- 注意vue-resource依赖于vue，要放到后面 -->
    <script src="https://cdn.bootcss.com/vue-resource/1.5.1/vue-resource.min.js"></script>
</head>

<body>
    <div id="app">
        <input type="button" value="get请求" @click="getInfo">
        <input type="button" value="post请求" @click="postInfo">
        <input type="button" value="jsonp请求" @click="jsonpInfo">
    </div>
    <script>
        var vm = new Vue({
            el: '#app',
            data: {},
            methods: {
                getInfo() {
                    // 当发起get请求后，通过.then来设置成功的回调函数
                    this.$http.get("http://www.liulongbin.top:3005/api/getprodlist").then(function(result) {
                        console.log(result)
                        console.log(result.data)
                        console.log(result.body) // 推荐使用body
                    })
                },
                postInfo() {
                    // 手动发起的post请求，默认没有表单格式，所以，有的服务器处理不了，可以设置emulateJson: true
                    this.$http.post("http://www.liulongbin.top:3005/api/addproduct", {}, {
                        emulateJson: true
                    }).then(result => {
                        console.log(result)
                    })
                },
                jsonpInfo() {
                    this.$http.jsonp("http://www.liulongbin.top:3005/api/jsonp").then(result => {
                        console.log(result)
                        console.log(result.body)
                    })
                }
            }
        })
    </script>
</body>

</html>
```

### JSONP原理

目前主流的实现跨域通信的解决方案，ajax 请求受同源策略影响，不允许进行跨域请求，而 script 标签 src 属性中的链接却可以访问跨域的js脚本，利用这个特性，服务端不再返回JSON格式的数据，而是返回一段调用某个函数的js代码，在src中进行了调用，这样实现了跨域。

>  `node.js 和 jsonp`一个案例

**客户端jsonp页面**

用open with live server启动，vscode需要安装`Live Server`插件

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <script>
        function showInfo(data) {
            console.log(data)
        }
    </script>

    <script src="http://127.0.0.1:3000/getscript?method=showInfo"></script>
</body>

</html>
```

**后端node**

使用nodemon app.js启动

```js
// 导入内置模块
const http = require('http')
    // 解析url地址，从而得到pathname query
const urlModule = require('url')

// 创建一个http服务器
const server = http.createServer()

//监听http服务器的request请求
server.on('request', function(req, res) {
    const { pathname: url, query } = urlModule.parse(req.url, true)
    if (url === '/getscript') {
        var data = {
            name: 'xjj',
            age: 18,
            gender: '女孩子'
        }
        var scriptStr = `${query.method}(${JSON.stringify(data)})`
        res.end(scriptStr)
    } else {
        res.end('404')
    }
})

// 指定端口号并启动服务器监听
server.listen(3000, function() {
    console.log('server listen at http://127.0.0.1:3000')
})
```

### 品牌列表更改案例

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>vue html demo</title>
    <script src="./lib/vue.js"></script>
    <!-- 注意vue-resource依赖于vue，要放到后面 -->
    <script src="https://cdn.bootcss.com/vue-resource/1.5.1/vue-resource.min.js"></script>
    <link rel="stylesheet" href="./lib/bootstrap.css">
</head>

<body>
    <div id="app">

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">添加品牌</h3>
            </div>
            <div class="panel-body form-inline">

                <label>Name: 
                    <input type="text" v-model="name" class="form-control">
                </label>
                <input type="button" value="添加" @click="addProdList" class="btn btn-primary">
            </div>
        </div>

        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Ctime</th>
                    <th>Optation</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="item in list " v-bind:key="item.id">
                    <td>{{ item.id }}</td>
                    <td>{{ item.name }}</td>
                    <td>{{ item.ctime }}</td>
                    <td><a @click="delProdList(item.id)">删除</a></td>
                </tr>
            </tbody>
        </table>

    </div>
    <script>
        // 注意全局路径结尾有斜线，下面请求的时候不能有斜线
        Vue.http.options.root = "http://www.liulongbin.top:3005/";
        // 设置以普通表单格式，将数据提交给服务器 application/x-www-form-urlencoded 
        Vue.http.options.emulateJSON = true;
        var vm = new Vue({
            el: '#app',
            data: {
                name: '',
                list: [{
                    id: 1,
                    name: '五菱宏光',
                    ctime: new Date()
                }, {
                    id: 2,
                    name: '众泰',
                    ctime: new Date()
                }]
            },
            // 生命周期函数，初始化注入后执行
            created() {
                this.getAllList()
            },
            methods: {
                getAllList() {
                    this.$http.get('api/getprodlist').then(result => {
                        // console.log(result)
                        if (result.body.status === 0) {
                            this.list = result.body.message
                        } else {
                            console.log("返回数据为空")
                        }
                    })
                },
                addProdList() {
                    this.$http.post('api/addproduct', {
                        name: this.name
                    }).then(result => {
                        console.log("添加品牌: " + this.name)
                        if (result.body.status === 0) {
                            alert("添加成功")
                            this.getAllList()
                            this.name = ''
                        } else {
                            console.log("error")
                        }
                    })
                },
                delProdList(id) {
                    console.log("删除id: " + id)
                    this.$http.get("api/delproduct/" + id).then(result => {
                        console.log(result)
                        alert(result.body.message)
                    })
                    this.getAllList()
                }
            }
        })
    </script>
</body>

</html>
```

## 6、Vue 中的动画&过度

#### 概述

vue在插入、更新或者移除DOM时，提供多种不同方式的应用过度效果。

包括以下工具：

* 在css过度和动画中自动应用class
* 可以配合使用第三方css动画库，如Animate.css
* 在过度钩子函数中使用javascript直接操作DOM
* 可以配合使用第三方javascript动画库，如Velocity.js

### 单元素、组件的过度

Vue提供了`transition`的封装组件，在下列情形中，可以给任何元素和组件添加进入、离开过度

* 条件渲染（使用`v-if`）
* 条件展示（使用`v-show`）
* 动态组件
* 组件根节点

一个典型的例子：

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>vue html demo</title>
    <script src="./lib/vue.js"></script>
    <style>
        .fade-enter-active,
        .fade-leave-active {
            transition: opacity .5s;
        }
        
        .fade-enter,
        .fade-leave-to {
            opacity: 0;
        }
    </style>
</head>

<body>
    <div id="app">
        <button v-on:click="show = !show">Toggle</button>
        <!-- 控制元素过度效果 -->
        <transition name="fade" mode="">
            <p v-if="show">hello</p>
        </transition>
    </div>
    <script>
        var vm = new Vue({
            el: '#app',
            data: {
                show: TextTrackCue
            },
            methods: {}
        })
    </script>
</body>

</html>
```

当插入或删除包含在`transition`组件中的元素时，Vue将做以下处理：

1. 自动嗅探目标元素是否应用了css过度或动画，如果是，在恰当的时机添加、删除CSS类名。
2. 如果过渡组件提供了 [JavaScript 钩子函数](https://cn.vuejs.org/v2/guide/transitions.html#JavaScript-钩子)，这些钩子函数将在恰当的时机被调用。
3. 如果没有找到 JavaScript 钩子并且也没有检测到 CSS 过渡/动画，DOM 操作 (插入/删除) 在下一帧中立即执行。(注意：此指浏览器逐帧动画机制，和 Vue 的 `nextTick` 概念不同)

### 过度的类名

在进入、离开的过度中，会有6个class切换。

1. `v-enter`：在定义进入过度的开始状态。在元素被插入之前生效，在元素被插入之后的下一帧移除
2. `v-enter-active`：定义进入过度生效时的状态。在整个进入过度的阶段中应用，在元素被插入之前生效，在过度、动画完成之后移除。这个类可以被用来定义进入过度的过程时间，延迟和曲线函数。
3. `v-enter-to`：2.1.8版本以上定义进入过度的结束状态。在元素被插入之后下一帧生效（与此同时`v-enter`被移除），在过度、动画完成之后移除。
4. `v-leave`: 定义离开过渡的开始状态。在离开过渡被触发时立刻生效，下一帧被移除。
5. `v-leave-active`：定义离开过渡生效时的状态。在整个离开过渡的阶段中应用，在离开过渡被触发时立刻生效，在过渡/动画完成之后移除。这个类可以被用来定义离开过渡的过程时间，延迟和曲线函数。
6. `v-leave-to`: **2.1.8版及以上** 定义离开过渡的结束状态。在离开过渡被触发之后下一帧生效 (与此同时 `v-leave` 被删除)，在过渡/动画完成之后移除。

![image-20200227154506004](image\Vue\image-20200227154506004.png)

对于这些在过渡中切换的类名来说，如果你使用一个没有名字的 ``，则 `v-` 是这些类名的默认前缀。如果你使用了 ``，那么 `v-enter` 会替换为 `my-transition-enter`。

### CSS过度

常用的过度都是使用CSS过度，如下：

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>vue html demo</title>
    <script src="./lib/vue.js"></script>
    <style>
        /* 可以设置不同的进入和离开动画 */
        .fade-enter-active {
            transition: all .3s ease;
        }
        
        .fade-leave-active {
            transition: all .8s cubic-bezier(1.0, 0.5, 0.8, 1.0);
        }
        
        .fade-enter,
        .fade-leave-to {
            transform: translateX(10px);
            opacity: 0;
        }
    </style>
</head>

<body>
    <div id="app">
        <button v-on:click="show = !show">Toggle</button>
        <!-- 控制元素过度效果 -->
        <transition name="fade" mode="">
            <p v-if="show">hello</p>
        </transition>
    </div>
    <script>
        var vm = new Vue({
            el: '#app',
            data: {
                show: TextTrackCue
            },
            methods: {}
        })
    </script>
</body>

</html>
```

### 多个元素的过度

我们之后讨论[多个组件的过渡](https://cn.vuejs.org/v2/guide/transitions.html#多个组件的过渡)，对于原生标签可以使用 `v-if`/`v-else`。最常见的多标签过渡是一个列表和描述这个列表为空消息的元素：

```html
<transition>
  <table v-if="items.length > 0">
    <!-- ... -->
  </table>
  <p v-else>Sorry, no items found.</p>
</transition>
```

可以这样使用，但是有一点需要注意：

> 当有**相同标签名**的元素切换时，需要通过 `key` attribute 设置唯一的值来标记以让 Vue 区分它们，否则 Vue 为了效率只会替换相同标签内部的内容。即使在技术上没有必要，**给在 `` 组件中的多个元素设置 key 是一个更好的实践。**

示例：

```html
<transition>
  <button v-if="isEditing" key="save">
    Save
  </button>
  <button v-else key="edit">
    Edit
  </button>
</transition>
```

在一些场景中，也可以通过给同一个元素的 `key` attribute 设置不同的状态来代替 `v-if` 和 `v-else`，上面的例子可以重写为：

```html
<transition>
  <button v-bind:key="isEditing">
    {{ isEditing ? 'Save' : 'Edit' }}
  </button>
</transition>
```

## 7、路由Vue-Router

### 什么是路由

1. **后端路由：**对于普通的网站，所有的超链接都是URL地址，所有的URL地址都对应服务器上的资源
2. **前端路由：**对于单页面程序来说，主要通过URL中的hash（#号）来实现不同页面之间的切换，同时，hash有一个特点：HTTP请求中不会包含hash相关的内容；所以，单页面程序中的页面跳转主要用hash实现。
3. 在单页面应用程序中，这种通过hash改变来切换页面的方式，称作前端路由；

### vue-router使用

#### 安装

1、直接打开保存下载：https://unpkg.com/vue-router/dist/vue-router.js

在Vue后面加载

```html
<script src="/path/to/vue.js"></script>
<script src="/path/to/vue-router.js"></script>
```

2、NPM

```sh
npm install vue-router
```

如果在一个模块化工程中使用它，必须要通过 `Vue.use()` 明确地安装路由功能：

```js
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
```

#### 介绍

Vue Router 是Vue官方的路由管理器，让构造单页面应用变得易如反掌。包含功能：

* 嵌套的路由、视图表
* 模块化的、基于组件的路由配置
* 路由参数、查询、通配符
* 基于Vue.js过度系统的视图过度效果
* 细粒度的导航控制
* 带有自动激活的CSS class的连接
* HTML5历史模式或hash模式，在IE9中自动降级
* 自定义的滚动条行为

#### 案例:

```html
    <body>
        <div id="app">

            <a href="#/login" >登陆</a>
            <a href="#/register">注册</a>
            <hr>
            <!-- 可以使用 vue-router提供的router-link元素代替a标签-->
            <router-link to="/login">登陆</router-link>
            <router-link to="/register">注册</router-link>

            <!-- 5、这个vue-router提供的元素，专门用来当做占位符的 -->
            <router-view ></router-view>
        </div>
        <script>
            // 2、定义 (路由) 组件
            var login = {
                template: '<h1>登陆组件</h1>'
            }
            var register = {
                template: '<h1>注册组件</h1>'
            }
            // 3、创建一个路由对象，然后传 `routes` 配置规则
            var routerObj = new VueRouter({
                routes: [
                    // 每个路由规则，都是一个对象，这个规则对象，身上，有两个必须的属性
                    // 属性1：是path，表示监听的路由连接地址
                    // 属性2：是component，表示如果路由是前面匹配到的path，则展示component属性对应的组件
                    {path: '/', redirect: 'login'}, // 设置默认登陆进入login组件
                    {path: '/login', component: login},
                    {path: '/register', component: register}
                ],
                linkActiveClass: 'myactive',  // 修改激活router-link标签的类名
            })
            var vm = new Vue({
                el: '#app',
                data: {},
                methods:{},
                // 4、将路由规则对象，注册到vm实例上
                router: routerObj
            })
        </script>
    </body>
```

### watch监听路由改变

```html
<body>
        <div id="app">
            <router-link to="/login">登陆</router-link>
            <router-link to="/register">注册</router-link>

            <router-view></router-view>
        </div>
        <script>
            var login = {
                template: '<h1>这是登陆组件</h1>'
            }
            var register = {
                template: '<h1>这是注册组件</h1>'
            }

            var routerObj = new VueRouter({
                routes: [
                    {path: '/', redirect: 'login'},
                    {path: '/login', component: login},
                    {path: '/register', component: register}
                ]
            })
            var vm = new Vue({
                el: '#app',
                data: {},
                methods:{},
                router: routerObj,
                watch: {
                    '$route.path': function(newVal, oldVal){
                        console.log(newVal + ' --- ' + oldVal)
                    }
                }

            })
        </script>
    </body>
```

# 三、全局API

## 什么事全局API？

全局API并不在构造器里，而是先声明全局变量或者直接在Vue上定义一些新功能，Vue内置了一些全局API。

## Vue.directive自定义指令

*定义全局指令v-focus，其中参数不需要加v-前缀，但调用时必须加上v-前缀*

指令中传递的三个参数

- el: 指令所绑定的元素，可以用来直接操作DOM。
- binding: 一个对象，包含指令的很多信息。
- vnode: Vue编译生成的虚拟节点。

```html
    <body>
        <div id="app">
            <div v-focus="color">{{ num }}</div>
            <p><button @click="add">add</button></p>
            <p><button onclick="unbind()">解绑</button></p>
        </div>
        <script>
            function unbind(){
                vm.$destroy();
            }
            Vue.directive("focus", {
                // 自定义指令有五个生命周期（也叫钩子函数）
                bind: function(el, binding){ // 在元素刚绑定了指令的时候，还没有插入到DOM中去，这个时候调用focus方法没有作用
                    console.log("1 - bind")
                    el.style = "color:" + binding.value
                },
                inserted: function(){ // 当绑定元素插入到DOM中会执行
                    console.log("2 - inserted")
                },
                update: function(){ // 当VNode更新的是否，会执行updated，可能会触发多次
                    console.log("3 - updated")
                },
                componentUpdated: function(){ // 组件更新完成
                    console.log("4 - componentUpdated")
                },
                unbind: function(){ // 解绑
                    console.log("5 - unbind")
                }

            })
            var vm = new Vue({
                el: '#app',
                data: { num: 10, color: 'red'},
                methods:{
                    add(){
                        this.num++;
                    }
                }
            })
        </script>
    </body>

```

## Vue.extend构造器组件

什么是组件？组件的出现，就是为了拆分Vue实例的代码量，能够让我们以不同的组件，来划分不同的功能模块，将来我们需要什么样的功能，就可以去调用对应的组件即可。

组件化和模块化的不同：

* 模块化：是从代码逻辑的角度进行划分的
* 组件化：是从UI界面的角度进行划分的

### 定义组件的方式

> 注意：template中必须只能有一个根元素，建议最外层用<div></div>包裹

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>vue html demo</title>
    <script src="./lib/vue.js"></script>
</head>

<body>
    <!-- 在被控制的#app外面，使用template元素定义组件的HTML模板结构 -->
    <template id="tmp1">
        <!-- 注意只能有一个根元素，建议用div包裹 -->
        <div>
            <h1>这是通过template元素，在外不定义的组件结构。</h1>
        </div>
    </template>
    <div id="app">
        <!-- 如果使用组件，直接把组件的名称以HTML标签的形式，引入到页面中 -->
        <my-com1></my-com1>
        <my-com2></my-com2>
        <my-com3></my-com3>
        <my-com4></my-com4>
        <mycom5></mycom5>
    </div>
    <script>
        // 1.使用Vue.extend来创建全局的Vue组件
        var com1 = Vue.extend({
            template: '<h3>这是使用Vue.extend 创建的组件</h3>'
        })

        // 2.使用Vue.component('组件名称',创建出来的组件模板对象)
        // 注意：如果组件名称使用了驼峰命名如：‘myCom1’，则在引用组件时候，需要把大写的驼峰改为小写的字母同时两个单词之间使用‘-’连接
        // 如果不使用驼峰，则直接拿名称来使用即可
        Vue.component('myCom1', com1)


        // 3.直接引用 Vue.extend
        Vue.component('myCom2', Vue.extend({
            template: '<h3>这是直接使用嵌套 Vue.extend 创建的组件</h3>'
        }))

        // 4.省略Vue.extend
        Vue.component('myCom3', {
            template: '<div><h3>省略Vue.extend 创建的组件</h3><span>123</span></div>'
        })

        // 5.引用外部定义组件
        Vue.component('myCom4', {
            template: '#tmp1'
        })

        // 组件中的data，必须是一个方法，且必须有返回值
        Vue.component('mycom5', {
            template: '<h1>全局组件———— {{ msg }}</h1>',
            data: function() {
                return {
                    msg: '这是组件中data定义的数据'
                }
            }
        })

        var vm = new Vue({
            el: '#app',
            data: {},
            methods: {}
        })
    </script>
</body>

</html>
```

### 组件中的data和methods

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>vue html demo</title>
    <script src="./lib/vue.js"></script>
</head>

<body>
    <template id='tmp1'>
        <div>
            <input type="button" value="加1" @click="increment">
            <h3>{{ count }}</h3>
        </div>
    </template>
    <div id="app">
        <mycom1></mycom1>
        <hr>
        <mycom1></mycom1>
        <hr>
        <mycom1></mycom1>
    </div>
    <script>
        // 全局变量
        var dataobj = {
                count: 0
            }
            // 组件中的data，必须是一个方法，且必须有返回值
        Vue.component('mycom1', {
            template: '#tmp1',
            data: function() {
                return { count: 0 }
            },
            methods: {
                increment() {
                    this.count++
                }
            }
        })

        var vm = new Vue({
            el: '#app',
            data: {},
            methods: {}
        })
    </script>
</body>

</html>
```

### 组件切换方式一

```html
<body>
    <div id="app">
        <a href="" @click.prevent="flag=true">登陆</a>
        <a href="" @click.prevent="flag=false">注册</a>
        <login v-if="flag"></login>
        <register v-else="flag"></register>
    </div>
    <script>
        Vue.component('login', {
            template: '<h3>登陆组件</h3>'
        })
        Vue.component('register', {
            template: '<h3>注册组件</h3>'
        })

        var vm = new Vue({
            el: '#app',
            data: {
                flag: true
            },
            methods: {}
        })
    </script>
</body>
```

### 组件切换方式二

```html
<body>
    <div id="app">
        <a href="" @click.prevent="comName='login'">登陆</a>
        <a href="" @click.prevent="comName='register'">注册</a>
        <!-- vue提供了component元素，来展示对应名称的组件 -->
        <component :is="comName"></component>
    </div>
    <script>
        Vue.component('login', {
            template: '<h3>登陆组件</h3>'
        })
        Vue.component('register', {
            template: '<h3>注册组件</h3>'
        })

        var vm = new Vue({
            el: '#app',
            data: {
                comName: 'login'
            },
            methods: {}
        })
    </script>
</body>
```



### 父组件向子组件传值

```html
<body>
    <div id="app">
        <!-- 父组件可以在引用子组件的时候，通过属性绑定的形式，把需要传递给子组件的数据，用v-bind绑定的形式，传递给子组件内部，供子组件使用 -->
        <com1 v-bind:parentmsg="msg"></com1>
    </div>
    <script>
        var vm = new Vue({
            el: '#app',
            data: {
                msg: '父组件中的数据'
            },
            methods: {},
            components: {
                com1: {
                    template: '<h1>这是子组件 -- {{ parentmsg }}</h1>',
                    props: ['parentmsg'], // 把父组件传递过来的属性名称，放到props数组中就可以使用
                }
            }
        })
    </script>
</body>
```

### 父组件向子组件传方法

![image-20200228221415646](image\Vue\image-20200228221415646.png)



### 使用ref获取DOM元素和组件

获取DOM元素

```html
    <div id="app">
        <input type="button" value="获取元素" @click="getElement" ref="mybtn">
        <h3 ref="myh3">今天天气不错。</h3>
    </div>
    <script>
        var vm = new Vue({
            el: '#app',
            data: {},
            methods: {
                getElement() {
                    // refs是reference 引用
                    alert(this.$refs.myh3.innerText)
                }
            }
        })
    </script>
</body>
```

## Vue.set全局操作

是在构造器外部操作构造器内部的数据、属性或者方法。

```html
    <body>
        <div id="app">
            {{ count }}
        </div>
        <p><button onclick="add()">add</button></p>
        <script>
            function add(){
                // Vue.set(outData, 'count', 2) //用Vue.set改变
                // vm.count++ // 用Vue对象的方法添加
                outData.count++ // 直接操作外部数据
            }
            var outData = {
                count: 1,
                goods: 'car'
            }
            var vm = new Vue({
                el: '#app',
                data: outData,
                methods:{}
            })
        </script>
    </body>
```

为什么使用Vue.set？

由于Javascript的限制，Vue不能自动检测以下变动的数组。

- 当你利用索引直接设置一个项时，vue不会为我们自动更新。
- 当你修改数组的长度时，vue不会为我们自动更新。

看一段代码：

```html
<body>
    <h1>Vue.set 全局操作</h1>
    <hr>
    <div id="app">
        <ul>
            <li v-for=" aa in arr">{{aa}}</li>
        </ul>

    </div>
    <button onclick="add()">外部添加</button>

    <script type="text/javascript">

        function add(){
            console.log("我已经执行了");
           app.arr[1]='ddd';
           //Vue.set(app.arr,1,'ddd');
        }
        var outData={
            arr:['aaa','bbb','ccc']
        };
        var app=new Vue({
            el:'#app',
            data:outData
        })
    </script>
</body>
```



## 钩子函数

一个指令定义对象可以提供如下几个钩子函数（均为可选）：

* `bind`: 只能调用一次，指令第一次绑定到元素时调用。在这里可以进行一次性的初始化设置。
* `inserted`：被绑定元素插入父节点时调用（仅保证父节点存在，但不一定已被插入文档中）。
* `update`：所在组件的VNode更新时调用，但是可能发生在其子VNode更新之前。指令的值可能发生了改变，也可能没有。但是你可以通过比较更新前后的值来忽略不必要的模板更新。
* `componentUpdated`：指令所在组件的VNode及其子VNode全部更新后调用。
* `unbind`：只能调用一次，指令与元素解绑时调用。

**钩子函数的参数**

指令钩子函数会被传入以下参数：

![image-20200223213444978](image\Vue\image-20200223213444978.png)

## template制作模板

```html
    <body>
        <h1>template三种写法</h1>
        <hr>
        <div id="app">
            
        </div>
        <!-- 第二种 -->
        <template id="demo">
            <h2>这是标签模板</h2>
        </template>
        <script type="x-template" id="demo1">
            <h2>这是Script标签模板</h2>
        </script>
        <script>
            var vm = new Vue({
                el: '#app',
                data: {},
                methods:{},
                template: '#demo1'
                // template: `<h2>这是选项模板</h2>`    // 第一种
            })
        </script>
    </body>
```

## component自定义标签

组件是可复用的 Vue 实例，且带有一个名字：在这个例子中是 ``。我们可以在一个通过 `new Vue` 创建的 Vue 根实例中，把这个组件作为自定义元素来使用：

```html
    <body>
        <div id="app">
            <button-counter></button-counter>
        </div>
        <script>
            // 定义一个名为button-counter全局组件
            Vue.component('button-counter', {
                data: function(){
                    return {
                        count: 0
                    }
                },
                template: `<button v-on:click="count++">You clicked me {{ count }} times.</button>`
            })
            
            var vm = new Vue({
                el: '#app',
                data: {},
                methods:{}
            })
        </script>
    </body>
```

因为组件是可复用的 Vue 实例，所以它们与 `new Vue` 接收相同的选项，例如 `data`、`computed`、`watch`、`methods` 以及生命周期钩子等。仅有的例外是像 `el` 这样根实例特有的选项。

### `data`必须是一个函数，有return返回数值

```js
data: function () {
  return {
    count: 0
  }
}
```

如果 Vue 没有这条规则，复用多个按钮，点击任意一个按钮所有的都会变化

### props向组件传递数据

Prop 是你可以在组件上注册的一些自定义 attribute。当一个值传递给一个 prop attribute 的时候，它就变成了那个组件实例的一个属性。为了给博文组件传递一个标题，我们可以用一个 `props` 选项将其包含在该组件可接受的 prop 列表中：

```html
    <body>
        <div id="app">
            <blog-post title="My journey with Vue"></blog-post>
            <blog-post title="Blogging with Vue"></blog-post>
            <blog-post title="Why Vue is so fun"></blog-post>
        </div>
        <script>
            // 通过props向组件传值
            Vue.component('blog-post',{
                props:['title'],
                template: '<h3>{{ title }}</h3>'
            })
            
            var vm = new Vue({
                el: '#app',
                data: {},
                methods:{}
            })
        </script>
    </body>
```

然而在一个典型的应用中，你可能在`data`里有一个博文的数组：

```html
    <body>
        <div id="app">
            <!-- 通过v-bind来动态传递prop -->
            <blog-post
                v-for="post in posts"
                v-bind:key="post.id"
                v-bind:title="post.title"
            ></blog-post>
        </div>
        <script>
            // 通过props向组件传值
            Vue.component('blog-post',{
                props:['title'],
                template: '<h3>{{ title }}</h3>'
            })

            var vm = new Vue({
                el: '#app',
                data: {
                    posts: [
                        { id: 1, title: 'My journey with Vue'},
                        { id: 2, title: 'Blogging with Vue'},
                        { id: 3, title: 'Why Vue is so fun'},
                    ]
                },
                methods:{}
            })
        </script>
    </body>
```

如上所示，可以使用`v-bind`来动态传递prop。这在你一开始不清楚要渲染的具体内容，比如从一个API获取博文列表的时候，是非常有用的。

#### prop的大小写（camelCase vs kebab-case)

HTML中的attribute名是大小写不敏感的，所以浏览器会把所有大写字符解释为小写字符。这意味着你使用DOM中的模板时，camelCase(驼峰命名法)的prop名需要使用其等价的kebab-case（短横线分隔命名）：

```js
Vue.component('blog-post', {
    // 在 JavaScript 中是 camelCase 的
    props: ['postTitle'],
    template: '<h3>{{ postTitle }}</h3>'
})
```

```html
<!-- 在 HTML 中是 kebab-case 的 -->
<blog-post post-title="hello!"></blog-post>
```

重申一次，如果你使用字符串模板，那么这个限制就不存在了。

#### prop类型

到这里，我们看到了以字符串数组形式列出的prop:

```js
props: ['title', 'likes', 'isPublished', 'commentIds', 'author']
```

但是，通常你希望每个prop都有指定的值类型。这是，你可以以对象形式列出prop，这些属性的名称和值分别是prop各自的名称和类型：

```js
props: {
  title: String,
  likes: Number,
  isPublished: Boolean,
  commentIds: Array,
  author: Object,
  callback: Function,
  contactsPromise: Promise // or any other constructor
}
```

这不仅为你的组件提供了文档，还会在它们遇到错误的类型时从浏览器的 JavaScript 控制台提示用户。你会在这个页面接下来的部分看到[类型检查和其它 prop 验证](https://cn.vuejs.org/v2/guide/components-props.html#Prop-验证)。

##### 传递静态或动态prop

像这样，传入一个静态的值：

```
<blog-post title="My journey with Vue"></blog-post>
```

你也知道 prop 可以通过 `v-bind` 动态赋值，例如：

```html
<!-- 动态赋予一个变量的值 -->
<blog-post v-bind:title="post.title"></blog-post>

<!-- 动态赋予一个复杂表达式的值 -->
<blog-post
  v-bind:title="post.title + ' by ' + post.author.name"
></blog-post>
```

在上述两个示例中，我们传入的值都是字符串类型的，但实际上*任何*类型的值都可以传给一个 prop。

##### 传入一个数字

```
<!-- 即便 `42` 是静态的，我们仍然需要 `v-bind` 来告诉 Vue -->
<!-- 这是一个 JavaScript 表达式而不是一个字符串。-->
<blog-post v-bind:likes="42"></blog-post>

<!-- 用一个变量进行动态赋值。-->
<blog-post v-bind:likes="post.likes"></blog-post>
```

##### 传入一个布尔值

```
<!-- 包含该 prop 没有值的情况在内，都意味着 `true`。-->
<blog-post is-published></blog-post>

<!-- 即便 `false` 是静态的，我们仍然需要 `v-bind` 来告诉 Vue -->
<!-- 这是一个 JavaScript 表达式而不是一个字符串。-->
<blog-post v-bind:is-published="false"></blog-post>

<!-- 用一个变量进行动态赋值。-->
<blog-post v-bind:is-published="post.isPublished"></blog-post>
```

##### 传入一个数组

```
<!-- 即便数组是静态的，我们仍然需要 `v-bind` 来告诉 Vue -->
<!-- 这是一个 JavaScript 表达式而不是一个字符串。-->
<blog-post v-bind:comment-ids="[234, 266, 273]"></blog-post>

<!-- 用一个变量进行动态赋值。-->
<blog-post v-bind:comment-ids="post.commentIds"></blog-post>
```

##### 传入一个对象

```
<!-- 即便对象是静态的，我们仍然需要 `v-bind` 来告诉 Vue -->
<!-- 这是一个 JavaScript 表达式而不是一个字符串。-->
<blog-post
  v-bind:author="{
    name: 'Veronica',
    company: 'Veridian Dynamics'
  }"
></blog-post>

<!-- 用一个变量进行动态赋值。-->
<blog-post v-bind:author="post.author"></blog-post>
```

##### 传入一个对象的所有属性

如果你想要将一个对象的所有属性都作为 prop 传入，你可以使用不带参数的 `v-bind` (取代 `v-bind:prop-name`)。例如，对于一个给定的对象 `post`：

```
post: {
  id: 1,
  title: 'My Journey with Vue'
}
```

下面的模板：

```
<blog-post v-bind="post"></blog-post>
```

等价于：

```
<blog-post
  v-bind:id="post.id"
  v-bind:title="post.title"
></blog-post>
```

#### prop验证

我们可以为组件的 prop 指定验证要求，例如你知道的这些类型。如果有一个需求没有被满足，则 Vue 会在浏览器控制台中警告你。这在开发一个会被别人用到的组件时尤其有帮助。

为了定制 prop 的验证方式，你可以为 `props` 中的值提供一个带有验证需求的对象，而不是一个字符串数组。例如：

```
Vue.component('my-component', {
  props: {
    // 基础的类型检查 (`null` 和 `undefined` 会通过任何类型验证)
    propA: Number,
    // 多个可能的类型
    propB: [String, Number],
    // 必填的字符串
    propC: {
      type: String,
      required: true
    },
    // 带有默认值的数字
    propD: {
      type: Number,
      default: 100
    },
    // 带有默认值的对象
    propE: {
      type: Object,
      // 对象或数组默认值必须从一个工厂函数获取
      default: function () {
        return { message: 'hello' }
      }
    },
    // 自定义验证函数
    propF: {
      validator: function (value) {
        // 这个值必须匹配下列字符串中的一个
        return ['success', 'warning', 'danger'].indexOf(value) !== -1
      }
    }
  }
})
```

当 prop 验证失败的时候，(开发环境构建版本的) Vue 将会产生一个控制台的警告。

##### 类型检查

`type` 可以是下列原生构造函数中的一个：

- `String`
- `Number`
- `Boolean`
- `Array`
- `Object`
- `Date`
- `Function`
- `Symbol`

额外的，`type` 还可以是一个自定义的构造函数，并且通过 `instanceof` 来进行检查确认。例如，给定下列现成的构造函数：

```
function Person (firstName, lastName) {
  this.firstName = firstName
  this.lastName = lastName
}
```

你可以使用：

```
Vue.component('blog-post', {
  props: {
    author: Person
  }
})
```

来验证 `author` prop 的值是否是通过 `new Person` 创建的。

### 单个根元素

当构建一个 `` 组件时，你的模板最终会包含的东西远不止一个标题：

```html
<h3>{{ title }}</h3>
```

最最起码，你会包含这篇博文的正文：

```html
<h3>{{ title }}</h3>
<div v-html="content"></div>
```

然而如果你在模板中尝试这样写，Vue 会显示一个错误，并解释道 **every component must have a single root element (每个组件必须只有一个根元素)**。你可以将模板的内容包裹在一个父元素内，来修复这个问题，例如：

```html
<div class="blog-post">
  <h3>{{ title }}</h3>
  <div v-html="content"></div>
</div>
```

看起来当组件变得越来越复杂的时候，我们的博文不只需要标题和内容，还需要发布日期、评论等等。为每个相关的信息定义一个 prop 会变得很麻烦：

```html
<blog-post
  v-for="post in posts"
  v-bind:key="post.id"
  v-bind:title="post.title"
  v-bind:content="post.content"
  v-bind:publishedAt="post.publishedAt"
  v-bind:comments="post.comments"
></blog-post>
```

所以是时候重构一下这个 `` 组件了，让它变成接受一个单独的 `post` prop：

```js
<blog-post
  v-for="post in posts"
  v-bind:key="post.id"
  v-bind:post="post"
></blog-post>
Vue.component('blog-post', {
  props: ['post'],
  template: `
    <div class="blog-post">
      <h3>{{ post.title }}</h3>
      <div v-html="post.content"></div>
    </div>
  `
})
```

上述的这个和一些接下来的示例使用了 JavaScript 的[模板字符串](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Template_literals)来让多行的模板更易读。它们在 IE 下并没有被支持，所以如果你需要在不 (经过 Babel 或 TypeScript 之类的工具) 编译的情况下支持 IE，请使用[折行转义字符](https://css-tricks.com/snippets/javascript/multiline-string-variables-in-javascript/)取而代之。

现在，不论何时为 `post` 对象添加一个新的属性，它都会自动地在 `` 内可用。

# 四、Vue CLI

## 介绍

Vue CLI是一个基于Vue.js进行快速开发的完整系统，提供：

- 通过`@vue/cli`搭建交互式的项目脚手架。
- 通过`@vue/cli`+ `@vue/cli-service-global`快速开始零配置原型开发。
- 一个运行时依赖（`@vue/cli-service`），该依赖：
  - 可升级；
  - 基于webpack构建，并带有合理的默认配置；
  - 可以通过项目内的配置文件进行配置；
  - 可以通过插件进行扩展
- 一个丰富的官方插件集合，集成了前端生态中最好的工具。
- 一套完全图形化的创建和管理Vue.js项目的用户界面

### 该系统的组件

Vue CLI有几个独立的部分——如果你看到了我们的源代码，你会发现在这个仓库里同时管理了多个单独发布的包。

#### CLI

是一个全部安装的npm包，提供了终端里的`vue`命令。它可以通过`vue create`快速创建一个新项目的脚手架，或者直接通过`vue serve`构建新想法的圆形。你也可以通过`vue ui`通过一套图形化界面管理你的所有项目。

#### CLI服务

是一个开发环境依赖。它是一个npm包，局部安装在每个`@vue/cli`创建的项目中。

CLI服务是构建与webpack和webpack-dev-server之上的。它包含了：

- 加载其它CLI插件的核心服务；
- 一个针对绝大部分应用优化过的内部的webpack配置；
- 项目内部的`vue-cli-service`命令，提供`serve`、`build`和`inspect`命令。

#### CLI插件

是向你的Vue项目提供可选功能的npm包，例如Bable/TypeScript转译、ESlint集成、单元测试和end-to-end测试等。Vue CLI插件的名字以`@vue/cli-plugin-`（内建插件）或`vue-cli-plugin-`(社区插件)开头，非常容易使用。

当你在项目内部运行`vue-cli-service`命令时，它会自动解析并加载`package.json`中列出的所有CLI插件。

## 安装

> **Node版本要求**
>
> Vue CLI需要Node.js 8.9或更高版本.你可以使用nvm或nvm-windows在同一台电脑中管理多个Node版本

可以使用下列任一命令安装这个新的包

```sh
npm install -g @vue/cli
# or
yarn global add @vue/cli
```

安装之后，你就可以在命令行中访问`vue`命令。看看是否展示出一份所有可用命令的帮助信息，来验证是否安装成功。

```sh
vue --version
```

## 基础

### 快速原型开发

你可以使用`vue serve` 和 `vue build`命令对单个`*.vue`文件进行快速原型开发，不过这需要先额外安装一个全局的扩展：

```sh
npm install -g @vue/cli-service-global
```

`vue serve`的缺点就是它需要安装全局依赖，这使得它在不同机器上的一致性不能得到保证。因此这只适用于快速原型开发。

#### vue serve

```sh
Usage: serve [options] [entry]
在开发环境模式下零配置为.js 或 .vue文件启动一个服务器

Options：
	-o, --open 打开浏览器
	-c, --copy 将本地URL复制到剪切板
	-h, --help 输出用法信息
```

你所需要的紧紧是一个`App.vue`文件：

```vue
<template>
	<hi>Hello!</hi>
</template>
```

然后在这个`App.vue`文件所在的目录下运行：

```sh
vue serve
```

`vue serve` 使用了和 `vue create` 创建的项目相同的默认设置 (webpack、Babel、PostCSS 和 ESLint)。它会在当前目录自动推导入口文件——入口可以是 `main.js`、`index.js`、`App.vue` 或 `app.vue` 中的一个。你也可以显式地指定入口文件：

```bash
vue serve MyComponent.vue
```

如果需要，你还可以提供一个 `index.html`、`package.json`、安装并使用本地依赖、甚至通过相应的配置文件配置 Babel、PostCSS 和 ESLint。

#### vue build

```text
Usage: build [options] [entry]

在生产环境模式下零配置构建一个 .js 或 .vue 文件


Options:

  -t, --target <target>  构建目标 (app | lib | wc | wc-async, 默认值：app)
  -n, --name <name>      库的名字或 Web Components 组件的名字 (默认值：入口文件名)
  -d, --dest <dir>       输出目录 (默认值：dist)
  -h, --help             输出用法信息
```

你也可以使用 `vue build` 将目标文件构建成一个生产环境的包并用来部署：

```bash
vue build MyComponent.vue
```

`vue build` 也提供了将组件构建成为一个库或一个 Web Components 组件的能力。查阅[构建目标](https://cli.vuejs.org/zh/guide/build-targets.html)了解更多。

### 创建一个项目

#### vue create

运行以下命令来创建一个新项目：

```sh
vue create hello-world
```

你会被提示选取一个preset。你可以选默认的包含了基本的Babel + ESLint设置的preset，也可以选“手动选择特性”来选取需要的特性。

![image-20200309142738249](image\Vue\image-20200309142738249.png)

这个默认的设置非常适合快速创建一个新项目的原型，而手动设置则提供了更多的选项，它们是面向生产的项目更加需要的。

![image-20200309152301220](image\Vue\image-20200309152301220.png)

`vue create` 命令有一些可选项，你可以通过运行以下命令进行探索：

```bash
vue create --help
用法：create [options] <app-name>

创建一个由 `vue-cli-service` 提供支持的新项目


选项：

  -p, --preset <presetName>       忽略提示符并使用已保存的或远程的预设选项
  -d, --default                   忽略提示符并使用默认预设选项
  -i, --inlinePreset <json>       忽略提示符并使用内联的 JSON 字符串预设选项
  -m, --packageManager <command>  在安装依赖时使用指定的 npm 客户端
  -r, --registry <url>            在安装依赖时使用指定的 npm registry
  -g, --git [message]             强制 / 跳过 git 初始化，并可选的指定初始化提交信息
  -n, --no-git                    跳过 git 初始化
  -f, --force                     覆写目标目录可能存在的配置
  -c, --clone                     使用 git clone 获取远程预设选项
  -x, --proxy                     使用指定的代理创建项目
  -b, --bare                      创建项目时省略默认组件中的新手指导信息
  -h, --help                      输出使用帮助信息
```

## 使用图形化界面

你也可以通过 `vue ui` 命令以图形化界面创建和管理项目：

```bash
vue ui
```

上述命令会打开一个浏览器窗口，并以图形化界面将你引导至项目创建的流程。

![图形化界面预览](image\Vue\ui-new-project.png)

## 组件使用

### axios路由和nprogress进度条

1. 安装axios和nprogress运行依赖

2. 在main.js中引入

   ```js
   import axios from 'axios'
   import NProgress from 'nprogress'
   import 'nprogress/nprogress.css'
   
   // 配置请求根路径
   axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/'
   
   // 在request拦截器中，展示进度条NProgress.start()
   
   // axios请求拦截
   axios.interceptors.request.use(config => {
     NProgress.start()
     // 为请求头对象，添加Token验证的Authorization字段
     config.headers.Authorization = window.sessionStorage.getItem('token')
     return config
   })
   
   // 在response拦截器中，隐藏进度条NProgress.done()
   axios.interceptors.response.use(config => {
     NProgress.done()
     return config
   })
   Vue.prototype.$http = axios
   ```

   

3. 



# vue-cli 3.0之跨域问题

## 开发环境

两种配置：

​	**1：先在项目的根目录下新建 vue.config.js 文件**
​	**2：在module.exports内设置devServer来处理代理**

**不设置 axios 的 baseURL 或者 赋值为空字符串**

```js
axios.defaults.baseURL = '';
```

**在 proxy 中可以写多个代理地址**

```js
  devServer: {
      proxy: {
          '/user': {
              //要访问的跨域的api的域名
              target: 'http://www.user.com',
              // 是否启用websockets
              ws: true,
              // 使用的是http协议则设置为false，https协议则设置为true
              secure:false,
              changOrigin: true,
              pathRewrite: {
                 '^/user': ''
              }
          },
          '/order': {
              target: 'http://www.order.com',
              ws: true,
              secure:false,
              changOrigin: true,
          },
          '/pay': {
              target: 'http://www.pay.com',
              ws: true,
              secure:false,
              changOrigin: true,
          },
      }
  }

```

proxy 的匹配规则是根据正则匹配 如上 /user、/order、/pay 如果 请求地址 包含 此字符串，就算匹配成功，一旦匹配成功就 不会 继续向下匹配
如：

请求 /user/info 则匹配 target 为 http://www.user.com
请求 /order/user/list 则匹配 target 也为 http://www.user.com
请求 /order/pay/user/list 则匹配 target 也为 http://www.user.com
请求 /pay/order/list 则匹配 target 为 http://www.order.com
如果我们需要以 某个路径开头 才算 匹配成功，则可以改成：'^/user'、'^/account'、'^/pay'

## 打包上线

使用nginx做了代理解决跨域问题。

### 1、windows安装

官方网站下载地址：

```
https://nginx.org/en/download.html
```

![image-20200524222713864](image\Vue\image-20200524222713864.png)

解压后，进入目录

### 2、配置conf文件

```XML
       server {
        listen       8001;
        server_name  127.0.0.1;
		
        location / {
            root   E:\Projects\hixngl\hixngl-frontground-pro\dist;
            index  index.html;
        }
		
		location /api/ {
			rewrite ^/api/(.*)$ /$1 break;
            proxy_pass  http://127.0.0.1:8087;
        }

		location /baiduapi/ {
			rewrite ^/baiduapi/(.*)$ /$1 break;
            proxy_pass  https://aip.baidubce.com;
        }
    }
```

![image-20200531171021388](image\Vue\\image-20200531171021388.png)

### 3、启动nginx

1） 直接双击该目录下的nginx.exe，即可启动nginx服务器

2） 命令行计入该文件夹，执行nginx命令，也会直接启动nginx服务器

```
E:\project\nginx-1.17.10> nginx
```

### 4、访问nginx

打开浏览器，输入地址：http://localhost，访问页面，出现如下页面表示访问成功

### 5、停止nginx

命令行进入nginx根目录，执行如下命令，停止服务器：

```
# 强制停止nginx服务器，如果有未处理的数据，丢弃
E:\project\nginx-1.17.10> nginx -s stop

# 优雅的停止nginx服务器，如果有未处理的数据，等待处理完成之后停止
E:\project\nginx-1.17.10> nginx -s quit
```

# 五、使用笔记

## 组件对比

### `watch`、`computed`和`methods`之间的对比

1. `watch`属性的结果会被缓存，除非依赖的响应式属性变化才会重新计算。主要当做属性来使用
2. `methods`方法表示一个具体的操作，主要书写业务逻辑；
3. `watch`一个对象，键是需要观察的表达式，值是回调函数。主要用来监听某些特定的数据变化，从而进行某些具体的业务逻辑操作；可以看做是`watch`和`methods`的结合体；

## nodejs使用

### cnpm的安装

淘宝团队做的国内镜像，因为npm的服务器位于国外可能会影响安装。淘宝镜像与官方同步频率目前为 10分钟 一次以保证尽量与官方服务同步。

安装：命令提示符执行
 `npm install cnpm -g --registry=https://registry.npm.taobao.org`

 `cnpm -v` 来测试是否成功安装

### nrm的安装

作用：提供一些常用的NPM包镜像地址，能够让我们快速切换安装包时候的服务器地址；

1. 使用`npm i nrm -g`全局安装`nrm`
2. 使用`nrm ls`查看所有可用的镜像源地址以及当前使用的镜像源
3. 使用`nrm use cnpm`或`nrm use taobao`切换不同的镜像源地址；

### webpack安装

作用：

1. 能够处理JS文件的相互依赖关系；
2. 能够处理JS的兼容问题，把高级的、浏览器不支持的语法转为低级的浏览器能支持的语法

安装`npm i webpack webpack-cli -g`

对一个文件打包`webpack --mode=development .\src\main.js -o .\dist\bundle.js`

### webpack-dev-server安装

作用：自动打包，自动刷新页面

安装`npm i webpack-dev-server -g`

使用：

1. 在`package.json`中添加`"dev": "webpack-dev-server"`

   ```json
     "scripts": {
       "test": "echo \"Error: no test specified\" && exit 1",
       "dev": "webpack-dev-server"
     },
   ```

2. 添加`webpack.config.js`文件

   ```js
   const path = require('path'); //调用node.js中的路径
   module.exports = {
       entry: {
           index: './src/main.js' //需要打包的文件
       },
       output: {
           filename: 'bundle.js', //输入的文件名是什么，生成的文件名也是什么
           path: path.resolve(__dirname, './dist/') //指定生成的文件目录
       },
       mode: "development", //开发模式，没有对js等文件压缩，默认生成的是压缩文件
       devServer: {
           open: true,    // 自动打开浏览器
           port: 8080,    // 设置启动时的端口
           contentBase: 'src', // 指定托管的根目录
           hot: true   //  启动热更新
       }
   }
   ```

3. 启动`npm run dev`

## Element UI

### 使用阿里icon单色

1. 在[阿里icon网页](https://www.iconfont.cn/?spm=a313x.7781069.1998910419.d4d0a486a)查找图标添加到项目里

   ![image-20200410103258635](image\Vue\image-20200410103258635.png)

2. 解压，拷贝下面文件到vue项目中

   ![image-20200410103431718](image\Vue\image-20200410103431718.png)

3. 在项目中的main.js文件，导入iconfont.css样式

   ```js
   // 导入阿里icon图标
   import './assets/iconfont/iconfont.css'
   ```

4. 在class使用iconfont + 对应的FontClass名引用图标

   ```
   <i class="iconfont el-icon-alitousufenxi"></i>
   ```

### 使用阿里icon彩色

1. 下载的压缩文件里的iconfont.js文件拷入项目

2. 在main.js导入iconfont.js

   ```js
   // 导入阿里icon图标
   import './assets/iconfont/iconfont.js'
   ```

3. 引用svg图标

   ```html
                   <svg class="icon" aria-hidden="true">
                     <use xlink:href="#el-icon-alitouxiang2"></use>
                   </svg>
   ```

4. 设置css样式

   ```css
     .icon {
       margin: 10px 0px;
       position: relative;
       width: 60%;
       height: 60%;
     }
   ```

   

## V-charts

在使用 echarts 生成图表时，经常需要做繁琐的数据类型转化、修改复杂的配置项，v-charts 的出现正是为了解决这个痛点。基于 Vue2.0 和 echarts 封装的 v-charts 图表组件，只需要统一提供一种对前后端都友好的数据格式设置简单的配置项，便可轻松生成常见的图表，官网[v-charts](https://v-charts.js.org/#/)

1、安装

```
npm i v-charts echarts -S
```

![image-20200412145642788](image\Vue\image-20200412145642788.png)

2、项目main.js中引入v-charts

```js
// 引入Vcharts
import VCharts from 'v-charts'
Vue.use(VCharts)
```

3、项目中使用，挂载到一个div标签上

```js
<template>
  <el-container>
    <el-header >
      vue接入v-charts使用教程
    </el-header>
    <el-main>
      <div style="display: inline; float: left; width: 50%; height: 800px; border: #3b4151 solid 1px">
        <ve-line :data="chartData"  height="400px"></ve-line>
        <ve-histogram :data="chartData"  height="400px"></ve-histogram>
      </div>
      <div style="display: inline;float: right; width: 50%; border: #3b4151 solid 1px">
        <ve-pie :data="chartData" height="400px"></ve-pie>
        <ve-radar :data="chartData" height="400px"></ve-radar>
      </div>
    </el-main>
  </el-container>



</template>

<script>
  export default {
    name: 'simple',
    data: function () {
      return {
        chartData: {
          columns: ['日期', '访问用户', '下单用户', '下单率'],
          rows: [
            { '日期': '1/1', '访问用户': 1393, '下单用户': 1093, '下单率': 0.32 },
            { '日期': '1/2', '访问用户': 3530, '下单用户': 3230, '下单率': 0.26 },
            { '日期': '1/3', '访问用户': 2923, '下单用户': 2623, '下单率': 0.76 },
            { '日期': '1/4', '访问用户': 1723, '下单用户': 1423, '下单率': 0.49 },
            { '日期': '1/5', '访问用户': 3792, '下单用户': 3492, '下单率': 0.323 },
            { '日期': '1/6', '访问用户': 4593, '下单用户': 4293, '下单率': 0.78 }
          ]
        }
      }
    }
  }
</script>

<style scoped>
  .el-header{
    background-color: #1F5DEA;
    color: white;
    text-align: center;
    line-height: 60px;
    font-size: 24px;
  }
</style>


```

## Echarts

参考文档：http://www.imooc.com/article/283898

### 配置组件

#### 安装

```
npm install echarts
```

#### 在plugins下新建文件夹echarts

![image-20200416102214096](image\Vue\image-20200416102214096.png)

#### 新建自定义utils.js工具

```js
/* eslint-disable no-unused-expressions */

'use strict'
import echarts from 'echarts'

// 颜色集
// const colorList = ['#6481F1', '#4CDA99', '#F0CA00', '#FF7362', '#A689EC', '#12BCE4']
const colorList = ['#5bc2e7', '#6980c5', '#70dfdf', '#FF7362', '#A689EC', '#024b51', '#0780cf', '#765005']
// const colorList = ['#05f8d6', '#0082fc', '#fdd845', '#22ed7c', '#09b0d3', '#1d27c9', '#f9e264', '#f47a75', '#009db2', '#024b51', '#0780cf', '#765005']
const colorMap = ['#05f8d6', '#fdd845', '#f23e26']

export default (el, classify, ...arg) => {
  const instance = echarts.init(el)
  instance.setOption(allOptions[`${classify}Options`](...arg))
  return instance
}
const allOptions = {

  // 折线图,柱状图，区域图
  baseOptions: (dataArray, type, title, areaType, colors) => {
    // 是否留白
    var isArea = true

    dataArray.series.forEach((item, i) => {
      // 默认设置
      item.stack = ''
      item.areaStyle = null
      // 放大标点
      item.itemStyle = {
        normal: {
          borderWidth: 3
        }
      }
      // line图的线加粗
      item.lineStyle = {
        normal: {
          width: 3
        }
      }
      item.smooth = false

      if (type === 'baseline' || type === 'line') {
        item.type = 'line'
        // line图的线加粗加阴影
        item.lineStyle = {
          normal: {
            width: 3,
            shadowColor: colorList[i] + 52,
            shadowBlur: 5,
            shadowOffsetY: 7
          }
        }
        if (type === 'line') {
          // 把折线改成曲线
          item.smooth = true
        }
      } else if (type === 'bar' || type === 'stackedbar') {
        item.type = 'bar'
        if (type === 'stackedbar') {
          item.stack = '总量'
        }
      } else if (type === 'area' || type === 'stackedarea') {
        item.type = 'line'
        isArea = false
        item.type = 'line'
        item.areaStyle = {}

        // 把折线改成曲线
        item.smooth = true

        if (type === 'stackedarea') {
          // 堆叠
          item.stack = '总量'
        }
      }
    })

    return {
      color: colors !== undefined && colors.length > 0 ? colors : colorList,
      backgroundColor: 'white',
      title: {
        text: title
        // textStyle:{
        //     color:"#ccc"
        // }
      },
      tooltip: {
        trigger: 'axis'
      },
      legend: {
        // data: ['最高温度', '最低温度'],
        textStyle: {
          //   color:"#fff"
        }
      },
      // 右侧工具栏按钮
      toolbox: {
        show: true,
        feature: {
          mark: {
            show: true
          },
          dataView: {
            show: true,
            readOnly: false
          },
          magicType: {
            show: true,
            type: ['line', 'bar']
          },
          // restore: {
          //   show: true
          // },
          saveAsImage: {
            show: true
          }
        }
      },
      xAxis: [{
        type: areaType === 'y' ? 'value' : 'category',
        data: areaType === 'y' ? null : dataArray.axisData,
        boundaryGap: isArea, // 坐标轴两边是否留白
        axisTick: {
          show: true // 隐藏X轴刻度
        },
        axisLabel: {
          show: true,
          textStyle: {
            // color: "#ebf8ac" //X轴文字颜色
          }
        }
      }],
      yAxis: [{
        type: areaType === 'y' ? 'category' : 'value',
        data: areaType === 'y' ? dataArray.axisData : null,
        // name: "℃'",
        nameTextStyle: {
          // color: "#ebf8ac"
        },
        axisLabel: {
          // formatter: '{value}℃'
        },
        // 坐标轴在 grid 区域中的分隔线。
        splitLine: {
          lineStyle: {
            type: 'solid'
            // type: 'dashed'
            // color: '#DDD'
          }
        }
      }],
      series: dataArray.series
    }
  },
  pieOptions: (dataArray, type, title, areaType, colors) => {
    dataArray.series.forEach((item, i) => {
      item.type = 'pie'
      if (type === 'radiusPie') {
        item.radius = ['50%', '70%']
      } else {
        item.radius = [0, '75%']
      }
    })
    return {
      color: colors !== undefined && colors.length > 0 ? colors : colorList,
      backgroundColor: 'white',
      title: {
        text: title,
        subtext: ''
        // textStyle:{
        //     color:"#ccc"
        // }
      },
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
      },
      legend: {
        // data: ['最高温度', '最低温度'],
        type: 'scroll',
        orient: 'vertical',
        left: 10,
        top: 20
      },
      // 右侧工具栏按钮
      toolbox: {
        show: true,
        feature: {
          mark: {
            show: true
          },
          dataView: {
            show: true,
            readOnly: false
          },
          magicType: {
            show: true,
            type: ['line', 'bar']
          },
          // restore: {
          //   show: true
          // },
          saveAsImage: {
            show: true
          }
        }
      },
      series: dataArray.series
    }
  },
  radarOptions: (dataArray, type, title, areaType, colors) => {
    // debugger
    dataArray.series.forEach((item, i) => {
      item.type = 'radar'
      if (type === 'radarArea') {
        item.areaStyle = {}
      } else {
        item.areaStyle = null
      }
    })
    return {
      color: colors !== undefined && colors.length > 0 ? colors : colorList,
      backgroundColor: 'white',
      title: {
        text: title
        // textStyle:{
        //     color:"#ccc"
        // }
      },
      tooltip: {},
      radar: {
        shape: 'circle',
        name: {
            textStyle: {
                color: '#303133',
                // backgroundColor: '#999',
                borderRadius: 3,
                padding: [3, 5]
            }
        },
        indicator: dataArray.indicator
      },
      // 右侧工具栏按钮
      toolbox: {
        show: true,
        feature: {
          mark: {
            show: true
          },
          dataView: {
            show: true,
            readOnly: false
          },
          magicType: {
            show: true,
            type: ['line', 'bar']
          },
          // restore: {
          //   show: true
          // },
          saveAsImage: {
            show: true
          }
        }
      },
      series: dataArray.series
    }
  },
  gaugeOptions: (dataArray, type, title, areaType, colors) => {
    // debugger
    dataArray.series.forEach((item, i) => {
      item.type = 'gauge'
      item.detail = {
        formatter: '{value}%'
      }
    })
    return {
      title: {
        text: title
        // textStyle:{
        //     color:"#ccc"
        // }
      },
      tooltip: {
        formatter: '{a} <br/>{b} : {c}%'
      },
      // 右侧工具栏按钮
      toolbox: {
        show: true,
        feature: {
          mark: {
            show: true
          },
          dataView: {
            show: true,
            readOnly: false
          },
          magicType: {
            show: true,
            type: ['line', 'bar']
          },
          // restore: {
          //   show: true
          // },
          saveAsImage: {
            show: true
          }
        }
      },
      series: dataArray.series
    }
  },
  mapOptions: (dataArray, type, title, areaType, colors) => {
    // debugger
    dataArray.series.forEach((item, i) => {
      item.type = 'map'
      item.itemStyle = { // 地图区域的多边形 图形样式。
          emphasis: { // 高亮状态下的样试
            label: {
              show: true
            }
          }
        }
      item.zoom = 1 // 放大比例
      item.roam = true
      item.label = { // 图形上的文本标签，可用于说明图形的一些数据信息
          show: true
      }
      // 海南地图过小解决方案
      if (item.map === '海南' || item.map === 'hainan') {
        item.center = [109.844902, 19.1492]
        item.layoutCenter = ['50%', '50%']
        item.layoutSize = '800%'
      } else {
        item.center = undefined
        item.layoutCenter = undefined
        item.layoutSize = undefined
      }
    })
    return {
      backgroundColor: '', // 背景颜色
      title: {
        text: title,
        // subtext: 'China',
        color: '#fff',
        // sublink: 'http://www.pm25.in',
        x: 'center'
      },
      // 是视觉映射组件，用于进行『视觉编码』，也就是将数据映射到视觉元素（视觉通道）。
      visualMap: {
        min: dataArray.min, // 最小值
        max: dataArray.max, // 最大值
        calculable: true, // 是否显示拖拽用的手柄（手柄能拖拽调整选中范围）。
        inRange: {
          color: colors !== undefined && colors.length > 0 ? colors : colorMap
        },
        textStyle: {
          color: '#fff'
        }
      },
      // 提示框，鼠标移入
      tooltip: {
        show: true, // 鼠标移入是否触发数据
        trigger: 'item' // 出发方式
        // formatter: '{b}-销售数量：{c}'
      },
      series: dataArray.series
    }
  },
  mapPointOptions: (dataArray, type, title, areaType, colors) => {
    // debugger
    var myGeo = {}
    myGeo.map = dataArray.mapName
    myGeo.itemStyle = {
      normal: {
        // color: 'transparent', //地图背景色
        borderColor: 'rgba(147, 235, 248, 1)', //省市边界线
        borderWidth: 1,
          areaColor: {
            type: 'radial',
            x: 0.5,
            y: 0.5,
            r: 0.8,
            colorStops: [{
              offset: 0,
              color: 'rgba(147, 235, 248, 0)' // 0% 处的颜色
            }, {
              offset: 1,
              color: 'rgba(147, 235, 248, .2)' // 100% 处的颜色
            }],
            globalCoord: false // 缺省为 false
          },
          shadowColor: 'rgba(128, 217, 248, 1)',
          // shadowColor: 'rgba(255, 255, 255, 1)',
          shadowOffsetX: -2,
          shadowOffsetY: 2,
          shadowBlur: 10
      },
      emphasis: {
        color: 'rgba(37, 43, 61, .5)' //悬浮背景
      }
    }
    myGeo.zoom = 1 // 放大比例
    myGeo.roam = true
    myGeo.label = {
      normal: {
        show: false,
        textStyle: {
          color: '#fff'
        }
      },
      emphasis: {
        show: false,
        textStyle: {
          color: '#fff'
        },
        emphasis: {
          show: true
        }
      }
    }
    // 海南地图过小解决方案
    if (dataArray.mapName === '海南' || dataArray.mapName === 'hainan') {
      myGeo.center = [109.844902, 19.1492]
      myGeo.layoutCenter = ['50%', '50%']
      myGeo.layoutSize = '800%'
    } else {
      myGeo.center = undefined
      myGeo.layoutCenter = undefined
      myGeo.layoutSize = undefined
    }

    // 把数据放入series
    dataArray.series.push(dataArray.pointData)
    return {
      backgroundColor: '#013954', // 背景颜色
      title: {
        text: title,
        // subtext: 'China',
        color: '#fff'
        // sublink: 'http://www.pm25.in',
        // x: 'center'
      },
      // 是视觉映射组件，用于进行『视觉编码』，也就是将数据映射到视觉元素（视觉通道）。
      visualMap: {
        min: dataArray.min, // 最小值
        max: dataArray.max, // 最大值
        calculable: true, // 是否显示拖拽用的手柄（手柄能拖拽调整选中范围）。
        inRange: {
          color: colors !== undefined && colors.length > 0 ? colors : colorMap
        },
        textStyle: {
          color: '#fff'
        }
      },
      geo: myGeo,
      // 提示框，鼠标移入
      tooltip: {
        show: true, // 鼠标移入是否触发数据
        trigger: 'item', // 出发方式
        formatter: "{a} : <br> {c}"
      },
      series: dataArray.series
    }
  }

}
```

#### 新建myEcharts.vue组件

```vue
<template>
  <div :class="`echart-box echart-${classify}`"></div>
</template>

<script>
    // 引入自定义echarts工具
  import init from './utils.js'
    // 引入海南地图hainan.js
  import '../../assets/js/hainan'

  export default {
    props: {
      classify: { // 应用那个类型的图标
        type: String
      },
      dataArray: {  // 柱状图渲染数据,数据的每一项都是Number
        type: Object
      },
      type: {
        type: String
      },
      title: {
        type: String
      },
      areaType: {
        type: String
      },
      colors:{
        type: Array
      }
    },
    data() {
      return {
        myChart: null,
        sonRefresh: false
      }
    },
    beforeDestroy() {
      // 销毁图表实例，避免内存溢出
      this.myChart.dispose && this.myChart.dispose();
    },
    mounted() {
      // 调用utils来绘制图形
      this.myChart = init(this.$el, ...Object.values(this.$props))
    }
  }
</script>

<style scoped>
  .echart-box {
    width: 100%;
    height: 100%;
    /* height: 300px; */
  }
</style>
```

#### 在main.js中引入全局自定义echarts组件

```js
import MyEcharts from '@/plugins/echarts/myEcharts.vue'
```

### 基本使用

```vue
<myEcharts classify="base" :dataArray="chartData" type="line" title="" areaType="x" :colors="['#6481F1', '#4CDA99', '#F0CA00']"></myEcharts>
```

  classify表示使用的那个options

​    chartData表示引用的数据

​    type表示图形样式

​    title表示标题

​    areaType设置区域在x轴还是y轴，默认为x轴

​    colors可以指定颜色集

​    如果要显示数据标签可以在series里对应集合加label: { show: true }



![image-20200416105026837](image\Vue\image-20200416105026837.png)

![image-20200416105054286](image\Vue\image-20200416105054286.png)

![image-20200416105112001](image\Vue\image-20200416105112001.png)

![image-20200416105139042](image\Vue\image-20200416105139042.png)

### 折线图

```vue
<template>
  <div class="chart-div">
    <myEcharts classify="base" :dataArray="chartData" type="baseline" title="" :colors="[]"></myEcharts>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        chartData: {
          'series': [{
            name: '访问用户',
            data: [120, 132, 101, 134, 90, 230, 210]
          },
          {
            name: '下单用户',
            data: [20, 82, 91, 34, 90, 30, 10]
          },
          {
            name: '用户',
            data: [150, 232, 201, 154, 190, 330, 410]
          }],
          'axisData': ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
        },
      }
    }
</script>
```

### 曲线图

```vue
<template>
  <div class="chart-div">
    <myEcharts classify="base" :dataArray="chartData" type="line" title="" :colors="[]"></myEcharts>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        chartData: {
          'series': [{
            name: '访问用户',
            data: [120, 132, 101, 134, 90, 230, 210]
          },
          {
            name: '下单用户',
            data: [20, 82, 91, 34, 90, 30, 10]
          },
          {
            name: '用户',
            data: [150, 232, 201, 154, 190, 330, 410]
          }],
          'axisData': ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
        },
      }
    }
</script>
```

### 区域图

```vue
<template>
  <div class="chart-div">
    <myEcharts classify="base" :dataArray="chartData" type="area" title="" :colors="[]"></myEcharts>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        chartData: {
          'series': [{
            name: '访问用户',
            data: [120, 132, 101, 134, 90, 230, 210]
          },
          {
            name: '下单用户',
            data: [20, 82, 91, 34, 90, 30, 10]
          },
          {
            name: '用户',
            data: [150, 232, 201, 154, 190, 330, 410]
          }],
          'axisData': ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
        },
      }
    }
</script>
```

### 堆叠区域图

```vue
<template>
  <div class="chart-div">
    <myEcharts classify="base" :dataArray="chartData" type="stackedarea" title="" :colors="[]"></myEcharts>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        chartData: {
          'series': [{
            name: '访问用户',
            data: [120, 132, 101, 134, 90, 230, 210]
          },
          {
            name: '下单用户',
            data: [20, 82, 91, 34, 90, 30, 10]
          },
          {
            name: '用户',
            data: [150, 232, 201, 154, 190, 330, 410]
          }],
          'axisData': ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
        },
      }
    }
</script>
```

### 折线图

```vue
<template>
  <div class="chart-div">
    <myEcharts classify="base" :dataArray="chartData" type="baseline" title="" :colors="[]"></myEcharts>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        chartData: {
          'series': [{
            name: '访问用户',
            data: [120, 132, 101, 134, 90, 230, 210]
          },
          {
            name: '下单用户',
            data: [20, 82, 91, 34, 90, 30, 10]
          },
          {
            name: '用户',
            data: [150, 232, 201, 154, 190, 330, 410]
          }],
          'axisData': ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
        },
      }
    }
</script>
```

### 柱状图

```vue
<template>
  <div class="chart-div">
    <myEcharts classify="base" :dataArray="chartData" type="bar" title="" :colors="[]"></myEcharts>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        chartData: {
          'series': [{
            name: '访问用户',
            data: [120, 132, 101, 134, 90, 230, 210]
          },
          {
            name: '下单用户',
            data: [20, 82, 91, 34, 90, 30, 10]
          },
          {
            name: '用户',
            data: [150, 232, 201, 154, 190, 330, 410]
          }],
          'axisData': ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
        },
      }
    }
</script>
```

### 堆叠柱状图

```vue
<template>
  <div class="chart-div">
    <myEcharts classify="base" :dataArray="chartData" type="stackedbar" title="" :colors="[]"></myEcharts>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        chartData: {
          'series': [{
            name: '访问用户',
            data: [120, 132, 101, 134, 90, 230, 210]
          },
          {
            name: '下单用户',
            data: [20, 82, 91, 34, 90, 30, 10]
          },
          {
            name: '用户',
            data: [150, 232, 201, 154, 190, 330, 410]
          }],
          'axisData': ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
        },
      }
    }
</script>
```

### 正负极

```vue
<template>
  <div class="chart-div">
    <myEcharts classify="base" :dataArray="chartData1" type="bar" title="" :colors="[]"></myEcharts>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        chartData1: {
          'series': [
            {
              name: '利润',
              label: {
                show: true,
                position: 'inside'
              },
              data: [200, 170, 240, 244, 200, 220, 210]
            },
            {
              name: '收入',
              label: {
                show: true
              },
              data: [320, 302, 341, 374, 390, 450, 420]
            },
            {
              name: '支出',
              label: {
                show: true,
                position: 'left'
              },
              data: [-120, -132, -101, -134, -190, -230, -210]
            }
          ],
          'axisData': ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
        },
      }
    }
</script>
```

### 正负极显示在y轴

```vue
<template>
  <div class="chart-div">
    <myEcharts classify="base" :dataArray="chartData2" type="bar" title="" areaType="y" :colors="[]"></myEcharts>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        chartData2: {
          'series': [
            {
              name: '利润',
              label: {
                show: true,
                position: 'inside'
              },
              data: [200, 170, 240, 244, 200, 220, 210]
            },
            {
              name: '收入',
              label: {
                show: true
              },
              data: [320, 302, 341, 374, 390, 450, 420]
            },
            {
              name: '支出',
              label: {
                show: true,
                position: 'left'
              },
              data: [-120, -132, -101, -134, -190, -230, -210]
            }
          ],
          'axisData': ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
        },
      }
    }
</script>
```

### 基础饼图

```vue
<template>
  <div class="chart-div">
    <myEcharts classify="pie" :dataArray="chartData3" type="pie" title="" :colors="[]"></myEcharts>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        chartData3: {
          'series': [
            {
              data: [{ name: 'Apples', value: 70 }, 
              { name: 'Strawberries', value: 68 }, 
              { name: 'Bananas', value: 48 }, 
              { name: 'Oranges', value: 40 }, 
              { name: 'Pears', value: 32 }, 
              { name: 'Pineapples', value: 27 }, 
              { name: 'Grapes', value: 18 }]
            }
          ],
        }
      }
    }
</script>
```

### 环形图

```vue
<template>
  <div class="chart-div">
    <myEcharts classify="pie" :dataArray="chartData3" type="radiusPie" title="" :colors="[]"></myEcharts>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        chartData3: {
          'series': [
            {
              data: [{ name: 'Apples', value: 70 }, 
              { name: 'Strawberries', value: 68 }, 
              { name: 'Bananas', value: 48 }, 
              { name: 'Oranges', value: 40 }, 
              { name: 'Pears', value: 32 }, 
              { name: 'Pineapples', value: 27 }, 
              { name: 'Grapes', value: 18 }]
            }
          ],
        },
      }
    }
</script>
```

### 基础雷达图

```vue
<template>
  <div class="chart-div">
    <myEcharts classify="radar" :dataArray="chartData4" type="radar" title="" :colors="[]"></myEcharts>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        chartData4: {
          'series': [{
            data: [
              {
                value: [4300, 10000, 28000, 35000, 50000, 19000],
                name: '预算分配（Allocated Budget）'
              },
              {
                value: [5000, 14000, 28000, 31000, 42000, 21000],
                name: '实际开销（Actual Spending）'
              }
            ]
          }],
          'indicator': [
            { name: '销售（sales）', max: 6500 },
            { name: '管理（Administration）', max: 16000 },
            { name: '信息技术（Information Techology）', max: 30000 },
            { name: '客服（Customer Support）', max: 38000 },
            { name: '研发（Development）', max: 52000 },
            { name: '市场（Marketing）', max: 25000 }
          ]
        },
      }
    }
</script>
```

### 区域雷达图

```vue
<template>
  <div class="chart-div">
    <myEcharts classify="radar" :dataArray="chartData5" type="radarArea" title="" :colors="[]"></myEcharts>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        chartData5: {
          'series': [{
            data: [
              {
                name: '降水量',
                value: [2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 75.6, 82.2, 48.7, 18.8, 6.0, 2.3],
              },
              {
                name: '蒸发量',
                value: [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 35.6, 62.2, 32.6, 20.0, 6.4, 3.3]
              }
            ]
          }]
        },
      }
    },
    created() {
      this.month()
    }
    methods: {
      month() {
        var res = [];
        for (var i = 1; i <= 12; i++) {
          res.push({ text: i + '月', max: 100 });
        }
        this.chartData5.indicator = res
      }
    }
</script>
```

### 仪表盘

```vue
<template>
  <div class="chart-div">
    <myEcharts classify="gauge" :dataArray="chartData6" type="gauge" title="" :colors="[]"></myEcharts>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        chartData6: {
          'series': [{
            data: [
              {
                name: '完成率',
                value: 50,
              }
            ]
          }]
        },
      }
    }
</script>
```

### 地图区域渲染

```vue
<template>
  <div class="chart-div">
    <myEcharts classify="map" :dataArray="chartData7" type="map" title="" :colors="[]"></myEcharts>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        chartData7: {
          'min': 0,
          'max': 600,
          'series': [{
            // 要和hainan.js里registerMap保持一致
            map: '海南',
            data: [
              { name: '海口市', value: Math.round(Math.random() * 500) },
              { name: '琼海市', value: Math.round(Math.random() * 500) },
              { name: '定安市', value: Math.round(Math.random() * 500) },
              { name: '文昌市', value: Math.round(Math.random() * 500) },
              { name: '澄迈市', value: Math.round(Math.random() * 500) },
              { name: '儋州市', value: Math.round(Math.random() * 500) },
              { name: '万宁市', value: Math.round(Math.random() * 500) },
              { name: '陵水市', value: Math.round(Math.random() * 500) },
              { name: '保亭市', value: Math.round(Math.random() * 500) },
              { name: '三亚市', value: Math.round(Math.random() * 500) },
              { name: '东方市', value: Math.round(Math.random() * 500) },
            ]
          }]
        },
      }
    }
</script>
```

### 地图撒点

```vue
<template>
  <div class="chart-div">
    <myEcharts classify="mapPoint" :dataArray="chartData7" type="map" title="" :colors="[]"></myEcharts>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        chartData8: {
          'mapName': '海南',
          'min': 0,
          'max': 10,
          'series': [{
            "name": "pm2.5",
            "type": "scatter",
            "coordinateSystem": "geo",
            "data": [{
              "value": [109.854902, 19.1492, 3] }, {
              "value": [109.834902, 19.2492, 1] }, {
              "value": [109.855902, 19.3492, 2] }, {
              "value": [109.738902, 19.4492, 1] }, {
              "value": [109.744902, 19.1192, 4] }, {
              "value": [109.644902, 19.1292, 9] }, {
              "value": [109.944902, 19.1392, 2] }, {
              "value": [109.34902, 19.1692, 1] }, {
              "value": [109.544902, 19.1792, 8]
            }],
            "label": {
              "normal": {
                "formatter": "{b}",
                "position": "right",
                "show": true
              },
              "emphasis": {
                "show": true
              }
            },
            "itemStyle": {
              "normal": {
                "color": "#05C3F9"
              }
            }
          }]
        }
      }
    }
</script>
```

## vue-jsonp

参考https://www.jianshu.com/p/e4379e6690c3

#### 前端要实现跨域的一种方法就是使用jsonp，不过今天不是来介绍jsonp的。我们来看看在vue中如何使用jsonp。

首先介绍今天的主角：vue-jsonp，在这里先贴上github地址：[https://github.com/LancerComet/vue-jsonp#readme](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2FLancerComet%2Fvue-jsonp%23readme)
 这个包已经帮我们封装好jsonp了

##### 让我们尝试在vue中使用它实现jsonp

- 第一步：在vue项目中安装上vue-jsonp



```undefined
npm i vue-jsonp --save
```

- 然后在main.js上导入并安装



```jsx
import Vue from 'vue'//vue-jsonp依赖于vue，要先安装vue
import VueJsonp from 'vue-jsonp'
Vue.use(VueJsonp)
```

- 为了能在每个组件都使用，把vue-jsonp提供的方法再封装为单文件，创建一个common.js文件



```jsx
exports.install = function(Vue,options){
// 参数：method（API名），data（请求参数对象）
  Vue.prototype.jsp = function(method,data){
        // baseUrl就是服务器地址
        const baseUrl = 'http://192.168.191.1:8000/';
        let url = `${baseUrl}${method}?`;
        // jsonp请求参数和get方法类似，都是params的方式拼接
        for(let item in data){
            url += `&${item}=${data[item]}`;
        }
        return this.$jsonp(url);
    };
}
```

- 同样，为了能全局使用，先在main.js上手动安装这个common.js



```jsx
import Common from './main/common/common.js'
Vue.use(Common);
```

- 好了，现在咱们可以在组件中使用封装过的jsonp方法了



```kotlin
methods:{
      bookIfo(){
          // 传入API名以及请求参数对象
          this.jsp('bookIfo',{book:this.current}).then((data)=>{
                this.book = data;
          }).catch((err)=>{
              console.log(err);
          })
      },
}
```

## axios+promise实际开发用法

参考文档:https://www.cnblogs.com/lalalagq/p/9901185.html

### **axios特点**

1.从浏览器中创建 XMLHttpRequests
2.从 node.js 创建 http 请求
3.支持 Promise API
4.拦截请求和响应 （就是有interceptor）
5.转换请求数据和响应数据
6.取消请求
7.自动转换 JSON 数据
8.客户端支持防御 XSRF

### **安装axios和qs**

```
npm i axios --save
npm i qs--save
```

### **创建项公共模块API**

我是用vue-cli创建的项目在src->util->api.js(公共请求模块js)

**引入axios和qs**

```
import axios from 'axios'
```

有时候向后端发送数据，后端无法接收,考虑使用qs模块

```
import qs from 'qs'
```

**判定开发模式**

```
if (process.env.NODE_ENV == 'development') {    

    axios.defaults.baseURL = '/api';

}else if (process.env.NODE_ENV == 'debug') {    

    axios.defaults.baseURL = 'http://v.juhe.cn';

}else if (process.env.NODE_ENV == 'production') {    

    axios.defaults.baseURL = 'http://v.juhe.cn';

}
```

**全局设置头部信息**

```
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
```

**全局设置超时时间**

```
axios.defaults.timeout = 10000;
```

**请求路由拦截**

在请求发出去之前，可以做一些判断，看是否是合法用户

```
axios.interceptors.request.use(function (config) {
    // 一般在这个位置判断token是否存在
    return config;
   }, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});
```

**响应拦截**

```
axios.interceptors.response.use(function (response){
     // 处理响应数据
    if (response.status === 200) {            
        return Promise.resolve(response);        
    } else {            
        return Promise.reject(response);        
    }
    }, function (error){
    // 处理响应失败
    return Promise.reject(error);
});
```

### 封装请求方法

**使用ES6模块化export导出import导入**

在ES6前， 前端就使用RequireJS或者seaJS实现模块化， requireJS是基于AMD规范的模块化库， 而像seaJS是基于CMD规范的模块化库， 两者都是为了为了推广前端模块化的工具，现在ES6自带了模块化，不过现代浏览器对模块(module)支持程度不同， 需要使用babelJS， 或者Traceur把ES6代码转化为兼容ES5版本的js代码;

**get请求**

```
export function get(url, params){    
    return new Promise((resolve, reject) =&gt;{        
        axios.get(url, {            
            params: params        
        }).then(res =&gt; {
            resolve(res.data);
        }).catch(err =&gt;{
            reject(err.data)        
        })    
    });
}
```

**post请求**

```
export function post(url, params) {
    return new Promise((resolve, reject) =&gt; {
         axios.post(url, qs.stringify(params))
        .then(res =&gt; {
            resolve(res.data);
        })
        .catch(err =&gt;{
            reject(err.data)
        })
    });
}
```

### 实际使用

在main.js中引入js

```
import {get,post} from './utils/api'
//将方法挂载到Vue原型上
Vue.prototype.get = get;
Vue.prototype.post = post;
```

**配置请求环境**
这里通过devServer请求代理
当在请求过程中有/api字符串会自动转换为**目标服务器地址(target)**

```
devServer: {
   historyApiFallback: true,
   hot: true,
   inline: true,
   stats: { colors: true },
   proxy: {
     //匹配代理的url
     '/api': {
          // 目标服务器地址
             target: 'http://v.juhe.cn',
             //路径重写
             pathRewrite: {'^/api' : ''},
             changeOrigin: true,
             secure: false,
           }
      },
    disableHostCheck:true
   }
```

**这是请求聚合数据的接口为列子**

```
this.get('/toutiao/index?type=top&amp;key=秘钥',{})
    .then((res)=&gt;{
        if(res.error_code===0){
            resolve(res);
        }else{
            //这里抛出的异常被下面的catch所捕获
            reject(error);
        }
    })
    .catch((err)=&gt;{
        console.log(err)
    })
```

**返回数据**

![img](image\Vue\\1504257-20181103162617210-125777262.png)

**使用promise**
1.比如用户想请求url1接口完后再调url2接口

```
    var promise = new Promise((resolve,reject)=&gt;{
        let url1 = '/toutiao/index?type=top&amp;key=秘钥'
        this.get(url,{})
        .then((res)=&gt;{
            resolve(res);
        })
        .catch((err)=&gt;{
            console.log(err)
        })
    });
    promise.then((res)=&gt;{
        let url2 = '/toutiao/index?type=top&amp;key=秘钥'
        this.get(ur2,{})
        .then((res)=&gt;{
            //只有当url1请求到数据后才会调用url2，否则等待
            resolve(res);
        })
        .catch((err)=&gt;{
            console.log(err)
        })
    })
```

**Promise对象**

```
Promise有三种状态
pending: 等待中，或者进行中，表示还没有得到结果
resolved: 已经完成，表示得到了我们想要的结果，可以继续往下执行
rejected: 也表示得到结果，但是由于结果并非我们所愿，因此拒绝执(用catch捕获异常)
```

这三种状态不受外界影响，而且状态只能从pending改变为resolved或者rejected，并且不可逆(顾名思义承诺的后的东西怎么又能返回呢)。在Promise对象的构造函数中，将一个函数作为第一个参数。而这个函数，就是用来处理Promise的状态变化

这里要注意，不管是then或者catch返回的都是一个新的Promise实例！而每个Primise实例都有最原始的Pending（进行中）到Resolve（已完成），或者Pending（进行中）到Reject（已失败）的过程

**Promise基本用法**

Promise.all()用法

```
var p = Promise.all([p1, p2, p3]);
```

all()接受数组作为参数。p1,p2,p3都是Promise的实例对象，p要变成Resolved状态需要p1，p2，p3状态都是Resolved，如果p1,p2,p3至少有一个状态是Rejected，p就会变成Rejected状态

Promise.race()用法

```
var p = new Promise( [p1,p2,p3] )
```

只要p1、p2、p3之中有一个实例率先改变状态，p的状态就跟着改变。那个率先改变的 Promise 实例的返回值，就传递给p的回调函数,p的状态就会改变Resolved状态

Promise resolve()用法

```
Promise.resolve('foo')
// 等价于
new Promise(resolve =&gt; resolve('foo'))
```

有时需要将现有对象转为Promise对象，Promise.resolve方法就起到这个作用.

Promise reject()用法

```
Promise.reject('foo')
// 等价于
new Promise(reject =&gt; reject('foo'))
```

Promise.reject(reason)方法也会返回一个新的 Promise 实例，该实例的状态为rejected。但是Promise.reject()方法的参数，会原封不动地作为reject的理由，变成后续方法的参数。这一点与Promise.resolve方法不一致。

# 六、网站推荐

## UI框架

### Element-UI

饿了么前端团队推出的一款基于Vue.js 2.0 的桌面端UI框架,手机端有对应框架是 Mint UI。

地址：https://element.eleme.cn/#/zh-CN/component/installation

### iView

iView 是一套基于 Vue.js 的开源 UI 组件库，主要服务于 PC 界面的中后台产品。

使用单文件的 Vue 组件化开发模式 基于 npm + webpack + babel 开发，支持 ES2015 高质量、功能丰富 友好的 API ，自由灵活地使用空间，由TalkingData开发维护。

地址：http://v1.iviewui.com/

### Ant Design of Vue

阿里的ant desgin样式库，整体上还是比较简洁素气：

 地址：https://www.antdv.com/docs/vue/introduce-cn/

## 学习视频

**[Vue实战项目：电商管理系统（Element-UI）](https://www.bilibili.com/video/av74592164?p=129)**
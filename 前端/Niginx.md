转载：https://blog.csdn.net/yujing1314/article/details/107000737

# Nginx知识网结构图

![image-20210124152505623](image/Niginx/image-20210124152505623.png)

Nginx是一个高性能的HTTP和反向代理服务器，特点是占用内存少，并发能力强，事实上nginx的并发能力确实在同类型的网页服务器中表现较好

nginx专为性能优化而开发，性能是其最重要的要求，十分注重效率，有报告nginx能支持高达50000个并发连接数

# 基本概念



## 正向代理

正向代理：局域网中的电脑用户想要直接访问网络是不可行的，只能通过代理服务器来访问，这种代理服务就被称为正向代理。

![image-20210124152523205](image/Niginx/image-20210124152523205.png)

## 反向代理

反向代理：客户端无法感知代理，因为客户端访问网络不需要配置，只要把请求发送到反向代理服务器，由反向代理服务器去选择目标服务器获取数据，然后再返回到客户端，此时反向代理服务器和目标服务器对外就是一个服务器，暴露的是代理服务器地址，隐藏了真实服务器IP地址

![image-20210124152534450](image/Niginx/image-20210124152534450.png)

## 负载均衡

客户端发送多个请求到服务器，服务器处理请求，有一些可能要与数据库进行狡猾，服务器处理完毕之后，再将结果返回给客户端

普通请求和响应过程
![image-20210124152616996](image/Niginx/image-20210124152616996.png)
但是随着信息数量增长，访问量和数据量飞速增长，普通架构无法满足现在的需求

我们首先想到的是升级服务器配置，可以由于摩尔定律的日益失效，单纯从硬件提升性能已经逐渐不可取了，怎么解决这种需求呢？

我们可以增加服务器的数量，构建集群，将请求分发到各个服务器上，将原来请求集中到单个服务器的情况改为请求分发到多个服务器，也就是我们说的负载均衡

**图解负载均衡**

![image-20210124153118439](image/Niginx/image-20210124153118439.png)、

假设有15个请求发送到代理服务器，那么由代理服务器根据服务器数量，平均分配，每个服务器处理5个请求，这个过程就叫做负载均衡

## 动静分离

为了加快网站的解析速度，可以把动态页面和静态页面交给不同的服务器来解析，加快解析的速度，降低由单个服务器的压力

**动静分离之前的状态：**

![image-20210124153221898](image/Niginx/image-20210124153221898.png)

**动静分离之后：**

![image-20210124153243099](image/Niginx/image-20210124153243099.png)

# 安装

参考：https://blog.csdn.net/yujing1314/article/details/97267369

# 常用命令

```shell
cd /usr/local/nginx/sbin

#查看版本
./nginx -v

#启动
./nginx

#关闭（两种方式，推荐使用第二个）
./nginx -s stop
./nginx -s quit

#重新加载nginx配置
./nginx -s reload
```



# 配置文件

配置文件分三部分组成

### **全局块**

从配置文件开始到events块之间，主要是设置一些影响nginx服务器整体运行的配置指令

并发处理服务的配置，值越大，可以支持的并发处理量越多，但是会受到硬件、软件等设备的制约

![image-20210124154955034](image/Niginx/image-20210124154955034.png)

### **http块**

诸如反向代理和负载均衡都在此配置

```yaml
http {
    include       mime.types;
    default_type  application/octet-stream;

    #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    #                  '$status $body_bytes_sent "$http_referer" '
    #                  '"$http_user_agent" "$http_x_forwarded_for"';

    #access_log  logs/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    #keepalive_timeout  0;
    keepalive_timeout  65;

    #gzip  on;

    server {
        listen       80;
        server_name  localhost;

        #charset koi8-r;

        #access_log  logs/host.access.log  main;

        location / {
            root   html;
            index  index.html index.htm;
        }

        #error_page  404              /404.html;

        # redirect server error pages to the static page /50x.html
        #
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }

        # proxy the PHP scripts to Apache listening on 127.0.0.1:80
        #
        #location ~ \.php$ {
        #    proxy_pass   http://127.0.0.1;
        #}

        # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
        #
        #location ~ \.php$ {
        #    root           html;
        #    fastcgi_pass   127.0.0.1:9000;
        #    fastcgi_index  index.php;
        #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
        #    include        fastcgi_params;
        #}

        # deny access to .htaccess files, if Apache's document root
        # concurs with nginx's one
        #
        #location ~ /\.ht {
        #    deny  all;
        #}
    }
}
```

**location指令说明**

- 该语法用来匹配url，语法如下

```yaml
location[ = | ~ | ~* | ^~] url{

}
```

1. =:用于不含正则表达式的url前，要求字符串与url严格匹配，匹配成功就停止向下搜索并处理请求
2. ~：用于表示url包含正则表达式，并且区分大小写。
3. ~*：用于表示url包含正则表达式，并且不区分大瞎写
4. ^~：用于不含正则表达式的url前，要求ngin服务器找到表示url和字符串匹配度最高的location后，立即使用此location处理请求，而不再匹配
5. 如果有url包含正则表达式，不需要有~开头标识

# 实例

## 配置反向代理1

目的：在浏览器地址栏输入地址www.123.com跳转linux系统tomcat主页面

具体实现
先配置tomcat：因为比较简单，此处不再赘叙
并在windows访问

具体流程

![image-20210124155720934](image/Niginx/image-20210124155720934.png)

配置

![image-20210124155803190](image/Niginx/image-20210124155803190.png)

## 配置反向代理2

1.目标
访问http://192.168.25.132:9001/edu/ 直接跳转到192.168.25.132:8080
访问http://192.168.25.132:9001/vod/ 直接跳转到192.168.25.132:8081

2.准备
配置两个tomcat，端口分别为8080和8081，都可以访问，端口修改配置文件即可。

3.配置

![image-20210124160032859](image/Niginx/image-20210124160032859.png)

实现了同一个端口代理，通过edu和vod路径的切换显示不同的页面

## 负载均衡

负载均衡用于从“upstream”模块定义的后端服务器列表中选取一台服务器接受用户的请求。一个最基本的upstream模块是这样的，模块内的server是服务器列表：

```nginx
#动态服务器组
upstream dynamic_zuoyu {
        server localhost:8080;  #tomcat 8.5
        server localhost:8081;  #tomcat 8.5
        server localhost:8082;  #tomcat 8.5
        server localhost:8083;  #tomcat 8.5
```

在upstream模块配置完成后，要让指定的访问反向代理到服务器列表：

```nginx
#其他页面反向代理到tomcat容器
 location ~ .*$ {
           index index.jsp index.html;
            proxy_pass http://dynamic_zuoyu;
  }
```

　这就是最基本的负载均衡实例，但这不足以满足实际需求；目前Nginx服务器的upstream模块支持6种方式的分配：

| 轮询               | 默认方式        |
| ------------------ | --------------- |
| weight             | 权重方式        |
| ip_hash            | 依据ip分配方式  |
| least_conn         | 最少连接方式    |
| fair（第三方）     | 响应时间方式    |
| url_hash（第三方） | 依据URL分配方式 |

在这里，只详细说明Nginx自带的负载均衡策略，第三方不多描述。

### 1、轮询

最基本的配置方法，上面的例子就是轮询的方式，它是upstream模块默认的负载均衡默认策略。每个请求会按时间顺序逐一分配到不同的后端服务器。

有如下参数：

| fail_timeout | 与max_fails结合使用。                                        |
| ------------ | ------------------------------------------------------------ |
| max_fails    | 设置在fail_timeout参数设置的时间内最大失败次数，如果在这个时间内，所有针对该服务器的请求都失败了，那么认为该服务器会被认为是停机了， |
| fail_time    | 服务器会被认为停机的时间长度,默认为10s。                     |
| backup       | 标记该服务器为备用服务器。当主服务器停止时，请求会被发送到它这里。 |
| down         | 标记服务器永久停机了。                                       |

 `注意：`

- 在轮询中，如果服务器down掉了，会自动剔除该服务器。
- 缺省配置就是轮询策略。
- 此策略适合服务器配置相当，无状态且短平快的服务使用。

### 2、weight

权重方式，在轮询策略的基础上指定轮询的几率。例子如下：

```nginx
#动态服务器组
upstream dynamic_zuoyu {
    server localhost:8080   weight=2;  #tomcat 8.5
    server localhost:8081;  #tomcat 8.5
    server localhost:8082   backup;  #tomcat 8.5
    server localhost:8083   max_fails=3 fail_timeout=20s;  #tomcat 8.5
}
```

在该例子中，weight参数用于指定轮询几率，weight的默认值为1,；weight的数值与访问比率成正比，比如Tomcat 7.0被访问的几率为其他服务器的两倍。

　　注意：

- 权重越高分配到需要处理的请求越多。
- 此策略可以与least_conn和ip_hash结合使用。
- 此策略比较适合服务器的硬件配置差别比较大的情况。

### 3、ip_hash

　指定负载均衡器按照基于客户端IP的分配方式，这个方法确保了相同的客户端的请求一直发送到相同的服务器，以保证session会话。这样每个访客都固定访问一个后端服务器，`可以解决session不能跨服务器的问题`。

```nginx
#动态服务器组
upstream dynamic_zuoyu {
        ip_hash;    #保证每个访客固定访问一个后端服务器
        server localhost:8080   weight=2;  #tomcat 8.5
        server localhost:8081;  #tomcat 8.5
        server localhost:8082;  #tomcat 8.5
        server localhost:8083   max_fails=3 fail_timeout=20s;  #tomcat 8.5
    }
```

　　注意：

- 在nginx版本1.3.1之前，不能在ip_hash中使用权重（weight）。
- ip_hash不能与backup同时使用。
- 此策略适合有状态服务，比如session。
- 当有服务器需要剔除，必须手动down掉。

### 4、least_conn

把请求转发给连接数较少的后端服务器。轮询算法是把请求平均的转发给各个后端，使它们的负载大致相同；但是，有些请求占用的时间很长，会导致其所在的后端负载较高。这种情况下，least_conn这种方式就可以达到更好的负载均衡效果。

```nginx
#动态服务器组
    upstream dynamic_zuoyu {
        least_conn;    #把请求转发给连接数较少的后端服务器
        server localhost:8080   weight=2;  #tomcat 8.5
        server localhost:8081;  #tomcat 8.5
        server localhost:8082 backup;  #tomcat 8.5
        server localhost:8083   max_fails=3 fail_timeout=20s;  #tomcat 8.5
    }
```

注意：

- 此负载均衡策略适合请求处理时间长短不一造成服务器过载的情况。

### 5、第三方策略

第三方的负载均衡策略的实现需要安装第三方插件。

**①fair**

按照服务器端的响应时间来分配请求，响应时间短的优先分配。

```nginx
#动态服务器组
upstream dynamic_zuoyu {
        server localhost:8080;  #tomcat 8.5
        server localhost:8081;  #tomcat 8.5
        server localhost:8082;  #tomcat 8.5
        server localhost:8083;  #tomcat 8.5
        fair;    #实现响应时间短的优先分配
    }
```

**②url_hash**

　按访问url的hash结果来分配请求，使每个url定向到同一个后端服务器，要配合缓存命中来使用。同一个资源多次请求，可能会到达不同的服务器上，导致不必要的多次下载，缓存命中率不高，以及一些资源时间的浪费。而使用url_hash，可以使得同一个url（也就是同一个资源请求）会到达同一台服务器，一旦缓存住了资源，再此收到请求，就可以从缓存中读取。

```nginx
#动态服务器组
upstream dynamic_zuoyu {
        hash $request_uri;    #实现每个url定向到同一个后端服务器
        server localhost:8080;  #tomcat 8.5
        server localhost:8081;  #tomcat 8.5
        server localhost:8082;  #tomcat 8.5
        server localhost:8083;  #tomcat 8.5
 }
```

## 动静分离实战

**什么是动静分离**
把动态请求和静态请求分开，不是讲动态页面和静态页面物理分离，可以理解为nginx处理静态页面，tomcat处理动态页面

动静分离大致分为两种：一、纯粹将静态文件独立成单独域名放在独立的服务器上，也是目前主流方案；二、将动态跟静态文件混合在一起发布，通过nginx分开

**动静分离图析**

![image-20210124161725247](image/Niginx/image-20210124161725247.png)

## nginx高可用

如果nginx出现问题

![image-20210124162053123](image/Niginx/image-20210124162053123.png)

解决办法

![image-20210124162106758](image/Niginx/image-20210124162106758.png)

前期准备

1. 两台nginx服务器
2. 安装keepalived
3. 虚拟ip

安装keepalived

```shell
[root@192 usr]# yum install keepalived -y
[root@192 usr]# rpm -q -a keepalived
keepalived-1.3.5-16.el7.x86_64
```

修改配置文件

```shell
[root@192 keepalived]# cd /etc/keepalived
[root@192 keepalived]# vi keepalived.conf 
```

分别将如下配置文件复制粘贴，覆盖掉keepalived.conf
虚拟ip为192.168.25.50

对应主机ip需要修改的是
smtp_server 192.168.25.147（主）smtp_server 192.168.25.147（备）
state MASTER（主） state BACKUP（备）

启动

```shell
[root@192 sbin]# systemctl start keepalived.service
```

![image-20210124162340360](image/Niginx/image-20210124162340360.png)

访问虚拟ip成功
![image-20210124162414861](image/Niginx/image-20210124162414861.png)
关闭主机147的nginx和keepalived，发现仍然可以访问

# 原理解析

![image-20210124162432282](image/Niginx/image-20210124162432282.png)

如下图，就是启动了一个master，一个worker，master是管理员，worker是具体工作的进程

![image-20210124162508345](image/Niginx/image-20210124162508345.png)

worker如何工作

![image-20210124162554639](image/Niginx/image-20210124162554639.png)

- worker数应该和CPU数相等
- 一个master多个worker可以使用热部署，同时worker是独立的，一个挂了不会影响其他的
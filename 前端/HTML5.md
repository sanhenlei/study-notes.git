# HTML5教程

## HTML5 是如何起步的？

HTML5 是 W3C 与 [WHATWG](https://baike.baidu.com/item/WHATWG/5803339?fr=aladdin) 合作的结果,WHATWG 指 Web Hypertext Application Technology Working Group。。

WHATWG 致力于 web 表单和应用程序，而 W3C 专注于 XHTML 2.0。在 2006 年，双方决定进行合作，来创建一个新版本的 HTML。

HTML5 中的一些有趣的新特性：

- 用于绘画的 canvas 元素
- 用于媒介回放的 video 和 audio 元素
- 对本地离线存储的更好的支持
- 新的特殊内容元素，比如 article、footer、header、nav、section
- 新的表单控件，比如 calendar、date、time、email、url、search

------

## HTML5 <!DOCTYPE>

<!doctype> 声明必须位于 HTML5 文档中的第一行,使用非常简单:

<!DOCTYPE html>
------

## 最小的HTML5文档

下面是一个简单的HTML5文档：

```html
<!DOCTYPE html>        
<html>              
<head>          
<title>文档标题</title>            
</head>           
<body>          
  文档内容......         
</body>                
</html>
```

------

## HTML5的改进

- 新元素
- 新属性
- 完全支持 CSS3
- Video 和 Audio
- 2D/3D 制图
- 本地存储
- 本地 SQL 数据
- Web 应用

------

## HTML5 多媒体

使用 HTML5 你可以简单的在网页中播放 视频(video)与音频 (audio) 。

- HTML5 [](https://www.w3cschool.cn/html5/html5-video.html)
- HTML5 [](https://www.w3cschool.cn/html5/html5-audio.html)

------

## HTML5 应用

使用 HTML5 你可以简单地开发应用

- 本地数据存储
- 访问本地文件
- 本地 SQL 数据
- 缓存引用
- Javascript 工作者
- XHTMLHttpRequest 2

------

## HTML5 图形

使用 HTML5 你可以简单的绘制图形:

- 使用 [](https://www.w3cschool.cn/html5/q2ybmfle.html) 元素
- 使用内联 [SVG](https://www.w3cschool.cn/html5/html5-svg.html)
- 使用 [CSS3 2D](https://www.w3cschool.cn/css3/g94ojfll.html)/[CSS 3D](https://www.w3cschool.cn/css3/g5lhsflm.html)

------

## HTML5 使用 CSS3

- 新选择器
- 新属性
- 动画
- 2D/3D 转换
- 圆角
- 阴影效果
- 可下载的字体

# HTML5 浏览器支持

## HTML5 浏览器支持

------

目前市面上的浏览器有很多版本，你可以让一些较早的浏览器（不支持HTML5）支持 HTML5。

------

## HTML5 浏览器支持

现代的浏览器都支持 HTML5。

此外，所有浏览器，包括旧的和最新的，对无法识别的元素会作为内联元素自动处理。

正因为如此，你可以 **"教会"** 浏览器处理 **"未知"** 的 HTML 元素。

![Note](image\HTML5\1456312537617181.jpg)    甚至你可以教会 IE6  (Windows XP 2001) 浏览器处理未知的 HTML 元素。 


## 将 HTML5 元素定义为块元素

HTML5 定了 8 个新的 HTML **语义（semantic）** 元素。所有这些元素都是**块级** 元素。

为了能让旧版本的浏览器正确显示这些元素，你可以设置 CSS 的 **display** 属性值为 **block**:

```html
header, section, footer, aside, nav, main, article, figure {
  display: block;
}
```

## 为 HTML 添加新元素

你可以为 HTML 添加新的元素。

该实例向 HTML 添加的新的元素，并为该元素定义样式，元素名为**<myHero>** ：

```html
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>为 HTML 添加新元素</title>
  <script>document.createElement("myHero")</script>
  <style>
  myHero {
    display: block;
    background-color: #ddd;
    padding: 50px;
    font-size: 30px;
  }
  </style>
</head>

<body>

<h1>我的第一个标题</h1>

<p>我的第一个段落。</p>

<myHero>我的第一个新元素</myHero>

</body>
</html>
```

JavaScript 语句 **document.createElement("myHero")** 是为了为 IE 浏览器添加新的元素。

------

## Internet Explorer 浏览器问题

你可以使用以上的方法来为 IE 浏览器添加 HTML5 元素，但是：

![Note](image\HTML5\1456312537617181.jpg)    Internet Explorer 8 及更早 IE 版本的浏览器不支持以上的方式。

我们可以使用 Sjoerd Visscher 创建的 "HTML5 Enabling JavaScript", " **shiv**" 来解决该问题:

<!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js" rel="external nofollow" ></script>
<![endif]-->

以上代码是一个注释，作用是在 IE 浏览器的版本小于 IE9 时将读取 html5.js 文件，并解析它。

**注意：**国内用户请使用百度静态资源库（Google 资源库在国内不稳定）：

<!--[if lt IE 9]>

 <script src="http://apps.bdimg.com/libs/html5shiv/3.7/html5shiv.min.js" rel="external nofollow" rel="external nofollow" ></script>
<![endif]-->

针对IE浏览器html5shiv 是比较好的解决方案。html5shiv主要解决HTML5提出的新的元素不被IE6-8识别，这些新元素不能作为父节点包裹子元素，并且不能应用CSS样式。

------

## 完美的 Shiv 解决方案

```html
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Styling HTML5</title>
  <!--[if lt IE 9]>
  <script src="http://apps.bdimg.com/libs/html5shiv/3.7/html5shiv.min.js" rel="external nofollow" rel="external nofollow" ></script>
  <![endif]-->
</head>

<body>

<h1>我的第一篇文章</h1>

<article>
学技术，从W3Cschool开始！
</article>

</body>
</html>
```

html5shiv.js 引用代码必须放在**<head>**元素中，因为 IE 浏览器在解析 HTML5 新元素时需要先加载该文件。

# HTML5 新元素
# 开发时常用

## 解析 url 参数

通过 `replace` 方法获取 `url` 中的参数键值对，可以快速解析 `get` 参数。

```
const q = {};
location.search.replace(/([^?&=]+)=([^&]+)/g,(_,k,v)=>q[k]=v);
console.log(q);
```



# DIV相关

## DIV并排显示的几种方法

### 第一种使用display的inline属性

```html
<div style="width:300px; height:auto; float:left; display:inline">AAAA</div>
<div style="width:300px; height:auto; float:left; display:inline">BBBB</div>
```

效果：

<div style="width:50%; height:20px;background:#ccc; float:left; display:inline">AAAA</div><div style="width:50%; height:20px;background:#888; float:left; display:inline">BBBB</div>
### 第二种使用float

```html
<div id= "main ">
<div id= "left " style="float:left;border:1px solid red; padding:10px;"> 1111 </div>
<div id= "right " style="float:left;border:1px solid red; padding:10px;"> 2222</div>
<!-- 如果不用clear屬性可能會出現瀏覽器不兼容問題,clear設這元素周圍沒有浮動元素 -->
<div style="clear:both"></div>
</div>
```

效果：

<div id= "main ">
<div id= "left " style="float:left;border:1px solid red; padding:10px;"> 1111 </div>
<div id= "right " style="float:left;border:1px solid red; padding:10px;"> 2222</div>
<!-- 如果不用clear屬性可能會出現瀏覽器不兼容問題,clear設這元素周圍沒有浮動元素 -->
<div style="clear:both"></div>
</div>

## CSS+DIV四种布局方式

### 一、 常规流式布局

元素按照自身的常规显示方式显示，有两个特点：

1. 元素按照自身HTML元素定义的位置显示（怎么写的怎么显示）
2. 元素按照自身的常规显示特性显示

比如块级元素垂直排列，行级元素水平排列。

以前没学布局时，其实是用的就是常规流布局。

### 二、 浮动（顺便讲解布局步骤）

具体代码：
左浮动 float:left;
右浮动 float:right;

我们知道编程一般都是有套路的，使用CSS+DIV布局也不例外，大体分为以下四步：
布局步骤：
一、画效果图
在纸上先画出我们想要的页面的具体显示的框架。比如我们想要把一个网页分成上中下三个部分。
二、使用DIV进行分割
拿本例来说我们网页整体分为上中下三部分，所以我们可以使用三个div来先大体分割一下该网页.
三、使用CSS来控制DIV布局
使用CSS样式来控制布局的具体宽、高，并使用显著的背景标注，在需要修改时可以清楚的看到该模块。
四、使用以上三步进行细分
在每一模块都要通过以上三步具体划分。

**为什么使用DIV分割布局？**
我们知道HTML中的每个元素都像一个盒子一样，每个盒子都能容纳其他元素，比如div元素、p元素、h4元素等都能容纳其他元素，那么为什么我们布局的时候要用div容纳其他元素,而不用p元素、h4元素或者其他元素呢？这是因为div元素是最干净的盒子它没有其他的属性，换句话说如果只写div标签而不加任何属性的话，它只是一个没有任何特性的容器，而其他元素比如p元素,它就有自己格外的属性，比如用两个p元素布局，它们之间有空行。如果用h4布局，放在h4里面的文字都被加大了显示了，像p元素、h4元素这些容器如果用来布局的话会影响效果，所以我们使用不加带任何特性的最干净的盒子—–DIV来布局。

案例：

![image-20201210104845958](image\常用杂项\image-20201210104845958.png)

```html
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Document</title>
    <style>
        * {
            margin: 0px auto;
        }
        
        #all {
            text-align: center;
        }
        
        div.logo {
            width: 1300px;
            height: 60px;
            background-color: yellow;
        }
        
        div.copyright {
            width: 1300px;
            height: 60px;
            background-color: goldenrod;
        }
        
        div.middle {
            width: 1300px;
            height: 500px;
        }
        
        div.menu {
            width: 200px;
            height: 500px;
            background-color: #E4393C;
            /*菜单左浮动*/
            float: left;
        }
        
        div.main {
            width: 1100px;
            height: 500px;
            background-color: #bad0ef;
            /*主题右浮动*/
            float: right;
            /*左浮动、右浮动都可以*/
            float: left;
        }
    </style>
</head>

<body>
    <div id="all">

        <div class="logo">
            logo
        </div>

        <div class="middle">
            <div class="menu">
                menu
            </div>

            <div class="main">
                网站的主体内容
            </div>
        </div>

        <div class="copyright">
            bottom
        </div>

    </div>
</body>

</html>
```

如果不使用浮动，则块级元素默认是垂直排列，而改为行级元素又无法调整宽高和边距，所以我们采用浮动，来使两个块级元素水平排列。

> 多类症:不要过多的使用类选择器,这样会造成代码臃肿，可读性差,能使用其他选择器代替的就使用其他选择器

**浮动的若干特性**：

- 1.框1向右移动
- 2.框1向左浮动
- 3.所有三个框向左浮动
- 清除浮动

如下图第二行第三个“被卡住”的情况，或者我们想要前两个框向左浮动，不想要第三个浮动，可以在第三个框的DIV处加 clear:left; 属性。意思是清除向左浮动的特性，第三个元素直接换行显示。

案例：

![image-20201210113040989](image\常用杂项\image-20201210113040989.png)

```html
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Document</title>
    <style>
        .d {
            width: 210px;
            height: 200px;
            border: 1px dashed black;
            margin: 5px;
        }
        
        .dd {
            width: 60px;
            height: 60px;
            background-color: #80ff80;
            text-align: center;
            line-height: 60px;
            margin: 5px;
        }
        
        p {
            margin-left: 5px;
            font-size: 12px;
        }
    </style>
</head>

<body>
    <div style="float:left;">
        <p>不浮动</p>
        <div class="d">
            <div class="dd">框1</div>
            <div class="dd">框2</div>
            <div class="dd">框3</div>
        </div>
    </div>

    <div style="float:left;">
        <p>框1:右浮</p>
        <div class="d">
            <div class="dd" style="float: right;">框1</div>
            <div class="dd">框2</div>
            <div class="dd">框3</div>
        </div>
    </div>

    <div style="float:left;">
        <p>框1:左浮</p>
        <div class="d">
            <div class="dd" style="float:left;">框1</div>
            <div class="dd">框2</div>
            <div class="dd">框3</div>
        </div>
    </div>

    <div style="float:left;">
        <p>三个都左浮，宽足够</p>
        <div class="d">
            <div class="dd" style="float:left;">框1</div>
            <div class="dd" style="float:left;">框2</div>
            <div class="dd" style="float:left;">框3</div>
        </div>
    </div>

    <div style="float:left;">
        <p>三个都左浮，宽不足</p>
        <div class="d" style="width: 160px;">
            <div class="dd" style="float:left;">框1</div>
            <div class="dd" style="float:left;">框2</div>
            <div class="dd" style="float:left;">框3</div>
        </div>
    </div>

    <div style="float:left;">
        <p>三个都左浮，宽不足，框1过高</p>
        <div class="d" style="width: 160px;">
            <div class="dd" style="float:left;height: 80px;">框1</div>
            <div class="dd" style="float:left;">框2</div>
            <div class="dd" style="float:left;">框3</div>
        </div>
    </div>

    <div style="float:left;">
        <p>三个都左浮，clear: left;</p>
        <div class="d" style="width: 160px;">
            <div class="dd" style="float:left;height: 80px;">框1</div>
            <div class="dd" style="float:left;">框2</div>
            <div class="dd" style="float:left;clear: left;">框3</div>
        </div>
    </div>
</body>

</html>
```

### 三、定位布局

#### 1.静态定位 static


默认值，不写position相当于写上position:static;以前没学定位的时候其实都是静态定位，元素在它原本的位置显示，即使加了top、left等也不起作用。

#### 2.相对定位 relative

相对定位是相对于自身的原始位置进行平移，如果设置position：relative；表示相对定位。
z-index:值越大越在上面
注意：z-index必须加在已经定位的元素上才起作用。

特点：

   1）不影响元素本身的特性；
   2）不使元素脱离文档流（元素移动之后原始位置会被保留）；
   3）如果没有定位偏移量，对元素本身没有任何影响；
   4）提升层级。

注：定位元素位置控制：top/right/bottom/left  定位元素偏移量

#### 3.绝对定位 absolute

子容器相对于父容器的定位，如果没有父容器，则相对于body定位。

特点：

   1）使元素完全脱离文档流；
   2）使内嵌支持宽高；
   3）块属性标签内容撑开宽度；
   4）如果有定位父级相对于定位父级发生偏移，没有定位父级相对于document发生偏移；
   5）相对定位一般都是配合绝对定位元素使用；
   6）提升层级

注意：z-index:[number]；  定位层级
   a、定位元素默认后者层级高于前者；
   b、建议在兄弟标签之间比较层级

![image-20201210145804498](image\常用杂项\image-20201210145804498.png)

#### 4.固定定位 fixed

也是相对定位，相对于`窗口`的，长用于顶面菜单栏、广告弹出、全局功能按钮

> 那么我们什么时候用相对定位，什么时候用绝对定位呢？
> 一般顶层容器我们使用相对定位，子容器使用绝对定位，这样的好处是父容器移动时，子容器能够跟着父容器移动，而不用再调整子容器的位置。

### 四、粘性布局

**sticky 粘性定位** 基于用户的滚动位置来定位。粘性定位的元素是依赖于用户的滚动，在 position:relative 与 position:fixed 定位之间切换。它的行为就像 position:relative; 而当页面滚动超出目标区域时，它的表现就像 position:fixed，它会固定在目标位置。元素定位表现为在跨越特定阈值前为相对定位，之后为固定定位。这个特定阈值指的是 top, right, bottom 或 left 之一，换言之，指定 top, right, bottom 或 left 四个阈值其中之一，才可使粘性定位生效。否则其行为与相对定位相同。

![](image\常用杂项\sticky-layout-s.gif)

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>优酷弹幕</title>
    <style>
        * {
            margin: 0;
            padding: 0;
        }
        
        .box {
            width: 300px;
            height: 400px;
            border: 1px solid #cccccc;
            margin-left: 100px;
        }
        
        h4 {
            background-color: #80ffff;
            padding: 10px 50px;
            position: sticky;
            top: 0;
        }
    </style>
</head>

<body>
    <div class="box">
        <h4>标题1</h4>
        <span class="content">内容1</span>
    </div>
    <div class="box">
        <h4>标题22</h4>
        <span class="content">内容2</span>
    </div>
    <div class="box">
        <h4>标题333</h4>
        <span class="content">内容3</span>
    </div>

    <div class="box">
        <h4>标题4444</h4>
        <span class="content">内容4</span>
    </div>

    <div class="box">
        <h4>标题55555</h4>
        <span class="content">内容5</span>
    </div>
</body>

</html>
```



## 水平垂直居中的几种方法

### 第一种：块元素使用position和margin

父对象使用relative，内容使用absolute和margin

![image-20201210160223534](image\常用杂项\image-20201210160223534.png)

### 第二种：块元素使用position和transform

先用top、left把内容左上角移到父对象中心点，在用transform把内容的中心点和父对象中心点重合

![image-20201210160736524](image\常用杂项\image-20201210160736524.png)

### 第三种：块元素使用flex

使用display:flex弹性布局;  垂直居中align-items: center;   水平居中justify-content: center;

![image-20201210161629326](image\常用杂项\image-20201210161629326.png)

### 第四种：文本使用text-align和line-height

父元素是块元素，子元素不是，可以在父元素中设置水平居中text-align: center;  垂直居中line-height:设置和父级元素一样的高度

![image-20201210165453934](image\常用杂项\image-20201210165453934.png)

### 第五种：文本使用table-cell

父元素是块元素，子元素不是，可以使用display: table-cell;  水平居中text-align: center;  垂直居中vertical-align: middle;

![image-20201210164828958](image\常用杂项\image-20201210164828958.png)



## 一行文本超出

```css
overflow: hidden;
text-overflow:ellipsis;
white-space: nowrap;
```

## 多行文本超出显示

```css
display: -webkit-box;
-webkit-box-orient: vertical;
-webkit-line-clamp: 3;
overflow: hidden;
```



# 好看的案例

## 彩色水滴  

[打开文件](file:///F:\OneDrive\程序开发资料\md笔记\前端\案例\彩色水滴案例.html )

![image-20201214103022006](image\常用杂项\image-20201214103022006.png)

```html
<!DOCTYPE html>
<html>

<head>
    <title>aa</title>
    <style>
        body {
            background: black;
        }
        
        .container {
            position: relative;
            width: 300px;
            height: 300px;
            background-color: #121212;
            margin: 5px auto;
            display: flex;
            justify-content: center;
        }
        
        .circ {
            position: absolute;
            width: 132px;
            height: 132px;
            border-radius: 121px 121px 121px 0;
            background: linear-gradient(0deg, #962161 5%, #12bbea 65%);
            border: 7px solid #330060;
            transform: rotate(135deg);
            top: 70px
        }
        
        .c6 {
            position: absolute;
            width: 102px;
            height: 132px;
            border-bottom: 3px solid white;
            border-left: 3px solid transparent;
            border-right: 3px solid transparent;
            border-radius: 132px / 100px;
            transform: rotate(-142deg);
            /* background-color: green; */
            margin: -135px 3px;
            opacity: .5
        }
        
        .c6:before {
            content: "";
            position: absolute;
            width: 50px;
            height: 90px;
            border-top: 3px solid white;
            border-left: 3px solid transparent;
            border-right: 3px solid transparent;
            border-radius: 132px / 100px;
            transform: rotate(-3deg);
            margin: 80px 0px;
            opacity: 1
        }
        
        .c7 {
            position: absolute;
            width: 90px;
            height: 121px;
            border-radius: 50%;
            margin: -5px 25px;
            transform: rotate(-60deg);
            background: radial-gradient(circle at 50% 25%, #139cdd0d 5%, transparent 5%), radial-gradient(closest-side circle at 50% 25%, #db3b7d 100%, transparent 100%), radial-gradient(circle at 50% 75%, rgba(255, 255, 255, 0.1) 5%, transparent 5%), radial-gradient(closest-side circle at 50% 75%, #0ebeff 100%, transparent 100%), linear-gradient(to right, #db3b7d 50%, #0ebeff 50%);
        }
        
        .c9 {
            position: relative;
            width: 132px;
            height: 132px;
            border-radius: 121px 121px 121px 0;
            background: linear-gradient(0deg, #962161 0%, #12bbea 100%);
            border: 5px solid #330060;
            left: -3px;
            opacity: .21;
        }
        
        .c12 {
            position: absolute;
            width: 5px;
            height: 70px;
            border-radius: 50%;
            background: white;
            margin: -53px 53px;
            transform: rotate(83deg);
            opacity: .5
        }
        
        .c5 {
            position: absolute;
            width: 4px;
            height: 40px;
            border-radius: 50%;
            background: white;
            margin: -65px 5px;
            transform: rotate(0deg);
            opacity: .5;
            z-index: 333;
        }
        
        .c21 {
            position: absolute;
            width: 50px;
            height: 90px;
            border-bottom: 3px solid white;
            border-left: 3px solid transparent;
            border-right: 3px solid transparent;
            border-radius: 132px / 100px;
            transform: rotate(-125deg);
            margin: -135px -8px;
            opacity: .3
        }
        
        .c21::before {
            content: "";
            position: absolute;
            width: 50px;
            height: 132px;
            border-top: 3px solid white;
            border-left: 3px solid transparent;
            border-right: 3px solid transparent;
            border-radius: 132px / 100px;
            transform: rotate(16deg);
            margin: 50px -17px;
            opacity: 1
        }
        
        .circ3 {
            /* position: relative; */
            width: 212px;
            height: 90px;
            border-radius: 100%;
            background: #cb56d3;
            background: -webkit-radial-gradient(center, ellipse cover, #cb56d3 0%, #d345d8 12%, #330060 45%);
            background: radial-gradient(ellipse at center, #cb56d3 0%, #d345d8 12%, #330060 45%);
            filter: progid: DXImageTransform.Microsoft.gradient(startColorstr='#cb56d3', endColorstr='#330060', GradientType=1);
            margin: 172px auto;
        }
        
        .d {
            width: 210px;
            height: 210px;
            border: 1px dashed black;
            margin: 5px auto;
            display: flex;
            background-color: #121212;
        }
        
        .d1 {
            width: 102px;
            height: 132px;
            border-bottom: 3px solid #fff;
            border-left: 3px solid transparent;
            border-right: 3px solid transparnt;
            border-radius: 132px / 100px;
            margin: 5px auto;
            opacity: 0.5
        }
        
        .d1:before {
            content: "";
            position: absolute;
            width: 50px;
            height: 90px;
            border-top: 3px solid #fff;
            border-left: 3px solid transparent;
            border-right: 3px solid transparent;
            border-radius: 132px / 100px;
            transform: rotate(-3deg);
            margin: 80px 0px;
            opacity: 1
        }
        
        .d2 {
            position: relative;
            width: 132px;
            height: 132px;
            border-radius: 121px 121px 121px 0;
            background: linear-gradient(0deg, #962161 0%, #12bbea 100%);
            border: 5px solid #330060;
            top: 40px;
            transform: rotate(135deg);
            margin: 0px auto;
        }

        .d3 {
            /* position: relative; */
            width: 200px;
            height: 90px;
            border-radius: 100%;
            background: #cb56d3;
            background: -webkit-radial-gradient(center, ellipse cover, #cb56d3 0%, #d345d8 12%, #330060 45%);
            background: radial-gradient(ellipse at center, #cb56d3 0%, #d345d8 12%, #330060 45%);
            filter: progid: DXImageTransform.Microsoft.gradient(startColorstr='#cb56d3', endColorstr='#330060', GradientType=1);
            margin: 72px auto;
        }

        .d4 {
            position: absolute;
            width: 90px;
            height: 121px;
            border-radius: 50%;
            margin: 35px 65px;
            transform: rotate(90deg);
            background: radial-gradient(circle at 50% 25%, #139cdd0d 5%, transparent 5%), 
                        radial-gradient(closest-side circle at 50% 25%, #db3b7d 100%, transparent 100%), 
                        radial-gradient(circle at 50% 75%, rgba(255, 255, 255, 0.1) 5%, transparent 5%), 
                        radial-gradient(closest-side circle at 50% 75%, #0ebeff 100%, transparent 100%), 
                        linear-gradient(to right, #db3b7d 50%, #0ebeff 50%);
        }

        .d5 {
            position: absolute;
            width: 5px;
            height: 70px;
            border-radius: 50%;
            background: white;
            margin: 53px 83px;
            transform: rotate(33deg);
            opacity: .5;
        }

        .dec {
            width: 100%;
            height: 250px;
            display: flex;
        }
        
        p {
            margin-left: 5px;
            font-size: 12px;
            color: #80ff80;
        }

        .cssdec{
            width: 100%;
            display: flex;
        }
        
        xmp {
            color: #fff;
            font-size: 12px;
            width: 250px;
            overflow: auto;
            border: 1px solid #fff;
        }
    </style>
</head>

<body>
    <div class="container">

        <div class="circ">
            <div class="c7"></div>
            <div class="c9"></div>
            <div class="c6"></div>
            <div class="c5"></div>
            <div class="c12"></div>
            <div class="c21"></div>
        </div>
        <div class="circ3"></div>
    </div>

    <div class="dec">
        <div style="width: 250px;">
            <p>d1</p>
            <div class="d">
                <div class="d1"></div>
            </div>
        </div>
        <div style="width: 250px;">
            <p>d2</p>
            <div class="d">
                <div class="d2"></div>
            </div>
        </div>
        <div style="width: 250px;">
            <p>d3</p>
            <div class="d">
                <div class="d3"></div>
            </div>
        </div>
        <div style="width: 250px;">
            <p>d4</p>
            <div class="d">
                <div class="d4"></div>
            </div>
        </div>
        <div style="width: 250px;">
            <p>d5</p>
            <div class="d">
                <div class="d5"></div>
            </div>
        </div>
    </div>

    <div class="cssdec">
<xmp>
.d1 {
    width: 102px;
    height: 132px;
    border-bottom: 3px solid #fff;
    border-left: 3px solid transparent;
    border-right: 3px solid transparnt;
    border-radius: 132px / 100px;
    margin: 5px auto;
    opacity: 0.5
}

.d1:before {
    content: "";
    position: absolute;
    width: 50px;
    height: 90px;
    border-top: 3px solid #fff;
    border-left: 3px solid transparent;
    border-right: 3px solid transparent;
    border-radius: 132px / 100px;
    transform: rotate(-3deg);
    margin: 80px 0px;
    opacity: 1
}
</xmp>
<xmp>
.d2 {
    position: relative;
    width: 132px;
    height: 132px;
    border-radius: 121px 121px 121px 0;
    background: linear-gradient(0deg, #962161 0%, #12bbea 100%);
    border: 5px solid #330060;
    top: 40px;
    transform: rotate(135deg);
    margin: 0px auto;
}
</xmp>
<xmp>
.d3 {
    width: 200px;
    height: 90px;
    border-radius: 100%;
    background: #cb56d3;
    background: -webkit-radial-gradient(center, ellipse cover, #cb56d3 0%, #d345d8 12%, #330060 45%);
    background: radial-gradient(ellipse at center, #cb56d3 0%, #d345d8 12%, #330060 45%);
    filter: progid: DXImageTransform.Microsoft.gradient(startColorstr='#cb56d3', endColorstr='#330060', GradientType=1);
    margin: 72px auto;
}
</xmp>
<xmp>
.d4 {
    position: absolute;
    width: 90px;
    height: 121px;
    border-radius: 50%;
    margin: 35px 65px;
    transform: rotate(90deg);
    background: radial-gradient(circle at 50% 25%, #139cdd0d 5%, transparent 5%), 
                radial-gradient(closest-side circle at 50% 25%, #db3b7d 100%, transparent 100%), 
                radial-gradient(circle at 50% 75%, rgba(255, 255, 255, 0.1) 5%, transparent 5%), 
                radial-gradient(closest-side circle at 50% 75%, #0ebeff 100%, transparent 100%), 
                linear-gradient(to right, #db3b7d 50%, #0ebeff 50%);
}
</xmp>
<xmp>
.d5 {
    position: absolute;
    width: 5px;
    height: 70px;
    border-radius: 50%;
    background: white;
    margin: 53px 83px;
    transform: rotate(33deg);
    opacity: .5;
}
</xmp>
    </div>
</body>

</html>
```

## 图片悬停遮罩效果

![image-20201214105450712](image\常用杂项\image-20201214105450712.png)

```html
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<title>图片悬停遮罩效果</title>
</head>
<body>

<!-- 代码部分begin -->
<style>
* { margin: 0; padding: 0; }
body { font-family: "Microsoft yahei", Arial; background: #eee; }
.box { width: 500px; height: 500px; margin: 50px auto; }
.box ul li { float: left; width: 190px; height: 190px; background: #979797; border: solid 10px #979797; margin: 10px; list-style: none; position: relative; overflow: hidden; }
.box ul li .cover a { width: 30px; height: 30px; background: #ffffff; display: block; border-radius: 50%; line-height: 30px; margin: 30px auto; }
.box ul li .cover a i { color: red; }
.box ul li .cover { width: 190px; height: 190px; background: rgba(255, 39, 42, 0.7); position: absolute; left: 0; top: 0; text-align: center; color: #ffffff; transform-origin: right bottom; -webkit-transform-origin: right bottom; -moz-transform-origin: right bottom; transform: rotate(90deg); -webkit-transform: rotate(90deg); -moz-transform: rotate(90deg); transition: all 0.35s; -webkit-transition: all 0.35s; -moz-transition: all 0.35s; }
.box ul li .cover p { margin-top: 10px; font-size: 14px; }
.box ul li:hover .cover { transform: rotate(0deg); -webkit-transform: rotate(0deg); -moz-transform: rotate(0deg); }</style>
<div class="box">
<ul>
	<li>
		<img src="images/1.jpg" width="190" height="190" alt="">
		<div class="cover">
			<a href=""><i class="fa fa-plus"></i></a>
			<h4>Alice</h4>
			<p>来自美国洛杉矶</p>
		</div>
	</li>
	<li>
		<img src="images/1.jpg" width="190" height="190" alt="">
		<div class="cover">
			<a href=""><i class="fa fa-plus"></i></a>
			<h4>Alice</h4>
			<p>来自美国洛杉矶</p>
		</div>
	</li>
</ul>
</div>
<!-- 代码部分end -->

</body>
</html>
```

## 用css绘制各种图形

[打开文件](file:///F:\OneDrive\程序开发资料\md笔记\前端\案例\ccs绘制各种图形.html )




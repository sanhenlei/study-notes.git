概述

构建一个项目需要用到很多第三方的类库，如写一个使用Spring的Web项目就需要引入大量的jar包。一个项目Jar包的数量之多往往让我们瞠目结舌，并且Jar包之间的关系错综复杂，一个Jar包往往又会引用其他Jar包，缺少任何一个Jar包都会导致项目编译失败。

以往开发项目时，程序员往往需要花较多的精力在引用Jar包搭建项目环境上，而这一项工作尤为艰难，少一个Jar包、多一个Jar包往往会报一些让人摸不着头脑的异常。

而Maven就是一款帮助程序员`构建项目的工具`，我们只需要告诉Maven需要哪些Jar 包，它会帮助我们下载所有的Jar，极大提升开发效率。

Maven是Apache下的一个纯java开发的开源项目，它是一个项目管理工具，使用maven对java项目进行构建、依赖管理。

# 安装

## 下载

官方地址：http://maven.apache.org/download.cgi

![下载](image/Maven/0a8530171677c776c7036c8479591c6d.png)

解压并新建一个本地仓库文件夹

![image-20210213134525218](image/Maven/image-20210213134525218.png)

## 配置 repository

**本地仓库（.m2）**

**运行原理**
运行Maven的时候，Maven所需要的任何构件都是直接从本地仓库获取的。如果本地仓库没有，它会首先尝试从远程仓库下载构件至本地仓库，然后再使用本地仓库的构件。

**[Repository 仓库](https://blog.csdn.net/u010811939/article/details/81110920)**

![img](image/Maven/20181109121300183.gif)

由于在配置时本地的仓库需要通过over wall下载 ,速度很慢，这里直接给大家放百度网盘链接，自行食用

 **[👉【repository本地库 网盘提取码：2020】](https://pan.baidu.com/s/1NUuA_siAJDdSXmVKtUio4Q)**

下载后，直接解压安装在和meven同一个盘符下

打开 Meven conf

![img](image/Maven/2019073109380839.png)

复制 settings 文件到 C盘用户 m2文件下 

 ![img](image/Maven/2019073109390566.png) 

 ![img](image/Maven/20190731094147195.png)

修改刚刚复制到 .m2文件下的 settings 文件（conf下的不用修改）

为的是在使用 repository时，把下载的文件路径更改到你安装的 **D:\repository** 下

更改如下：

**<localRepository>D:\repository</localRepository>**

![img](image/Maven/20190731094920573.png)

保存之后就配置完成了 



## 配置 settings

在创建 meven项目时，由于导入的 jar包要在**apache**的官网上下载，速度特别慢（没错，我创建项目又用了半个小时），外网没办法....

我们需要修改一下镜像文件，改为国内阿里爸爸的镜像（30分钟的事3分钟解决）

**打开 meven目录conf下的 settings**

![img](image/Maven/20190731095928495.png)

ctrl + f 搜索 **“<mirrors>”**

在<mirrors>标签里增加以下内容：

![img](image/Maven/20190731100110850.png)

```xml
 <mirrors>
    <mirror>
    	<id>alimaven</id>
    	<name>aliyun maven</name>
    	<url>http://maven.aliyun.com/nexus/content/groups/public/</url>
    	<mirrorOf>*</mirrorOf>
    </mirror>
  </mirrors>
```

再去下载maven依赖的时候，有些依赖包的地址已经变成了aliyun的地址了，速度比之前快了很多 

## 环境变量

![点击更改设置](image/Maven/11b1778de7b2d5cb7fd0831dd20029ef.png)
![点击环境变量](image/Maven/61e18f89f8d01379d248a4d37f1ee3f6.png)
![新建M2_HOME](image/Maven/ec1d9ca0c7947a043f8788c3dc48967c.png)
![配置Path](image/Maven/0ef05d38773cb0f5ebdebd960659778f.png)
![查询是否配置成功](image/Maven/6cc34567dc3d74050b6243afb8526433.png)

# IDEA中配置maven

打开-File-Settings
![配置maven](image/Maven/3390df174daba72695ba575c8603a7aa.png)

**5.新建maven WEB项目**
打开-File-New-Project
点击NEXT
![创建maven项目](image/Maven/900fd4b05cecaa7ab1fd25af0ea7b063.png)
点击NEXT
![这里写图片描述](image/Maven/1e3a79b37a3b072ea858fcb1627d508b.png)
添加的配置为 archetypeCatalog=internal
点击NEXT
![这里写图片描述](image/Maven/04261c8b10258e4a6768363b832d0fc7.png)
点击NEXT
![这里写图片描述](image/Maven/28a6a326fb06a4fe3e751484070121bf.png)
点击Finish后项目开始创建
点击右下角查看进去
![这里写图片描述](image/Maven/082f80459d2aec3b8b9b6e273b3b7426.png)

**6.maven web模板项目结构**
![这里写图片描述](image/Maven/d14101018a2d1d3bd76365a176caba69.png)

![这里写图片描述](image/Maven/73b4e1e3ef289a8a4aff6f69277e83f5.png)

![这里写图片描述](image/Maven/cb655d3abac0d339716a35536f0de3cb.png)
同样在main下新建test测试文件夹,再在此文件夹下新建java测试源码文件夹和resource测试资源文件夹
![这里写图片描述](image/Maven/8b5e7513fc3aa3265c7f26762204e4fb.png)
也可以右键项目-选择Open Module Settings打开项目配置页面更改
![这里写图片描述](image/Maven/9ed6cfcd6fa75dd034c5dd585411007c.png)

**7.配置依赖jar包**
![这里写图片描述](image/Maven/fc9e9abaea082974b28a27c73741c194.png)

# 目录结构

若要使用Maven，那么项目的目录结构必须符合Maven的规范，其目录结构如下：

![img](image/Maven/201808131239225)

# 基本命令

1. **mvn -v:**查询Maven版本

   本命令用于检查maven是否安装成功。

   Maven安装完成之后，在命令行输入***\*mvn -v\****，若出现maven信息，则说明安装成功。

2. **mvn compile**：编译

   将java源文件编译成class文件

3. **mvn test**:测试项目

   执行test目录下的测试用例

4. **mvn package**:打包

   将项目打成jar包

5. **mvn clean**:删除target文件夹

6. **mvn install**:安装

   将当前项目放到Maven的本地仓库中。供其他项目使用



# jar包搜索

官方地址：http://mvnrepository.com/

# 详解

## 什么是Maven仓库？

Maven仓库用来存放Maven管理的所有Jar包。分为：本地仓库 和 中央仓库。

- **本地仓库**：Maven本地的Jar包仓库。
- **中央仓库**： Maven官方提供的远程仓库。

当项目编译时，Maven首先从本地仓库中寻找项目所需的Jar包，若本地仓库没有，再到Maven的中央仓库下载所需Jar包。

## 什么是“坐标”？

在Maven中，坐标是Jar包的唯一标识，Maven通过坐标在仓库中找到项目所需的Jar包。

如下代码中，groupId和artifactId构成了一个Jar包的坐标。

```XML
<dependency>
   <groupId>cn.missbe.web.search</groupId>
   <artifactId>resource-search</artifactId>
   <packaging>jar</packaging>
   <version>1.0-SNAPSHOT</version>
</dependency>
```

- **groupId**:所需Jar包的项目名
- **artifactId**:所需Jar包的模块名
- **version**:所需Jar包的版本号

## 传递依赖 与 排除依赖

- 传递依赖：如果我们的项目引用了一个Jar包，而该Jar包又引用了其他Jar包，那么在默认情况下项目编译时，Maven会把直接引用和简洁引用的Jar包都下载到本地。
- 排除依赖：如果我们只想下载直接引用的Jar包，那么需要在pom.xml中做如下配置：(将需要排除的Jar包的坐标写在中)

```XML
<exclusions>
   <exclusion>
      <groupId>cn.missbe.web.search</groupId>
      <artifactId>resource-search</artifactId>
      <packaging>pom</packaging>
      <version>1.0-SNAPSHOT</version>
   </exclusion>
</exclusions>
```

## 依赖范围scope

在项目发布过程中，帮助决定哪些构件被包括进来。欲知详情请参考依赖机制。   

| 取值     | 含义                                                        | 举例                 |
| -------- | ----------------------------------------------------------- | -------------------- |
| compile  | 该依赖可以在整个项目中使用，参与打包部署，默认值            | commons-fileupload   |
| test     | 该依赖只能在测试代码中使用，并且不参与打包部署的            | junit                |
| provided | 该依赖编写源代码时需要，不参与打包部署                      | servlet-api、jsp-api |
| runtime  | 该依赖编写代码时不需要，运行时需要，参与打包部署            | mysql-connector      |
| system   | 表示使用本地系统路径下的jar包，需要和一个systemPath一起使用 | ojdbc.jar            |

使用本地jar包（放在自己创建的lib文件下，与src同级）：

```xml
<dependency>
  <groupId>mysql</groupId>
  <artifactId>mysql-connector-java</artifactId>
  <version>8.0.22</version>
  <systemPath>${basedir}/lib/mysql-connector-java-8.0.17.jar</systemPath>
</dependency>
```

## properties

全局属性，一般情况下同于定义全局的jar包的版本

作用：比如在properties中定义版本<ljy.version>4.3.12</ljy.version>

那么在对于的依赖下添加${ljy.version}

在properties中改了 后面对应的依赖版本也跟着改了（方便）

在依赖的version中alt+ctrl+v（快速提取）

如：

```xml
<properties>
  <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
  <maven.compiler.source>1.7</maven.compiler.source>
  <maven.compiler.target>1.7</maven.compiler.target>
  <mysql-connector-java.version>8.0.22</mysql-connector-java.version>
  // 可定义时间变量
  <maven.build.timestamp.format>yyyy-MM-dd_HH_mm</maven.build.timestamp.format>  
</properties>

   <dependency>
      <groupId>mysql</groupId>
      <artifactId>mysql-connector-java</artifactId>
      <version>${mysql-connector-java.version}</version>
    </dependency>
```

## repositories

用来配置当前工程使用的远程仓库

依赖查找的顺序：本地仓库——>当前工程pom.xml中配置的远程仓库——>setting.xml中配置的远程仓库

spring为例：url 是spring官方地址

```xml
<repositories>
<!--      有些最新的jar包，可能在中央仓库上并没有提供，此时可以使用jar包官方提供的仓库-->
        <repository>
            <id>spring_repo</id>
            <url>https://repo.spring.io/milestone/</url>
        </repository>
```

## 依赖冲突

若项目中多个Jar同时引用了相同的Jar时，会产生依赖冲突，但Maven采用了两种避免冲突的策略，因此在Maven中是不存在依赖冲突的。

**1.短路优先**

```java
本项目——>A.jar——>B.jar——>X.jar
本项目——>C.jar——>X.jar
```

若本项目引用了A.jar，A.jar又引用了B.jar，B.jar又引用了X.jar，并且C.jar也引用了X.jar。

在此时，Maven只会引用路径最短的Jar。

**2.声明优先**

若引用路径长度相同时，在pom.xml中谁先被声明，就使用谁。

## 聚合

1. 什么是聚合？

   将多个项目同时运行就称为聚合。

2. 如何实现聚合？

   只需在pom中作如下配置即可实现聚合：

```XML
<modules>
    <module>web-connection-pool</module>
    <module>web-java-crawler</module>
</modules>
```

## 继承

1. 什么是继承？

   在聚合多个项目时，如果这些被聚合的项目中需要引入相同的Jar，那么可以将这些Jar写入父pom中，各个子项目继承该pom即可。

2. 如何实现继承？

- 父pom配置：将需要继承的Jar包的坐标放入标签即可。

```XML
<dependencyManagement>
    <dependencies>
          <dependency>
            <groupId>cn.missbe.web.search</groupId>
            <artifactId>resource-search</artifactId>
            <packaging>pom</packaging>
            <version>1.0-SNAPSHOT</version>
          </dependency> 
    </dependencies>
</dependencyManagement>
```

- 子pom配置：

```XML
<parent>
      <groupId>父pom所在项目的groupId</groupId>
      <artifactId>父pom所在项目的artifactId</artifactId>
      <version>父pom所在项目的版本号</version>
</parent>
 <parent>
   		<groupId>cn.missbe.web.search</groupId>
      <artifactId>resource-search</artifactId>
      <version>1.0-SNAPSHOT</version>
</parent>
```

## 使用Maven构建Web项目

在eclipse中，用maven创建一个web项目工程：https://www.cnblogs.com/hongmoshui/p/7994759.html



1. New Maven项目：选择WebApp：

2. 若使用JSP，需添加Servlet依赖：

   注：Servlet依赖只在编译和测试时使用！

```XML
<dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>javax.servlet-api</artifactId>
    <version>3.0.1</version>
    <!-- 只在编译和测试时运行 -->
    <scope>provided</scope>
</dependency>
```

1. 在Bulid Path中设置resource输出目录：
2. 勾选：Dynamic Web Module
3. 删掉测试目录
4. 在pom中加入jetty的插件，并设置JDK版本：

```XML
<plugins> 
  <plugin> 
    <groupId>org.apache.maven.plugins</groupId>  
    <artifactId>maven-compiler-plugin</artifactId>  
    <configuration> 
      <source>1.8</source>  
      <target>1.8</target> 
    </configuration> 
  </plugin>  
  <plugin> 
    <groupId>org.eclipse.jetty</groupId>  
    <artifactId>jetty-maven-plugin</artifactId>  
    <version>9.3.10.v20160621</version>  
    <executions> 
      <execution> 
        <phase>package</phase> 
      </execution> 
    </executions> 
  </plugin> 
</plugins>
```

1. 运行项目：

2. 输入：jetty:run

3. 访问127.0.0.1:8080

   若出现如下界面，表示成功！

## pom.xml详解

pom.xml是Maven的核心，你的项目需要什么Jar包就在pom.xml里面配置。当编译项目时Maven读取该文件，并从仓库中下载相应的Jar包。

```XML
<?xml version="1.0" encoding="utf-8"?>
 
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0http://maven.apache.org/maven-v4_0_0.xsd">  
  <!--父项目的坐标。如果项目中没有规定某个元素的值，
那么父项目中的对应值即为项目的默认值。 
坐标包括group ID，artifact ID和 version。-->  
  <parent> 
    <!--被继承的父项目的构件标识符-->  
    <artifactId/>  
    <!--被继承的父项目的全球唯一标识符-->  
    <groupId/>  
    <!--被继承的父项目的版本-->  
    <version/> 
  </parent>  
  <!--声明项目描述符遵循哪一个POM模型版本。模型本身的版本很少改变，虽然如此，
但它仍然是必不可少的，这是为了当Maven引入了新的特性或者其他模型变更的时候，
确保稳定性。-->  
  <modelVersion>4.0.0</modelVersion>  
  <!--项目的全球唯一标识符，通常使用全限定的包名区分该项目和其他项目。
并且构建时生成的路径也是由此生成， 如com.mycompany.app生成的相对路径为：
/com/mycompany/app-->  
  <groupId>cn.missbe.web</groupId>  
  <!-- 构件的标识符，它和group ID一起唯一标识一个构件。换句话说，
你不能有两个不同的项目拥有同样的artifact ID和groupID；在某个 
特定的group ID下，artifact ID也必须是唯一的。构件是项目产生的或使用的一个东西，
Maven为项目产生的构件包括：JARs，源码，二进制发布和WARs等。-->  
  <artifactId>search-resources</artifactId>  
  <!--项目产生的构件类型，例如jar、war、ear、pom。插件可以创建
他们自己的构件类型，所以前面列的不是全部构件类型-->  
  <packaging>war</packaging>  
  <!--项目当前版本，格式为:主版本.次版本.增量版本-限定版本号-->  
  <version>1.0-SNAPSHOT</version>  
  <!--项目的名称, Maven产生的文档用-->  
  <name>search-resources</name>  
  <!--项目主页的URL, Maven产生的文档用-->  
  <url>http://www.missbe.cn</url>  
  <!-- 项目的详细描述, Maven 产生的文档用。  当这个元素能够用HTML格式描述时
（例如，CDATA中的文本会被解析器忽略，就可以包含HTML标 签）， 
不鼓励使用纯文本描述。如果你需要修改产生的web站点的索引页面，
你应该修改你自己的索引页文件，而不是调整这里的文档。-->  
  <description>A maven project to study maven.</description>  
  <!--描述了这个项目构建环境中的前提条件。-->  
  <prerequisites> 
    <!--构建该项目或使用该插件所需要的Maven的最低版本-->  
    <maven/> 
  </prerequisites>  
  <!--构建项目需要的信息-->  
  <build> 
    <!--该元素设置了项目源码目录，当构建项目的时候，
构建系统会编译目录里的源码。该路径是相对于pom.xml的相对路径。-->  
    <sourceDirectory/>  
    <!--该元素设置了项目脚本源码目录，该目录和源码目录不同：
绝大多数情况下，该目录下的内容 会被拷贝到输出目录(因为脚本是被解释的，而不是被编译的)。-->  
    <scriptSourceDirectory/>  
    <!--该元素设置了项目单元测试使用的源码目录，当测试项目的时候，
构建系统会编译目录里的源码。该路径是相对于pom.xml的相对路径。-->  
    <testSourceDirectory/>  
    <!--被编译过的应用程序class文件存放的目录。-->  
    <outputDirectory/>  
    <!--被编译过的测试class文件存放的目录。-->  
    <testOutputDirectory/>  
    <!--使用来自该项目的一系列构建扩展-->  
    <extensions> 
      <!--描述使用到的构建扩展。-->  
      <extension> 
        <!--构建扩展的groupId-->  
        <groupId/>  
        <!--构建扩展的artifactId-->  
        <artifactId/>  
        <!--构建扩展的版本-->  
        <version/> 
      </extension> 
    </extensions>  
    <!--这个元素描述了项目相关的所有资源路径列表，例如和项目相关的属性文件，
这些资源被包含在最终的打包文件里。-->  
    <resources> 
      <!--这个元素描述了项目相关或测试相关的所有资源路径-->  
      <resource> 
        <!-- 描述了资源的目标路径。该路径相对target/classes目录（例如${project.build.outputDirectory}）。举个例 子，如果你想资源在特定的包里(org.apache.maven.messages)，你就必须该元素设置为org/apache/maven /messages。
然而，如果你只是想把资源放到源码目录结构里，就不需要该配置。-->  
        <targetPath/>  
        <!--是否使用参数值代替参数名。参数值取自properties元素或者文件里配置的属性，
文件在filters元素里列出。-->  
        <filtering/>  
        <!--描述存放资源的目录，该路径相对POM路径-->  
        <directory/>  
        <!--包含的模式列表，例如**/*.xml.-->  
        <includes/>  
        <!--排除的模式列表，例如**/*.xml-->  
        <excludes/> 
      </resource> 
    </resources>  
    <!--这个元素描述了单元测试相关的所有资源路径，例如和单元测试相关的属性文件。-->  
    <testResources> 
      <!--这个元素描述了测试相关的所有资源路径，参见build/resources/resource元素的说明-->  
      <testResource> 
        <targetPath/>
        <filtering/>
        <directory/>
        <includes/>
        <excludes/> 
      </testResource> 
    </testResources>  
    <!--构建产生的所有文件存放的目录-->  
    <directory/>  
    <!--产生的构件的文件名，默认值是${artifactId}-${version}。-->  
    <finalName/>  
    <!--当filtering开关打开时，使用到的过滤器属性文件列表-->  
    <filters/>  
    <!--子项目可以引用的默认插件信息。该插件配置项直到被引用时才会被解析或绑定到生命周期。
给定插件的任何本地配置都会覆盖这里的配置-->  
    <pluginManagement> 
      <!--使用的插件列表 。-->  
      <plugins> 
        <!--plugin元素包含描述插件所需要的信息。-->  
        <plugin> 
          <!--插件在仓库里的group ID-->  
          <groupId/>  
          <!--插件在仓库里的artifact ID-->  
          <artifactId/>  
          <!--被使用的插件的版本（或版本范围）-->  
          <version/>  
          <!--是否从该插件下载Maven扩展（例如打包和类型处理器），由于性能原因，
只有在真需要下载时，该元素才被设置成enabled。-->  
          <extensions/>  
          <!--在构建生命周期中执行一组目标的配置。每个目标可能有不同的配置。-->  
          <executions> 
            <!--execution元素包含了插件执行需要的信息-->  
            <execution> 
              <!--执行目标的标识符，用于标识构建过程中的目标，或者匹配继承过程中需要合并的执行目标-->  
              <id/>  
              <!--绑定了目标的构建生命周期阶段，如果省略，目标会被绑定到源数据里配置的默认阶段-->  
              <phase/>  
              <!--配置的执行目标-->  
              <goals/>  
              <!--配置是否被传播到子POM-->  
              <inherited/>  
              <!--作为DOM对象的配置-->  
              <configuration/> 
            </execution> 
          </executions>  
          <!--项目引入插件所需要的额外依赖-->  
          <dependencies> 
            <!--参见dependencies/dependency元素-->  
            <dependency>......</dependency> 
          </dependencies>  
          <!--任何配置是否被传播到子项目-->  
          <inherited/>  
          <!--作为DOM对象的配置-->  
          <configuration/> 
        </plugin> 
      </plugins> 
    </pluginManagement>  
    <!--使用的插件列表-->  
    <plugins> 
      <!--参见build/pluginManagement/plugins/plugin元素-->  
      <plugin> 
        <groupId/>
        <artifactId/>
        <version/>
        <extensions/>  
        <executions> 
          <execution> 
            <id/>
            <phase/>
            <goals/>
            <inherited/>
            <configuration/> 
          </execution> 
        </executions>  
        <dependencies> 
          <!--参见dependencies/dependency元素-->  
          <dependency>......</dependency> 
        </dependencies>  
        <goals/>
        <inherited/>
        <configuration/> 
      </plugin> 
    </plugins> 
  </build>  
  <!--模块（有时称作子项目） 被构建成项目的一部分。
列出的每个模块元素是指向该模块的目录的相对路径-->  
  <modules/>  
  <!--发现依赖和扩展的远程仓库列表。-->  
  <repositories> 
    <!--包含需要连接到远程仓库的信息-->  
    <repository> 
      <!--如何处理远程仓库里发布版本的下载-->  
      <releases> 
        <!--true或者false表示该仓库是否为下载某种类型构件（发布版，快照版）开启。 -->  
        <enabled/>  
        <!--该元素指定更新发生的频率。Maven会比较本地POM和远程POM的时间戳。这里的选项是：always（一直），daily（默认，每日），interval：X（这里X是以分钟为单位的时间间隔），或者never（从不）。-->  
        <updatePolicy/>  
        <!--当Maven验证构件校验文件失败时该怎么做：ignore（忽略），fail（失败），或者warn（警告）。-->  
        <checksumPolicy/> 
      </releases>  
      <!-- 如何处理远程仓库里快照版本的下载。有了releases和snapshots这两组配置，
POM就可以在每个单独的仓库中，为每种类型的构件采取不同的 策略。
例如，可能有人会决定只为开发目的开启对快照版本下载的支持。
参见repositories/repository/releases元素 -->  
      <snapshots> 
        <enabled/>
        <updatePolicy/>
        <checksumPolicy/> 
      </snapshots>  
      <!--远程仓库唯一标识符。可以用来匹配在settings.xml文件里配置的远程仓库-->  
      <id>banseon-repository-proxy</id>  
      <!--远程仓库名称-->  
      <name>banseon-repository-proxy</name>  
      <!--远程仓库URL，按protocol://hostname/path形式-->  
      <url>http://192.168.1.169:9999/repository/</url>  
      <!-- 用于定位和排序构件的仓库布局类型-可以是default（默认）或者legacy（遗留）。Maven 2为其仓库提供了一个默认的布局；然 而，Maven 1.x有一种不同的布局。我们可以使用该元素指定布局是default（默认）还是legacy（遗留）。-->  
      <layout>default</layout> 
    </repository> 
  </repositories>  
  <!--发现插件的远程仓库列表，这些插件用于构建和报表-->  
  <pluginRepositories> 
    <!--包含需要连接到远程插件仓库的信息.参见repositories/repository元素-->  
    <pluginRepository>......</pluginRepository> 
  </pluginRepositories>  
  <!--该元素描述了项目相关的所有依赖。 这些依赖组成了项目构建过程中的一个个环节。
它们自动从项目定义的仓库中下载。要获取更多信息，请看项目依赖机制。-->  
  <dependencies> 
    <dependency> 
      <!--依赖的group ID-->  
      <groupId>org.apache.maven</groupId>  
      <!--依赖的artifact ID-->  
      <artifactId>maven-artifact</artifactId>  
      <!--依赖的版本号。 在Maven 2里, 也可以配置成版本号的范围。-->  
      <version>3.8.1</version>  
      <!-- 依赖类型，默认类型是jar。它通常表示依赖的文件的扩展名，但也有例外
。一个类型可以被映射成另外一个扩展名或分类器。类型经常和使用的打包方式对应，
 尽管这也有例外。一些类型的例子：jar，war，ejb-client和test-jar。
如果设置extensions为 true，就可以在 plugin里定义新的类型。所以前面的类型的例子不完整。-->  
      <type>jar</type>  
      <!-- 依赖的分类器。分类器可以区分属于同一个POM，但不同构建方式的构件。
分类器名被附加到文件名的版本号后面。例如，如果你想要构建两个单独的构件成 JAR，
一个使用Java 1.4编译器，另一个使用Java 6编译器，你就可以使用分类器来生成两个单独的JAR构件。-->  
      <classifier/>  
      <!--依赖范围。在项目发布过程中，帮助决定哪些构件被包括进来。欲知详情请参考依赖机制。    
                - compile ：默认范围，用于编译      
                - provided：类似于编译，但支持你期待jdk或者容器提供，类似于classpath      
                - runtime: 在执行时需要使用      
                - test:    用于test任务时使用      
                - system: 需要外在提供相应的元素。通过systemPath来取得      
                - systemPath: 仅用于范围为system。提供相应的路径      
                - optional:   当项目自身被依赖时，标注依赖是否传递。用于连续依赖时使用-->  
      <scope>test</scope>  
      <!--仅供system范围使用。注意，不鼓励使用这个元素，
并且在新的版本中该元素可能被覆盖掉。该元素为依赖规定了文件系统上的路径。
需要绝对路径而不是相对路径。推荐使用属性匹配绝对路径，例如${java.home}。-->  
      <systemPath/>  
      <!--当计算传递依赖时， 从依赖构件列表里，列出被排除的依赖构件集。
即告诉maven你只依赖指定的项目，不依赖项目的依赖。此元素主要用于解决版本冲突问题-->  
      <exclusions> 
        <exclusion> 
          <artifactId>spring-core</artifactId>  
          <groupId>org.springframework</groupId> 
        </exclusion> 
      </exclusions>  
      <!--可选依赖，如果你在项目B中把C依赖声明为可选，你就需要在依赖于B的项目（例如项目A）中显式的引用对C的依赖。可选依赖阻断依赖的传递性。-->  
      <optional>true</optional> 
    </dependency> 
  </dependencies>  
  <!-- 继承自该项目的所有子项目的默认依赖信息。这部分的依赖信息不会被立即解析,
而是当子项目声明一个依赖（必须描述group ID和 artifact ID信息），
如果group ID和artifact ID以外的一些信息没有描述，
则通过group ID和artifact ID 匹配到这里的依赖，并使用这里的依赖信息。-->  
  <dependencyManagement> 
    <dependencies> 
      <!--参见dependencies/dependency元素-->  
      <dependency>......</dependency> 
    </dependencies> 
  </dependencyManagement>  
  <!--项目分发信息，在执行mvn deploy后表示要发布的位置。
有了这些信息就可以把网站部署到远程服务器或者把构件部署到远程仓库。-->  
  <distributionManagement> 
    <!--部署项目产生的构件到远程仓库需要的信息-->  
    <repository> 
      <!--是分配给快照一个唯一的版本号（由时间戳和构建流水号）？
还是每次都使用相同的版本号？参见repositories/repository元素-->  
      <uniqueVersion/>  
      <id>banseon-maven2</id>  
      <name>banseon maven2</name>  
      <url>file://${basedir}/target/deploy</url>  
      <layout/> 
    </repository>  
    <!--构件的快照部署到哪里？如果没有配置该元素，默认部署到repository元素配置的仓库，
参见distributionManagement/repository元素-->  
    <snapshotRepository> 
      <uniqueVersion/>  
      <id>banseon-maven2</id>  
      <name>Banseon-maven2 Snapshot Repository</name>  
      <url>scp://svn.baidu.com/banseon:/usr/local/maven-snapshot</url>  
      <layout/> 
    </snapshotRepository>  
    <!--部署项目的网站需要的信息-->  
    <site> 
      <!--部署位置的唯一标识符，用来匹配站点和settings.xml文件里的配置-->  
      <id>banseon-site</id>  
      <!--部署位置的名称-->  
      <name>business api website</name>  
      <!--部署位置的URL，按protocol://hostname/path形式-->  
      <url>scp://svn.baidu.com/banseon:/var/www/localhost/banseon-web</url> 
    </site>  
    <!--项目下载页面的URL。如果没有该元素，用户应该参考主页。
使用该元素的原因是：帮助定位那些不在仓库里的构件（由于license限制）。-->  
    <downloadUrl/>  
    <!-- 给出该构件在远程仓库的状态。不得在本地项目中设置该元素，
因为这是工具自动更新的。有效的值有：none（默认），
converted（仓库管理员从 Maven 1 POM转换过来），partner（直接从伙伴Maven 2仓库同步过来），deployed（从Maven 2实例部 署），verified（被核实时正确的和最终的）。-->  
    <status/> 
  </distributionManagement>  
  <!--以值替代名称，Properties可以在整个POM中使用，也可以作为触发条件（见settings.xml配置文件里activation元素的说明）。格式是<name>value</name>。-->  
  <properties/> 
</project>
```

# 常用

### 打jar包加入时间戳

```xml
<properties>  
  <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>  
  <maven.build.timestamp.format>yyyy-MM-dd_HH_mm</maven.build.timestamp.format>  
</properties>  

<build>  
  <finalName>${project.artifactId}-${maven.build.timestamp}</finalName>  
</build>
```

### maven-antrun-plugin复制插件

https://blog.csdn.net/qq_33547169/article/details/83059858

如果项目有多个模块，想把打的jar包复制到一个统一路径，在父项目如下配置

```xml
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.6.0</version>
                <configuration>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                    <encoding>${project.build.sourceEncoding}</encoding>
                </configuration>
            </plugin>
            <plugin>
                <!--复制jar包到指定文件目录-->
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-antrun-plugin</artifactId>
                <version>1.8</version>
                <executions>
                    <execution>
                        <id>copy</id>
                        <phase>package</phase> <!-- maven生命周期 -->
                        <configuration>
                            <tasks> <!-- 其他语法自行百度maven-antrun-plugin插件的相关用法-->
                                <copy todir="E:\word-log\plugins\" overwrite="true"> <!--  文件将要输出目录 ,overwrite是否重写-->
                                    <fileset dir="${project.build.directory}"> <!-- 待拷贝文件的目录，默认目录在项目的target下-->
<!--                                        <include name="${project.artifactId}.jar" /> &lt;!&ndash; 待拷贝文件，可以使用下面的通配符，还可以是其他目录的文件。此处需要与project.bulid.finalName对应，否则可能找不到文件 &ndash;&gt;-->
                                         <include name="*.jar" />
                                    </fileset>
                                </copy>
                                <!--打印-->
                                <echo message="完成复制"/>
                            </tasks>
                        </configuration>
                        <goals>
                            <goal>run</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
```
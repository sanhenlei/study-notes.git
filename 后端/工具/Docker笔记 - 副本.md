## 安装

### linux   Centos下安装

**在线安装**

```shell
# 1、yum 包更新到最新 
yum update
# 2、安装需要的软件包， yum-util 提供yum-config-manager功能，另外两个是devicemapper驱动依赖的 
yum install -y yum-utils device-mapper-persistent-data lvm2
# 3、 设置阿里yum源
yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo 

# 4、 安装docker，出现输入的界面都按 y 
yum install docker-ce docker-ce-cli containerd.io
######下载比较慢或失败，可以用高速安装
curl -sSL https://get.daocloud.io/docker | sh

# 开机自启
systemctl enable docker 
# 启动docker服务  
systemctl start docker

# 5、 查看docker版本，验证是否验证成功
docker -v
```

**离线安装**

1、百度云Docker 18.06.1地址https://pan.baidu.com/s/18xsHuuTNKiql9YazJRNecw    提取码：pfwt

```shell
1、解压
tar -xvf docker-18.06.1-ce.tgz

2、将解压出来的docker文件内容移动到 /usr/bin/ 目录下
cp docker/* /usr/bin/

3、将docker注册为service
vim /etc/systemd/system/docker.service

将下列配置加到docker.service中并保存
[Unit]
Description=Docker Application Container Engine
Documentation=https://docs.docker.com
After=network-online.target firewalld.service
Wants=network-online.target
[Service]
Type=notify
# the default is not to use systemd for cgroups because the delegate issues still
# exists and systemd currently does not support the cgroup feature set required
# for containers run by docker
ExecStart=/usr/bin/dockerd
ExecReload=/bin/kill -s HUP $MAINPID

# Having non-zero Limit*s causes performance problems due to accounting overhead
# in the kernel. We recommend using cgroups to do container-local accounting.
LimitNOFILE=infinity
LimitNPROC=infinity
LimitCORE=infinity

# Uncomment TasksMax if your systemd version supports it.
# Only systemd 226 and above support this version.
#TasksMax=infinity
TimeoutStartSec=0

# set delegate yes so that systemd does not reset the cgroups of docker containers
Delegate=yes

# kill only the docker process, not all processes in the cgroup
KillMode=process

# restart the docker process if it exits prematurely
Restart=on-failure
StartLimitBurst=3
StartLimitInterval=60s

[Install]
WantedBy=multi-user.target

4、启动

chmod +x /etc/systemd/system/docker.service             #添加文件权限并启动docker
systemctl daemon-reload                                                       #重载unit配置文件
systemctl start docker                                                             #启动Docker
systemctl enable docker.service                                           #设置开机自启
5、验证
systemctl status docker                                                         #查看Docker状态
docker -v                                                                                     #查看Docker版本
```





2、Docker 17.03.2

需要下载一个selinux包：[docker-ce-selinux-17.03.2.ce-1.el7.centos.noarch.rpm](https://download.docker.com/linux/centos/7/x86_64/stable/Packages/docker-ce-selinux-17.03.2.ce-1.el7.centos.noarch.rpm)
和docker包：[docker-ce-17.03.2.ce-1.el7.centos.x86_64.rpm](https://download.docker.com/linux/centos/7/x86_64/stable/Packages/docker-ce-17.03.2.ce-1.el7.centos.x86_64.rpm)
注意版本一定要对应

已经上传百度网盘链接：链接：https://pan.baidu.com/s/1krooEF5X5ZeOZdf71yc0nw 提取码：48ad 

下载安装包后，拷贝到linx环境

```shell
#先安装selinux包：
rpm -ivh docker-ce-selinux-17.03.2.ce-1.el7.centos.noarch.rpm 

#然后安装docker包：
rpm -ivh docker-ce-17.03.2.ce-1.el7.centos.x86_64.rpm
```

### Windows安装

#### 电脑不支持Hyper-V，使用docker-toolbox

参考https://blog.csdn.net/qq_41380248/article/details/89013751

下载docker-toolbox
https://mirrors.aliyun.com/docker-toolbox/windows/docker-toolbox/

安装后有三个软件

![image-20200715092121540](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%B7%A5%E5%85%B7/image/Docker%E7%AC%94%E8%AE%B0/image-20200715092121540.png)

Oracle VM VirtualBox：是一个虚拟机管理软件，类似于vmware支持linux、windows等



ssh连接ip：192.168.99.100登陆虚拟主机。

用户名：docker 

密码： tcuser





#### 电脑支持Hyper-V的安装：



## 框架

![image-20200506171426140](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%B7%A5%E5%85%B7/image/Docker%E7%AC%94%E8%AE%B0/image-20200506171426140.png)

### 配置Docker镜像加速器

默认情况下，将从dockerhub（https://hub.docker.com/）上下载docker镜像，太慢，一般都会配置镜像加速器：

- ​	USTC: 中科大镜像加速器
- 阿里云
- 网易云
- 腾讯云

这里使用[阿里云](https://cr.console.aliyun.com/cn-hangzhou/instances/mirrors/)镜像：登陆阿里云》进入控制台》搜索镜像加速器

```
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://of1kymcg.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```



![image-20200506181810637](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%B7%A5%E5%85%B7/image/Docker%E7%AC%94%E8%AE%B0/image-20200506181810637.png)

## Docker命令

### 服务相关的命令

- 启动服务：systemctl start docker
- 停止服务：systemctl stop docker
- 重启服务：systemctl restart docker
- 查看服务状态：systemctl status docker
- 开启启动服务：systemctl enable docker

### 镜像相关的命令

| 命令                                    | 说明                                       |
| --------------------------------------- | ------------------------------------------ |
| docker images                           | 列出本地镜像                               |
| docker images  ubuntu                   | 列出本地镜像中REPOSITORY为ubuntu的镜像列表 |
| docker images -q                        | 列出本地所有镜像的id                       |
| docker search redis                     | 查找远端redis镜像                          |
| docker pull redis                       | 下载redis镜像版本号为latest                |
| docker pull redis:4.0                   | 下载4.0版本的redis镜像                     |
| docker rmi -f IMAGE [IMAGE...]          | 删除本地一个或多少镜像，**-f :**强制删除； |
| docker rmi 'docker images -q'           | 删除本地所有镜像                           |
| docker export c_redis > c_redis.tar     | 容器导出                                   |
| docker import c_redis.tar c_redis:tag   | 容器                                       |
| docker save cd6d8154f1e1 > my_redis.tar | 镜像导出                                   |
| docker load < my_redis.tar              | 镜像导入                                   |

> 查看有哪些镜像版本号：在https://hub.docker.com/网站进行搜索

```shell
[root@MiWiFi-R3-srv ~]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
[root@MiWiFi-R3-srv ~]# docker search redis
NAME                             DESCRIPTION                                     STARS               OFFICIAL            AUTOMATED
redis                            Redis is an open source key-value store that…   8118                [OK]                
bitnami/redis                    Bitnami Redis Docker Image                      144                                     [OK]
sameersbn/redis                                                                  80                                      [OK]
[root@MiWiFi-R3-srv ~]# docker pull redis
Using default tag: latest
latest: Pulling from library/redis
54fec2fa59d0: Pull complete 
9c94e11103d9: Pull complete 
04ab1bfc453f: Pull complete 
a22fde870392: Pull complete 
def16cac9f02: Pull complete 
1604f5999542: Pull complete 
Digest: sha256:f7ee67d8d9050357a6ea362e2a7e8b65a6823d9b612bc430d057416788ef6df9
Status: Downloaded newer image for redis:latest
docker.io/library/redis:latest
[root@MiWiFi-R3-srv ~]# docker images      
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
redis               latest              f9b990972689        4 days ago          104MB
[root@MiWiFi-R3-srv ~]# 
```



### 容器相关的命令

| 命令                                        | 说明                                                         |
| ------------------------------------------- | ------------------------------------------------------------ |
| docker ps                                   | 查看正在运行的容器                                           |
| docker ps -a                                | 显示所有的容器，包括未运行的和历史的                         |
| docker run -it --name=c1 centos:7 /bin/bash | **-i:** 以交互模式运行容器，通常与 -t 同时使用；<br>**-t:** 为容器重新分配一个伪输入终端，通常与 -i 同时使用；<br>**--name="nginx-lb":** 为容器指定一个名称；<br>`这种进入，exit退出后容器会关闭` |
| docker run -id --name=c2 centos:7           | **-d:** 后台运行容器，并返回容器ID；                         |
| docker exec -it c2 /bin/bash                | 进入容器<br/>`这种进入，exit退出后容器不关闭`                |
| docker stop c2                              | 停止容器                                                     |
| docker start c2                             | 启动容器                                                     |
| docker rm c1                                | 删除容器                                                     |
| docker inspect c2                           | 查看容器信息                                                 |
| docker network list                         | 查看网络信息                                                 |

## 容器的数据卷

### 数据卷概念及作用

- Docker容器删除后，在容器中产生的数据还在吗？
- Docker容器和外部机器可以直接交换文件吗?
- 容器之间能进行数据交互吗？

**数据卷**

- `数据卷是宿主机中的一个目录或文件`
- 当容器目录和数据卷目录绑定后，对方的修改会立即同步
- 一个数据卷可以被多个容器同时挂载
- 一个容器可以被挂载多个数据卷

![image-20200506221550935](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%B7%A5%E5%85%B7/image/Docker%E7%AC%94%E8%AE%B0/image-20200506221550935.png)

**数据卷作用**

- 容器数据持久化
- 外部机器和容器间通信
- 容器之间数据交换

### 配置数据卷

- 创建启动容器时，使用-v参数设置数据卷

  ```shell
  docker run ... -v 宿主机目录(文件):容器内目录(文件) ..
  #例如：
  docker run -it --name=c1 -v /root/data:/root/data_container centos:7 /bin/bash
  ```

- 注意事项：

  1. 目录必须是绝对路径
  2. 如果目录不存在，会自动创建
  3. 可以挂载多个数据卷

### 配置数据卷容器

多容器进行数据交换的方法：

- 多个容器挂载同一个数据卷
- 数据卷容器

![image-20200506223510681](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%B7%A5%E5%85%B7/image/Docker%E7%AC%94%E8%AE%B0/image-20200506223510681.png)

1. 创建启动c3数据卷容器，使用-v参数 设置数据卷

   ```shell
   docker run -it --name=c3 -v /volume centos:7 /bin/bash
   ```

2. 创建启动c1、c2容器，使用--volumes-from参数设置数据卷

   ```shell
   docker run -it --name=c1 --volumes-from c3 centos:7 /bin/bash
   
   docker run -it --name=c2 --volumes-from c3 centos:7 /bin/bash
   ```



## DockerFile

> DockerFile是用来构建Docker镜像的构建文件，是有一系列命令和参数构成的脚本。

| 保留字指令 | 说明                                                         |
| ---------- | ------------------------------------------------------------ |
| FROM       | 基础镜像，当前新镜像是基于哪个镜像                           |
| MAINTAINER | 镜像维护者的姓名和邮箱地址                                   |
| RUN        | 容器构建时需要运行的命令                                     |
| EXPOSE     | 当前容器对外暴露出的端口                                     |
| WORKDIR    | 指定在创建容器后，终端默认登陆进来的工作目录，不写默认在根目录下 |
| ENV        | 用来在构建镜像过程中设置环境变量                             |
| ADD        | 将宿主机目录下的文件拷贝进镜像且ADD命令会自动处理URL和解压tar压缩包 |
| COPY       | 类似ADD，拷贝文件和目录到镜像中                              |
| VOLUME     | 容器数据卷，用于保存数据和持久化工作                         |
| CMD        | 指定一个容器启动时要运行的命令<br>DockerFile中可以有多个CMD指令，但只有最后一个生效， |
| ENTRYPOINT | 指定一个容器启动时要运行的命令<br/>ENTRYPOINT的目的和CMD一样，都是在指定容器启动程序及参数 |
| ONBUILD    | 当构建一个被继承的DockerFile时运行命令，父镜像在被子继承后父镜像的onbuild被触发 |

>  DockerFile目的从一个现有容器上添加自定义指令后生成一个新的容器

**编写DockerFile**

在根目录下新建mydocker文件夹，并进入，新建一个DockerFile文件

```shell
vim DockerFile

# volume test
FROM centos:7
VOLUME ["/dataVolumeContainer1","/dataVolumeContainer2"]
CMD echo "finished, -------success1"
CMD /bin/bash

FROM centos
RUN yum install -y curl
CMD ["curl", "-s", "http://ip.cn"]
```



**build后生成镜像**

```shell
docker build -f /mydocker/DockerFile -t wxl/centos .
```

**运行镜像**

```shell
docker run -it wxl/centos /bin/bash
```



## Docker应用部署

### 安装tomcat

```shell
#下载镜像
docker pull tomcat

#创建容器
#-p 主机端口:docker容器端口，-P 随机分配端口， -i 交互，-t 终端
docker run -id --name=tomcat -p 80:8080 -v /root/tomcat/webapps:/usr/local/tomcat/webapps -v /root/tomcat/logs:/usr/local/tomcat/logs tomcat

#进入tomcat容器查看webapps目录下是否为空，是就把webapps.dist下的文件都拷贝过去
docker exec -it tomcat /bin/bash

cp -r webapps.dist/* webapps

#外部测试
http://192.168.31.72:8081
```

![image-20200508163323684](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%B7%A5%E5%85%B7/image/Docker%E7%AC%94%E8%AE%B0/image-20200508163323684.png)

### 安装myslq:5.6

```shell
#下载镜像
docker pull mysql:5.6

#创建容器
docker run -p 3306:3306 --name=c_mysql -v /root/mysql/conf:/etc/mysql/conf.d -v /root/mysql/logs:/logs -v /root/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 -d mysql:5.6
#### -e MYSQL_ROOT_PASSWORD=123456：初始化root用户的密码
#### -d 是后台运行容器

#进入容器
docker exec -it c_mysql /bin/bash
#测试容器内连接mysql
root@8d0908c26150:/# mysql -u root -p"123456"
Enter password: 

#数据库备份
docker exec c_mysql sh -c 'exec mysqldump --all-databases -uroot -p"123456"' > /root/all-databases.sql
```

测试外部服务连接

![image-20200507170735976](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%B7%A5%E5%85%B7/image/Docker%E7%AC%94%E8%AE%B0/image-20200507170735976.png)

### 安装redis

```shell
#下载镜像
docker pull redis:4.0

#创建容器
docker run --name=redis -p 6379:6379 -v /root/redis/data:/data -v /root/redis/conf/redis.conf:/usr/local/etc/redis/redis.conf -d redis:4.0 redis-server /usr/local/etc/redis/redis.conf --appendonly yes

#windows下创建
docker run -d --name=redis -p 6379:6379  redis:4.0 redis-server  --appendonly yes  


#### --appendonly yes 开启持久化
#### -d 后台运行

#如果需要修改端口，新建redis.conf文件
vim /root/redis/conf/redis.conf/redis.conf


#测试
[root@MiWiFi-R3-srv ~]# docker exec -it redis redis-cli
127.0.0.1:6379> set k1 v1
OK
127.0.0.1:6379> get k1
"v1"
```

### 安装Oracle

镜像大概3GB

https://blog.csdn.net/hundan_520520/article/details/100735399

### 安装Centos7基础容器

```shell
#拉取
docker pull centos:7.2.1511

#运行容器并进入容器
docker run -it centos:7.2.1511 /bin/bash

#先安装wget
yum install -y wget

#将Centos7的yum源更换为国内阿里云的源
#https://finolo.gy/2018/06/%E5%B0%86Centos7%E7%9A%84yum%E6%BA%90%E6%9B%B4%E6%8D%A2%E4%B8%BA%E5%9B%BD%E5%86%85%E9%98%BF%E9%87%8C%E4%BA%91%E7%9A%84%E6%BA%90/
#备份
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
#下载新的CentOS-Base.repo 到/etc/yum.repos.d/
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo

#之后运行yum makecache生成缓存
yum clean all
yum makecache
yum update

#安装常用软件
yum install -y openssh-server openssh-clients vim lrzsz gcc-c++ pcre pcre-devel zlib zlib-devel ruby openssl openssl-devel patch bash-completion zlib.i686 libstdc++.i686 lsof unzip zip iproute initscripts net-tools.x86_64

#执行下面命令
/usr/sbin/sshd-keygen 

#设置root密码
passwd root

#创建data文件夹，方便共享安装包或文件
mkdir /data

#启动sshd服务
/usr/sbin/sshd

#查看进程
ps -ef | grep sshd

#验证sshd服务
ssh root@localhost
```

会遇到连不上去的问题，这时，需要修改`/etc/ssh/sshd_config`配置。

把里面的`UsePAM yes`改为`UsePAM no`



**生成一个新的镜像**

```shell
#退出容器
exit

#查看容器id
docker ps -a

#生成一个新的镜像
docker commit 90553bf74c75 my_centos
```



**建议挂载容器卷**，方便共享安装包或文件

设置vm的共享文件夹

![image-20200715102719949](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%B7%A5%E5%85%B7/image/Docker%E7%AC%94%E8%AE%B0/image-20200715102719949.png)

```shell
#切换到root
sudo -i

mkdir /data

#挂载
sudo mount -t vboxsf data /data

#新建容器时指定容器卷就可以共享文件了
docker run -it --name=hadoop1 -v /data:/data my_centos /bin/bash 
```

### 安装hadoop集群

操作系统： CentOS 7.2 64位

网路设置:

| hostname       | IP         |
| -------------- | ---------- |
| cluster-master | 172.21.0.2 |
| cluster-slave1 | 172.21.0.3 |
| cluster-slave2 | 172.21.0.4 |
| cluster-slave3 | 172.21.0.5 |

#### 1、拉取centos镜像

```shell
docker pull daocloud.io/library/centos:latest
```

#### 2、设置固定IP

```shell
docker network create --subnet=172.31.0.0/16 hadoopnet
```

#### 3、创建容器

> 注意端口映射：hadoop2.x版本是50070，hadoop3.0是9870

```shell
#cluster-master
#-p 设置docker映射到容器的端口 后续查看web管理页面使用
docker run -d --privileged -ti -v /sys/fs/cgroup:/sys/fs/cgroup --name cluster-master -h cluster-master -p 18088:18088 -p 9870:9870 --net hadoopnet --ip 172.31.0.2 my_centos /usr/sbin/init

#cluster-slaves
docker run -d --privileged -ti -v /sys/fs/cgroup:/sys/fs/cgroup --name cluster-slave1 -h cluster-slave1 --net hadoopnet --ip 172.31.0.3 my_centos /usr/sbin/init

docker run -d --privileged -ti -v /sys/fs/cgroup:/sys/fs/cgroup --name cluster-slave2 -h cluster-slave2 --net hadoopnet --ip 172.31.0.4 my_centos /usr/sbin/init

docker run -d --privileged -ti -v /sys/fs/cgroup:/sys/fs/cgroup --name cluster-slave3 -h cluster-slave3 --net hadoopnet --ip 172.31.0.5 my_centos /usr/sbin/init
```

启动控制台并进入`docker`容器中：

```shell
docker exec -it cluster-master /bin/bash

docker exec -it cluster-slave1 /bin/bash
docker exec -it cluster-slave2 /bin/bash
docker exec -it cluster-slave3 /bin/bash

#给每一台设置密码为：root
passwd
```

#### 4、安装OpenSSH免密登录

**`cluster-master`安装：**

```shell
#cluster-master需要修改配置文件（特殊）
#cluster-master

#安装openssh
[root@cluster-master /]# yum -y install openssh openssh-server openssh-clients

[root@cluster-master /]# systemctl start sshd
####ssh自动接受新的公钥
####master设置ssh登录自动添加kown_hosts
[root@cluster-master /]# vi /etc/ssh/ssh_config
#将原来的StrictHostKeyChecking ask
#设置StrictHostKeyChecking为no
#保存
[root@cluster-master /]# systemctl restart sshd
```

**分别对slaves安装OpenSSH**

```shell
#安装openssh
[root@cluster-slave1 /]#yum -y install openssh openssh-server openssh-clients

[root@cluster-slave1 /]# systemctl start sshd
```

**cluster-master公钥分发**

在master机上执行
ssh-keygen -t rsa
并一路回车，完成之后会生成~/.ssh目录，目录下有id_rsa（私钥文件）和id_rsa.pub（公钥文件），再将id_rsa.pub重定向到文件authorized_keys

```
ssh-keygen -t rsa
#一路回车

[root@cluster-master /]# cat ~/.ssh/id_rsa.pub > ~/.ssh/authorized_keys
```

文件生成之后用scp将公钥文件分发到集群slave主机

```
[root@cluster-master /]# ssh root@cluster-slave1 'mkdir ~/.ssh'
[root@cluster-master /]# scp ~/.ssh/authorized_keys root@cluster-slave1:~/.ssh
[root@cluster-master /]# ssh root@cluster-slave2 'mkdir ~/.ssh'
[root@cluster-master /]# scp ~/.ssh/authorized_keys root@cluster-slave2:~/.ssh
[root@cluster-master /]# ssh root@cluster-slave3 'mkdir ~/.ssh'
[root@cluster-master /]# scp ~/.ssh/authorized_keys root@cluster-slave3:~/.ssh
```

分发完成之后测试(ssh root@cluster-slave1)是否已经可以免输入密码登录

#### 5、Ansible安装，自动化管理的工具，用来传文件比较方便，可有可无

```
[root@cluster-master /]# yum -y install epel-release
[root@cluster-master /]# yum -y install ansible
#这样的话ansible会被安装到/etc/ansible目录下
```

此时我们再去编辑ansible的hosts文件

```
vi /etc/ansible/hosts
[cluster]
cluster-master
cluster-slave1
cluster-slave2
cluster-slave3

[master]
cluster-master

[slaves]
cluster-slave1
cluster-slave2
cluster-slave3
```

#### 6、配置docker容器hosts

由于/etc/hosts文件在容器启动时被重写，直接修改内容在容器重启后不能保留，为了让容器在重启之后获取集群hosts，使用了一种启动容器后重写hosts的方法。
需要在每一个容器里中追加以下指令

```
vim ~/.bashrc

:>/etc/hosts
cat >>/etc/hosts<<EOF
127.0.0.1   localhost
172.31.0.2  cluster-master
172.31.0.3  cluster-slave1
172.31.0.4  cluster-slave2
172.31.0.5  cluster-slave3
EOF


#执行
source ~/.bashrc
```

使配置文件生效,可以看到/etc/hosts文件已经被改为需要的内容

```
[root@cluster-master ansible]# cat /etc/hosts
127.0.0.1   localhost
172.18.0.2  cluster-master
172.18.0.3  cluster-slave1
172.18.0.4  cluster-slave2
172.18.0.5  cluster-slave3
```

用ansible分发.bashrc至集群slave下

```
ansible cluster -m copy -a "src=~/.bashrc dest=~/"
```

#### 7、软件环境配置

```shell
#安装jdk1.8，把安装包拷入docker容器
docker cp /data/jdk-8u201-linux-x64.tar.gz cluster-master:/opt/
docker cp /data/jdk-8u201-linux-x64.tar.gz cluster-slave1:/opt/
docker cp /data/jdk-8u201-linux-x64.tar.gz cluster-slave2:/opt/
docker cp /data/jdk-8u201-linux-x64.tar.gz cluster-slave3:/opt/

#安装hadoop，把安装包拷入docker容器
docker cp /data/hadoop-3.2.1.tar.gz cluster-master:/opt/
docker cp /data/hadoop-3.2.1.tar.gz cluster-slave1:/opt/
docker cp /data/hadoop-3.2.1.tar.gz cluster-slave2:/opt/
docker cp /data/hadoop-3.2.1.tar.gz cluster-slave3:/opt/

cd /opt
tar -xf jdk-8u201-linux-x64.tar.gz
tar -xf hadoop-3.2.1.tar.gz
ln -s hadoop-3.2.1 hadoop

#配置环境变量
vim ~/.bashrc

# hadoop
export HADOOP_HOME=/opt/hadoop
export PATH=$HADOOP_HOME/bin:$HADOOP_HOME/sbin:$PATH

#java
export JAVA_HOME=/opt/jdk1.8.0_201
export PATH=$JAVA_HOME/bin:$PATH


source ~/.bashrc
```

#### 8、配置hadoop

cd $HADOOP_HOME/etc/hadoop/

修改vim core-site.xml

```xml
<configuration>
    <property>
        <name>hadoop.tmp.dir</name>
        <value>/home/hadoop/tmp</value>
        <description>A base for other temporary directories.</description>
    </property>
    <!-- file system properties -->
    <property>
        <name>fs.default.name</name>
        <value>hdfs://cluster-master:9000</value>
    </property>
    <property>
    <name>fs.trash.interval</name>
        <value>4320</value>
    </property>
</configuration>
```

修改vim hdfs-site.xml

```xml
<configuration>
<property>
   <name>dfs.namenode.name.dir</name>
   <value>/home/hadoop/tmp/dfs/name</value>
 </property>
 <property>
   <name>dfs.datanode.data.dir</name>
   <value>/home/hadoop/data</value>
 </property>
 <property>
   <name>dfs.replication</name>
   <value>3</value>
 </property>
 <property>
   <name>dfs.webhdfs.enabled</name>
   <value>true</value>
 </property>
 <property>
   <name>dfs.permissions.superusergroup</name>
   <value>staff</value>
 </property>
 <property>
   <name>dfs.permissions.enabled</name>
   <value>false</value>
 </property>
 </configuration>
```

修改vim mapred-site.xml

```xml
<configuration>
<property>
  <name>mapreduce.framework.name</name>
  <value>yarn</value>
</property>
<property>
    <name>mapred.job.tracker</name>
    <value>cluster-master:9001</value>
</property>
<property>
  <name>mapreduce.jobtracker.http.address</name>
  <value>cluster-master:50030</value>
</property>
<property>
  <name>mapreduce.jobhisotry.address</name>
  <value>cluster-master:10020</value>
</property>
<property>
  <name>mapreduce.jobhistory.webapp.address</name>
  <value>cluster-master:19888</value>
</property>
<property>
  <name>mapreduce.jobhistory.done-dir</name>
  <value>/jobhistory/done</value>
</property>
<property>
  <name>mapreduce.intermediate-done-dir</name>
  <value>/jobhisotry/done_intermediate</value>
</property>
<property>
  <name>mapreduce.job.ubertask.enable</name>
  <value>true</value>
</property>
</configuration>
```

修改vim yarn-site.xml

```xml
<configuration>
    <property>
   <name>yarn.resourcemanager.hostname</name>
   <value>cluster-master</value>
 </property>
 <property>
   <name>yarn.nodemanager.aux-services</name>
   <value>mapreduce_shuffle</value>
 </property>
 <property>
   <name>yarn.nodemanager.aux-services.mapreduce.shuffle.class</name>
   <value>org.apache.hadoop.mapred.ShuffleHandler</value>
 </property>
 <property>
   <name>yarn.resourcemanager.address</name>
   <value>cluster-master:18040</value>
 </property>
<property>
   <name>yarn.resourcemanager.scheduler.address</name>
   <value>cluster-master:18030</value>
 </property>
 <property>
   <name>yarn.resourcemanager.resource-tracker.address</name>
   <value>cluster-master:18025</value>
 </property> <property>
   <name>yarn.resourcemanager.admin.address</name>
   <value>cluster-master:18141</value>
 </property>
<property>
   <name>yarn.resourcemanager.webapp.address</name>
   <value>cluster-master:18088</value>
 </property>
<property>
   <name>yarn.log-aggregation-enable</name>
   <value>true</value>
 </property>
<property>
   <name>yarn.log-aggregation.retain-seconds</name>
   <value>86400</value>
 </property>
<property>
   <name>yarn.log-aggregation.retain-check-interval-seconds</name>
   <value>86400</value>
 </property>
<property>
   <name>yarn.nodemanager.remote-app-log-dir</name>
   <value>/tmp/logs</value>
 </property>
<property>
   <name>yarn.nodemanager.remote-app-log-dir-suffix</name>
   <value>logs</value>
 </property>
</configuration>
```

在Hadoop安装目录下找到sbin文件夹

cd $HADOOP_HOME/sbin

在里面修改四个文件

1、对于start-dfs.sh和stop-dfs.sh文件，添加下列参数：

```sh
#!/usr/bin/env bash

HDFS_DATANODE_USER=root
HADOOP_SECURE_DN_USER=hdfs
HDFS_NAMENODE_USER=root
HDFS_SECONDARYNAMENODE_USER=root
```


2、对于start-yarn.sh和stop-yarn.sh文件，添加下列参数：

```sh
#!/usr/bin/env bash
YARN_RESOURCEMANAGER_USER=root
HADOOP_SECURE_DN_USER=yarn
YARN_NODEMANAGER_USER=root
```



使用ansible-playbook分发配置文件至slave主机

```yaml
vim hadoop-dis.yaml

---
- hosts: cluster
  tasks:
    - name: copy core-site to slaves
      copy: src=/opt/hadoop/etc/hadoop/core-site.xml dest=/opt/hadoop/etc/hadoop
    - name: copy hdfs-site to slaves
      copy: src=/opt/hadoop/etc/hadoop/hdfs-site.xml dest=/opt/hadoop/etc/hadoop
    - name: copy mapred-site to slaves
      copy: src=/opt/hadoop/etc/hadoop/mapred-site.xml dest=/opt/hadoop/etc/hadoop
    - name: copy yarn-site to slaves
      copy: src=/opt/hadoop/etc/hadoop/yarn-site.xml dest=/opt/hadoop/etc/hadoop
    - name: copy start-dfs to slaves
      copy: src=/opt/hadoop/sbin/start-dfs.sh dest=/opt/hadoop/sbin
    - name: copy start-yarn to slaves
      copy: src=/opt/hadoop/sbin/start-yarn.sh dest=/opt/hadoop/sbin
    - name: copy stop-dfs to slaves
      copy: src=/opt/hadoop/sbin/stop-dfs.sh dest=/opt/hadoop/sbin
    - name: copy stop-yarn to slaves
      copy: src=/opt/hadoop/sbin/stop-yarn.sh dest=/opt/hadoop/sbin

  handlers:
    - name: exec source
      shell: source ~/.bashrc
```

执行

```shell
ansible-playbook /opt/hadoop/etc/hadoop/hadoop-dis.yaml
```

#### 9、启动hadoop

每个节点都格式化namenode

```shell
mkdir /home/hadoop/data
hadoop namenode -format
```

如果看到storage format success等字样，即可格式化成功

启动每个节点

```shell
cd $HADOOP_HOME/sbin
start-all.sh
```

启动后可使用jps命令查看是否启动成功，

![image-20210121152929513](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%B7%A5%E5%85%B7/image/Docker%E7%AC%94%E8%AE%B0/image-20210121152929513.png)

![image-20210121154656377](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%B7%A5%E5%85%B7/image/Docker%E7%AC%94%E8%AE%B0/image-20210121154656377.png)



#### 10、验证服务

访问

宿主机访问：http://192.168.17.130:18088/         直接访问：http://172.31.0.2:18088 

宿主机访问：http://192.168.17.130:9870             直接访问：http://172.31.0.2:9870

### 安装logstash

一、下载镜像

```shell
#查找
docker search logstash

docker pull prima/logstash
```

二、配置好配置文件/root/logstash/logstash.yml,内容如下

```yaml
path.config: /usr/share/logstash/conf.d/*.conf
path.logs: /var/log/logstash
```

三、添加test.conf文件，内容如下：

```yaml
vim /root/logstash/conf.d/test.conf


input { stdin { } }

filter {
  grok {
    match => { "message" => "%{COMBINEDAPACHELOG}" }
  }
  date {
    match => [ "timestamp" , "dd/MMM/yyyy:HH:mm:ss Z" ]
  }
}
 
output {
  stdout { codec => rubydebug }
}
```

四、启动容器 挂载的conf.d目录是放置一些配置文件，比如要同步从mysql同步数据到es，配置文件就放到这个目录下面（便于管理）

```sh
docker run -it -d -p 5044:5044 -p 5045:5045 --name logstash --net somenetwork -v /root/logstash/logstash.yml:/usr/share/logstash/config/logstash.yml -v /root/logstash/conf.d/:/usr/share/logstash/conf.d/ logstash
```

五、测试

docker exec -it logstash -e 'input { stdin {} } output { stdout {} }'



### 安装Filebeat

一、下载镜像

```sh
#查找
docker search filebeat

docker pull prima/filebeat
```

## Docker-compose应用部署

### Kafka+Zookeeper集群

#### 集群规划

| 容器名         | hostname | Ip addr        | port      |
| -------------- | -------- | -------------- | --------- |
| zk1            | zoo1     | 172.19.0.2     | 2181:2181 |
| zk2            | zoo2     | 172.19.0.3     | 2182:2181 |
| zk3            | zoo3     | 172.19.0.4     | 2183:2181 |
| kafka1         | kafka1   | 随机分配       | 9092:9092 |
| kafka2         | kafka2   | 随机分配       | 9093:9092 |
| kafka3         | kafka3   | 随机分配       | 9094:9092 |
| kafka4         | kafka4   | 随机           | 9095:9092 |
| 宿主机root OSX |          | 192.168.17.130 |           |

在docker里创建一个网络名字为br17219，你们subnet不能和已有的重复

docker network ls

docker network create --driver bridge --subnet 172.20.0.0/16 --gateway=172.20.0.1 br17219 

docker-compose.yml

```yaml
version: '3'
services:
  zoo1:
    image:  zookeeper
    restart: unless-stopped
    hostname: zoo1
    ports:
      - "2181:2181"
      - "2881:2888"
      - "3881:3888"
    container_name: zoo1
    networks:
       zoo_net:
          ipv4_address: 172.19.0.2
    extra_hosts:
      - "zoo1:172.19.0.2"
      - "zoo2:172.19.0.3"
      - "zoo3:172.19.0.4"
    volumes:
      - "/volume/kafka/zoo1data:/data"
      - "/volume/kafka/zoo1data_log:/datalog"
    environment:
      ZOO_MY_ID: 1
      ZOO_SERVERS: server.1=zoo1:2881:3881;2181 server.2=zoo2:2882:3882;2181 server.3=zoo3:2883:3883;2181

  zoo2:
    image:  zookeeper
    restart: unless-stopped
    hostname: zoo2
    container_name: zoo2
    networks:
       zoo_net:
          ipv4_address: 172.19.0.3
    extra_hosts:
      - "zoo1:172.19.0.2"
      - "zoo2:172.19.0.3"
      - "zoo3:172.19.0.4"
    ports:
      - "2182:2181"
      - "2882:2888"
      - "3882:3888"
    volumes:
      - "/volume/kafka/zoo2data:/data"
      - "/volume/kafka/zoo2data_log:/datalog"

    environment:
      ZOO_MY_ID: 2
      ZOO_SERVERS: server.1=zoo1:2881:3881;2181 server.2=zoo2:2882:3882;2181 server.3=zoo3:2883:3883;2181

  zoo3:
    image:  zookeeper
    restart: unless-stopped
    hostname: zoo3
    container_name: zoo3
    networks:
       zoo_net:
          ipv4_address: 172.19.0.4
    extra_hosts:
      - "zoo1:172.19.0.2"
      - "zoo2:172.19.0.3"
      - "zoo3:172.19.0.4"
    ports:
      - "2183:2181"
      - "2883:2888"
      - "3883:3888"
    volumes:
      - "/volume/kafka/zoo3data:/data"
      - "/volume/kafka/zoo3data_log:/datalog"

    environment:
      ZOO_MY_ID: 3
      ZOO_SERVERS: server.1=zoo1:2881:3881;2181 server.2=zoo2:2882:3882;2181 server.3=zoo3:2883:3883;2181

  kafka1:
    image: wurstmeister/kafka
    hostname: kafka1
    ports:
      - "9092:9092"
    extra_hosts:
      - "zoo1:172.19.0.2"
      - "zoo2:172.19.0.3"
      - "zoo3:172.19.0.4"
    networks:
      - zoo_net
    restart: always

    environment:
      KAFKA_ADVERTISED_HOST_NAME: 192.168.17.130                     ## 修改:宿主机IP
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://192.168.17.130:9092    ## 修改:宿主机IP
      KAFKA_ZOOKEEPER_CONNECT: "zoo1:2181"
      KAFKA_ADVERTISED_PORT: 9092
      KAFKA_BROKER_ID: 1
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 3
    volumes:
      - "/volume/kafka/kafka1-data:/kafka"
    depends_on:
      - zoo1
    container_name: kafka1
  kafka2:
    image: wurstmeister/kafka
    hostname: kafka2
    networks:
      - zoo_net
    restart: always
    ports:
      - "9093:9092"
    extra_hosts:
      - "zoo1:172.19.0.2"
      - "zoo2:172.19.0.3"
      - "zoo3:172.19.0.4"
    volumes:
      - "/volume/kafka/kafka2-data:/kafka"
    environment:
      KAFKA_ADVERTISED_HOST_NAME: 192.168.17.130           ## 修改:宿主机IP
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://192.168.17.130:9093   ## 修改:宿主机IP
      KAFKA_ZOOKEEPER_CONNECT: "zoo1:2181"
      KAFKA_ADVERTISED_PORT: 9093
      KAFKA_BROKER_ID: 2
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 3
    depends_on:
      - zoo1
    container_name: kafka2
  kafka3:
    image: wurstmeister/kafka
    hostname: kafka3
    ports:
      - "9094:9092"
    restart: always
    extra_hosts:
      - "zoo1:172.19.0.2"
      - "zoo2:172.19.0.3"
      - "zoo3:172.19.0.4"
    volumes:
      - "/volume/kafka/kafka3-data:/kafka"

    networks:
      - zoo_net
    environment:
      KAFKA_ADVERTISED_HOST_NAME: 192.168.17.130          ## 修改:宿主机IP
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://192.168.17.130:9094   ## 修改:宿主机IP
      KAFKA_ZOOKEEPER_CONNECT: "zoo1:2181"
      KAFKA_ADVERTISED_PORT: 9094
      KAFKA_BROKER_ID: 3
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 3
    depends_on:
      - zoo1
    container_name: kafka3
  kafka4:
    image: wurstmeister/kafka
    hostname: kafka4
    ports:
      - "9095:9092"
    restart: always
    extra_hosts:
      - "zoo1:172.19.0.2"
      - "zoo2:172.19.0.3"
      - "zoo3:172.19.0.4"
    volumes:
      - "/volume/kafka/kafka4-data:/kafka"

    networks:
      - zoo_net
    environment:
      KAFKA_ADVERTISED_HOST_NAME: 192.168.17.130          ## 修改:宿主机IP
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://192.168.17.130:9095   ## 修改:宿主机IP
      KAFKA_ZOOKEEPER_CONNECT: "zoo1:2181"
      KAFKA_ADVERTISED_PORT: 9095
      KAFKA_BROKER_ID: 4
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 3
    depends_on:
      - zoo1
    container_name: kafka4

networks:
  zoo_net:
    ipam:
      config:
      - subnet: 172.19.0.0/16
```



#### 操作

```shell
#### 进入单个容器
docker exec -it kafka1 /bin/bash

#查看集群日志
docker-compose logs -f --tail 50

#查看单个容器日志
docker logs -f kafka1


#查看zookeeper进程
[root@localhost zookeeper]# docker exec -t zk1 zkServer.sh status
ZooKeeper JMX enabled by default
Using config: /conf/zoo.cfg
Client port found: 2181. Client address: localhost.
Mode: follower
[root@localhost zookeeper]# docker exec -t zk2 zkServer.sh status 
ZooKeeper JMX enabled by default
Using config: /conf/zoo.cfg
Client port found: 2181. Client address: localhost.
Mode: follower
[root@localhost zookeeper]# docker exec -t zk3 zkServer.sh status 
ZooKeeper JMX enabled by default
Using config: /conf/zoo.cfg
Client port found: 2181. Client address: localhost.
Mode: leader
[root@localhost zookeeper]# 



####################在容器里测试kafka
#创建topic
docker exec kafka1 kafka-topics.sh --create --topic t1 --partitions 4 --zookeeper zoo1:2181 --replication-factor 3

#获取topic列表
docker exec kafka1 kafka-topics.sh --list --zookeeper zoo1:2181

#进入kafka生产者
docker exec -it kafka1 kafka-console-producer.sh --topic t1 --broker-list kafka1:9092,kafka2:9092,kafka3:9092,kafka4:9092

#进入kafka消费者
docker exec kafka1 kafka-console-consumer.sh --topic t1 --bootstrap-server kafka1:9092,kafka2:9092,kafka3:9092,kafka4:9092  --from-beginning

#修改topic消息大小
docker exec kafka1 kafka-configs.sh --bootstrap-server kafka1:9092,kafka2:9092,kafka3:9092,kafka4:9092 --entity-type topics --entity-name t1 --alter --add-config max.message.bytes=10485760


####################在宿主机上安装一个kafka进行测试
#创建topic
kafka-topics.sh --create --zookeeper 192.168.17.130:2181 --partitions 4 --replication-factor 3 --topic t2

#获取topic列表
kafka-topics.sh --list --zookeeper 192.168.17.130:2181

#生产者
kafka-console-producer.sh --broker-list 192.168.17.130:9092 --topic t2

#消费者
kafka-console-consumer.sh --bootstrap-server 192.168.17.130:9092 --topic t2 --from-beginning


#修改topic消息大小
kafka-configs.sh --bootstrap-server 192.168.17.130:9092 --entity-type topics --entity-name t1 --alter --add-config max.message.bytes=10485760
```

#### **kafka-manager管理工具安装**

vim kafka_manager.yml

```yaml
version: '3.1'
services:
  kafka-manager:
    container_name: kafka-manager
    image: sheepkiller/kafka-manager
    restart: always
    ports:
      - 9000:9000
    environment:
      KM_VERSION: 1.3.3.18
      ZK_HOSTS: zoo1:2181,zoo2:2181,zoo3:2181
    networks:
      br17219:
        ipv4_address: 172.20.0.17

networks:
  br17219:
    external:
      name: br17219
```

启动容器

docker-compose -f kafka-manager.yml up -d

启动后，登陆到docker容器里，将配置文件注掉一条，然后将zk的地址写入，不然界面是出不来的

docker exec -it kafka-manager bash

docker cp kafka-manager:/kafka-manager-1.3.1.8/conf/application.conf ./ 

vi conf/application.conf   #修改下面两个

> kafka-manager.zkhosts="zoo1:2181,zoo2:2181,zoo3:2181"
> #kafka-manager.zkhosts=${?ZK_HOSTS}

docker cp application.conf kafka-manager:/kafka-manager-1.3.1.8/conf

重启容器

docker restart kafka-manager 

查看日志是否有报错

docker logs -f  kafka-manager 

没问题了在浏览器查看

http://192.168.17.130:9000/

![image-20200717164002882](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%B7%A5%E5%85%B7/image/Docker%E7%AC%94%E8%AE%B0/image-20200717164002882.png)



需要添加后监控

![image-20200717164032746](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%B7%A5%E5%85%B7/image/Docker%E7%AC%94%E8%AE%B0/image-20200717164032746.png)

#### **kafkaoffsetmonitor监控工具安装配置**

拉取

docker pull 564239555/kafkaoffsetmonitor

启动容器,`注意net要和zookeeper使用的保持一致`

docker run -itd -p 8089:8080 --name kafkaoffsetmonitor --net kafka-zookeeper_zoo_net 564239555/kafkaoffsetmonitor

复制容器的两个文件拷出来

docker cp kafkaoffsetmonitor:/start.sh ./

docker cp kafkaoffsetmonitor:/cp/kafka-offset-monitor-graphite-assembly-0.1.0-SNAPSHOT.jar ./

修改启动文件

ZK_HOSTS=zoo1:2181,zoo2:2181,zoo3:2181

修改jar包里面offsetapp/index.html文件里的三个script如下

    <script src="//cdn.static.runoob.com/libs/angular.js/1.4.6/angular.min.js"></script>
    <script src="//cdn.static.runoob.com/libs/angular.js/1.4.6/angular-route.js"></script>
    <script src="//cdn.static.runoob.com/libs/angular.js/1.4.6/angular-resource.js"></script>
再拷贝进去

docker cp start.sh kafkaoffsetmonitor:/

docker cp kafka-offset-monitor-graphite-assembly-0.1.0-SNAPSHOT.jar kafkaoffsetmonitor:/cp

重启容器

docker restart kafkaoffsetmonitor

查看启动日志

docker logs -f kafkaoffsetmonitor

修改脚本

vi /kafkaoffsetmonitor/conf/kafkaoffsetmonitor.sh

nohup java -cp KafkaOffsetMonitor-assembly-0.2.1.jar com.quantifind.kafka.offsetapp.OffsetGetterWeb --zk zoo1:2181,zoo2:2181,zoo3:2181 --port 8080 --refresh 10.seconds --retain 2.days &

查看nohup.out日志是否有报错



### Elasticsearch集群

创建文件夹

```shell
mkdir -p /volume/es/config
mkdir /volume/es/{data1,data2,data3}

#主节点配置
vi /volume/es/config/master.yml
cluster.name: meitu
node.name: node-master
node.master: true
node.data: true
http.port: 9200
network.host: 0.0.0.0
#network.publish_host: master-ip
#discovery.zen.ping.unicast.hosts: ["master-ip"]
http.cors.enabled: true
http.cors.allow-origin: "*" 
discovery.zen.minimum_master_nodes: 1
xpack.security.enabled:false

#从节点1
vi /volume/es/config/slave1.yml
cluster.name: meitu
node.name: node-data-1
node.master: false
node.data: true
http.port: 9200
network.host: 0.0.0.0
#network.publish_host: data-ip
discovery.zen.ping.unicast.hosts: ["es1","es3"]
http.cors.enabled: true
http.cors.allow-origin: "*" 
discovery.zen.minimum_master_nodes: 1

#从节点2
vi /volume/es/config/slave2.yml
cluster.name: meitu
node.name: node-data-2
node.master: false
node.data: true
http.port: 9200
network.host: 0.0.0.0
#network.publish_host: data-ip
discovery.zen.ping.unicast.hosts: ["es1","es2"]
http.cors.enabled: true
http.cors.allow-origin: "*" 
discovery.zen.minimum_master_nodes: 1

```

设置

```shell
if [ $(grep 'vm.max_map_count' /etc/sysctl.conf |wc -l) -eq 0 ] ; \
then echo 'vm.max_map_count=655360' >> /etc/sysctl.conf; \
fi

sysctl -p
```



docker-compose.yml

```yaml
version: '2'
services:
  es1:
    image: docker.io/library/elasticsearch:5.6.3
    container_name: es1
    environment:
      - "ES_JAVA_OPTS=-Xms256m -Xmx256m"
    ulimits:
      memlock:
        soft: -1
        hard: -1
      nofile:
        soft: 65536
        hard: 65536
    mem_limit: 1g
    cap_add:
      - IPC_LOCK
    volumes:
      - /volume/es/data1:/usr/share/elasticsearch/data
      - /volume/es/config/master.yml:/usr/share/elasticsearch/config/elasticsearch.yml
    ports:
      - 9201:9200
    networks:
      - esnet
  es2:
    image: docker.io/library/elasticsearch:5.6.3
    container_name: es2
    environment:
      - bootstrap.memory_lock=true
      - "ES_JAVA_OPTS=-Xms256m -Xmx256m"
    ulimits:
      memlock:
        soft: -1
        hard: -1
      nofile:
        soft: 65536
        hard: 65536
    mem_limit: 1g
    cap_add:
      - IPC_LOCK
    volumes:
      - /volume/es/data2:/usr/share/elasticsearch/data
      - /volume/es/config/slave1.yml:/usr/share/elasticsearch/config/elasticsearch.yml
    ports:
      - 9202:9200
    links:
      - es1
    networks:
      - esnet
  es3:
    image: docker.io/library/elasticsearch:5.6.3
    container_name: es3
    environment:
      - bootstrap.memory_lock=true
      - "ES_JAVA_OPTS=-Xms256m -Xmx256m"
    ulimits:
      memlock:
        soft: -1
        hard: -1
      nofile:
        soft: 65536
        hard: 65536
    mem_limit: 1g
    cap_add:
      - IPC_LOCK
    volumes:
      - /volume/es/data3:/usr/share/elasticsearch/data
      - /volume/es/config/slave2.yml:/usr/share/elasticsearch/config/elasticsearch.yml
    ports:
      - 9203:9200
    links:
      - es1
    networks:
      - esnet

volumes:
  esdata1:
    driver: local
  esdata2:
    driver: local
  esdata3:
    driver: local
networks:
  esnet:
    driver: bridge

```

查看日志

docker-compose logs

查看集群健康度

curl http://localhost:9201/_cat/health?v 或  http://192.168.17.130:9201/_cat/health?v

查看节点信息

curl http://localhost:9201/_cat/nodes?pretty 或  http://192.168.17.130:9201/_cat/nodes?v

查看所有索引

curl http://localhost:9201/_cat/indices?v 或 http://192.168.17.130:9201/_cat/indices?v

删除索引

curl -XDELETE http://localhost:9201/test-2021.01.07?pretty

curl  http://localhost/sensor/_search?pretty 或 http://192.168.17.130:9201/sensor/_search?pretty



### Hadoop集群快速部署

#### 部署

docker-compose.yml

```yaml
version: "2.2"

services:
  namenode:
    image: bde2020/hadoop-namenode:1.1.0-hadoop2.7.1-java8
    # 配置好 docker 内的假域名
    hostname: namenode
    container_name: namenode
    restart: always
    volumes:
      - /volume/hadoop/namenode:/hadoop/dfs/name
      - /volume/hadoop/input_files:/hadoop/input_files
    environment:
      - CLUSTER_NAME=test
      # 配置 hdfs 用户权限问题，不需要只允许 hadoop 用户访问
      - HDFS_CONF_dfs_permissions=false
    env_file:
      - ./hadoop.env
    ports:
      - 9000:9000
      - 50070:50070
    #network_mode: 'host'
  
  resourcemanager:
    image: bde2020/hadoop-resourcemanager:1.1.0-hadoop2.7.1-java8
    hostname: resourcemanager
    container_name: resourcemanager
    restart: always
    depends_on:
      - namenode
      - datanode1
      - datanode2
    env_file:
      - ./hadoop.env
    ports:
      - 8030:8030
      - 8031:8031
      - 8032:8032
      - 8033:8033
      - 8088:8088
    #network_mode: 'host'

  historyserver:
    image: bde2020/hadoop-historyserver:1.1.0-hadoop2.7.1-java8
    hostname: historyserver
    container_name: historyserver
    restart: always
    depends_on:
      - namenode
      - datanode1
      - datanode2
    volumes:
      - /volume/hadoop/historyserver:/hadoop/yarn/timeline
    env_file:
      - ./hadoop.env
    ports:
      - "8188:8188"
    #network_mode: 'host'

  nodemanager1:
    image: bde2020/hadoop-nodemanager:1.1.0-hadoop2.7.1-java8
    hostname: nodemanager1
    container_name: nodemanager1
    restart: always
    depends_on:
      - namenode
      - datanode1
      - datanode2
    env_file:
      - ./hadoop.env
    ports:
      - 8040:8040
      - 8041:8041
      - 8042:8042
    #network_mode: 'host'
  
  datanode1:
    image: bde2020/hadoop-datanode:1.1.0-hadoop2.7.1-java8
    hostname: datanode1
    container_name: datanode1
    restart: always
    depends_on:
      - namenode
    volumes:
      - /volume/hadoop/datanode1:/hadoop/dfs/data
    env_file:
      - ./hadoop.env
    environment:
      # 等价于在 hdfs-site.xml 中配置 dfs.datanode.address
      - HDFS_CONF_dfs_datanode_address=0.0.0.0:50011
      # dfs.datanode.ipc.address 不使用默认端口的意义是在同一机器起多个 datanode，暴露端口需要不同
      - HDFS_CONF_dfs_datanode_ipc_address=0.0.0.0:50021
      # dfs.datanode.http.address
      - HDFS_CONF_dfs_datanode_http_address=0.0.0.0:50071
    ports:
      - 50011:50011
      - 50021:50021
      - 50071:50071
    #network_mode: 'host'
  
  datanode2:
    image: bde2020/hadoop-datanode:1.1.0-hadoop2.7.1-java8
    hostname: datanode2
    container_name: datanode2
    restart: always
    depends_on:
      - namenode
    volumes:
      - /volume/hadoop/datanode2:/hadoop/dfs/data
    env_file:
      - ./hadoop.env
    environment:
      - HDFS_CONF_dfs_datanode_address=0.0.0.0:50012
      - HDFS_CONF_dfs_datanode_ipc_address=0.0.0.0:50022
      - HDFS_CONF_dfs_datanode_http_address=0.0.0.0:50072
    ports:
      - 50012:50012
      - 50022:50022
      - 50072:50072
    #network_mode: 'host'
  
  datanode3:
    image: bde2020/hadoop-datanode:1.1.0-hadoop2.7.1-java8
    hostname: datanode3
    container_name: datanode3
    restart: always
    depends_on:
      - namenode
    volumes:
      - /volume/hadoop/datanode3:/hadoop/dfs/data
    env_file:
      - ./hadoop.env
    environment:
      - HDFS_CONF_dfs_datanode_address=0.0.0.0:50013
      - HDFS_CONF_dfs_datanode_ipc_address=0.0.0.0:50023
      - HDFS_CONF_dfs_datanode_http_address=0.0.0.0:50073
    ports:
      - 50013:50013
      - 50023:50023
      - 50073:50073
    #network_mode: 'host'

```

在当前目录新建文件：hadoop.env

```cpp
CORE_CONF_fs_defaultFS=hdfs://namenode:9000
CORE_CONF_hadoop_http_staticuser_user=root
CORE_CONF_hadoop_proxyuser_hue_hosts=*
CORE_CONF_hadoop_proxyuser_hue_groups=*

HDFS_CONF_dfs_webhdfs_enabled=true
HDFS_CONF_dfs_permissions_enabled=false

YARN_CONF_yarn_log___aggregation___enable=true
YARN_CONF_yarn_resourcemanager_recovery_enabled=true
YARN_CONF_yarn_resourcemanager_store_class=org.apache.hadoop.yarn.server.resourcemanager.recovery.FileSystemRMStateStore
YARN_CONF_yarn_resourcemanager_fs_state___store_uri=/rmstate
YARN_CONF_yarn_nodemanager_remote___app___log___dir=/app-logs
YARN_CONF_yarn_log_server_url=http://historyserver:8188/applicationhistory/logs/
YARN_CONF_yarn_timeline___service_enabled=true
YARN_CONF_yarn_timeline___service_generic___application___history_enabled=true
YARN_CONF_yarn_resourcemanager_system___metrics___publisher_enabled=true
YARN_CONF_yarn_resourcemanager_hostname=resourcemanager
YARN_CONF_yarn_timeline___service_hostname=historyserver
YARN_CONF_yarn_resourcemanager_address=resourcemanager:8032
YARN_CONF_yarn_resourcemanager_scheduler_address=resourcemanager:8030
YARN_CONF_yarn_resourcemanager_resource___tracker_address=resourcemanager:8031
```

构建启动容器

docker-compose up -d

#### 集群说明

查看集群IP：docker inspect  `docker ps | grep hadoop | awk '{print $1}'` | grep IPAdd | grep 172

| 命令                                             | hostsname       | ip         | port                                                         | 作用                                                         |
| ------------------------------------------------ | --------------- | ---------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| docker inspect nodemanager1 \| grep IPAddress    | nodemanager1    | 172.21.0.6 | 8040:8040<br/>8041:8041<br/>8042:8042                        | 管理一个YARN集群中的每一个节点，比如监视资源使用情况         |
| docker inspect resourcemanager \| grep IPAddress | resourcemanager | 172.21.0.8 | 8030:8030<br/>8031:8031<br/>8032:8032<br/>8033:8033<br/>8088:8088 | 是Yarn集群主控节点，负责协调和管理整个集群（所有NodeManager）的资源 |
| docker inspect historyserver \| grep IPAddress   | historyserver   | 172.21.0.7 | 8188:8188                                                    | 可通过网页或RPC方式获取作业的信息                            |
| docker inspect datanode1 \| grep IPAddress       | datanode1       | 172.21.0.4 | 50011:50011<br/>50021:50021<br/>50071:50071                  | 储了实际的数据                                               |
| docker inspect datanode2  \| grep IPAddress      | datanode2       | 172.21.0.5 | 50012:50012<br/>50022:50022<br/>50072:50072                  | 储了实际的数据                                               |
| docker inspect datanode3 \| grep IPAddress       | datanode3       | 172.21.0.3 | 50013:50013<br/>50023:50023<br/>50073:50073                  | 储了实际的数据                                               |
| docker inspect namenode \| grep IPAddress        | namenode        | 172.21.0.2 | 9000:9000<br/>50070:50070                                    | 管理文件系统的元数据                                         |

一个HDFS集群主要由一个NameNode和很多个Datanode组成

在客户端host加入

192.168.17.130 resourcemanager
192.168.17.130 nodemanager1
192.168.17.130 namenode
192.168.17.130 historyserver
192.168.17.130 datanode1
192.168.17.130 datanode2
192.168.17.130 datanode3



在宿主机hosts里加入映射

vim /etc/hosts

172.21.0.2  namenode
172.21.0.3  datanode3
172.21.0.4  datanode1
172.21.0.5  datanode2
172.21.0.6  nodemanager1
172.21.0.7  historyserver
172.21.0.8  resourcemanager



#### 访问

```ruby
Namenode: http://192.168.17.130:50070/dfshealth.html#tab-overview
Datanode1: http://192.168.17.130:50071/
Datanode2: http://192.168.17.130:50072/
Nodemanager: http://192.168.17.130:8042/node
Resource manager: http://192.168.17.130:8088/cluster
History server: http://192.168.17.130:8188/applicationhistory
```

http://192.168.17.130:50070/dfshealth.html#tab-overview

![image-20200811161057953](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%B7%A5%E5%85%B7/image/Docker%E7%AC%94%E8%AE%B0/image-20200811161057953.png)

http://192.168.17.130:8042/node

![image-20200811161250211](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%B7%A5%E5%85%B7/image/Docker%E7%AC%94%E8%AE%B0/image-20200811161250211.png)

http://192.168.17.130:8088/cluster

![image-20200811161025113](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%B7%A5%E5%85%B7/image/Docker%E7%AC%94%E8%AE%B0/image-20200811161025113.png)

http://192.168.17.130:8188/applicationhistory

![image-20200811161438199](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%B7%A5%E5%85%B7/image/Docker%E7%AC%94%E8%AE%B0/image-20200811161438199.png)



#### 使用

```shell
# 进入容器
docker exec -it nodemanager1 bash
# 查看文件夹
hadoop fs -ls /
# 创建文件夹
hadoop fs -mkdir /xdr
# 删除文件夹
hadoop fs -rm -r /xdr


```



### Hadoop集群完整部署

使用docker部署zookeeper、kafka、hadoop、yarn、hbase、sqoop、flume、hive、spark

1、Hadoop是Apache公司的大数据处理套件，是所有大数据组件的总称。目前Hadoop已从1.0发展至3.0时代。
2、HDFS把所有廉价的pc服务器组成了一个超级大硬盘，且通过多副本保证了数据安全。
3、MapReduce用于把一个超级大的数据文件分解至不同的廉价pc服务器进行处理，并最终返回数据处理结果。
4、Yarn用于优化mapreduce的处理框架，是一个资源调度组件，让mapreduce处理更加稳健、高效。
5、Zookeeper是一个协调组件，很多组件都依赖于它的运行。如选择HA领导、实现Mysql的高可用。它相当于是一个领导角色，负责协调资源的高可用运行。
6、Sqoop是一个ETL工具，负责各类数据库（Mysql等）与hadoop存储之间的互相倒换。
7、Hive是一个mapreduce之上的神器，你通过sql命令就可以代替mapreduce的编程。
8、Spark是MapReduce的升级替换组件，基于内存计算，数据处理速度提高10-100倍。
9、Kafka是一个队列工具，数据、消息的排队全靠它，有了它的帮助，数据的堵塞问题不再是个事。
10、Flume是一个前端日志采用工具，部署在web等前端服务器，将日志数据源源不断进行采集。
11、HBase是数据的海量存储仓库，是一个Nosql数据库，可以保障数据的海量存储。
12、Pig是另一个简化版的Mapreduce上层处理工具，通过简单的脚本即可生成Mapreduce程序进行快速的数据处理。

>  Hadoop有三种分布模式：单机模式、伪分布、全分布模式，本文讲解全分布式搭建方式

#### Hadoop介绍

一些概念介绍和操作教程 官网：http://hadoop.apache.org/docs/r1.0.4/cn/index.html

Hadoop的四大组件：**common、HDFS、MapReduce、YARN**

**common(工具类)**：

包括Hadoop常用的工具类，由原来的Hadoopcore部分更名而来。主要包括系统配置工具Configuration、远程过程调用RPC、序列化机制和Hadoop抽象文件系统FileSystem等。它们为在通用硬件上搭建云计算环境提供基本的服务，并为运行在该平台上的软件开发提供了所需的API。

**Hadoop Distributed File System(HDFS)**：

Hadoop实现了一个分布式的文件系统，HDFS为海量的数据提供了存储。HDFS是基于节点的形式进行构建的，里面有一个父节点NameNode，他在机器内部提供了服务，NameNode本身不干活，NameNode将数据分成块，只是把数据分发给子节点，交由子节点来进行存储，由于只存在一个父节点，所以这是HDFS的一个缺点，单点失败。以及n个子节点dataNode,dataNode在机器内部提供了数据块，存储在HDFS的数据被分成块，然后将这些块分到多个计算机(dataNode)中，这与传统的RAID架构大有不同。块的大小（通常为64MB）和复制的块数量在创建文件时由客户机决定。NameNode可以控制所有文件操作。

![image-20200812154255441](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%B7%A5%E5%85%B7/image/Docker%E7%AC%94%E8%AE%B0/image-20200812154255441.png)、、



**NameNode**：

NameNode 是一个通常在 HDFS实例中的单独机器上运行的软件。它负责管理文件系统名称空间和控制外部客户机的访问。NameNode 决定是否将文件映射到 DataNode 上的复制块上。对于最常见的 3 个复制块，第一个复制块存储在同一机架的不同节点上，最后一个复制块存储在不同机架的某个节点上。

**DataNode**:

DataNode 也是一个通常在HDFS实例中的单独机器上运行的软件。Hadoop 集群包含一个 NameNode 和大量 DataNode。DataNode 通常以机架的形式组织，机架通过一个交换机将所有系统连接起来。Hadoop 的一个假设是：机架内部节点之间的传输速度快于机架间节点的传输速度。

**MapReduce:**

 基于YARN的大型数据集并行处理系统。是一种计算模型，用以进行大数据量的计算。Hadoop的MapReduce实现，和Common、HDFS一起，构成了Hadoop发展初期的三个组件。MapReduce将应用划分为Map和Reduce两个步骤，其中Map对数据集上的独立元素进行指定的操作，生成键-值对形式中间结果。Reduce则对中间结果中相同“键”的所有“值”进行规约，以得到最终结果。MapReduce这样的功能划分，非常适合在大量计算机组成的分布式并行环境里进行数据处理。

**YARN:**

分布式集群资源管理框架，管理者集群的资源（Memory,cpu core）

​      合理调度分配给各个程序（MapReduce）使用

​      主节点：resourceManager

​         掌管集群中的资源

​      从节点：nodeManager

​         管理每台集群资源

#### 部署

docker安装参考：https://www.jianshu.com/p/293370799c6f

https://www.cnblogs.com/coolwxb/p/10975352.html

### Hive环境部署

```shell
# 下载安装文件
git clone https://gitee.com/sanhenlei/docker-hive.git

# 注：本文所有docker-compose都是在docker-hive目录下执行的
cd docker-hive 

# 这步在后台起一个hive，元数据库用的是postgresql # 会费一点时间，需要耐心等待 
docker-compose up -d

# 进入bash
docker-compose exec hive-server bash

# 使用beeline客户端连接
/opt/hive/bin/beeline -u jdbc:hive2://localhost:10000
```

## Docker-Compose

### 介绍

Docker-Compose项目是Docker官方的开源项目，负责实现对Docker容器集群的快速编排。
Docker-Compose将所管理的容器分为三层，分别是工程（project），服务（service）以及容器（container）。Docker-Compose运行目录下的所有文件（docker-compose.yml，extends文件或环境变量文件等）组成一个工程，若无特殊指定工程名即为当前目录名。一个工程当中可包含多个服务，每个服务中定义了容器运行的镜像，参数，依赖。一个服务当中可包括多个容器实例，Docker-Compose并没有解决负载均衡的问题，因此需要借助其它工具实现服务发现及负载均衡。
Docker-Compose的工程配置文件默认为docker-compose.yml，可通过环境变量COMPOSE_FILE或-f参数自定义配置文件，其定义了多个有依赖关系的服务及每个服务运行的容器。
使用一个Dockerfile模板文件，可以让用户很方便的定义一个单独的应用容器。在工作中，经常会碰到需要多个容器相互配合来完成某项任务的情况。例如要实现一个Web项目，除了Web服务容器本身，往往还需要再加上后端的数据库服务容器，甚至还包括负载均衡容器等。
Compose允许用户通过一个单独的docker-compose.yml模板文件（YAML 格式）来定义一组相关联的应用容器为一个项目（project）。
Docker-Compose项目由Python编写，调用Docker服务提供的API来对容器进行管理。因此，只要所操作的平台支持Docker API，就可以在其上利用Compose来进行编排管理。

### 安装

yum -y install  epel-release

yum -y install python-pip

pip install --upgrade pip

pip install docker-compose

docker-compose version

#### 另一种 安装

进入vm里

```shell
#切换root
sudo -i
#x下载安装
curl -L https://get.daocloud.io/docker/compose/releases/download/1.12.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose

chmod +x /usr/local/bin/docker-compose
exit
docker-compose version

```

### 常用命令

#### Docker-compose命令格式

```
docker-compose [-f <arg>...] [options] [COMMAND] [ARGS...]
选项包括
-f --file FILE指定Compose模板文件，默认为docker-compose.yml
-p --project-name NAME 指定项目名称，默认使用当前所在目录为项目名
--verbose  输出更多调试信息
-v，-version 打印版本并退出
--log-level LEVEL 定义日志等级(DEBUG, INFO, WARNING, ERROR, CRITICAL)
```

#### docker-compose up

```
docker-compose up [options] [--scale SERVICE=NUM...] [SERVICE...]
选项包括：
-d 在后台运行服务容器
-no-color 不是有颜色来区分不同的服务的控制输出
-no-deps 不启动服务所链接的容器
--force-recreate 强制重新创建容器，不能与-no-recreate同时使用
–no-recreate 如果容器已经存在，则不重新创建，不能与–force-recreate同时使用
–no-build 不自动构建缺失的服务镜像
–build 在启动容器前构建服务镜像
–abort-on-container-exit 停止所有容器，如果任何一个容器被停止，不能与-d同时使用
-t, –timeout TIMEOUT 停止容器时候的超时（默认为10秒）
–remove-orphans 删除服务中没有在compose文件中定义的容器
```

####  docker-compose ps

```
docker-compose ``ps` `[options] [SERVICE...]
列出项目中所有的容器
```

####  docker-compose stop

```
docker-compose stop [options] [SERVICE...]
选项包括
-t, –timeout TIMEOUT 停止容器时候的超时（默认为10秒）
docker-compose stop
停止正在运行的容器，可以通过docker-compose start 再次启动
```

####  docker-compose -h

```
docker-compose -h
查看帮助
```

####  docker-compose down

```
docker-compose down [options]
停止和删除容器、网络、卷、镜像。
选项包括：
–rmi type，删除镜像，类型必须是：all，删除compose文件中定义的所有镜像；local，删除镜像名为空的镜像
-v, –volumes，删除已经在compose文件中定义的和匿名的附在容器上的数据卷
–remove-orphans，删除服务中没有在compose中定义的容器
docker-compose down
停用移除所有容器以及网络相关
```

####  docker-compose logs

```
docker-compose logs [options] [SERVICE...]
查看服务容器的输出。默认情况下，docker-compose将对不同的服务输出使用不同的颜色来区分。可以通过–no-color来关闭颜色。
docker-compose logs
查看服务容器的输出
-f 跟踪日志输出
```

####  docker-compose build

```
docker-compose build [options] [--build-arg key=val...] [SERVICE...]
构建（重新构建）项目中的服务容器。
选项包括：
–compress 通过gzip压缩构建上下环境
–force-rm 删除构建过程中的临时容器
–no-cache 构建镜像过程中不使用缓存
–pull 始终尝试通过拉取操作来获取更新版本的镜像
-m, –memory MEM为构建的容器设置内存大小
–build-arg key=val为服务设置build-time变量
服务容器一旦构建后，将会带上一个标记名。可以随时在项目目录下运行docker-compose build来重新构建服务
```

####  docker-compose pull

```
docker-compose pull [options] [SERVICE...]
拉取服务依赖的镜像。
选项包括：
–ignore-pull-failures，忽略拉取镜像过程中的错误
–parallel，多个镜像同时拉取
–quiet，拉取镜像过程中不打印进度信息
docker-compose pull
拉取服务依赖的镜像
```

####  docker-compose restart

```
docker-compose restart [options] [SERVICE...]
重启项目中的服务。
选项包括：
-t, –timeout TIMEOUT，指定重启前停止容器的超时（默认为10秒）
docker-compose restart
重启项目中的服务
```

####  docker-compose rm

```
docker-compose ``rm` `[options] [SERVICE...]
删除所有（停止状态的）服务容器。
选项包括：
–f, –force，强制直接删除，包括非停止状态的容器
-v，删除容器所挂载的数据卷
docker-compose rm
删除所有（停止状态的）服务容器。推荐先执行docker-compose stop命令来停止容器。
```

####  docker-compose start

```
docker-compose start [SERVICE...]
启动已经存在的服务容器。
```

####  docker-compose run

```
docker-compose run [options] [-``v` `VOLUME...] [-p PORT...] [-e KEY=VAL...] SERVICE [COMMAND] [ARGS...]
在指定服务上执行一个命令。
docker-compose run ubuntu ping www.baidu.com
在指定容器上执行一个ping命令。
```

####  docker-compose scale

```
docker-compose scale web=3 db=2
设置指定服务运行的容器个数。通过service=num的参数来设置数量
```

#### docker-compose pause

```
docker-compose pause [SERVICE...]
暂停一个服务容器
```

#### docker-compose kill

```
docker-compose kill [options] [SERVICE...]
通过发送SIGKILL信号来强制停止服务容器。
支持通过-s参数来指定发送的信号，例如通过如下指令发送SIGINT信号：
docker-compose kill -s SIGINT
```

#### docker-compose config

```
docker-compose config [options]
验证并查看compose文件配置。
选项包括：
–resolve-image-digests 将镜像标签标记为摘要
-q, –quiet 只验证配置，不输出。 当配置正确时，不输出任何内容，当文件配置错误，输出错误信息
–services 打印服务名，一行一个
–volumes 打印数据卷名，一行一个
```

#### docker-compose create

```
docker-compose create [options] [SERVICE...]
为服务创建容器。
选项包括：
–force-recreate：重新创建容器，即使配置和镜像没有改变，不兼容–no-recreate参数
–no-recreate：如果容器已经存在，不需要重新创建，不兼容–force-recreate参数
–no-build：不创建镜像，即使缺失
–build：创建容器前　　，生成镜像
```

#### docker-compose exec

```
docker-compose exec [options] SERVICE COMMAND [ARGS...]
选项包括：
-d 分离模式，后台运行命令。
–privileged 获取特权。
–user USER 指定运行的用户。
-T 禁用分配TTY，默认docker-compose exec分配TTY。
–index=index，当一个服务拥有多个容器时，可通过该参数登陆到该服务下的任何服务，例如：docker-compose exec –index=1 web /bin/bash ，web服务中包含多个容器

```

#### docker-compose port

```
docker-compose port [options] SERVICE PRIVATE_PORT
显示某个容器端口所映射的公共端口。
选项包括：
–protocol=proto，指定端口协议，TCP（默认值）或者UDP
–index=index，如果同意服务存在多个容器，指定命令对象容器的序号（默认为1）
```

#### docker-compose push

```
docker-compose push [options] [SERVICE...]
推送服务依的镜像。
选项包括：``–ignore-push-failures 忽略推送镜像过程中的错误
```

#### docker-compose stop

```
docker-compose stop [options] [SERVICE...]
停止运行的容器
```

#### docker-compose uppause

```
docker-compose unpause [SERVICE...]
恢复处于暂停状态中的服务。
```

### **YAML常用的字段**

| 字段                     | 描述                                                         |
| ------------------------ | ------------------------------------------------------------ |
| build dockerfile context | 指定dockerfile文件名构建镜像上下文路径                       |
| image                    | 指定镜像                                                     |
| command                  | 执行命令，覆盖默认命令                                       |
| container_name           | 指定容器名称，由于容器名称是唯一的，如果指定自定义名称，则无法scale |
| deploy                   | 指定部署和运行服务相关配置，只能在Swarm模式使用              |
| environment              | 添加环境变量                                                 |
| networks                 | 加入网络，引用顶级networks下条目                             |
| ports                    | 暴漏端口，与-p相同，但端口不能低于60                         |
| volumes                  | 挂载宿主机路径或命名卷在顶级volumes定义卷名称                |
| restart                  | 重启策略，默认no，always\|on-failurel\|unless-stopped        |
| hostname                 | 容器主机名                                                   |


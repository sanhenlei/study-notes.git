# 一、Scala介绍

## 1、简介

* Scala 是一门多范式（multi-paradigm）的编程语言，设计初衷是要集成面向对象编程和函数式编程的各种特性。
* Scala 运行在Java虚拟机上，并兼容现有的Java程序。
* Scala 源代码被编译成Java字节码，所以它可以运行于JVM之上，并可以调用现有的Java类库。

> 作者：联邦理工学院洛桑（EPFL）的Martin Odersky于2001年基于Funnel的工作开始设计Scala。

## 2、特性

### **面向对象特性**

* Scala是一种纯面向对象的语言，每个值都是对象。对象的数据类型以及行为由类和特质描述。

* 类抽象机制的扩展有两种途径：一种途径是子类继承，另一种途径是灵活的混入机制。这两种途径能避免多重继承的种种问题。

### **函数式编程**

* Scala也是一种函数式语言，其函数也能当成值来使用。Scala提供了轻量级的语法用以定义匿名函数，支持高阶函数，允许嵌套多层函数，并支持柯里化。Scala的case class及其内置的模式匹配相当于函数式编程语言中常用的代数类型。
* 更进一步，程序员可以利用Scala的模式匹配，编写类似正则表达式的代码处理XML数据。

### **静态类型**

Scala具备类型系统，通过编译时检查，保证代码的安全性和一致性。类型系统具体支持以下特性：

- 泛型类
- 协变和逆变
- 标注
- 类型参数的上下限约束
- 把类别和抽象类型作为对象成员
- 复合类型
- 引用自己时显式指定类型
- 视图
- 多态方法

### **扩展性**

Scala的设计秉承一项事实，即在实践中，某个领域特定的应用程序开发往往需要特定于该领域的语言扩展。Scala提供了许多独特的语言机制，可以以库的形式轻易无缝添加新的语言结构：

- 任何方法可用作前缀或后缀操作符
- 可以根据预期类型自动构造闭包。

### **并发性**

Scala使用Actor作为其并发模型，Actor是类似线程的实体，通过邮箱发收消息。Actor可以复用线程，因此可以在程序中可以使用数百万个Actor,而线程只能创建数千个。在2.10之后的版本中，使用Akka作为其默认Actor实现。



## 3、特征

* Java和scala可以无缝混编，都是运行在JVM上的

* 类型推测(自动推测类型)，不用指定类型

* 并发和分布式（Actor，类似Java多线程Thread） 

* 特质trait，特征(类似java中interfaces 和 abstract结合)

* 模式匹配，match case（类似java switch case）

* 高阶函数（函数的参数是函数，函数的返回是函数），可进行函数式编程

* 编译运行：

  先编译：scalac  HelloWorld.scala  将会生成两个文件：HelloWorld$.class和HelloWorld.class

  再运行：scala HelloWorld

  输出结果：hello world！

  

# 二、Scala安装

## 1、先安装JAVA

确保你本地已经安装了 JDK 1.5 以上版本，并且设置了 JAVA_HOME 环境变量及 JDK 的 bin 目录。

使用以下命令查看是否安装了 Java：

```shell
##linux上
$ java -version
java version "1.8.0_31"
Java(TM) SE Runtime Environment (build 1.8.0_31-b13)
Java HotSpot(TM) 64-Bit Server VM (build 25.31-b07, mixed mode)


##windows上
打开cmd
C:\Users\Administrator>java -version
java version "1.8.0_181"
Java(TM) SE Runtime Environment (build 1.8.0_181-b13)
Java HotSpot(TM) 64-Bit Server VM (build 25.181-b13, mixed mode)
```

> 可以参考[Java 开发环境配置](https://www.runoob.com/java/java-environment-setup.html)。

## 2、在windows安装Scala

### 2.1 下载

从 Scala 官网地址 http://www.scala-lang.org/downloads 下载 Scala 二进制包(页面底部)，本教程我们将下载 *2.11.7*版本，如下图所示：

![image-20200109085643504](image\scala笔记\image-20200109085643504.png)

双击安装，完成后新增环境变量SCALA_HOME：

![image-20200109092008864](image\scala笔记\image-20200109092008864.png)



### 2.2 设置 Path 变量

找到系统变量下的"Path"如图，单击编辑。在"变量值"一栏的最前面添加如下的路径： %SCALA_HOME%\bin;%SCALA_HOME%\jre\bin

### 2.3 设置 Classpath 变量

找到找到系统变量下的"Classpath"如图，单击编辑，如没有，则单击"新建":

- "变量名"：ClassPath
- "变量值"：.;%SCALA_HOME%\bin;%SCALA_HOME%\lib\dt.jar;%SCALA_HOME%\lib\tools.jar.;

**注意：**"变量值"最前面的 .; 不要漏掉。最后单击确定即可。

![image-20200109092509587](image\scala笔记\image-20200109092509587.png)

检查环境变量是否设置好了：调出"cmd"检查。单击 【开始】，在输入框中输入cmd，然后"回车"，输入 scala，然后回车，如环境变量设置ok，你应该能看到这些信息。

![image-20200109093259037](image\scala笔记\image-20200109093259037.png)

## 3、在linux安装Scala

下载：

![image-20200109093955226](F:\OneDrive\程序开发资料\md笔记\image\scala笔记\image-20200109093955226.png)

```shell
#解压
tar -xvzf scala-2.13.1.tgz && ln -v scala-2.13.1 scala 

#配置环境变量
vim /etc/profile
export SCALA_HOME=/opt/scala/bin
export PATH=$PATH:$SCALA_HOME/bin

#生效
source /etc/profile
```



# 三、基本语法

Scala 与 Java 的最大区别是：Scala 语句末尾的分号 ; 是可选的。

我们可以认为 Scala 程序是对象的集合，通过调用彼此的方法来实现消息传递。接下来我们来理解下，类，对象，方法，实例变量的概念：

* **对象**：对象有属性和行为。例如：一只狗的属性有：颜色、名字，行为有：叫、跑、吃等。对象是一个类的实例。
* **类**: 类是对象的抽象，而对象是类的具体实例。
* **方法**：方法描述的基本的行为，一个类可以包含多个方法。
* **字段**：每个对象都有它唯一的实例变量集合，即字段。对象的属性通过给字段赋值来创建。

### 1、第一个Scala程序

#### 交互式编程

交互式编程不需要创建脚本文件，可以在cmd或linux上通过以下命令调用：

```shell
C:\Users\Administrator>scala
Welcome to Scala 2.13.1 (Java HotSpot(TM) 64-Bit Server VM, Java 1.8.0_181).
Type in expressions for evaluation. Or try :help.

scala> 1+1
res0: Int = 2

scala> print("Hello World!")
Hello World!
scala>
```

#### 脚本形式

也可以通过创建一个.scala的文件来执行代码，如下：

```scala
object HelloWorld {
   /* 这是我的第一个 Scala 程序
    * 以下程序将输出'Hello World!' 
    */
   def main(args: Array[String]) {
      println("Hello, world!") // 输出 Hello World
   }
}
```

```shell
PS E:\Projects\javastudy\src\main\java\com\javastudy\scala> scalac HelloWorld.scala  
one warning found
PS E:\Projects\javastudy\src\main\java\com\javastudy\scala> ls
    目录: E:\Projects\javastudy\src\main\java\com\javastudy\scala
Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-a----        2020/1/13     11:24            682 HelloWorld$.class
-a----        2020/1/13     11:24            645 HelloWorld.class
-a----         2020/1/9     10:05             90 HelloWorld.scala


PS E:\Projects\javastudy\src\main\java\com\javastudy\scala> scala HelloWorld
Hello World!
```

### 2、基本语法

Scala基本语法需要注意以下几点：

* **区分大小写**：Scala是大小写敏感的

* **类名**：对于所有的类名的第一个字母要大写。

  ​		如果需要使用几个单词来构成一个类的名称，每个单词的第一个字母要大写。

  ​		示例：*class MyFirstScalaClass*

* **方法名称**：所有的方法名称的第一个字母用小写。

  ​		如果若干单词被用于构成方法的名称，则每个单词的第一个字母应大写。

  ​		示例：*def myMethodName()*

* **def main(args: Array[String])** - Scala程序从main()方法开始处理，这是每一个Scala程序的强制程序入口部分。

### 3、标识符

Scala可以使用两种形式的标识符，字符数字和符号。

字符数字使用字母或是下划线开头、后面可以接字母或是数字，符号"$"在Scala中也看作为字母。然而以"$"开头的标识符为保留的Scala编译器产生的标志符号使用，应用程序应该避免使用"$"开始的标识符，以免造成冲突。

Scala 的命名规则采用和 Java 类似的 camel 命名规则，首字符小写，比如 toString。类名的首字符还是使用大写。此外也应该避免使用以下划线结尾的标志符以避免冲突。符号标志符包含一个或多个符号，如+，:，? 等，比如:

> "+ ++ ::: < ?> :->"

### 4、Scala 注释

Scala 类似 Java 支持单行和多行注释。多行注释可以嵌套，但必须正确嵌套，一个注释开始符号对应一个结束符号。注释在 Scala 编译中会被忽略，实例如下：

```scala
object HelloWorld {
   /* 这是一个 Scala 程序
    * 这是一行注释
    * 这里演示了多行注释
    */
   def main(args: Array[String]) {
      // 输出 Hello World
      // 这是一个单行注释
      println("Hello, world!") 
   }
}
```

### 5、Scala 包

#### 定义包

Scala 使用 package 关键字定义包，在Scala将代码定义到某个包中有两种方式：

第一种方法和 Java 一样，在文件的头定义包名，这种方法就后续所有代码都放在该包中。 比如：

```scala
package com.runoob
class HelloWorld
```

第二种方法有些类似 C#，如：

```scala
package com.runoob {
  class HelloWorld 
}
```

第二种方法，可以在一个文件中定义多个包。

#### 引用

Scala 使用 import 关键字引用包。

```scala
import java.awt.Color  // 引入Color
 
import java.awt._  // 引入包内所有成员
 
def handler(evt: event.ActionEvent) { // java.awt.event.ActionEvent
  ...  // 因为引入了java.awt，所以可以省去前面的部分
}
```

import语句可以出现在任何地方，而不是只能在文件顶部。import的效果从开始延伸到语句块的结束。这可以大幅减少名称冲突的可能性。

如果想要引入包中的几个成员，可以使用selector（选取器）：

```scala
import java.awt.{Color, Font}
 
// 重命名成员
import java.util.{HashMap => JavaHashMap}
 
// 隐藏成员
import java.util.{HashMap => _, _} // 引入了util包的所有成员，但是HashMap被隐藏了
```

# 四、数据类型

Scala 与 Java有着相同的数据类型，下表列出了 Scala 支持的数据类型：

| 数据类型 | 描述                                                         |
| -------- | ------------------------------------------------------------ |
| Byte     | 8位有符号补码整数。数值区间为 -128 到 127                    |
| Short    | 16位有符号补码整数。数值区间为 -32768 到 32767               |
| Int      | 32位有符号补码整数。数值区间为 -2147483648 到 2147483647     |
| Long     | 64位有符号补码整数。数值区间为 -9223372036854775808 到 9223372036854775807 |
| Float    | 32 位, IEEE 754 标准的单精度浮点数                           |
| Double   | 64 位 IEEE 754 标准的双精度浮点数                            |
| Char     | 16位无符号Unicode字符, 区间值为 U+0000 到 U+FFFF             |
| String   | 字符序列                                                     |
| Boolean  | true或false                                                  |
| Unit     | 表示无值，和其他语言中void等同。用作不返回任何结果的方法的结果类型。Unit只有一个实例值，写成()。 |
| null     | null 或空引用                                                |
| Nothing  | Nothing类型在Scala的类层级的最底端；它是任何其他类型的子类型。 |
| Any      | Any是所有其他类的超类                                        |
| AnRef    | AnyRef类是Scala里所有引用类(reference class)的基类           |

上表中列出的数据类型都是对象，也就是说scala没有java中的原生类型。在scala是可以对数字等基础类型调用方法的。#

### 1、Scala 基础字面量

Scala 非常简单且直观。接下来我们会详细介绍 Scala 字面量。

#### 整型字面量

整型字面量用于Int类型，如果表示Long，可以在数字后面添加L或者小写的l作为后缀。

```scala
0
035
21
0xFFFFFF
0777L
```

#### 浮点型字面量

如果浮点数后面有f或F后缀时，表示这是一个Float类型，否则就是一个Double类型的。实例如下：

```scala
0.0
1e30f
3.14159f
1.0e100
.1
```

#### 布尔型字面量

布尔型字面量有true和false.

#### 符号字面量

符号字面量被写成： **'<标识符>** ，这里 **<标识符>** 可以是任何字母或数字的标识（注意：不能以数字开头）。这种字面量被映射成预定义类scala.Symbol的实例。

如： 符号字面量 **'x** 是表达式 **scala.Symbol("x")** 的简写，符号字面量定义如下：



```
package scala
final case class Symbol private (name: String) {
   override def toString: String = "'" + name
}
```

#### 字符字面量

在 Scala 字符变量使用单引号 **'** 来定义，如下：

```
'a' 
'\u0041'
'\n'
'\t'
```

其中 **\** 表示转义字符，其后可以跟 **u0041** 数字或者 **\r\n** 等固定的转义字符。

#### 字符串字面量

在 Scala 字符串字面量使用双引号 **"** 来定义，如下：

```
"Hello,\nWorld!"
"菜鸟教程官网：www.runoob.com"
```

#### 多行字符串

多行字符串用三个双引号来表示分隔符，格式为：**""" ... """**。

实例如下：

```
val foo = """菜鸟教程
www.runoob.com
www.w3cschool.cc
www.runnoob.com
以上三个地址都能访问"""
```

#### Null 值

空值是 scala.Null 类型。

Scala.Null和scala.Nothing是用统一的方式处理Scala面向对象类型系统的某些"边界情况"的特殊类型。

Null类是null引用对象的类型，它是每个引用类（继承自AnyRef的类）的子类。Null不兼容值类型。

###  2、转义字符

下表列出了常见的转义字符：

| 转义字符 | Unicode | 描述                                |
| :------- | :------ | :---------------------------------- |
| \b       | \u0008  | 退格(BS) ，将当前位置移到前一列     |
| \t       | \u0009  | 水平制表(HT) （跳到下一个TAB位置）  |
| \n       | \u000a  | 换行(LF) ，将当前位置移到下一行开头 |
| \f       | \u000c  | 换页(FF)，将当前位置移到下页开头    |
| \r       | \u000d  | 回车(CR) ，将当前位置移到本行开头   |
| \"       | \u0022  | 代表一个双引号(")字符               |
| \'       | \u0027  | 代表一个单引号（'）字符             |
| \\       | \u005c  | 代表一个反斜线字符 '\'              |

0 到 255 间的 Unicode 字符可以用一个八进制转义序列来表示，即反斜线‟\‟后跟 最多三个八进制。

在字符或字符串中，反斜线和后面的字符序列不能构成一个合法的转义序列将会导致 编译错误。

以下实例演示了一些转义字符的使用：

```
object Test {
   def main(args: Array[String]) {
      println("Hello\tWorld\n\n" );
   }
} 
```

# 五、变量

变量是一种使用方便的占位符，用于引用计算机内存地址，变量创建后会占用一定的内存空间。

基于变量的数据类型，操作系统会进行内存分配并且决定什么将被存储在保留内存中，因此通过给变量分配不同的数据类型，你可以在这些变量中存储整数，小数或者字母。

### 变量声明

在学习如何声明变量与常量之前，我们先来了解一些变量与常量。

- 一、变量： 在程序运行过程中其值可能发生改变的量叫做变量。如：时间，年龄。
- 二、常量 在程序运行过程中其值不会发生变化的量叫做常量。如：数值 3，字符'A'。

在 Scala 中，使用关键词 **"var"** 声明变量，使用关键词 **"val"** 声明常量。

声明变量实例如下：

```
var myVar : String = "Foo"
var myVar : String = "Too"
```

以上定义了变量 myVar，我们可以修改它。

声明常量实例如下：

```
val myVal : String = "Foo"
```

以上定义了常量 myVal，它是不能修改的。如果程序尝试修改常量 myVal 的值，程序将会在编译时报错。

### 变量类型声明

变量的类型在变量名之后等号之前声明。定义变量的类型的语法格式如下：

```scala
var VariableName : DataType [=  Initial Value]
或
val VariableName : DataType [=  Initial Value]
```

### 变量类型引用

在 Scala 中声明变量和常量不一定要指明数据类型，在没有指明数据类型的情况下，其数据类型是通过变量或常量的初始值推断出来的。

所以，如果在没有指明数据类型的情况下声明变量或常量必须要给出其初始值，否则将会报错。

```
var myVar = 10;
val myVal = "Hello, Scala!";
```

以上实例中，myVar 会被推断为 Int 类型，myVal 会被推断为 String 类型。

### 多个变量声明

Scala 支持多个变量的声明：

```
val xmax, ymax = 100  // xmax, ymax都声明为100
```

如果方法返回值是元组，我们可以使用 val 来声明一个元组：

```
scala> val pa = (40,"Foo")
pa: (Int, String) = (40,Foo)
```

# 六、访问修饰符

Scala 访问修饰符基本和java的一样，分别有：private、protected、public。

如果没有指定访问修饰符，默认情况下，Scala对象的访问级别都是public。

Scala中的private限定符，比Java更严格，在嵌套类情况下，外层类甚至不能访问被嵌套类的私有方法。

#### 私有（Private）成员

用private关键字修饰，带有此标记的成员仅在包含了成员定义的类或对象内部可见，同样的规则还适用内部类。

```scala
class Outer{
    class Inner{
        private def f(){println("f")}
        class InnerMost{
            f()	// 正确
        }
    }
    (new Inner).f()	// 错误
}
```

**(new Inner).f( )** 访问不合法是因为 **f** 在 Inner 中被声明为 private，而访问不在类 Inner 之内。

但在 InnerMost 里访问 **f** 就没有问题的，因为这个访问包含在 Inner 类之内。

Java中允许这两种访问，因为它允许外部类访问内部类的私有成员。

### 保护(Protected)成员

在 scala 中，对保护（Protected）成员的访问比 java 更严格一些。因为它只允许保护成员在定义了该成员的的类的子类中被访问。而在java中，用protected关键字修饰的成员，除了定义了该成员的类的子类可以访问，同一个包里的其他类也可以进行访问。

```scala
package p{
    class Super{
        protected def f(){println("f")}
        class Sub extends Super{
            f()
        }
        class Other{
            (new Super).f() // 错误
        }
    }
    
}
```

上例中，Sub 类对 f 的访问没有问题，因为 f 在 Super 中被声明为 protected，而 Sub 是 Super 的子类。相反，Other 对 f 的访问不被允许，因为 other 没有继承自 Super。而后者在 java 里同样被认可，因为 Other 与 Sub 在同一包里。

### 公共(Public)成员

Scala中，如果没有指定任何的修饰符，则默认为 public。这样的成员在任何地方都可以被访问。

```scala
class Outer {
   class Inner {
      def f() { println("f") }
      class InnerMost {
         f() // 正确
      }
   }
   (new Inner).f() // 正确因为 f() 是 public
}
```

## 作用域保护

Scala中，访问修饰符可以通过使用限定词强调。格式为:

```scala
private[x] 
或 
protected[x]
```

这里的x指代某个所属的包、类或单例对象。如果写成private[x],读作"这个成员除了对[…]中的类或[…]中的包中的类及它们的伴生对像可见外，对其它所有类都是private。

这种技巧在横跨了若干包的大型项目中非常有用，它允许你定义一些在你项目的若干子包中可见但对于项目外部的客户却始终不可见的东西。

```scala
package bobsrockets{
    package navigation{
        private[bobsrockets] class Navigator{
         protected[navigation] def useStarChart(){}
         class LegOfJourney{
             private[Navigator] val distance = 100
             }
            private[this] var speed = 200
            }
        }
        package launch{
        import navigation._
        object Vehicle{
        private[launch] val guide = new Navigator
        }
    }
}
```

上述例子中，类Navigator被标记为private[bobsrockets]就是说这个类对包含在bobsrockets包里的所有的类和对象可见。

比如说，从Vehicle对象里对Navigator的访问是被允许的，因为对象Vehicle包含在包launch中，而launch包在bobsrockets中，相反，所有在包bobsrockets之外的代码都不能访问类Navigator。

# 七、运算符

一个运算符是一个符号，用于告诉编译器来执行指定的数学运算和逻辑运算。

Scala 含有丰富的内置运算符，包括以下几种类型：

- 算术运算符
- 关系运算符
- 逻辑运算符
- 位运算符
- 赋值运算符

接下来我们将为大家详细介绍以上各种运算符的应用。

------

### 算术运算符

下表列出了 Scala 支持的算术运算符。

假定变量 A 为 10，B 为 20：

| 运算符 | 描述 | 实例                 |
| :----- | :--- | :------------------- |
| +      | 加号 | A + B 运算结果为 30  |
| -      | 减号 | A - B 运算结果为 -10 |
| *      | 乘号 | A * B 运算结果为 200 |
| /      | 除号 | B / A 运算结果为 2   |
| %      | 取余 | B % A 运算结果为 0   |

#### 实例

```scala
object Test {
   def main(args: Array[String]) {
      var a = 10;
      var b = 20;
      var c = 25;
      var d = 25;
      println("a + b = " + (a + b) );
      println("a - b = " + (a - b) );
      println("a * b = " + (a * b) );
      println("b / a = " + (b / a) );
      println("b % a = " + (b % a) );
      println("c % a = " + (c % a) );
      
   }
}
```

### 关系运算符

下表列出了 Scala 支持的关系运算符。

假定变量 A 为 10，B 为 20：

| 运算符 | 描述     | 实例                      |
| :----- | :------- | :------------------------ |
| ==     | 等于     | (A == B) 运算结果为 false |
| !=     | 不等于   | (A != B) 运算结果为 true  |
| >      | 大于     | (A > B) 运算结果为 false  |
| <      | 小于     | (A < B) 运算结果为 true   |
| >=     | 大于等于 | (A >= B) 运算结果为 false |
| <=     | 小于等于 | (A <= B) 运算结果为 true  |

#### 实例

```scala
object Test {
   def main(args: Array[String]) {
      var a = 10;
      var b = 20;
      println("a == b = " + (a == b) );
      println("a != b = " + (a != b) );
      println("a > b = " + (a > b) );
      println("a < b = " + (a < b) );
      println("b >= a = " + (b >= a) );
      println("b <= a = " + (b <= a) );
   }
}
```

### 逻辑运算符

下表列出了 Scala 支持的逻辑运算符。

假定变量 A 为 1，B 为 0：

| 运算符 | 描述   | 实例                       |
| :----- | :----- | :------------------------- |
| &&     | 逻辑与 | (A && B) 运算结果为 false  |
| \|\|   | 逻辑或 | (A \|\| B) 运算结果为 true |
| !      | 逻辑非 | !(A && B) 运算结果为 true  |

#### 实例

```scala
object Test {
   def main(args: Array[String]) {
      var a = true;
      var b = false;

      println("a && b = " + (a&&b) );

      println("a || b = " + (a||b) );

      println("!(a && b) = " + !(a && b) );
   }
} 
```

### 位运算符

位运算符用来对二进制位进行操作，~,&,|,^分别为取反，按位与与，按位与或，按位与异或运算，如下表实例：

| p    | q    | p & q | p \| q | p ^ q |
| :--- | :--- | :---- | :----- | :---- |
| 0    | 0    | 0     | 0      | 0     |
| 0    | 1    | 0     | 1      | 1     |
| 1    | 1    | 1     | 1      | 0     |
| 1    | 0    | 0     | 1      | 1     |

如果指定 A = 60; 及 B = 13; 两个变量对应的二进制为：

```scala
A = 0011 1100

B = 0000 1101

-------位运算----------

A&B = 0000 1100

A|B = 0011 1101

A^B = 0011 0001

~A  = 1100 0011
```

Scala 中的按位运算法则如下：

| 运算符 | 描述           | 实例                                                         |
| :----- | :------------- | :----------------------------------------------------------- |
| &      | 按位与运算符   | (a & b) 输出结果 12 ，二进制解释： 0000 1100                 |
| \|     | 按位或运算符   | (a \| b) 输出结果 61 ，二进制解释： 0011 1101                |
| ^      | 按位异或运算符 | (a ^ b) 输出结果 49 ，二进制解释： 0011 0001                 |
| ~      | 按位取反运算符 | (~a ) 输出结果 -61 ，二进制解释： 1100 0011， 在一个有符号二进制数的补码形式。 |
| <<     | 左移动运算符   | a << 2 输出结果 240 ，二进制解释： 1111 0000                 |
| >>     | 右移动运算符   | a >> 2 输出结果 15 ，二进制解释： 0000 1111                  |
| >>>    | 无符号右移     | A >>>2 输出结果 15, 二进制解释: 0000 1111                    |

#### 实例

```scala
object Test {
   def main(args: Array[String]) {
      var a = 60;           /* 60 = 0011 1100 */  
      var b = 13;           /* 13 = 0000 1101 */
      var c = 0;

      c = a & b;            /* 12 = 0000 1100 */ 
      println("a & b = " + c );

      c = a | b;            /* 61 = 0011 1101 */
      println("a | b = " + c );

      c = a ^ b;            /* 49 = 0011 0001 */
      println("a ^ b = " + c );

      c = ~a;               /* -61 = 1100 0011 */
      println("~a = " + c );

      c = a << 2;           /* 240 = 1111 0000 */
      println("a << 2 = " + c );

      c = a >> 2;           /* 15 = 1111 */
      println("a >> 2  = " + c );

      c = a >>> 2;          /* 15 = 0000 1111 */
      println("a >>> 2 = " + c );
   }
} 
```

执行以上代码，输出结果为：

```shell
$ scalac Test.scala 
$ scala Test
a & b = 12
a | b = 61
a ^ b = 49
~a = -61
a << 2 = 240
a >> 2  = 15
a >>> 2 = 15
```

### 赋值运算符

以下列出了 Scala 语言支持的赋值运算符:

| 运算符 | 描述                                                         | 实例                                  |
| :----- | :----------------------------------------------------------- | :------------------------------------ |
| =      | 简单的赋值运算，指定右边操作数赋值给左边的操作数。           | C = A + B 将 A + B 的运算结果赋值给 C |
| +=     | 相加后再赋值，将左右两边的操作数相加后再赋值给左边的操作数。 | C += A 相当于 C = C + A               |
| -=     | 相减后再赋值，将左右两边的操作数相减后再赋值给左边的操作数。 | C -= A 相当于 C = C - A               |
| *=     | 相乘后再赋值，将左右两边的操作数相乘后再赋值给左边的操作数。 | C *= A 相当于 C = C * A               |
| /=     | 相除后再赋值，将左右两边的操作数相除后再赋值给左边的操作数。 | C /= A 相当于 C = C / A               |
| %=     | 求余后再赋值，将左右两边的操作数求余后再赋值给左边的操作数。 | C %= A is equivalent to C = C % A     |
| <<=    | 按位左移后再赋值                                             | C <<= 2 相当于 C = C << 2             |
| >>=    | 按位右移后再赋值                                             | C >>= 2 相当于 C = C >> 2             |
| &=     | 按位与运算后赋值                                             | C &= 2 相当于 C = C & 2               |
| ^=     | 按位异或运算符后再赋值                                       | C ^= 2 相当于 C = C ^ 2               |
| \|=    | 按位或运算后再赋值                                           | C \|= 2 相当于 C = C \| 2             |

#### 实例

```scala
object Test {
   def main(args: Array[String]) {
      var a = 10;    
      var b = 20;
      var c = 0;

      c = a + b;
      println("c = a + b  = " + c );

      c += a ;
      println("c += a  = " + c );

      c -= a ;
      println("c -= a = " + c );

      c *= a ;
      println("c *= a = " + c );

      a = 10;
      c = 15;
      c /= a ;
      println("c /= a  = " + c );

      a = 10;
      c = 15;
      c %= a ;
      println("c %= a  = " + c );

      c <<= 2 ;
      println("c <<= 2  = " + c );

      c >>= 2 ;
      println("c >>= 2  = " + c );

      c >>= a ;
      println("c >>= a  = " + c );

      c &= a ;
      println("c &= 2  = " + c );
     
      c ^= a ;
      println("c ^= a  = " + c );

      c |= a ;
      println("c |= a  = " + c );
   }
} 
```

# 八、方法与函数

Scala 有方法与函数，二者在语义上的区别很小。Scala 方法是类的一部分，而函数是一个对象可以赋值给一个变量。换句话来说在类中定义的函数即是方法。

Scala 中的方法跟 Java 的类似，方法是组成类的一部分。

Scala 中的函数则是一个完整的对象，Scala 中的函数其实就是继承了 Trait 的类的对象。

Scala 中使用 **val** 语句可以定义函数，**def** 语句定义方法。



```scala
calss Test{
    def m(x:Int) = x +3;
    val f = (x:Int) => x+3
}
```

> 注意：有些翻译上函数(function)与方法(method)是没有区别的。
>
> **函数可作为一个参数传入到方法中，而方法不行。**

### 方法声明

Scala 方法声明格式如下：

``` scala
def functionName ([参数列表]) : [return type]
```

如果你不写等于号和方法主体，那么方法会被隐式声明为**抽象(abstract)**，包含它的类型于是也是一个抽象类型。

### 方法定义

方法定义由一个 **def** 关键字开始，紧接着是可选的参数列表，一个冒号 **:** 和方法的返回类型，一个等于号 **=** ，最后是方法的主体。

Scala 方法定义格式如下：

```shell
def functionName ([参数列表]) : [return type] = {
	function body
	return [expr]
}
```

以上代码中 **return type** 可以是任意合法的 Scala 数据类型。参数列表中的参数可以使用逗号分隔。

以下方法的功能是将两个传入的参数相加并求和：

```scala
object add{
    def addInt(a:Int, b:Int) : Int ={
        var sum:Int = 0;
        sum = a + b;
        return sum
    }
}
```

如果方法没有返回值，可以返回为 **Unit**，这个类似于 Java 的 **void**, 实例如下：

```scala
object Hello{
    def printMe() : Unit = {
        println("Hello Scala!")
    }
}
```

### 方法调用

Scala 提供了多种不同的方法调用方式：

以下是调用方法的标准格式：

```
functionName( 参数列表 )
```

如果方法使用了实例的对象来调用，我们可以使用类似java的格式 (使用 **.** 号)：

```
[instance.]functionName( 参数列表 )
```

以上实例演示了定义与调用方法的实例:

```scala
object Test {
   def main(args: Array[String]) {
        println( "Returned Value : " + addInt(5,7) );
   }
   def addInt( a:Int, b:Int ) : Int = {
      var sum:Int = 0
      sum = a + b

      return sum
   }
}
```

执行以上代码，输出结果为：

```
$ scalac Test.scala 
$ scala Test
Returned Value : 12
```

Scala 也是一种函数式语言，所以函数是 Scala 语言的核心。以下一些函数概念有助于我们更好的理解 Scala 编程：

|                      函数概念解析接案例                      |                                                              |
| :----------------------------------------------------------: | :----------------------------------------------------------- |
| [函数传名调用(Call-by-Name)](https://www.runoob.com/scala/functions-call-by-name.html) | [指定函数参数名](https://www.runoob.com/scala/functions-named-arguments.html) |
| [函数 - 可变参数](https://www.runoob.com/scala/functions-variable-arguments.html) | [递归函数](https://www.runoob.com/scala/recursion-functions.html) |
| [默认参数值](https://www.runoob.com/scala/functions-default-parameter-values.html) | [高阶函数](https://www.runoob.com/scala/higher-order-functions.html) |
| [内嵌函数](https://www.runoob.com/scala/nested-functions.html) | [匿名函数](https://www.runoob.com/scala/anonymous-functions.html) |
| [偏应用函数](https://www.runoob.com/scala/partially-applied-functions.html) | [函数柯里化(Function Currying)](https://www.runoob.com/scala/currying-functions.html) |

# 九、闭包

闭包是一个函数，返回值依赖于声明在函数外部的一个或多个变量。

闭包通常来讲可以简单的认为是可以访问一个函数里面局部变量的另外一个函数。

如下面这段匿名的函数：

```
val multiplier = (i:Int) => i * 10  
```

函数体内有一个变量 i，它作为函数的一个参数。如下面的另一段代码：

```
val multiplier = (i:Int) => i * factor
```

在 multiplier 中有两个变量：i 和 factor。其中的一个 i 是函数的形式参数，在 multiplier 函数被调用时，i 被赋予一个新的值。然而，factor不是形式参数，而是自由变量，考虑下面代码：

```
var factor = 3  
val multiplier = (i:Int) => i * factor  
```

这里我们引入一个自由变量 factor，这个变量定义在函数外面。

这样定义的函数变量 multiplier 成为一个"闭包"，因为它引用到函数外面定义的变量，定义这个函数的过程是将这个自由变量捕获而构成一个封闭的函数。

完整实例

```scala
object Test {  
   def main(args: Array[String]) {  
      println( "muliplier(1) value = " +  multiplier(1) )  
      println( "muliplier(2) value = " +  multiplier(2) )  
   }  
   var factor = 3  
   val multiplier = (i:Int) => i * factor  
}  
```

# 十、常用数据类型方法

### String 方法

| 序号 | 方法及描述                                                   |
| :--- | :----------------------------------------------------------- |
| 1    | **char charAt(int index)**返回指定位置的字符                 |
| 2    | **int compareTo(Object o)**比较字符串与对象                  |
| 3    | **int compareTo(String anotherString)**按字典顺序比较两个字符串 |
| 4    | **int compareToIgnoreCase(String str)**按字典顺序比较两个字符串，不考虑大小写 |
| 5    | **String concat(String str)**将指定字符串连接到此字符串的结尾 |
| 6    | **boolean contentEquals(StringBuffer sb)**将此字符串与指定的 StringBuffer 比较。 |
| 7    | **static String copyValueOf(char[] data)**返回指定数组中表示该字符序列的 String |
| 8    | **static String copyValueOf(char[] data, int offset, int count)**返回指定数组中表示该字符序列的 String |
| 9    | **boolean endsWith(String suffix)**测试此字符串是否以指定的后缀结束 |
| 10   | **boolean equals(Object anObject)**将此字符串与指定的对象比较 |
| 11   | **boolean equalsIgnoreCase(String anotherString)**将此 String 与另一个 String 比较，不考虑大小写 |
| 12   | **byte getBytes()**使用平台的默认字符集将此 String 编码为 byte 序列，并将结果存储到一个新的 byte 数组中 |
| 13   | **byte[] getBytes(String charsetName**使用指定的字符集将此 String 编码为 byte 序列，并将结果存储到一个新的 byte 数组中 |
| 14   | **void getChars(int srcBegin, int srcEnd, char[] dst, int dstBegin)**将字符从此字符串复制到目标字符数组 |
| 15   | **int hashCode()**返回此字符串的哈希码                       |
| 16   | **int indexOf(int ch)**返回指定字符在此字符串中第一次出现处的索引 |
| 17   | **int indexOf(int ch, int fromIndex)**返回在此字符串中第一次出现指定字符处的索引，从指定的索引开始搜索 |
| 18   | **int indexOf(String str)**返回指定子字符串在此字符串中第一次出现处的索引 |
| 19   | **int indexOf(String str, int fromIndex)**返回指定子字符串在此字符串中第一次出现处的索引，从指定的索引开始 |
| 20   | **String intern()**返回字符串对象的规范化表示形式            |
| 21   | **int lastIndexOf(int ch)**返回指定字符在此字符串中最后一次出现处的索引 |
| 22   | **int lastIndexOf(int ch, int fromIndex)**返回指定字符在此字符串中最后一次出现处的索引，从指定的索引处开始进行反向搜索 |
| 23   | **int lastIndexOf(String str)**返回指定子字符串在此字符串中最右边出现处的索引 |
| 24   | **int lastIndexOf(String str, int fromIndex)**返回指定子字符串在此字符串中最后一次出现处的索引，从指定的索引开始反向搜索 |
| 25   | **int length()**返回此字符串的长度                           |
| 26   | **boolean matches(String regex)**告知此字符串是否匹配给定的正则表达式 |
| 27   | **boolean regionMatches(boolean ignoreCase, int toffset, String other, int ooffset, int len)**测试两个字符串区域是否相等 |
| 28   | **boolean regionMatches(int toffset, String other, int ooffset, int len)**测试两个字符串区域是否相等 |
| 29   | **String replace(char oldChar, char newChar)**返回一个新的字符串，它是通过用 newChar 替换此字符串中出现的所有 oldChar 得到的 |
| 30   | **String replaceAll(String regex, String replacement**使用给定的 replacement 替换此字符串所有匹配给定的正则表达式的子字符串 |
| 31   | **String replaceFirst(String regex, String replacement)**使用给定的 replacement 替换此字符串匹配给定的正则表达式的第一个子字符串 |
| 32   | **String[] split(String regex)**根据给定正则表达式的匹配拆分此字符串 |
| 33   | **String[] split(String regex, int limit)**根据匹配给定的正则表达式来拆分此字符串 |
| 34   | **boolean startsWith(String prefix)**测试此字符串是否以指定的前缀开始 |
| 35   | **boolean startsWith(String prefix, int toffset)**测试此字符串从指定索引开始的子字符串是否以指定前缀开始。 |
| 36   | **CharSequence subSequence(int beginIndex, int endIndex)**返回一个新的字符序列，它是此序列的一个子序列 |
| 37   | **String substring(int beginIndex)**返回一个新的字符串，它是此字符串的一个子字符串 |
| 38   | **String substring(int beginIndex, int endIndex)**返回一个新字符串，它是此字符串的一个子字符串 |
| 39   | **char[] toCharArray()**将此字符串转换为一个新的字符数组     |
| 40   | **String toLowerCase()**使用默认语言环境的规则将此 String 中的所有字符都转换为小写 |
| 41   | **String toLowerCase(Locale locale)**使用给定 Locale 的规则将此 String 中的所有字符都转换为小写 |
| 42   | **String toString()**返回此对象本身（它已经是一个字符串！）  |
| 43   | **String toUpperCase()**使用默认语言环境的规则将此 String 中的所有字符都转换为大写 |
| 44   | **String toUpperCase(Locale locale)**使用给定 Locale 的规则将此 String 中的所有字符都转换为大写 |
| 45   | **String trim()**删除指定字符串的首尾空白符                  |
| 46   | **static String valueOf(primitive data type x)**返回指定类型参数的字符串表示形式 |

### 数组方法

下表中为 Scala 语言中处理数组的重要方法，使用它前我们需要使用 **import Array._** 引入包。

| 序号 | 方法和描述                                                   |
| :--- | :----------------------------------------------------------- |
| 1    | **def apply( x: T, xs: T\* ): Array[T]**创建指定对象 T 的数组, T 的值可以是 Unit, Double, Float, Long, Int, Char, Short, Byte, Boolean。 |
| 2    | **def concat[T]( xss: Array[T]\* ): Array[T]**合并数组       |
| 3    | **def copy( src: AnyRef, srcPos: Int, dest: AnyRef, destPos: Int, length: Int ): Unit**复制一个数组到另一个数组上。相等于 Java's System.arraycopy(src, srcPos, dest, destPos, length)。 |
| 4    | **def empty[T]: Array[T]**返回长度为 0 的数组                |
| 5    | **def iterate[T]( start: T, len: Int )( f: (T) => T ): Array[T]**返回指定长度数组，每个数组元素为指定函数的返回值。以上实例数组初始值为 0，长度为 3，计算函数为**a=>a+1**：`scala> Array.iterate(0,3)(a=>a+1) res1: Array[Int] = Array(0, 1, 2)` |
| 6    | **def fill[T]( n: Int )(elem: => T): Array[T]**返回数组，长度为第一个参数指定，同时每个元素使用第二个参数进行填充。 |
| 7    | **def fill[T]( n1: Int, n2: Int )( elem: => T ): Array[Array[T]]**返回二数组，长度为第一个参数指定，同时每个元素使用第二个参数进行填充。 |
| 8    | **def ofDim[T]( n1: Int ): Array[T]**创建指定长度的数组      |
| 9    | **def ofDim[T]( n1: Int, n2: Int ): Array[Array[T]]**创建二维数组 |
| 10   | **def ofDim[T]( n1: Int, n2: Int, n3: Int ): Array[Array[Array[T]]]**创建三维数组 |
| 11   | **def range( start: Int, end: Int, step: Int ): Array[Int]**创建指定区间内的数组，step 为每个元素间的步长 |
| 12   | **def range( start: Int, end: Int ): Array[Int]**创建指定区间内的数组 |
| 13   | **def tabulate[T]( n: Int )(f: (Int)=> T): Array[T]**返回指定长度数组，每个数组元素为指定函数的返回值，默认从 0 开始。以上实例返回 3 个元素：`scala> Array.tabulate(3)(a => a + 5) res0: Array[Int] = Array(5, 6, 7)` |
| 14   | **def tabulate[T]( n1: Int, n2: Int )( f: (Int, Int ) => T): Array[Array[T]]**返回指定长度的二维数组，每个数组元素为指定函数的返回值，默认从 0 开始。 |

### List(列表)

Scala 列表类似于数组，它们所有元素的类型都相同，但是它们也有所不同：列表是不可变的，值一旦被定义了就不能改变，其次列表 具有递归的结构（也就是链接表结构）而数组不是。。

列表的元素类型 T 可以写成 List[T]。例如，以下列出了多种类型的列表：

```scala
// 字符串列表
val site: List[String] = List("Runoob", "Google", "Baidu")

// 整型列表
val nums: List[Int] = List(1, 2, 3, 4)

// 空列表
val empty: List[Nothing] = List()

// 二维列表
val dim: List[List[Int]] =
   List(
      List(1, 0, 0),
      List(0, 1, 0),
      List(0, 0, 1)
   )
```

构造列表的两个基本单位是 **Nil** 和 **::**

**Nil** 也可以表示为一个空列表。

以上实例我们可以写成如下所示：

```scala
// 字符串列表
val site = "Runoob" :: ("Google" :: ("Baidu" :: Nil))

// 整型列表
val nums = 1 :: (2 :: (3 :: (4 :: Nil)))

// 空列表
val empty = Nil

// 二维列表
val dim = (1 :: (0 :: (0 :: Nil))) ::
          (0 :: (1 :: (0 :: Nil))) ::
          (0 :: (0 :: (1 :: Nil))) :: Nil
```

------

#### 列表基本操作

Scala列表有三个基本操作：

- `head` 返回列表第一个元素
- `tail` 返回一个列表，包含除了第一元素之外的其他元素
- `isEmpty` 在列表为空时返回true

对于Scala列表的任何操作都可以使用这三个基本操作来表达。实例如下:

```
object Test {
   def main(args: Array[String]) {
      val site = "Runoob" :: ("Google" :: ("Baidu" :: Nil))
      val nums = Nil

      println( "第一网站是 : " + site.head )
      println( "最后一个网站是 : " + site.tail )
      println( "查看列表 site 是否为空 : " + site.isEmpty )
      println( "查看 nums 是否为空 : " + nums.isEmpty )
   }
}
```

执行以上代码，输出结果为：

```
$ vim Test.scala 
$ scala Test.scala 
第一网站是 : Runoob
最后一个网站是 : List(Google, Baidu)
查看列表 site 是否为空 : false
查看 nums 是否为空 : true
```

------

#### 连接列表

你可以使用 **:::** 运算符或 **List.:::()** 方法或 **List.concat()** 方法来连接两个或多个列表。实例如下:

```
object Test {
   def main(args: Array[String]) {
      val site1 = "Runoob" :: ("Google" :: ("Baidu" :: Nil))
      val site2 = "Facebook" :: ("Taobao" :: Nil)

      // 使用 ::: 运算符
      var fruit = site1 ::: site2
      println( "site1 ::: site2 : " + fruit )
      
      // 使用 List.:::() 方法
      fruit = site1.:::(site2)
      println( "site1.:::(site2) : " + fruit )

      // 使用 concat 方法
      fruit = List.concat(site1, site2)
      println( "List.concat(site1, site2) : " + fruit  )
      

   }
}
```

执行以上代码，输出结果为：

```
$ vim Test.scala 
$ scala Test.scala 
site1 ::: site2 : List(Runoob, Google, Baidu, Facebook, Taobao)
site1.:::(site2) : List(Facebook, Taobao, Runoob, Google, Baidu)
List.concat(site1, site2) : List(Runoob, Google, Baidu, Facebook, Taobao)
```

------

#### List.fill()

我们可以使用 List.fill() 方法来创建一个指定重复数量的元素列表：

```
object Test {
   def main(args: Array[String]) {
      val site = List.fill(3)("Runoob") // 重复 Runoob 3次
      println( "site : " + site  )

      val num = List.fill(10)(2)         // 重复元素 2, 10 次
      println( "num : " + num  )
   }
}
```

执行以上代码，输出结果为：

```
$ vim Test.scala 
$ scala Test.scala 
site : List(Runoob, Runoob, Runoob)
num : List(2, 2, 2, 2, 2, 2, 2, 2, 2, 2)
```

------

#### List.tabulate()

List.tabulate() 方法是通过给定的函数来创建列表。

方法的第一个参数为元素的数量，可以是二维的，第二个参数为指定的函数，我们通过指定的函数计算结果并返回值插入到列表中，起始值为 0，实例如下：



```
object Test {
   def main(args: Array[String]) {
      // 通过给定的函数创建 5 个元素
      val squares = List.tabulate(6)(n => n * n)
      println( "一维 : " + squares  )

      // 创建二维列表
      val mul = List.tabulate( 4,5 )( _ * _ )      
      println( "多维 : " + mul  )
   }
}
```

执行以上代码，输出结果为：

```
$ vim Test.scala 
$ scala Test.scala 
一维 : List(0, 1, 4, 9, 16, 25)
多维 : List(List(0, 0, 0, 0, 0), List(0, 1, 2, 3, 4), List(0, 2, 4, 6, 8), List(0, 3, 6, 9, 12))
```

------

#### List.reverse

List.reverse 用于将列表的顺序反转，实例如下：

```
object Test {
   def main(args: Array[String]) {
      val site = "Runoob" :: ("Google" :: ("Baidu" :: Nil))
      println( "site 反转前 : " + site )

      println( "site 反转后 : " + site.reverse )
   }
}
```

执行以上代码，输出结果为：

```
$ vim Test.scala 
$ scala Test.scala 
site 反转前 : List(Runoob, Google, Baidu)
site 反转后 : List(Baidu, Google, Runoob)
```

------

#### Scala List 常用方法

下表列出了 Scala List 常用的方法：

| 序号 | 方法及描述                                                   |
| :--- | :----------------------------------------------------------- |
| 1    | **def +:(elem: A): List[A]**为列表预添加元素`scala> val x = List(1) x: List[Int] = List(1) scala> val y = 2 +: x y: List[Int] = List(2, 1) scala> println(x) List(1)` |
| 2    | **def ::(x: A): List[A]**在列表开头添加元素                  |
| 3    | **def :::(prefix: List[A]): List[A]**在列表开头添加指定列表的元素 |
| 4    | **def :+(elem: A): List[A]**复制添加元素后列表。`scala> val a = List(1) a: List[Int] = List(1) scala> val b = a :+ 2 b: List[Int] = List(1, 2) scala> println(a) List(1)` |
| 5    | **def addString(b: StringBuilder): StringBuilder**将列表的所有元素添加到 StringBuilder |
| 6    | **def addString(b: StringBuilder, sep: String): StringBuilder**将列表的所有元素添加到 StringBuilder，并指定分隔符 |
| 7    | **def apply(n: Int): A**通过列表索引获取元素                 |
| 8    | **def contains(elem: Any): Boolean**检测列表中是否包含指定的元素 |
| 9    | **def copyToArray(xs: Array[A], start: Int, len: Int): Unit**将列表的元素复制到数组中。 |
| 10   | **def distinct: List[A]**去除列表的重复元素，并返回新列表    |
| 11   | **def drop(n: Int): List[A]**丢弃前n个元素，并返回新列表     |
| 12   | **def dropRight(n: Int): List[A]**丢弃最后n个元素，并返回新列表 |
| 13   | **def dropWhile(p: (A) => Boolean): List[A]**从左向右丢弃元素，直到条件p不成立 |
| 14   | **def endsWith[B](that: Seq[B]): Boolean**检测列表是否以指定序列结尾 |
| 15   | **def equals(that: Any): Boolean**判断是否相等               |
| 16   | **def exists(p: (A) => Boolean): Boolean**判断列表中指定条件的元素是否存在。判断l是否存在某个元素:`scala> l.exists(s => s == "Hah") res7: Boolean = true` |
| 17   | **def filter(p: (A) => Boolean): List[A]**输出符号指定条件的所有元素。过滤出长度为3的元素:`scala> l.filter(s => s.length == 3) res8: List[String] = List(Hah, WOW)` |
| 18   | **def forall(p: (A) => Boolean): Boolean**检测所有元素。例如：判断所有元素是否以"H"开头：scala> l.forall(s => s.startsWith("H")) res10: Boolean = false |
| 19   | **def foreach(f: (A) => Unit): Unit**将函数应用到列表的所有元素 |
| 20   | **def head: A**获取列表的第一个元素                          |
| 21   | **def indexOf(elem: A, from: Int): Int**从指定位置 from 开始查找元素第一次出现的位置 |
| 22   | **def init: List[A]**返回所有元素，除了最后一个              |
| 23   | **def intersect(that: Seq[A]): List[A]**计算多个集合的交集   |
| 24   | **def isEmpty: Boolean**检测列表是否为空                     |
| 25   | **def iterator: Iterator[A]**创建一个新的迭代器来迭代元素    |
| 26   | **def last: A**返回最后一个元素                              |
| 27   | **def lastIndexOf(elem: A, end: Int): Int**在指定的位置 end 开始查找元素最后出现的位置 |
| 28   | **def length: Int**返回列表长度                              |
| 29   | **def map[B](f: (A) => B): List[B]**通过给定的方法将所有元素重新计算 |
| 30   | **def max: A**查找最大元素                                   |
| 31   | **def min: A**查找最小元素                                   |
| 32   | **def mkString: String**列表所有元素作为字符串显示           |
| 33   | **def mkString(sep: String): String**使用分隔符将列表所有元素作为字符串显示 |
| 34   | **def reverse: List[A]**列表反转                             |
| 35   | **def sorted[B >: A]: List[A]**列表排序                      |
| 36   | **def startsWith[B](that: Seq[B], offset: Int): Boolean**检测列表在指定位置是否包含指定序列 |
| 37   | **def sum: A**计算集合元素之和                               |
| 38   | **def tail: List[A]**返回所有元素，除了第一个                |
| 39   | **def take(n: Int): List[A]**提取列表的前n个元素             |
| 40   | **def takeRight(n: Int): List[A]**提取列表的后n个元素        |
| 41   | **def toArray: Array[A]**列表转换为数组                      |
| 42   | **def toBuffer[B >: A]: Buffer[B]**返回缓冲区，包含了列表的所有元素 |
| 43   | **def toMap[T, U]: Map[T, U]**List 转换为 Map                |
| 44   | **def toSeq: Seq[A]**List 转换为 Seq                         |
| 45   | **def toSet[B >: A]: Set[B]**List 转换为 Set                 |
| 46   | **def toString(): String**列表转换为字符串                   |

### Set(集合)

Scala Set(集合)是没有重复的对象集合，所有的元素都是唯一的。

Scala 集合分为可变的和不可变的集合。

默认情况下，Scala 使用的是不可变集合，如果你想使用可变集合，需要引用 **scala.collection.mutable.Set** 包。

默认引用 scala.collection.immutable.Set，不可变集合实例如下：

```
val set = Set(1,2,3)
println(set.getClass.getName) // 

println(set.exists(_ % 2 == 0)) //true
println(set.drop(1)) //Set(2,3)
```

如果需要使用可变集合需要引入 scala.collection.mutable.Set：

```
import scala.collection.mutable.Set // 可以在任何地方引入 可变集合

val mutableSet = Set(1,2,3)
println(mutableSet.getClass.getName) // scala.collection.mutable.HashSet

mutableSet.add(4)
mutableSet.remove(1)
mutableSet += 5
mutableSet -= 2

println(mutableSet) // Set(5, 3, 4)

val another = mutableSet.toSet
println(another.getClass.getName) // scala.collection.immutable.Set
```

> **注意：** 虽然可变Set和不可变Set都有添加或删除元素的操作，但是有一个非常大的差别。对不可变Set进行操作，会产生一个新的set，原来的set并没有改变，这与List一样。 而对可变Set进行操作，改变的是该Set本身，与ListBuffer类似。

------

#### 集合基本操作

```scala
object Sets {
  def main(args: Array[String]): Unit = {
    val set = Set(1,2,3)
    println(set.getClass.getName) //不可变集合

    import scala.collection.mutable.Set
    val set1 = Set(1,2,3)
    println(set1.getClass.getName)  //可变集合

    println(set1.add(4))

    //注意： 虽然可变Set和不可变Set都有添加或删除元素的操作，
    // 但是有一个非常大的差别。对不可变Set进行操作，会产生一个新的set，原来的set并没有改变，这与List一样。 而对可变Set进行操作，改变的是该Set本身，与ListBuffer类似。

    val set4 = Set(10, 22, 31, 4, 51, 6)
    println("exists判断是否有被2整除的元素: " + set4.exists(_ % 2 == 0))
    println("contains元素在集合中是否存在: " + set.contains(3))
    println("filter输出符合指定条件的所有不可变集合元素: " + set4.filter(_ %2 == true))
    println("find查找不可变集合中满足指定条件的第一个元素: " + set4.find(_ %2 == true))

    println("head返回集合第一个元素: " + set.head)
    println("返回所有元素，除了最后一个: " + set.init)
    println("tail返回一个集合，包含除了第一元素之外的其他元素: " + set.tail)
    println("isEmpty是否为空：" + set.isEmpty)
    println("size返回不可变集合元素的数量：" + set.size)
    println("sum返回不可变集合中所有数字元素之和：" + set.sum)


    println("min集合中的最小元素是: " + set.min)
    println("max集合中的最大元素是: " + set.max)

    println("++合并两个集合: " + (set ++ set1))
    println("&查看两个集合的交集元素: " + set.&(set1))
    println("intersect查看两个集合的交集元素: " + set.intersect(set1))
    println("&~查看两个集合的差集元素: " + set.&~(set1))


    println("不可变，为集合添加新元素: " + (set+1))
    println("不可变，为集合添加新元素: " + (set+5))
    println("不可变，为集合添加新元素: " + set)
    println("可变，为集合添加新元素: " + (set1+1))
    println("可变，为集合添加新元素: " + (set1+5))
    println("可变，为集合添加新元素: " + set1)
    println("移除集合中指定的元素: " + (set-2))
    val setstr = Set("a", "b", "c")
    println("drop返回丢弃前n个元素新集合: " + setstr.drop(2))
    println("take返回前 n 个元素: " + setstr.take(2))
    println("takeRight返回后 n 个元素: " + setstr.takeRight(2))

    println("将集合转换为数组: " + setstr.toArray)
    println("返回缓冲区: " + setstr.toBuffer)
    println("返回 List: " + setstr.toList)
//    println("返回 Map: " + setstr.toMap)
    println("返回一个字符串: " + setstr.toString())
    println("创建一个新的迭代器来迭代元素: " + setstr.iterator)

  }
```

### Iterator(迭代器)

Scala Iterator（迭代器）不是一个集合，它是一种用于访问集合的方法。

迭代器 it 的两个基本操作是 **next** 和 **hasNext**。

调用 **it.next()** 会返回迭代器的下一个元素，并且更新迭代器的状态。

调用 **it.hasNext()** 用于检测集合中是否还有元素。

让迭代器 it 逐个返回所有元素最简单的方法是使用 while 循环：

| 参数名    | 说明                     | 例子        |
| --------- | ------------------------ | ----------- |
| user      | 登陆系统的账号名         | zhangsan    |
| workOrder | 工单依据，金库审批的理由 |             |
| msisdn    | 查询的手机号码           | 18812341234 |






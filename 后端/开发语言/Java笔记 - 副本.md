![image-20200810101109280](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810101109280.png)

## 进程

参考：https://mp.weixin.qq.com/s/4fwGeor9Jlu9vJkvCy_Vdw

### 进程的概念

我们编写的代码只是一个存储在硬盘的静态文件，通过编译后就会生成二进制可执行文件，当我们运行这个可执行文件后，它会被装载到内存中，接着 CPU 会执行程序中的每一条指令，那么这个**运行中的程序，就被称为「进程」**。

现在我们考虑有一个会读取硬盘文件数据的程序被执行了，那么当运行到读取文件的指令时，就会去从硬盘读取数据，但是硬盘的读写速度是非常慢的，那么在这个时候，如果 CPU 傻傻的等硬盘返回数据的话，那 CPU 的利用率是非常低的。

做个类比，你去煮开水时，你会傻傻的等水壶烧开吗？很明显，小孩也不会傻等。我们可以在水壶烧开之前去做其他事情。当水壶烧开了，我们自然就会听到“嘀嘀嘀”的声音，于是再把烧开的水倒入到水杯里就好了。

所以，当进程要从硬盘读取数据时，CPU 不需要阻塞等待数据的返回，而是去执行另外的进程。当硬盘数据返回时，CPU 会收到个**中断**，于是 CPU 再继续运行这个进程。

![image-20200810101038256](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810101038256.png)

这种**多个程序、交替执行**的思想，就有 CPU 管理多个进程的初步想法。

对于一个支持多进程的系统，CPU 会从一个进程快速切换至另一个进程，其间每个进程各运行几十或几百个毫秒。

虽然单核的 CPU 在某一个瞬间，只能运行一个进程。但在 1 秒钟期间，它可能会运行多个进程，这样就产生**并行的错觉**，实际上这是**并发**。

> 并发和并行有什么区别？

一图胜千言。

![并发与并行](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810101432221.png)

> 进程与程序的关系的类比

到了晚饭时间，一对小情侣肚子都咕咕叫了，于是男生见机行事，就想给女生做晚饭，所以他就在网上找了辣子鸡的菜谱，接着买了一些鸡肉、辣椒、香料等材料，然后边看边学边做这道菜。

![image-20200810144821882](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810144821882.png)

突然，女生说她想喝可乐，那么男生只好把做菜的事情暂停一下，并在手机菜谱标记做到哪一个步骤，把状态信息记录了下来。

然后男生听从女生的指令，跑去下楼买了一瓶冰可乐后，又回到厨房继续做菜。

**这体现了，CPU 可以从一个进程（做菜）切换到另外一个进程（买可乐），在切换前必须要记录当前进程中运行的状态信息，以备下次切换回来的时候可以恢复执行。**

所以，可以发现进程有着「**运行 - 暂停 - 运行**」的活动规律。

### 进程的状态

在上面，我们知道了进程有着「运行 - 暂停 - 运行」的活动规律。一般说来，一个进程并不是自始至终连续不停地运行的，它与并发执行中的其他进程的执行是相互制约的。

它有时处于运行状态，有时又由于某种原因而暂停运行处于等待状态，当使它暂停的原因消失后，它又进入准备运行状态。

所以，**在一个进程的活动期间至少具备三种基本状态，即运行状态、就绪状态、阻塞状态。**

![image-20200810101849045](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810101849045.png)

上图中各个状态的意义：

- 运行状态（*Runing*）：该时刻进程占用 CPU；
- 就绪状态（*Ready*）：可运行，但因为其他进程正在运行而暂停停止；
- 阻塞状态（*Blocked*）：该进程正在等待某一事件发生（如等待输入/输出操作的完成）而暂时停止运行，这时，即使给它CPU控制权，它也无法运行；

当然，进程另外两个基本状态：

- 创建状态（*new*）：进程正在被创建时的状态；
- 结束状态（*Exit*）：进程正在从系统中消失时的状态；

于是，一个完整的进程状态的变迁如下图：

![image-20200810102101639](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810102101639.png)



再来详细说明一下进程的状态变迁：

再来详细说明一下进程的状态变迁：

- *NULL -> 创建状态*：一个新进程被创建时的第一个状态；
- *创建状态 -> 就绪状态*：当进程被创建完成并初始化后，一切就绪准备运行时，变为就绪状态，这个过程是很快的；
- *就绪态 -> 运行状态*：处于就绪状态的进程被操作系统的进程调度器选中后，就分配给 CPU 正式运行该进程；
- *运行状态 -> 结束状态*：当进程已经运行完成或出错时，会被操作系统作结束状态处理；
- *运行状态 -> 就绪状态*：处于运行状态的进程在运行过程中，由于分配给它的运行时间片用完，操作系统会把该进程变为就绪态，接着从就绪态选中另外一个进程运行；
- *运行状态 -> 阻塞状态*：当进程请求某个事件且必须等待时，例如请求 I/O 事件；
- *阻塞状态 -> 就绪状态*：当进程要等待的事件完成时，它从阻塞状态变到就绪状态；

另外，还有一个状态叫**挂起状态**，它表示进程没有占有物理内存空间。这跟阻塞状态是不一样，阻塞状态是等待某个事件的返回。

由于虚拟内存管理原因，进程的所使用的空间可能并没有映射到物理内存，而是在硬盘上，这时进程就会出现挂起状态，另外调用 sleep 也会被挂起。

![image-20200810104548184](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810104548184.png)

挂起状态可以分为两种：

- 阻塞挂起状态：进程在外存（硬盘）并等待某个事件的出现；
- 就绪挂起状态：进程在外存（硬盘），但只要进入内存，即刻立刻运行；

这两种挂起状态加上前面的五种状态，就变成了七种状态变迁（留给我的颜色不多了），见如下图：

![image-20200810104624272](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810104624272.png)

### 进程的控制结构

在操作系统中，是用**进程控制块**（*process control block，PCB*）数据结构来描述进程的。

**PCB 是进程存在的唯一标识**，这意味着一个进程的存在，必然会有一个 PCB，如果进程消失了，那么 PCB 也会随之消失。

> PCB 具体包含什么信息呢？

**进程描述信息：**

- 进程标识符：标识各个进程，每个进程都有一个并且唯一的标识符；
- 用户标识符：进程归属的用户，用户标识符主要为共享和保护服务；

**进程控制和管理信息：**

- 进程当前状态，如 new、ready、running、waiting 或 blocked 等；
- 进程优先级：进程抢占 CPU 时的优先级；

**资源分配清单：**

- 有关内存地址空间或虚拟地址空间的信息，所打开文件的列表和所使用的 I/O 设备信息。

**CPU 相关信息：**

- CPU 中各个寄存器的值，当进程被切换时，CPU 的状态信息都会被保存在相应的 PCB 中，以便进程重新执行时，能从断点处继续执行。

可见，PCB 包含信息还是比较多的。

> 每个 PCB 是如何组织的呢？

通常是通过**链表**的方式进行组织，把具有**相同状态的进程链在一起，组成各种队列**。比如：

- 将所有处于就绪状态的进程链在一起，称为**就绪队列**；
- 把所有因等待某事件而处于等待状态的进程链在一起就组成各种**阻塞队列**；
- 另外，对于运行队列在单核 CPU 系统中则只有一个运行指针了，因为单核 CPU 在某个时间，只能运行一个程序。

那么，就绪队列和阻塞队列链表的组织形式如下图：

![image-20200810105107298](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810105107298.png)

除了链接的组织方式，还有索引方式，它的工作原理：将同一状态的进程组织在一个索引表中，索引表项指向相应的 PCB，不同状态对应不同的索引表。

一般会选择链表，因为可能面临进程创建，销毁等调度导致进程状态发生变化，所以链表能够更加灵活的插入和删除。

### 进程的控制

我们熟知了进程的状态变迁和进程的数据结构 PCB 后，再来看看进程的**创建、终止、阻塞、唤醒**的过程，这些过程也就是进程的控制。

**01 创建进程**

操作系统允许一个进程创建另一个进程，而且允许子进程继承父进程所拥有的资源，当子进程被终止时，其在父进程处继承的资源应当还给父进程。同时，终止父进程时同时也会终止其所有的子进程。

创建进程的过程如下：

- 为新进程分配一个唯一的进程标识号，并申请一个空白的 PCB，PCB 是有限的，若申请失败则创建失败；
- 为进程分配资源，此处如果资源不足，进程就会进入等待状态，以等待资源；
- 初始化 PCB；
- 如果进程的调度队列能够接纳新进程，那就将进程插入到就绪队列，等待被调度运行；

**02 终止进程**

进程可以有 3 种终止方式：正常结束、异常结束以及外界干预（信号 `kill` 掉）。

终止进程的过程如下：

- 查找需要终止的进程的 PCB；
- 如果处于执行状态，则立即终止该进程的执行，然后将 CPU 资源分配给其他进程；
- 如果其还有子进程，则应将其所有子进程终止；
- 将该进程所拥有的全部资源都归还给父进程或操作系统；
- 将其从 PCB 所在队列中删除；

**03 阻塞进程**

当进程需要等待某一事件完成时，它可以调用阻塞语句把自己阻塞等待。而一旦被阻塞等待，它只能由另一个进程唤醒。

阻塞进程的过程如下：

- 找到将要被阻塞进程标识号对应的 PCB；
- 如果该进程为运行状态，则保护其现场，将其状态转为阻塞状态，停止运行；
- 将该 PCB 插入的阻塞队列中去；

**04 唤醒进程**

进程由「运行」转变为「阻塞」状态是由于进程必须等待某一事件的完成，所以处于阻塞状态的进程是绝对不可能叫醒自己的。

如果某进程正在等待 I/O 事件，需由别的进程发消息给它，则只有当该进程所期待的事件出现时，才由发现者进程用唤醒语句叫醒它。

唤醒进程的过程如下：

- 在该事件的阻塞队列中找到相应进程的 PCB；
- 将其从阻塞队列中移出，并置其状态为就绪状态；
- 把该 PCB 插入到就绪队列中，等待调度程序调度；

进程的阻塞和唤醒是一对功能相反的语句，如果某个进程调用了阻塞语句，则必有一个与之对应的唤醒语句。

### 进程的上下文切换

各个进程之间是共享 CPU 资源的，在不同的时候进程之间需要切换，让不同的进程可以在 CPU 执行，那么这个**一个进程切换到另一个进程运行，称为进程的上下文切换**。

> 在详细说进程上下文切换前，我们先来看看 CPU 上下文切换

大多数操作系统都是多任务，通常支持大于 CPU 数量的任务同时运行。实际上，这些任务并不是同时运行的，只是因为系统在很短的时间内，让各个任务分别在 CPU 运行，于是就造成同时运行的错觉。

任务是交给 CPU 运行的，那么在每个任务运行前，CPU 需要知道任务从哪里加载，又从哪里开始运行。

所以，操作系统需要事先帮 CPU 设置好 **CPU 寄存器和程序计数器**。

CPU 寄存器是 CPU 内部一个容量小，但是速度极快的内存（缓存）。我举个例子，寄存器像是你的口袋，内存像你的书包，硬盘则是你家里的柜子，如果你的东西存放到口袋，那肯定是比你从书包或家里柜子取出来要快的多。

再来，程序计数器则是用来存储 CPU 正在执行的指令位置、或者即将执行的下一条指令位置。

所以说，CPU 寄存器和程序计数是 CPU 在运行任何任务前，所必须依赖的环境，这些环境就叫做 **CPU 上下文**。

既然知道了什么是 CPU 上下文，那理解 CPU 上下文切换就不难了。

CPU 上下文切换就是先把前一个任务的 CPU 上下文（CPU 寄存器和程序计数器）保存起来，然后加载新任务的上下文到这些寄存器和程序计数器，最后再跳转到程序计数器所指的新位置，运行新任务。

系统内核会存储保持下来的上下文信息，当此任务再次被分配给 CPU 运行时，CPU 会重新加载这些上下文，这样就能保证任务原来的状态不受影响，让任务看起来还是连续运行。

上面说到所谓的「任务」，主要包含进程、线程和中断。所以，可以根据任务的不同，把 CPU 上下文切换分成：**进程上下文切换、线程上下文切换和中断上下文切换**。

> 进程的上下文切换到底是切换什么呢？

进程是由内核管理和调度的，所以进程的切换只能发生在内核态。

所以，**进程的上下文切换不仅包含了虚拟内存、栈、全局变量等用户空间的资源，还包括了内核堆栈、寄存器等内核空间的资源。**

通常，会把交换的信息保存在进程的 PCB，当要运行另外一个进程的时候，我们需要从这个进程的 PCB 取出上下文，然后恢复到 CPU 中，这使得这个进程可以继续执行，如下图所示：

![image-20200810115632778](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810115632778.png)

大家需要注意，进程的上下文开销是很关键的，我们希望它的开销越小越好，这样可以使得进程可以把更多时间花费在执行程序上，而不是耗费在上下文切换。

> 发生进程上下文切换有哪些场景？

- 为了保证所有进程可以得到公平调度，CPU 时间被划分为一段段的时间片，这些时间片再被轮流分配给各个进程。这样，当某个进程的时间片耗尽了，就会被系统挂起，切换到其它正在等待 CPU 的进程运行；
- 进程在系统资源不足（比如内存不足）时，要等到资源满足后才可以运行，这个时候进程也会被挂起，并由系统调度其他进程运行；
- 当进程通过睡眠函数 sleep 这样的方法将自己主动挂起时，自然也会重新调度；
- 当有优先级更高的进程运行时，为了保证高优先级进程的运行，当前进程会被挂起，由高优先级进程来运行；
- 发生硬件中断时，CPU 上的进程会被中断挂起，转而执行内核中的中断服务程序；

以上，就是发生进程上下文切换的常见场景了。、

## 线程

在早期的操作系统中都是以进程作为独立运行的基本单位，直到后面，计算机科学家们又提出了更小的能独立运行的基本单位，也就是**线程。**

### 为什么使用线程？

我们举个例子，假设你要编写一个视频播放器软件，那么该软件功能的核心模块有三个：

- 从视频文件当中读取数据；
- 对读取的数据进行解压缩；
- 把解压缩后的视频数据播放出来；

对于单进程的实现方式，我想大家都会是以下这个方式：

![单进程实现方式](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810145840329.png)

对于单进程的这种方式，存在以下问题：

- 播放出来的画面和声音会不连贯，因为当 CPU 能力不够强的时候，`Read` 的时候可能进程就等在这了，这样就会导致等半天才进行数据解压和播放；
- 各个函数之间不是并发执行，影响资源的使用效率；

那改进成多进程的方式：

![image-20200810150004879](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810150004879.png)

对于多进程的这种方式，依然会存在问题：

- 进程之间如何通信，共享数据？
- 维护进程的系统开销较大，如创建进程时，分配资源、建立 PCB；终止进程时，回收资源、撤销 PCB；进程切换时，保存当前进程的状态信息；

那到底如何解决呢？需要有一种新的实体，满足以下特性：

- 实体之间可以并发运行；
- 实体之间共享相同的地址空间；

这个新的实体，就是**线程( \*Thread\* )**，线程之间可以并发运行且共享相同的地址空间。

### 什么是线程？

**线程是进程当中的一条执行流程。**

同一个进程内多个线程之间可以共享代码段、数据段、打开的文件等资源，但每个线程都有独立一套的寄存器和栈，这样可以确保线程的控制流是相对独立的。

![image-20200810150223943](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810150223943.png)

> 
> 线程的优缺点？

线程的优点：

- 一个进程中可以同时存在多个线程；
- 各个线程之间可以并发执行；
- 各个线程之间可以共享地址空间和文件等资源；

线程的缺点：

- 当进程中的一个线程奔溃时，会导致其所属进程的所有线程奔溃。

举个例子，对于游戏的用户设计，则不应该使用多线程的方式，否则一个用户挂了，会影响其他同个进程的线程。

### 线程与进程的比较

线程与进程的比较如下：

- 进程是资源（包括内存、打开的文件等）分配的单位，线程是 CPU 调度的单位；
- 进程拥有一个完整的资源平台，而线程只独享必不可少的资源，如寄存器和栈；
- 线程同样具有就绪、阻塞、执行三种基本状态，同样具有状态之间的转换关系；
- 线程能减少并发执行的时间和空间开销；

对于，线程相比进程能减少开销，体现在：

- 线程的创建时间比进程快，因为进程在创建的过程中，还需要资源管理信息，比如内存管理信息、文件管理信息，而线程在创建的过程中，不会涉及这些资源管理信息，而是共享它们；
- 线程的终止时间比进程快，因为线程释放的资源相比进程少很多；
- 同一个进程内的线程切换比进程切换快，因为线程具有相同的地址空间（虚拟内存共享），这意味着同一个进程的线程都具有同一个页表，那么在切换的时候不需要切换页表。而对于进程之间的切换，切换的时候要把页表给切换掉，而页表的切换过程开销是比较大的；
- 由于同一进程的各线程间共享内存和文件资源，那么在线程之间数据传递的时候，就不需要经过内核了，这就使得线程之间的数据交互效率更高了；

所以，线程比进程不管是时间效率，还是空间效率都要高。

### 线程的上下文切换

在前面我们知道了，线程与进程最大的区别在于：**线程是调度的基本单位，而进程则是资源拥有的基本单位**。

所以，所谓操作系统的任务调度，实际上的调度对象是线程，而进程只是给线程提供了虚拟内存、全局变量等资源。

对于线程和进程，我们可以这么理解：

- 当进程只有一个线程时，可以认为进程就等于线程；
- 当进程拥有多个线程时，这些线程会共享相同的虚拟内存和全局变量等资源，这些资源在上下文切换时是不需要修改的；

另外，线程也有自己的私有数据，比如栈和寄存器等，这些在上下文切换时也是需要保存的。

> 线程上下文切换的是什么？

这还得看线程是不是属于同一个进程：

- 当两个线程不是属于同一个进程，则切换的过程就跟进程上下文切换一样；
- **当两个线程是属于同一个进程，因为虚拟内存是共享的，所以在切换时，虚拟内存这些资源就保持不动，只需要切换线程的私有数据、寄存器等不共享的数据**；

所以，线程的上下文切换相比进程，开销要小很多。

### 线程的实现

主要有三种线程的实现方式：

- **用户线程（\*User Thread\*）**：在用户空间实现的线程，不是由内核管理的线程，是由用户态的线程库来完成线程的管理；
- **内核线程（\*Kernel Thread\*）**：在内核中实现的线程，是由内核管理的线程；
- **轻量级进程（\*LightWeight Process\*）**：在内核中来支持用户线程；

那么，这还需要考虑一个问题，用户线程和内核线程的对应关系。

首先，第一种关系是**多对一**的关系，也就是多个用户线程对应同一个内核线程：

![image-20200810151415031](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810151415031.png)

第二种是**一对一**的关系，也就是一个用户线程对应一个内核线程：

![image-20200810151434062](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810151434062.png)

第三种是**多对多**的关系，也就是多个用户线程对应到多个内核线程：

![image-20200810151452206](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810151452206.png)

> 用户线程如何理解？存在什么优势和缺陷？

用户线程是基于用户态的线程管理库来实现的，那么**线程控制块（\*Thread Control Block, TCB\*）** 也是在库里面来实现的，对于操作系统而言是看不到这个 TCB 的，它只能看到整个进程的 PCB。

所以，**用户线程的整个线程管理和调度，操作系统是不直接参与的，而是由用户级线程库函数来完成线程的管理，包括线程的创建、终止、同步和调度等。**

用户级线程的模型，也就类似前面提到的**多对一**的关系，即多个用户线程对应同一个内核线程，如下图所示：

![image-20200810151617772](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810151617772.png)

用户线程的**优点**：

- 每个进程都需要有它私有的线程控制块（TCB）列表，用来跟踪记录它各个线程状态信息（PC、栈指针、寄存器），TCB 由用户级线程库函数来维护，可用于不支持线程技术的操作系统；
- 用户线程的切换也是由线程库函数来完成的，无需用户态与内核态的切换，所以速度特别快；

用户线程的**缺点**：

- 由于操作系统不参与线程的调度，如果一个线程发起了系统调用而阻塞，那进程所包含的用户线程都不能执行了。
- 当一个线程开始运行后，除非它主动地交出 CPU 的使用权，否则它所在的进程当中的其他线程无法运行，因为用户态的线程没法打断当前运行中的线程，它没有这个特权，只有操作系统才有，但是用户线程不是由操作系统管理的。
- 由于时间片分配给进程，故与其他进程比，在多线程执行时，每个线程得到的时间片较少，执行会比较慢；

> 那内核线程如何理解？存在什么优势和缺陷？

**内核线程是由操作系统管理的，线程对应的 TCB 自然是放在操作系统里的，这样线程的创建、终止和管理都是由操作系统负责。**

内核线程的模型，也就类似前面提到的**一对一**的关系，即一个用户线程对应一个内核线程，如下图所示：

![image-20200810151929315](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810151929315.png)

内核线程的**优点**：

- 在一个进程当中，如果某个内核线程发起系统调用而被阻塞，并不会影响其他内核线程的运行；
- 分配给线程，多线程的进程获得更多的 CPU 运行时间；

内核线程的**缺点**：

- 在支持内核线程的操作系统中，由内核来维护进程和线程的上下问信息，如 PCB 和 TCB；
- 线程的创建、终止和切换都是通过系统调用的方式来进行，因此对于系统来说，系统开销比较大；

> 最后的轻量级进程如何理解？

**轻量级进程（\*Light-weight process，LWP\*）是内核支持的用户线程，一个进程可有一个或多个 LWP，每个 LWP 是跟内核线程一对一映射的，也就是 LWP 都是由一个内核线程支持。**

另外，LWP 只能由内核管理并像普通进程一样被调度，Linux 内核是支持 LWP 的典型例子。

在大多数系统中，**LWP与普通进程的区别也在于它只有一个最小的执行上下文和调度程序所需的统计信息**。一般来说，一个进程代表程序的一个实例，而 LWP 代表程序的执行线程，因为一个执行线程不像进程那样需要那么多状态信息，所以 LWP 也不带有这样的信息。

在 LWP 之上也是可以使用用户线程的，那么 LWP 与用户线程的对应关系就有三种：

- `1 : 1`，即一个 LWP 对应 一个用户线程；
- `N : 1`，即一个 LWP 对应多个用户线程；
- `N : N`，即多个 LMP 对应多个用户线程；

接下来针对上面这三种对应关系说明它们优缺点。先下图的 LWP 模型：

![image-20200810152223708](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810152223708.png)

**1 : 1 模式**

一个线程对应到一个 LWP 再对应到一个内核线程，如上图的进程 4，属于此模型。

- 优点：实现并行，当一个 LWP 阻塞，不会影响其他 LWP；
- 缺点：每一个用户线程，就产生一个内核线程，创建线程的开销较大。

**N : 1 模式**

多个用户线程对应一个 LWP 再对应一个内核线程，如上图的进程 2，线程管理是在用户空间完成的，此模式中用户的线程对操作系统不可见。

- 优点：用户线程要开几个都没问题，且上下文切换发生用户空间，切换的效率较高；
- 缺点：一个用户线程如果阻塞了，则整个进程都将会阻塞，另外在多核 CPU  中，是没办法充分利用 CPU 的。

**M : N 模式**

根据前面的两个模型混搭一起，就形成 `M:N` 模型，该模型提供了两级控制，首先多个用户线程对应到多个 LWP，LWP 再一一对应到内核线程，如上图的进程 3。

- 优点：综合了前两种优点，大部分的线程上下文发生在用户空间，且多个线程又可以充分利用多核 CPU 的资源。

**组合模式**

如上图的进程 5，此进程结合 `1:1` 模型和 `M:N` 模型。开发人员可以针对不同的应用特点调节内核线程的数目来达到物理并行性和逻辑并行性的最佳方案。



## 调度

进程都希望自己能够占用 CPU 进行工作，那么这涉及到前面说过的进程上下文切换。

一旦操作系统把进程切换到运行状态，也就意味着该进程占用着 CPU 在执行，但是当操作系统把进程切换到其他状态时，那就不能在 CPU 中执行了，于是操作系统会选择下一个要运行的进程。

选择一个进程运行这一功能是在操作系统中完成的，通常称为**调度程序**（*scheduler*）。

那到底什么时候调度进程，或以什么原则来调度进程呢？

### 调度时机

在进程的生命周期中，当进程从一个运行状态到另外一状态变化的时候，其实会触发一次调度。

比如，以下状态的变化都会触发操作系统的调度：

- *从就绪态 -> 运行态*：当进程被创建时，会进入到就绪队列，操作系统会从就绪队列选择一个进程运行；
- *从运行态 -> 阻塞态*：当进程发生 I/O 事件而阻塞时，操作系统必须另外一个进程运行；
- *从运行态 -> 结束态*：当进程退出结束后，操作系统得从就绪队列选择另外一个进程运行；

因为，这些状态变化的时候，操作系统需要考虑是否要让新的进程给 CPU 运行，或者是否让当前进程从 CPU 上退出来而换另一个进程运行。

另外，如果硬件时钟提供某个频率的周期性中断，那么可以根据如何处理时钟中断
，把调度算法分为两类：

- **非抢占式调度算法**挑选一个进程，然后让该进程运行直到被阻塞，或者直到该进程退出，才会调用另外一个进程，也就是说不会理时钟中断这个事情。
- **抢占式调度算法**挑选一个进程，然后让该进程只运行某段时间，如果在该时段结束时，该进程仍然在运行时，则会把它挂起，接着调度程序从就绪队列挑选另外一个进程。这种抢占式调度处理，需要在时间间隔的末端发生**时钟中断**，以便把 CPU 控制返回给调度程序进行调度，也就是常说的**时间片机制**。

### 调度原则

*原则一*：如果运行的程序，发生了 I/O 事件的请求，那 CPU 使用率必然会很低，因为此时进程在阻塞等待硬盘的数据返回。这样的过程，势必会造成 CPU 突然的空闲。所以，**为了提高 CPU 利用率，在这种发送 I/O 事件致使 CPU 空闲的情况下，调度程序需要从就绪队列中选择一个进程来运行。**

*原则二*：有的程序执行某个任务花费的时间会比较长，如果这个程序一直占用着 CPU，会造成系统吞吐量（CPU 在单位时间内完成的进程数量）的降低。所以，**要提高系统的吞吐率，调度程序要权衡长任务和短任务进程的运行完成数量。**

*原则三*：从进程开始到结束的过程中，实际上是包含两个时间，分别是进程运行时间和进程等待时间，这两个时间总和就称为周转时间。进程的周转时间越小越好，**如果进程的等待时间很长而运行时间很短，那周转时间就很长，这不是我们所期望的，调度程序应该避免这种情况发生。**

*原则四*：处于就绪队列的进程，也不能等太久，当然希望这个等待的时间越短越好，这样可以使得进程更快的在 CPU 中执行。所以，**就绪队列中进程的等待时间也是调度程序所需要考虑的原则。**

*原则五*：对于鼠标、键盘这种交互式比较强的应用，我们当然希望它的响应时间越快越好，否则就会影响用户体验了。所以，**对于交互式比较强的应用，响应时间也是调度程序需要考虑的原则。**

![image-20200810152955083](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810152955083.png)

针对上面的五种调度原则，总结成如下：

- **CPU 利用率**：调度程序应确保 CPU 是始终匆忙的状态，这可提高 CPU 的利用率；
- **系统吞吐量**：吞吐量表示的是单位时间内 CPU 完成进程的数量，长作业的进程会占用较长的 CPU 资源，因此会降低吞吐量，相反，短作业的进程会提升系统吞吐量；
- **周转时间**：周转时间是进程运行和阻塞时间总和，一个进程的周转时间越小越好；
- **等待时间**：这个等待时间不是阻塞状态的时间，而是进程处于就绪队列的时间，等待的时间越长，用户越不满意；
- **响应时间**：用户提交请求到系统第一次产生响应所花费的时间，在交互式系统中，响应时间是衡量调度算法好坏的主要标准。

说白了，这么多调度原则，目的就是要使得进程要「快」。

### 调度算法

不同的调度算法适用的场景也是不同的。

接下来，说说在**单核 CPU 系统**中常见的调度算法。

#### 01 先来先服务调度算法

最简单的一个调度算法，就是非抢占式的**先来先服务（\*First Come First Severd, FCFS\*）算法**了。

![image-20200810153210885](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810153210885.png)

顾名思义，先来后到，**每次从就绪队列选择最先进入队列的进程，然后一直运行，直到进程退出或被阻塞，才会继续从队列中选择第一个进程接着运行。**

这似乎很公平，但是当一个长作业先运行了，那么后面的短作业等待的时间就会很长，不利于短作业。

FCFS 对长作业有利，适用于 CPU 繁忙型作业的系统，而不适用于 I/O 繁忙型作业的系统。

#### 02 最短作业优先调度算法

**最短作业优先（\*Shortest Job First, SJF\*）调度算法**同样也是顾名思义，它会**优先选择运行时间最短的进程来运行**，这有助于提高系统的吞吐量。

![SJF 调度算法](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810153308156.png)

这显然对长作业不利，很容易造成一种极端现象。

比如，一个长作业在就绪队列等待运行，而这个就绪队列有非常多的短作业，那么就会使得长作业不断的往后推，周转时间变长，致使长作业长期不会被运行。

#### 03 高响应比优先调度算法

前面的「先来先服务调度算法」和「最短作业优先调度算法」都没有很好的权衡短作业和长作业。

那么，**高响应比优先 （\*Highest Response Ratio Next, HRRN\*）调度算法**主要是权衡了短作业和长作业。

**每次进行进程调度时，先计算「响应比优先级」，然后把「响应比优先级」最高的进程投入运行**，「响应比优先级」的计算公式：

![image-20200810153526144](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810153526144.png)

从上面的公式，可以发现：

- 如果两个进程的「等待时间」相同时，「要求的服务时间」越短，「响应比」就越高，这样短作业的进程容易被选中运行；
- 如果两个进程「要求的服务时间」相同时，「等待时间」越长，「响应比」就越高，这就兼顾到了长作业进程，因为进程的响应比可以随时间等待的增加而提高，当其等待时间足够长时，其响应比便可以升到很高，从而获得运行的机会；

#### 04 时间片轮转调度算法

最古老、最简单、最公平且使用最广的算法就是**时间片轮转（\*Round Robin, RR\*）调度算法**。

![RR 调度算法](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810153651412.png)

**每个进程被分配一个时间段，称为时间片（\*Quantum\*），即允许该进程在该时间段中运行。**

- 如果时间片用完，进程还在运行，那么将会把此进程从 CPU 释放出来，并把 CPU 分配另外一个进程；
- 如果该进程在时间片结束前阻塞或结束，则 CPU 立即进行切换；

另外，时间片的长度就是一个很关键的点：

- 如果时间片设得太短会导致过多的进程上下文切换，降低了 CPU 效率；
- 如果设得太长又可能引起对短作业进程的响应时间变长。将

通常时间片设为 `20ms~50ms` 通常是一个比较合理的折中值。

#### 05 最高优先级调度算法

前面的「时间片轮转算法」做了个假设，即让所有的进程同等重要，也不偏袒谁，大家的运行时间都一样。

但是，对于多用户计算机系统就有不同的看法了，它们希望调度是有优先级的，即希望调度程序能**从就绪队列中选择最高优先级的进程进行运行，这称为最高优先级（\*Highest Priority First，HPF\*）调度算法**。

进程的优先级可以分为，静态优先级或动态优先级：

- 静态优先级：创建进程时候，就已经确定了优先级了，然后整个运行时间优先级都不会变化；
- 动态优先级：根据进程的动态变化调整优先级，比如如果进程运行时间增加，则降低其优先级，如果进程等待时间（就绪队列的等待时间）增加，则升高其优先级，也就是**随着时间的推移增加等待进程的优先级**。

该算法也有两种处理优先级高的方法，非抢占式和抢占式：

- 非抢占式：当就绪队列中出现优先级高的进程，运行完当前进程，再选择优先级高的进程。
- 抢占式：当就绪队列中出现优先级高的进程，当前进程挂起，调度优先级高的进程运行。

但是依然有缺点，可能会导致低优先级的进程永远不会运行。

#### 06 多级反馈队列调度算法

**多级反馈队列（\*Multilevel Feedback Queue\*）调度算法**是「时间片轮转算法」和「最高优先级算法」的综合和发展。

顾名思义：

- 「多级」表示有多个队列，每个队列优先级从高到低，同时优先级越高时间片越短。
- 「反馈」表示如果有新的进程加入优先级高的队列时，立刻停止当前正在运行的进程，转而去运行优先级高的队列；

![多级反馈队列](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810154112232.png)

来看看，它是如何工作的：

- 设置了多个队列，赋予每个队列不同的优先级，每个**队列优先级从高到低**，同时**优先级越高时间片越短**；
- 新的进程会被放入到第一级队列的末尾，按先来先服务的原则排队等待被调度，如果在第一级队列规定的时间片没运行完成，则将其转入到第二级队列的末尾，以此类推，直至完成；
- 当较高优先级的队列为空，才调度较低优先级的队列中的进程运行。如果进程运行时，有新进程进入较高优先级的队列，则停止当前运行的进程并将其移入到原队列末尾，接着让较高优先级的进程运行；

可以发现，对于短作业可能可以在第一级队列很快被处理完。对于长作业，如果在第一级队列处理不完，可以移入下次队列等待被执行，虽然等待的时间变长了，但是运行时间也会更长了，所以该算法很好的**兼顾了长短作业，同时有较好的响应时间。**

> 看的迷迷糊糊？那我拿去银行办业务的例子，把上面的调度算法串起来，你还不懂，你锤我！

**办理业务的客户相当于进程，银行窗口工作人员相当于 CPU。**

现在，假设这个银行只有一个窗口（单核 CPU ），那么工作人员一次只能处理一个业务。

![image-20200810154929344](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810154929344.png)

那么最简单的处理方式，就是先来的先处理，后面来的就乖乖排队，这就是**先来先服务（\*FCFS\*）调度算法**。但是万一先来的这位老哥是来贷款的，这一谈就好几个小时，一直占用着窗口，这样后面的人只能干等，或许后面的人只是想简单的取个钱，几分钟就能搞定，却因为前面老哥办长业务而要等几个小时，你说气不气人？

![image-20200810154941586](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810154941586.png)

有客户抱怨了，那我们就要改进，我们干脆优先给那些几分钟就能搞定的人办理业务，这就是**短作业优先（\*SJF\*）调度算法**。听起来不错，但是依然还是有个极端情况，万一办理短业务的人非常的多，这会导致长业务的人一直得不到服务，万一这个长业务是个大客户，那不就捡了芝麻丢了西瓜

![image-20200810154954124](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810154954124.png)

那就公平起见，现在窗口工作人员规定，每个人我只处理 10 分钟。如果 10 分钟之内处理完，就马上换下一个人。如果没处理完，依然换下一个人，但是客户自己得记住办理到哪个步骤了。这个也就是**时间片轮转（\*RR\*）调度算法**。但是如果时间片设置过短，那么就会造成大量的上下文切换，增大了系统开销。如果时间片过长，相当于退化成退化成 FCFS 算法了。

![image-20200810155010501](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810155010501.png)

既然公平也可能存在问题，那银行就对客户分等级，分为普通客户、VIP 客户、SVIP 客户。只要高优先级的客户一来，就第一时间处理这个客户，这就是**最高优先级（\*HPF\*）调度算法**。但依然也会有极端的问题，万一当天来的全是高级客户，那普通客户不是没有被服务的机会，不把普通客户当人是吗？那我们把优先级改成动态的，如果客户办理业务时间增加，则降低其优先级，如果客户等待时间增加，则升高其优先级。

![image-20200810155025825](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810155025825.png)

那有没有兼顾到公平和效率的方式呢？这里介绍一种算法，考虑的还算充分的，**多级反馈队列（\*MFQ\*）调度算法**，它是时间片轮转算法和优先级算法的综合和发展。它的工作方式：

![image-20200810155042750](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200810155042750.png)

- 银行设置了多个排队（就绪）队列，每个队列都有不同的优先级，**各个队列优先级从高到低**，同时每个队列执行时间片的长度也不同，**优先级越高的时间片越短**。
- 新客户（进程）来了，先进入第一级队列的末尾，按先来先服务原则排队等待被叫号（运行）。如果时间片用完客户的业务还没办理完成，则让客户进入到下一级队列的末尾，以此类推，直至客户业务办理完成。
- 当第一级队列没人排队时，就会叫号二级队列的客户。如果客户办理业务过程中，有新的客户加入到较高优先级的队列，那么此时办理中的客户需要停止办理，回到原队列的末尾等待再次叫号，因为要把窗口让给刚进入较高优先级队列的客户。

可以发现，对于要办理短业务的客户来说，可以很快的轮到并解决。对于要办理长业务的客户，一下子解决不了，就可以放到下一个队列，虽然等待的时间稍微变长了，但是轮到自己的办理时间也变长了，也可以接受，不会造成极端的现象，可以说是综合上面几种算法的优点。



## 多线程

**创建多少个线程合适？**参考：https://www.jianshu.com/p/f30ee2346f9f

线程就是独立的执行路径；

在程序运行时，即使没有自己创建线程，后台也会有多个线程，如主线程，gc线程；

main()称为主线程，为系统的入口，用于执行整个程序；

 对同一份资源操作时，会存在资源抢夺的问题，需要加入并发控制；

线程会带来额外的开销，如CPU调度时间，并发控制开销；

一个进程可以有多个线程，如视频中同时听声音、看图像、看弹幕、等等

### 调用方法原理

![image-20200503132035803](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200503132035803.png)



### Process与Thread

- **程序**是指令和数据的有序集合，其本身没有任何运行的含义，是一个静态的概念。
- 而**进程**则是执行程序的一次执行过程，他有一个动态的概念。是系统资源分配的单位
- 通常在一个进程中可以包含若干个**线程**，当然一个进程中至少有一个线程，不然没有存在的意义。线程是CPU调度和执行的单位。

> 注意：很多多线程是模拟出来的，真正的多线程是指由多个CPU，即多核，如服务器。如果是模拟出来的多线程，即在一个CPU的情况下，在同一个时间点，CPU只能执行一个代码，因为切换的很快，所以就有同时执行的错局。

### 线程创建

**Thread类、Runnable接口(`推荐`)、Callable接口**

1. 继承Thread类

   - 子类继承Thread类具备多线程能力
   - 启动线程：子类对象.start()
   - `不建议使用：避免OOP单继承局限性`

   ```java
   // 创建线程方式一：继承Thread类，重写run()方法，调用start开启线程
   // 总结：注意，线程开启不一定立即执行，由CPU调度执行
   public class TestThread1 extends Thread {
       @Override
       public void run() {
           // run方法线程体
           for(int i=0; i<200; i++){
               System.out.println("run方法---" + i);
           }
       }
   
       public static void main(String[] args){
           // main线程，主线程
   
           // 创建一个线程对象
           TestThread1 testThread1 = new TestThread1() ;
   
           // 调用start()方法开启线程
           testThread1.start();
   
           for(int i=0; i<200; i++){
               System.out.println("主线程---" + i);
           }
       }
   }
   ```

   

2. 实现Runnable接口

   - 实现接口Runnable具有多线程能力
   - 启动线程：传入目标对象+Thread对象.start()
   - 推荐使用：`避免单继承局限性，灵活方便，方便同时一个对象被多个线程使用`

   ```java
   // 创建线程方式二：实现runnable接口，重写run方法，执行线程需要丢入runnable接口实现类，调用start方法
   public class TestThread2 implements Runnable {
       @Override
       public void run() {
           // run方法线程体
           for(int i=0; i<200; i++){
               System.out.println("run方法---" + i);
           }
       }
   
       public static void main(String[] args){
           // main线程，主线程
   
           // 创建runnable接口实现类对象
           TestThread2 testThread2 = new TestThread2() ;
           // 创建线程对象，通过线程对象来开启我们的线程，代理
           new Thread(testThread2).start();
   
           for(int i=0; i<1000; i++){
               System.out.println("主线程---" + i);
           }
       }
   }
   ```

   

3. 实现Callable接口

   ```java
   // 线程创建方式三：实现callable接口
   public class TestCallable implements Callable<Boolean> {
       private String name;
       public TestCallable(String name){
           this.name = name;
       }
       @Override
       public Boolean call() {
           // run方法线程体
           for(int i=0; i<1000; i++){
   
           }
           System.out.println("完成call方法---" + name);
           return true;
       }
   
       public static void main(String[] args) throws ExecutionException, InterruptedException {
           // main线程，主线程
           TestCallable t1 = new TestCallable("1");
           TestCallable t2 = new TestCallable("2");
           TestCallable t3 = new TestCallable("3");
   
           // 创建执行服务
           ExecutorService ser = Executors.newFixedThreadPool(3);
   
           // 提交执行
           Future<Boolean> r1 = ser.submit(t1);
           Future<Boolean> r2 = ser.submit(t2);
           Future<Boolean> r3 = ser.submit(t3);
   
           // 获取结果
           boolean rs1 = r1.get();
           boolean rs2 = r2.get();
           boolean rs3 = r3.get();
   
           // 关闭服务
           ser.shutdown();
       }
   }
   ```

4. 初始并发问题

   下面代码会出现两个人拥有同一个票的情况，多个线程操作同一个资源的情况下，线程不安全，数据紊乱。

   ```java
   // 多个线程同时操作一个对象
   // 买火车票的例子
   public class TestThread4 implements Runnable {
       // 票数
       private int ticketNums = 10;
   
       @Override
       public void run(){
           while (true){
               if(ticketNums <= 0){
                   break;
               }
               //模拟延时
               try {
                   Thread.sleep(200);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
               System.out.println(Thread.currentThread().getName() + "-->拿到了第" + ticketNums-- + "票" );
           }
       }
   
       public static void main(String[] args) {
           TestThread4 ticket = new TestThread4();
   
           new Thread(ticket, "小明").start();
           new Thread(ticket, "老师").start();
           new Thread(ticket, "黄牛").start();
       }
   }
   ```

### 线程的状态

![image-20200503220257143](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200503220257143.png)

| 方法                           | 说明                                       |
| ------------------------------ | ------------------------------------------ |
| setPriority(int newPriority)   | 更改线程的优先级                           |
| static void sleep(long millis) | 在指定的毫秒数内让当前正在执行的线程休眠   |
| void join()                    | 等待该线程终止                             |
| static void yieId()            | 暂停当前正在执行的线程对象，并执行其他线程 |
| void interrupt()               | 中断线程，别用这个方式                     |
| boolean isAlive()              | 测试线程是否出于活动状态                   |

#### 停止线程

![image-20200503220923295](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200503220923295.png)

#### sleep

- sleep(时间)指定当前线程阻塞的毫秒数；
- sleep存在异常InterruptException;
- sleep时间达到后线程进入就绪状态；
- sleep可以模拟网络延时，倒计时等；
- 每一个对象都有一个锁，sleep不会释放锁。

#### yieId

![image-20200503222810976](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200503222810976.png)

#### Join

- Join合并线程，待此线程执行完成后，再执行其他线程，其他线程阻塞
- 可以想象成插队

```java
// 测试join方法
public class TestJoin implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("线程vip来了" + i);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        TestJoin t = new TestJoin();
        Thread  thread = new Thread(t);
        thread.start();

        // 主线程
        for (int i = 0; i < 500; i++) {
            if ( i == 200 ){
                thread.join();
            }
            System.out.println("main" + i);
        }
    }
}
```

#### 观察线程状态

```java
// 测试线程的状态
public class TestState {
    public static void main(String[] args) {
        Thread thread = new Thread(()->{
            for (int i = 0; i < 5; i++) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("////////");
        });

        // 观察状态
        Thread.State state = thread.getState();
        System.out.println(state);

        // 启动后
        thread.start();
        state = thread.getState();
        System.out.println(state);

        while (state != Thread.State.TERMINATED){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            state = thread.getState();
            System.out.println(state);
        }
    }
}
```

#### 线程优先级

![image-20200504092900165](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200504092900165.png)

```java
public class TestPriority {
    public static void main(String[] args) {
        // 主线程优先级
        System.out.println(Thread.currentThread().getName() + "-->" + Thread.currentThread().getPriority());

        MyPriority myPriority = new MyPriority();

        Thread t1 = new Thread(myPriority);
        Thread t2 = new Thread(myPriority);
        Thread t3 = new Thread(myPriority);
        Thread t4 = new Thread(myPriority);
        Thread t5 = new Thread(myPriority);
        Thread t6 = new Thread(myPriority);

        // 设置优先级,再启动
        t1.start();

        t2.setPriority(2);
        t2.start();

        t3.setPriority(4);
        t3.start();

        t4.setPriority(Thread.MAX_PRIORITY);
        t4.start();

        t5.setPriority(8);
        t5.start();

        t6.setPriority(Thread.MIN_PRIORITY);
        t6.start();

    }
}

class MyPriority implements Runnable{

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + "-->" + Thread.currentThread().getPriority());
    }
}
```

守护(daemon)线程

- 线程分为`用户线程`和`守护线程`
- 虚拟机必须确保 用户线程执行完毕
- 虚拟机不用等待守护线程执行完毕
- 如，后台记录操作日志、监控内存、垃圾回收等待..

```java
// 测试守护线程
public class TestDaemon {
    public static void main(String[] args) {
        God god = new God();
        You1 you1 = new You1();

        Thread thread = new Thread(god);
        // 默认是false表示是用户线程，正常的线程都是用户线程。。。
        thread.setDaemon(true);

        thread.start();

        new Thread(you1).start();
    }
}


class God implements Runnable {

    @Override
    public void run() {
        while(true){
            System.out.println("上帝保佑你");
        }
    }
}

class You1 implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 300; i++) {
            System.out.println("你一生开心的活着");
        }
        System.out.println("----goodbye! world!-------");
    }
}
```

#### 线程同步

##### 并发

同一个对象被多个线程同时操作

`队列`+`锁`保存线程安全

由于同一进程的多个线程共享同一块存储空间，在带来方便的同时，也带来了访问冲突问题，为了保证数据在方法中被访问时的正确性，在访问时加入`锁机制synchronized`，当一个线程获得对象的排它锁，独占资源，其他线程必须等待，使用后释放锁即可，存在以下问题：

- 一个线程持有锁会导致其他所有需要次锁的线程挂起；
- 在多线程竞争下，加锁，释放锁会导致比较多的上下文切换和调度延时，引起性能问题；
- 如果一个优先级高的线程等待一个优先级低的线程释放锁，会导致优先级倒置，引起性能问题。

```java
// 不安全的买票
public class UnsafeBuyTicket {
    public static void main(String[] args) {
        BuyTicket buyTicket = new BuyTicket();

        new Thread(buyTicket,"我").start();
        new Thread(buyTicket,"你").start();
        new Thread(buyTicket,"黄牛").start();
    }
}


class BuyTicket implements Runnable {
    // 票数
    private int ticketNums = 10;
    // 外部停止方法
    boolean flag = true;

    @Override
    public void run() {
        // 买票
        while(flag){
            buy();
        }
    }

    private void buy(){
        if(ticketNums <= 0){
            flag = false;
            return;
        }
        //模拟延时
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 买票
        System.out.println(Thread.currentThread().getName() + "拿到" + ticketNums--);
    }
}
```

##### **同步方法**

- 由于我们可以通过private关键字来保证数据对象只能被方法访问，所以我们只需要针对方法提出一套机制，这套机制就是synchronized关键字，它包括两种用法：synchronized方法和synchronized块

  > 同步方法：public synchronized void method(int args){}

- synchronized方法控制对象的访问，每个对象对应一把锁，每个synchronized方法都必须获得调用该方法的对象的锁才能执行，否则线程会阻塞，方法一旦执行，就独占该锁，知道该方法返回才释放锁，后面被阻塞的线程才能获得这个锁，继续执行。

  > 缺陷：若将一个大的方法申明为synchronized将会影响效率

  ```java
  // 安全的买票
  public class SafeBuyTicket {
      public static void main(String[] args) {
          BuyTicket buyTicket = new BuyTicket();
  
          new Thread(buyTicket,"我").start();
          new Thread(buyTicket,"你").start();
          new Thread(buyTicket,"黄牛").start();
      }
  }
  
  
  class BuyTicket implements Runnable {
      // 票数
      private int ticketNums = 10;
      // 外部停止方法
      boolean flag = true;
  
      @Override
      public void run() {
          // 买票
          while(flag){
              buy();
          }
      }
  
      // synchronized 同步方法，锁的是this
      private synchronized void buy(){
          if(ticketNums <= 0){
              flag = false;
              return;
          }
          //模拟延时
          try {
              Thread.sleep(100);
          } catch (InterruptedException e) {
              e.printStackTrace();
          }
          // 买票
          System.out.println(Thread.currentThread().getName() + "拿到" + ticketNums--);
      }
  }
  ```

##### **同步块**

![image-20200504112115390](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200504112115390.png)

#### 线程锁

##### 死锁

多个线程各自占有一些共享资源，并且互相等待其他线程占有的资源才能运行，而导致两个或多个线程都在等待对方释放资源，都停止执行的情形，某一个同步块同时拥有`两个以上对象的锁`时，就可能会发生“死锁”的问题。

下面是一个死锁的例子：

```java
// 死锁：多个线程相互抱着对方需要的资源，然后形成僵持
public class DeadLock {
    public static void main(String[] args) {
        Makeup makeup1 = new Makeup(0,"灰姑娘");
        Makeup makeup2 = new Makeup(1,"白姑娘");

        makeup1.start();
        makeup2.start();
    }
}


//口红
class Lipstick{
}

//镜子
class Mirror{
}

class Makeup extends Thread{
    //需要的资源只有一份，用static来保证只有一份
    static Lipstick lipstick = new Lipstick();
    static Mirror mirror = new Mirror();

    int choice; //选择
    String girlName; //使用化妆品的人

    Makeup(int choice, String girlName){
        this.choice = choice;
        this.girlName = girlName;
    }

    @Override
    public void run(){
        makeup();
    }

    // 化妆, 互相持有对方的锁，就是需要拿到对方的资源
    private void makeup(){
        if(choice == 0){
            synchronized (lipstick){
                System.out.println(this.girlName + "获得口红的锁");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                // 1秒后获得镜子的锁
                synchronized (mirror){
                    System.out.println(this.girlName + "获得镜子的锁");
                }
            }
        }else{
            synchronized (mirror){
                System.out.println(this.girlName + "获得镜子的锁");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                // 1秒后获得口红的锁
                synchronized (lipstick){
                    System.out.println(this.girlName + "获得口红的锁");
                }
            }
        }
    }
}
```

解决方法：

```java
private void makeup(){
    if(choice == 0){
        synchronized (lipstick){
            System.out.println(this.girlName + "获得口红的锁");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // 放到外面
        synchronized (mirror){
            System.out.println(this.girlName + "获得镜子的锁");
        }
    }else{
        synchronized (mirror){
            System.out.println(this.girlName + "获得镜子的锁");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // 放到外面
        synchronized (lipstick){
            System.out.println(this.girlName + "获得口红的锁");
        }
    }
}
```

##### 死锁避免方法

产生死锁的四个必要条件：

1. 互斥条件：一个资源每次只能被一个进程使用
2. 请求与保持条件：一个进程因请求资源而阻塞时，对以获得的资源保持不放。
3. 不剥夺条件：进程已获得的资源，在未使用完之前，不能强行剥夺。
4. 循环等待条件：若干进程之间形成一种头尾想接的循环等待资源关系

##### Lock（锁）

```java
// 测试Lock锁
public class TestLock {
    public static void main(String[] args) {
        TestLock2 testLock2 = new TestLock2();
        new Thread(testLock2).start();
        new Thread(testLock2).start();
        new Thread(testLock2).start();
    }
}

class TestLock2 implements Runnable{
    int ticketNums = 10;
    // 定义lock锁
    private final ReentrantLock lock = new ReentrantLock();
    @Override
    public void run() {
        while(true){
            try{
                lock.lock();// 加锁
                if(ticketNums>0){
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(ticketNums--);
                }else{
                    break;
                }
            }finally{
                lock.unlock();// 解锁
            }
        }
    }
```

##### synchronized与Lock的对比

![image-20200504223039879](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200504223039879.png)

##### 管程法

```java
// 测试生产者和消费者模型--》利用缓冲区解决：管程法
public class TestPC {
    public static void main(String[] args) {
        SynContainer container = new SynContainer();
        new Productor(container).start();
        new Consummer(container).start();
    }
}

// 生产者
class Productor extends Thread{
    SynContainer container;
    public Productor(SynContainer container){
        this.container = container;
    }

    // 生产
    @Override
    public void run(){
        for (int i = 0; i < 100; i++) {
            container.push(new Chicken(i));
            System.out.println("生成了" + i + "只鸡");
        }
    }
}

// 消费者
class Consummer extends Thread{
    SynContainer container;
    public Consummer(SynContainer container){
        this.container = container;
    }

    // 消费
    @Override
    public void run(){
        for (int i = 0; i < 100; i++) {
            System.out.println("消费了--》" + container.pop().id + "只鸡");
        }
    }
}

// 产品
class Chicken{
    int id;

    public Chicken(int id) {
        this.id = id;
    }
}

// 缓冲区
class SynContainer{
    // 需要一个容器大小
    Chicken[] chickens = new Chicken[10];
    // 容器计数
    int count = 0;

    // 生产者放入产品
    public synchronized void push(Chicken chicken){
        // 如果容器满了，就需要等待消费者消费
        if(count == chickens.length){
            // 通知消费者消费、生成等待
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // 如果容器没有慢，就需要丢入产品
        chickens[count] = chicken;
        count++;

        // 可以通知消费者消费了
        this.notifyAll();
    }

    // 消费者消费产品
    public synchronized Chicken pop(){
        // 判断能否消费
        if(count == 0){
            // 等待生产者生产，消费者等待
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // 如果可以消费
        count--;
        Chicken chicken = chickens[count];

        // 吃完了，通知生产者生成
        this.notifyAll();
        return chicken;
    }
}
```

##### 信号灯法

```java
// 测试生产者消费者问题2：信号灯法，标志位解决
public class TestPC2 {
    public static void main(String[] args) {
        TV tv = new TV();

        new Player(tv).start();
        new Watcher(tv).start();
    }
}

// 生产者--》演员
class Player  extends Thread{
    TV tv;
    public Player(TV tv){
        this.tv = tv;
    }

    @Override
    public void run(){
        for (int i = 0; i < 20; i++) {
            if(i%2==0){
                this.tv.play("快乐大本营播放中");
            }else{
                this.tv.play("抖音");
            }
        }
    }
}

// 消费者--》观众
class Watcher extends Thread{
    TV tv;
    public Watcher(TV tv){
        this.tv = tv;
    }

    @Override
    public void run(){
        for (int i = 0; i < 20; i++) {
            tv.watch();
        }
    }
}

// 产品--》节目
class TV{
    // 演员表演，观众等待
    // 观众观看，演员等待
    String voice; // 表演的节目
    boolean flag = true;

    // 表演
    public synchronized void play(String voice){
        if(!flag){
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("演员表演了：" + voice);
        this.notifyAll();// 通知唤醒
        this.voice = voice;
        this.flag = !this.flag;
    }

    // 观看
    public synchronized void watch(){
        if(flag){
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("观众观看了：" + voice);

        // 通知演员表演
        this.notifyAll();
        this.flag = !this.flag;
    }
}
```

### 线程池

![image-20200505220950354](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200505220950354.png)

![image-20200505221057472](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200505221057472.png)

```java
// 测试线程池
public class TestPool {
    public static void main(String[] args) {
        // 1.创建服务，创建线程池
        ExecutorService service = Executors.newFixedThreadPool(10);


        // 执行
        service.execute(new MyThread());
        service.execute(new MyThread());
        service.execute(new MyThread());
        service.execute(new MyThread());

        // 2.关闭连接
        service.shutdown();
    }
}

class MyThread implements Runnable{

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
    }
}
```

### 静态代理模式

```java
// 静态代理模式：
//    真实对象和代理对象都要实现同一个接口
//    代理对象要代理真实角色
// 好处：
//    代理对象可以做很多真实对象做不了的事情
//    代理对象专注做自己的事情
public class StaticProxy {
    public static void main(String[] args) {
        WeddingCompany weddingCompany = new WeddingCompany(new You());
        weddingCompany.HappyMarry();
    }
}

interface Marry{
    void HappyMarry();
}

// 真实角色
class You implements Marry{
    @Override
    public void HappyMarry(){
        System.out.println("我要结婚了，很开心");
    }
}

// 代理角色
class WeddingCompany implements Marry{
    private Marry target;

    public WeddingCompany(Marry target) {
        this.target = target;
    }

    @Override
    public void HappyMarry(){
        before();
        this.target.HappyMarry(); // 代理真实角色
        after();
    }

    private void after() {
        System.out.println("结婚之后，收尾款");
    }

    private void before() {
        System.out.println("结婚之前，布置现场");
    }
}
```

### Lambda表达式

```java
public class TestLambda1 {
    // 3.静态内部类
    static class Like2 implements ILike{
        @Override
        public void lambda(){
            System.out.println("I like lambda2");
        }
    }

    public static void main(String[] args) {
        ILike iLike = new Like();
        iLike.lambda();

        iLike = new Like2();
        iLike.lambda();

        // 4.局部内部类
        class Like3 implements ILike{
            @Override
            public void lambda(){
                System.out.println("I like lambda3");
            }
        }

        iLike = new Like3();
        iLike.lambda();

        // 5.匿名内部类，没有类的名称。必须借助接口或者父类
        iLike = new ILike() {
            @Override
            public void lambda() {
                System.out.println("I like lambda4");
            }
        };
        iLike.lambda();

        // 6.用lambda简化
        iLike = () -> {
            System.out.println("I like lambda5");
        };
        iLike.lambda();
    }
}

// 1.定义一个函数式接口
interface ILike{
    void lambda();
}

// 2.实现类
class Like implements ILike{
    @Override
    public void lambda(){
        System.out.println("I like lambda");
    }
}
```

## POI和EasyExcel

EasyExcel官网地址：https://alibaba-easyexcel.github.io/index.html

![image-20200509172411668](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200509172411668.png)

## 并行程序开发及优化

### 并行程序设计模式

现在我们想要炒一道菜，但是我们没有厨具和菜，现在我们从网上订购了一套厨具，但在厨具送来的期间，我们不必一直等到厨具到来，而是可以先去买菜，然后厨具到了之后直接开始炒菜

这就是Future模式，在程序设计中，当某一段程序提交了一个请求，期望得到一个答复。但非常不幸的是，服务程序对这个请求的处理可能很慢，比如这个请求可能是通过互联网、HTTP或者Web Service等并不太高效的方式调用的。**在传统的单线程模式下，调用函数是同步的，也就是说他必须等到服务程序返回结果后，才能进行其他处理**。而在Future模式下，调用方式改为异步，而原先等待返回的时间段，在主调用函数中，则可用于处理其他事务

#### 传统模式与Future模式流程对比

![在这里插入图片描述](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/20191130142619323.jpg)

![在这里插入图片描述](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/20191130142626449.jpg)

#### Future模式的代码实现

![在这里插入图片描述](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/20191130143413564.jpg)

| 参与者     | 作用                                                         |
| ---------- | ------------------------------------------------------------ |
| Main       | 系统启动，调用Client发送请求，并使用返回数据                 |
| Client     | 返回Data对象，立即返回FutureData，并开启ClientThread线程装配RealData |
| Data       | 返回数据的接口                                               |
| FutureData | Future数据，构造很快，但是是一个虚拟数据，需要装配RealData   |
| RealData   | 真实数据，构造比较缓慢                                       |

```java
package parallelModel;

/**
 * @ClassName FutureTest
 * @Description TODO 并行程序设计模式之Future模式，类似于商品订单
 * @Author wxl
 * @Date 2020-08-09 21:57
 * @Version 1.0
 */
public class FutureTest {

    // 调用client发起请求
    public static void main(String[] args) {
        Client client = new Client();
        // 这里会立刻返回，因为得到的是FutureData而不是RealData
        Data data = client.request("name");
        System.out.println("请求完毕");
        try{
            // 这里可以用一个sleep代替对其他业务逻辑的处理
            // 在处理这些业务的过程中，RealData被创建，从而充分利用了等待时间
            Thread.sleep(2000);
        } catch (InterruptedException e){
            e.printStackTrace();
        }

        // 获取真实的数据
        System.out.println("数据 = " + data.getResult());

    }

    /**
     * 获取FutureData
     */
    public static class Client {
        public Data request(final String queryStr){
            FutureData futureData = new FutureData();

            // RealData的构造很慢，所以在单独的线程中进行
            new Thread(){
                public void run(){
                    RealData realData = new RealData(queryStr);
                    futureData.setRealData(realData);
                }
            }.start();

            return futureData;  // FutureData会立刻返回
        }
    }

    /**
     * 一个Data接口，提供getResult()方法
     */
    public interface Data{
        public String getResult();
    }

    /**
     * FutureData是Future模式的关键， 是真实数据RealData的代理，封装了获取RealData的等待过程
     */
    public static class FutureData implements Data {
        protected RealData realData = null;     // FutureData是RealData的包装
        protected boolean isReady = false;
        public synchronized void setRealData(RealData realData){
            if(isReady){
                return;
            }

            this.realData = realData;
            isReady = true;
            notifyAll();        // RealData已被注入，通知getResult()
        }

        @Override
        public synchronized String getResult() {        // 会等待RealData构造完成
            while( !isReady ){
                try{
                    wait();     // 一直等待，知道RealData被注入
                } catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
            return realData.result;
        }
    }

    /**
     * RealData是最终需要使用的数据模型，它的构造很慢，使用sleep()函数模拟
     */
    public static class RealData implements Data {
        protected final String result;

        public RealData(String para) {
            // RealData的构造可能很慢，需要用户等待
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < 10; i++) {
                sb.append(para);
                try {
                    // 这里使用一个sleep模拟一个很慢的操作过程
                    Thread.sleep(100);
                } catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
            this.result = sb.toString();
        }


        @Override
        public String getResult() {
            return this.result;
        }
    }
}
```

#### 扩展-JDK的内置实现

由于Future模式的常用性，以至于JDK的并发包中就已经内置了一种Future模式的实现。JDK中的实现是相当复杂的，并提供了更为丰富的线程控制功能，但其中的基本用意和核心概念是完全一致的



### POI-Excel

#### 引入依赖

```xml
        <!--xls(03)-->
        <dependency>
            <groupId>org.apache.poi</groupId>
            <artifactId>poi</artifactId>
            <version>3.9</version>
        </dependency>
        <!--xls(07)-->
        <dependency>
            <groupId>org.apache.poi</groupId>
            <artifactId>poi-ooxml</artifactId>
            <version>3.9</version>
        </dependency>
        <!--日期格式化工具-->
        <dependency>
            <groupId>joda-time</groupId>
            <artifactId>joda-time</artifactId>
            <version>2.10.1</version>
        </dependency>
        <!--日期格式化工具-->
        <dependency>
            <groupId>joda-time</groupId>
            <artifactId>joda-time</artifactId>
            <version>2.10.1</version>
        </dependency>
```

#### 写excel

##### 写2003和2007的差别

|      | 最大支持行数 | POM依赖                      | 工作薄创建方式      | 文件名后缀 |
| ---- | ------------ | ---------------------------- | ------------------- | ---------- |
| 2003 | 65535        | <artifactId>poi</artifactId> | new HSSFWorkbook(); | xls        |
| 2007 | 1048576      | <artifactId>poi-             | new XSSFWorkbook(); | xlsx       |

##### 代码

```java
public class PoiExcelWrite {
    String PATH = "E:\\project\\javastudy\\logs";
    public void testWrite03(){

        // 1、创建一个工作薄
        Workbook workbook = new HSSFWorkbook();
        // 2、创建一个工作表
        Sheet sheet = workbook.createSheet("表1");
        // 3、创建一个行 (1,1)
        Row row1 = sheet.createRow(0);
        // 4、创建一个单元格
        Cell cell11 = row1.createCell(0);
        // 5、写入值
        cell11.setCellValue("注册成功率%");

        Cell cell12 = row1.createCell(1);
        cell12.setCellValue(97.34);

        Row row2 = sheet.createRow(1);
        Cell cell21 = row2.createCell(0);
        cell21.setCellValue("时间");
        Cell cell22 = row2.createCell(1);
        String time = new DateTime().toString("yyyy-MM-dd HH:mm:ss");
        cell22.setCellValue(time);

        try {
            // 生成一张表（IO流）  03版本就是使用xls结尾
            FileOutputStream fileOutputStream = new FileOutputStream(PATH + "\\导出_注册03.xls");
            workbook.write(fileOutputStream);
            fileOutputStream.close();
            System.out.println("输出03表格完成！");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void testWrite07(){

        // 1、创建一个工作薄
        Workbook workbook = new XSSFWorkbook();
        // 2、创建一个工作表
        Sheet sheet = workbook.createSheet("表1");
        // 3、创建一个行 (1,1)
        Row row1 = sheet.createRow(0);
        // 4、创建一个单元格
        Cell cell11 = row1.createCell(0);
        // 5、写入值
        cell11.setCellValue("注册成功率%");

        Cell cell12 = row1.createCell(1);
        cell12.setCellValue(97.34);

        Row row2 = sheet.createRow(1);
        Cell cell21 = row2.createCell(0);
        cell21.setCellValue("时间");
        Cell cell22 = row2.createCell(1);
        String time = new DateTime().toString("yyyy-MM-dd HH:mm:ss");
        cell22.setCellValue(time);

        try {
            // 生成一张表（IO流）  03版本就是使用xlsx结尾
            FileOutputStream fileOutputStream = new FileOutputStream(PATH + "\\导出_注册07.xlsx");
            workbook.write(fileOutputStream);
            fileOutputStream.close();
            System.out.println("输出07表格完成！");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
  
    public void testWrite07BigDataS(){
        long begin = System.currentTimeMillis();
        // 1、创建一个工作薄
        Workbook workbook = new SXSSFWorkbook();
        // 2、创建一个工作表
        Sheet sheet = workbook.createSheet("表1");
        // 写入数据
        for (int i = 0; i < 65536; i++) {
            // 3、创建一个行 (1,1)
            Row row1 = sheet.createRow(i);
            for (int j = 0; j < 10; j++) {
                // 4、创建一个单元格
                Cell cell11 = row1.createCell(j);
                // 5、写入值
                cell11.setCellValue(i + "_" + j);
            }
        }
        System.out.println("over");
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(PATH + "\\testWrite07BigDataS.xlsx");
            workbook.write(fileOutputStream);
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println((double) (end - begin)/1000);
    }
}
```

##### 批量写入对比

|       | 缺点                                              | 优点                                         |
| ----- | ------------------------------------------------- | -------------------------------------------- |
| HSSF  | 最多只能写入65536行，否则抛出异常                 | 写入缓存，不操作磁盘，最后一次性写入，速度快 |
| XSSF  | 写入速度非常慢，耗内存，会出现内存溢出，如100万条 | 可以写较大的数据量                           |
| SXSSF |                                                   | 可以写入非常大的量，写入速度快，占用更少内存 |

#### 读excel

##### 代码

```java
public class PoiExcelRead {
    String PATH = "E:\\project\\javastudy\\logs";
    public void testRead03(){
        try {
            // 1、获取文件流
            FileInputStream fileInputStream = new FileInputStream(PATH + "\\导出_注册03.xls");
            // 2、创建一个工作薄
            Workbook workbook = new HSSFWorkbook(fileInputStream);
            for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
                // 3、得到表
                Sheet sheet = workbook.getSheetAt(i);
                System.out.println(sheet.getSheetName());
                for (int r = 0; r < sheet.getPhysicalNumberOfRows(); r++) {
                    // 4、得到行
                    Row row = sheet.getRow(r);
                    for (int c = 0; c < row.getPhysicalNumberOfCells(); c++) {
                        // 5、得到列
                        Cell cell = row.getCell(c);
                        if(cell != null){
                            int type = cell.getCellType();
                            String value = null;
                            // 根据不同的类型读取
                            switch (type){
                                case HSSFCell.CELL_TYPE_NUMERIC:    // 数字（日期、普通数字）
                                    if(HSSFDateUtil.isCellDateFormatted(cell)){
                                        Date date = cell.getDateCellValue();
                                        value = new DateTime(date).toString("yyyy-MM-dd HH:mm:ss");
                                    }else{
                                        // 防止数字过长，转为字符串
                                        cell.setCellType(HSSFCell.CELL_TYPE_STRING);
                                        value = cell.getStringCellValue();
                                    }
                                    break;
                                case HSSFCell.CELL_TYPE_STRING:     // 字符串
                                    value = cell.getStringCellValue();
                                    break;
                                case HSSFCell.CELL_TYPE_FORMULA:    // 公式
                                    value = String.valueOf(cell.getCellFormula());
                                    break;
                                case HSSFCell.CELL_TYPE_BLANK:      // 空
                                    value = "";
                                    break;
                                case HSSFCell.CELL_TYPE_BOOLEAN:    // 布尔
                                    value = String.valueOf(cell.getBooleanCellValue());
                                    break;
                                case HSSFCell.CELL_TYPE_ERROR:      // 错误
                                    value = String.valueOf(cell.getErrorCellValue());
                                    break;
                                default:
                            }
                            System.out.print(value + "|");
                        }
                    }
                    System.out.println();
                }
            }
            fileInputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
```

##### 读取计算公式

```java
public void testFormula(){
    try {
        FileInputStream fileInputStream = new FileInputStream(PATH + "\\testFormula.xls");
        Workbook workbook = new HSSFWorkbook(fileInputStream);
        // 1、拿到有公式的cell
        Sheet sheet = workbook.getSheetAt(0);
        Row row = sheet.getRow(4);
        Cell cell = row.getCell(0);

        // 2、新建计算公式装置
        FormulaEvaluator formulaEvaluator = new HSSFFormulaEvaluator((HSSFWorkbook) workbook);

        int type = cell.getCellType();
        if(type == HSSFCell.CELL_TYPE_FORMULA){
            // 拿到公式
            String formula = cell.getCellFormula();
            System.out.println(formula);

            // 计算
            CellValue cellValue = formulaEvaluator.evaluate(cell);
            String vlaue = cellValue.formatAsString();
            System.out.println(vlaue);
        }

        fileInputStream.close();
    } catch (FileNotFoundException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }
}
```

### EasyExcel操作

Java解析、生成Excel比较有名的框架有Apache poi、jxl。但他们都存在一个严重的问题就是非常的耗内存，poi有一套SAX模式的API可以一定程度的解决一些内存溢出的问题，但POI还是有一些缺陷，比如07版Excel解压缩以及解压后存储都是在内存中完成的，内存消耗依然很大。easyexcel重写了poi对07版Excel的解析，能够原本一个3M的excel用POI sax依然需要100M左右内存降低到几M，并且再大的excel不会出现内存溢出，03版依赖POI的sax模式。在上层做了模型转换的封装，让使用者更加简单方便

> 官网https://www.yuque.com/easyexcel/doc/easyexcel

#### 引入依赖

```xml
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>easyexcel</artifactId>
            <version>2.2.3</version>
        </dependency>

				<!--日期格式化工具-->
        <dependency>
            <groupId>joda-time</groupId>
            <artifactId>joda-time</artifactId>
            <version>2.10.1</version>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>1.7.21</version>
        </dependency>

        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
            <version>1.2.4</version>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.16.12</version>
            <scope>provided</scope>
        </dependency>
```

#### 写excel

1、创建一个实体类

```java
@Data
public class DemoData {
    @ExcelProperty("字符串标题")
    private String string;
    @ExcelProperty("日期标题")
    private Date date;
    @ExcelProperty("数字标题")
    private Double doubleData;
    /**
     * 忽略这个字段
     */
    @ExcelIgnore
    private String ignore;
}
```

2、测试数据

```java
private List<DemoData> data() {
    List<DemoData> list = new ArrayList<DemoData>();
    for (int i = 0; i < 10; i++) {
        DemoData data = new DemoData();
        data.setString("字符串" + i);
        data.setDate(new Date());
        data.setDoubleData(0.56);
        list.add(data);
    }
    return list;
}
```

3、根据list数据写入excel

```java
public void simpleWrite() {
    // 写法1
    String fileName = PATH + "\\simpleWrite.xlsx";
    // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为模板 然后文件流会自动关闭
    // 如果这里想使用03 则 传入excelType参数即可
    EasyExcel.write(fileName, DemoData.class).sheet("模板").doWrite(data());
}
```

![image-20200524213814823](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200524213814823.png)

#### 读excel

1、对象

```java
@Data
public class DemoData {
    private String string;
    private Date date;
    private Double doubleData;
}
```

2、监听器

```java
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

// 有个很重要的点 DemoDataListener 不能被spring管理，要每次读取excel都要new,然后里面用到spring可以构造方法传进去
public class DemoDataListener extends AnalysisEventListener<DemoData> {
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoDataListener.class);
    /**
     * 每隔5条存储数据库，实际使用中可以3000条，然后清理list ，方便内存回收
     */
    private static final int BATCH_COUNT = 5;
    List<DemoData> list = new ArrayList<DemoData>();
    /**
     * 假设这个是一个DAO，当然有业务逻辑这个也可以是一个service。当然如果不用存储这个对象没用。
     */
    private DemoDAO demoDAO;
    public DemoDataListener() {
        // 这里是demo，所以随便new一个。实际使用如果到了spring,请使用下面的有参构造函数
        demoDAO = new DemoDAO();
    }
    /**
     * 如果使用了spring,请使用这个构造方法。每次创建Listener的时候需要把spring管理的类传进来
     *
     * @param demoDAO
     */
    public DemoDataListener(DemoDAO demoDAO) {
        this.demoDAO = demoDAO;
    }
    /**
     * 这个每一条数据解析都会来调用
     *
     * @param data
     *            one row value. Is is same as {@link AnalysisContext#readRowHolder()}
     * @param context
     */
    @Override
    public void invoke(DemoData data, AnalysisContext context) {
        System.out.println(JSON.toJSONString(data));
        //LOGGER.info("解析到一条数据:{}", JSON.toJSONString(data));
        list.add(data);
        // 达到BATCH_COUNT了，需要去存储一次数据库，防止数据几万条数据在内存，容易OOM
        if (list.size() >= BATCH_COUNT) {
            saveData();
            // 存储完成清理 list
            list.clear();
        }
    }
    /**
     * 所有数据解析完成了 都会来调用
     *
     * @param context
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        // 这里也要保存数据，确保最后遗留的数据也存储到数据库
        saveData();
        LOGGER.info("所有数据解析完成！");
    }
    /**
     * 加上存储数据库
     */
    private void saveData() {
        LOGGER.info("{}条数据，开始存储数据库！", list.size());
        demoDAO.save(list);
        LOGGER.info("存储数据库成功！");
    }
}
```

3、持久层

```java
/**
 * 假设这个是你的DAO存储。当然还要这个类让spring管理，当然你不用需要存储，也不需要这个类。
 **/
public class DemoDAO {
    public void save(List<DemoData> list) {
        // 持久化操作
        // 如果是mybatis,尽量别直接调用多次insert,自己写一个mapper里面新增一个方法batchInsert,所有数据一次性插入
    }
}
```

4、代码

```java
public class EasyExcelRead {
    private String PATH = "E:\\project\\javastudy\\logs";

    /**
     * 最简单的读
     * <p>1. 创建excel对应的实体对象 参照{@link DemoData}
     * <p>2. 由于默认一行行的读取excel，所以需要创建excel一行一行的回调监听器，参照{@link DemoDataListener}
     * <p>3. 直接读即可
     */
    public void simpleRead() {
        // 有个很重要的点 DemoDataListener 不能被spring管理，要每次读取excel都要new,然后里面用到spring可以构造方法传进去
        // 写法1：
        String fileName = PATH + "\\simpleWrite.xlsx";
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet 文件流会自动关闭
        EasyExcel.read(fileName, DemoData.class, new DemoDataListener()).sheet().doRead();
    }
}
```

## 注解

### 什么是注解？

- Annotation是从JDK5.0开始引入的新技术

- Annotation的作用：

  - 不是程序本身，可以对程序做出解释
  - `可以被其他程序（比如：编译器等）读取`

- Annotation的格式：

  注解是以“@注解名”在代码中存在的，还可以添加一些参数值，例如：@SuppressWarnings(value="unchecked)

- Annotation在哪里使用?

  可以附加在package，class，method，field等上面，相当于给他们添加了额外的辅助信息，我们可以通过反射机制编程实现对这些元数据的访问。

  

### 内置注解

- @Override：定义在java.lang.Override中，此注释只适用于修辞方法，表示一个方法声明打算重写超类中的另一个方法声明
- @Deprecated：定义在java.long.Deprecated中，此注释可以用于修辞方法，属性、类、表示不建议使用这个的元素，通常是因为它很危险或者存在更好的选择
- @SuppressWarnings：定义在java.lang.SuppressWarnings中，用来抑制编译时的告警信息
  - 与前两个注释有所不同，你需要添加一个参数才能正确使用，这些参数都是已经定义好了的，我们选择性的使用就好了
    - @SuppressWarnings("all")
    - @SuppressWarnings("unchecked")
    - @SuppressWarnings(value={"unchecked","deprecation"})
    - 等等....

### 元注解

- 元注解的作用就是负责注解其他注解，Java定义了4个标准的meta-annotation类型，他们被用来提供对其它annotation类型作说明
- 这些类型和他们所支持的类在java.lang.annotation包中可以找到
  - @Target：用于描述注解的作用范围（即：被描述的注解可以用在什么地方）
  - @Retention：表示需要在什么级别保存该注释信息，用于描述注解的生命周期（SOURCE<CLASS<RUNTIME）
  - @Document：说明该注解将被包含在javadoc中
  - @Inherited：说明子类可以继承父类中的该注解

### 自定义注解

- 使用@interface 自定义注解时，自动继承了java.lang.Annotation接口

```java
// 自定义注解
public class Test03 {
    // 如果没有默认值，我们就必须给注解赋值
    @MyAnn(name="wxl", schools={"zhenwanzhong", "xianzhigaozhong"})
    public void test(){}

    // 如果只有一个参数且是value，可以直接写值
    @MyAnn1("wxl")
    public void test1(){}
}

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@interface MyAnn{
    // 这不是方法，是注解的参数：参数类型 + 参数名（）
    String name();
    int age() default 0;
    int id() default -1;  // 如果默认值为-1，表示不存在

    String[] schools();
}

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@interface MyAnn1 {
    String value();
}
```

## 反射机制

### 静态 VS 动态语言

**动态语言**

- 是一类在运行时可以改变其结构的语言：例如新的函数、对象、甚至代码可以被引进，已有的函数可以被删除或是其它结构上的变化。通俗点说就是在运行时代码可以根据某些条件改变自身结构。
- 主要动态语言：Object-C、C#、JavaScript、PHP、Python等

静态语言

- 与动态语言相对应的，运行时结构不可变的语言就是静态语言，如Java、C、C++.
- Java不是动态语言，但Java可以称为“准动态语言”。即Java有一定的动态性，我们可以利用反射机制获得类似动态语言的特性。Java的动态性让编程的时候更加灵活

### Reflection（反射）

- 是java被视为动态语言的关键，反射机制允许程序在执行期借助于Reflection API取得任何类的内部信息，并能直接操作任意对象的内部属性及方法。

  > Class c = Class.forName("java.lang.String")

- 加载完类之后，在堆内存的方法区中就产生了一个Class类型的对象（一个类只有一个Class对象），这个对象就包含了完整的类的结构信息。我们可以通过这个对象看到类的结构。这个对象就像一面镜子，透过这个镜子看到类的结构，所以我们形象的称为：反射。

正常方式：

```mermaid
graph LR
    A[引入需要的'包类'名称] --> B[通过new实例化] --> C[取得实例化对象]
```

反射方式：

```mermaid
graph LR
    A[实例化对象] --> B["getClass()方法"] --> C[得到完整的'包类'名称]
```

### Java反射机制的功能

- 在运行时判断任意一个对象所属的类
- 在运行时构造任意一个类的对象
- 在运行时判断任意一个类所具有的成员变量和方法
- 在运行时获取泛型信息
- 在运行时调用任意一个对象的成员变量和方法
- 在运行时处理注解
- 生成动态代理
- ......

### Java反射机制的优点和缺点

**优点：**

可以实现动态创建对象和编译，体现出很大的灵活性

**缺点：**

对性能有影响，使用反射基本上是一种解释操作，我们可以告诉JVM，我们希望做什么并且它满足我们的要求。这类操作总是慢于直接执行相同的操作。

### 反射的主要API

- java.lang.Class :代表一个类
- java.lang.reflect.Method :代表类的方法
- java.lang.reflect.Field :代表类的成员变量
- java.lang.reflect.Constructor :代表类的构造器

```java
// 什么是反射
public class Test01 {
    public static void main(String[] args) throws ClassNotFoundException {
        // 通过反射获取类的class对象
        Class c1 = Class.forName("com.javastudy.java.reflection.User");
        System.out.println(c1);

        Class c2 = Class.forName("com.javastudy.java.reflection.User");
        Class c3 = Class.forName("com.javastudy.java.reflection.User");

        // 一个类在内存中只有一个Class对象
        // 一个类被加载后，类的整个结构都会封装在Class中
        System.out.println(c1.hashCode());
        System.out.println(c2.hashCode());
        System.out.println(c3.hashCode());
    }
}


// 实体类
class User{
    private String name;

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
```

### Class类

![image-20200511174757566](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200511174757566.png)

![image-20200511174901347](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200511174901347.png)

### Class类的常用方法

![image-20200511174607188](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200511174607188.png)

### 获取Class类的实例

![image-20200511174959810](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200511174959810.png)

```java
// 测试Class类的创建方式有哪些
public class Test03 {
    public static void main(String[] args) throws ClassNotFoundException {
        Person person = new Student();
        System.out.println("这个人是：" + person.name);

        // 方式一： 通过对象获得
        Class c1 = person.getClass();
        System.out.println(c1.hashCode());

        // 方式二：forName获得
        Class c2 = Class.forName("com.javastudy.java.reflection.Student");
        System.out.println(c2.hashCode());

        // 方式三：通过类名.class获得
        Class c3 = Student.class;
        System.out.println(c3.hashCode());

        // 方式四： 基本内置类型的包装类都有一个.Type属性
        Class c4 = Integer.TYPE;

        // 获取父类类型
        Class c5 = c1.getSuperclass();
        System.out.println(c5);
    }
}

class Person{
    public String name;
    public Person(){};
    public Person(String name){
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }
}

class Student extends Person{
    public Student(){
        this.name = "学生";
    }
}

class Teacher extends Person{
    public Teacher(){
        this.name = "老师";
    }
}
```

### 哪些类有Class对象

- class：外部类，成员（成员内部类、静态内部类），局部内部类，匿名内部类
- interface：接口
- []：数组
- enum：枚举
- annotation：注解@interface
- primitive type：基本数据类型
- void

```java
// 所有类型的Class
public class Test04 {
    public static void main(String[] args) {
        Class c1 = Object.class;
        Class c2 = Comparable.class;
        Class c3 = String[].class;
        Class c4 = int[][].class;
        Class c5 = Override.class;  // 注解
        Class c6 = ElementType.class; // 枚举
        Class c7 = Integer.class;   // 基本数据类型
        Class c8 = void.class;
        Class c9 = Class.class;

        int[] a = new int[10];
        int[] b = new int[100];
        // 只要元素类型与维度一样，就是同一个Class
        System.out.println(a.getClass().hashCode());
        System.out.println(b.getClass().hashCode());
    }
}
```

### Class对象能做什么？

- 创建类的对象：调用Class对象的newInstance()方法
  - 1、类必须有一个无参的构造器
  - 2、类的构造器的访问权限需要足够

```java
// 动态的创建对象，通过反射
public class Test06 {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException, NoSuchFieldException {
        Class c1 = Class.forName("com.javastudy.java.reflection.User");

        // 构造一个对象
        //User user =(User) c1.newInstance();  // 本质上是调用了类的无参构造器
        //System.out.println(user);

        // 通过构造器创建对象
        Constructor constructor = c1.getDeclaredConstructor(String.class, int.class);
        User user2 = (User) constructor.newInstance("wxl", 18);
        System.out.println(user2);

        // 通过反射调用普通方法
        User user3 = (User) c1.newInstance();
        // 通过反射获取方法
        Method sn = c1.getDeclaredMethod("setName", String.class);
        sn.invoke(user3, "lyp");
        System.out.println(user3.getName());

        // 通过反射操作属性
        User user4 =(User) c1.newInstance();
        Field name = c1.getDeclaredField("name");
        // 不能直接操作私有属性，我们需要关闭程序的安全检测，属性或者方法的setAccessible(true)
        name.setAccessible(true);
        name.set(user4, "cc");
        System.out.println(user4.getName());
    }
}
```



## 加解密

### AES加解密

#### 后端

```java

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

import sun.misc.BASE64Decoder;

/**
 * AES的加密和解密
 * @author libo
 */
public class Aes {
    private static final String KEY_ALGORITHM = "AES";
    private static final String DEFAULT_CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";//默认的加密算法

    /**
     * AES 加密操作
     *
     * @param content 待加密内容
     * @param key 加密密码
     * @return 返回Base64转码后的加密数据
     */
    public static String encode(String content, String key) {
        try {
            Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);// 创建密码器

            byte[] byteContent = content.getBytes("utf-8");

            cipher.init(Cipher.ENCRYPT_MODE, getSecretKey(key));// 初始化为加密模式的密码器

            byte[] result = cipher.doFinal(byteContent);// 加密

            return Base64.encodeBase64String(result);//通过Base64转码返回
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    /**
     * AES 解密操作
     *
     * @param content
     * @param key
     * @return
     */
    public static String decode(String content, String key) {

        try {
            //实例化
            Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);
            //使用密钥初始化，设置为解密模式
            cipher.init(Cipher.DECRYPT_MODE, getSecretKey(key));
            //执行操作
            byte[] result = cipher.doFinal(org.apache.commons.codec.binary.Base64.decodeBase64(content));
            return new String(result, "utf-8");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private static SecretKeySpec getSecretKey(final String key) throws UnsupportedEncodingException {
        //返回生成指定算法密钥生成器的 KeyGenerator 对象
        KeyGenerator kg = null;
        try {
            kg = KeyGenerator.getInstance(KEY_ALGORITHM);
            //AES 要求密钥长度为 128
            kg.init(128, new SecureRandom(key.getBytes()));
            //生成一个密钥
            SecretKey secretKey = kg.generateKey();
            return new SecretKeySpec(Arrays.copyOf(key.getBytes("utf-8"), 16), KEY_ALGORITHM);// 转换为AES专用密钥
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    public static void main(String args[]){
        // 测试DES加密
        String content = "Hu772211";
        String key = "57548230121b47b78def74821362ab52";
        Aes aes = new Aes();
        System.out.println("AES加密前：" + content);
        System.out.println("AES加密密钥和解密密钥：" + key);
        String encrypt1 = aes.encode(content, key);
        System.out.println("AES加密后：" + encrypt1);
        String decrypt1 = aes.decode(encrypt, key);
        System.out.println("AES解密后：" + decrypt1);
    }

}
```

#### 前端

```js
key='XY20190520'
 
    //如果key的长度小于16时的处理
    function fillKey(key) => {
        const filledKey = Buffer.alloc(128 / 8);
        const keys = Buffer.from(key);
        if (keys.length < filledKey.length) {
            filledKey.map((b, i) => filledKey[i] = keys[i]);
        }
        return filledKey;
    }
 
    function encrypt(word) => {
        const key = CryptoJS.enc.Utf8.parse(this.fillKey(key)); //16位
        let encrypted = '';
        if (typeof(word) == 'string') {
            let srcs = CryptoJS.enc.Utf8.parse(word);
            encrypted = CryptoJS.AES.encrypt(srcs, key, {
                mode: CryptoJS.mode.ECB,
                padding: CryptoJS.pad.Pkcs7
            });
        } else if (typeof(word) == 'object') {//对象格式的转成json字符串
            let data = JSON.stringify(word);
            let srcs = CryptoJS.enc.Utf8.parse(data);
            encrypted = CryptoJS.AES.encrypt(srcs, key, {
                mode: CryptoJS.mode.ECB,
                padding: CryptoJS.pad.Pkcs7
            })
        }
        return encrypted.toString();
    }
 
    function decrypt(word,key) =>{
        let key = CryptoJS.enc.Utf8.parse(this.fillKey(key));
        let decrypt = CryptoJS.AES.decrypt(word, key, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        });
        return decrypt.toString(CryptoJS.enc.Utf8);
    }
```



### DES加解密

#### 后端

```java
package com.nsn.util.http;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import java.security.InvalidKeyException;
import java.security.SecureRandom;

import static sun.security.x509.CertificateX509Key.KEY;

public class Des {
    /**
     * DES加密
     * @param datasource
     * @return
     */
    public static String encode(String datasource, String key){
        try{
            SecureRandom random = new SecureRandom();
            DESKeySpec desKey = new DESKeySpec(key.getBytes("utf-8"));
            //创建一个密匙工厂，然后用它把DESKeySpec转换成
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey securekey = keyFactory.generateSecret(desKey);
            //Cipher对象实际完成加密操作
            Cipher cipher = Cipher.getInstance("DES");
            //用密匙初始化Cipher对象
            cipher.init(Cipher.ENCRYPT_MODE, securekey, random);
            //现在，获取数据并加密
            byte[] temp = Base64.encodeBase64(cipher.doFinal(datasource.getBytes()));
            return IOUtils.toString(temp,"UTF-8");
        }catch(Throwable e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * DES解密
     * @return
     */
    public static String decode(String src, String key){
        try {
            // DES算法要求有一个可信任的随机数源
            SecureRandom random = new SecureRandom();
            // 创建一个DESKeySpec对象
            DESKeySpec desKey = new DESKeySpec(key.getBytes("utf-8"));
            // 创建一个密匙工厂
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            // 将DESKeySpec对象转换成SecretKey对象
            SecretKey securekey = keyFactory.generateSecret(desKey);
            // Cipher对象实际完成解密操作
            Cipher cipher = Cipher.getInstance("DES");
            // 用密匙初始化Cipher对象
            cipher.init(Cipher.DECRYPT_MODE, securekey, random);
            // 真正开始解密操作
            return IOUtils.toString(cipher.doFinal(Base64.decodeBase64(src)),"UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static void main(String args[]){
        String content = "Hu772211";
        String key = "57548230121b47b78def74821362ab52";

        // 测试DES加密
        Des des = new Des();
        System.out.println("DES加密前：" + content);
        System.out.println("DES加密密钥和解密密钥：" + key);
        String encrypt = des.encode(content, key);
        System.out.println("DES加密后：" + encrypt);
        String decrypt = des.decode(encrypt, key);
        System.out.println("DES解密后：" + decrypt);
    }

}
```

#### 前端

```js
function encryptByDES(message, key) {
    var keyHex = CryptoJS.enc.Utf8.parse(key);      
    var encrypted = CryptoJS.DES.encrypt(message, keyHex, {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    });
    return encrypted.toString();
}

function decryptByDES(ciphertext, key) {        
    var keyHex = CryptoJS.enc.Utf8.parse(key);      
    var decrypted = CryptoJS.DES.decrypt({
        ciphertext: CryptoJS.enc.Base64.parse(ciphertext)
    }, keyHex, {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    });

    return decrypted.toString(CryptoJS.enc.Utf8);
}
```



## LinkedList

1、链表介绍

链表是一种物理存储单元上非连续。非顺序的存储结构，数据元素的逻辑顺序是通过链表的指针连接次序实现的。链表由一系列节点（链表中每一个元素称为节点）组成，节点可以在运行时动态生成。每个节点包括两个部分：一个是存储数据域，另一个是存储下一个节点地址的指针域。

双链表是链表的一种，由节点组成，每个数据节点中都有两个指针，分别指向直接后继和直接前驱。

![image-20200709115914710](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200709115914710.png)

### ArrayList、LinkedList对比分析

![image-20200709152118337](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/image-20200709152118337.png)

### 尾部插入方法

```java
    public boolean add(E e) {
        linkLast(e);
        return true;
    }
    
    void linkLast(E e) {
        final Node<E> l = last;
        // 新增节点，l前驱节点，e,新创建节点，null后继节点
        final Node<E> newNode = new Node<>(l, e, null);
        // 设置新节点为尾节点
        last = newNode;
        // 判断是否存在上一个节点
        if (l == null)
        	// 不存在就指定新节点为头节点
            first = newNode;
        else
        	// 存在就指定上一个节点的后继节点为新节点
            l.next = newNode;
        size++;
        modCount++;
    }
```

### 指定游标插入

```java
    public void add(int index, E element) {
        // 监测是否0<= index <= size
        checkPositionIndex(index);
		// 判断是否是尾部位置
        if (index == size)
            // 是就在尾部插入
            linkLast(element);
        else
            // 在element后插入
            linkBefore(element, node(index));
    }

    void linkBefore(E e, Node<E> succ) {
        // assert succ != null;
        // 元节点的前驱
        final Node<E> pred = succ.prev;
        // 创建新节点
        final Node<E> newNode = new Node<>(pred, e, succ);
        succ.prev = newNode;
        if (pred == null)
            first = newNode;
        else
            pred.next = newNode;
        size++;
        modCount++;
    }
```

### 删除元素

```java
//删除某个对象
public boolean remove(Object o) {
    if (o == null) {
        for (Node<E> x = first; x != null; x = x.next) {
            if (x.item == null) {
                unlink(x);
                return true;
            }
        }
    } else {
        for (Node<E> x = first; x != null; x = x.next) {
            if (o.equals(x.item)) {
                unlink(x);
                return true;
            }
        }
    }
    return false;
}
//删除某个位置的元素
public E remove(int index) {
    checkElementIndex(index);
    return unlink(node(index));
}
//删除某节点，并将该节点的上一个节点（如果有）和下一个节点（如果有）关联起来
E unlink(Node<E> x) {
    final E element = x.item;
    final Node<E> next = x.next;
    final Node<E> prev = x.prev;

    if (prev == null) {
        first = next;
    } else {
        prev.next = next;
        x.prev = null;
    }

    if (next == null) {
        last = prev;
    } else {
        next.prev = prev;
        x.next = null;
    }

    x.item = null;
    size--;
    modCount++;
    return element;
}
```

### 遍历

普通for循环，增强for循环，Iterator迭代器

```java
/**
 * 普通for循环
 * @param list
 */
public static void listByNormalFor(LinkedList<String> list){
    long start = System.currentTimeMillis();
    for (int i = 0; i < list.size(); i++) {
        list.get(i);
    }
    long end = System.currentTimeMillis();
    System.out.println("listByNormalFor: " + (end - start));
}

/**
 * 增强for循环
 * @param list
 */
public static void listByStrengthenFor(LinkedList<String> list){
    long start = System.currentTimeMillis();
    for (String s : list ) {
    }
    long end = System.currentTimeMillis();
    System.out.println("listByStrengthenFor: " + (end - start));
}

/**
 * 增强for循环
 * @param list
 */
public static void listByIterator(LinkedList<String> list){
    long start = System.currentTimeMillis();
    for (Iterator iter = list.iterator() ; iter.hasNext();) {
        iter.next();
    }
    long end = System.currentTimeMillis();
    System.out.println("listByIterator: " + (end - start));
}
```

运行结果：

listByNormalFor: 163
listByStrengthenFor: 3
listByIterator: 1



## 序列化和反序列化

- 序列化和反序列化是什么?
- 实现序列化和反序列化为什么要实现Serializable接口?
- 实现Serializable接口就算了, 为什么还要显示指定serialVersionUID的值?
- 我要为serialVersionUID指定个什么值?

### 序列化和反序列化

- 序列化：把对象转换为字节序列的过程称为对象的序列化.
- 反序列化：把字节序列恢复为对象的过程称为对象的反序列化.

### 什么时候需要用到序列化和反序列化呢?

当我们只在本地JVM里运行下Java实例, 这个时候是不需要什么序列化和反序列化的, 但当我们需要将内存中的对象持久化到磁盘, 数据库中时, 当我们需要与浏览器进行交互时, 当我们需要实现RPC时, 这个时候就需要序列化和反序列化了.

前两个需要用到序列化和反序列化的场景, 是不是让我们有一个很大的疑问? 我们在与浏览器交互时, 还有将内存中的对象持久化到数据库中时, 好像都没有去进行序列化和反序列化, 因为我们都没有实现Serializable接口, 但一直正常运行.

下面先给出结论:

**只要我们对内存中的对象进行持久化或网络传输, 这个时候都需要序列化和反序列化.**

理由:

服务器与浏览器交互时真的没有用到Serializable接口吗? JSON格式实际上就是将一个对象转化为字符串, 所以服务器与浏览器交互时的数据格式其实是字符串, 我们来看来String类型的源码:

```
public final class String
    implements java.io.Serializable, Comparable<String>, CharSequence {
    /** The value is used for character storage. */
    private final char value[];

    /** Cache the hash code for the string */
    private int hash; // Default to 0

    /** use serialVersionUID from JDK 1.0.2 for interoperability */
    private static final long serialVersionUID = -6849794470754667710L;

    ......
}
```

String类型实现了Serializable接口, 并显示指定serialVersionUID的值.

然后我们再来看对象持久化到数据库中时的情况, Mybatis数据库映射文件里的insert代码:

```
<insert id="insertUser" parameterType="org.tyshawn.bean.User">
    INSERT INTO t_user(name, age) VALUES (#{name}, #{age})
</insert>
```

实际上我们并不是将整个对象持久化到数据库中, 而是将对象中的属性持久化到数据库中, 而这些属性都是实现了Serializable接口的基本属性.

### 实现序列化和反序列化为什么要实现Serializable接口?

在Java中实现了Serializable接口后, JVM会在底层帮我们实现序列化和反序列化, 如果我们不实现Serializable接口, 那自己去写一套序列化和反序列化代码也行, 至于具体怎么写, Google一下你就知道了.

### 实现Serializable接口就算了, 为什么还要显示指定serialVersionUID的值?

如果不显示指定serialVersionUID, JVM在序列化时会根据属性自动生成一个serialVersionUID, 然后与属性一起序列化, 再进行持久化或网络传输. 在反序列化时, JVM会再根据属性自动生成一个新版serialVersionUID, 然后将这个新版serialVersionUID与序列化时生成的旧版serialVersionUID进行比较, 如果相同则反序列化成功, 否则报错.

如果显示指定了serialVersionUID, JVM在序列化和反序列化时仍然都会生成一个serialVersionUID, 但值为我们显示指定的值, 这样在反序列化时新旧版本的serialVersionUID就一致了.

在实际开发中, 不显示指定serialVersionUID的情况会导致什么问题? 如果我们的类写完后不再修改, 那当然不会有问题, 但这在实际开发中是不可能的, 我们的类会不断迭代, 一旦类被修改了, 那旧对象反序列化就会报错. 所以在实际开发中, 我们都会显示指定一个serialVersionUID, 值是多少无所谓, 只要不变就行.

写个实例测试下:

(1) User类

不显示指定serialVersionUID.

```
public class User implements Serializable {

    private String name;
    private Integer age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
```

(2) 测试类

先进行序列化, 再进行反序列化.

```
public class SerializableTest {

    private static void serialize(User user) throws Exception {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File("D:\\111.txt")));
        oos.writeObject(user);
        oos.close();
    }

    private static User deserialize() throws Exception{
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("D:\\111.txt")));
        return (User) ois.readObject();
    }


    public static void main(String[] args) throws Exception {
        User user = new User();
        user.setName("tyshawn");
        user.setAge(18);
        System.out.println("序列化前的结果: " + user);

        serialize(user);

        User dUser = deserialize();
        System.out.println("反序列化后的结果: "+ dUser);
    }
}
```

(3) 结果

先注释掉反序列化代码, 执行序列化代码, 然后User类新增一个属性sex

```
public class User implements Serializable {

    private String name;
    private Integer age;
    private String sex;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                '}';
    }
}
```

再注释掉序列化代码执行反序列化代码, 最后结果如下:

> 序列化前的结果: User{name='tyshawn', age=18}
> Exception in thread "main" java.io.InvalidClassException: org.tyshawn.SerializeAndDeserialize.User; local class incompatible: stream classdesc serialVersionUID = 1035612825366363028, local class serialVersionUID = -1830850955895931978

报错结果为序列化与反序列化产生的serialVersionUID不一致.

接下来我们在上面User类的基础上显示指定一个serialVersionUID

```
private static final long serialVersionUID = 1L;
```

再执行上述步骤, 测试结果如下:

> 序列化前的结果: User{name='tyshawn', age=18}
> 反序列化后的结果: User{name='tyshawn', age=18, sex='null'}

显示指定serialVersionUID后就解决了序列化与反序列化产生的serialVersionUID不一致的问题.更多面试题，欢迎关注 公众号Java面试题精选

### Java序列化的其他特性

先说结论, 被transient关键字修饰的属性不会被序列化, static属性也不会被序列化.

我们来测试下这个结论:

(1) User类

```
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;
    private Integer age;
    private transient String sex;
    private static String signature = "你眼中的世界就是你自己的样子";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public static String getSignature() {
        return signature;
    }

    public static void setSignature(String signature) {
        User.signature = signature;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex +'\'' +
                ", signature='" + signature + '\'' +
                '}';
    }
}
```

(2) 测试类

```
public class SerializableTest {

    private static void serialize(User user) throws Exception {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File("D:\\111.txt")));
        oos.writeObject(user);
        oos.close();
    }

    private static User deserialize() throws Exception{
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("D:\\111.txt")));
        return (User) ois.readObject();
    }


    public static void main(String[] args) throws Exception {
        User user = new User();
        user.setName("tyshawn");
        user.setAge(18);
        user.setSex("man");
        System.out.println("序列化前的结果: " + user);

        serialize(user);

        User dUser = deserialize();
        System.out.println("反序列化后的结果: "+ dUser);
    }
}
```

(3) 结果

先注释掉反序列化代码, 执行序列化代码, 然后修改User类signature = “我的眼里只有你”, 再注释掉序列化代码执行反序列化代码, 最后结果如下:

> 序列化前的结果: User{name='tyshawn', age=18, sex='man', signature='你眼中的世界就是你自己的样子'}
> 反序列化后的结果: User{name='tyshawn', age=18, sex='null', signature='我的眼里只有你'}

### static属性为什么不会被序列化?

因为序列化是针对对象而言的, 而static属性优先于对象存在, 随着类的加载而加载, 所以不会被序列化.

看到这个结论, 是不是有人会问, serialVersionUID也被static修饰, 为什么serialVersionUID会被序列化? 其实serialVersionUID属性并没有被序列化, JVM在序列化对象时会自动生成一个serialVersionUID, 然后将我们显示指定的serialVersionUID属性值赋给自动生成的serialVersionUID.



# 实用笔记

## 全局异常捕获

现在比较常用的方式是给前端返回JSON，但是有些时候程序出现一些异常，导致前端页面报错不是很友好，所以就需要对项目进行全局的异常捕获，返回给前端固定的格式，进行友好的处理！

**1 、Java异常分类**

![img](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%90%8E%E7%AB%AF/%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/image/Java%E7%AC%94%E8%AE%B0/20180903094157693)

Java把异常当做对象来处理。Throwable是所有错误或异常的超类。Throwable类有两个直接子类：Error类和Exception类。
1、Error是指java运行时系统的内部错误和资源耗尽错误，是程序无法处理的异常，应用程序不会抛出该类对象。
2、Exception是程序本身可以处理的异常，应尽可能去处理这些异常。Exception分两类，一个是运行时异常RuntimeException，一个是检查异常CheckedException。
3、CheckedException一般是外部错误，这种异常都发生在编译阶段，Java编译器会强制程序去捕获此类异常。
4、RuntimeException是那些可能在Java 虚拟机正常运行期间抛出的异常的超类。这种错误是由程序员引起的错误，可以修正代码解决。

**2、异常处理**

在J2EE项目的开发中，不管是对底层的数据库操作过程，还是业务层的处理过程，还是控制层的处理过程，都不可避免会遇到各种可预知的、不可预知的异常需要处理。每个过程都单独处理异常，系统的代码耦合度高，工作量大且不好统一，维护的工作量也很大。 那么，能不能将所有类型的异常处理从各处理过程解耦出来，这样既保证了相关处理过程的功能较单一，也实现了异常信息的统一处理和维护？答案是肯定的。下面将介绍使用Spring MVC统一处理异常的解决和实现过程

- 使用Spring MVC提供的SimpleMappingExceptionResolver
- 实现Spring的异常处理接口HandlerExceptionResolver 自定义自己的异常处理器
- 使用@ExceptionHandler注解实现异常处理



**3、案例**

1.继承Exception,自定义异常类

```java
package com.zichen.xhkq.exception;
 
/**
 * 
 * <p>Title: CustomException</p>
 * <p>Description: 系统自定义的异常类型，实际开发中可能要定义多种异常类型</p>
 * @author	CNZZ
 * @version 1.0
 */
public class CustomException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5819174480253773214L;
	//异常信息
	private String message;
	
	public CustomException(String message){
		super(message);
		this.message = message;
		
	}
 
	public String getMessage() {
		return message;
	}
 
	public void setMessage(String message) {
		this.message = message;
	}
	
}
```

2.实现HandlerExceptionResolver，自定义异常处理器，可以判断处理自定义异常和系统异常，可以跳转异常页面

```java
package com.zichen.xhkq.exception;
 
import java.io.IOException;
 
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
 
/**
 * 
 * <p>Title: CustomExceptionResolver</p>
 * <p>Description: 自定义异常处理器</p>
 * @author	CNZZ
 * @version 1.0
 */
public class CustomExceptionResolver implements HandlerExceptionResolver  {
 
	//前端控制器DispatcherServlet在进行HandlerMapping、调用HandlerAdapter执行Handler过程中，如果遇到异常就会执行此方法
	//handler最终要执行的Handler，它的真实身份是HandlerMethod
	//Exception ex就是接收到异常信息
	@Override
	public ModelAndView resolveException(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex) {
		//输出异常
		ex.printStackTrace();
		
		//统一异常处理代码
		//针对系统自定义的CustomException异常，就可以直接从异常类中获取异常信息，将异常处理在错误页面展示
		//异常信息
		String message = null;
		CustomException customException = null;
		//如果ex是系统 自定义的异常，直接取出异常信息
		if(ex instanceof CustomException){
			customException = (CustomException)ex;
		}else{
			//针对非CustomException异常，对这类重新构造成一个CustomException，异常信息为“未知错误”
			customException = new CustomException("未知错误");
		}
		
		//错误 信息
		message = customException.getMessage();
		
		request.setAttribute("message", message);
 
		
		try {
			//转向到错误 页面
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new ModelAndView();
	}
 
}
```

3.配置javaBean

```xml
<!--全局异常处理 -->
    <bean id="customExceptionResolver" class="com.zichen.xhkq.exception.CustomExceptionResolver" />
```



## MapStruct

在前几天的文章《[为什么阿里巴巴禁止使用Apache Beanutils进行属性的copy？](https://www.hollischuang.com/archives/5337)》中，我曾经对几款属性拷贝的工具类进行了对比。

然后在评论区有些读者反馈说MapStruct才是真的香，于是我就抽时间了解了一下MapStruct。结果我发现，这真的是一个神仙框架，炒鸡香。

这一篇文章就来简单介绍下MapStruct的用法，并且再和其他几个工具类进行一下对比。

### 为什么需要MapStruct ？

首先，我们先说一下MapStruct这类框架适用于什么样的场景，为什么市面上会有这么多的类似的框架。

在软件体系架构设计中，分层式结构是最常见，也是最重要的一种结构。很多人都对三层架构、四层架构等并不陌生。

甚至有人说：**"计算机科学领域的任何问题都可以通过增加一个间接的中间层来解决，如果不行，那就加两层。"**

但是，随着软件架构分层越来越多，那么各个层次之间的数据模型就要面临着相互转换的问题，典型的就是我们可以在代码中见到各种O，如DO、DTO、VO等。

一般情况下，同样一个数据模型，我们在不同的层次要使用不同的数据模型。**如在数据存储层，我们使用DO来抽象一个业务实体；在业务逻辑层，我们使用DTO来表示数据传输对象；到了展示层，我们又把对象封装成VO来与前端进行交互。**

那么，数据的从前端透传到数据持久化层（从持久层透传到前端），就需要进行对象之间的互相转化，即在不同的对象模型之间进行映射。

通常我们可以使用get/set等方式逐一进行字段映射操作，如：

```
personDTO.setName(personDO.getName());
personDTO.setAge(personDO.getAge());
personDTO.setSex(personDO.getSex());
personDTO.setBirthday(personDO.getBirthday());
1234
```

但是，编写这样的映射代码是一项冗长且容易出错的任务。MapStruct等类似的框架的目标是通过自动化的方式尽可能多地简化这项工作。

### MapStruct的使用

MapStruct（https://mapstruct.org/ ）是一种代码生成器，它极大地简化了基于"约定优于配置"方法的Java bean类型之间映射的实现。生成的映射代码使用纯方法调用，因此快速、类型安全且易于理解。

> 约定优于配置，也称作按约定编程，是一种软件设计范式，旨在减少软件开发人员需做决定的数量，获得简单的好处，而又不失灵活性。

假设我们有两个类需要进行互相转换，分别是PersonDO和PersonDTO，类定义如下：

```java
public class PersonDO {
    private Integer id;
    private String name;
    private int age;
    private Date birthday;
    private String gender;
}

public class PersonDTO {
    private String userName;
    private Integer age;
    private Date birthday;
    private Gender gender;
}
```

我们演示下如何使用MapStruct进行bean映射。

想要使用MapStruct，首先需要依赖他的相关的jar包，使用maven依赖方式如下：

```java
...
<properties>
    <org.mapstruct.version>1.3.1.Final</org.mapstruct.version>
</properties>
...
<dependencies>
    <dependency>
        <groupId>org.mapstruct</groupId>
        <artifactId>mapstruct</artifactId>
        <version>${org.mapstruct.version}</version>
    </dependency>
</dependencies>
...
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <version>3.8.1</version>
            <configuration>
                <source>1.8</source> <!-- depending on your project -->
                <target>1.8</target> <!-- depending on your project -->
                <annotationProcessorPaths>
                    <path>
                        <groupId>org.mapstruct</groupId>
                        <artifactId>mapstruct-processor</artifactId>
                        <version>${org.mapstruct.version}</version>
                    </path>
                    <!-- other annotation processors -->
                </annotationProcessorPaths>
            </configuration>
        </plugin>
    </plugins>
</build>
```

因为MapStruct需要在编译器生成转换代码，所以需要在maven-compiler-plugin插件中配置上对mapstruct-processor的引用。这部分在后文会再次介绍。

之后，我们需要定义一个做映射的接口，主要代码如下：

```java
@Mapper
interface PersonConverter {
    PersonConverter INSTANCE = Mappers.getMapper(PersonConverter.class);

    @Mappings(@Mapping(source = "name", target = "userName"))
    PersonDTO do2dto(PersonDO person);
}
```

使用注解 `@Mapper`定义一个Converter接口，在其中定义一个do2dto方法，方法的入参类型是PersonDO，出参类型是PersonDTO，这个方法就用于将PersonDO转成PersonDTO。

测试代码如下：

```java
public static void main(String[] args) {
    PersonDO personDO = new PersonDO();
    personDO.setName("Hollis");
    personDO.setAge(26);
    personDO.setBirthday(new Date());
    personDO.setId(1);
    personDO.setGender(Gender.MALE.name());
    PersonDTO personDTO = PersonConverter.INSTANCE.do2dto(personDO);
    System.out.println(personDTO);
}
```

输出结果：

```java
PersonDTO{userName='Hollis', age=26, birthday=Sat Aug 08 19:00:44 CST 2020, gender=MALE}
```

可以看到，我们使用MapStruct完美的将PersonDO转成了PersonDTO。

上面的代码可以看出，MapStruct的用法比较简单，主要依赖`@Mapper`注解。

但是我们知道，大多数情况下，我们需要互相转换的两个类之间的属性名称、类型等并不完全一致，还有些情况我们并不想直接做映射，那么该如何处理呢？

其实MapStruct在这方面也是做的很好的。

### MapStruct处理字段映射

首先，可以明确的告诉大家，如果要转换的两个类中源对象属性与目标对象属性的类型和名字一致的时候，会自动映射对应属性。

那么，如果遇到特殊情况如何处理呢？

#### 名字不一致如何映射

如上面的例子中，在PersonDO中用name表示用户名称，而在PersonDTO中使用userName表示用户名，那么如何进行参数映射呢。

这时候就要使用`@Mapping`注解了，只需要在方法签名上，使用该注解，并指明需要转换的源对象的名字和目标对象的名字就可以了，如将name的值映射给userName，可以使用如下方式：

```java
@Mapping(source = "name", target = "userName")
```

#### 可以自动映射的类型

除了名字不一致以外，还有一种特殊情况，那就是类型不一致，如上面的例子中，在PersonDO中用String类型表示用户性别，而在PersonDTO中使用一个Genter的枚举表示用户性别。

这时候类型不一致，就需要涉及到互相转换的问题

其实，MapStruct会对部分类型自动做映射，不需要我们做额外配置，如例子中我们将String类型自动转成了枚举类型。

一般情况下，对于以下情况可以做自动类型转换：

- 基本类型及其他们对应的包装类型。
- 基本类型的包装类型和String类型之间
- String类型和枚举类型之间

#### 自定义常量

如果我们在转换映射过程中，想要给一些属性定义一个固定的值，这个时候可以使用 constant

```java
@Mapping(source = "name", constant = "hollis")
```

#### 类型不一致的如何映射

还是上面的例子，如果我们需要在Person这个对象中增加家庭住址这个属性，那么我们一般在PersonoDTO中会单独定义一个HomeAddress类来表示家庭住址，而在Person类中，我们一般使用String类型表示家庭住址。

这就需要在HomeAddress和String之间使用JSON进行互相转化，这种情况下，MapStruct也是可以支持的。

```java
public class PersonDO {
    private String name;
    private String address;
}

public class PersonDTO {
    private String userName;
    private HomeAddress address;
}
@Mapper
interface PersonConverter {
    PersonConverter INSTANCE = Mappers.getMapper(PersonConverter.class);

    @Mapping(source = "userName", target = "name")
    @Mapping(target = "address",expression = "java(homeAddressToString(dto2do.getAddress()))")
    PersonDO dto2do(PersonDTO dto2do);

    default String homeAddressToString(HomeAddress address){
        return JSON.toJSONString(address);
    }
}
```

我们只需要在PersonConverter中在定义一个方法（因为PersonConverter是一个接口，所以在JDK 1.8以后的版本中可以定义一个default方法），这个方法的作用就是将HomeAddress转换成String类型。

> default方法：Java 8 引入的新的语言特性，用关键字default来标注，被default所标注的方法，需要提供实现，而子类可以选择实现或者不实现该方法

然后在dto2do方法上，通过以下注解方式即可实现类型的转换：

```java
@Mapping(target = "address",expression = "java(homeAddressToString(dto2do.getAddress()))")
```

上面这种是自定义的类型转换，还有一些类型的转换是MapStruct本身就支持的，如String和Date之间的转换：

```java
@Mapping(target = "birthday",dateFormat = "yyyy-MM-dd HH:mm:ss")
```

以上，简单介绍了一些常用的字段映射的方法，也是我自己在工作中经常遇到的几个场景，更多的情况大家可以查看官方的示例（https://github.com/mapstruct/mapstruct-examples）。

### MapStruct的性能

前面说了这么多MapStruct的用法，可以看出MapStruct的使用还是比较简单的，并且字段映射上面的功能很强大，那么他的性能到底怎么样呢？

参考《[为什么阿里巴巴禁止使用Apache Beanutils进行属性的copy？](https://www.hollischuang.com/archives/5337)》中的示例，我们对MapStruct进行性能测试。

分别执行1000、10000、100000、1000000次映射的耗时分别为：0ms、1ms、3ms、6ms。

可以看到，**MapStruct的耗时相比较于其他几款工具来说是非常短的**。

那么，为什么MapStruct的性能可以这么好呢？

其实，MapStruct和其他几类框架最大的区别就是：**与其他映射框架相比，MapStruct在编译时生成bean映射，这确保了高性能，可以提前将问题反馈出来，也使得开发人员可以彻底的错误检查。**

还记得前面我们在引入MapStruct的依赖的时候，特别在maven-compiler-plugin中增加了mapstruct-processor的支持吗？

并且我们在代码中使用了很多MapStruct提供的注解，这使得在编译期，MapStruct就可以直接生成bean映射的代码，相当于代替我们写了很多setter和getter。

如我们在代码中定义了以下一个Mapper：

```java
@Mapper
interface PersonConverter {
    PersonConverter INSTANCE = Mappers.getMapper(PersonConverter.class);

    @Mapping(source = "userName", target = "name")
    @Mapping(target = "address",expression = "java(homeAddressToString(dto2do.getAddress()))")
    @Mapping(target = "birthday",dateFormat = "yyyy-MM-dd HH:mm:ss")
    PersonDO dto2do(PersonDTO dto2do);

    default String homeAddressToString(HomeAddress address){
        return JSON.toJSONString(address);
    }
}
```

经过代码编译后，会自动生成一个PersonConverterImpl：

```java
@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-08-09T12:58:41+0800",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_181 (Oracle Corporation)"
)
class PersonConverterImpl implements PersonConverter {

    @Override
    public PersonDO dto2do(PersonDTO dto2do) {
        if ( dto2do == null ) {
            return null;
        }

        PersonDO personDO = new PersonDO();

        personDO.setName( dto2do.getUserName() );
        if ( dto2do.getAge() != null ) {
            personDO.setAge( dto2do.getAge() );
        }
        if ( dto2do.getGender() != null ) {
            personDO.setGender( dto2do.getGender().name() );
        }

        personDO.setAddress( homeAddressToString(dto2do.getAddress()) );

        return personDO;
    }
}
```

在运行期，对于bean进行映射的时候，就会直接调用PersonConverterImpl的dto2do方法，这样就没有什么特殊的事情要做了，只是在内存中进行set和get就可以了。

所以，因为在编译期做了很多事情，所以MapStruct在运行期的性能会很好，并且还有一个好处，那就是可以把问题的暴露提前到编译期。

使得如果代码中字段映射有问题，那么应用就会无法编译，强制开发者要解决这个问题才行。

### 总结

本文介绍了一款Java中的字段映射工具类，MapStruct，他的用法比较简单，并且功能非常完善，可以应付各种情况的字段映射。

并且因为他是编译期就会生成真正的映射代码，使得运行期的性能得到了大大的提升。


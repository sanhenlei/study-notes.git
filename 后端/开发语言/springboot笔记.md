# 一、Springboot入门

## 什么是Spring

Spring是一个后端开源框架，2003年兴起的一个轻量级的Java开发框架。

Spring是为了解决企业级应用开发的复杂性而创建的，简化开发。

## Spring是如何简化Java开发的

为了降低Java开发的复杂性，Spring采用了一下4中关键策略：

1. 基于POJO<Plain Ordinary *Java* Objects，基础对象>的轻量级和最小侵入性编程；
2. 通过IOC<Inversion of Control，控制反转>，依赖注入（DI）和面向接口实现松耦合；
3. 基于切面（AOP<Aspect Oriented Programming>）和惯例进行声明式编程；
4. 通过切面和模版减少样式化代码；

## 什么是SpringBoot

学过javaweb的同学就知道，开发一个web应用，从最初开始接触Servlet结合Tomcat, 跑出一个Hello Wolrld程序，是要经历特别多的步骤； 后来就用了框架Struts，再后来是SpringMVC，到了现在的SpringBoot，过一两年又会有其他web框架出现；什么是SpringBoot呢，就是`一个javaweb的开发框架`，和`SpringMVC类似`，对比其他javaweb框架的好处，官方说是简化开发，约定大于配置， you can "just run"，能迅速的开发web应用，几行代码开发一个http接口。

所有的技术框架的发展似乎都遵循了一条主线规律：从一个复杂应用场景衍生一种规范框架，人们只需要进行各种配置而不需要自己去实现它，这时候强大的配置功能成了优点；发展到一定程度之后，人们根据实际生产应用情况，选取其中实用功能和设计精华，重构出一些轻量级的框架；之后为了提高开发效率，嫌弃原先的各类配置过于麻烦，于是开始提倡“约定大于配置”，进而衍生出一些一站式的解决方案。

这就是Java企业级应用->J2EE->Spring->SpringBoot的过程。

随着Spring不断的发展，涉及的领域越来越多，项目整合开发需要配合各种各样的文件，慢慢变得不那么易用简单，违背了最初的理念，甚至人称配置地域。Spring Boot正式这样一个背景下被抽象出来的开发框架，目的为了让大家`更容易的使用Spring、更容易的继承各种常用的中间件、开源软件`；

Spring Boot 基于Spring开发，Spring Boot本身并不提供Spring框架的核心特性以及扩展功能，只是用于快速、敏捷的开发新一代基于Spring框架的应用程序。也就是说，它并不是用来代替Spring的解决方案，而是和Spring框架紧密结合用于提升Spring开发者体验的工具。Spring Boot以约定大于配置的核心思想，默认帮我们进行了很多设置，多数Spring Boot应用只需要很少的Spring配置。同时它集成了大量常用的第三方库配置（例如Redis、MongoDB、Jpa、RabbitMQ、Quartz等等），Spring Boot应用中这些第三方库几乎可以零配置的开箱即用。

​	`简单来说SpringBoot其实不是什么新的框架，它默认配置了很多框架的使用方式，就像maven整合了所有的jar包，Spring Boot整合了所有的框架。`

## Spring Boot的主要优点：

- 为所有Spring开发者更快的入门
- 开箱即用，提供各种默认配置来简化项目配置
- 内嵌式容器简化web项目
- 没有冗余代码生成和XML配置的要求



## 微服务

### 什么事微服务

微服务是一种架构风格，它要求我们在开发一个应用的时候，这个应用必须构建成一系列小服务的组合；可以通过http的方式进行互通，要说微服务架构，先得说说过去我们的单体应用框架。

### 单体应用框架

所谓单体应用框架（all in one）是指，我们将一个应用中的所有服务都封装在一个应用中。

无论是ERP、CRM或是其他什么系统，你都把数据库访问，web访问，等各个功能放到一个war包内。

这样做的好处是：易于开发和测试；也十分方便部署；当需要扩展时，只需要将war复制多份，然后放到多个服务器上，在做个负载均衡就可以了。

单体应用框架的缺点是，哪怕我只需要一个非常小 的地方，都需要停掉整个服务，重新打包、部署这个应用war包。特别是对于一个大型应用，我们不可能把所有内容都放到一个应用里面，我们如何维护、如何分工都是问题。

### 微服务架构

把每个功能元素独立出来，把独立出来的元素的动态组合，需要的功能元素才去拿来组合，需要多一些时可以整合多个功能元素，所以微服务架构师对功能元素进行复制，而没有对整个应用进行复制。

这样做的好处是：

- 节省了调用资源
- 每个功能元素的服务都是一个可替换的、可独立升级的软件代码

![image-20200319110455862](image/springboot笔记/image-20200319110455862.png)

Martin Flower 于 2014 年 3 月 25 日写的《Microservices》，详细的阐述了什么是微服务。

- 原文地址：http://martinfowler.com/articles/microservices.html
- 翻译：https://www.cnblogs.com/liuning8023/p/4493156.html

### 如何构建微服务

一个大型系统的微服务架构，就像一个复杂交织的神经网络，每一个神经元就是一个功能元素，他们各自完成自己的功能，然后通过http相互请求调用。比如一个电商系统，查缓存、连数据库、浏览页面、结账、支付等服务都是一个个独立的功能服务，都被微化了，他们作为一个个微服务共同构建了一个庞大的系统。如果修改其中一个功能，只需要更新升级其中一个功能服务单元即可。

但是这种庞大的系统架构给部署和运维带来了很大的难度。浴室spring为我们带来了构建大型分布式微服务的全套、全称产品：

- 构建一个个功能独立的微服务应用单元，可以使用SpringBoot，可以帮我们快速构建一个应用；
- 大型分布式网络服务的调用，这部分由spring colud来完成，实现分布式；
- 在分布式中间，进行流式数据计算、批处理，我们有spring cloud data flow
- spring为我们想清楚了整个从开始构建应用到大型分布式应用全流程方案

![image-20200319111209853](image/springboot笔记/image-20200319111209853.png)

## 实战开发

### 准备工作

  将学习如何快速创建一个Spring Boot应用，并且实现一个简单的Http请求处理，通过这个例子对SpringBoot有一个初步的了解，并体验其结构简单、开发快速的特性。

  **环境准备：**

  - java version 1.8.0_181
  - maven 3.3.1
  - SpringBoot 2.x
  - IDEA

  ### SpringBoot原理

  #### 自动配置

  pom.xml

  - spring-boot-dependencies：核心依赖在父工程中！
  - 在写或者引入一些SpringBoot依赖的时候，不需要指定版本，是因为有这些版本仓库

  #### 启动器

  ```xml
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter</artifactId>
          </dependency>
  ```

  - 说白了就是SpringBoot的启动场景；
  - 比如spring-boot-starter-web，帮我们自动导入web环境所有的依赖！
  - SpringBoot会将所有的功能场景，都变成一个个启动器
  - 我们使用什么功能，只需要找到对应的的启动器就可以了

- 主程序类，主入口类


```java
@SpringBootApplication(exclude = MongoAutoConfiguration.class)
public class Application {

    protected static final Logger logger = LoggerFactory.getLogger(Application.class);
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

        String url = "http://localhost:8088/templates/javastudy/home.html";
        logger.info("在浏览器中打开: " + url);

        Runtime run = Runtime.getRuntime();
        try{
            run.exec("cmd /c start " + url);
            logger.debug("启动浏览器打开项目成功");
        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage());
        }

    }

}
```

**@SpringBootApplication:**  Spring Boot应用标注在某个类上说明这个类似SpringBoot的主配置类，SpringBoot就应该运行这个类的main方法来启动应用；

**@SpringBootConfiguration：**SpringBoot的配置类：

​			标注在某个类上，表示这是一个Spring Boot的配置类；

​			@Configuration：配置类上来标注这个注解

​				配置类》》配置文件；配置类也是容器中的一个组件：@Component

@EnableAutoConfiguration：开启自动配置功能；

  #### 主程序	

  ```java
  // 标注这个类似一个springboot的应用
  @SpringBootApplication
  public class Springboot01Application {
  //    将springboot应用启动
      public static void main(String[] args) {
          SpringApplication.run(Springboot01Application.class, args);
      }
  }
  ```

  **注解**

  ```java
  @SpringBootConfiguration	// springboot的配置
  	@Configuration		// spring配置类
  	@Component			// 说明这是一个spring组件
  @EnableAutoConfiguration	// 自动配置
  	@AutoConfigurationPackage	// 自动配置包
  		@Import({Registrar.class})		// 自动配置“包注册”
  	@Import({AutoConfigurationImportSelector.class}) // 自动配置导入选择
  // 获取所有的配置
  List<String> configurations = this.getCandidateConfigurations(annotationMetadata, attributes);
  
  ```

  获取候选的配置

  ```java
      protected List<String> getCandidateConfigurations(AnnotationMetadata metadata, AnnotationAttributes attributes) {
          List<String> configurations = SpringFactoriesLoader.loadFactoryNames(this.getSpringFactoriesLoaderFactoryClass(), this.getBeanClassLoader());
          Assert.notEmpty(configurations, "No auto configuration classes found in META-INF/spring.factories. If you are using a custom packaging, make sure that file is correct.");
          return configurations;
      }
  ```

  > META-INF/spring.factories 自动配置的核心文件，springboot所有自动配置都是在启动的时候扫描并加载这个文件，所有的自动配置类都在这里面，但是不一定生效，要判断条件是否成立，只要导入了对应的start，就有对应的启动器了，有了启动器，我们自动装配的就会生效，然后就配置成功

  1、springboot在启动的时候，从类路径下`META-INF/spring.factories`获取指定的值；

  2、将这些自动配置的类导入容器，自动配置就会生效，帮我们进行配置

  3、以前我们需要自动配置的东西，现在springboot帮我们做了！

  4、整合javaEE，解决方案会自动配置的东西都在spring-boot-autoconfigure-2.2.0.RELEASE.jar这个包下

  5、它会把所有需要导入的组件，以类名的方式返回，这些组件就会被添加到容器；

  6、容器中也会存在非常多的xxxAutoConfiguration的文件（@Bean），就是这些类的容器中导入了这个场景需要的所有组件，并自动配置，@Configuration, JavaConfig

  7.有了自动配置类，免去了我们手动编写配置文件的工作。

  ### pom.xml分析

  - 项目元数据信息：创建时候输入的Project Metadata部分，也就是Maven项目的基本元素，包括：groupId、artifactId、version、name、description等
  - parent：继承`spring-boot-starter-parent`的依赖管理，控制版本与打包等内容
  - dependencies：项目具体依赖，这里包含了`spring-boot-starter-web`用于实现HTTP接口（该依赖中包含了Spring MVC），官网对它的描述是：使用Spring MVC构建Web（包括RESTful）应用程序的入门者，使用Tomcat作为默认嵌入式容器。`spring-boot-starter-test`用于编写单元测试的依赖包。
  - build：构建配置部分。默认使用了`spring-boot-maven-plugin`，配合`spring-boot-starter-parent`就可以把spring boot应用打包成JAR来直接运行。

  ### 主启动类的运行步骤

  ![image-20200322172053939](image/springboot笔记/image-20200322172053939.png)

### 打jar包

#### lib分离

注意修改“<mainClass>com.nokia.App</mainClass>”

```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <configuration>
                <source>1.8</source>
                <target>1.8</target>
            </configuration>
        </plugin>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-jar-plugin</artifactId>
            <configuration>
                <archive>
                    <manifest>
                        <addClasspath>true</addClasspath>
                        <useUniqueVersions>false</useUniqueVersions>
                        <classpathPrefix>lib/</classpathPrefix>
                        <mainClass>com.nokia.App</mainClass>
                    </manifest>
                </archive>
            </configuration>
        </plugin>
    </plugins>
</build>
```

# 二、配置文件

## 1、配置文件

SpringBoot使用一个全局的配置文件，配置文件名是固定的；

* application.properties
* application.yml

配置文件的作用：修改SpringBoot自动配置的默认值；



YAML语言：

​		以前的配置文件；大多都是使用xml文件;

​		YAML以数据为中心，比xml、json等更适合做配置文件；

YAML例子：

```yaml
server:
	port: 8081
```

XML例子：

```xml
<server>
	<port>8081</port>
</server>
```



### 2、YAML语法：

1、基本语法

* k:(空格)v：表示一对键值对（空格必须有）；

* 以空格的缩进来控制层级关系；只要是左对齐的一列数据，都是同一个层级

* 属性和值是大小写敏感；

2、值的写法

​	**字面量：普通的值（数字、字符串、布尔）**

​		k: v : 字面直接来写；

​				 字符串默认不用加上单引号或者双引号；

​				 "" ：双引号；不会转义字符串里面的特殊字符，特殊字符会作为本身表示的意思；

​						·如：name: "zhangsan \n list"		输出的会换行

​				 '' ：单引号 ：会转义特殊字符

**对象、Map**

​	k: v

​			对象还是k: v的方式

```yaml
#正常写法：
person:
  name: zhangsan
  age: 18
  
#行内写法：
person: {name: zhangsan,age: 18}
```

**数组**

用-值表示数组中的一个元素

```yaml
#正常写法
pets:
  - cat
  - dog
  - pig
#行内写法
pets: [cat,dog,pig]
```



### 3、配置文件值注入

文件结构：

![1574610196869](.\image\1574610196869.png)

配置文件：

```java
person:
  name: zhangsan
  age: 18
  boss: false
  birth: 2010/10/06
  maps: {k1: v1, k2: v2}
  lists:
    - lisi
    - zhaoliu
  dog:
    name: xiaogou
    age: 2
```

bean文件：

```java
//@ConfigurationProperties 默认从全局配置文件中获取
@Component
@ConfigurationProperties(prefix = "person")
public class Person {
    private String name;
    private Integer age;
    private boolean boss;
    private Date  birth;
    private Map<String, Object> maps;
    private List<Object> lists;
    private Dog dogs;
```

pom.xml文件：

```java
        <!--导入配置文件处理器-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-configuration-processor</artifactId>
            <optional>true</optional>
        </dependency>
```

**@Value获取值和@ConfigurationProerties获取值比较**

| 对比           | @ConfigurationProerties  | @Value       |
| -------------- | ------------------------ | ------------ |
| 功能           | 批量注入配置文件中的属性 | 一个一个绑定 |
|                | 支持                     | 不支持       |
| SpEL           | 不支持                   | 支持         |
| JSR303数据校验 | 支持                     | 不支持       |
| 复杂类型封装   | 支持                     | 不支持       |

如果说，我们只是在某个业务逻辑中需要获取一下配置文件中的某项值，使用@Value;

如果说，我们专门编写了一个javaBean来和配置文件进行映射，我们就直接使用@ConfigurationProperties;

### 4、@PropertySource&@ImportResource

@PropertySource：加载指定的配置文件；

person.properties文件

```properties
person.name=zhangsan
person.age=18
person.boss=false
person.birth=2010/10/06
person.maps.k1=v1
person.maps.k2=v2
person.lists=lisi,zhao,liu
person.dog.name=xiaogou
person.dog.age=2
```

bean文件：

```java
@Component
@PropertySource(value = "classpath:pp.properties")
@ConfigurationProperties(prefix = "pp")
public class Pp {
    private String name;
    private Integer age;
    private boolean boss;
    private Date birth;
    private Map<String, Object> maps;
    private List<Object> lists;
    private Dog dogs;
```

@ImportResource:  导入Spring的配置文件，让配置文件里面的内容生效；

* 老方法注入，用途<bean></bean>：

```java
@ImportResource(locations = {"classpath:beans.xml"})
@SpringBootApplication
public class SpringBootStudyApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootStudyApplication.class, args);
    }

}
```

beans.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

        <bean id="helloService" class="com.xixin.springbootstudy.service.HelloService"></bean>
</beans>
```

* Springboot推荐方法给容器中添加组件的方式，推荐使用全注解方式 

1、配置类====Sprint配置文件

2、使用@Bean给容器添加组件

```java
/**
 * @Configuration : 指名当前类是一个配置类，就是来替代之前的spring配置文件
 */

@Configuration
public class MyAppConfig {
    // 将方法的返回值添加到容器中，容器中这个组件默认的id就是方法名
    @Bean
    public HelloService helloService(){
        System.out.println("配置类@Bean给容器中添加组件了。");
        return new HelloService();
    }
}
```

### 5、配置文件占位符

1、随机数

```yaml
${random.value}、${random.int}、${random.long}
${random.int(10)}、${random.int[1024、65535]}
```

2、占位符获取之前配置的值，如果没有可以使用:指定默认值

```yaml
person:
  name: zhangsan${random.uuid}
  age: ${random.int}
  boss: false
  birth: 2010/10/06
  maps: {k1: v1, k2: v2}
  lists:
    - lisi
    - zhaoliu
  dog:
    name: ${person.name}_xiaogou
    age: ${person.hello:5}
```

### 6、Profile

1. 多Profile文件

我们在主配置文件编写的时候，文件名可以是application-{profile}.properties/yml

默认使用application.properties的配置；

2. yml支持多文档块方式

```yaml
server:
  port: 8081
spring:
  profiles:
    active: dev
---
server:
  port: 8083
spring:
  profiles: dev
---
server:
  port: 8084
spring:
  profiles: prod
---
```



3. 激活指定Profile文件

* 在配置文件中指定spring.profiles.active=dev

* 命令行：

  ​	java -jar **.jar --spring.profiles.active=dev

### 7、配置文件加载位置

从上到下优先级越低，所有位置的文件都会被加载，高优先级配置会覆盖低优先级

1. file:./config/
2. file:./
3. classpath:/config/
4. classpath:./

也可以通过配置spring.config.location来改变默认的配置，项目打包好以后，可以使用命令行参数的形式，启动项目的时候来指定配置文件的新位置；

```xml
命令行参数
java -jar xxxxx.jar(项目包) --server.prot=8081
可以通过类似的方式修改配置
来自java：comp/env的NDI属性
Java系统属性（System.getProperties()）
操作系统环境变量
*RandomValuePropertySource配置的random.属性值
jar包外部的application-{profile}.properties或application.yml（带spring.profile）配置文件
jar包内部的application-{profile}.properties或application.yml（带spring.profile）配置文件
jar包外部的application.properties或application.yml（不带spring.profile）配置文件
jar包内部的application.properties或application.yml（不带spring.profile）配置文件
@Configuration注解类上的@PropertySource
通过SpringApplication.setDefaultProperties指定的默认属性
```

# 三、日志

## 1、日志框架

市面上的日志框架：

JUL、JCL、Jboss-logging、logback、log4j、log4j2、slf4j....

| 日志门面                  | 日志实现               |
| ------------------------- | ---------------------- |
| JCL、SLF4j、jboss-logging | Log4j、Log4j2、Logback |

`SpringBoot选用：门面Slf4j，日志实现Logback`

## 2、Slf4j使用

在类中添加两个包：

```java
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelloWorld {
  public static void main(String[] args) {
    Logger logger = LoggerFactory.getLogger(HelloWorld.class);
    logger.info("Hello World");
  }
}
```

 ![img](image\springboot笔记\concrete-bindings.png) 

每一个日志的实现框架都有自己的配置文件。使用slf4j以后，配置文件还是做成日志实现框架自己本身的配置文件；

2、遗留问题

Spring、MyBatis等不同的框架下用了不同的日志框架，导致不统一。

如何让系统中所有的日志都统一到sfl4j：

1、将系统中其他日志框架先排除出去；

2、用中间包来替换原有的日志框架；

3、导入sfl4j其他包来实现

## 3、日志使用

#### 1、默认设置

SpringBoot默认配置好了日志：如下使用，或者用@Sfl4j注解可以就不用创建Logger对象了。

```java
@SpringBootTest
class SpringBootStudyApplicationTests {
    Logger log = (Logger) LoggerFactory.getLogger(getClass());
    @Test
    void contextLoads() {
        // 日志级别，由低到高，
        // 可以调整输出的日志级别，设置后就只输出这个级别及以后的高级别
        log.trace("这是trace日志。。。");
        log.debug("这是debug日志。。。");
        // springboot默认输出的是info级别的，使用的是root级别
        log.info("这是info日志。。。");
        log.warn("这是warn日志。。。");
        log.error("这是error日志。。。");
    }
}
```

指定日志配置文件位置：

| logging.file | logging.path | Example  | Description                      |
| ------------ | ------------ | -------- | -------------------------------- |
| (none)       | (none)       |          | 只在控制台输出                   |
| 指定文件名   | (none)       | my.log   | 输出日志到my.log文件             |
| (none)       | 指定目录     | /var/log | 输出到指定目录的spring.log文件中 |

```properties
#root日志以info级别输出
logging.level.root=info
#org.springframework.web包下的日志以DEBUG级别输出
logging.level.org.springframework.web=DEBUG
#org.hibernate包下的日志以ERROR级别输出
logging.level.org.hibernate=ERROR 

#注意：两个不能同时使用
logging.file.name=logs/info.log
#logging.file.path=logs/

#自定义日志输出格式
logging.pattern.console=%d{yyyy/MM/dd-HH:mm:ss.SSS} [%thread] %-5level %logger- %msg%n
logging.pattern.file=%d{yyyy/MM/dd-HH:mm:ss.SSS} [%thread] %-5level %logger- %msg%n
```

```
日志输出格式:
	%d：表示日期时间，
	%thread：表示线程名，
	%-5level: 级别从左显示5个字符宽度
	%loggerd{50}：表示logger名字最长50个字符，否则按照句点分割
	%msg: 日志消息
	%n：是换行符
```

#### 2、指定配置

给类路径下放上每个日志框架的配置文件即可；

| Logging System          | Customization                                                |
| ----------------------- | ------------------------------------------------------------ |
| Logback                 | logback-spring.xml, logback-spring.groovy, logback.xml or logback.groovy |
| Log4j2                  | log4g2-spring.xml or log4j2.xml                              |
| JDK(Java Utill Logging) | logging.properties                                           |

logback.xml：直接被日志框架识别了；

logback-spring.xml：推荐此方法，日志框架不直接加载日志的配置，有springboot解析日志配置，可以使用SpringBoot的高级Profile功能，如下：

```xml
<springProfile name="dev">
    可以指定某段配置只在开发环境下生效
</springProfile>
<springProfile name="prod">
    可以指定某段配置只在生产环境下生效
</springProfile>
```

![1574956897085](\image\springboot笔记\1574956897085.png)

## 4、Logback

#### 简单使用

*  如果无需复杂的日志配置，执行简单设置日志打印级别，打印方式可直接再 application.yml 中配置 

* 默认情况下 Spring Boot 将 info 级别的日志输出到控制台中，不会写到日志文件，且不能进行复杂配置。

  #####  打印到文件中

  在application.yml里配置

  ```yaml
  logging:
    # 配置输出额日志文件名,可以带路径
    #  file: out.log
    # 配置日志存放路径,日志文件名为：spring.log
    path: ./log
    file:
      # 设置日志文件大小
      max-size: 10MB
  ```

   **注意**:file 和 path 是不能同时配置的，如果同时配置`path`不会生效。 

#### logback.xml使用

​		接下来说明如何通过独立的 xml 配置文件来配置日志打印。虽然 springboot 是要消灭 xml 的，但是有些复杂功能还是得编写 xml。使用 xml 后要将 application.yml 中的配置去掉，避免冲突. 

​		根据不同的日志系统，按照指定的规则组织配置文件名，并放在 resources 目录下，就能自动被 spring boot 加载：

- Logback：logback-spring.xml, logback-spring.groovy, logback.xml, logback.groovy
- Log4j: log4j-spring.properties, log4j-spring.xml, log4j.properties, log4j.xml
- Log4j2: log4j2-spring.xml, log4j2.xml
- JDK (Java Util Logging)： logging.properties

 想要自定义文件名的可在application.yaml配置：`logging.config`指定配置文件名: 

```yaml
logging:
  config: classpath:logback-spring.xml
```

#### logback 配置文件的组成

 根节点`<configuration>`有 5 个子节点，下面来进行一一介绍。 

`<root>`节点

  root 节点是必选节点，用来指定最基础的日志输出级别，只有一个 level 属性，用于设置打印级别，可选如下：`TRACE`,`DEBUG`,`INFO`,`WARN`,`ERROR`,`ALL`,`OFF`。 

 root 节点可以包含 0 个或多个元素，将`appender`添加进来。如下： 

```xml
<root level="debug">
 <appender-ref ref="console" />
 <appender-ref ref="file" />
</root>
```

`<contextName>`节点

 设置上下文名称，默认为`default`，可通过`%contextName`来打印上下文名称，一般不使用此属性。 

`<property>`节点

 用于定义变量，方便使用。有两个属性：name,value。定义变量后，可以使用`${}`来使用变量。如下： 

```xml
<property name="path" value="./log"/>
<property name="appname" value="app"/>
```

`<appender>`节点

 appender 用来格式化日志输出的节点，这个最重要。有两个属性： 

​					`name`:该本 appender 命名

​					`class`:指定输出策略，通常有两种：控制台输出，文件输出

1. 输出到控制台/按时间输出日志

```xml
<?xml version="1.0" encoding="UTF-8"?>
<configuration debug="false">
    <!--设置存储路径变量-->
    <property name="LOG_HOME" value="./log"/>

    <!--控制台输出appender-->
    <appender name="console" class="ch.qos.logback.core.ConsoleAppender">
        <!--设置输出格式-->
        <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <!--格式化输出：%d表示日期，%thread表示线程名，%-5level：级别从左显示5个字符宽度%msg：日志消息，%n是换行符-->
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{50} - %msg%n</pattern>
            <!--设置编码-->
            <charset>UTF-8</charset>
        </encoder>
    </appender>

    <!--文件输出,时间窗口滚动-->
    <appender name="timeFileOutput" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <!--日志名,指定最新的文件名，其他文件名使用FileNamePattern -->
        <File>${LOG_HOME}/timeFile/out.log</File>
        <!--文件滚动模式-->
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <!--日志文件输出的文件名,可设置文件类型为gz,开启文件压缩-->
            <FileNamePattern>${LOG_HOME}/timeFile/info.%d{yyyy-MM-dd}.%i.log.gz</FileNamePattern>
            <!--日志文件保留天数-->
            <MaxHistory>30</MaxHistory>
            <!--按大小分割同一天的-->
            <timeBasedFileNamingAndTriggeringPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP">
                <maxFileSize>10MB</maxFileSize>
            </timeBasedFileNamingAndTriggeringPolicy>
        </rollingPolicy>

        <!--输出格式-->
        <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <!--格式化输出：%d表示日期，%thread表示线程名，%-5level：级别从左显示5个字符宽度%msg：日志消息，%n是换行符-->
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{50} - %msg%n</pattern>
            <!--设置编码-->
            <charset>UTF-8</charset>
        </encoder>

    </appender>

    <!--指定基础的日志输出级别-->
    <root level="INFO">
        <!--appender将会添加到这个loger-->
        <appender-ref ref="console"/>
        <appender-ref ref="timeFileOutput"/>
    </root>
</configuration>
```

2、设置只输出单个级别

 在 appender 中设置,filter 子节点，在默认级别上再此过滤，配置 onMatch，onMismatch 可实现只输出单个级别 

```xml
<appender ...>
    <filter class="ch.qos.logback.classic.filter.LevelFilter">
        <level>INFO</level>
        <!--接受匹配-->
        <onMatch>ACCEPT</onMatch>
        <!--拒绝不匹配的-->
        <onMismatch>DENY</onMismatch>
    </filter>
</appender>
```



`<logger>`节点

**构成**

  此节点用来设置一个包或具体的某一个类的日志打印级别、以及指定``，有以下三个属性：

- name: 必须。用来指定受此 loger 约束的某个包或者某个具体的类
- level：可选。设置打印级别。默认为 root 的级别。
- addtivity: 可选。是否向上级 loger(也就是 root 节点)传递打印信息。默认为 true。

使用示例如下：

1. 不指定级别，不指定 appender

```
<!-- 控制com.example.service下类的打印，使用root的level和appender -->
<logger name="com.example.service"/>
```

2.指定级别，不指定 appender

```
<!-- 控制com.example.service下类的打印，使用root的appender打印warn级别日志 -->
<logger name="com.example.service" level="WARN"/>
```

3.指定级别，指定 appender

```
<!-- 控制com.example.service下类的打印，使用console打印warn级别日志 -->
<!-- 设置addtivity是因为这里已经指定了appender，如果再向上传递就会被root下的appender再次打印 -->
<logger name="com.example.service" level="WARN" addtivity="false">
    <appender-ref ref="console">
</logger>
```

通过指定 appender 就能将指定的包下的日志打印到指定的文件中。



#### 多环境日志输出

通过设置文件名为-spring 结尾，可分环境配置 logger，示例如下： 

```xml
<configuration>
    <!-- 测试环境+开发环境. 多个使用逗号隔开. -->
    <springProfile name="test,dev">
        <logger name="com.example.demo.controller" level="DEBUG" additivity="false">
            <appender-ref ref="console"/>
        </logger>
    </springProfile>
    <!-- 生产环境. -->
    <springProfile name="prod">
        <logger name="com.example.demo" level="INFO" additivity="false">
            <appender-ref ref="timeFileOutput"/>
        </logger>
    </springProfile>
</configuration>
```

 通过配置`spring.profiles.active`也能做到切换上面的 logger 打印设置 

#### 使用的logback-spring.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<configuration debug="false">
    <!--设置存储路径变量-->
    <property name="LOG_HOME" value="./logs"/>

    <!--控制台输出appender-->
    <appender name="console" class="ch.qos.logback.core.ConsoleAppender">
        <!--设置输出格式-->
        <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <!--格式化输出：%d表示日期，%thread表示线程名，%-5level：级别从左显示5个字符宽度%msg：日志消息，%n是换行符-->
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} %-5level [%thread] %logger{50} - %msg%n</pattern>
            <!--设置编码-->
            <charset>UTF-8</charset>
        </encoder>
    </appender>

    <!--文件输出,时间窗口滚动-->
    <appender name="infoOutput" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <!--只输出INFO-->
        <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <!--过滤 INFO-->
            <level>INFO</level>
            <!--匹配到就禁止-->
            <onMatch>ACCEPT</onMatch>
            <!--没有匹配到就允许-->
            <onMismatch>DENY</onMismatch>
        </filter>

        <!--日志名,指定最新的文件名，其他文件名使用FileNamePattern -->
        <File>${LOG_HOME}/info.log</File>
        <!--文件滚动模式-->
        <rollingPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
            <!--日志文件输出的文件名,可设置文件类型为gz,开启文件压缩-->
            <FileNamePattern>${LOG_HOME}/info.%d{yyyy-MM-dd}.%i.log.gz</FileNamePattern>
            <!--日志文件保留天数-->
            <MaxHistory>30</MaxHistory>
            <!--按大小分割同一天的-->
            <maxFileSize>10MB</maxFileSize>
        </rollingPolicy>

        <!--输出格式-->
        <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <!--格式化输出：%d表示日期，%thread表示线程名，%-5level：级别从左显示5个字符宽度%msg：日志消息，%n是换行符-->
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} %-5level [%thread] %logger{50} - %msg%n</pattern>
            <!--设置编码-->
            <charset>UTF-8</charset>
        </encoder>

    </appender>

    <appender name="errorOutput" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <!--只输出ERROR-->
        <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <!--过滤 ERROR-->
            <level>ERROR</level>
            <!--匹配到就禁止-->
            <onMatch>ACCEPT</onMatch>
            <!--没有匹配到就允许-->
            <onMismatch>DENY</onMismatch>
        </filter>
        <!--日志名,指定最新的文件名，其他文件名使用FileNamePattern -->
        <File>${LOG_HOME}/error.log</File>
        <!--文件滚动模式-->
        <rollingPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
            <!--日志文件输出的文件名,可设置文件类型为gz,开启文件压缩-->
            <FileNamePattern>${LOG_HOME}/error.%d{yyyy-MM-dd}.%i.log.gz</FileNamePattern>
            <!--日志文件保留天数-->
            <MaxHistory>30</MaxHistory>
            <!--按大小分割同一天的-->
            <maxFileSize>10MB</maxFileSize>
        </rollingPolicy>

        <!--输出格式-->
        <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <!--格式化输出：%d表示日期，%thread表示线程名，%-5level：级别从左显示5个字符宽度%msg：日志消息，%n是换行符-->
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} %-5level [%thread] %logger{50} - %msg%n</pattern>
            <!--设置编码-->
            <charset>UTF-8</charset>
        </encoder>

    </appender>


    <root level="info">
        <appender-ref ref="console"/>
        <appender-ref ref="infoOutput"/>
        <appender-ref ref="errorOutput"/>
    </root>

    <!--&lt;!&ndash; 测试环境+开发环境. 多个使用逗号隔开. &ndash;&gt;-->
    <!--<springProfile name="test,dev">-->
        <!--<logger name="com.javastudy" level="DEBUG" additivity="false">-->
            <!--<appender-ref ref="console"/>-->
            <!--<appender-ref ref="infoOutput"/>-->
            <!--<appender-ref ref="errorOutput"/>-->
        <!--</logger>-->
    <!--</springProfile>-->
    <!--&lt;!&ndash; 生产环境. &ndash;&gt;-->
    <!--<springProfile name="prod">-->
        <!--<logger name="com.javastudy" level="INFO" additivity="false">-->
            <!--<appender-ref ref="infoOutput"/>-->
            <!--<appender-ref ref="errorOutput"/>-->
        <!--</logger>-->
    <!--</springProfile>-->
</configuration>
```

# 四、Mybatis

## Mybatis-plus的用法

是一个 [MyBatis](http://www.mybatis.org/mybatis-3/) 的增强工具，在 MyBatis 的基础上只做增强不做改变，为简化开发、提高效率而生。

可参考下面文章

https://my.oschina.net/architectliuyuanyuan/blog/3092799#h3_6

## 介绍

三层架构

- 表现层：是用于展示数据的
- 业务层：是处理数据的
- 持久层：是和数据库交互的

Mybatis是一款优秀的**持久层框架**

特点：

- 简单易学
- 灵活
- sql和代码的分离，提高了可维护性
- 提供映射标签，支持对象与数据库的orm字段关系映射
- 提供对象关系映射标签，支持对象关系组件维护
- 提供xml标签，支持编写动态sql。

可参考项目结构：

![image-20200326091606596](image\springboot笔记\image-20200326091606596.png)

## 入门

搭建环境、导入Mybatis、编写代码、测试

### 搭建环境

1. 新建普通maven项目

2. 删除src

3. 导入依赖

   ```xml
       <dependencies>
           <!--mybatis-->
           <dependency>
               <groupId>org.projectlombok</groupId>
               <artifactId>lombok</artifactId>
               <version>1.18.12</version>
           </dependency>
   
           <dependency>
               <groupId>org.mybatis</groupId>
               <artifactId>mybatis</artifactId>
               <version>3.5.4</version>
           </dependency>
   
           <!--oracle-->
           <dependency>
               <groupId>com.oracle.ojdbc</groupId>
               <artifactId>ojdbc8</artifactId>
               <version>19.3.0.0</version>
           </dependency>
   
           <!--  解决java.sql.SQLException: 不支持的字符集 (在类路径中添加 orai18n.jar): ZHS16GBK-->
           <dependency>
               <groupId>cn.easyproject</groupId>
               <artifactId>orai18n</artifactId>
               <version>12.1.0.2.0</version>
           </dependency>
   
           <!--测试框架junit-->
           <dependency>
               <groupId>junit</groupId>
               <artifactId>junit</artifactId>
               <version>4.13</version>
           </dependency>
       </dependencies>
   ```

### 创建一个模块

   - 编写mybatis核心配置文件，在resources下新建mybatis-config.xml文件

     ```xml
     <?xml version="1.0" encoding="UTF-8" ?>
     <!DOCTYPE configuration
             PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
             "http://mybatis.org/dtd/mybatis-3-config.dtd">
     <configuration>
         <environments default="development">
             <environment id="development">
                 <transactionManager type="JDBC"/>
                 <dataSource type="POOLED">
                     <property name="driver" value="oracle.jdbc.OracleDriver"/>
                     <property name="url" value="jdbc:oracle:thin:@192.168.1.154:1521:oss"/>
                     <property name="username" value="sqmdb"/>
                     <property name="password" value="Uar_2016"/>
                 </dataSource>
             </environment>
         </environments>
     </configuration>
     ```

   - 编写mybatis工具类

     ```java
     /**
      * sqlSessionFactory  -->sqlSession
      */
     public class MybaisUtils {
         private static SqlSessionFactory sqlSessionFactory;
     
         static {
             try {
                 //使用Mybate，第一步：获取sqlSessionFactory对象
                 String resource = "mybatis-config.xml";
                 InputStream inputStream = Resources.getResourceAsStream(resource);
                 sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
             } catch (IOException e) {
                 e.printStackTrace();
             }
         }
     
         //既然有了 SqlSessionFactory ，顾名思义，我们就可以从中获得 SqlSession 的实例了。
         // SqlSession 完全包含了面向数据库执行 SQL 命令所需的所有方法。
         public static SqlSession getSqlSession(){
             // true表示自动提交事务
             return sqlSessionFactory.openSession(true);
         }
     }
     ```

###    编写代码

- 实体类

  ```java
  @Data
  public class User {
      private Integer userid;
      private String username;
      private Integer age;
  }
  ```

- Dao接口

  ```java
  public interface UserDao {
      List<User> getUserList();
  }
  ```

- 接口实现类有原来的`UserDaoImpl`转变为一个`Mapper`配置文件

  ```xml
  <?xml version="1.0" encoding="UTF-8" ?>
  <!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
  <!--namespace绑定一个对应的Dao/Mapper接口-->
  <mapper namespace="com.wxl.dao.UserDao">
      <select id="getUserList" resultType="com.wxl.pojo.User">
          select * from mybatis.wxl1
      </select>
  </mapper>
  ```

### 测试

  ```java
  public class UserDaoTest {
  
      @Test
      public void test(){
          // 第一步：获取SqlSession对象
          SqlSession sqlSession =  MybaisUtils.getSqlSession();
          // 第二步：获取sql脚本
          UserDao userDao = sqlSession.getMapper(UserDao.class);
  
          List<User> users = userDao.getUserList();
  
          for (User u : users) {
              System.out.println(u);
          }
  
          sqlSession.close();
      }
  }
  ```

  会遇到的错误：

  1. 配置文件没有注册

     确定mybatis-config.xml文件有

     ```xml
     <!--每一个Mapper.xml都需要在Mybatis核心配置文件中注册-->
     <mappers>
         <mapper resource="com/wxl/dao/UserMapper.xml"/>
     </mappers>
     ```

     确定pom.xml文件有

     ```xml
     <build>
         <!--这样也可以把所有的xml文件，打包到相应位置。 -->
         <resources>
             <resource>
                 <directory>src/main/resources</directory>
                 <includes>
                     <include>**/*.properties</include>
                     <include>**/*.xml</include>
                     <include>**/*.tld</include>
                 </includes>
                 <filtering>false</filtering>
             </resource>
             <resource>
                 <directory>src/main/java</directory>
                 <includes>
                     <include>**/*.properties</include>
                     <include>**/*.xml</include>
                     <include>**/*.tld</include>
                 </includes>
                 <filtering>false</filtering>
             </resource>
         </resources>
     </build>
     ```

  2. 绑定接口错误

  3. 方法名不对

  4. 返回类型不对

## CRUD

1. namespace

   namespace中的包名要和Dao/mapper接口的包名一致！

2. select

   选择，查询语句

   - id: 就是对应的namespace中的方法名；
   - resultType：Sql语句执行的返回值类型；
   - parameterType: 查询参数类型；

3. 接口类

   ```java
   public interface UserMapper {
   
       List<User> getUserList();
   
       User getUserById(int id);
   
       int insertUser(User user);
   
       int updateUser(@Param("id")int id, @Param("name")String name);
   
       int deleteUser(int id);
   
       // 万能的map
       int addUser(Map<String,Object> map);
   
       // 模糊查询
       List<User>  getUserLike(String name);
   }
   ```

4. mapper
   ```xml
   <?xml version="1.0" encoding="UTF-8" ?>
   <!DOCTYPE mapper
           PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
           "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
   <!--namespace绑定一个对应的Dao/Mapper接口-->
   <mapper namespace="com.wxl.dao.UserMapper">
       <insert id="insertUser" parameterType="com.wxl.pojo.User">
           insert into wxl1(userid, username, age) values (#{userid}, #{username}, #{age})
       </insert>
   
       <select id="getUserList" resultType="com.wxl.pojo.User">
           select * from wxl1
       </select>
   
       <select id="getUserById" resultType="com.wxl.pojo.User" parameterType="int">
           select * from wxl1 where userid = #{id}
       </select>
   
       <update id="updateUser">
           update wxl1 set username = #{name} where userid = #{id}
       </update>
   
       <delete id="deleteUser" parameterType="int">
           delete from wxl1 where userid = #{id}
       </delete>
   
       <!--map参数中的key可以任意定义，比对象灵活-->
       <insert id="addUser" parameterType="map">
           insert into wxl1 (userid, username, age) values(#{addid}, #{addname}, #{addage})
       </insert>
   
       <select id="getUserLike" resultType="com.wxl.pojo.User">
           select * from wxl1 where username like #{name}
       </select>
   </mapper>
   ```
   
5. 注意事项

   - 增、改、删要有提交事务`sqlSession.commit();`

   - mapper的sql语句结尾不能有分号
   - like查询建议吧%放到传参里

## 配置解析

### 1、核心配置文件

MyBatis 的配置文件包含了会深深影响 MyBatis 行为的设置（settings）和属性
（properties）信息。文档的顶层结构如下：

configuration 配置

- properties 属性

- settings 设置

- typeAliases 类型别名

- typeHandlers 类型处理器

- objectFactory 对象工厂

-  plugins 插件

- environments 环境

  - environment 环境变量

  - transactionManager 事务管理器

  - dataSource 数据源

- databaseIdProvider 数据库厂商标识

-  mappers 映射器

### 2、环境配置（environments）

MyBatis 可以配置成适应多种环境，这种机制有助于将 SQL 映射应用于多种数据库之中， 现实情况下有多种理由需要这么做。例如，开发、测试和生产环境需要有不同的配置；或者想在具有相同 Schema 的多个生产数据库中使用相同的 SQL 映射。还有许多类似的使用场景。

注意一些关键点:

- 默认使用的环境 ID（比如：default="development"）。
- 每个 environment 元素定义的环境 ID（比如：id="development"）。
- 事务管理器的配置（比如：type="JDBC"）。
- 数据源的配置（比如：type="POOLED"）。

默认环境和环境 ID 顾名思义。 环境可以随意命名，但务必保证默认的环境 ID 要匹配其中一个环境 ID。

### 3、属性（properties）

这些属性可以在外部进行配置，并可以进行动态替换。你既可以在典型的 Java 属性文件中配置这些属性，也可以在 properties 元素的子元素中设置。例如：

编写一个配置文件：

```properties
driver=oracle.jdbc.OracleDriver
url=jdbc:oracle:thin:@192.168.1.154:1521:oss
username=sqmdb
password=Uar_2016
```

在核心配置文件中引入

```xml
<properties resource="db.properties">
</properties>
```

### 4、类型别名（typeAliases）

`第一种：`类型别名可为 Java 类型设置一个缩写名字。 它仅用于 XML 配置，意在降低冗余的全限定类名书写。

```xml
<typeAliases>
    <typeAlias type="com.wxl.pojo.User" alias="User"/>
</typeAliases>
```

当这样配置时，`User` 可以用在任何使用 `com.wxl.pojo.User` 的地方。

`第二种：`也可以指定一个包名，MyBatis 会在包名下面搜索需要的 Java Bean，比如:

默认别名就是类的类名，建议类名小写

```xml
<typeAliases>
    <package name="com.wxl.pojo"/>
</typeAliases>
```

在实体类比较少的是否，使用第一种方式；

如果实体类很多，建议使用第二种；

第一种可以DIY别名，第二种如果也自定义名字可以在实体类上加@Alias("myuser")注解

### 5、设置（settings）

这是 MyBatis 中极为重要的调整设置，它们会改变 MyBatis 的运行时行为。 下表描述了设置中各项设置的含义、默认值等。

| 设置名             | 描述                                                         | 有效值                                                       | 默认值 |
| :----------------- | :----------------------------------------------------------- | :----------------------------------------------------------- | :----- |
| cacheEnabled       | 全局性地开启或关闭所有映射器配置文件中已配置的任何缓存。     | true \| false                                                | true   |
| lazyLoadingEnabled | 延迟加载的全局开关。当开启时，所有关联对象都会延迟加载。 特定关联关系中可通过设置 `fetchType` 属性来覆盖该项的开关状态。 | true \| false                                                | false  |
| logImpl            | 指定 MyBatis 所用日志的具体实现，未指定时将自动查找。        | SLF4J \| LOG4J \| LOG4J2 \| JDK_LOGGING \| COMMONS_LOGGING \| STDOUT_LOGGING \| NO_LOGGING | 未设置 |

### 6、映射器（mapper）

方式一：

```xml
<mappers>
    <!-- 使用相对于类路径的资源引用 -->
    <mapper resource="com/wxl/dao/UserMapper.xml"/>
</mappers>
```

方式二：

```xml
<mappers>
    <!-- 使用映射器接口实现类的完全限定类名 -->
    <mapper class="com.wxl.dao.UserMapper"/>
</mappers>
```

方式三：

```xml
<!-- 将包内的映射器接口实现全部注册为映射器 -->
<mappers>
  <package name="com.wxl.dao"/>
</mappers>
```

方式二和方式三需要注意：

- 接口和他的Mapper配置文件必须同名！
- 接口和他的Mapper配置文件必须在同一个包下

## 输出LOG4J日志

先在mybatis-config.xml里配置seting

```xml
<settings>
    <setting name="logImpl" value="LOG4J "/>
</settings>
```

在pom.xml里引入包

```xml
<dependency>
    <groupId>log4j</groupId>
    <artifactId>log4j</artifactId>
    <version>1.2.17</version>
</dependency>
```

在resources下创建log4j.properties文件

```properties
# 日志输出级别（DEBUG）和输出位置（stdout, A）
log4j.rootLogger=DEBUG, stdout, A

# 日志输出位置为控制台
log4j.appender.stdout=org.apache.log4j.ConsoleAppender
log4j.appender.stdout.Target = System.out
log4j.appender.stdout.layout=org.apache.log4j.PatternLayout
log4j.appender.stdout.layout.ConversionPattern=[QC] %p [%t] %C.%M(%L) | %m%n

# debug日志输出位置为文件
log4j.appender.A = org.apache.log4j.DailyRollingFileAppender
log4j.appender.A.File = ./logs/debug.log
log4j.appender.A.Append = true
log4j.appender.A.Threshold = DEBUG
log4j.appender.A.DatePattern ='.'yyyy-MM-dd
log4j.appender.A.layout = org.apache.log4j.PatternLayout
log4j.appender.A.layout.ConversionPattern = %d %l - %m%n

# 定义相应包路径下的日志输出级别
log4j.logger.org.mybatis = DEBUG
log4j.logger.java.sql = DEBUG
log4j.logger.java.sql.Statement = DEBUG
log4j.logger.java.sql.ResultSet = DEBUG
log4j.logger.java.sql.PreparedStatement = DEBUG


```

## 分页

### 普通分页查询

oarcle分页查询限制要比mysql的复杂

接口：

```java
List<User> getUserListByPage(@Param("start") int start, @Param("end") int end);
```

mapper脚本：

```xml
  <select id="getUserListByPage" resultType="user" parameterType="map">
      select * from(
          select t.*,rownum rn from(
              select *
                from wxl1 c
          ) t
          where rownum &lt;= #{end}
      )
where rn &gt;= #{start}
  </select>
```

测试类：

```java
@Test
public void test(){
    SqlSession sqlSession = MybaisUtils.getSqlSession();
    UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
    int pagenum = 1;
    int pagesize = 2;
    int start =(pagenum-1)*pagesize+1;
    int end = pagenum*pagesize;
    List<User> users = userMapper.getUserListByPage(start,end);
    System.out.println(JSONArray.toJSONString(users));
    sqlSession.close();
}
```

### 插件分页查询

MyBatis 分页插件 PageHelper：https://pagehelper.github.io/

#### 1.引入pagehelper依赖

```xml
<dependency>
    <groupId>com.github.pagehelper</groupId>
    <artifactId>pagehelper</artifactId>
    <version>5.1.2<ersion>
</dependency>
```

#### 2.配置applicationContext.xml文件

> 在spring的`sqlsessionfactory`的`bean`中增加一个分页拦截器属性

```xml
<bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
    <property name="plugins">
                <array>
                    <bean class="com.github.pagehelper.PageInterceptor">
                        <property name="properties">
                            <value>
                                <!-- 这里设定你的数据库类型 -->
                               helperDialect=mysql
                            </value>
                        </property>
                    </bean>
                </array>
    </property>
</bean>
```

#### 3.调用PageHelper的方法

> 在service方法中调用PageHelper的静态方法`startPage`（注意**一定要在实际查询数据库之前调用该方法**）,传入需要查询的页号和每页大小，返回PageHelper插件提供的PageInfo对象。即可自动完成数据库物理分页，无须在你的sql语句中手工加`limit`子句

![startPage方法](image\springboot笔记\23432423543.png)

#### 4. PageInfo的结构

> 关于PageInfo的结构请参看源码，这里通过返回的json来展示。根据需要取PageInfo对象的相应属性即可。

![PageInfo的结构](image\springboot笔记\123132312312.jps)

## 注解开发

简单的脚本可以使用注解简化开发

1、接口类

```java
public interface UserMapper {
    @Select("select * from wxl1")
    List<User>  getUsers();
}
```

2、在mybatis-config.xml引入接口

```xml
<mappers>
    <mapper class="com.wxl.dao.UserMapper"/>
</mappers>
```

3、测试

```java
@Test
public void test(){
    SqlSession sqlSession = MybaisUtils.getSqlSession();
    UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
    List<User> users = userMapper.getUsers();
    for(User u : users){
        System.out.println(u);
    }
}
```



## 动态SQL

MyBatis 的强大特性之一便是它的动态 SQL。如果你有使用 JDBC 或其他类似框架的经验，你就能体会到根据不同条件拼接 SQL 语句有多么痛苦。拼接的时候要确保不能忘了必要的空格，还要注意省掉列名列表最后的逗号。利用动态 SQL 这一特性可以彻底摆脱这种痛苦。

> 所谓动态sql，本质还是SQL，只是我们可以在SQL中执行一些逻辑代码

### if

动态 SQL 通常要做的事情是有条件地包含 where 子句的一部分。比如:

```xml
<select id="findActiveBlogLike"
     resultType="Blog">
  SELECT * FROM BLOG WHERE state = ‘ACTIVE’ 
  <if test="title != null">
    AND title like #{title}
  </if>
  <if test="author != null and author.name != null">
    AND author_name like #{author.name}
  </if>
</select>
```

### choose, when, otherwise

有些时候，我们不想用到所有的条件语句，而只想从中择其一二。针对这种情况，MyBatis 提供了 choose 元素，它有点像 Java 中的 switch 语句。

还是上面的例子，但是这次变为提供了"title"就按"title"查找，提供了"author"就按"author"查找，若两者都没有提供，就返回所有符合条件的BLOG（实际情况可能是由管理员按一定策略选出BLOG列表，而不是返回大量无意义的随机结果）。

```xml
<select id="findActiveBlogLike"
     resultType="Blog">
  SELECT * FROM BLOG WHERE state = ‘ACTIVE’
  <choose>
    <when test="title != null">
      AND title like #{title}
    </when>
    <when test="author != null and author.name != null">
      AND author_name like #{author.name}
    </when>
    <otherwise>
      AND featured = 1
    </otherwise>
  </choose>
</select>
```

### trim, where, set

where 元素知道只有在一个以下的if条件有值的情况下才去插入"WHERE"子句。而且，若最后的内容是"AND"或"OR"开头的，where 元素也知道如何将他们去除。

```xml
<select id="findActiveBlogLike"
     resultType="Blog">
  SELECT * FROM BLOG 
  <where> 
    <if test="state != null">
         state = #{state}
    </if> 
    <if test="title != null">
        AND title like #{title}
    </if>
    <if test="author != null and author.name != null">
        AND author_name like #{author.name}
    </if>
  </where>
</select>
```

如果 where 元素没有按正常套路出牌，我们还是可以通过自定义 trim 元素来定制我们想要的功能。比如，和 where 元素等价的自定义 trim 元素为：

```xml
<trim prefix="WHERE" prefixOverrides="AND |OR ">
  ... 
</trim>
```

prefixOverrides 属性会忽略通过管道分隔的文本序列（注意此例中的空格也是必要的）。它带来的结果就是所有在 prefixOverrides 属性中指定的内容将被移除，并且插入 prefix 属性中指定的内容。

类似的用于动态更新语句的解决方案叫做 set。set 元素可以被用于动态包含需要更新的列，而舍去其他的。比如：

```xml
<update id="updateAuthorIfNecessary">
  update Author
    <set>
      <if test="username != null">username=#{username},</if>
      <if test="password != null">password=#{password},</if>
      <if test="email != null">email=#{email},</if>
      <if test="bio != null">bio=#{bio}</if>
    </set>
  where id=#{id}
</update>
```

这里，set 元素会动态前置 SET 关键字，同时也会消除无关的逗号，因为用了条件语句之后很可能就会在生成的赋值语句的后面留下这些逗号。

### foreach

动态 SQL 的另外一个常用的必要操作是需要对一个集合进行遍历，通常是在构建 IN 条件语句的时候。比如：

```xml
<select id="selectPostIn" resultType="domain.blog.Post">
  SELECT *
  FROM POST P
  WHERE ID in
  <foreach item="item" index="index" collection="list"
      open="(" separator="," close=")">
        #{item}
  </foreach>
</select>
```

## 缓存

### 介绍

一次查询的结果，给他暂存在一个可以直接取到的地方，即放到内存中。

1. 什么是缓存【Cahe】？
   - 存在内存中的临时数据
   - 将用户经常查询的数据放到缓存中，用户去查询数据就不用从磁盘上（关系型数据库数据文件）查询，从缓存中查询，从而提高查询效率，解决了高并发系统的性能问题。
2. 为什么使用缓存？
   - 减少和数据库的交互次数，减少系统开销，提高系统效率。
3. 什么样的数据能使用缓存？
   - 经常查询并且不经常改动的数据。

### Mybatis缓存

MyBatis 内置了一个强大的事务性查询缓存机制，它可以非常方便地配置和定制。 为了使它更加强大而且易于配置，我们对 MyBatis 3 中的缓存实现进行了许多改进。

MyBatis系统中默认定义了两级缓存：**一级缓存**和**二级缓存**

-  默认情况下，只有一级缓存开启。（SqlSession级别的缓存，也称为本地缓存）
- 二级缓存需要手动开启和配置，他是基于namespace级别的缓存。
- 为了提高扩展性，MyBatis定义了缓存接口Cache。我们可以通过实现Cache接口来自定义二级缓存

### 一级缓存

也叫本地缓存：SqlSession

- 默认一级缓存都是开启的
- 与数据库同一次会话期间查询到的数据会放在本地缓存中。
- 以后如果需要获取相同的数据，直接从缓存中拿，没必要再去查询数据库。

**手动清理缓存**

```java
sqlSession.clearCache();
```

### 二级缓存

也叫全局缓存，一级缓存作用域太低了，所以诞生了二级缓存

基于namespace级别的缓存，一个名称空间，对应一个二级缓存

工作机制

- 一个会话查询一条数据，这个数据就会被放在当前会话的一级缓存中；
- `需要当前会话关闭了，一级缓存中的数据被保存到二级缓存中`；
- 新的会话查询信息，就可以从二级缓存中获取内容。

1、在mybatis-config.xml中添加setting开启全局缓存：

```xml
<!--开启全局缓存-->
<setting name="cacheEnabled" value="true"/>
```

2、启动二级缓存，只需要在你的 SQL 映射文件中添加一行：

```xml
<cache/>
```

基本上就是这样。这个简单语句的效果如下:

- 映射语句文件中的**所有 select 语句的结果将会被缓存**。
- 映射语句文件中的**所有 insert、update 和 delete 语句会刷新缓存**。
- 缓存会使用最近最少使用算法（LRU, Least Recently Used）算法来清除不需要的缓存。
- 缓存不会定时进行刷新（也就是说，没有刷新间隔）。
- 缓存会保存列表或对象（无论查询方法返回哪种）的 1024 个引用。
- 缓存会被视为读/写缓存，这意味着获取到的对象并不是共享的，可以安全地被调用者修改，而不干扰其他调用者或线程所做的潜在修改。

**提示** 缓存只作用于 cache 标签所在的映射文件中的语句。如果你混合使用 Java API 和 XML 映射文件，在共用接口中的语句将不会被默认缓存。你需要使用 @CacheNamespaceRef 注解指定缓存作用域。

这些属性可以通过 cache 元素的属性来修改。比如：

```xml
<cache
  eviction="FIFO"
  flushInterval="60000"
  size="512"
  readOnly="true"/>
```

这个更高级的配置创建了一个 FIFO 缓存，每隔 60 秒刷新，最多可以存储结果对象或列表的 512 个引用，而且返回的对象被认为是只读的，因此对它们进行修改可能会在不同线程中的调用者产生冲突。

可用的清除策略有：

- `LRU` – 最近最少使用：移除最长时间不被使用的对象。
- `FIFO` – 先进先出：按对象进入缓存的顺序来移除它们。
- `SOFT` – 软引用：基于垃圾回收器状态和软引用规则移除对象。
- `WEAK` – 弱引用：更积极地基于垃圾收集器状态和弱引用规则移除对象。

默认的清除策略是 LRU。



### 测试

1、测试一个SqlSession中查询两次相同记录

```java
@Test
public void test(){
    SqlSession sqlSession = MybaisUtils.getSqlSession();
    UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
    User user = userMapper.queryUserById(1);
    System.out.println(user);

    System.out.println("===================");

    User user1 = userMapper.queryUserById(1);
    System.out.println(user1);

    System.out.println(user == user1);
    
    sqlSession.close();
}
```

2、测试同一个mapper里启用二级缓存

```java
@Test
public void test(){
    SqlSession sqlSession = MybaisUtils.getSqlSession();
    SqlSession sqlSession2 = MybaisUtils.getSqlSession();
    UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
    UserMapper userMapper2 = sqlSession2.getMapper(UserMapper.class);
    
    User user = userMapper.queryUserById(1);
    System.out.println(user);
    sqlSession.close();     // 关闭后把一级缓存放到二级缓存

    System.out.println("===================");

    User user1 = userMapper2.queryUserById(1);
    System.out.println(user1);
    System.out.println(user == user1);

    //sqlSession.close();   // 放在这里关闭不会把一级缓存放到二级缓存
    sqlSession2.close();
}
```

### 查询缓存流程图

![image-20200327172238359](image\springboot笔记\image-20200327172238359.png)



### 自定义缓存ehcache

**mybatis-ehcache实例**

1. 先导包

   ```xml
   <dependency>
       <groupId>org.mybatis.caches</groupId>
       <artifactId>mybatis-ehcache</artifactId>
       <version>1.2.0</version>
   </dependency>
   ```

2. 配置ehcache.xml文件

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <ehcache xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:noNamespaceSchemaLocation="http://ehcache.org/ehcache.xsd">
   
       <!-- 磁盘缓存位置 -->
       <diskStore path="java.io.tmpdir/ehcache"/>
   
       <!-- 默认缓存 -->
       <defaultCache
               maxEntriesLocalHeap="10000"
               eternal="false"
               timeToIdleSeconds="120"
               timeToLiveSeconds="120"
               maxEntriesLocalDisk="10000000"
               diskExpiryThreadIntervalSeconds="120"
               memoryStoreEvictionPolicy="LRU">
           <persistence strategy="localTempSwap"/>
       </defaultCache>
   
       <!-- helloworld缓存 -->
       <cache name="HelloWorldCache"
              maxElementsInMemory="1000"
              eternal="false"
              timeToIdleSeconds="5"
              timeToLiveSeconds="5"
              overflowToDisk="false"
              memoryStoreEvictionPolicy="LRU"/>
   
   </ehcache>
   ```

3. mapper文件中引入

   ```xml
   <cache type="org.mybatis.caches.ehcache.EhcacheCache"/>
   ```



## EhCache缓存

### **介绍**

EhCache 是一个`纯Java`的进程内`缓存框架`，具有快速、精干等特点，是Hibernate中默认CacheProvider。Ehcache是一种广泛使用的开源Java分布式缓存。主要面向通用缓存,Java EE和轻量级容器。它具有`内存`和`磁盘`存储，缓存加载器,缓存扩展,缓存异常处理程序,一个gzip缓存servlet过滤器,支持REST和SOAP api等特点。

### **特性**

- 快速、简单
- 多种`缓存策略`
- 缓存数据有两级：`内存和磁盘`，因此无需担心`容量问题`
- 缓存数据会在虚拟机`重启`的过程中`写入磁盘`
- 可以通过`RMI`、可插入API等方式进行`分布式缓存`
- 具有缓存和缓存管理器的侦听接口
- 支持`多`缓存管理器`实例`，以及一个实例的`多个缓存区域`
- 提供`Hibernate`的缓存实现

### **集成**

可以单独使用，一般在第三方库中被用到的比较多（如mybatis、shiro等）ehcache 对`分布式支持不够好`，多个节点`不能同步`，通常和redis一块使用

### **灵活性**

ehcache具备对象`api接口`和`可序列化api接口`
 `不能序列化的对象`可以使用出磁盘存储外`ehcache`的所有功能
 支持基于Cache和基于Element的过期策略，每个Cache的存活时间都是可以设置和控制的。
 提供了LRU、LFU和FIFO缓存淘汰算法，Ehcache 1.2引入了最少使用和先进先出缓存淘汰算法，构成了完整的缓存淘汰算法。
 提供内存和磁盘存储，Ehcache和大多数缓存解决方案一样，提供高性能的内存和磁盘存储。
 动态、运行时缓存配置，存活时间、空闲时间、内存和磁盘存放缓存的最大数目都是可以在运行时修改的。

### **应用持久化**

在`vm重启`后，持久化到磁盘的存储可以`复原数据`
 Ehache是第一个引入缓存数据持久化存储的开源java缓存框架，缓存的数据可以在机器重启后从磁盘上重新获得
 根据需要将缓存刷到磁盘。将缓存条目`刷到磁盘`的操作可以通过`cache.fiush`方法执行,这大大方便了ehcache的使用

### **ehcache 和 redis 比较**

- ehcache直接在jvm虚拟机中缓存，`速度快`，效率高；但是缓存`共享麻烦`，集群分布式应用不方便。
- redis是通过socket访问到缓存服务，效率比ecache低，比数据库要快很多，
   处理集群和分布式缓存方便，有成熟的方案。如果是`单个应用`或者对`缓存访问要求很高`的应用，用ehcache。如果是大型系统，存在缓存共享、分布式部署、`缓存内容很大`的，建议用redis。

### 实例

1. 导入依赖

   ```xml
     <dependencies>
       <dependency>
           <groupId>net.sf.ehcache</groupId>
           <artifactId>ehcache</artifactId>
           <version>2.10.2</version>
       </dependency>
       <dependency>
           <groupId>junit</groupId>
           <artifactId>junit</artifactId>
           <version>4.12</version>
           <scope>test</scope>
       </dependency>
     </dependencies>
   <!-- 引入springboot-cache注解-->
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-cache</artifactId>
               <version>2.2.4.RELEASE</version>
           </dependency>
   ```

2. 配置文件

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <ehcache xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:noNamespaceSchemaLocation="http://ehcache.org/ehcache.xsd">
   
     <!-- 磁盘缓存位置 -->
     <diskStore path="java.io.tmpdir/ehcache"/>
   
     <!-- 默认缓存 -->
     <defaultCache
             maxEntriesLocalHeap="10000"
             eternal="false"
             timeToIdleSeconds="120"
             timeToLiveSeconds="120"
             maxEntriesLocalDisk="10000000"
             diskExpiryThreadIntervalSeconds="120"
             memoryStoreEvictionPolicy="LRU">
       <persistence strategy="localTempSwap"/>
     </defaultCache>
   
     <!-- helloworld缓存 -->
     <cache name="HelloWorldCache"
            maxElementsInMemory="1000"
            eternal="false"
            timeToIdleSeconds="5"
            timeToLiveSeconds="5"
            overflowToDisk="false"
            memoryStoreEvictionPolicy="LRU"/>
   </ehcache>
   ```

3. 测试类

   ```java
   @Test
   public void test1(){
       // 1、创建缓存管理器
       CacheManager cacheManager = CacheManager.create("./src/main/resources/ehcache.xml");
       // 2、获取缓存对象
       Cache cache = cacheManager.getCache("HelloWorldCache");
       // 3、创建元素
       Element element = new Element("key1", "value1");
       // 4、将元素添加到缓存
       cache.put(element);
       // 5、获取缓存
       Element value = cache.get("key1");
       System.out.println(value);
       System.out.println(value.getObjectValue());
   
       // 6、删除元素
       cache.remove("key1");
   
       User user = new User(1,"小米", 10, "xiaomi@126.com");
       Element userElement = new Element("xm", user);
       cache.put(userElement);
       Element e = cache.get("xm");
       System.out.println(e.getObjectValue());
   
       System.out.println(cache.getSize());
   
       // 7、刷新缓存
       cache.flush();
       // 8、关闭缓存管理器
       cacheManager.shutdown();
   }
   ```

4. 配置文件说明

   diskStore
   
   ​	path ：指定磁盘存储的位置
   

defaultCache

   ​	默认的缓存

   cache

   ​	自定的缓存，当自定的配置不满足实际情况时可以通过自定义（可以包含多个cache节点）

   ​	name : 缓存的名称，可以通过指定名称获取指定的某个Cache对象

   ​	maxElementsInMemory ：内存中允许存储的最大的元素个数，0代表无限个
   ​		clearOnFlush：内存数量最大时是否清除。
   ​		eternal ：设置缓存中对象是否为永久的，如果是，超时设置将被忽略，对象从不过期。根据存储数据的不同，例如一些静态不变的数据如省市区等可以设置为永不过时
   ​		timeToIdleSeconds ： 设置对象在失效前的允许闲置时间（单位：秒）。仅当eternal=false对象不是永久有效时使用，可选属性，默认值是0，也就是可闲置时间无穷大。
      	timeToLiveSeconds ：缓存数据的 生存时间（TTL），也就是一个元素从构建到消亡的最大时间间隔值，这只能在元素不是永久驻留时有效，如果该值是0就意味着元素可以停顿无穷长的时间。(和上面的两者取最小值)
      	overflowToDisk：内存不足时，是否启用磁盘缓存。
     	 maxEntriesLocalDisk：当内存中对象数量达到maxElementsInMemory时，Ehcache将会对象写到磁盘中。
      	maxElementsOnDisk：硬盘最大缓存个数。
      	diskSpoolBufferSizeMB：这个参数设置DiskStore（磁盘缓存）的缓存区大小。默认是30MB。每个Cache都应该有自己的一个缓冲区。
     	 diskPersistent：是否在VM重启时存储硬盘的缓存数据。默认值是false。
      	diskExpiryThreadIntervalSeconds：磁盘失效线程运行时间间隔，默认是120秒。
      	memoryStoreEvictionPolicy：当达到maxElementsInMemory限制时，Ehcache将会根据指定的策略去清理内存。默认策略是LRU（最近最少使用）。你可以设置为FIFO（先进先出）或是LFU（较少使用）。这里比较遗憾，Ehcache并没有提供一个用户定制策略的接口，仅仅支持三种指定策略，感觉做的不够理想。

5. 数据都放到磁盘里去了

   ```xml
     <!-- helloworld缓存 -->
     <cache name="HelloWorldCache"
            maxElementsInMemory="1" //设置成1，overflowToDisk设置成true，只要有一个缓存元素，就直接存到硬盘上去
            eternal="false"
            timeToIdleSeconds="50000"
            timeToLiveSeconds="50000"
            overflowToDisk="true"
            diskPersistent="true" //设置成true表示缓存虚拟机重启期数据 
            memoryStoreEvictionPolicy="LRU"/>
   ```

   

6. spring持久化测试情况

7. 


# 五、Web开发

1. 使用SpringBoot
   * 创建SpringBoot应用，选中需要开发的模块
   * SpringBoot已经默认将这些场景配置好了，只需要在配置文件中指定少量配置就可以运行起来
   * 自己编写业务代码
2. 

```
xxxxAutoConfiguration:给容器中自动配置组件；
xxxxProperties:配置类来封装配置文件的内容；

```

## 静态资源访问

1. 在springboot，我们可以用以下方式处理静态资源
   - webjars: `localhost:8080/webjars`
   - public, static, /**, resources: `localhost:8080/`
2. 优先级：resources>static(默认)>public



## controller层

### Controller的使用

| @Controller     | 处理http请求                                                 |
| --------------- | ------------------------------------------------------------ |
| @RestController | Spring4之后新加的注解，原来返回json需要@ResponseBody配合@Controller |
| @RequestMapping | 配置url映射                                                  |


项目前后台交互的话 无非两种方式

一种普通整体页面提交，比如form提交；

还有一种局部刷新，或者叫做异步刷新，ajax提交；

@Controller就是整体页面刷新提交的处理注解

@RestController就是ajax提交，一般返回json格式

### @RequestMapping

基本用法
@RequestMapping注解告知Spring这个类或者函数映射到哪个URL，通常@RequestMapping参数有：

```java
@RequestMapping(
		path = { "/test" },
		params = { "name", "userId"},
		method = { RequestMethod.GET},
		consumes = {"text/plain", "application/"},
        produces = "text/plain",
        headers = "content-type=text/"
)
```

其中，path表示访问路径，params表示URL携带的参数，method表示请求方式
（1）@RequestMapping参数为空时，表示该函数或者类映射剩余所有页面，也就是404页面
（2）@RequestMapping不指定参数名时，默认为path参数
（3）@RequestMapping的path参数可以是个变量，通过{}来标识。
（4）params:指定request中必须包含某些参数值是，才让该方法处理。
			headers:指定request中必须包含某些指定的header值，才能让该方法处理请求。
			value:指定请求的实际地址，指定的地址可以是URI Template 模式
			method:指定请求的method类型， GET、POST、PUT、DELETE等
			consumes:指定处理请求的提交内容类型（Content-Type），如application/json,text/html;
			produces:指定返回的内容类型，仅当request请求头中的(Accept)类型中包含该指定类型才返回
（5）@PathVaiable

###  @ResposeBody

正常情况下，Controller类中函数返回是一个模板对象（页面），但是有时候我们希望返回值数值型（字符串或者Json格式数据），这时就需要添加@ResponseBody注解
@ResponseBody注解可以添加到类或者函数上

### @RestController、@GetMapping 和@PostMapping

这三个注解都是组合注解，其中
@RestController相当于@Controller + @ResposeBody
@GetMapping相当于@RequestMapping（method=RequestMethod.GET）
@PostMapping相当于@RequestMapping（method=RequestMethod.POST）

### @PathVaiable

用于读取URL中的数据,若在path当中使用{XXX}进行传递参数，则要使用@pathVariable("XXX")进行获取

```java
    @RequestMapping(path = {"/user/{userId}"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String userIndex(Model model, @PathVariable("userId") int userId) {
        model.addAttribute("vos", getNews(userId, 0, 10));
        return "home";
    }
```

### @RequestParam

```java
 @RequestMapping(path = {"/", "/index"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String index(Model model,
                        @RequestParam(value = "pop", defaultValue = "0") int pop) {
        model.addAttribute("vos", getNews(0, 0, 10));
        model.addAttribute("pop", pop);
        return "home";
    }
```

使用@RequestParam进行request内容的获取，通过指定键值value = "XXX", 获取键值为“XXX”的值

### @CookieValue

以下为获取名称为"nowcoderid"的cookie的值

```java
@RequestMapping(value = {"/response"})
    @ResponseBody
    public String response(@CookieValue(value = "nowcoderid", defaultValue = "a") String nowcoderId,
                           @RequestParam(value = "key", defaultValue = "key") String key,
                           @RequestParam(value = "value", defaultValue = "value") String value,
                           HttpServletResponse response) {
        response.addCookie(new Cookie(key, value));
        response.addHeader(key, value);
        return "NowCoderId From Cookie:" + nowcoderId;
    }
```

### @ExceptionHandler（Exception.class）

用在方法上面表示遇到这个异常就执行以下方法。

## swagger2使用

### maven依赖

```xml
<!--swagger2-->
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger2</artifactId>
    <version>2.8.0</version>
</dependency>
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger-ui</artifactId>
    <version>2.8.0</version>
</dependency>
```

### 新建配置类

```java
@Configuration
@EnableSwagger2
public class SwaggerApp {
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                //为当前包路径
                .apis(RequestHandlerSelectors.basePackage("com.nsn.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    /**
     * 构建 api文档的详细信息函数,注意这里的注解引用的是哪个
     * @return
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                //页面标题
                .title("maven构建Spring boot项目")
                //创建人
                //.contact(new Contact("Bryan", "http://blog.bianxh.top/", ""))
                //版本号
                .version("1.0")
                //描述
                .description("maven构建Spring boot项目 集成mybatis使用pagehelper插件 ，实现热部署")
                .build();
    }
}
```

### 新建测试controller类

```java
@RestController
public class UserController {

    @ApiOperation(value = "接口的功能介绍",notes = "提示接口使用者注意事项",httpMethod = "GET")
    @ApiImplicitParam(dataType = "string",name = "name",value = "姓名",required = true)
    @RequestMapping(value = "/")
    public String index(String name) {

        return "hello "+ name;
    }
}
```

### 访问路径

启动项目spring-boot-swagger2,访问 http://localhost:8080/swagger-ui.html

### swagger2注解说明

#### @Api

用在请求的类上，表示对类的说明

​    tags="说明该类的作用，可以在UI界面上看到的注解"
​    value="该参数没什么意义，在UI界面上也看到，所以不需要配置"
示例：

```java
@Api(tags="APP用户注册Controller")
```

#### @ApiOperation

用在请求的方法上，说明方法的用途、作用

​    value="说明方法的用途、作用"
​    notes="方法的备注说明"
示例：

```java
@ApiOperation(value="用户注册",notes="手机号、密码都是必输项，年龄随边填，但必须是数字")
```

#### @ApiImplicitParams

用在请求的方法上，表示一组参数说明
    @ApiImplicitParam：用在@ApiImplicitParams注解中，指定一个请求参数的各个方面
        name：参数名
        value：参数的汉字说明、解释
        required：参数是否必须传
        paramType：参数放在哪个地方
            · header --> 请求参数的获取：@RequestHeader
            · query --> 请求参数的获取：@RequestParam
            · path（用于restful接口）--> 请求参数的获取：@PathVariable
            · body（不常用）
            · form（不常用）    
        dataType：参数类型，默认String，其它值dataType="Integer"       
        defaultValue：参数的默认值

示例：

```java
@ApiImplicitParams({
    @ApiImplicitParam(name="mobile",value="手机号",required=true,paramType="form"),
    @ApiImplicitParam(name="password",value="密码",required=true,paramType="form"),
    @ApiImplicitParam(name="age",value="年龄",required=true,paramType="form",dataType="Integer")
})
```

#### @ApiResponses

用在请求的方法上，表示一组响应
    @ApiResponse：用在@ApiResponses中，一般用于表达一个错误的响应信息
        code：数字，例如400
        message：信息，例如"请求参数没填好"
        response：抛出异常的类

示例：

```java
@ApiOperation(value = "select1请求",notes = "多个参数，多种的查询参数类型")
@ApiResponses({
    @ApiResponse(code=400,message="请求参数没填好"),
    @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对")
})
```

#### @ApiModel

用于响应类上，表示一个返回响应数据的信息
            （这种一般用在post创建的时候，使用@RequestBody这样的场景，
            请求参数无法使用@ApiImplicitParam注解进行描述的时候）
    @ApiModelProperty：用在属性上，描述响应类的属性

示例：

```java
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description= "返回响应数据")
public class RestMessage implements Serializable{

    @ApiModelProperty(value = "是否成功")
    private boolean success=true;
    @ApiModelProperty(value = "返回对象")
    private Object data;
    @ApiModelProperty(value = "错误编号")
    private Integer errCode;
    @ApiModelProperty(value = "错误信息")
    private String message;
}
```

## JWT-Token认证

### 简介

包含三块内容：

1. 头部信息

   有两部分组成

   - 令牌类型，即JWT

   - 使用的签名算法，例如HMACSHA256或RSA.

     ```
     {
       "alg": "HS256",
       "typ": "JWT"
     }
     ```

     这个JSON被编码为base64Url，形成JWT的第一部分

2. 载荷信息

   其中包含声明（claims），声明可以存放在实体（通常是用户）和其它数据的声明，包括3中类型

   - 已注册声明

     是一组预定义声明，不是强制性的，但建议使用，以提供一组游泳的，可互操作的声明。如：iss（发行人）、exp（到期时间）、sub（主题）、adu（观众）等

   - 公开声明

     可以参考[ IANA JSON Web令牌注册表](https://www.iana.org/assignments/jwt/jwt.xhtml) 查看公共的声明

   - 私有声明

     根据自己的业务需要自定义一些数据格式，例如：

     ```json
     {
     "sub": "1234567890",
     "name": "John Doe",
     "admin": true
     }
     ```

3. 签名信息

   这个部分需要 base64 加密后的 头部信息（header） 和 base64 加密后的载荷信息（payload），使用连接组成的字符串，然后通过头部信息（header）中声明的加密方式进行加盐 secret 组合在加密，然后就构成了 JWT 的第三部分。

   例如，如果要使用HMAC SHA256算法，将按以下方式创建签名：

   ```
   HMACSHA256(
   base64UrlEncode(header) + "." +
   base64UrlEncode(payload),
   secret)
   ```

   

### 引入依赖

```xml
<!--token jwt-->
<dependency>
    <groupId>com.auth0</groupId>
    <artifactId>java-jwt</artifactId>
    <version>3.10.1</version>
</dependency>
```

### 使用方法

1. createToken() 创建不携带自定义信息的token

   ```java
   @Test
   public void createToken() {
    
        // 第一步：构建密钥信息
   	String secret = "secret";// token 密钥
   	Algorithm algorithm = Algorithm.HMAC256("secret");
    
   	// 第二步：构建头部信息
   	Map<String, Object> map = new HashMap<String, Object>();
   	map.put("alg", "HS256");
   	map.put("typ", "JWT");
    
   	Date nowDate = new Date();
   	Date expireDate = getAfterDate(nowDate, 0, 0, 0, 2, 0, 0);// 2小过期
   	
        // 第三步：通过定义注册和自定义声明 并组合头部信息和密钥信息生成jwt token
   	String token = JWT.create()
   		.withHeader(map)// 设置头部信息 Header 
   		.withIssuer("SERVICE")//设置 载荷 签名是有谁生成 例如 服务器
   		.withSubject("this is test token")//设置 载荷 签名的主题
   		// .withNotBefore(new Date())//设置 载荷 定义在什么时间之前，该jwt都是不可用的.
   		.withAudience("APP")//设置 载荷 签名的观众 也可以理解谁接受签名的
   		.withIssuedAt(nowDate) //设置 载荷 生成签名的时间
   		.withExpiresAt(expireDate)//设置 载荷 签名过期的时间
   		.sign(algorithm);//签名 Signature
   	Assert.assertTrue(token.length() > 0);
   }
   ```

2. createTokenWithClaim() 创建携带自定义信息的 token

   ```java
   @Test
   public String createTokenWithChineseClaim2() throws UnsupportedEncodingException {
    
   	Date nowDate = new Date();
   	Date expireDate = getAfterDate(nowDate, 0, 0, 0, 2, 0, 0);// 2小过期
    
   	Map<String, Object> map = new HashMap<String, Object>();
   	map.put("alg", "HS256");
   	map.put("typ", "JWT");
    
   	User user = new User();
   	user.setUserNaem("张三");
   	user.setDeptName("技术部");
   	Gson gson = new Gson();
   	String userJson = gson.toJson(user);
    
   	String userJsonBase64 = BaseEncoding.base64().encode(userJson.getBytes());
    
   	Algorithm algorithm = Algorithm.HMAC256("secret");
   	String token = JWT.create().withHeader(map)
   			.withClaim("loginName", "zhuoqianmingyue").withClaim("user", userJsonBase64).withIssuer("SERVICE")// 签名是有谁生成
   			.withSubject("this is test token")// 签名的主题
   			// .withNotBefore(new Date())//该jwt都是不可用的时间
   			.withAudience("APP")// 签名的观众 也可以理解谁接受签名的
   			.withIssuedAt(nowDate) // 生成签名的时间
   			.withExpiresAt(expireDate)// 签名过期的时间
   			.sign(algorithm);//签名 Signature
    
   	return token;
   }
   ```

3. verifyToken() 验证我们的token信息并解析token中的内容

   ```java
   @Test
   public void verifyToken() throws UnsupportedEncodingException {
        // 第一步：构建密钥信息
   	String token = createTokenWithChineseClaim2();
   	Algorithm algorithm = Algorithm.HMAC256("secret");
       
        // 第二步：通过密钥信息和签名的发布者的信息生成JWTVerifier (JWT验证类) 
        // 不添加 .withIssuer("SERVICE") 也是可以获取 JWTVerifier cc
   	JWTVerifier verifier = JWT.require(algorithm).withIssuer("SERVICE").build(); 
       
        // 第三步：通过JWTVerifier 的verify获取 token中的信息
   	DecodedJWT jwt = verifier.verify(token);
   	
        // 获取到我们之前生成 token 的 签名的主题，观众 和自定义的声明信息
   	String subject = jwt.getSubject();c
   	List<String> audience = jwt.getAudience();
   	Map<String, Claim> claims = jwt.getClaims();
   	for (Entry<String, Claim> entry : claims.entrySet()) {
   		String key = entry.getKey();
   		Claim claim = entry.getValue();
   		log.info("key:" + key + " value:" + claim.asString());
   	}
   	Claim claim = claims.get("loginName");
    
   	log.info(claim.asString());
   	log.info(subject);
   	log.info(audience.get(0));
    
   }
   ```



# 全局异常捕获

1、创建一个返回结果类，Result.java

```java
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result<T> {
    private String status;
    private String Message;
    private T data;

    public static Result ok(){
        return new Result<>("ok", null, null);
    }

    public static <T> Result<JSONArray> ok(JSONArray data){
        return new Result<>("ok", null, data);
    }

    public static <T> Result<String> ok(String str){
        log.info(str);
        return new Result<>("ok", null, str);
    }

    public static <T> Result<T> ok(String message, T data){
        log.info(message);
        return new Result<>("ok", message, data);
    }

    public static <T> Result<T> error(String message){
        log.error(message);
        return new Result<>("error", message, null);
    }
}
```

2、自定义一个响应页面，在resource/templates下，my_error.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>捕获全局异常</title>
</head>
<body>
    <h1 style="color: red">发生错误：</h1>
    <div th:text="${url}"></div>
    <div th:text="${exception.message}"></div>
</body>
</html>
```

3、自定义一个异常捕获类, MyExceptionHandler.java

```java
import com.javastudy.core.entity.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@ResponseBody
@ControllerAdvice
public class MyExceptionHandler {
    public static String ERROR_VIEW = "my_error";

    @ExceptionHandler(value = Exception.class)
    public Object errorHandler(HttpServletRequest request, HttpServletResponse response, Exception e) throws Exception{
        log.error("捕获到异常：");
        e.printStackTrace();//打印异常信息
        if(isAjax(request)){
            return Result.error(e.getMessage());
        }else{
            ModelAndView mv = new ModelAndView();
            mv.addObject("exception",e);
            mv.addObject("url",request.getRequestURL());//发生异常的路径
            mv.setViewName(ERROR_VIEW);//指定发生异常之后跳转页面
            return mv;
        }

    }

    public static boolean isAjax(HttpServletRequest request){
        return  (request.getHeader("X-Requested-With") != null
                && "XMLHttpRequest"
                .equals( request.getHeader("X-Requested-With").toString()) );
    }
}
```

# Spring三大核心思想

## AOP

### AOP是什么

**Aspect Oriented Programming：面向切面编程**
什么时候会出现面向切面编程的需求？按照软件重构的思想，如果多个类中出现重复的代码，就应该考虑定义一个共同的抽象类，将这些共同的代码提取到抽象类中，比如Teacher,Student都有username，那么就可以把username及相关的get、set方法抽取到SysUser中，这种情况，我们称为纵向抽取。
但是如果，我们的情况是以下情况，又该怎么办？ 给所有的类方法添加性能检测，事务控制，该怎么抽取？ PerformanceMonitor TransactionManager AOP就是希望将这些分散在各个业务逻辑代码中的相同代码，通过横向切割的方式抽取到一个独立的模块中，让业务逻辑类依然保存最初的单纯。

![image-20210325164018371](image/springboot笔记/image-20210325164018371.png)


抽取出来简单，难点就是如何将这些独立的逻辑融合到业务逻辑中，完成跟原来一样的业务逻辑，这就是AOP解决的主要问题。

还是看不懂?接下基础案例说明

### 基础案例说明

为了更好说明，我们接下来，要讲解的知识点，我们以一个常见的例子来说明
我们以数据库的操作为例来说明：

1. 获取连接对象

2. 执行SQL(核心业务代码)

3. 如果有异常,回滚事务,无异常则提交事务

4. 关闭连接

上述的几个部署，“2”是核心业务代码，其他都是非核心业务代码，但是我们又必须写 而面向切面编程就是为了解决这样的问题，将这些非核心业务代码进行抽离，这样开发者只需要关注“核心业务代码”即可。 这样开发效率自然提高。
在项目开发中，SpringAOP是非常常用的技能之一，下面我画一个图来说明，spring都做了什么

![image-20210325164303428](image/springboot笔记/image-20210325164303428.png)

### AOP术语

- 连接点（Joinpoint) 程序执行的某个特定位置，如某个方法调用前，调用后，方法抛出异常后，这些代码中的特定点称为连接点。简单来说，就是在哪加入你的逻辑增强
  连接点表示具体要拦截的方法，上面切点是定义一个范围，而连接点是具体到某个方法

- **切点（PointCut）** 每个程序的连接点有多个，如何定位到某个感兴趣的连接点，就需要通过切点来定位。比如，连接点--数据库的记录，切点--查询条件
  切点用于来限定Spring-AOP启动的范围，通常我们采用表达式的方式来设置，所以关键词是范围

- **增强（Advice**） 增强是织入到目标类连接点上的一段程序代码。在Spring中，像BeforeAdvice等还带有方位信息
  通知是直译过来的结果，我个人感觉叫做“业务增强”更合适 对照代码就是拦截器定义的相关方法，通知分为如下几种：

  - `前置通知(before)`:在执行业务代码前做些操作，比如获取连接对象

  - `后置通知(after)`:在执行业务代码后做些操作，无论是否发生异常，它都会执行，比如关闭连接对象

  - `异常通知（afterThrowing）`:在执行业务代码后出现异常，需要做的操作，比如回滚事务

  - `返回通知(afterReturning),`在执行业务代码后无异常，会执行的操作

  - `环绕通知(around)`，这个目前跟我们谈论的事务没有对应的操作，所以暂时不谈

- **目标对象（Target**） 需要被加强的业务对象

- **织入（Weaving）** 织入就是将增强添加到对目标类具体连接点上的过程。
    织入是一个形象的说法，具体来说，就是生成代理对象并将切面内容融入到业务流程的过程。
    
- **代理类（Proxy**） 一个类被AOP织入增强后，就产生了一个代理类。

- **切面（Aspect）** 切面由切点和增强组成，它既包括了横切逻辑的定义，也包括了连接点的定义，SpringAOP就是将切面所定义的横切逻辑织入到切面所制定的连接点中。
    比如上文讨论的数据库事务，这个数据库事务代码贯穿了我们的整个代码，我们就可以这个叫做切面。 SpringAOP将切面定义的内容织入到我们的代码中，从而实现前后的控制逻辑。 比如我们常写的拦截器Interceptor，这就是一个切面类
    
    一图胜千言
    
    ![image-20210325164651666](image/springboot笔记/image-20210325164651666.png)

### Spring概述

1，AOP编程可不是Spring独有的，Spring只是支持AOP编程的框架之一，这一点非常重要，切勿搞反了关系。
2，AOP分两类，一类可以对方法的参数进行拦截，一类是对方法进行拦截，SpringAOP属于后者，所以Spring的AOP是属于方法级的

### spring AOP实现-基于注解的方式

@Asoect创建切面类

![image-20210325164752968](image/springboot笔记/image-20210325164752968.png)

@Before： 标识一个前置增强方法，相当于BeforeAdvice的功能.
@After： final增强，不管是抛出异常或者正常退出都会执行.
@AfterReturning： 后置增强，似于AfterReturningAdvice, 方法正常退出时执行.
@AfterThrowing： 异常抛出增强，相当于ThrowsAdvice.
@Around： 环绕增强，相当于MethodInterceptor.
execution：用于匹配方法执行的连接点；

eg.

- 任意公共方法的执行：execution(public \* \*(..))

- 任何一个以“set”开始的方法的执行：execution(\* set\*(..))

- AccountService 接口的任意方法的执行：execution(\* com.xyz.service.AccountService.\*(..))

- 定义在service包里的任意方法的执行： execution(* com.xyz.service.\*.\*(..))

- 定义在service包和所有子包里的任意类的任意方法的执行：execution(* com.xyz.service..\*.\*(..))

`第一个表示匹配任意的方法返回值， …(两个点)表示零个或多个，第一个…表示service包及其子包,第二个表示所有类, 第三个*表示所有方法，第二个…表示方法的任意参数个数`

- 定义在pointcutexp包和所有子包里的JoinPointObjP2类的任意方法的执行：execution(\*com.test.spring.aop.pointcutexp..JoinPointObjP2.\*(..))")
- pointcutexp包里的任意类： within(com.test.spring.aop.pointcutexp.\*)
- pointcutexp包和所有子包里的任意类：within(com.test.spring.aop.pointcutexp..\*)
- 实现了Intf接口的所有类,如果Intf不是接口,限定Intf单个类：this(com.test.spring.aop.pointcutexp.Intf)
- 当一个实现了接口的类被AOP的时候,用getBean方法必须cast为接口类型,不能为该类的类型
- 带有@Transactional标注的所有类的任意方法： @within(org.springframework.transaction.annotation.Transactional) @target(org.springframework.transaction.annotation.Transactional)
- 带有@Transactional标注的任意方法：
  @annotation(org.springframework.transaction.annotation.Transactional)
  @within和@target针对类的注解,@annotation是针对方法的注解
- 参数带有@Transactional标注的方法：@args(org.springframework.transaction.annotation.Transactional)
- 参数为String类型(运行是决定)的方法： args(String)

至于@Around解析请跳转:
https://blog.csdn.net/qq_41981107/article/details/85260765

## IOC(控制反转)

实现将组件间的关系从程序内部提到外部容器（spring的xml）来管理。 

 首先外部容器（spring.xml）中会动态的注册业务所需的对象（接口/类）

## DI(依赖注入)
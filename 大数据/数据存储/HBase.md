# 概述

官网：http://hbase.apache.org/

![image-20210301164044848](image/HBase/image-20210301164044848.png)

Hbase是分布式列存储NOSQL数据库

- HBase 本质上是一个数据模型，可以提供`快速随机访问海量结构化数据`。利用 Hadoop 的文件系统（HDFS）提供的容错能      力。它是 Hadoop 的生态系统，使用 HBase 在 HDFS 读取消费/随机访问数据，是 Hadoop 文件系统的一部分。
- HBase 是一个面向列的数据库，在表中它由行排序。表模式定义只能列族，也就是`键值对`。一个表有多个列族以及每一个列族可以有任意数量的列。后续列的值连续地存储在磁盘上。表中的每个单元格值都具有时间戳。总之，在一个 HBase：`表是行的集合`、`行是列族的集合`、`列族是列的集合`、`列是键值对的集合`。

HBase从另一个角度处理伸缩性问题。它通过`线性方式`从下到上增加节点来进行扩展。HBase不是关系型数据库，也不支持SQL，但是它有自己的特长，这是RDBMS不能处理的，HBase巧妙地将大而稀疏的表放在商用的服务器集群上。

HBase 是Google Bigtable 的开源实现，与Google Bigtable 利用GFS作为其文件存储系统类似， HBase 利用Hadoop HDFS 作为其文件存储系统；Google 运行MapReduce 来处理Bigtable中的海量数据， HBase 同样利用Hadoop MapReduce来处理HBase中的海量数据；Google Bigtable 利用Chubby作为协同服务， HBase 利用Zookeeper作为对应。

## 特点

1. 大：一个表可以有上亿行，上百万列。
2. 面向列：面向列表（簇）的存储和权限控制，列（簇）独立检索。
3. 稀疏：对于为空（NULL）的列，并不占用存储空间，因此，表可以设计的非常稀疏。
4. 无模式：每一行都有一个可以排序的主键和任意多的列，列可以根据需要动态增加，同一张表中不同的行可以有截然不同的列。
5. 数据多版本：每个单元中的数据可以有多个版本，默认情况下，版本号自动分配，版本号就是单元格插入时的时间戳。
6. 数据类型单一：HBase中的数据都是字符串，没有类型。

# 架构设计

![HBase架构图](image/HBase/20200520165022388.jpg)

从图中可以看出Hbase是由Client、Zookeeper、Master、HRegionServer、HDFS等几个组件组成，下面来介绍一下几个组件的相关功能：

从上面这个图里，我们可以发现HBase是由Client、Zookeeper、HMaster、HRegionServer、HDFS等几个组件组成，那么它们都有什么作用呢？可以概括如下：

1. Client：其实client不仅包含了访问Hbase的接口，还维护了对应的cache来加速Hbase的访问，比如cache的.META.元数据的信息。
2. Zookeeper：在大数据生态里zookeeper是很常见的，它主要作为分布式的协调。在HBase里它主要负责的工作如下：
   1. 保证集群中只有1个master在运行，如果master异常，会通过竞争机制产生新的master来提供服务；
   2. 监控RegionServer的状态，当RegionSevrer有异常的时候，通过回调的形式通知Master RegionServer上下线的信息；
   3. 通过Zoopkeeper存储元数据的统一入口地址。
3. HMaster：其实HMaster是Master Server的实现，它主要负责监控集群中的RegionServer实例，并为RegionServer分配Region，同时它还维护整个集群的负载均衡，是所有元数据（metadata）改变的接口。一般在集群里它通常运行在NameNode上，并会运行两个后台线程：
   1. LoadBalancer线程，控制region来平衡集群的负载；
   2. CatalogJanitor线程，周期性的检查hbase:meta表。
4. HRegionServer：其实它就相当于hadoop的DataNode，是RegionServer的实现，用来管理master为其分配的Region，并处理来自客户端的读写请求，负责和底层HDFS交互并存储数据到HDFS，Region变大以后的拆分，以及Storefile的合并工作，集群中RegionServer运行在DataNode上。
5. HDFS：HDFS是HBase运行的底层的文件系统，为HBase提供高可用（Hlog存储在HDFS）的支持，提供元数据和表数据的底层分布式存储服务同时实现了数据多副本，保证了高可靠和高可用性。

# 存储设计

参考：[如何像海豚一样在数据海洋里遨游？｜Hbase数据处理流程详解](https://blog.csdn.net/qq_26803795/article/details/105797099)

分析HBase的数据存储方式，其实就是探讨数据如何在磁盘上进行有效的组织。因为我们通常以如何高效读取和消费数据为目的，而不是数据存储本身。其实在HBase中，表被分割成很多个更小的块然后分散的存储在不同的服务器上，这些小块叫做**Regions**，存放Regions的地方叫做**RegionServer**。**Master**进程负责处理不同的RegionServer之间的Region的分发，在Hbase实现中**HRegionServer**和**HRegion**类代表RegionServer和Region。HRegionServer除了包含一些HRegions之外，还会处理HLog和HFile两种类型的文件用于数据存储。

## HLog设计

HFile是预写日志文件，也称为WAL，是write-ahead log的简称。关于HLog我们知道如下内容就可以了：

1. **MasterProcWAL**：HMaster记录管理操作，比如解决冲突的服务器，表创建和其它DDLs等操作到它的WAL文件中，这个WALs存储在MasterProcWALs目录下，它不像RegionServer的WALs，HMaster的WAL也支持弹性操作，就是如果Master服务器挂了，其它的Master接管的时候继续操作这个文件。

2. **WAL记录所有的Hbase数据改变**，如果一个RegionServer在MemStore进行FLush的时候挂掉了，WAL可以保证数据的改变被应用到。如果写WAL失败了，那么修改数据的完整操作就是失败的。

   - 通常情况，每个RegionServer只有一个WAL实例。在2.0之前，WAL的实现叫做HLog
   - WAL位于*/hbase/WALs/*目录下
   - MultiWAL: 如果每个RegionServer只有一个WAL，由于HDFS必须是连续的，导致必须写WAL连续的，然后出现性能问题。MultiWAL可以让RegionServer同时写多个WAL并行的，通过HDFS底层的多管道，最终提升总的吞吐量，但是不会提升单个Region的吞吐量。

3. 启用multiwal的配置代码：

   ```xml
   // 启用multiwal
   <property>
     <name>hbase.wal.provider</name>
     <value>multiwal</value>
   </property>
   12345
   ```

## HFile设计

HFile才是真实的数据存储文件，是Hbase在HDFS中存储数据的格式，它包含多层的索引，这样在Hbase检索数据的时候就不用完全的加载整个文件。索引的大小(keys的大小，数据量的大小)影响block的大小，在大数据集的情况下，block的大小设置为每个RegionServer 1GB也是常见的。

Hadoop是一个高容错、高延时的分布式文件系统和高并发的批处理系统，不适用于提供实时计算；HBase是可以提供实时计算的分布式数据库，数据被保存在HDFS分布式文件系统上，由HDFS保证期高容错性，但是再生产环境中，HBase是如何基于hadoop提供实时性呢？ HBase上的数据是以StoreFile(HFile)二进制流的形式存储在HDFS上block块儿中；但是HDFS并不知道hbase存的是什么，它只知道存储文件是二进制文件，也就是说，hbase的存储数据对于HDFS文件系统是透明的。下面是HBase文件在HDFS上的存储示意图。

HBase HRegion servers集群中的所有的region的数据在服务器启动时都是被打开的，并且在内冲初始化一些memstore，相应的这就在一定程度上加快系统响 应；而Hadoop中的block中的数据文件默认是关闭的，只有在需要的时候才打开，处理完数据后就关闭，这在一定程度上就增加了响应时间。

从根本上说，HBase能提供实时计算服务主要原因是由其架构和底层的数据结构决定的，即由LSM-Tree + HTable(region分区) + Cache决定——客户端可以直接定位到要查数据所在的HRegion server服务器，然后直接在服务器的一个region上查找要匹配的数据，并且这些数据部分是经过cache缓存的。具体查询流程如下图所示：

![在这里插入图片描述](image/HBase/2020032808062144.png)

具体数据访问流程如下：

1. Client会通过内部缓存的相关的-ROOT-中的信息和.META.中的信息直接连接与请求数据匹配的HRegion server；
2. 然后直接定位到该服务器上与客户请求对应的Region，客户请求首先会查询该Region在内存中的缓存——Memstore(Memstore是一个按key排序的树形结构的缓冲区)；
3. 如果在Memstore中查到结果则直接将结果返回给Client；
4. 在Memstore中没有查到匹配的数据，接下来会读已持久化的StoreFile文件中的数据。前面的章节已经讲过，StoreFile也是按 key排序的树形结构的文件——并且是特别为范围查询或block查询优化过的，；另外HBase读取磁盘文件是按其基本I/O单元(即 HBase Block)读数据的。

具体过程就是：

如果在BlockCache中能查到要造的数据则这届返回结果，否则就读去相应的StoreFile文件中读取一block的数据，如果还没有读到要查的 数据，就将该数据block放到HRegion Server的blockcache中，然后接着读下一block块儿的数据，一直到这样循环的block数据直到找到要请求的数据并返回结果；如果将该 Region中的数据都没有查到要找的数据，最后接直接返回null，表示没有找的匹配的数据。当然blockcache会在其大小大于一的阀值（heapsize * hfile.block.cache.size * 0.85）后启动基于LRU算法的淘汰机制，将最老最不常用的block删除。



# 数据处理流程

## 数据写流程

![image-20210219103601507](image/HBase/image-20210219103601507.png)

大致流程如上图所示：

1. 首先会从zookeepper里找到meta表的region的位置并读取meta表中的数据。
2.  由于meta表里存储了用户表的region信息，所以会根据namespce、表名和rowkey在meta表中的数据里找到写入数据对应的region信息。
3. 根据上一步拿到的region信息扎到它对应的regionserver，然后就可以写入数据了。
4. 其实写数据会把数据分别写到Hlog和memstore各一份。（写入Hlog是为了便于数据的持久化和恢复，写入memstore是为了查询）

`注意：在向memstore写入数据的过程中，会涉及到数据flush和数据合并过程，这一块咱们放到后面单独解释。`

## 数据读流程

![image-20210219104625866](image/HBase/image-20210219104625866.png)

大致流程如上图所示：

1. 首先Client先访问zookeeper集群，从zookeeper的meta表（存储元数据）读取region的位置和meta表中的数据。（meta表中存储了用户表的region信息）
2. Hbase会根据namespace、表名和rowkey在meta表中找到相对应的region信息。
3. 根据上一步拿到的region信息找到它对应的regionserver，并查找这个region来读取数据。
4. 读取数据的时候会先从memstore找数据，如果没有，再到BlockCache里面读；BlockCache还没有，在到StoreFile上读。

Hbase中的数据是有序的，并且还有底层索引支持，所以整体的查询速度非常快。

`注意：如果是从StoreFile里面读取的数据，读取完不会直接返回给客户端，而是先写入BlockCache，再返回给客户端。`

## 数据flush流程

flush是Hbase的一个重要操作，其实很多情况下都会触发Flush操作，如下：

1. 最常见的是Region中所有MemStore占用的内存超过相关阀值；
2. 整个RegionServer的MemStore占用内存总和大于相关阀值；
3. WAL数量大于相关阀值；
4. 定期自动刷写或手动触发刷写；
5. 数据更新超过一定阀值。

以第一种情况为例，当某个 Region 中所有 MemStore 占用的内存（包括 OnHeap + OffHeap）大小超过刷写阈值的时候会触发一次刷写，这个阈值由 `hbase.hregion.memstore.flush.size` 参数控制，默认是128M，老版本是64M。

**其实我们每次调用 put、delete 等操作时Hbase底层都会检查这个条件**。数据的flush操作比较复杂，我们把数据flush简单概括为三步，了解这些就足够了：

1. 第一步将数据刷到硬盘，将内存中的数据删除，同时删除HLog中的历史数据；
2. 第二步将数据存储到HDFS中；
3. 最后一步在HLog中做标记点。

## 数据合并流程

经过上面的flush操作之后，memstore里的数据会存储为Hfile（其实就是StoreFile），随着次数的增多和数据的增大，HDFS上面会出现非常多的Hfile文件，通过上面的分析，我们已经知道读数据操作会对Hfile文件进行归并查询，因此大量的Hfile文件会严重影响读数据的性能，而且这样对DataNode的影响也比较大。

**那Hbase怎么解决这个问题呢？**

Hbase会对Hfile文件进行compact操作，也就是这里要介绍的合并操作。合并操作会产生大量的IO，因此合并是利用IO操作换取后续读写的性能，可以说是前人栽树，后人乘凉的模式。

数据的合并又分为小合并和大合并：

1. 小合并（minor compaction）：小合并时会选择周围相邻的一些小的Hfile文件，来形成一个大一点的Hfile文件。（注意：小合并不会进行过期数据的清除。）
2. 大合并（major compaction）：大合并会把所有的Hfile文件合并成一个大的Hfile文件。（注意：大合并会删除过期数据，超出版本号的旧版本数据和delete标记的数据）

实际场景下，大合并更多的是手动进行的，因此我们通常把参数`hbase.hregion.majorcompaction`的值设为0，表示禁用major compaction。（默认值为7天，允许上下浮动hbase.hregion.majorcompaction * hbase.hregion.majorcompaction.jitter的值，后者默认是0.5。即[7 - 70.5, 7 + 70.5]）

**什么情况下会触发数据合并操作呢？**

在flush操作之后，会对当前Store的文件数量进行判断，如果大于`hbase.hstore.compaction.min`（旧版本是`hbase.hstore.compactionThreshold`）的值（默认是3），就会触发数据合并操作。实际场景下这个参数默认值太小了，一般需要调大。一次minor cmpaction最多合并`hbase.hstore.compaction.max`个文件（默认值10）。

**那数据合并的具体情况是怎样的呢？**

1. 第一步：Hmaster触发合并操作后，Region将数据块加载到本地，便于进行合并；
2. 第二步：当合并的数据超过256M，进行拆分，将拆分后的Region分配给不同的HregionServer管理；
3. 最后：当HregionServer宕机后，将HregionServer上的hlog拆分（注意：HLog会同步到HDFS。），然后分配给不同的HregionServer加载，并会修改.META文件。

# 数据模型

HBase 以表的形式存储数据。表由行和列组成。列划分为若干个列族（row family），如下图所示。

![image-20210207155545620](image/HBase/image-20210207155545620.png)

1. **Table**：HBase的table是由多个行组成的；
2. **Row**：一个行在Hbase中由一个或多个有值的列组成。Row按照字母进行排序，因此行健的设计非常重要。这种设计方式可以让有关系的行非常的近，通常行健的设计是网站的域名反转，比如(org.apache.www, org.apache.mail, org.apache.jira)，这样的话所有的Apache的域名就很接近；
3. **Column**：列由列簇加上列的标识组成，一般是“列簇：列标识”，创建表的时候不用指定列标识；
4. **Column Family**：列簇在物理上包含了许多的列与列的值，每个列簇都有一些存储的属性可配置。例如是否使用缓存，压缩类型，存储版本数等。在表中，每一行都有相同的列簇，尽管有些列簇什么东西也没有存；
5. **Column Qualifier**：列簇的限定词，理解为列的唯一标识。但是列标识是可以改变的，因此每一行可能有不同的列标识；
6. **Cell**：Cell是由row，column family,column qualifier包含时间戳与值组成的，一般表达某个值的版本；
7. **Timestamp**：时间戳一般写在value的旁边，代表某个值的版本号，默认的时间戳是你写入数据的那一刻，但是你也可以在写入数据的时候指定不同的时间戳

我们还需要知道HBase 是一个稀疏的、分布式、持久、多维、排序的映射，它以行键（row key），列键（column key）和时间戳（timestamp）为索引。

HBase在存储数据的时候，有两个SortedMap，首先按照rowkey进行字典排序，然后再对Column进行字典排序。

其实HBase表设计的好坏关键在于RowKey设计的质量，想要设计出一个尽可能完美的rowkey可以参考之前的一篇文章：[大白话彻底搞懂HBase RowKey详细设计](https://blog.csdn.net/qq_26803795/article/details/105994960)，这里我们就不再详细展开了。

## Row Key

RowKey作为HBase的核心知识点，RowKey设计会影响到数据在HBase中的分布，还会影响我们查询效率，所以RowKey的设计质量决定了HBase的质量。是咱们大数据从业者必知必会的，自然也是面试必问的考察点。

### 概念

RowKey从字面意思来看是行键的意思，咱们知道HBase可以理解为一个nosql（not only sql）数据库，既然是数据库，那么咱们日常使用最多的就是增删改查（curd）。其实在增删改查的过程中RowKey就充当了**主键**的作用，它和众多的nosql数据库一样，可以唯一的标识一行记录。

RowKey行键 (RowKey)可以是**任意字符串**，在HBase内部，RowKey保存为字节数组。存储时，数据按照RowKey的字典序(byte order)**排序存储**。设计RowKey时，要充分利用**排序存储这个特性**，将经常一起读取的行存储放到一起。

RowKey的特点小结如下：

1. RowKey类似于主键，可以唯一的标识一行记录；
2. 由于数据按照RowKey的字典序(byte order)**排序存储**，因此HBase中的数据永远都是有序的。
3. RowKey可以由用户自己指定，只要保证这个字符串不重复就可以了。

> **知识点补充：在HBase中检索数据时使用到RowKey的一共有三种方式：**
>
> - **get**：通过指定单个RowKey来获取对应的唯一一条记录；
> - **like**：通过RowKey的range来进行匹配；
> - **scan**：通过设置startRow和stopRow参数来进行范围匹配（**注意：如果不设置就是全表扫描**）。

### 作用

要了解RowKey的作用，首先我们需要知道在HBase中，一个Region就相当于一个数据分片，每个Region都有StartRowKey和StopRowKey（用来表示 Region存储的RowKey的范围），HBase表里面的数据是按照RowKey来分散存储到不同的Region里面的。

为了避免**热点现象**咱们需要将数据记录均衡的分散到不同的Region中去，因此需要RowKey满足这种散列的特点。此外，在数据读写过程中也是与RowKey密切相关的。RowKey的作用可以归纳如下：

1. Hbase在读写数据时需要通过RowKey找到对应的Region；
2. MemStore和HFile中的数据都是按照 RowKey 的字典序排序。

### 热点现象

#### 热点现象怎么产生

我们已经知道HBase中的行是按照RowKey的字典顺序排序的，这种设计优化了scan操作，可以将相关的行以及会被一起读取的行存取在相邻位置，便于scan读取。

然后万事万物都有两面性，在咱们实际生产中，当大量 请求访问HBase集群的一个或少数几个节点，造成少数RegionServer的读写请求过多，负载过大，而其他RegionServer负载却很小，这样就造成**热点现象**（其实和数据倾斜类似）。

掌握了热点现象的概念，我们就应该知道大量的访问会使热点Region所在的主机负载过大，引起性能下降，甚至导致Region不可用。所以**我们在向HBase中插入数据的时候，应优化RowKey的设计，使数据被写入集群的多个region，而不是一个。尽量均衡地把记录分散到不同的Region中去，平衡每个Region的压力**。

`其实RowKey的优化主要就是在解决怎么避免热点现象。那么有哪些避免热点现象的方法呢？各有什么缺点？带着问题，接着往下看。`

#### 如何避免热点现象

在日常使用中，主要有3个方法来避免热点现象，分别是**反转，加盐和哈希**。听起来很奇怪，下面咱们逐个举例详细分析：

##### 反转（Reversing）

第一种咱们要分析的方法是**反转，顾名思义它就是把固定长度或者数字格式的 rowkey进行反转**，反转分为一般数据反转和时间戳反转，其中以时间戳反转较常见。

**适用场景：**

比如咱们初步设计出的RowKey在数据分布上不均匀，但RowKey尾部的数据却呈现出了良好的随机性（注意：随机性强代表经常改变，没意义，但分布较好），此时，可以考虑将RowKey的信息翻转，或者直接将尾部的bytes提前到RowKey的开头。反转可以有效的使RowKey随机分布，但是反转后有序性肯定就得不到保障了，因此它牺牲了RowKey的有序性。

**缺点：**

利于Get操作，但不利于Scan操作，因为数据在原RowKey上的自然顺序已经被打乱。

**举例：**

比如咱们通常会有需要快速获取数据的最近版本的数据处理需求，这时候就需要把时间戳作为RowKey来查询了，但是时间戳正常情况下是这样的：

```
1588610367373
1588610367396
```

前面这部分是相同的，在查询的时候就容易造成热点现象，因此需要使用时间戳反转的方式来处理。实际生产中可以用 `Long.Max_Value - timestamp` 追加到 key 的末尾，比如 `[key][reverse_timestamp]`, `[key]` 的最新值可以通过 `scan [key]`获得`[key]`的第一条记录，因为HBase中RowKey是有序的，所以第一条记录是最后录入的数据。

常见的场景，比如需要保存一个用户的操作记录，就可以按照操作时间倒序排序，在设计rowkey的时候，可以这样设计 `[反转后的userId][Long.Max_Value - timestamp]`，在查询用户的所有操作记录数据的时候，直接指定反转后的userId，startRow 是 `[反转后的userId][000000000000]`，stopRow 是 `[反转后的userId][Long.Max_Value - timestamp]`。如果需要查询某段时间的操作记录，startRow 是`[反转后的userId[Long.Max_Value - 起始时间]`， stopRow 是`[反转后的userId][Long.Max_Value - 结束时间]`。

##### 加盐（Salting）

第二种咱们要介绍的方法是加盐，玩过密码学的可能知道密码学里也有加盐的方法，但是咱们RowKey的加盐和密码学不一样，它的**原理是在原RowKey的前面添加固定长度的随机数，也就是给RowKey分配一个随机前缀使它和之前的RowKey的开头不同。**

**适用场景：**

比如咱们设计的RowKey是有意义的，但是数据类似，随机性比较低，反转也没法保证随机性，这样就没法根据RowKey分配到不同的Region里，这时候就可以使用加盐的方式了。

需要注意随机数要能保障数据在所有Regions间的负载均衡，也就是说分配的随机前缀的种类数量应该和你想把数据分散到的那些region的数量一致。只有这样，加盐之后的rowkey才会根据随机生成的前缀分散到各个region中，避免了热点现象。

**缺点：**

大白话来理解就是加了盐就尝不到原有的味道了。因为添加的是随机数，添加后如果还基于原RowKey查询，就无法知道随机数是什么，那样在查询的时候就需要去各个可能的Region中查找，同时加盐对于读取是利空的。并且加盐这种方式增加了读写时的吞吐量。

##### 哈希（Hashing）

最后介绍大家最熟悉的哈希方法，不管是学的啥技术，都会涉及到哈希，也都大同小异，比较简单。

这里的哈希是基于RowKey的完整或部分数据进行Hash，而后将哈希后的值完整替换或部分替换原RowKey的前缀部分。这里说的hash常用的有MD5、sha1、sha256 或 sha512 等算法。

**适用场景：**

其实哈希和加盐的适用场景类似，但是由于加盐方法的前缀是随机数，用原rowkey查询时不方便，因此出现了哈希方法，由于哈希是使用各种常见的算法来计算出的前缀，因此哈希既可以使负载分散到整个集群，又可以轻松读取数据。

**缺点：**

与反转类似，哈希也打乱了RowKey的自然顺序，因此也不利于Scan。

### RowKey设计原则

通过前面的分析我们应该知道了HBase中RowKey设计的重要性了，为了帮助我们设计出完美的RowKey，HBase提出了RowKey的设计原则，一共有四点：**长度原则、唯一原则、排序原则，散列原则**。

RowKey在字段的选择上，需要遵循的**最基本原则是唯一原则**，因为RowKey必须能够唯一的识别一行数据。无论应用的负载特点是什么样，RowKey字段都应该**首先考虑最高频的查询场景**。数据库通常都是以如何高效的读取和消费数据为目的，而不仅仅是数据存储本身。然后再结合具体的负载特点，再对选取的RowKey字段值进行改造，结合RowKey的优化，也就是避免热点现象的那些方法来优化就可以了。

#### 长度原则

RowKey本质上是一个二进制码的流，可以是任意字符串，最大长度为64kb，实际应用中一般为10-100byte，以byte[]数组形式保存，一般设计成定长。官方建议越短越好，不要超过16个字节，原因可以概括为如下几点：

1. **影响HFile的存储效率：**HBase里的数据在持久化文件HFile中其实是按照Key-Value对形式存储的。这时候如果RowKey很长，比如达到了200byte，那么仅仅1000w行的记录，只考虑RowKey就需占用近2GB的空间，极大的影响了HFile的存储效率。
2. **降低检索效率：**由于MemStore会缓存部分数据到内存中，如果RowKey比较长，就会导致内存的有效利用率降低，也就不能缓存更多的数据，从而降低检索效率。
3. **16字节是64位操作系统的最佳选择：**64位系统，内存8字节对齐，控制在16字节，8字节的整数倍利用了操作系统的最佳特性。

#### 唯一原则

其实唯一原则咱们可以结合HashMap的源码设计或者主键的概念来理解，由于RowKey用来唯一标识一行记录，所以必须在设计上保证RowKey的唯一性。

**需要注意**：由于HBase中数据存储的格式是Key-Value对格式，所以如果向HBase中同一张表插入相同RowKey的数据，则原先存在的数据会被新的数据给覆盖掉（和HashMap效果相同）。

#### 排序原则

HBase会把RowKey按照ASCII进行自然有序排序，所以反过来我们在设计RowKey的时候可以根据这个特点来设计完美的RowKey，好好的利用这个特性就是排序原则。

#### 散列原则

散列原则用大白话来讲就是咱们设计出的RowKey需要能够均匀的分布到各个RegionServer上。

比如设计RowKey的时候，当Rowkey 是按时间戳的方式递增，就不要将时间放在二进制码的前面，可以将 Rowkey 的高位作为散列字段，由程序循环生成，可以在低位放时间字段，这样就可以提高数据均衡分布在每个Regionserver实现负载均衡的几率。

**结合前面分析的热点现象的起因，思考：**

如果没有散列字段，首字段只有时间信息，那就会出现所有新数据都在一个 RegionServer上堆积的热点现象，这样在做数据检索的时候负载将会集中在个别RegionServer上，不分散，就会降低查询效率。

HBase里的RowKey是按照**字典序**存储，因此在设计RowKey时，咱们要充分利用这个排序特点，将经常一起读取的数据存储到一块，将最近可能会被访问的数据放在一块。如果最近写入HBase表中的数据是最可能被访问的，可以考虑将时间戳作为row key的一部分，由于是字典序排序，所以可以使用`Long.MAX_VALUE - timestamp`作为row key，这样能保证新写入的数据在读取时可以被快速找到。

## Column Family

hbase表中的每个列，都归属与某个列族。列族是表的chema的一部分(而列不是)，必须在使用表之前定义。列名都以列族作为前缀。例如*courses:history ， courses:math 都属于 courses* 这个列族。

访问控制、磁盘和内存的使用统计都是在列族层面进行的。实际应用中，列族上的控制权限能 帮助我们管理不同类型的应用：我们允许一些应用可以添加新的基本数据、一些应用可以读取基本数据并创建继承的列族、一些应用则只允许浏览数据（甚至可能因 为隐私的原因不能浏览所有数据）。

## 时间戳

HBase中通过row和columns确定的为一个存贮单元称为cell。每个 cell都保存着同一份数据的多个版本。版本通过时间戳来索引。时间戳的类型是 64位整型。时间戳可以由hbase(在数据写入时自动 )赋值，此时时间戳是精确到毫秒的当前系统时间。时间戳也可以由客户显式赋值。如果应用程序要避免数据版本冲突，就必须自己生成具有唯一性的时间戳。每个 cell中，不同版本的数据按照时间倒序排序，即最新的数据排在最前面。

为了避免数据存在过多版本造成的的管理 (包括存贮和索引)负担，hbase提供了两种数据版本回收方式。一是保存数据的最后n个版本，二是保存最近一段时间内的版本（比如最近七天）。用户可以针对每个列族进行设置。

## Cell

由*{row key, column(* =<family> + <label>*), version}* 唯一确定的单元。cell中的数据是没有类型的，全部是字节码形式存贮。



# 安装

## 官网下载

官网：https://hbase.apache.org/downloads.html

![image-20210303151943513](image/HBase/image-20210303151943513.png)

## 搭建单机版

https://blog.csdn.net/qq_37171353/article/details/89947202?ops_request_misc=%25257B%252522request%25255Fid%252522%25253A%252522161372297916780262549884%252522%25252C%252522scm%252522%25253A%25252220140713.130102334..%252522%25257D&request_id=161372297916780262549884&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduend~default-2-89947202.first_rank_v2_pc_rank_v29&utm_term=HBASE



## 使用Docker快速安装

```sh
#查找
docker search Hbase

#拉取
docker pull harisekhon/hbase

#创建容器
docker run -d --name hbase --hostname hbase -p 16010:16010 -p 16020:16020 -p 16030:16030 -p 16000:16000 -p 2181:2181 harisekhon/hbase:latest

#进入容器
docker exec -it hbase bin/bash

#进入容器后执行下面命令进入hbase shell界面
hbase shell
```

查看管理页面http://192.168.17.130:16010/

![image-20210219153506092](image/HBase/image-20210219153506092.png)



## Hadoop集群中安装

### 下载匹配hbase版本

**1、查看hadoop与hbase的版本匹配关系**

官网：http://hbase.apache.org/book.html

搜索：Hadoop version support matrix

![image-20210303152848922](image/HBase/image-20210303152848922.png)



**2、查看集群版本**

先运行hadoop集群

```sh
docker start cluster-master cluster-slave1 cluster-slave2 cluster-slave3

docker exec -it cluster-master /bin/bash

cd /opt/hadoop/sbin

./start-all-cluster.sh
```



![image-20210303152508209](image/HBase/image-20210303152508209.png)

**3、hbase可以下载2.3.4版本**

https://mirrors.bfsu.edu.cn/apache/hbase/2.3.4/hbase-2.3.4-bin.tar.gz

### 安装Hbase

1、**拷贝软件包**

docker cp /data/soft/hadoop/hbase-2.3.4-bin.tar.gz cluster-master:/opt

cd /opt

tar -xf hbase-2.3.4-bin.tar.gz

ln -s hbase-2.3.4 hbase

2、**配置环境变量**

vim ~/.bashrc

```sh
# hbase
export HBASE_HOME=/opt/hbase
export PATH=$HBASE_HOME/bin:$PATH
```

3、**分别创建Hbase的元数据文件目录“tmp”和HDFS的日志文件目录“logs”**

cd /opt/hbase

mkdir tmp logs

4、**hbase-env.sh配置修改**

vim conf/hbase-env.sh

- 找到配置项“JAVA_HOME”所在行，将其改为以下内容：

  export JAVA_HOME=${JAVA_HOME}

- 找到配置项“HBASE_CLASSPATH”，该项用于指定Hadoop的配置文件所在的路径，将其值改为一下内容

  export HBASE_CLASSPATH=${HADOOP_HOME}/etc/hadoop

- 找到配置项“HBASE_LOG_DIR”，该项用于指定HBase的日志文件的本地存放路径，将其值改为以下内容：

  export HBASE_LOG_DIR=${HBASE_HOME}/logs

- `因测试环境没有zookeeper，不用修改这里`找到配置项“HBASE_MANAGES_ZK”，该项用于关闭HBase自带的Zookeeper组件，将其值改为一下内容：

  export HBASE_MANAGES_ZK=false

5、**hbase-site.xml配置修改**

vim conf/hbase-site.xml

```xml
<configuration>
    
    <property>
        <name>hbase.rootdir</name>
        <value>hdfs://cluster-master:9000/hbase</value>
    </property>
    
    <property>
        <name>hbase.tmp.dir</name>
        <value>/opt/hbase/tmp</value>
    </property>
    
    <property>
        <name>hbase.cluster.distributed</name>
        <value>true</value>
    </property>
    
    <property>
        <name>hbase.zookeeper.quorum</name>
        <value>cluster-master:2181,cluster-slave1:2181,cluster-slave2:2181,cluster-slave3:2181</value>
    </property>
    
    <property>
        <name>hbase.master.maxclockskew</name>
        <value>60000</value>
    </property>
 
</configuration>
```

6、**对配置文件regionservers进行修改，删除文件中原有的所有内容，然后添加集群中所有数据节点的主机名，每行一个主机的主机名，配置格式如下：**

vim conf/regionservers

```sh
cluster-slave2
cluster-slave3
```

7、**创建配置文件“backup-masters”，并对配置文件进行修改。添加集群中所有备用节点的主机名，每行一个主机的主机名，配置格式如下：**

vim conf/backup-masters

```sh
cluster-slave1
```

8、**使用ansible-playbook分发配置文件至slave主机**

vim hbase-dis.yaml

```yaml
---
- hosts: cluster
  tasks:
    - name: copy core-site to slaves
      copy: src=/opt/hbase-2.3.4 dest=/opt/

  handlers:
    - name: exec source
      shell: source ~/.bashrc
```

执行：

ansible-playbook hbase-dis.yaml

9、**启动集群**

在主节点执行：/opt/hbase/bin/start-hbase.sh

![image-20210304091351346](image/HBase/image-20210304091351346.png)

![image-20210304091618306](image/HBase/image-20210304091618306.png)

10、**测试**

![image-20210304092122396](image/HBase/image-20210304092122396.png)

# Shell操作

```sh
#进入hbase命令行
hbase shell
 
#显示hbase中的表
list

#查看帮助信息
help

#退出
exit / quit

#简单创建user表，包含info、data两个列族
create 'user', 'info', 'data'

#创表时设置列族为 ropledata的过期时间是1296000秒，也就是15天;分配到各个region的切分方式为SNAPPY
create 'csdn', {NAME =>'ropledata', COMPRESSION => 'SNAPPY',TTL => '1296000'},{SPLITS => ['09_', '18_', '27_', '36_', '45_', '54_', '63_', '72_', '81_', '90_']}

向user表中插入信息，row key为rk0001，列族info中添加name列标示符，值为zhangsan
put 'user', 'rk0001', 'info:name', 'zhangsan'
 
向user表中插入信息，row key为rk0001，列族info中添加gender列标示符，值为female
put 'user', 'rk0001', 'info:gender', 'female'
 
向user表中插入信息，row key为rk0001，列族info中添加age列标示符，值为20
put 'user', 'rk0001', 'info:age', 20
 
向user表中插入信息，row key为rk0001，列族data中添加pic列标示符，值为picture
put 'user', 'rk0001', 'data:pic', 'picture'
 
获取user表中row key为rk0001的所有信息
get 'user', 'rk0001'
 
获取user表中row key为rk0001，info列族的所有信息
get 'user', 'rk0001', 'info'
 
获取user表中row key为rk0001，info列族的name、age列标示符的信息
get 'user', 'rk0001', 'info:name', 'info:age'
 
获取user表中row key为rk0001，info、data列族的信息
get 'user', 'rk0001', 'info', 'data'
get 'user', 'rk0001', {COLUMN => ['info', 'data']}
 
get 'user', 'rk0001', {COLUMN => ['info:name', 'data:pic']}
 
获取user表中row key为rk0001，列族为info，版本号最新5个的信息
get 'user', 'rk0001', {COLUMN => 'info', VERSIONS => 2}
get 'user', 'rk0001', {COLUMN => 'info:name', VERSIONS => 5}
get 'user', 'rk0001', {COLUMN => 'info:name', VERSIONS => 5, TIMERANGE => [1392368783980, 1392380169184]}
 
获取user表中row key为rk0001，cell的值为zhangsan的信息
get 'people', 'rk0001', {FILTER => "ValueFilter(=, 'binary:zhangsan')"}
 
获取user表中row key为rk0001，列标示符中含有a的信息
get 'people', 'rk0001', {FILTER => "(QualifierFilter(=,'substring:a'))"}
 
put 'user', 'rk0002', 'info:name', 'fanbingbing'
put 'user', 'rk0002', 'info:gender', 'female'
put 'user', 'rk0002', 'info:nationality', '中国'
get 'user', 'rk0002', {FILTER => "ValueFilter(=, 'binary:中国')"}
 
 
查询user表中的所有信息
scan 'user'
 
查询user表中列族为info的信息
scan 'user', {COLUMNS => 'info'}
scan 'user', {COLUMNS => 'info', RAW => true, VERSIONS => 5}
scan 'user', {COLUMNS => 'info', RAW => true, VERSIONS => 3}
 
查询user表中列族为info和data的信息
scan 'user', {COLUMNS => ['info', 'data']}
scan 'user', {COLUMNS => ['info:name', 'data:pic']}
 
查询user表中列族为info、列标示符为name的信息
scan 'user', {COLUMNS => 'info:name'}
 
查询user表中列族为info、列标示符为name的信息,并且版本最新的5个
scan 'user', {COLUMNS => 'info:name', VERSIONS => 5}
 
查询user表中列族为info和data且列标示符中含有a字符的信息
scan 'user', {COLUMNS => ['info', 'data'], FILTER => "(QualifierFilter(=,'substring:a'))"}
 
查询user表中列族为info，rk范围是[rk0001, rk0003)的数据
scan 'user', {COLUMNS => 'info', STARTROW => 'rk0001', ENDROW => 'rk0003'}
 
查询user表中row key以rk字符开头的
scan 'user',{FILTER=>"PrefixFilter('rk')"}
 
查询user表中指定范围的数据
scan 'user', {TIMERANGE => [1392368783980, 1392380169184]}
 
删除数据
删除user表row key为rk0001，列标示符为info:name的数据
delete 'user', 'rk0001', 'info:name'
删除user表row key为rk0001，列标示符为info:name，timestamp为1392383705316的数据
delete 'user', 'rk0001', 'info:name', 1392383705316
 
清空user表中的数据
truncate 'user'
 
修改表结构
首先停用user表
disable 'user'
 
添加两个列族f1和f2
alter 'people', NAME => 'f1'
alter 'user', NAME => 'f2'
 
启用表
enable 'user'
 
删除一个列族：
alter 'user', NAME => 'f1', METHOD => 'delete' 或 alter 'user', 'delete' => 'f1'
 
添加列族f1同时删除列族f2
alter 'user', {NAME => 'f1'}, {NAME => 'f2', METHOD => 'delete'}
 
将user表的f1列族版本号改为5
alter 'people', NAME => 'info', VERSIONS => 5
启用表
enable 'user'
 
删除表
disable 'user'
drop 'user'
 
 
#统计表一共有多少rowkey，也就是行数
count 'user'
```

`提示：清空表/修改表结构的操作先disable。`



# JAVA操作

## API介绍

![img](image/HBase/1122015-20170511193441238-678041304.png)

### HBaseAdmin

关系： org.apache.hadoop.hbase.client.HBaseAdmin
作用：提供了一个接口来管理 HBase 数据库的表信息。它提供的方法包括：创建表，删 除表，列出表项，使表有效或无效，以及添加或删除表列族成员等。
![img](image/HBase/1122015-20170511194405816-821856168.png)

![img](image/HBase/1122015-20170511194415566-642295716.png)

###  HBaseConfiguration

关系： org.apache.hadoop.hbase.HBaseConfiguration
作用：对 HBase 进行配置

![img](image/HBase/1122015-20170511195034644-738803490.png)

### HTableDescriptor

关系： org.apache.hadoop.hbase.HTableDescriptor
作用：包含了表的名字极其对应表的列族

![img](image/HBase/1122015-20170511195249801-502794494.png)

###  HColumnDescriptor

关系： org.apache.hadoop.hbase.HColumnDescriptor
作用：维护着关于列族的信息，例如版本号，压缩设置等。它通常在创建表或者为表添 加列族的时候使用。列族被创建后不能直接修改，只能通过删除然后重新创建的方式。
列族被删除的时候，列族里面的数据也会同时被删除。

![img](image/HBase/1122015-20170511195332144-1988603702.png)

###  HTable

关系： org.apache.hadoop.hbase.client.HTable
作用：可以用来和 HBase 表直接通信。此方法对于更新操作来说是非线程安全的。

![img](image/HBase/1122015-20170511195431769-894664693.png)

![img](image/HBase/1122015-20170511195447738-1259135400.png)

###  Put

关系： org.apache.hadoop.hbase.client.Put
作用：用来对单个行执行添加操作

![img](image/HBase/1122015-20170511195638019-1633152237.png)

###  Get

关系： org.apache.hadoop.hbase.client.Get
作用：用来获取单个行的相关信息

![img](image/HBase/1122015-20170511195733863-1822625999.png)

### Result

关系： org.apache.hadoop.hbase.client.Result
作用：存储 Get 或者 Scan 操作后获取表的单行值。使用此类提供的方法可以直接获取值 或者各种 Map 结构（ key-value 对）
![img](image/HBase/1122015-20170512142230129-1523738800.png)

## 通过hbase-client

> docker所在服务器ip：192.168.17.130
>
> docker容器ip：172.17.0.2
>
> 代码运行的服务器上配置hosts——192.168.17.130  hbase
>
> 在linux上配置hosts——172.17.0.2 hbase

### POM

```xml
<!-- 特别注意版本号，与安转的hbase一致-->
<dependency>
    <groupId>org.apache.hbase</groupId>
    <artifactId>hbase-client</artifactId>
    <version>2.1.3</version>
</dependency>
```

**关于java.io.IOException: HADOOP_HOME or hadoop.home.dir are not set.的问题**

可参考https://www.cnblogs.com/huxinga/p/6875929.html

下载后，在代码里添加System.setProperty("hadoop.home.dir", "E:\\vmware\\data\\soft\\hadoop\\hadoop-common-2.2.0-bin-master");

![image-20210224165601258](image/HBase/image-20210224165601258.png)



### HBaseService.java

```java
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Scan;

import java.util.List;
import java.util.Map;

public interface HBaseService {
    /**
     * 创建表
     * expression : create 'tableName','[Column Family 1]','[Column Family 2]'
     * @param tableName      表名
     * @param columnFamilies 列族名
     */
    void createTable(String tableName, String... columnFamilies);


    /**
     * 插入或更新
     *  expression : put <tableName>,<rowKey>,<family:column>,<value>,<timestamp>
     * @param tableName    表名
     * @param rowKey      行id
     * @param columnFamily  列族名
     * @param column      列
     * @param value       值
     */
     void insertOrUpdate(String tableName, String rowKey, String columnFamily, String column, String value);

    /**
     *   插入或更新多个字段
     * expression : put <tableName>,<rowKey>,<family:column>,<value>,<timestamp>
     * @param tableName    表名
     * @param rowKey        行id
     * @param columnFamily  列族名
     * @param columns     列
     * @param values      值
     */
    public void insertOrUpdate(String tableName, String rowKey, String columnFamily, String[] columns, String[] values);

    /**
     * 批量插入
     * @param tableName
     * @param list <Put>
     */
    public void insertBach(String tableName, List<Put> list);

    /**
     * 删除行
     * @param tableName       表名
     * @param rowKey      行id
     */
    public void deleteRow(String tableName, String rowKey);


    /**
     * 删除表
     * expression : disable 'tableName' 之后 drop 'tableName'
     * @param tableName    表名
     */
    public void deleteTable(String tableName);

    /**
     * 获取具体值
     * expression : get 'tableName','rowkey','family:column'
     * @param tableName       表名
     * @param rowKey      行id
     * @param family      列族名
     * @param column      列名
     * @return string
     */
    public String getValue(String tableName, String rowKey, String family, String column);

    /**
     * 查询指定行
     * experssion : get 'tableName','rowKey'
     * @param tableName       表名
     * @param rowKey      行id
     * @return String
     */
    public Map<String, Map<String, String>> getOneByRow(String tableName, String rowKey);

    /**
     * 判断表是否已经存在，这里使用间接的方式来实现
     * @param tableName 表名
     * @return boolean
     */
    public boolean tableExists(String tableName);

    /**
     * 查询所有表的表名
     */
    public List<String> getAllTableNames();

    /**
     * 查询表中的所有数据
     */
    public Map<String, Map<String, String>> getAllData(String tableName);


    /**
     * 通过表名及过滤条件查询数据
     * @param tableName
     * @param scan
     * @return
     */
    Map<String, Map<String, String>> queryByScan(String tableName, Scan scan);

    /**
     * 读取一个文件批量入库
     * @param filePath
     * @param tableName
     * @param rowkeyIndex
     * @param tsIndex
     */
    public void insertBatchByFile(String filePath, String tableName, int rowkeyIndex, int tsIndex);

    /**
     * 通过特定格式的RowKey进行查询
     * @param tableName
     * @param index
     * @param st
     * @param et
     * @return
     */
    public Map<String, Map<String,String>> queryByIndex(String tableName, String index, String st, String et);

}
```

### HBaseServiceImpl.java

```java
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.PageFilter;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 操作hbase实现增删改查
 * @author Tony.niu (hystrix0779@yeah.net)
 * @version V1.0
 * @date 2020/7/28
 **/
@Slf4j
@Service
public class HBaseServiceImpl implements HBaseService {

    private static String zkServer = "192.168.17.130";
    private static String port = "2181";

    private Connection connection;
    private Admin admin;
    private final ThreadLocal<List<Put>> threadLocal = new ThreadLocal<>();
    private final int CACHE_LIST_SIZE = 1000;

    {
        System.setProperty("hadoop.home.dir", "E:\\vmware\\data\\soft\\hadoop\\hadoop-common-2.2.0-bin-master");
        Configuration configuration = HBaseConfiguration.create();
        configuration.set("hbase.zookeeper.quorum", zkServer);
        configuration.set("hbase.zookeeper.property.clientPort", port);

        log.info("Connect hbase Starting...");
        try {
            connection = ConnectionFactory.createConnection(configuration);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        System.out.println(connection.isClosed());
        try {
            admin = connection.getAdmin();
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("Connect hbase Success!");
    }

    /**
     * 创建表
     * expression : create 'tableName','[Column Family 1]','[Column Family 2]'
     * @param tableName      表名
     * @param columnFamilies 列族名
     */
    public void createTable(String tableName, String... columnFamilies) {
        TableName name = TableName.valueOf(tableName);
        boolean isExists = false;
        isExists = this.tableExists(tableName);

        if (isExists) {
            try {
                throw new TableExistsException(tableName + " is exists!");
            } catch (TableExistsException e) {
                e.printStackTrace();
            }
        }

        TableDescriptorBuilder descriptorBuilder = TableDescriptorBuilder.newBuilder(name);
        List<ColumnFamilyDescriptor> columnFamilyList = new ArrayList<>();

        for (String columnFamily : columnFamilies) {
            ColumnFamilyDescriptor columnFamilyDescriptor = ColumnFamilyDescriptorBuilder
                    .newBuilder(columnFamily.getBytes()).build();
            columnFamilyList.add(columnFamilyDescriptor);
        }

        descriptorBuilder.setColumnFamilies(columnFamilyList);
        TableDescriptor tableDescriptor = descriptorBuilder.build();
        try {
            admin.createTable(tableDescriptor);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 插入或更新
     *  expression : put <tableName>,<rowKey>,<family:column>,<value>,<timestamp>
     * @param tableName    表名
     * @param rowKey      行id
     * @param columnFamily  列族名
     * @param column      列
     * @param value       值
     */
    public void insertOrUpdate(String tableName, String rowKey, String columnFamily, String column, String value){
        this.insertOrUpdate(tableName, rowKey, columnFamily, new String[]{column}, new String[]{value});
    }

    /**
     *   插入或更新多个字段
     * expression : put <tableName>,<rowKey>,<family:column>,<value>,<timestamp>
     * @param tableName    表名
     * @param rowKey        行id
     * @param columnFamily  列族名
     * @param columns     列
     * @param values      值
     */
    public void insertOrUpdate(String tableName, String rowKey, String columnFamily, String[] columns, String[] values){
        Table table = null;
        try {
            table = connection.getTable(TableName.valueOf(tableName));
            Put put = new Put(Bytes.toBytes(rowKey));

            for (int i = 0; i < columns.length; i++) {
                put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columns[i]), Bytes.toBytes(values[i]));
                table.put(put);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * 删除行
     * @param tableName       表名
     * @param rowKey      行id
     */
    public void deleteRow(String tableName, String rowKey){
        Table table = null;
        try {
            table = connection.getTable(TableName.valueOf(tableName));

            Delete delete = new Delete(rowKey.getBytes());

            table.delete(delete);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * 删除列族
     * @param tableName       表名
     * @param rowKey      行id
     * @param columnFamily 列族名
     */
    public void deleteColumnFamily(String tableName, String rowKey, String columnFamily) {
        Table table = null;
        try {
            table = connection.getTable(TableName.valueOf(tableName));

            Delete delete = new Delete(rowKey.getBytes());
            delete.addFamily(Bytes.toBytes(columnFamily));

            table.delete(delete);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 删除列
     * experssion : delete 'tableName','rowKey','columnFamily:column'
     * @param tableName       表名
     * @param rowKey      行id
     * @param columnFamily 列族名
     * @param column      列名
     */
    public void deleteColumn(String tableName, String rowKey, String columnFamily, String column){
        Table table = null;
        try {
            table = connection.getTable(TableName.valueOf(tableName));
            Delete delete = new Delete(rowKey.getBytes());
            delete.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(column));

            table.delete(delete);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * 删除表
     * expression : disable 'tableName' 之后 drop 'tableName'
     * @param tableName    表名
     * @throws IOException io异常
     */
    public void deleteTable(String tableName) {
        boolean isExists = false;
        try {
            isExists = this.tableExists(tableName);
            if (!isExists) {
                return;
            }

            TableName name = TableName.valueOf(tableName);
            admin.disableTable(name);
            admin.deleteTable(name);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取具体值
     * expression : get 'tableName','rowkey','family:column'
     * @param tableName       表名
     * @param rowKey      行id
     * @param family      列族名
     * @param column      列名
     * @return string
     */
    public String getValue(String tableName, String rowKey, String family, String column) {
        Table table = null;
        String value = "";

        if (StringUtils.isBlank(tableName) || StringUtils.isBlank(family) || StringUtils.isBlank(rowKey) || StringUtils
                .isBlank(column)) {
            return null;
        }

        try {
            table = connection.getTable(TableName.valueOf(tableName));
            Get g = new Get(rowKey.getBytes());
            g.addColumn(family.getBytes(), column.getBytes());
            Result result = table.get(g);
            List<Cell> ceList = result.listCells();
            if (ceList != null && ceList.size() > 0) {
                for (Cell cell : ceList) {
                    value = Bytes.toString(cell.getValueArray(), cell.getValueOffset(), cell.getValueLength());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                assert table != null;
                table.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return value;
    }

    /**
     * 查询指定行
     * experssion : get 'tableName','rowKey'
     * @param tableName       表名
     * @param rowKey      行id
     * @return String
     */
    public Map<String, Map<String, String>> getOneByRow(String tableName, String rowKey) {
        Table table = null;
        Map<String, Map<String, String>> results = null ;
        try{
            table = connection.getTable(TableName.valueOf(tableName));
            Get g = new Get(rowKey.getBytes());
            Result result = table.get(g);
            List<Cell> cellList = result.listCells();
            results = cellList.size() > 0 ? new HashMap<String, Map<String, String>>() : results;
            for(Cell cell : cellList){
                String familyName = Bytes.toString(CellUtil.cloneFamily(cell));
                if(results.get(familyName) == null){
                    results.put(familyName, new HashMap<>());
                }
                results.get(familyName).put(Bytes.toString(CellUtil.cloneQualifier(cell)),Bytes.toString(CellUtil.cloneValue(cell)));
            }
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try {
                assert table != null;
                table.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return results;
        }
    };


    /**
     *   根据条件取出点位指定时间内的所有记录
     * @param tableName       表名("OPC_TEST")
     * @param family 列簇名("OPC_COLUMNS")
     * @param column 列名("site")
     * @param value 值(采集点标识)
     * @param startMillis 开始时间毫秒值(建议传递当前时间前一小时的毫秒值，在保证查询效率的前提下获取到点位最新的记录
     * @return  Map<String,String>
     * @throws IOException io异常
     */
    public Map<String,String> scanBatchOfTable(String tableName,String family,String [] column,String [] value,Long startMillis,Long endMillis) throws IOException {

        if(Objects.isNull(column) || column.length != value.length) {
            return null;
        }

        FilterList filterList = new FilterList();

        for (int i = 0; i < column.length; i++) {
            SingleColumnValueFilter filter =  new SingleColumnValueFilter(Bytes.toBytes(family), Bytes.toBytes(column[i]), CompareOperator.EQUAL, Bytes.toBytes(value[i]));
            filterList.addFilter(filter);
        }

        Table table = connection.getTable(TableName.valueOf(tableName));

        Scan scan = new Scan();
        scan.setFilter(filterList);

        if(startMillis != null && endMillis != null) {
            scan.setTimeRange(startMillis,endMillis);
        }

        return getStringStringMap(table, scan);
    }

    /**
     *   根据条件取出点位最近时间的一条记录
     * expressions : scan 't1',{FILTER=>"PrefixFilter('2015')"}
     * @param tableName       表名("OPC_TEST")
     * @param family 列簇名("OPC_COLUMNS")
     * @param column 列名("site")
     * @param value 值(采集点标识)
     * @param startMillis 开始时间毫秒值(建议传递当前时间前一小时的毫秒值，在保证查询效率的前提下获取到点位最新的记录)
     * @return Map<String,String>
     * @throws IOException io异常
     */
    public Map<String,String> scanOneOfTable(String tableName, String family, String column, String value, Long startMillis, Long endMillis) throws IOException {
        Table table = connection.getTable(TableName.valueOf(tableName));

        Scan scan = new Scan();
        scan.setReversed(true);

        PageFilter pageFilter = new PageFilter(1);
        scan.setFilter(pageFilter);

        if(startMillis != null && endMillis != null) {
            scan.setTimeRange(startMillis,endMillis);
        }

        if (StringUtils.isNotBlank(column)) {
            SingleColumnValueFilter filter =  new SingleColumnValueFilter(Bytes.toBytes(family), Bytes.toBytes(column), CompareOperator.EQUAL, Bytes.toBytes(value));
            scan.setFilter(filter);
        }

        return getStringStringMap(table, scan);
    }

    private Map<String, String> getStringStringMap(Table table, Scan scan) throws IOException {
        ResultScanner scanner = table.getScanner(scan);
        Map<String,String> resultMap = new HashMap<>();
        try {
            for(Result result:scanner){
                for(Cell cell:result.rawCells()){
                    String values= Bytes.toString(CellUtil.cloneValue(cell));
                    String qualifier=Bytes.toString(CellUtil.cloneQualifier(cell));

                    resultMap.put(qualifier, values);
                }
            }
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }
        return resultMap;
    }


    /**
     * 判断表是否已经存在，这里使用间接的方式来实现
     * @param tableName 表名
     * @return boolean
     */
    public boolean tableExists(String tableName) {
        TableName[] tableNames = new TableName[0];
        try {
            tableNames = admin.listTableNames();
            if (tableNames != null && tableNames.length > 0) {
                for (TableName name : tableNames) {
                    if (tableName.equals(name.getNameAsString())) {
                        return true;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        return false;
    }

    /**
     * 批量插入
     * @param tableName
     * @param list <Put>
     */
    public void insertBach(String tableName,List<Put> list){
        Table table = null;
        try {
            table = connection.getTable(TableName.valueOf(tableName));
            table.put(list);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     *  同一个rowKey里批量添加column和value
     *
     * @param tableName HBase表名
     * @param rowKey    HBase表的rowkey
     * @param values     写入HBase表的值value
     * @param flag      提交标识符号。需要立即提交时，传递，值为 “end”
     */
    public void bulkPut(String tableName, String rowKey, String columnFamily, String [] columns, String [] values,String flag) {
        try {
            List<Put> list = threadLocal.get();
            if (list == null) {
                list = new ArrayList<Put>();
            }

            Put put = new Put(Bytes.toBytes(rowKey));

            for (int i = 0; i < columns.length; i++) {
                put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columns[i]), Bytes.toBytes(values[i]));
                list.add(put);
            }

            if (list.size() >= CACHE_LIST_SIZE || "end".equals(flag)) {

                Table table = connection.getTable(TableName.valueOf(tableName));
                table.put(list);

                list.clear();
            } else {
                threadLocal.set(list);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            threadLocal.remove();
        }
    }

    /**
     * 根据表名获取table
     */
    private Table getTable(String tableName) throws IOException {
        return connection.getTable(TableName.valueOf(tableName));
    }

    /**
     * 查询所有表的表名
     */
    public List<String> getAllTableNames() {
        List<String> result = new ArrayList<>();
        try {
            TableName[] tableNames = admin.listTableNames();
            for (TableName tableName : tableNames) {
                result.add(tableName.getNameAsString());
            }
        } catch (IOException e) {
            log.error("获取所有表的表名失败", e);
        } finally {
            close(null,null);
        }
        return result;
    }

    /**
     * 查询表中的所有数据
     * @param tableName
     * @return
     */
    public Map<String, Map<String, String>> getAllData(String tableName) {
        Scan scan = new Scan();
        return this.queryByScan(tableName, scan);
    }


    /**
     * 通过表名及过滤条件查询数据
     * @param tableName
     * @param scan
     * @return
     */
    public Map<String, Map<String, String>> queryByScan(String tableName, Scan scan) {
        long s = System.currentTimeMillis();
        // <rowKey,对应的行数据>
        Map<String, Map<String, String>> result = new HashMap<>();
        ResultScanner rs = null;
        //获取表
        Table table = null;
        try {
            table = getTable(tableName);
            rs = table.getScanner(scan);
            for (Result r : rs) {
                // 每一行数据
                Map<String, String> columnMap = new HashMap<>();
                String rowKey = null;
                // 行键，列族和列限定符一起确定一个单元（Cell）
                for (Cell cell : r.listCells()) {
                    if (rowKey == null) {
                        rowKey = Bytes.toString(cell.getRowArray(), cell.getRowOffset(), cell.getRowLength());
                    }
                    columnMap.put(
                            //列限定符
                            Bytes.toString(cell.getQualifierArray(), cell.getQualifierOffset(), cell.getQualifierLength()),
                            //列族
                            Bytes.toString(cell.getValueArray(), cell.getValueOffset(), cell.getValueLength()));
                }
                if (rowKey != null) {
                    result.put(rowKey, columnMap);
                }
            }
        } catch (IOException e) {
            log.error(MessageFormat.format("遍历查询指定表中的所有数据失败,tableName:{0}", tableName), e);
        } finally {
            close(rs, table);
        }
        long e = System.currentTimeMillis();
        log.info("Hbase Query Result Row Number:{}, Use Times:{}ms",result.size(), (e-s));
        return result;
    }

    /**
     * 通过特定格式的RowKey进行查询
     * @param tableName
     * @param index
     * @param st
     * @param et
     * @return
     */
    public Map<String, Map<String,String>> queryByIndex(String tableName, String index, String st, String et){
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
//            Date sd = sf.parse(st);
//            Date ed = sf.parse(et);

            Scan scan = new Scan();
//            scan.withStartRow(Bytes.toBytes(new StringBuffer(index).reverse().toString() + "_" + sd.getTime()));
//            scan.withStopRow(Bytes.toBytes(new StringBuffer(index).reverse().toString() + "_" + ed.getTime()));
            scan.withStartRow(Bytes.toBytes(new StringBuffer(index).reverse().toString() + "_" + st));
            scan.withStopRow(Bytes.toBytes(new StringBuffer(index).reverse().toString() + "_" + et));
            return queryByScan(tableName, scan);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 读取一个文件批量入库
     * @param filePath
     * @param tableName
     * @param rowkeyIndex
     * @param tsIndex
     */
    public void insertBatchByFile(String filePath, String tableName, int rowkeyIndex, int tsIndex){
        log.info("Start Read File To Hbase:{}", filePath);
        long s = System.currentTimeMillis();
        try {
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            int r = 0;
            List<Put> list = new ArrayList<>();
            StringBuilder line = null;
            int row = 0;
            int n = 0;
            if(!this.tableExists(tableName)){
                this.createTable(tableName,"c");
            }
            while (br.ready()){
                line = new StringBuilder(br.readLine());
                String index = line.substring(getFromIndex(line, "\\|", rowkeyIndex) + 1, getFromIndex(line, "\\|", rowkeyIndex + 1));
                String ts = line.substring(getFromIndex(line, "\\|", tsIndex) + 1, getFromIndex(line, "\\|", tsIndex + 1));
                if(index != "" && ts != ""){
                    // 把日期字符串转为unix时间
//                    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    Date d = sf.parse(ts.substring(0,18));
//                    String tsUnix = d.getTime() + ts.substring(20,25);
                    Put put = new Put(Bytes.toBytes(new StringBuffer(index).reverse().toString() + "_" + ts));
                    put.addColumn(Bytes.toBytes("c"), Bytes.toBytes("i"),Bytes.toBytes(String.valueOf(line)));
                    list.add(put);

                    n++;
                    row++;
                    // 单机测试，文件25MB，18229行
                    // 2000行提交一次耗时21898ms
                    // 5000行提交一次耗时18563ms
                    // 10000行提交一次耗时13937ms
                    // 一次性提交3539ms
//                    if(row >= 10000){
//                        this.insertBach(tableName, list);
//                        list.clear();
//                    }
                }
            }

            if(row > 0){
                this.insertBach(tableName, list);
            }

            br.close();
            long e = System.currentTimeMillis();
            log.info("End Read File To Hbase:{}, Row Number:{}, Times:{}ms",filePath, n, e-s);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 查找XDR话单中指定字符串的位置，比分割字符串效率高
     * @param str
     * @param modelStr
     * @param count
     * @return
     */
    public int getFromIndex(StringBuilder str, String modelStr, Integer count) {
        //对子字符串进行匹配
        Matcher slashMatcher = Pattern.compile(modelStr).matcher(str);
        int index = 0;
        //matcher.find();尝试查找与该模式匹配的输入序列的下一个子序列
        while (slashMatcher.find()) {
            index++;
            //当modelStr字符第count次出现的位置
            if (index == count) {
                break;
            }
        }
        //matcher.start();返回以前匹配的初始索引。
        return slashMatcher.start();
    }


    public void close(){
        if (admin != null) {
            try {
                admin.close();
            } catch (IOException e) {
                log.error("关闭Admin失败", e);
            }
        }

        if(connection != null){
            try {
                connection.close();
            } catch (IOException e) {
                log.error("关闭Admin失败", e);
            }
        }
    }

    /**
     * 关闭流
     */
    private void close(ResultScanner rs, Table table) {
        if (admin != null) {
            try {
                admin.close();
            } catch (IOException e) {
                log.error("关闭Admin失败", e);
            }
            if (rs != null) {
                rs.close();
            }
            if (table != null) {
                assert rs != null;
                rs.close();
            }
            if (table != null) {
                try {
                    table.close();
                } catch (IOException e) {
                    log.error("关闭Table失败", e);
                }
            }
        }
    }
 }
```

### HbaseTest.java

```java
import com.javastudy.bigdata.hbase.hbaseClient.HBaseService;
import org.apache.hadoop.hbase.client.Scan;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HbaseTest {


    @Autowired
    private HBaseService hBaseService;

    @Test
    public void getAllTables(){
        List<String> list = hBaseService.getAllTableNames();
        for(String s : list){
            System.out.println(s);
        }
    }

    @Test
    public void insertBatch(){
        // 3表示msisdn的位置，10表示ts_start位置
        hBaseService.insertBatchByFile(System.getProperty("user.dir") + "/file/xdr/s1sipflvolte_201908130548.0.15271.csv", "s1sipflvolte", 3,10);
    }

    @Test
    public void getLimit(){
        Scan scan = new Scan();
        scan.setLimit(5);
        print(hBaseService.queryByScan("s1sipflvolte", scan));
    }

    @Test
    public void queryByMsisdn(){
        Scan scan = new Scan();
        print(hBaseService.queryByIndex("s1sipflvolte","8615120631600","2019-08-13 00:00:00","2019-08-13 10:00:00"));
    }

    @Test
    public void tt(){
        String tableName = "test";

        System.out.println("==============删除表:" + tableName);
//        hBaseService.deleteTable(tableName);

        System.out.println("==============创建表:" + tableName);
        hBaseService.createTable(tableName,"cf1","cf2");

        System.out.println("==============插入数据：");
        hBaseService.insertOrUpdate(tableName,"1","cf1","col1","v1");
        hBaseService.insertOrUpdate(tableName,"2","cf2","col2","v2");
        hBaseService.insertOrUpdate(tableName,"3","cf1","col1","v3");
        hBaseService.insertOrUpdate(tableName,"4","cf2","col2","v4");

        System.out.println("==============获取表全部数据:");
        print(hBaseService.getAllData(tableName));

        System.out.println("==============获取具体某一个值：");
        String v = hBaseService.getValue(tableName,"2","cf2", "col2");
        System.out.println(v);

        System.out.println("==============用rowkey获取值：");
        print(hBaseService.getOneByRow(tableName, "3"));

        System.out.println("==============用rowkey区间过滤");
        Scan scan = new Scan();
        scan.withStartRow("1".getBytes());
        scan.withStopRow("3".getBytes());
        print(hBaseService.queryByScan(tableName,scan));
    }

    public void print(Map<String, Map<String,String>> map){
        map.forEach((k,v)->{
            System.out.println("key：" + k + ",====value：" + v);
        });
    }
}
```

## 通过spring-boot-starter-hbase

###  pom.xml 

```xml
		<dependency>
            <groupId>com.spring4all</groupId>
            <artifactId>spring-boot-starter-hbase</artifactId>
            <version>1.0.0.RELEASE</version>
        </dependency>
```

### application

```properties
## HBase 配置
#指定 HBase 的 zk 地址
spring.data.hbase.quorum=192.168.17.130:2181
#指定 HBase 在 HDFS 上存储的路径,通过hbase-site.xml查看
#spring.data.hbase.rootDir=/hbase-data
#指定 ZK 中 HBase 的根 ZNode，下面是docker启的hbase配置
#spring.data.hbase.nodeParent=/hbase 
```

或

```yaml
## HBase 配置
hbase:
  config:
    hbase:
      zookeeper:
        quorum: 192.168.17.130
        port: 2182
        znode:/hbase-data
      client:
        keyvalue:
          maxsize: 1572864000
```

### 代码结构：

gitee：https://gitee.com/sanhenlei/javastudy/tree/master/src/main/java/com/javastudy/bigdata/hbase/hbasestarter

![image-20210301161226884](image/HBase/image-20210301161226884.png)

### CityServiceImpl.java

```java
import com.javastudy.bigdata.hbase.hbasestarter.dao.CityRowMapper;
import com.javastudy.bigdata.hbase.hbasestarter.domain.City;
import com.javastudy.bigdata.hbase.hbasestarter.service.CityService;
import com.spring4all.spring.boot.starter.hbase.api.HbaseTemplate;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.hbase.client.Mutation;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Service
public class CityServiceImpl implements CityService {
    {
        System.setProperty("hadoop.home.dir", "E:\\vmware\\data\\soft\\hadoop\\hadoop-common-2.2.0-bin-master");
    }
    @Autowired
    private HbaseTemplate hbaseTemplate;

    /**
     * 根据开始和结束rowid获取数据
     * @param tableName
     * @param startRow
     * @param stopRow
     * @return
     */
    public List<City> query(String tableName,String startRow, String stopRow){
        log.info("Start Query Table:{}, startRow:{}, stropRow:{}", tableName, startRow, stopRow);
        long s = System.currentTimeMillis();
        Scan scan = new Scan();
        scan.withStartRow(Bytes.toBytes(startRow));
        scan.withStopRow(Bytes.toBytes(stopRow));
        scan.setCaching(5000);
        List<City> list = this.hbaseTemplate.find(tableName, scan, new CityRowMapper());
        long e = System.currentTimeMillis();
        log.info("End Query Table, Times:{}ms", e-s);
        return list;
    }

    /**
     * 根据rowid获取某一行数据
     * @param tableName
     * @param row
     * @return
     */
    public City get(String tableName,String row){
        return this.hbaseTemplate.get(tableName, row, new CityRowMapper());
    }

    /**
     * 插入或更新一条数据
     * @param tableName
     * @param family
     * @param id
     * @param name
     * @param age
     */
    public void saveOrUpdate(String tableName, String family, String id, String name, String age){
        List<Mutation> saveUpdate = new ArrayList<>();
        Put put = new Put(Bytes.toBytes(String.valueOf(id)));
        put.addColumn(Bytes.toBytes(family),Bytes.toBytes("id"),Bytes.toBytes(id));
        put.addColumn(Bytes.toBytes(family),Bytes.toBytes("name"),Bytes.toBytes(name));
        put.addColumn(Bytes.toBytes(family),Bytes.toBytes("age"),Bytes.toBytes(age));
        saveUpdate.add(put);
        this.hbaseTemplate.saveOrUpdates(tableName, saveUpdate);
    }

    /**
     * 读取一个文件批量入库
     * @param filePath
     * @param tableName
     * @param rowkeyIndex
     * @param tsIndex
     */
    public void insertBatch(String filePath, String tableName, int rowkeyIndex, int tsIndex){
        log.info("Start Read File To Hbase:{}", filePath);
        long s = System.currentTimeMillis();
        try {
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            int r = 0;
            List<Mutation> list = new ArrayList<>();
            StringBuilder line = null;
            int row = 0;
            int n = 0;

            while (br.ready()){
                line = new StringBuilder(br.readLine());
                String index = line.substring(getFromIndex(line, "\\|", rowkeyIndex) + 1, getFromIndex(line, "\\|", rowkeyIndex + 1));
                String ts = line.substring(getFromIndex(line, "\\|", tsIndex) + 1, getFromIndex(line, "\\|", tsIndex + 1));
                // 把日期字符串转为unix时间
                SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date d = sf.parse(ts.substring(0,18));
                String tsUnix= d.getTime() + ts.substring(20,25);
                Put put = new Put(Bytes.toBytes(new StringBuffer(index).reverse().toString() + "_" + tsUnix));
                put.addColumn(Bytes.toBytes("c"), Bytes.toBytes("i"),Bytes.toBytes(String.valueOf(line)));
                list.add(put);

                n++;
                row++;
                if(row >= 2000){
                    this.hbaseTemplate.saveOrUpdates(tableName, list);
                    list.clear();
                }
            }

            if(row > 0){
                this.hbaseTemplate.saveOrUpdates(tableName, list);
            }

            br.close();
            long e = System.currentTimeMillis();
            log.info("End Read File To Hbase:{}, Row Number:{}, Times:{}ms",filePath, n, e-s);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    /**
     * 查找XDR话单中指定字符串的位置，比分割字符串效率高
     * @param str
     * @param modelStr
     * @param count
     * @return
     */
    public int getFromIndex(StringBuilder str, String modelStr, Integer count) {
        //对子字符串进行匹配
        Matcher slashMatcher = Pattern.compile(modelStr).matcher(str);
        int index = 0;
        //matcher.find();尝试查找与该模式匹配的输入序列的下一个子序列
        while (slashMatcher.find()) {
            index++;
            //当modelStr字符第count次出现的位置
            if (index == count) {
                break;
            }
        }
        //matcher.start();返回以前匹配的初始索引。
        return slashMatcher.start();
    }
}
```

# 性能优化

## 写入优化

非集群、非大表写入仅参考

**单机测试，文件25MB，18229行**
	 2000行提交一次耗时21898ms
	 5000行提交一次耗时18563ms
	 10000行提交一次耗时13937ms
	 `一次性提交耗时3539ms`



## 查询优化

Hbase虽然能提供海量数据的实时读写，但是一旦数据量非常大，查询延迟也会非常高，所以要做好优化工作。

### 一、表结构设计

#### 1、列族越少越好

（1）列族（cf）数量，在内存结构中一个cf对应一个store区域，一个store中又存在多个storefile小文件，小storefile是不断合并新的大的storefile，数据量大了，storefile自然会多，合并任务也自然增多，会降低性能，增加列族性能会更有甚之。

（2）由于数据的备份、迁移、合并等操作都是基于列族层面进行的。列族少，也会减少数据备份、迁移、合并过程中内存、磁盘IO的耗时。

（3）每个列族读写次数不一样，假设A列族数据量很大，B列族数据量较小， 当A列族，memstore数据达到阀值时候，就会flush到磁盘中，这时会带动B列族的memstore刷新到内存中，导致B列族频繁flush，增加不必要的磁盘IO操作。

#### 2、参数设置

把IN_MEMORY 设置为true，开启内存缓存，默认为false

### 二、rowKey的设计

rowKey即行健，相当于一级索引，根据业务需要，设计合理可以大大提高hbase查询效率

#### 1、均匀分布

rowKey尽量要短，写入要分散，分布均匀，根据业务合理设置预分区，避免热点写

常见的有加随机数、hash计算、反转

（1）随机数俗称加salt，就是在rowkey前加随机数，如下，在前边添加a、b、c....之类的随机数。

| rowkey      | salt_rowkey   |
| ----------- | ------------- |
| 15738862581 | a_15738862581 |
| 15837456185 | b_15837456185 |
| 15837456189 | c_15837456189 |

这种rowkey设计可以均匀分布在各个Region中，但是它打乱了自然的字典排序，是不利于scan的

（2）hash计算  计算rowkey的hash值，并取前N位与原rowkey拼接

| rowkey      | hash_rowkey         |
| ----------- | ------------------- |
| 15738862581 | 2f5f2eb_15738862581 |
| 15837456175 | cb4ght_15837456175  |
| 15837456159 | eh4kj3_15837456159  |

 这种rowkey一定程度均匀分布rowkey，对单个随机查询效率较高，但是也是不利于我们scan的

（3）反转就是将一段长度或者全部长度的rowkey进行倒序,如下将后四位反转后拼接

| rowkey      | reverse_rowkey |
| ----------- | -------------- |
| 15738862581 | 1852_573886    |
| 15837456175 | 5716_583745    |
| 15837456159 | 9516_1583745   |

 这种打乱了rowkey，牺牲了行排序

#### 2、存储设计

场景：查询用户所有订单，用户查询每个月的订单按时间由近及远排序，及时间倒序

这是我们就要利用hbase rowkey的字典排序特性来合理设计rowkey，以提高查询速度。由于每个查询都是基于某个用户的时间倒序。所以这里rowkey的设计用户id+时间戳timestamp来作为rowkey，这样一个用户的订单信息就会连续存储在一起，查询效率自然提高。如id=10000001，timestamp=1536425757188，rowkey=10000001_1536425757188。这样rowkey是使一个用户的订单连续分布一起了，但是时间正序，不符合倒序要求。因此要进一步优化，这时我们只需timestamp倒序就行了，用一个大数减去timestamp，timestamp=9223370500429018619=long.max-1536425757188 ，rowkey=10000001_9223370500429018619 如这样时间同一个用户最近的订单就排在最前面了。

rowkey：uid_(long.max-timestamp)，防止热点也可以加salt，salt_uid_(long.max-timestamp)
存储主要代码如下：

```java
public static void addOneRecord()throws Exception{
    	Table table = null;
       
        table = connection.getTable(TableName.valueOf(tableName));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        //rowKey id+时间倒序
        String rowKey= "100001"+(Long.MAX_VALUE - sdf.parse(new Date()).getTime());
 
		Put put = new Put(Bytes.toBytes(rowKey));
		put.addColumn(Bytes.toBytes(family), Bytes.toBytes(qualifier),Bytes.toBytes(value));
		table.put(put);
}
```



查询主要代码如下：

```java
public void queryAll throws Exception {
		// 查询 100001  8月份 所有的订单
		Scan scan = new Scan();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		
		String startRow = "100001" + (Long.MAX_VALUE - sdf.parse("20180901000000").getTime());
		String stopRow = "100001_" + (Long.MAX_VALUE - sdf.parse("20180801000000").getTime());
		
		scan.setStartRow(startRow.getBytes());
		scan.setStopRow(stopRow.getBytes());
		
		ResultScanner rss = hTable.getScanner(scan);
		for(Result rs : rss) {
			System.out.print(new String(CellUtil.cloneValue(rs.getColumnLatestCell("cf".getBytes(), "dnum".getBytes()))));
			System.out.print(" " + new String(CellUtil.cloneValue(rs.getColumnLatestCell("cf".getBytes(), "date".getBytes()))));
			System.out.println(" " + new String(CellUtil.cloneValue(rs.getColumnLatestCell("cf".getBytes(), "type".getBytes()))));
		}
}
```

#### 3、巧用过滤器

假设用户的订单有四个状态，1 待支付、2代发货、3待收货、 4已完成，用户要查询待发货的订单，这时要用到过滤器，减少查询量，提高查询速度

```java
public void query() throws Exception {
		FilterList list = new FilterList(FilterList.Operator.MUST_PASS_ALL);
		
		// 过滤器1、前缀过滤,用户过滤
                String id=100001;
		PrefixFilter filter1 = new PrefixFilter(id.getBytes());
		list.addFilter(filter1);
		
		String type=3;//待收货
		// 过滤器2、过滤type=3的用户
		SingleColumnValueFilter filter2 = new SingleColumnValueFilter("cf".getBytes(), "type".getBytes(), CompareOp.EQUAL, type.getBytes());
		list.addFilter(filter2);
		
		Scan scan = new Scan();
		scan.setFilter(list);
		
		ResultScanner rss = hTable.getScanner(scan);
		for(Result rs : rss) {
			System.out.print(new String(CellUtil.cloneValue(rs.getColumnLatestCell("cf".getBytes(), "num".getBytes()))));
			System.out.print(" " + new String(CellUtil.cloneValue(rs.getColumnLatestCell("cf".getBytes(), "date".getBytes()))));
			System.out.println(" " + new String(CellUtil.cloneValue(rs.getColumnLatestCell("cf".getBytes(), "type".getBytes()))));
		}
}
```





# 参考

https://blog.csdn.net/qq_26803795/article/details/106239302?ops_request_misc=%25257B%252522request%25255Fid%252522%25253A%252522161268483016780262574608%252522%25252C%252522scm%252522%25253A%25252220140713.130102334.pc%25255Fall.%252522%25257D&request_id=161268483016780262574608&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_v2~hot_rank-2-106239302.pc_search_result_before_js&utm_term=HBASE%25E8%25AF%25A6%25E8%25A7%25A3

https://blog.csdn.net/lukabruce/article/details/80624619?ops_request_misc=%25257B%252522request%25255Fid%252522%25253A%252522161268418316780274160348%252522%25252C%252522scm%252522%25253A%25252220140713.130102334.pc%25255Fall.%252522%25257D&request_id=161268418316780274160348&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_v2~hot_rank-2-80624619.pc_search_result_before_js&utm_term=HBASE

https://blog.csdn.net/nlx1450161741/article/details/107689770?ops_request_misc=%25257B%252522request%25255Fid%252522%25253A%252522161372720716780299013433%252522%25252C%252522scm%252522%25253A%25252220140713.130102334..%252522%25257D&request_id=161372720716780299013433&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduend~default-3-107689770.first_rank_v2_pc_rank_v29&utm_term=hbase+springboot

https://blog.csdn.net/weixin_43770982/article/details/89378846?ops_request_misc=&request_id=&biz_id=102&utm_term=hbase%2520springboot&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-8-89378846.first_rank_v2_pc_rank_v29

https://blog.csdn.net/lzxlfly/article/details/82314924?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.control&dist_request_id=05a53ea0-0584-414e-888a-37cf5619393f&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.control
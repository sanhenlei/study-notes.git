## Redis是什么？

是一个开源的、使用ANSI C语言编写的、遵守BSD歇息、支持网络交互的、可基于内存也可持久化的Key-Value数据库。

redis的官网：https://redis.io/

## 应用场景

参考：https://www.php.cn/redis/422103.html

1. **缓存**：

   在中大型网站中缓存是必不可少的，合理的利用缓存不仅能够提升网站访问速度，还能大大降低数据库的压力。Redis提供了键过期功能，也提供了灵活的键淘汰策略，所以，Redis用在缓存的场合非常多。

2. **排行榜**

   很多网站都有排行榜应用的，如京东的月度销量榜、商品按时间的上新排行榜等。Redis提供的有序集合数据类能够实现各种复杂的排行榜应用。

3. **计数器**

   如电商网站商品的浏览量、视频网站视频的播放量等，为了保证数据实时性，每次浏览器都要给+1，并发量高时如果每次都请求数据库操作无疑是种挑战和压力。Redis提供的incr命令（将key中存储的数字值加1）来实现计数器功能，内存操作，性能非常好 ，非常使用于这些计数场景。

4. **分布式会话**

   集群模式下，在应用不多的情况下一般使用容器自带的session复制功能就能满足，当应用增多相对复杂的系统中，一般都会搭建以Redis等内存数据库为中心的session服务，session不再由容器管理，而是由session服务及内存数据库管理。

5. **分布式锁**

   在很多互联网公司中都使用了分布式技术，分布式技术带来的技术挑战是对同一个资源的并发访问，如全局ID/减库存、秒杀等场景，并发量不大的场景可以使用数据库的`悲观锁`、`乐观锁`来实现，但在并发量高的场景中，利用数据库锁来控制资源的并发访问是不太理想的，大大影响了数据库的性能。可以利用Resis的`setnx`功能来编写分布式锁，如果设置返回1说明获取锁成功，否则获取锁失败，实际应用中要考虑的细节要更多。

6. **社交网络**

   点赞、踩、关注、被关注、共同好友等是社交网站的基本功能，社交网站的访问量通常来说比较大，而且传统的关系数据库类型不适合存储这种类型的数据，Redis提供的哈希、集合等数据结构能很方便的实现这些功能。

7. **最新列表**

   Redis列表结构，LPUSH可以在列表头部插入一个内容ID作为关键字，LTRIM可用来限制列表的数量，这样列表永远为N个ID，无需查询最新的列表，直接根据ID去到对应的内容页即可。

8. **消息系统**

   消息队列是大型网站必用中间件，如ActiveMQ、RabbitMQ、Kafka等流行的消息队列中间件，主要用于业务解耦、流量削峰及异步处理实时性低的业务。Redis提供了发布、订阅及阻塞队列功能，能实现一个简单的消息队列系统。

   

## 为什么使用Redis？

主要从两个角度考虑:`性能和并发`。redis还具备可以做分布式锁等功能。如果只是为了分布式锁，完全还有其他中间件（如zookeeper等）代替，并不是非要使用redis。因此，主要从性能和并发两个角度去考虑。

### 性能

如下图，我们再碰到需要执行耗时特别久，且结果不频繁变动的SQL，就特别适合运行结果放入缓存。这样，后面的请求就去缓存中读取，使得请求迅速响应。

![image-20200604114632285](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/Redis/image-20200604114632285.png)

### 并发

如下图，在大并发的情况下，所有的请求直接访问数据库，数据库会出现连接异常。这个时候，就需要使用redis做一个缓冲操作，让请求先访问到redis，而不是直接访问数据库。

## 安装

从redis.io下载最新版redis-X.Y.Z.tar.gz后解压，然后进入redis-X.Y.Z文件夹后直接make即可，安装非常简单。

### 第一步：下载redis安装包

wget http://download.redis.io/releases/redis-4.0.6.tar.gz

### 第二步：解压压缩包

tar -zxvf redis-4.0.6.tar.gz

### 第三步：yum安装gcc依赖

先查看是否有gcc

gcc -v

没有就安装

yum install gcc

### 第五步：编译安装

make MALLOC=libc　

cd src && make install

![image-20200604160943689](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/Redis/image-20200604160943689.png)

## 启动redis的三种方式

### 1、直接启动redis

进入src目录

cd /opt/redis-4.0.6/src

./redis-server

![image-20200604161538138](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/Redis/image-20200604161538138.png)

如上图：redis启动成功，但是这种启动方式需要一直打开窗口，不能进行其他操作，不太方便.

按ctrl + c可以关闭窗口

### 2、以后台进程方式启动redis

第一步：修改redis.conf文件

将`daemonize no`修改为`daemonize yes`

解决虚拟机访问不了的问题，用#将bind这句注释掉，修改protected-mode 为no

![image-20200604171652369](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/Redis/image-20200604171652369.png)

第二步：指定redis.conf文件启动

```shell
[root@localhost src]# ./redis-server /opt/redis-4.0.6/redis.conf      
17855:C 04 Jun 16:19:21.268 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
17855:C 04 Jun 16:19:21.269 # Redis version=4.0.6, bits=64, commit=00000000, modified=0, pid=17855, just started
17855:C 04 Jun 16:19:21.269 # Configuration loaded
[root@localhost src]# 
```

第三步：关闭redis进程

首先使用ps -aux | grep redis查看redis进程

使用kill命令杀死进程

### 3、设置redis开机自启动

1、在/etc目录下新建redis目录

mkdir -p /etc/redis

2、将/opt/redis-4.0.6/redis.conf 文件复制一份到/etc/redis目录下，并命名为6379.conf

cp /opt/redis-4.0.6/redis.conf /etc/redis/6379.conf

3、将redis的启动脚本复制一份放到/etc/init.d目录下

cp /opt/redis-4.0.6/utils/redis_init_script /etc/init.d/redisd

4、设置redis开机自启动

先切换到/etc/init.d目录下

cd /etc/init.d

vim redisd  ,在第一行加入如下两行注释，保存退出

```
# chkconfig:   2345 90 10
# description:  Redis is a persistent key-value database
```

然后执行自启命令

```
chkconfig redisd on
```

现在可以直接已服务的形式启动和关闭redis了

启动：

service redisd start

关闭：

方法1：service redisd stop

方法2：redis-cli SHUTDOWN



## 常用配置

**比较重要的3个可执行文件：**

redis-server：Redis服务器程序

redis-cli：Redis客户端程序，它是一个命令行操作工具。也可以使用telnet根据其纯文本协议操作。

redis-benchmark：Redis性能测试工具，测试Redis在你的系统及配置下的读写性能。

**修改 redis.conf文件**
daemonize yes --- 修改为yes  后台启动

requirepass 123456  ----注释取消掉设置账号密码

ps aux | grep '6379'  --- 查询端口

service iptables stop 停止防火墙

## redis命令

**redis命令连接方式**
./redis-cli -h 127.0.0.1 -p 6379 -a "123456"  --- redis 使用账号密码连接

### 连接操作

- ping：测试连接是否存活，结果为PONG  表示成功
- echo：打印
- quit：关闭连接
- auth：简单密码认证

### 数据库操作

- select 1：切换到指定的数据库，数据库索引号index用数字值指定，以0作为起始索引值
- dbsize：查看当前数据库key的数量
- flushdb：删除当前选择数据库中的所有key
- flushall：删除所有数据库中的所有key
- move key db 将某key移动到某个db中去

### 服务端命令

- time：返回当前服务器时间
- client list: 返回所有连接到服务器的客户端信息和统计数据 参见http://redisdoc.com/server/client_list.html
- client kill ip:port：关闭地址为 `ip:port` 的客户端
- save：将数据同步保存到磁盘
- bgsave：将数据异步保存到磁盘
- lastsave：返回上次成功将数据保存到磁盘的Unix时戳
- shundown：将数据同步保存到磁盘，然后关闭服务
- info：提供服务器的信息和统计
- info commandstats：显示每个命令执行了多少次，执行命令所耗费的毫秒数（总时间和平均时间）
- config resetstat：重置info命令中的某些统计数据
- config get：获取配置文件信息
- config set：动态地调整 Redis 服务器的配置(configuration)而无须重启，可以修改的配置参数可以使用命令 `CONFIG GET *` 来列出
- config rewrite：Redis 服务器时所指定的 `redis.conf` 文件进行改写
- monitor：实时转储收到的请求
- slaveof：改变复制策略设置

### 发布订阅命令

- psubscribe：订阅一个或多个符合给定模式的频道 例如psubscribe news.* tweet.*
- publish：将信息 `message` 发送到指定的频道 `channel 例如publish msg "good morning"`
- pubsub channels：列出当前的活跃频道 例如PUBSUB CHANNELS news.i*
- pubsub numsub：返回给定频道的订阅者数量 例如PUBSUB NUMSUB news.it news.internet news.sport news.music
- pubsub numpat：返回客户端订阅的所有模式的数量总和
- punsubscribe：指示客户端退订所有给定模式。
- subscribe：订阅给定的一个或多个频道的信息。例如 subscribe msg chat_room
- unsubscribe：指示客户端退订给定的频道。

### 对KEY的操作

- exists key：确认一个key是否存在
- del key：删除一个key
- type key：返回值的类型
- keys(pattern)：返回满足给定pattern的所有key
- randomkey：随机返回key空间的一个
- keyrename(oldname, newname)：重命名key
- dbsize：返回当前数据库中key的数目
- expire：设定一个key的活动时间（s）
- ttl：获得一个key的活动时间
- move(key, dbindex)：移动当前数据库中的key到dbindex数据库
- flushdb：删除当前选择数据库中的所有key
- flushall：删除所有数据库中的所有key

### **对String操作的命令**

- set (key, value)：给数据库中名称为key的string赋予值value
- get (key)：返回数据库中名称为key的string的value
- getset (key, value)：给名称为key的string赋予上一次的value
- mget (key1, key2,…, key N)：返回库中多个string的value
- setnx (key, value)：只有在key不存在时才添加key和value
- setex (key, time, value)：向库中添加string，设定过期时间time
- mset (key N, value N)：批量设置多个string的值
- msetnx(key N, value N)：如果所有名称为key i的string都不存在
- incr(key)：名称为key的string增1操作
- incrby(key, integer)：名称为key的string增加integer
- decr(key)：名称为key的string减1操作
- decrby(key, integer)：名称为key的string减少integer
- append(key, value)：名称为key的string的值附加value
- substr(key, start, end)：返回名称为key的string的value的子串

### **对List操作的命令**

- rpush(key, value)：在名称为key的list尾添加一个值为value的元素
- lpush(key, value)：在名称为key的list头添加一个值为value的 元素
- llen(key)：返回名称为key的list的长度
- lrange(key, start, end)：返回名称为key的list中start至end之间的元素
- ltrim(key, start, end)：截取名称为key的list
- lindex(key, index)：返回名称为key的list中index位置的元素
- lset(key, index, value)：给名称为key的list中index位置的元素赋值
- lrem(key, count, value)：删除count个key的list中值为value的元素
- lpop(key)：返回并删除名称为key的list中的首元素
- rpop(key)：返回并删除名称为key的list中的尾元素
- blpop(key1, key2,… key N, timeout)：lpop命令的block版本。
- brpop(key1, key2,… key N, timeout)：rpop的block版本。
- rpoplpush(srckey, dstkey)：返回并删除名称为srckey的list的尾元素，并将该元素添加到名称为dstkey的list的头部

### **对Set操作的命令**

- sadd(key, member)：向名称为key的set中添加元素member
- srem(key, member) ：删除名称为key的set中的元素member
- spop(key) ：随机返回并删除名称为key的set中一个元素
- smove(srckey, dstkey, member) ：移到集合元素
- scard(key) ：返回名称为key的set的基数
- sismember(key, member) ：member是否是名称为key的set的元素
- sinter(key1, key2,…key N) ：求交集
- sinterstore(dstkey, (keys)) ：求交集并将交集保存到dstkey的集合
- sunion(key1, (keys)) ：求并集
- sunionstore(dstkey, (keys)) ：求并集并将并集保存到dstkey的集合
- sdiff(key1, (keys)) ：求差集
- sdiffstore(dstkey, (keys)) ：求差集并将差集保存到dstkey的集合
- smembers(key) ：返回名称为key的set的所有元素
- srandmember(key) ：随机返回名称为key的set的一个元素


**对Hash操作的命令**

- hset(key, field, value)：向名称为key的hash中添加元素field
- hget(key, field)：返回名称为key的hash中field对应的value
- hmget(key, (fields))：返回名称为key的hash中field i对应的value
- hmset(key, (fields))：向名称为key的hash中添加元素field
- hincrby(key, field, integer)：将名称为key的hash中field的value增加integer
- hexists(key, field)：名称为key的hash中是否存在键为field的域
- hdel(key, field)：删除名称为key的hash中键为field的域
- hlen(key)：返回名称为key的hash中元素个数
- hkeys(key)：返回名称为key的hash中所有键
- hvals(key)：返回名称为key的hash中所有键对应的value
- hgetall(key)：返回名称为key的hash中所有的键（field）及其对应的value

## 性能测试

### 使用自带命令测试

```shel
cd /opt/redis-4.0.6/src

[root@localhost src]# ./redis-benchmark -n 10000 -q
PING_INLINE: 56179.77 requests per second
PING_BULK: 64935.07 requests per second
SET: 53475.93 requests per second
GET: 63291.14 requests per second
INCR: 63291.14 requests per second
LPUSH: 62111.80 requests per second
RPUSH: 62500.00 requests per second
LPOP: 62111.80 requests per second
RPOP: 62893.08 requests per second
SADD: 63694.27 requests per second
HSET: 61728.39 requests per second
SPOP: 64516.13 requests per second
LPUSH (needed to benchmark LRANGE): 61349.69 requests per second
LRANGE_100 (first 100 elements): 33783.79 requests per second
LRANGE_300 (first 300 elements): 16260.16 requests per second
LRANGE_500 (first 450 elements): 11947.43 requests per second
LRANGE_600 (first 600 elements): 9389.67 requests per second
MSET (10 keys): 55248.62 requests per second

[root@localhost src]#
[root@localhost src]# redis-benchmark -h 127.0.0.1 -p 6379 -t set,lpush -n 10000 -q
SET: 48780.49 requests per second
LPUSH: 61349.69 requests per second

[root@localhost src]# 
```



## Java操作redis

### 使用SpringBoot操作

参考：https://blog.csdn.net/qq_22211217/article/details/80463053

#### 1、添加依赖

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```
#### 2、配置文件

```properties

#==========redis 配置信息===========#
 
#（单机redis）数据库ip
spring.redis.host=127.0.0.1
#（单机redis）数据库端口
spring.redis.port= 6379
 
#（cluster集群，不使用则不用开启）在群集中执行命令时要遵循的最大重定向数目。
#spring.redis.cluster.max-redirects=3
#（cluster集群，不使用则不用开启）以逗号分隔的“主机：端口”对列表进行引导。
#spring.redis.cluster.nodes=127.0.0.1:7000,127.0.0.1:7001,127.0.0.1:7002,127.0.0.1:7003,127.0.0.1:7004,127.0.0.1:7005
 
#（哨兵模式，不使用则不用开启）Redis服务器的名称。
# spring.redis.sentinel.master=
#（哨兵模式，不使用则不用开启）主机：端口对的逗号分隔列表。
# spring.redis.sentinel.nodes=
 
#数据库指定索引
spring.redis.database= 1
#数据库密码
spring.redis.password=
#超时时间
spring.redis.timeout= 5000
#连接池最大连接数，负值表示不限制
spring.redis.jedis.pool.max-active= 1000
#连接池中最大空闲连接
spring.redis.jedis.pool.max-idle= 10
#连接池中最小空闲连接
spring.redis.jedis.pool.min-idle= 2
#连接池最大阻塞等待时间，负值表示不限制
spring.redis.jedis.pool.max-wait= -1
```

#### 3、项目结构

![img](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/Redis/20180526165510474)

#### 4、代码展示

##### RedisUtil.java

```java
package com.zyj.redis;
 
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.CollectionUtils;
 
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
 
/**
*@Description: redis工具类
*@Author: zyj 2018/5/26 8:02
*/

public class RedisUtil {

    private RedisTemplate<String, Object> redisTemplate;
 
    public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }
    //=============================common============================
    /**
     * 指定缓存失效时间
     * @param key 键
     * @param time 时间(秒)
     * @return
     */
    public boolean expire(String key,long time){
        try {
            if(time>0){
                redisTemplate.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
 
    /**
     * 根据key 获取过期时间
     * @param key 键 不能为null
     * @return 时间(秒) 返回0代表为永久有效
     */
    public long getExpire(String key){
        return redisTemplate.getExpire(key,TimeUnit.SECONDS);
    }
 
    /**
     * 判断key是否存在
     * @param key 键
     * @return true 存在 false不存在
     */
    public boolean hasKey(String key){
        try {
            return redisTemplate.hasKey(key);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
 
    /**
     * 删除缓存
     * @param key 可以传一个值 或多个
     */
    @SuppressWarnings("unchecked")
    public void del(String ... key){
        if(key!=null&&key.length>0){
            if(key.length==1){
                redisTemplate.delete(key[0]);
            }else{
                redisTemplate.delete(CollectionUtils.arrayToList(key));
            }
        }
    }
 
    //============================String=============================
    /**
     * 普通缓存获取
     * @param key 键
     * @return 值
     */
    public Object get(String key){
        return key==null?null:redisTemplate.opsForValue().get(key);
    }
 
    /**
     * 普通缓存放入
     * @param key 键
     * @param value 值
     * @return true成功 false失败
     */
    public boolean set(String key,Object value) {
        try {
            redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
 
    }
 
    /**
     * 普通缓存放入并设置时间
     * @param key 键
     * @param value 值
     * @param time 时间(秒) time要大于0 如果time小于等于0 将设置无限期
     * @return true成功 false 失败
     */
    public boolean set(String key,Object value,long time){
        try {
            if(time>0){
                redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
            }else{
                set(key, value);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
 
    /**
     * 递增
     * @param key 键
     * @param delta 要增加几(大于0)
     * @return
     */
    public long incr(String key, long delta){
        if(delta<0){
            throw new RuntimeException("递增因子必须大于0");
        }
        return redisTemplate.opsForValue().increment(key, delta);
    }
    /**
     * 递减
     * @param key 键
     * @param delta 要减少几(小于0)
     * @return
     */
    public long decr(String key, long delta){
        if(delta<0){
            throw new RuntimeException("递减因子必须大于0");
        }
        return redisTemplate.opsForValue().increment(key, -delta);
    }
    //================================Map=================================
    /**
     * HashGet
     * @param key 键 不能为null
     * @param item 项 不能为null
     * @return 值
     */
    public Object hget(String key,String item){
        return redisTemplate.opsForHash().get(key, item);
    }
    /**
     * 获取hashKey对应的所有键值
     * @param key 键
     * @return 对应的多个键值
     */
    public Map<Object,Object> hmget(String key){
        return redisTemplate.opsForHash().entries(key);
    }
 
    /**
     * HashSet
     * @param key 键
     * @param map 对应多个键值
     * @return true 成功 false 失败
     */
    public boolean hmset(String key, Map<String,Object> map){
        try {
            redisTemplate.opsForHash().putAll(key, map);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
 
    /**
     * HashSet 并设置时间
     * @param key 键
     * @param map 对应多个键值
     * @param time 时间(秒)
     * @return true成功 false失败
     */
    public boolean hmset(String key, Map<String,Object> map, long time){
        try {
            redisTemplate.opsForHash().putAll(key, map);
            if(time>0){
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
 
    /**
     * 向一张hash表中放入数据,如果不存在将创建
     * @param key 键
     * @param item 项
     * @param value 值
     * @return true 成功 false失败
     */
    public boolean hset(String key,String item,Object value) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
 
    /**
     * 向一张hash表中放入数据,如果不存在将创建
     * @param key 键
     * @param item 项
     * @param value 值
     * @param time 时间(秒)  注意:如果已存在的hash表有时间,这里将会替换原有的时间
     * @return true 成功 false失败
     */
    public boolean hset(String key,String item,Object value,long time) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            if(time>0){
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
 
    /**
     * 删除hash表中的值
     * @param key 键 不能为null
     * @param item 项 可以使多个 不能为null
     */
    public void hdel(String key, Object... item){
        redisTemplate.opsForHash().delete(key,item);
    }
 
    /**
     * 判断hash表中是否有该项的值
     * @param key 键 不能为null
     * @param item 项 不能为null
     * @return true 存在 false不存在
     */
    public boolean hHasKey(String key, String item){
        return redisTemplate.opsForHash().hasKey(key, item);
    }
 
    /**
     * hash递增 如果不存在,就会创建一个 并把新增后的值返回
     * @param key 键
     * @param item 项
     * @param by 要增加几(大于0)
     * @return
     */
    public double hincr(String key, String item,double by){
        return redisTemplate.opsForHash().increment(key, item, by);
    }
 
    /**
     * hash递减
     * @param key 键
     * @param item 项
     * @param by 要减少记(小于0)
     * @return
     */
    public double hdecr(String key, String item,double by){
        return redisTemplate.opsForHash().increment(key, item,-by);
    }
 
    //============================set=============================
    /**
     * 根据key获取Set中的所有值
     * @param key 键
     * @return
     */
    public Set<Object> sGet(String key){
        try {
            return redisTemplate.opsForSet().members(key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
 
    /**
     * 根据value从一个set中查询,是否存在
     * @param key 键
     * @param value 值
     * @return true 存在 false不存在
     */
    public boolean sHasKey(String key,Object value){
        try {
            return redisTemplate.opsForSet().isMember(key, value);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
 
    /**
     * 将数据放入set缓存
     * @param key 键
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public long sSet(String key, Object...values) {
        try {
            return redisTemplate.opsForSet().add(key, values);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
 
    /**
     * 将set数据放入缓存
     * @param key 键
     * @param time 时间(秒)
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public long sSetAndTime(String key,long time,Object...values) {
        try {
            Long count = redisTemplate.opsForSet().add(key, values);
            if(time>0)
            {expire(key, time);}
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
 
    /**
     * 获取set缓存的长度
     * @param key 键
     * @return
     */
    public long sGetSetSize(String key){
        try {
            return redisTemplate.opsForSet().size(key);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
 
    /**
     * 移除值为value的
     * @param key 键
     * @param values 值 可以是多个
     * @return 移除的个数
     */
    public long setRemove(String key, Object ...values) {
        try {
            Long count = redisTemplate.opsForSet().remove(key, values);
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
    //===============================list=================================
 
    /**
     * 获取list缓存的内容
     * @param key 键
     * @param start 开始
     * @param end 结束  0 到 -1代表所有值
     * @return
     */
    public List<Object> lGet(String key, long start, long end){
        try {
            return redisTemplate.opsForList().range(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
 
    /**
     * 获取list缓存的长度
     * @param key 键
     * @return
     */
    public long listSize(String key){
        try {
            return redisTemplate.opsForList().size(key);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
 
    /**
     * 通过索引 获取list中的值
     * @param key 键
     * @param index 索引  index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
     * @return
     */
    public Object lGetIndex(String key,long index){
        try {
            return redisTemplate.opsForList().index(key, index);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    /**
     * 将list放入缓存
     * @param key 键
     * @param value 值
     * @return
     */
    public boolean lSet(String key, Object value) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     * 将list放入缓存
     * @param key 键
     * @param value 值
     * @param time 时间(秒)
     * @return
     */
    public boolean lSet(String key, Object value, long time) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            if (time > 0)
            {expire(key, time);}
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
 
    /**
     * 将list放入缓存
     * @param key 键
     * @param value 值
     * @return
     */
    public boolean lSet(String key, List<Object> value) {
        try {
            redisTemplate.opsForList().rightPushAll(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
 
    /**
     * 将list放入缓存
     * @param key 键
     * @param value 值
     * @param time 时间(秒)
     * @return
     */
    public boolean lSet(String key, List<Object> value, long time) {
        try {
            redisTemplate.opsForList().rightPushAll(key, value);
            if (time > 0) expire(key, time);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
 
    /**
     * 根据索引修改list中的某条数据
     * @param key 键
     * @param index 索引
     * @param value 值
     * @return
     */
    public boolean lUpdateIndex(String key, long index,Object value) {
        try {
            redisTemplate.opsForList().set(key, index, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
 
    /**
     * 移除N个值为value
     * @param key 键
     * @param count 移除多少个
     * @param value 值
     * @return 移除的个数
     */
    public long lRemove(String key,long count,Object value) {
        try {
            Long remove = redisTemplate.opsForList().remove(key, count, value);
            return remove;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
```

##### RedisConfig.java

```java
package com.zyj.redis;
 
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
 
/**
*@Description: redis配置文件
*@Author: zyj 2018/5/26 8:06
*/
@Configuration
public class RedisConfig {
 
    /**
     * 实例化 RedisTemplate 对象
     * 提供给 RedisUtil 使用
     * @param redisConnectionFactory springboot配置好的连接工厂
     * @return RedisTemplate
     * @autor zyj
     * @date 2018年5月26日 08:47:27
     */
    @Bean
    public RedisTemplate<String, Object> RedisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        initRedisTemplate(redisTemplate, redisConnectionFactory);
        return redisTemplate;
    }
    /**
     * 设置数据存入 redis 的序列化方式,并开启事务
     *
     * @param redisTemplate
     * @param factory
     * @autor zyj
     * @date 2018年5月26日 08:47:27
     */
    private void initRedisTemplate(RedisTemplate<String, Object> redisTemplate, RedisConnectionFactory factory) {
        //如果不配置Serializer，那么存储的时候缺省使用String，如果用User类型存储，那么会提示错误User can't cast to String！
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashValueSerializer(new GenericJackson2JsonRedisSerializer());
        redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());
        // 开启事务
        redisTemplate.setEnableTransactionSupport(true);
        redisTemplate.setConnectionFactory(factory);
    }
 
    /**
     * 注入封装RedisTemplate 给RedisUtil提供操作类
     * @param redisTemplate
     * @return RedisUtil
     * @autor zyj
     * @date 2018年5月26日 08:47:27
     */
    @Bean(name = "redisUtil")
    public RedisUtil redisUtil(RedisTemplate<String, Object> redisTemplate) {
        RedisUtil redisUtil = new RedisUtil();
        redisUtil.setRedisTemplate(redisTemplate);
        return redisUtil;
    }
}
```

##### RedisTestController

```java
package com.redis.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RequestMapping("/redisTest")
public class RedisTestController {

    @Autowired
    RedisUtil redisUtil;

    /**
     * 测试redis写入
     * @param key
     * @param value
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public Object testReidsAdd(String key, String value){
        redisUtil.set(key, value);
        return redisUtil.get(key);
    }


    @RequestMapping("get")
    @ResponseBody
    public Object testReidsGet(String key){
        return redisUtil.get(key);
    }

    @RequestMapping("getTest")
    @ResponseBody
    public String get(){
        return "ok";
    }
}
```

5、web测试

![image-20200615113617060](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/Redis/image-20200615113617060.png)

![image-20200615113609777](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/Redis/image-20200615113609777.png)

![image-20200615113653265](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/Redis/image-20200615113653265.png)



### 使用jedis操作

参考https://blog.csdn.net/lovelichao12/article/details/75333035/，https://www.cnblogs.com/little-fly/p/10317122.html

需要jedis-2.1.0.jar，下载地址：http://files.cnblogs.com/liuling/jedis-2.1.0.jar.zip

如果需要使用Redis连接池的话，还需commons-pool-1.5.4.jar，下载地址:http://files.cnblogs.com/liuling/commons-pool-1.5.4.jar.zip

#### 使用maven依赖

```xml
     <dependency>
         <groupId>redis.clients</groupId>
         <artifactId>jedis</artifactId>
         <version>2.9.0</version>
      </dependency>
```

#### 无配置的简单操作：

```java
public class JedisService {
    static Jedis jedis;
    public static void main(String[] args) {
        jedis = new Jedis("10.209.221.26", 6379);
        System.out.println("连接成功");
        System.out.println("服务是否正在运行：" + jedis.ping());

        // 权限认证
        // jedis.auth("admin");

        // testString();
        // testList();
        // testHash();
        testSet();
        testScoreSet();


    }

    static void testString(){
        System.out.println("===========String操作============");
        jedis.set("a1", "value1");
        System.out.println("添加key=a1,value=value1");
        System.out.println("获取a1的值："  + jedis.get("a1"));
    }

    static void testList(){
        System.out.println("===========List操作============");
        // 判断是否存在key,存在则删除
        if(jedis.exists("list")){
            jedis.del("list");
        }


        // list是一个双向链表，用lpush和rpush添加数据
        // lpush从头加
        // 可用数组添加
        String[] strs = new String[]{"list", "wxl", "xfl", "wp", "wly"};
        // jedis.lpush("list", "wxl", "xfl", "wp", "wly");
        jedis.lpush("list", strs);
        System.out.println(jedis.lrange("list", 0, -1));
        jedis.del("list");
        jedis.lpush("list", "v1");
        jedis.lpush("list", "v2");
        jedis.lpush("list", "v3");
        jedis.lpush("list", "v4");
        System.out.println(jedis.lrange("list", 0, -1));
        // rpush从尾加
        jedis.del("list");
        jedis.rpush("list", "v1");
        jedis.rpush("list", "v2");
        jedis.rpush("list", "v3");
        jedis.rpush("list", "v4");
        System.out.println(jedis.lrange("list", 0, -1));

        // 获取指定区域list数据
        List<String> list = jedis.lrange("list", 0, 2);
        for (String s : list ) {
            System.out.println("列表数据：" + s);
        }

        // 获取全部列表数据
        list = jedis.lrange("list",0, -1);
        System.out.println(list);

        // 获取list长度
        System.out.println(jedis.llen("list"));

        // 排序
        // System.out.println(jedis.sort("list"));

        // 修改列表中某个值
        jedis.lset("list", 2, "66");

        // 获取指定下标的值
        System.out.println(jedis.lindex("list", 2));

        // 删除列表指定下标的值
        System.out.println(jedis.lrem("list", 2, "66"));
        System.out.println(jedis.lrange("list",0, -1));

        // 截取列表区间内的数据
        System.out.println(jedis.ltrim("list", 0, 2));
        System.out.println(jedis.lrange("list",0, -1));

        // 获取第一个下标值
        System.out.println(jedis.lpop("list"));

    }

    static void testHash(){
        System.out.println("===========Hash操作============");
        // 判断是否存在key,存在则删除
        if(jedis.exists("hash")){
            jedis.del("hash");
        }

        // 添加单个hash
        jedis.hset("hash", "m1", "v1");
        Map map = new HashMap();
        map.put("m2", "v2");
        map.put("m3", "v3");
        // 添加多个hash
        jedis.hmset("hash", map);

        // 获取hash中某个数据
        String v = jedis.hget("hash", "m3");
        System.out.println(v);
        // 获取多个数据
        List<String> l = jedis.hmget("hash", "m1", "m2", "m3");
        System.out.println(l);
    }

    static void testSet(){
        System.out.println("===========无序集合Set操作============");
        // 判断是否存在key,存在则删除
        if(jedis.exists("user")){
            jedis.del("user");
        }

        // 添加数据
        jedis.sadd("user", "wxl");
        jedis.sadd("user", "xfl");
        jedis.sadd("user", "wp");
        jedis.sadd("user", "lb");

        // 获取全部元素
        System.out.println(jedis.smembers("user"));

        // 删除元素
        System.out.println(jedis.srem("user","lb"));

        // 判断元素是否在集合中
        System.out.println(jedis.sismember("user","lb"));

        // 计算元素个数
        System.out.println(jedis.scard("user"));

        // 随机从集合返回一个元素
        System.out.println(jedis.srandmember("user"));
        // 随机从集合返回指定个数元素
        System.out.println(jedis.srandmember("user", 2));

        System.out.println("=======多个集合操作");
        jedis.sadd("uu", "s1","wxl", "xfl","s2");
        // 求多个集合的交集
        System.out.println(jedis.sinter("user","uu"));

        // 求多个集合的并集
        System.out.println(jedis.sunion("user", "uu"));

        // 求多个集合的差集
        System.out.println(jedis.sdiff("user", "uu"));

    }

    static void testScoreSet(){
        System.out.println("===========有序集合Set操作===========");
        // 判断是否存在key,存在则删除
        if(jedis.exists("userSort")){
            jedis.del("userSort");
        }

        // 添加数据
        jedis.zadd("userSort",1,"wxl");
        jedis.zadd("userSort",3,"lb");
        jedis.zadd("userSort",4,"xfl");
        jedis.zadd("userSort",2,"wp");
        jedis.zadd("userSort",6,"wly");
        jedis.zadd("userSort",5,"ck");
        jedis.zadd("userSort",7,"zdx");
        jedis.zadd("userSort",8,"mrh");

        // 从低到高获取全部元素
        System.out.println(jedis.zrange("userSort", 0, -1));

        // 从高到低获取全部元素
        System.out.println(jedis.zrevrange("userSort", 0, -1));

        // 计算元素个数
        System.out.println(jedis.zcard("userSort"));

        // 计算指定分数范围的成员个数
        System.out.println(jedis.zcount("userSort",2,5));

        // 获取某个成员的score
        System.out.println(jedis.zscore("userSort","ck"));

        System.out.println(jedis.zrange("userSort", 0, -1));
        // 获取元素下标
        System.out.println(jedis.zrank("userSort","ck"));

        // 删除元素
        System.out.println(jedis.zrem("userSort","lb"));

        // 删除指定排名内的升序元素
        System.out.println(jedis.zremrangeByRank("userSort", 1, 2));
        System.out.println(jedis.zrange("userSort", 0, -1));

        // 删除指定分数范围的成员
        System.out.println(jedis.zremrangeByScore("userSort", 4,7));
        System.out.println(jedis.zrange("userSort", 0, -1));
    }
}
```

#### 有配置的操作

##### 1、redis.properties

创建redis.properties配置文件，设置连接参数

```properties
# Redis settings  
redis.host=192.168.0.240
redis.port=6379
redis.pass=xxxxxx
redis.timeout=10000
 
redis.maxIdle=300
redis.maxTotal=600
# 毫秒
redis.maxWaitMillis=1000
redis.testOnBorrow=false
```

##### 2、PropertyUtil.java

创建属性文件加载工具类，用于获取redis.properties文件

```java
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 属性文件加载工具类
 */
public class PropertyUtil {
    // 加载properties文件到io流里面
    public static Properties loadProperties(String propertyFile){
        Properties properties = new Properties();
        try {
            InputStream is = Properties.class.getClassLoader().getResourceAsStream(propertyFile);
            if(is == null){
                is = Properties.class.getClassLoader().getResourceAsStream("properties/" + propertyFile);
            }
            properties.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

    /**
     * 根据key值取得队形的value值
     * @param propertyFile
     * @param key
     * @return
     */
    public static String getValue(String propertyFile, String key){
        Properties properties = loadProperties(propertyFile);
        return properties.getProperty(key);
    }
}
```

##### 3、JedisUtile.java

创建连接redis工具类

```java
package com.redis.demo2;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * redis工具类
 */
public class JedisUtil {
    private static JedisPool jedisPool = null;

    private JedisUtil(){}

    // 使用静态代码块的形式，只加载一次，节省资源
    static {
        Properties properties = PropertyUtil.loadProperties("redis.properties");
        String host = properties.getProperty("redis.host");
        String port = properties.getProperty("redis.port");
        String pass = properties.getProperty("redis.pass");
        String timeout = properties.getProperty("redis.timeout");
        String maxIdle = properties.getProperty("redis.maxIdle");
        String maxTotal = properties.getProperty("redis.maxTotal");
        String maxWaitMillis = properties.getProperty("redis.maxWaitMillis");
        String testOnBorrow = properties.getProperty("redis.testOnBorrow");

        JedisPoolConfig config = new JedisPoolConfig();
        // 控制开一个pool可分配多个jedis实例，通过pool.getResource()来获取
        // 如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted（耗尽）
        config.setMaxTotal(Integer.parseInt(maxTotal));
        //控制一个pool最多有多少个状态为idle(空闲的)的jedis实例。
        config.setMaxIdle(Integer.parseInt(maxIdle));
        //表示当borrow(引入)一个jedis实例时，最大的等待时间，如果超过等待时间，则直接抛出JedisConnectionException；
        config.setMaxWaitMillis(Long.parseLong(maxWaitMillis));
        //在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
        config.setTestOnBorrow(Boolean.valueOf(testOnBorrow));

        if(pass.equals("null")){
            jedisPool = new JedisPool(config, host, Integer.parseInt(port), Integer.parseInt(timeout));
        }else{
            jedisPool = new JedisPool(config, host, Integer.parseInt(port), Integer.parseInt(timeout), pass);
        }

    }

    /**
     * 从jedis连接池中获取jedis对象
     * @return
     */
    private static Jedis getJedis(){
        return jedisPool.getResource();
    }

    private static final JedisUtil jedisUtil  = new JedisUtil();

    /**
     * 获取JedisUtile实例
     * @return
     */
    public static JedisUtil getInstance(){
        return jedisUtil;
    }

    private void returnJedis(Jedis jedis){
        if(null != jedis && null != jedisPool){
            jedisPool.returnResource(jedis);
        }
    }

    /**
     * 添加sorted set
     *
     * @param key
     * @param value
     * @param score
     */
    public void zadd(String key, String value, double score) {
        Jedis jedis = getJedis();
        jedis.zadd(key, score, value);
        returnJedis(jedis);
    }

    /**
     * 返回指定位置的集合元素,0为第一个元素，-1为最后一个元素
     * @param key
     * @param start
     * @param end
     * @return
     */
    public Set<String> zrange(String key, int start, int end) {
        Jedis jedis = getJedis();
        Set<String> set = jedis.zrange(key, start, end);
        returnJedis(jedis);
        return set;
    }

    /**
     * 获取给定区间的元素，原始按照权重由高到低排序
     * @param key
     * @param start
     * @param end
     * @return
     */
    public Set<String> zrevrange(String key, int start, int end) {
        Jedis jedis = getJedis();
        Set<String> set = jedis.zrevrange(key, start, end);
        returnJedis(jedis);
        return set;
    }

    /**
     * 添加对应关系，如果对应关系已存在，则覆盖
     *
     * @param key
     * @param map 对应关系
     * @return 状态，成功返回OK
     */
    public String hmset(String key, Map<String, String> map) {
        Jedis jedis = getJedis();
        String s = jedis.hmset(key, map);
        returnJedis(jedis);
        return s;
    }

    /**
     * 向List头部追加记录
     *
     * @param key
     * @param value
     * @return 记录总数
     */
    public long rpush(String key, String value) {
        Jedis jedis = getJedis();
        long count = jedis.rpush(key, value);
        returnJedis(jedis);
        return count;
    }

    /**
     * 向List头部追加记录
     *
     * @param key
     * @param value
     * @return 记录总数
     */
    private long rpush(byte[] key, byte[] value) {
        Jedis jedis = getJedis();
        long count = jedis.rpush(key, value);
        returnJedis(jedis);
        return count;
    }

    /**
     * 删除
     *
     * @param key
     * @return
     */
    public long del(String key) {
        Jedis jedis = getJedis();
        long s = jedis.del(key);
        returnJedis(jedis);
        return s;
    }

    /**
     * 从集合中删除成员
     * @param key
     * @param value
     * @return 返回1成功
     * */
    public long zrem(String key, String... value) {
        Jedis jedis = getJedis();
        long s = jedis.zrem(key, value);
        returnJedis(jedis);
        return s;
    }

    public void saveValueByKey(int dbIndex, byte[] key, byte[] value, int expireTime)
            throws Exception {
        Jedis jedis = null;
        boolean isBroken = false;
        try {
            jedis = getJedis();
            jedis.select(dbIndex);
            jedis.set(key, value);
            if (expireTime > 0)
                jedis.expire(key, expireTime);
        } catch (Exception e) {
            isBroken = true;
            throw e;
        } finally {
            returnResource(jedis, isBroken);
        }
    }

    public byte[] getValueByKey(int dbIndex, byte[] key) throws Exception {
        Jedis jedis = null;
        byte[] result = null;
        boolean isBroken = false;
        try {
            jedis = getJedis();
            jedis.select(dbIndex);
            result = jedis.get(key);
        } catch (Exception e) {
            isBroken = true;
            throw e;
        } finally {
            returnResource(jedis, isBroken);
        }
        return result;
    }

    public void deleteByKey(int dbIndex, byte[] key) throws Exception {
        Jedis jedis = null;
        boolean isBroken = false;
        try {
            jedis = getJedis();
            jedis.select(dbIndex);
            jedis.del(key);
        } catch (Exception e) {
            isBroken = true;
            throw e;
        } finally {
            returnResource(jedis, isBroken);
        }
    }

    public void returnResource(Jedis jedis, boolean isBroken) {
        if (jedis == null)
            return;
        if (isBroken)
            jedisPool.returnBrokenResource(jedis);
        else
            jedisPool.returnResource(jedis);
    }

    /**
     * 获取总数量
     * @param key
     * @return
     */
    public long zcard(String key) {
        Jedis jedis = getJedis();
        long count = jedis.zcard(key);
        returnJedis(jedis);
        return count;
    }

    /**
     * 是否存在KEY
     * @param key
     * @return
     */
    public boolean exists(String key) {
        Jedis jedis = getJedis();
        boolean exists = jedis.exists(key);
        returnJedis(jedis);
        return exists;
    }

    /**
     * 重命名KEY
     * @param oldKey
     * @param newKey
     * @return
     */
    public String rename(String oldKey, String newKey) {
        Jedis jedis = getJedis();
        String result = jedis.rename(oldKey, newKey);
        returnJedis(jedis);
        return result;
    }

    /**
     * 设置失效时间
     * @param key
     * @param seconds
     */
    public void expire(String key, int seconds) {
        Jedis jedis = getJedis();
        jedis.expire(key, seconds);
        returnJedis(jedis);
    }

    /**
     * 删除失效时间
     * @param key
     */
    public void persist(String key) {
        Jedis jedis = getJedis();
        jedis.persist(key);
        returnJedis(jedis);
    }

    /**
     * 添加一个键值对，如果键存在不在添加，如果不存在，添加完成以后设置键的有效期
     * @param key
     * @param value
     * @param timeOut
     */
    public void setnxWithTimeOut(String key,String value,int timeOut){
        Jedis jedis = getJedis();
        if(0!=jedis.setnx(key, value)){
            jedis.expire(key, timeOut);
        }
        returnJedis(jedis);
    }

    /**
     * 返回指定key序列值
     * @param key
     * @return
     */
    public long incr(String key){
        Jedis jedis = getJedis();
        long l = jedis.incr(key);
        returnJedis(jedis);
        return l;
    }

    /**
     * 获取当前时间
     * @return 秒
     */
    public long currentTimeSecond(){
        Long l = 0l;
        Jedis jedis = getJedis();
        Object obj = jedis.eval("return redis.call('TIME')",0);
        if(obj != null){
            List<String> list = (List)obj;
            l = Long.valueOf(list.get(0));
        }
        returnJedis(jedis);
        return l;
    }
}
```

##### 4、RedisService.java

```java

import java.util.Date;
import java.util.Set;
import org.springframework.stereotype.Service;

/**
 * redis服务
 * @author lc
 */
@Service("redisService")
public class RedisService {


    /**
     * 添加SortSet型数据
     * @param key
     * @param value
     */
    public void addSortSet(String key, String value) {
        double score = new Date().getTime();
        JedisUtil jedisUtil = JedisUtil.getInstance();
        jedisUtil.zadd(key, value, score);
    }

    /**
     * 获取倒序的SortSet型的数据
     * @param key
     * @return
     */
    public Set<String> getDescSortSet(String key) {
        JedisUtil jedisUtil = JedisUtil.getInstance();
        return jedisUtil.zrevrange(key, 0, -1);
    }

    /**
     * 删除SortSet型数据
     * @param key
     * @param value
     */
    public void deleteSortSet(String key, String value) {
        JedisUtil jedisUtil = JedisUtil.getInstance();
        jedisUtil.zrem(key, value);
    }

    /**
     * 批量删除SortSet型数据
     * @param key
     * @param value
     */
    public void deleteSortSetBatch(String key, String[] value) {
        JedisUtil jedisUtil = JedisUtil.getInstance();
        jedisUtil.zrem(key, value);
    }

    /**
     * 范围获取倒序的SortSet型的数据
     * @param key
     * @return
     */
    public Set<String> getDescSortSetPage(String key,int start, int end) {
        JedisUtil jedisUtil = JedisUtil.getInstance();
        return jedisUtil.zrevrange(key, start, end);
    }

    /**
     * 获取SortSet型的总数量
     * @param key
     * @return
     */
    public long getSortSetAllCount(String key) {
        JedisUtil jedisUtil = JedisUtil.getInstance();
        return jedisUtil.zcard(key);
    }

    /**
     * 检查KEY是否存在
     * @param key
     * @return
     */
    public boolean checkExistsKey(String key) {
        JedisUtil jedisUtil = JedisUtil.getInstance();
        return jedisUtil.exists(key);
    }

    /**
     * 重命名KEY
     * @param oldKey
     * @param newKey
     * @return
     */
    public String renameKey(String oldKey, String newKey) {
        JedisUtil jedisUtil = JedisUtil.getInstance();
        return jedisUtil.rename(oldKey, newKey);
    }

    /**
     * 删除KEY
     * @param key
     */
    public void deleteKey(String key) {
        JedisUtil jedisUtil = JedisUtil.getInstance();
        jedisUtil.del(key);
    }

    /**
     * 设置失效时间
     * @param key
     * @param seconds 失效时间，秒
     */
    public void setExpireTime(String key, int seconds) {
        JedisUtil jedisUtil = JedisUtil.getInstance();
        jedisUtil.expire(key, seconds);
    }

    /**
     * 删除失效时间
     * @param key
     */
    public void deleteExpireTime(String key) {
        JedisUtil jedisUtil = JedisUtil.getInstance();
        jedisUtil.persist(key);
    }
}
```

##### 5、Test.java

```java
@Test
void contextLoads() {
    RedisService redisService = new RedisService();
    redisService.addSortSet("rmd_cart_test123", "111");

    System.out.println(redisService.getDescSortSet("rmd_cart_test123"));
}
```

## 五个应用场景实例

| string           | hash         | list         | set      | sorted sets                  |
| ---------------- | ------------ | ------------ | -------- | ---------------------------- |
| IP地址：递增原理 | 存储用户信息 | 模拟消息队列 | 自动排重 | 以某一个条件为权重，进行排序 |

### String场景+Hash场景

参考：https://blog.csdn.net/jpgzhu/article/details/106717918

**描述**

- 通过Redis缓存记录该用户最后一次登陆时间及登陆累计次数，判断是不是在1分钟内频繁登陆。

**代码**

```java
@RequestMapping("login")
@ResponseBody
public String login(String username){
    
    // 通过缓存判断同一用户某一时间段内的登录次数是否超出限定次数
    String requestLoginName = "request_login_".concat(username);
    // 设置以1为步长递增，并返回递增后的值
    long loginCount = redisUtil.incr(requestLoginName, 1L);
    if(loginCount == 1){
        // 设置登陆限制次数的key值时效60秒
        redisUtil.expire(requestLoginName, 60);
    }

    if(loginCount > 5){
        return "时间段内已超出登陆限定次数，请不要频繁登陆";
    }


    // 将当前用户信息与登陆时间写入Reids缓存的哈希表
    redisUtil.hset(username,"id", "123");
    redisUtil.hset(username,"name", "wxl");
    redisUtil.hset(username,"age", "29");
    redisUtil.hset(username,"lastLoginTime", Timestamp.valueOf(LocalDateTime.now()));
    return "缓存用户登陆信息";
}
```



![image-20200624165140770](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/Redis/image-20200624165140770.png)



![image-20200624165159923](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/Redis/image-20200624165159923.png)

### List场景

参考：https://blog.csdn.net/jpgzhu/article/details/106804798

**描述**

- 通过Redis缓存队列记录最新10笔用户上传文件的信息。

![image-20200624165756348](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/Redis/image-20200624165756348.png)

**代码**

```java
@RequestMapping("upload")
@ResponseBody
public String upload(String imageName){
    String key = "upload_queue_name";
    // 将最新10条上传的信息放入redis缓存
    if(redisUtil.listSize(key) >= 10){
        System.out.println("队列已满，移除最旧的上传信息:" + redisUtil.rightPop(key));
    }
    JSONObject json = new JSONObject();
    json.put("image", imageName + "_" + Math.random());
    json.put("time", new Date());

    return redisUtil.leftPush(key, json) ? "队列增加成功" : "队列增加失败";
}
```

![image-20200624173808594](F:\OneDrive\程序开发资料\md笔记\image\Redis\image-20200624173808594.png)



![image-20200624173742166](F:\OneDrive\程序开发资料\md笔记\image\Redis\image-20200624173742166.png)



![image-20200624173725821](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/Redis/image-20200624173725821.png)

### Set场景

**描述**

- 通过Redis集合缓存客户信息，要求自动去重，不得重复记录

![image-20200629153722284](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/Redis/image-20200629153722284.png)

**代码**


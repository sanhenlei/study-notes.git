# 概述

- HDFS（Hadoop Distributed File System）是一个分布式文件系统。
- 使用场景：适合一次写入，多次读出的场景，且不支持文件的修改。
- 优点：
  - 高容错性；
  - 适合处理大数据；
  - 可构建在廉价机器，通过多副本机制，提高可靠性。
- 缺点：
  - 不适合低延时数据访问；
  - 无法高效的对大量小文件进行存储；
  - 不支持并发写入。
- HDFS块：
  - HDFS中的文件在物理上是分块存储（Block），块的大小可以通过配置参数（dfs.blocksize）来规定，默认大小在Hadoop2.x版本中是128M，block块的大小可以通过 hdfs-site.xml 当中的配置文件进行指定。

- 最佳状态是寻址时间为传输时间的1%时，块设置太小会增加寻址时间，太大则会导致程序在处理这块数据时非常慢。
  - HDFS块的大小设置主要取决于磁盘传输速度。

# 架构

![image-20210304115309383](image/HDFS/image-20210304115309383.png)

1、客户端向NameNode发送请求，获取元数据信息，这些元数据包括命名空间、块映射洗洗及DataNode的位置信息等。

2、NameNode将元数据信息返回给客户端。

3、客户端获取到元数据信息后，到相应的DataNode上**读/写数据**

4、相关联的DataNode之间会**相互复制数据**，以达到DataNode副本数的要求

5、DataNode会定期向NameNode发送心跳信息，上报自身节点的状态信息

6、SecondaryNameNode并不是NameNode的备份，会定期获取NameNode上的`fsimage`和 `edits log` 日志，并将二者进行合并，产生 `fsimage.ckpt` 推送给 NameNode。

# 组成

## NameNode

**NameNode 是整个 Hadooop 集群中至关重要的组件，它维护着整个 HDFS 树，以及文件系统树中所有的文件和文件路径的元数据信息**。这些元数据信息包括**文件名**，**命令空间**，**文件属性**（文件生成的时间、文件的副本数、文件的权限）、**文件数据块**、**文件数据块与所在 DataNode 之间的映射关系**等。

​    **一旦 NameNode 宕机或 NameNode 上的元数据信息损坏或丢失，基本上就会丢失 Hadoop 集群中存储的所有数据，整个 Hadoop 集群也会随之瘫痪**

主要功能：

- 管理HDFS的命名空间；
- 管理文件的名字、大小、元数据、文件在节点上的情况
- 配置副本策略；
- 管理数据库（Block）映射信息；
- 处理客户端读写请求；
- 维护文件系统树和元数据

### 工作机制

![image-20210304151611183](image/HDFS/image-20210304151611183.png)

### namenode 与 datanode 启动

- **namenode工作机制**

1. 第一次启动namenode格式化后，创建fsimage和edits文件。如果不是第一次启动，直接加载编辑日志和镜像文件到内存。
2. 客户端对元数据进行增删改的请求。
3. namenode记录操作日志，更新滚动日志。
4. namenode在内存中对数据进行增删改查。

- **secondary namenode**

1. secondary namenode询问 namenode 是否需要 checkpoint。直接带回 namenode 是否检查结果。
2. secondary namenode 请求执行 checkpoint。
3. namenode 滚动正在写的edits日志。
4. 将滚动前的编辑日志和镜像文件拷贝到 secondary namenode。
5. secondary namenode 加载编辑日志和镜像文件到内存，并合并。
6. 生成新的镜像文件 fsimage.chkpoint。
7. 拷贝 fsimage.chkpoint 到 namenode。
8. namenode将 fsimage.chkpoint 重新命名成fsimage。

### FSImage与edits详解

所有的元数据信息都保存在了FsImage与Eidts文件当中，这两个文件就记录了所有的数据的元数据信息，元数据信息的保存目录配置在了 **hdfs-site.xml** 当中

```xml
        <!--fsimage文件存储的路径-->
        <property>
                <name>dfs.namenode.name.dir</name>
                <value>file:///opt/hadoop-2.6.0-cdh5.14.0/hadoopDatas/namenodeDatas</value>
        </property>
        <!-- edits文件存储的路径 -->
        <property>
                <name>dfs.namenode.edits.dir</name>
                <value>file:///opt/hadoop-2.6.0-cdh5.14.0/hadoopDatas/dfs/nn/edits</value>
          </property>
```

客户端对hdfs进行写文件时会首先被记录在edits文件中。

edits修改时元数据也会更新。

每次hdfs更新时edits先更新后客户端才会看到最新信息。

fsimage：是namenode中关于元数据的镜像，一般称为检查点。

一般开始时对namenode的操作都放在edits中，为什么不放在fsimage中呢？

因为fsimage是namenode的完整的镜像，内容很大，如果每次都加载到内存的话生成树状拓扑结构，这是非常耗内存和CPU。

fsimage内容包含了namenode管理下的所有datanode中文件及文件block及block所在的datanode的元数据信息。随着edits内容增大，就需要在一定时间点和fsimage合并。

### FSimage文件当中的文件信息查看

- 使用命令 hdfs  oiv

```sh
cd  /opt/hadoop-2.6.0-cdh5.14.0/hadoopDatas/namenodeDatas/current
hdfs oiv -i fsimage_0000000000000000112 -p XML -o hello.xml
```

### edits当中的文件信息查看

- 查看命令 hdfs  oev

```sh
cd  /opt/hadoop-2.6.0-cdh5.14.0/hadoopDatas/dfs/nn/edits
hdfs oev -i  edits_0000000000000000112-0000000000000000113 -o myedit.xml -p XML
```

### namenode元数据信息多目录配置

为了保证元数据的安全性，我们一般都是先确定好我们的磁盘挂载目录，将元数据的磁盘做RAID1

namenode的本地目录可以配置成多个，且每个目录存放内容相同，增加了可靠性。

- 具体配置方案:

  **hdfs-site.xml**

  ```xml
    <property>
           <name>dfs.namenode.name.dir</name>
           <value>file:///export/servers/hadoop-2.6.0-cdh5.14.0/hadoopDatas/namenodeDatas</value>
      </property>
  ```

### namenode故障恢复

在我们的secondaryNamenode对namenode当中的fsimage和edits进行合并的时候，每次都会先将namenode的fsimage与edits文件拷贝一份过来，所以fsimage与edits文件在secondarNamendoe当中也会保存有一份，如果namenode的fsimage与edits文件损坏，那么我们可以将secondaryNamenode当中的fsimage与edits拷贝过去给namenode继续使用，只不过有可能会丢失一部分数据。这里涉及到几个配置选项

- namenode保存fsimage的配置路径

```xml
<!--  namenode元数据存储路径，实际工作当中一般使用SSD固态硬盘，并使用多个固态硬盘隔开，冗余元数据 -->
    <property>
        <name>dfs.namenode.name.dir</name>
        <value>file:///export/servers/hadoop-2.6.0-cdh5.14.0/hadoopDatas/namenodeDatas</value>
    </property>
```

- namenode保存edits文件的配置路径

```xml
<property>
        <name>dfs.namenode.edits.dir</name>
        <value>file:///export/servers/hadoop-2.6.0-cdh5.14.0/hadoopDatas/dfs/nn/edits</value>
</property>
```

- secondaryNamenode保存fsimage文件的配置路径

```xml
<property>
        <name>dfs.namenode.checkpoint.dir</name>
        <value>file:///export/servers/hadoop-2.6.0-cdh5.14.0/hadoopDatas/dfs/snn/name</value>
</property>
```

- secondaryNamenode保存edits文件的配置路径

```xml
<property>
        <name>dfs.namenode.checkpoint.edits.dir</name>
        <value>file:///export/servers/hadoop-2.6.0-cdh5.14.0/hadoopDatas/dfs/nn/snn/edits</value>
</property>
```

**接下来我们来模拟namenode的故障恢复功能**

1. 杀死namenode进程: 使用jps查看namenode的进程号 , kill -9 直接杀死。
2. 删除namenode的fsimage文件和edits文件。

> 根据上述配置, 找到namenode放置fsimage和edits路径. 直接全部rm -rf 删除。
>
> 1. 拷贝secondaryNamenode的fsimage与edits文件到namenode的fsimage与edits文件夹下面去。
>    根据上述配置, 找到secondaryNamenode的fsimage和edits路径, 将内容 使用cp -r 全部复制到namenode对应的目录下即可。
> 2. 重新启动namenode, 观察数据是否存在。

## Secondary NameNode

**SecondaryNameNode 并不是 NameNode 的备份，在NameNode 发生故障时也不能立刻接管 NameNode 的工作**。SecondaryNameNode 在 Hadoop 运行的过程中具有两个作用：一个是**备份数据镜像**，另一个是**定期合并日志与镜像**，因此可以称其为 Hadoop 的**检查点**（checkpoint）。**SecondaryNameNode 定期合并 NameNode 中的 fsimage 和 edits log，能够防止 NameNode 重启时把整个 fsimage 镜像文件加载到内存，耗费过长的启动时间**。

​    SecondaryNameNode 的工作流程如图所示：

![image-20210304120515419](image/HDFS/image-20210304120515419.png)

**SecondaryNameNode的工作流程如下：**

​    （1）SecondaryNameNode 会**通知** NameNode **生成**新的 edits log 日志文件。

​    （2）NameNode **生成**新的 edits log 日志文件，然后将新的日志信息**写到**新生成的 edits log 日志文件中。

​    （3）SecondaryNameNode **复制** NameNode 上的 fsimage 镜像和 edits log 日志文件，此时使用的是 **http get** 方式。

​    （4）SecondaryNameNode 将fsimage将镜像文件**加载**到内存中，然后执行 edits log 日志文件中的操作，**生成**新的镜像文件 fsimage.ckpt。

​    （5）SecondaryNameNode 将 fsimage.ckpt 文件**发送**给 NameNode，此时使用的是 **http post** 方式。

​    （6）NameNode 将 edits log 日志文件**替换**成新生成的 edits.log 日志文件，同样将 fsimage文件**替换**成 SecondaryNameNode 发送过来的新的 fsimage 文件。

​    （7）NameNode **更新** fsimage 文件，将此次执行 checkpoint 的时间**写入** fstime 文件中。

​    经过 SecondaryNameNode 对 fsimage 镜像文件和 edits log 日志文件的**复制和合并**操作之后，NameNode 中的 fsimage 镜像文件就**保存**了最新的 checkpoint 的元数据信息， edits log 日志文件也会重新**写入**数据，两个文件中的数据不会变得很大。因此，当 **重启 NameNode 时，不会耗费太长的启动时间**。

​    **SecondaryNameNode 周期性地进行 checkpoint 操作需要满足一定的前提条件，这些条件如下**：

​    （1）`edits log` 日志文件的大小达到了一定的阈值，此时会对其进行合并操作。

​    （2）每隔一段时间进行 **checkpoint** 操作。

​    这些条件可以在`core-site.xml`文件中进行配置和调整，代码如下所示：

```xml
<property>
         <name>fs.checkpoint.period</name>
         <value>3600</value>
</property>
<property>
         <name>fs.checkpoint.size</name>
         <value>67108864</value>
</property>
```

上述代码配置了 checkpoint 发生的时间周期和 edits log 日志文件的大小阈值，说明如下。

​    （1）**fs.checkpoint.period**：表示触发 `checkpoint`发生的时间周期，这里配置的时间周期为 1 h。

​    （2）**fs.checkpoint.size**：表示 `edits log` 日志文件大小达到了多大的阈值时会发生 `checkpoint`操作，这里配置的 `edits log`大小阈值为 64 MB。

​    上述代码中配置的 `checkpoint`操作发生的情况如下：

​    （1）如果 edits log 日志文件经过 1 h 未能达到 64 MB，但是满足了 checkpoint发生的周期为 1 h 的条件，也会发生 checkpoint 操作。

​    （2）如果 edits log 日志文件大小在 1 h 之内达到了 64MB，满足了 checkpoint 发生的 edits log 日志文件大小阈值的条件，则会发生 checkpoint 操作。

> **注意**：如果 NameNode 发生故障或 NameNode 上的元数据信息丢失或损坏导致 NameNode 无法启动，此时就需要**人工干预**，将 NameNode 中的元数据状态恢复到 SecondaryNameNode 中的**元数据**状态。此时，如果 SecondaryNameNode 上的元数据信息与 NameNode 宕机时的元数据信息不同步，则或多或少地会导致 Hadoop 集群中丢失一部分数据。出于此原因，**应尽量避免将 NameNode 和 SecondaryNameNode 部署在同一台服务器上**

## DataNode

 **DataNode 是真正存储数据的节点**，这些数据以**数据块**的形式存储在 DataNode 上。一个数据块包含两个文件：一个是**存储数据本身的文件**，另一个是**存储元数据的文件**（这些元数据主要包括**数据块的长度、数据块的检验和、时间戳**）。

​    DataNode 运行时的**工作机制**如图所示：

![image-20210304120649380](image/HDFS/image-20210304120649380.png)

如图所示，DataNode 运行时的工作机制如下：

​    （1）DataNode启动之后，向 NameNode **注册**。

​    （2）NameNode **返回**注册成功的消息给 DataNode。

​    （3）DataNode 收到 NameNode 返回的注册成功的信息之后，会**周期性**地向 NameNode **上报**当前 DataNode 的所有块信息，默认发送所有数据块的时间周期是 **1h**。

​    （4）DataNode **周期性**地向NameNode 发送心跳信息；NameNode **收到** DataNode 发来的心跳信息后，会将DataNode 需要执行的命令放入到 **心跳信息**的 返回数据中，**返回**给 DataNode。DataNode 向 NameNode 发送心跳信息的默认时间周期是 **3s**。

​    （5）NameNode **超过一定的时间**没有收到 DataNode 发来的心跳信息，则 NameNode 会认为对应的 DataNode **不可用**。默认的超时时间是 **10 min**。

​    （6）**在存储上相互关联的 DataNode 会同步数据块，以达到数据副本数的要求**。

​    当 DataNode 发生故障导致 DataNode 无法与 NameNode 通信时，NameNode 不会立即认为 DataNode 已经 “死亡”。要经过一段**短暂的超时时长**后才会认为 DataNode 已经 “死亡”。HDFS 中默认的超时时长为 10 min + 30 s，可以用如下公式来表示这个超时时长：

```
timeout = 2 * dfs.namenode.heartbeat.recheck-interval +10 * dfs.heartbeat.interval
```

其中，各参数的含义如下：

​    （1）`timeout`：超时时长。

​    （2）`dfs.namenode.heartbeat.recheck-interval`：检查过期 DataNode 的时间间隔，与 `dfs.heartbeat.interval` 结合使用，默认的单位是 ms，默认时间是 **5 min**。

​    （3）`dfs.heartbeat.interval`：检测数据节点的时间间隔，默认的单位为 s，默认的时间是**3 s**。

​    所以，可以得出 DataNode 的默认超时时长为 **630s**，如下所示：

```
timeout = 2 * 5 * 60 + 10 * 3 = 630s
```

DataNode 的超时时长也可以在 `hdfs-site.xml`文件中进行配置，代码如下所示：

```xml
<property>
     <name>dfs.namenode.heartbeat.recheck-interval</name>
     <value>3000</value>
</property>
<property>
     <name>dfs.heartbeat.interval</name>
     <value>2</value>
</property>
```

根据上面的公式可以得出，在配置文件中配置的超时时长为：

```
timeout = 2 * 3000 / 1000 + 10 * 2 = 26s
```

当 DataNode 被 NameNode 判定为 “**死亡**”时，HDFS 就会马上自动进行**数据块的容错复制**。此时，当被 NameNode 判定为 “死亡” 的 DataNode 重新加入集群中时，如果其存储的数据块并没有损坏，就会造成 **HDFS 上某些数据块的备份数超过系统配置的备份数目**。

​    HDFS上**删除多余的数据块**需要的时间长短和数据块报告的时间间隔有关。该参数可以在 `hdfs-site.xml`文件中进行配置，代码如下所示：

```xml
<property>
     <name>dfs.blockreport.intervalMsec</name>
     <value>21600000</value>
     <description>Determines block reporting interval in milliseconds.</description>
</property>
```

 数据块报告的时间间隔默认为 `21600000`ms，即 6h，可以通过调整此参数的大小来调整数据块报告的时间间隔。

## Client

- 文件切分，文件上传HDFS的时候，Client将文件切分成一个一个的Block，然后进行上传；
- 与NameNode交互，获取文件的位置信息；
- 与DataNode交互，读写数据；
- 管理和操作HDFS：格式化和增删改查。



# 安全模式

**安全模式是hadoop的一种保护机制，用于保证集群中的数据块的安全性**。当集群启动的时候，会首先进入安全模式。当系统处于安全模式时会检查数据块的完整性。

假设我们设置的副本数（即参数dfs.replication）是3，那么在datanode上就应该有3个副本存在，假设只存在2个副本，那么比例就是2/3=0.666。hdfs默认的副本率0.999。我们的副本率0.666明显小于0.999，因此系统会自动的复制副本到其他dataNode，使得副本率不小于0.999。如果系统中有5个副本，超过我们设定的3个副本，那么系统也会删除多于的2个副本。

**在安全模式状态下，文件系统只接受读数据请求，而不接受删除、修改等变更请求**。在，当整个系统达到安全标准时，HDFS自动离开安全模式。30s

安全模式操作命令

```sh
    hdfs  dfsadmin  -safemode  get #查看安全模式状态
    hdfs  dfsadmin  -safemode  enter #进入安全模式
    hdfs  dfsadmin  -safemode  leave #离开安全模式
```

# Block块和副本机制

HDFS 将所有的文件全部抽象成为 block 块来进行存储，不管文件大小，全部一视同仁都是以 block 块的统一大小和形式进行存储，方便我们的分布式文件系统对文件的管理。

所有的文件都是以 block 块的方式存放在 hdfs 文件系统当中，在 Hadoop 1 版本当中，文件的 block 块默认大小是 64M，Hadoop 2 版本当中，文件的 block 块大小默认是128M，block块的大小可以通过 hdfs-site.xml 当中的配置文件进行指定。

```xml
<property>
    <name>dfs.block.size</name>
    <value>块大小 以字节为单位</value> //只写数值就可以
</property>
```

### 抽象为block块的好处

- 1)  一个文件有可能大于集群中任意一个磁盘 
  10T*3/128 = xxx块 2T，2T，2T 文件方式存—–>多个block块，这些block块属于一个文件
- 2) 使用块抽象而不是文件可以简化存储子系统
- 3) 块非常适合用于数据备份进而提供数据容错能力和可用性

### 块缓存

**通常 DataNode 从磁盘中读取块，但对于访问频繁的文件，其对应的块可能被显示的缓存在 DataNode 的内存中，以堆外块缓存的形式存在**。默认情况下，一个块仅缓存在一个DataNode的内存中，当然可以针对每个文件配置DataNode的数量。**作业调度器通过在缓存块的DataNode上运行任务，可以利用块缓存的优势提高读操作的性能**。

例如：
连接（join）操作中使用的一个小的查询表就是块缓存的一个很好的候选。用户或应用通过在缓存池中增加一个cache directive来告诉namenode需要缓存哪些文件及存多久。缓存池（cache pool）是一个拥有管理缓存权限和资源使用的管理性分组。

例如:

一个文件 130M，会被切分成2个block块，保存在两个block块里面，实际占用磁盘130M空间，而不是占用256M的磁盘空间

### hdfs的文件权限验证

hdfs的文件权限机制与linux系统的文件权限机制类似

r:read  w:write  x:execute   
权限x对于文件表示忽略，对于文件夹表示是否有权限访问其内容

如果linux系统用户zhangsan使用hadoop命令创建一个文件，那么这个文件在HDFS当中的owner就是zhangsan

HDFS文件权限的目的，防止好人做错事，而不是阻止坏人做坏事。HDFS相信你告诉我你是谁，你就是谁

### hdfs的副本因子

为了保证block块的安全性，也就是数据的安全性，在hadoop2当中，文件默认保存三个副本，我们可以更改副本数以提高数据的安全性，在hdfs-site.xml当中修改以下配置属性，即可更改文件的副本数

```xml
<property>
     <name>dfs.replication</name>
     <value>3</value>
</property>
```

# 文件写入过程

![image-20210304150756422](image/HDFS/image-20210304150756422.png)

1. Client 发起文件上传请求，通过 RPC 与 NameNode 建立通讯, NameNode 检查目标文件是否已存在，父目录是否存在，返回是否可以上传；
2. Client 请求第一个 block 该传输到哪些 DataNode 服务器上；
3. NameNode 根据配置文件中指定的备份数量及机架感知原理进行文件分配, 返回可用的 DataNode 的地址如：A, B, C；

> Hadoop 在设计时考虑到数据的安全与高效， 数据文件默认在 HDFS 上存放三份， 存储策略为本地一份，同机架内其它某一节点上一份，不同机架的某一节点上一份。

1. Client 请求 3 台 DataNode 中的一台 A 上传数据（本质上是一个 RPC 调用，建立 pipeline ），A 收到请求会继续调用 B，然后 B 调用 C，将整个 pipeline 建立完成， 后逐级返回 client；
2. **Client 开始往 A 上传第一个 block（先从磁盘读取数据放到一个本地内存缓存），以 packet 为单位（默认64K），A 收到一个 packet 就会传给 B，B 传给 C**。A 每传一个 packet 会放入一个应答队列等待应答；
3. 数据被分割成一个个 packet 数据包在 pipeline 上依次传输，在 pipeline 反方向上， 逐个发送 ack（命令正确应答），最终由 pipeline 中第一个 DataNode 节点 A 将 pipelineack 发送给 Client；
4. 当一个 block 传输完成之后，Client 再次请求 NameNode 上传第二个 block，重复步骤 2。

# 文件读取过程

![image-20210304151128229](image/HDFS/image-20210304151128229.png)

1. Client向NameNode发起RPC请求，来确定请求文件block所在的位置；
2. NameNode会视情况返回文件的部分或者全部block列表，对于每个block，NameNode 都会返回含有该 block 副本的 DataNode 地址； 这些返回的 DN 地址，会按照集群拓扑结构得出 DataNode 与客户端的距离，然后进行排序，排序两个规则：网络拓扑结构中距离 Client 近的排靠前；心跳机制中超时汇报的 DN 状态为 STALE，这样的排靠后；
3. Client 选取排序靠前的 DataNode 来读取 block，如果客户端本身就是DataNode，那么将从本地直接获取数据(短路读取特性)；
4. 底层上本质是建立 Socket Stream（FSDataInputStream），重复的调用父类 DataInputStream 的 read 方法，直到这个块上的数据读取完毕；
5. 当读完列表的 block 后，若文件读取还没有结束，客户端会继续向NameNode 获取下一批的 block 列表；
6. 读取完一个 block 都会进行 checksum 验证，如果读取 DataNode 时出现错误，客户端会通知 NameNode，然后再从下一个拥有该 block 副本的DataNode 继续读。
7. **read 方法是并行的读取 block 信息，不是一块一块的读取**；NameNode 只是返回Client请求包含块的DataNode地址，并不是返回请求块的数据；
8. 最终读取来所有的 block 会合并成一个完整的最终文件。

> 从 HDFS 文件读写过程中，可以看出，HDFS 文件写入时是串行写入的，数据包先发送给节点A，然后节点A发送给B，B再给C；而HDFS文件读取是并行的， 客户端 Client 直接并行读取block所在的节点。

## NameNode 工作机制以及元数据管理

# Shell操作

hadoop fs 或 hdfs dfs

> hadoop fs：查看所有HDFS命令

常用命令:

上传：

- -moveFromlLocal：从本地剪切粘贴到HDFS；
- -copyFromLocal：从本地复制粘贴到HDFS；
- -appendToFile：追加一个文件到已经存在的文件末尾；
- `-put：等同于copyFromLocal。`

下载：

- -copyToLocal：从HDFS拷贝到本地；
- `-get：等同于copyToLocal，就是从HDFS下载文件到本地；`
- -getmerge：合并下载多个文件，比如HDFS的目录 /user/atguigu/test下有多个文件:log.1。

HDFS直接操作

- `-ls: 显示目录信息；`
- `-mkdir：在HDFS上创建目录；`
- -chgrp 、-chmod、-chown：Linux文件系统中的用法一样，修改文件所属权限；
- -cp ：从HDFS的一个路径拷贝到HDFS的另一个路径；
- -mv：在HDFS目录中移动文件；
- -tail：显示一个文件的末尾；
- `-rm：删除文件或文件夹；`
- -rmdir：删除空目录；
- -du -h：统计文件夹的大小信息；
- -setrep：设置HDFS中文件的副本数量；

# JAVA操作

## 客户端环境准备：

- 务必使用英文无空格的Windows用户名；
- 安装Hadoop的windows依赖；
- 配置HADOOP_HOME环境变量；
- 配置Path环境变量。然后重启电脑；
- 如果上述操作后还有问题可以将bin目录下hadoop.dll和winutils.exe放到C:/windows/system32目录下；
- 创建Maven工程：
  - 导入POM依赖；
  - 配置log4j2.xml文件；
  - 编写测试类；
  - 运行测试类。

## JAR包

版本2.7.2

```xml
        <dependency>
          <groupId>org.apache.hadoop</groupId>
          <artifactId>hadoop-common</artifactId>
          <version>2.7.2</version>
        </dependency>
        <dependency>
          <groupId>org.apache.hadoop</groupId>
          <artifactId>hadoop-client</artifactId>
          <version>2.7.2</version>
        </dependency>
        <!--HDFS-->
        <dependency>
          <groupId>org.apache.hadoop</groupId>
          <artifactId>hadoop-hdfs</artifactId>
          <version>2.7.2</version>
        </dependency>
```



版本3.2.1

```xml
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-common</artifactId>
			<version>3.2.1</version>
		</dependency>
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-hdfs</artifactId>
			<version>3.2.1</version>
		</dependency>
		<dependency>
			<groupId>org.apache.hadoop</groupId>
			<artifactId>hadoop-client</artifactId>
			<version>3.2.1</version>
		</dependency>
```

## API操作

1）构建Configuration对象，读取并解析相关配置文件
　　　　Configuration conf=new Configuration();
2）设置相关属性
　　　　conf.set("fs.defaultFS","hdfs://1IP:9000");
3）获取特定文件系统实例fs（以HDFS文件系统实例）
　　　　FileSystem fs=FileSystem.get(new URI("hdfs://IP:9000"),conf,“hdfs");
4）通过文件系统实例fs进行文件操作(以删除文件实例)
　　　　fs.delete(new Path("/user/liuhl/someWords.txt"));

- 上传操作；
- 下载操作；
- 删除操作；
- 名称更改；
- 详情查看；
- 文件判断。

### 上传本地文件

通过“FileSystem.copyFromLocalFile(Path src,Path dst)”可以将本地文件上传到HDFS的指定位置上。具体代码如下：

```java
package HDFS.learnself;

import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class Upload_Files {
 public static void main(String[] args) throws IOException {
  //1.加载hdfs的配置文件
  Configuration conf=new Configuration();
  //2.获取hdfs的操作对象，得到一个FileSystem对象
  FileSystem fs=FileSystem.get(new URI("hdfs://hdp01:9000"), conf, "hdp02");
  //3.创建源目的文件路径和文件上传操作
  Path src=new Path("D:\\hdfs.txt");
  Path dst=new Path("/");
  fs.copyFromLocalFile(src, dst);
  //4.关闭流
  fs.close();
  System.out.println("文件上传成功！");

      }
 }
```

### 创建HDFS目录

通过“fs.mkdirs”方法进行目录的创建。具体代码如下：

```java
package HDFS.learnself;

import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class mkdir_list {
 public static void main(String[] args) throws IOException {
    //1.加载hdfs的配置信息
     Configuration conf=new Configuration();
    //2..获取hdfs的操作对象
     FileSystem fs=FileSystem.get(new URI("hdfs://hdp01:9000"), conf, "hdp02");
    //3.创建目录
     Path path=new Path("/list");
     fs.mkdirs(path);
    //4.关闭流
     fs.close();
     System.out.println("目录创建成功!");

       }
  }
```

### 写文件

通过“writeUTF()”方法可以实现对指定文件进行写操作。具体代码如下：

```java
package HDFS.learnself;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Hdfs;
import org.apache.hadoop.fs.Path;

public class Write {
 public static void main(String[] args) throws IOException, InterruptedException, URISyntaxException {
  //1.加载hdfs的配置文件
  Configuration conf=new Configuration();
  //2.获取hdfs的操作对象
  FileSystem fs=FileSystem.get(new URI("hdfs://hdp02:9000"), conf, "hdp02");
  //3.文件路径
  Path File=new Path("hdfs://hdp02:9000/test/cao.txt");
  //4.创建FSDataOutputStream对象
  FSDataOutputStream out=fs.create(File);
  //5.写入数据
  out.writeUTF("Hello world!");
  out.close();
  System.out.println("数据写入成功！");
    }
 }
```

### 读文件

通过“ReadUTF()”方法可以实现对指定文件进行读操作。具体代码如下：

```java
package HDFS.learnself;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class Read {
 public static void main(String[] args) throws IOException, InterruptedException, URISyntaxException {
  //1.加载hdfs的配置文件
  Configuration conf=new Configuration();
  //2.获取hdfs的操作对象
  FileSystem fs=FileSystem.get(new URI("hdfs://hdp01:9000"), conf, "hdp02");
  //3.读取文件的路径
  Path File=new Path("hdfs://hdp01:9000/test/cao.txt");
  //4.创建FSDataInputStream对象
  FSDataInputStream in=fs.open(File);
  String info = in.readUTF();
  System.out.println("数据读取成功！");
  System.out.println(info);
  in.close();
      }
 }
```

### 重命名

通过“fs.rename()”方法可以实现对指定文件进行重命名。具体代码如下：

```java
package HDFS.learnself;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class ChangeName {
 public static void main(String[] args) throws IOException, InterruptedException, URISyntaxException {
  //1.加载hdfs的配置文件
  Configuration conf=new Configuration();
  //2.获取hdfs的操作对象
  FileSystem fs=FileSystem.get(new URI("hdfs://hdp01:9000"), conf, "hdp02");
  //3.对文件名进行操作
  Path old=new Path("/test/fur.txt");
  Path now=new Path("/test/fur01.txt");
  //4.调用hdfs的rename重命名方法，返回值为boolean类型
  boolean isRename=fs.rename(old, now);
  System.out.println("重命名成功！");
       }
 }
```

### 删除文件

通过“fs.delete()”方法可以实现对指定文件进行删除。具体代码如下：

```java
package HDFS.learnself;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class DeleteFile {
 public static void main(String[] args) throws IOException, InterruptedException, URISyntaxException {
  //1.加载hdfs的配置文件
  Configuration conf=new Configuration();
  //2.获取hdfs的操作对象
  FileSystem fs=FileSystem.get(new URI("hdfs://hdp01:9000"), conf, "hdp02");
  //3.将要删除的文件路径
  String File="/test/cao.txt";
  //4.删除文件
  fs.delete(new Path(File), true);
  fs.close();
  System.out.println("文件删除成功！");
      }
 }
```

### 删除目录

通过“fs.delete()”方法可以实现对目录的（递归）删除。具体代码如下：

```java
package HDFS.learnself;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class DeleteList {
 public static void main(String[] args) throws IOException, InterruptedException, URISyntaxException {
  //1.加载hdfs的配置文件
  Configuration conf=new Configuration();
  //2.获取hdfs的操作对象
  FileSystem fs=FileSystem.get(new URI("hdfs://hdp01:9000"), conf, "hdp02");
  //3.将要删除的目录路径
  Path list=new Path("/list");
  //4.递归删除
  boolean isDelete=fs.delete(list, true);
  fs.close();
  System.out.println("目录删除成功！");
      }
 }
```

### 读取某个目录下的所有文件

通过“FileStatus.getPath()”方法可以查看指定目录下的所有文件。具体代码如下：

```java
package HDFS.learnself;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class CatFiles {
 public static void main(String[] args) throws IOException, InterruptedException, URISyntaxException {
  //1.加载hdfs的配置文件
  Configuration conf=new Configuration();
  //2.获取hdfs的操作对象
  FileSystem fs=FileSystem.get(new URI("hdfs://hdp01:9000"), conf, "hdp02");
  //3.创建要读取的文件路径
  Path listf=new Path("/tmp");
  //4.创建FileStatus对象，调用listStatus方法
  FileStatus stats[]=fs.listStatus(listf);
  for(int i=0;i<stats.length;i++){
   System.out.println(stats[i].getPath().toString());
  }
  fs.close();
  System.out.println("该目录下的文件查找完毕！");
      }
}
```

### 查看文件是否存在

通过“FileSystem.exists(Path f)”可以查看指定文件是否存在。具体代码如下：

```java
package HDFS.learnself;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class ifExists {
 public static void main(String[] args) throws IOException, InterruptedException, URISyntaxException {
  //1.加载hdfs的配置文件
  Configuration conf=new Configuration();
  //2.获取hdfs的操作对象
  FileSystem fs=FileSystem.get(new URI("hdfs://hdp01:9000"), conf, "hdp02");
  //3.要查找的文件路径
  Path File=new Path("/test/fur01.txt");
  //4.调用exists方法
  boolean isExists=fs.exists(File);
  System.out.println(isExists);
       }
  }
```

### 查看文件最后修改时间

通过“FileSystem.getModificationTime()”可以查看制定文件的最会修改时间（**注意:这里的时间是以时间戳的形式显示的**）。具体代码如下：

```java
package HDFS.learnself;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class Modify_Time {
 public static void main(String[] args) throws IOException, InterruptedException, URISyntaxException {
  //1.加载hdfs的配置文件
  Configuration conf=new Configuration();
  //2.获取hdfs的操作对象
  FileSystem fs=FileSystem.get(new URI("hdfs://hdp01:9000"), conf, "hdp02");
  //3.指定文件的路径
  Path File=new Path("/test/fur01.txt");
  //4.创建FileStatus对象，调用listStatus方法
  FileStatus filestatus=fs.getFileStatus(File);
  //5.调用getModificationTime方法
  long time = filestatus.getModificationTime();
  System.out.println(time);
  fs.close();
        }
  }
```

### 查看某个文件在HDFS集群中的位置

通过“FileSystem.getFileBlockLocation(FileStatus file,long start,long len)”可以查看指定文件所在位置。具体代码如下：

```java
package HDFS.learnself;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.BlockLocation;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class WhereFile {
 public static void main(String[] args) throws IOException, InterruptedException, URISyntaxException {
  //1.加载hdfs的配置文件
  Configuration conf=new Configuration();
  //2.获取hdfs的操作对象
  FileSystem fs=FileSystem.get(new URI("hdfs://hdp01:9000"), conf, "hdp02");
  //3.需要查找文件的路径
  Path File=new Path("/test/fur01.txt");
  //4.创建FileStatus对象，调用getFileStatus方法
  FileStatus filestatus=fs.getFileStatus(File);
  //5.创建一个BlockLocation数组对象，调用getFileBlockLocations方法
  BlockLocation [] blkLocations=fs.getFileBlockLocations(filestatus, 0, filestatus.getLen());
  int blockLen=blkLocations.length;
  for(int i=0;i<blockLen;i++){
   String [] hosts=blkLocations[i].getHosts();
   System.out.println("blockID:"+i+"\n"+"location:"+hosts[0]);
      }
 }
```

**说明：**

1.在最开始的两个API(文件上传和创建目录)的时候，笔者采用的是conf.set(）的方法，但在后面的API操作中，笔者统一的改为了FileSystem.get(uri，conf，user)的形式进行HDFS的配置的。主要是因为按照第一种方式，经常容易出现权限不足的异常，而采用后面的格式，一来防止出现权限异常问题，而来也改进了代码冗余问题。（**推荐读者使用后面的编写方式**）

2.在FileSystem.get(uri，conf，user)中指定URI的时候，笔者既写了“hdfs://hdp02:9000”,也写了“hdfs://hdp01:9000”。主要是因为笔者搭建的是高可用集群，有两个主节点（hdp01和hdp02），而不巧的是，在编程的过程中，开始的hdp02主节点宕掉了，所以后面处于Active状态的是hdp01主节点。（**请读者根据自己的实际情况编写**）

3.在FileSystem.get(uri，conf，user)中user是指集群登录的用户名。

# 增加新数据节点

需求说明:

随着公司业务的增长，数据量越来越大，原有的数据节点的容量已经不能满足存储数据的需求，需要在原有集群基础上动态添加新的数据节点。

##  环境准备

1. 复制一台新的虚拟机出来

> 将我们纯净的虚拟机复制一台出来，作为我们新的节点

1. 修改mac地址以及IP地址

```
修改mac地址命令
    vim /etc/udev/rules.d/70-persistent-net.rules
修改ip地址命令
    vim /etc/sysconfig/network-scripts/ifcfg-eth0
```

1. 关闭防火墙，关闭selinux

```
关闭防火墙
    service iptables stop
关闭selinux
    vim /etc/selinux/config
```

1. 更改主机名

```
更改主机名命令，将node04主机名更改为node04.hadoop.com
vim /etc/sysconfig/network
```

1. 四台机器更改主机名与IP地址映射

```
四台机器都要添加hosts文件
vim /etc/hosts

192.168.52.100 node01.hadoop.com  node01
192.168.52.110 node02.hadoop.com  node02
192.168.52.120 node03.hadoop.com  node03
192.168.52.130 node04.hadoop.com  node04
```

1. node04服务器关机重启

```
node04执行以下命令关机重启
    reboot -h now
```

1. node04安装jdk

```
node04统一两个路径
    mkdir -p /export/softwares/
    mkdir -p /export/servers/
```

**然后解压jdk安装包，配置环境变量**

1. 解压hadoop安装包

```
在node04服务器上面解压hadoop安装包到/export/servers , node01执行以下命令将hadoop安装包拷贝到node04服务器
    cd /export/softwares/
    scp hadoop-2.6.0-cdh5.14.0-自己编译后的版本.tar.gz node04:$PWD

node04解压安装包
    tar -zxf hadoop-2.6.0-cdh5.14.0-自己编译后的版本.tar.gz -C /export/servers/
```

1. 将node01关于hadoop的配置文件全部拷贝到node04

```
node01执行以下命令，将hadoop的配置文件全部拷贝到node04服务器上面
    cd /export/servers/hadoop-2.6.0-cdh5.14.0/etc/hadoop/
    scp ./* node04:$PWD
```

##  服役新节点具体步骤

1. 创建dfs.hosts文件

```
在node01也就是namenode所在的机器的/export/servers/hadoop-2.6.0-cdh5.14.0/etc/hadoop目录下创建dfs.hosts文件

[root@node01 hadoop]# cd /export/servers/hadoop-2.6.0-cdh5.14.0/etc/hadoop
[root@node01 hadoop]# touch dfs.hosts
[root@node01 hadoop]# vim dfs.hosts

添加如下主机名称（包含新服役的节点）
node01
node02
node03
node04
```

2. node01编辑hdfs-site.xml添加以下配置

> 在namenode的hdfs-site.xml配置文件中增加dfs.hosts属性

```
node01执行以下命令 :

cd /export/servers/hadoop-2.6.0-cdh5.14.0/etc/hadoop
vim hdfs-site.xml

# 添加一下内容
    <property>
         <name>dfs.hosts</name>
         <value>/export/servers/hadoop-2.6.0-cdh5.14.0/etc/hadoop/dfs.hosts</value>
    </property>
    <!--动态上下线配置: 如果配置文件中有, 就不需要配置-->
    <property>
        <name>dfs.hosts</name>
        <value>/export/servers/hadoop-2.6.0-cdh5.14.0/etc/hadoop/accept_host</value>
    </property>

    <property>
        <name>dfs.hosts.exclude</name>
        <value>/export/servers/hadoop-2.6.0-cdh5.14.0/etc/hadoop/deny_host</value>
    </property>
```

3. 刷新namenode

- node01执行以下命令刷新namenode

```
[root@node01 hadoop]# hdfs dfsadmin -refreshNodes
Refresh nodes successful
```

4. 更新resourceManager节点

- node01执行以下命令刷新resourceManager

```
[root@node01 hadoop]# yarn rmadmin -refreshNodes
19/03/16 11:19:47 INFO client.RMProxy: Connecting to ResourceManager at node01/192.168.52.100:8033
```

5. namenode的slaves文件增加新服务节点主机名称

> node01编辑slaves文件，并添加新增节点的主机，更改完后，slaves文件不需要分发到其他机器上面去

```
node01执行以下命令编辑slaves文件 :
    cd /export/servers/hadoop-2.6.0-cdh5.14.0/etc/hadoop
    vim slaves

添加一下内容:     
node01
node02
node03
node04
```

6. 单独启动新增节点

```
node04服务器执行以下命令，启动datanode和nodemanager : 
    cd /export/servers/hadoop-2.6.0-cdh5.14.0/
    sbin/hadoop-daemon.sh start datanode
    sbin/yarn-daemon.sh start nodemanager
```

7. 使用负载均衡命令，让数据均匀负载所有机器

```
node01执行以下命令 : 
    cd /export/servers/hadoop-2.6.0-cdh5.14.0/
    sbin/start-balancer.sh
```

# 退役旧数据

1. 创建dfs.hosts.exclude配置文件

在namenod所在服务器的/export/servers/hadoop-2.6.0-cdh5.14.0/etc/hadoop目录下创建dfs.hosts.exclude文件，并添加需要退役的主机名称

```
node01执行以下命令 : 
    cd /export/servers/hadoop-2.6.0-cdh5.14.0/etc/hadoop
    touch dfs.hosts.exclude
    vim dfs.hosts.exclude
添加以下内容:
    node04.hadoop.com

特别注意：该文件当中一定要写真正的主机名或者ip地址都行，不能写node04
```

2. 编辑namenode所在机器的hdfs-site.xml

> 编辑namenode所在的机器的hdfs-site.xml配置文件，添加以下配置

```
cd /export/servers/hadoop-2.6.0-cdh5.14.0/etc/hadoop
vim hdfs-site.xml

#添加一下内容:
    <property>
         <name>dfs.hosts.exclude</name>
         <value>/export/servers/hadoop-2.6.0-cdh5.14.0/etc/hadoop/dfs.hosts.exclude</value>
   </property>
```

3. 刷新namenode，刷新resourceManager

```
在namenode所在的机器执行以下命令，刷新namenode，刷新resourceManager : 

hdfs dfsadmin -refreshNodes
yarn rmadmin -refreshNodes
```

4. 节点退役完成，停止该节点进程

等待退役节点状态为decommissioned（所有块已经复制完成），停止该节点及节点资源管理器。注意：如果副本数是3，服役的节点小于等于3，是不能退役成功的，需要修改副本数后才能退役。

```
node04执行以下命令，停止该节点进程 : 
    cd /export/servers/hadoop-2.6.0-cdh5.14.0
    sbin/hadoop-daemon.sh stop datanode
    sbin/yarn-daemon.sh stop nodemanager
```

5. 从include文件中删除退役节点

```
namenode所在节点也就是node01执行以下命令删除退役节点 :
    cd /export/servers/hadoop-2.6.0-cdh5.14.0/etc/hadoop
    vim dfs.hosts

删除后的内容: 删除了node04
node01
node02
node03
```

6. node01执行一下命令刷新namenode，刷新resourceManager

```
hdfs dfsadmin -refreshNodes
yarn rmadmin -refreshNodes
```

7. 从namenode的slave文件中删除退役节点

```
namenode所在机器也就是node01执行以下命令从slaves文件中删除退役节点 : 
    cd /export/servers/hadoop-2.6.0-cdh5.14.0/etc/hadoop
    vim slaves
删除后的内容: 删除了 node04 
node01
node02
node03
```

8. 如果数据负载不均衡，执行以下命令进行均衡负载

```
node01执行以下命令进行均衡负载
    cd /export/servers/hadoop-2.6.0-cdh5.14.0/
    sbin/start-balancer.sh
```
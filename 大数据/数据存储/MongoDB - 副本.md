# 一、MongoDB集群

## 1、集群简介

### 架构概述

![image-20191203202212334](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/MongoDB/image-20191203202212334.png)

MongoDB部署架构分为单机、可复制集、分片集群，单机适合学习使用；分片集群比较复杂、运维难度高，在数据量达到一定瓶颈的时候才考虑使用，要慎重选择；可复制集是非常适合用于生产环境的一种架构

### 集群类型介绍

![](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/MongoDB/Image%202.png)

| 类型             | 特点                                                         |
| ---------------- | :----------------------------------------------------------- |
| Replica  Set副本 | Mongodb(M)表示主节点，Mongodb(S)表示备节点，Mongodb(A)表示仲裁节点。主备节点存储数据，仲裁节点不存储数据。客户端同时连接主节点与备节点，不连接仲裁节点。  默认设置下，主节点提供所有增删查改服务，备节点不提供任何服务。但是可以通过设置使备节点提供查询服务，这样就可以减少主节点的压力，当客户端进行数据查询时，请求自动转到备节点上  副本集具有自动故障恢复的功能 <br />***主从集群和副本集最大的区别就是副本集没有固定的“主节点”；整个集群会选出一个“主节点”，当其挂掉后，又在剩下的从节点中选中其他节点为“主节点”，副本集总有一个活跃点(primary)和一个或多个备份节点(secondary)。*** |
| Sharding分片     | 分片是一种在多台机器上分配数据的方法。MongoDB使用分片来支持具有非常大的数据集和高吞吐量操作。有两种解决系统增长的方法：垂直扩展和水平扩展。  垂直扩展涉及增加单个服务器的容量，例如使用更强大的CPU，增加更多RAM或增加存储空间量等。介于硬件成本和硬件性能单机器能支持的并发访问和存储容量是有限的。因此，垂直扩展是存在最大上限的。<br />水平扩展包括将系统数据集和负载分配到多个服务器上，添加额外的服务器以根据需要增加容量。尽管单台机器的整体速度或容量可能并不高，但每台机器可处理整个工作负载的一部分，效率可能会高于单台高速大容量服务器。而且很多时候可以选择成本很低的普通PC电脑；与单台机器的高端硬件相比，总成本可能会更低，但是增加了维护的复杂性。 <br /> ***与单机服务器和副本集相比，使用分片集群架构可以使应用程序具有更大的数据处理能力*** |
| Master-slave主备 | 对于主从复制的原理，非常简单，简单的说就是几台机器之间进行同步操作，我们在主节点上操作数据，需要同步到其他子节点上 |

通常来说，我们用第1、2种较多，第3种官方并不推荐。

| 1、Replica Set架构                   | 2、Sharding分片集群                   |
| ------------------------------------ | ------------------------------------- |
| ![](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/MongoDB/clip_image001.png) | ![ ](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/MongoDB/clip_image002.png) |

| 组件         | 作用                                                         |
| :----------- | ------------------------------------------------------------ |
| mongos       | 1、增删改查请求入口  <br />2、请求分发中心负责转发到对应的shard上  <br />3、通常设置多个 |
| config       | 1、存储数据库元信息（路由、分片）  <br />2、mongos从这里加载和更新配置  <br />3、通常设置多个 |
| shard        | 1、数据库拆分存储                                            |
| replica  set | 1、shard的备份，防止数据丢失，提高数据可用性                 |
| arbiter      | 1、不保存数据  <br />2、不要求硬件配置  <br />3、不能部署在同一个数据集节点中  <br />4、在数据库存储到分片时决定存储到哪个节点 |

mongos，数据库集群请求的入口，所有的请求都通过mongos进行协调，不需要在应用程序添加一个路由选择器，mongos自己就是一个请求分发中心，它负责把对应 的数据请求请求转发到对应的shard服务器上。在生产环境通常有多mongos作为请求的入口，防止其中一个挂掉所有的mongodb请求都没有办法操作。

config server，配置服务器，存储所有数据库元信息（路由、分片）的配置。mongos本身没有物理存储分片服务器和数据路由信息，只是缓存在内存里，配置服务器则实际存储这些数据。mongos第一次启动或者关掉重启就会从 config server 加载配置信息，以后如果配置服务器信息变化会通知到所有的 mongos 更新自己的状态，这样 mongos 就能继续准确路由。在生产环境通常有多个 config server 配置服务器，因为它存储了分片路由的元数据，防止数据丢失！

shard，分片（sharding）是指将数据库拆分，将其分散在不同的机器上的过程。将数据分散到不同的机器上，不需要功能强大的服务器就可以存储更多的数据和处理更大的负载。基本思想就是将集合切成小块，这些块分散到若干片里，每个片只负责总数据的一部分，最后通过一个均衡器来对各个分片进行均衡（数据迁移）。

replica set，中文翻译副本集，其实就是shard的备份，防止shard挂掉之后数据丢失。复制提供了数据的冗余备份，并在多个服务器上存储数据副本，提高了数据的可用性， 并可以保证数据的安全性。

仲裁者（Arbiter），是复制集中的一个MongoDB实例，它并不保存数据。仲裁节点使用最小的资源并且不要求硬件设备，不能将Arbiter部署在同一个数据集节点中，可以部署在其他应用服务器或者监视服务器中，也可部署在单独的虚拟机中。为了确保复制集中有奇数的投票成员（包括primary），需要添加仲裁节点做为投票，否则primary不能运行时不会自动切换primary。

简单了解之后，我们可以这样总结一下，应用请求mongos来操作mongodb的增删改查，配置服务器存储数据库元信息，并且和mongos做同步，数据最终存入在shard（分片）上，为了防止数据丢失同步在副本集中存储了一份，仲裁在数据存储到分片的时候决定存储到哪个节点。

## 2、分片+副本集集群搭建

### 环境说明

* **系统**：CentOS Linux release 7.4.1708
* **Mongo版本**：mongodb-linux-x86_64-rhel70-4.0.9.tar.gz
* 软件放到了移动硬盘：\soft\worksoft\linux\hadoop-hbase-mongodb\mongo
* **服务器规划**

| 10.199.155.171        | 10.199.155.183        | 10.199.155.210        |
| --------------------- | --------------------- | --------------------- |
| mongos  20000         | mongos  20000         | mongos  20000         |
| config  server 21000  | config  server 21000  | config  server 21000  |
| shard1  27001  主节点 | shard1  27001  副节点 | shard1  27001 仲裁    |
| shard2  27002  仲裁   | shard2  27002  主节点 | shard2  27002  副节点 |
| shard3  27003  副节点 | shard3  27003  仲裁   | shard3  27003  主节点 |

![](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/MongoDB/Image%203.png)

### 安装步骤

> **如下步骤在三台服务器上进行相同操作**

#### 1、解压安装包

```shell
tar -zxvf mongodb-linux-x86_64-rhel70-4.0.9.tar.gz
mv mongodb-linux-x86_64-rhel70-4.0.9 mongodb
```



#### 2、创建下面目录

```shell
mkdir -p /data/mongodb/conf
mkdir -p /data/mongodb/mongos/log
mkdir -p /data/mongodb/config/data
mkdir -p /data/mongodb/config/log
mkdir -p /data/mongodb/shard1/data
mkdir -p /data/mongodb/shard1/log
mkdir -p /data/mongodb/shard2/data
mkdir -p /data/mongodb/shard2/log
mkdir -p /data/mongodb/shard3/data
mkdir -p /data/mongodb/shard3/log
```

#### 3、配置文件创建

##### config server副本集配置

```shell
# 添加配置文件
vi /data/mongodb/conf/config.conf
```

```shell
# 系统日志
systemLog:
  destination: file
  logAppend: true
  path: /data/mongodb/config/log/config.log #日志存储位置

# 文件存储
storage:
  dbPath: /data/mongodb/config/data
  journal:
    enabled: true
#    directoryPerDB: true           #是否一个库一个文件夹
#    engine: wiredTiger             #数据引擎
  wiredTiger:                    #WT引擎配置
    engineConfig:
       cacheSizeGB: 20            #设置为4G,默认为物理内存的一半
       directoryForIndexes: true #是否将索引也按数据库名单独存储
       journalCompressor: zlib
    collectionConfig:            #表压缩配置
       blockCompressor: zlib
    indexConfig:                 #索引配置
       prefixCompression: true
# 配置启动管理方式
processManagement:
  fork: true
  pidFilePath: /data/mongodb/config/log/configsrv.pid

# 网络接口
net:
  port: 21000
  bindIp: 0.0.0.0

# 副本集
replication:
    replSetName: config

sharding:
    clusterRole: configsvr 
```

```shell
# 启动三台服务器的config server
/data/mongodb/bin/mongod -f /data/mongodb/conf/config.conf
```

```shell
#连接
/data/mongodb/bin/mongo --port 21000
#config变量
config = {
    _id : "config",
     members : [
         {_id : 0, host : "10.199.155.171:21000" },
         {_id : 1, host : "10.199.155.183:21000" },
         {_id : 2, host : "10.199.155.210:21000" }
     ]
 }

#初始化副本集
rs.initiate(config)

#查看分区状态
rs.status();
```



##### shard1分片副本集配置

```shell
# 添加配置文件
vi /data/mongodb/conf/shard1.conf
```

```shell
# 系统日志
systemLog:
  destination: file
  logAppend: true
  path: /data/mongodb/shard1/log/shard1.log
# 文件存储
storage:
  dbPath: /data/mongodb/shard1/data
  journal:
    enabled: true
  wiredTiger:                    #WT引擎配置
    engineConfig:
      cacheSizeGB: 10            #设置为4G,默认为物理内存的一半
      directoryForIndexes: true #是否将索引也按数据库名单独存储
      journalCompressor: zlib
    collectionConfig:            #表压缩配置
      blockCompressor: zlib
    indexConfig:                #索引配置
      prefixCompression: true
# 进程
processManagement:
  fork: true
  pidFilePath: /data/mongodb/shard1/log/shard1.pid
# 网络接口
net:
  port: 27001
  bindIp: 0.0.0.0
# 副本集
replication:
    replSetName: shard1
sharding:
    clusterRole: shardsvr
```

```shell
# 启动三台服务器的shard1 server
/data/mongodb/bin/mongod -f /data/mongodb/conf/shard1.conf
```

```shell
#连接
/data/mongodb/bin/mongo --port 27001

#使用admin数据库
use admin

#定义副本集配置，"arbiterOnly":true 代表其为仲裁节点。
config = {
    _id : "shard1",
     members : [
         {_id : 0, host : "10.199.155.171:27001" },
         {_id : 1, host : "10.199.155.183:27001" },
         {_id : 2, host : "10.199.155.210:27001" , arbiterOnly: true}
     ]
 }

#初始化副本集
rs.initiate(config)

#查看分区状态
rs.status();
```

##### shard2分片副本集配置

```shell
# 添加配置文件
vi /data/mongodb/conf/shard2.conf
```

```shell
# 系统日志
systemLog:
  destination: file
  logAppend: true
  path: /data/mongodb/shard2/log/shard2.log
# 文件存储
storage:
  dbPath: /data/mongodb/shard2/data
  journal:
    enabled: true
  wiredTiger:                    #WT引擎配置
    engineConfig:
      cacheSizeGB: 10            #设置为4G,默认为物理内存的一半
      directoryForIndexes: true #是否将索引也按数据库名单独存储
      journalCompressor: zlib
    collectionConfig:            #表压缩配置
      blockCompressor: zlib
    indexConfig:                #索引配置
      prefixCompression: true
# 进程
processManagement:
  fork: true
  pidFilePath: /data/mongodb/shard2/log/shard2.pid
# 网络接口
net:
  port: 27002
  bindIp: 0.0.0.0
# 副本集
replication:
    replSetName: shard2
sharding:
    clusterRole: shardsvr
```
```shell
# 启动三台服务器的shard2 server
/data/mongodb/bin/mongod -f /data/mongodb/conf/shard2.conf
```

```shell
#连接任一台
/data/mongodb/bin/mongo --port 27002

#使用admin数据库
use admin

#定义副本集配置，"arbiterOnly":true 代表其为仲裁节点。
config = {
    _id : "shard2",
     members : [
         {_id : 0, host : "10.199.155.171:27002" , arbiterOnly: true},
         {_id : 1, host : "10.199.155.183:27002" },
         {_id : 2, host : "10.199.155.210:27002" }
     ]
 }

#初始化副本集
rs.initiate(config)

#查看分区状态
rs.status();
```

##### shard3分片副本集配置
```shell
# 添加配置文件
vi /data/mongodb/conf/shard3.conf
```

```shell
# 系统日志
systemLog:
  destination: file
  logAppend: true
  path: /data/mongodb/shard3/log/shard3.log
# 文件存储
storage:
  dbPath: /data/mongodb/shard3/data
  journal:
    enabled: true
  wiredTiger:                    #WT引擎配置
    engineConfig:
      cacheSizeGB: 10            #设置为4G,默认为物理内存的一半
      directoryForIndexes: true #是否将索引也按数据库名单独存储
      journalCompressor: zlib
    collectionConfig:            #表压缩配置
      blockCompressor: zlib
    indexConfig:                #索引配置
      prefixCompression: true
# 进程
processManagement:
  fork: true
  pidFilePath: /data/mongodb/shard3/log/shard3.pid
# 网络接口
net:
  port: 27003
  bindIp: 0.0.0.0
# 副本集
replication:
    replSetName: shard3
sharding:
    clusterRole: shardsvr
```
```shell
# 启动三台服务器的shard3 server
/data/mongodb/bin/mongod -f /data/mongodb/conf/shard3.conf
```

```shell
#连接任一台
/data/mongodb/bin/mongo --port 27003

#使用admin数据库
use admin

#定义副本集配置，"arbiterOnly":true 代表其为仲裁节点。
config = {
    _id : "shard3",
     members : [
         {_id : 0, host : "10.199.155.171:27003" },
         {_id : 1, host : "10.199.155.183:27003" , arbiterOnly: true},
         {_id : 2, host : "10.199.155.210:27003" }
     ]
 }

#初始化副本集
rs.initiate(config)

#查看分区状态
rs.status();
```

##### mongos配置路由副本集配置

先启动配置服务器和分片服务器,后启动路由实例启动路由实

```shell
# 添加配置文件
vi /data/mongodb/conf/mongos.conf
```

```shell
# 系统日志
systemLog:
  destination: file
  logAppend: true
  path: /data/mongodb/mongos/log/mongos.log
processManagement:
  fork: true
  pidFilePath: /data/mongodb/mongos/log/mongos.pid
net:
  port: 20000
  bindIp: 0.0.0.0
  maxIncomingConnections: 20000
#监听的配置服务器,只能有1个或者3个， config为配置服务器的副本集名字
sharding:
   configDB: config/10.199.155.171:21000,10.199.155.183:21000,10.199.155.210:21000
```
```shell
# 启动三台服务器的mongos server
/data/mongodb/bin/mongos -config /data/mongodb/conf/mongos.conf
```

目前搭建了mongodb配置服务器、路由服务器，各个分片服务器，不过应用程序连接到mongos路由服务器并不能使用分片机制，还需要在程序里设置分片配置，让分片生效。

```shell
#连接任一台
/data/mongodb/bin/mongo --port 20000

#使用admin数据库
use admin

#串联路由服务器与分配副本集
sh.addShard("shard1/10.199.155.171:27001,10.199.155.183:27001,10.199.155.210:27001")
sh.addShard("shard2/10.199.155.171:27002,10.199.155.183:27002,10.199.155.210:27002")
sh.addShard("shard3/10.199.155.171:27003,10.199.155.183:27003,10.199.155.210:27003")
#查看集群状态
mongos> sh.status()
--- Sharding Status --- 
  sharding version: {
        "_id" : 1,
        "minCompatibleVersion" : 5,
        "currentVersion" : 6,
        "clusterId" : ObjectId("5de666a86f8463489aec56ef")
  }
  shards:
        {  "_id" : "shard1",  "host" : "shard1/10.199.155.171:27001,10.199.155.183:27001",  "state" : 1 }
        {  "_id" : "shard2",  "host" : "shard2/10.199.155.183:27002,10.199.155.210:27002",  "state" : 1 }
        {  "_id" : "shard3",  "host" : "shard3/10.199.155.171:27003,10.199.155.210:27003",  "state" : 1 }
  active mongoses:
        "4.0.9" : 1
  autosplit:
        Currently enabled: yes
  balancer:
        Currently enabled:  yes
        Currently running:  no
        Failed balancer rounds in last 5 attempts:  0
        Migration Results for the last 24 hours: 
                No recent migrations
  databases:
        {  "_id" : "config",  "primary" : "config",  "partitioned" : true }

```

#### 4、测试

##### 小测试

目前配置服务、路由服务、分片服务、副本集服务都已经串联起来了，但我们的目的是希望插入数据，数据能够自动分片。连接在mongos上，准备让指定的数据库、指定的集合分片生效。

```shell
#连接任一台
/data/mongodb/bin/mongo --port 20000

#设置分片chunk大小
use config
#设置1M是为了测试，否则要插入大量数据才能分片，默认为64MB。
db.settings.save({ "_id" : "chunksize", "value" : 1 })

use admin
#指定testdb分片生效
db.runCommand({enablesharding :"testdb"});
#指定数据库里需要分片的集合和片键
db.runCommand({shardcollection : "testdb.col",key : {id: 1}})


#插入数据
use testdb;
for (var i = 1; i <=100000; i++){
db.col.save({id: i, username: "正在往MongoDB分片集群插入数据，当前数据行数为"+i});
}

#查看分片情况如下，部分无关信息省掉了
sh.status()
```

![](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/MongoDB/Image%206.png)

![分片测试](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/MongoDB/clip_image0022.png)

##### 大量测试

> 用java读取CSV文件，单个文件10万行大概54MB，文件数：3个，插入：100075行,耗时：12.0s，入库速度：7800.0row/s

![](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/MongoDB/Image%2011.png)

## 3、客户端软件

### NoSQLBooster for MongoDB

对比了多个工具，个人觉得这款还可以，功能比较全，使用比较方便，增删改查、监控都可以。

> **在220服务器的目录C:\Users\wangxinlei\AppData\Local\Programs\nosqlbooster4mongo**

![](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/MongoDB/Image%205.png)

![](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/MongoDB/Image%207.png)

![](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/MongoDB/Image%208.png)

**当前实时操作：**

![](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/MongoDB/Image%209.png)

**Mongostat实时统计**

![](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/MongoDB/Image%2010.png)



# 二、命令行操作数据库

### 入门指令

```shell
登陆数据库
/data/mongodb/bin/mongo --port 20000

1、查看数据库
show dbs  查看当前有哪些的数据库
use databaseName 进入数据库
show tables/collections 查看当前库下的collection

2、创建数据库
MongoDB 的库是隐式创建,你可以 use 一个不存在的库
然后在该库下创建collection,即可创建库

3、创建collection(也就是表)
db.createCollection('collectionName')  
备注：
collection 允许隐式创建，即 db.collectionName.insert(document);

4、删除collection
db.collectionName.drop() ,

5、删除database
db.dropDatabase();

6、查看帮助信息
db.help();

7、创建分片的集合
sh.enableSharding("test")

```

### 增

```shell
介绍: mongodb存储的是文档,. 文档是json格式的对象
语法: db.collectionName.isnert(document);

①增加单篇文档
db.collectionName.insert({name:'jack'});

②增加单个文档,并指定_id
db.collectionName.insert({_id:1,name:'eric'});

③增加多个文档
db.collectionName.insert([
	{_id:2,name:'dora'},
	{name:'tony'},
	{_id:3,name:'lili',gender:'female',hobby:['Internet','music','swimming']}
]);

```

### 删

```shell
语法: db.collection.remove(查询表达式, 选项);
选项是指  {justOne:true/false},是否只删一行, 默认为false,指删除所有

备注：
a.查询表达式依然是个json对象
b.查询表达式匹配的行,将被删掉
c.如果不写查询表达式,collections中的所有文档将被删掉

①删除test表中 id属性值为 1 的文档 
db.test.remove({_id:1});

②删除test表中name属性为tony的文档,只删除1行
db.test.remove({name:'tony'},true);

```

### 改

```shell
语法: db.collection.update(查询表达式,新值,选项);

例:
db.test.update({name:'lili'},{name:'alex'});
是指选中test表中,name值为lili的文档,并把其文档值改为{name:'alex'},
结果: 文档中的其他列也不见了,改后只有_id和name列了。
即,新文档直接替换了旧文档,而不是修改

如果是想修改文档的某列,可以用$set关键字
db.collectionName.update(query,{$set:{name:'alex'}})

```

### 查

```shell
语法: db.collectionName.find(查询表达式,查询的列);
db.collections.find(表达式,{列1:1,列2:1,...});

①查询所有文档 所有内容
db.collectionName.find()

②查询所有文档的name属性 (_id属性默认总是查出来)
db.collectionName.find({},{name:1})

③查询所有文档的gender属性,且不查询_id属性
db.collectionName.find({},{name:1, _id:0})

④查询所有gender属性值为male的文档中的name属性
db.collectionName.find({gender:'male'},{name:1,_id:0});

⑤涉及数组查询，不如列为：array
db.collectionName.find({array:{$elemMatch:{$ne:null}});    #数组不为空
db.collectionName.find({$where:"this.array.length>0"});     #数组不为空
db.collectionName.find({array:{$size:0}})                           #空数组

```

### **for循环**

```shell
#查询指定数据中，然后对数据进行循环，然后将lastBuyDate的值赋给firstBuyDate字段
db.customer.find({"$and":[{'firstBuyDate':{$exists:false}},{'lastBuyDate':{$exists:true}}]}).forEach(function(item){db.getCollection('customer').update({"_id":item._id},{"$set": {"firstBuyDate":item.lastBuyDate}},{multi:true})})

备注：
如果数据库中没有这个字段，最后用db.getCollection()方法

```



# 三、Java操作MongoDB

|              | 2.x版本                   | 3.x以上                                                      |
| ------------ | ------------------------- | ------------------------------------------------------------ |
|              | mongo-java-driver-2.4.jar | mongodb-driver-3.8.2.jar                                     |
| 独立链接方式 |                           | 1、MongoClient mongoClient = new MongoClient( "host1" , 27017 );<br />2、MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://host1:27017")); |
| 集群连接     | 不支持                    | 1、MongoClient mongoClient = new MongoClient( new MongoClientURI("mongodb://host1:27017,host2:27017,host3:27017/?replicaSet=myReplicaSet"));<br />2、MongoClient mongoClient = new MongoClient(Arrays.asList(new ServerAddress("host1", 27017),<br/>        new ServerAddress("host2", 27017),<br/>        new ServerAddress("host3", 27017))); |
| 身份验证     |                           | MongoClientURI uri = new MongoClientURI("mongodb://user1:pwd1@host1/?authSource=db1&ssl=true"); |

## 2.x版本

比较老的jar包：mongo-java-driver-2.4.jar

目前流行的版本都是3.0以上，2.x简单介绍

```java
//连接mongo
Mongo mg = new MongoClient("192.168.186.129", 8888);
//连接数据库
DB db = mg.getDB("testDb");

//用于判断用户登录
// if(!db.authenticate("sdap", "sdap123".toCharArray())){
//方法已经不建议使用
// }

//获取集合列表
Set<String> collectionNames = db.getCollectionNames();
for(String strng : collectionNames) {
    System.out.println(string);
}

//连接指定集合
DBCollection collection2 = db.getCollection("t_users2");
CarPasitionObj u = new CarPasitionObj();
u.setLa(1231d);
u.setLo(42342d);
u.setPn("京1aaa");
u.setTp(12);
String obj2Json = JsonUtils.getSingletonInstance().obj2Json(u);
BasicDBObject dbObject1=new BasicDBObject();
dbObject1.put("key", "123131");
dbObject1.put("value", obj2Json);
//插入数据
collection2.save(dbObject1);
BasicDBObject dbObject=new BasicDBObject();
dbObject.put("key", "123131");
//查询数据
DBCursor find = collection2.find(dbObject);
while (find.hasNext()) {
    DBObject next= find.next();
    String key= (String)next.get("key");
    String json = (String)next.get("value");
    System.out.println(key);
    CarPasitionObj formJson = JsonUtils.getSingletonInstance().formJson(json, CarPasitionObj.class);
    System.out.println(formJson.getPn());
}
```





## 3.x版本

### 包引入
1、jar包：mongo-java-driver-2.8.2.jar
2、使用maven

```xml
<dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-mongodb</artifactId>
        </dependency>
        <dependency>
            <groupId>org.mongodb</groupId>
            <artifactId>mongodb-driver</artifactId>
            <version>3.8.2</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.mongodb/mongodb-driver-core -->
        <dependency>
            <groupId>org.mongodb</groupId>
            <artifactId>mongodb-driver-core</artifactId>
            <version>3.8.2</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.mongodb/bson -->
        <dependency>
            <groupId>org.mongodb</groupId>
            <artifactId>bson</artifactId>
            <version>3.8.2</version>
        </dependency>

    </dependencies>
```

### 连接方式

```java
#单机连接
MongoClient mongoClient = new MongoClient( "host1" , 27017 );
#或
MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://host1:27017"));

#集群连接
String url = "mongodb://host1:27017,host2:27017,host3:27017/?replicaSet=myReplicaSet";
MongoClient mongoClient = new MongoClient( new MongoClientURI(url));
#或
MongoClient mongoClient = new MongoClient(Arrays.asList(new ServerAddress("host1", 27017),
              new ServerAddress("host2", 27017),
              new ServerAddress("host3", 27017)));
#身份认证
#在连接后面添加
String url = "mongodb://user1:pwd1@host1/?authSource=db1&ssl=true";
#或 三个参数分别为 用户名 数据库名称 密码
MongoCredential credential = MongoCredential.createCredential(username, database, password);
MongoClientOptions options = MongoClientOptions.builder().sslEnabled(true).build();
MongoClient mongoClient = new MongoClient(new ServerAddress("host1", 27017),
                                           Arrays.asList(credential),
                                           options);
```

设置连接参数方法：

```java
public  MongoClient initConn(String url){
        log.info("进入mongos连接...");
        MongoClient mongoClient = null;
        String mongoserver = url.isEmpty() ? mongoEntity.getHosts() : url;
        String auth = mongoEntity.getAuth();
        log.info("Mongodb服务连接："+mongoserver);
        List<ServerAddress> addrs = new ArrayList<ServerAddress>();
        List<MongoCredential> credentials = new ArrayList<MongoCredential>();
        if(!mongoserver.isEmpty() && mongoserver.split(":").length > 0){
            for (String host : mongoserver.split(",")
            ) {
                addrs.add(new ServerAddress(host.split(":")[0], Integer.parseInt(host.split(":")[1].toString())));
                if(auth.equals("true")){
                    credentials.add(MongoCredential.createScramSha1Credential(mongoEntity.getUsername(), "admin", mongoEntity.getPassword().toCharArray()));
                }else{
                    credentials.add(MongoCredential.createScramSha1Credential("admin", "admin", "123456".toCharArray()));
                }
            }
            //配置参数
            MongoClientOptions.Builder buide = new MongoClientOptions.Builder();
            buide.connectionsPerHost(Integer.parseInt(mongoEntity.getConnectionsPerHost()));// 与目标数据库可以建立的最大链接数
            buide.connectTimeout(Integer.parseInt(mongoEntity.getConnectTimeout()));// 与数据库建立链接的超时时间
            buide.maxWaitTime(Integer.parseInt(mongoEntity.getMaxWaitTime()));// 一个线程成功获取到一个可用数据库之前的最大等待时间
            buide.threadsAllowedToBlockForConnectionMultiplier(100);
            buide.maxConnectionIdleTime(0);
            buide.maxConnectionLifeTime(Integer.parseInt(mongoEntity.getMaxConnectionLifeTime()));
            buide.socketTimeout(Integer.parseInt(mongoEntity.getSocketTimeout()));
            MongoClientOptions myOptions = buide.build();

            try{
                //通过连接认证获取MongoDB连接
                mongoClient = new MongoClient(addrs,myOptions);
                log.info("链接Mongodb成功");

            }catch (Exception e){
                e.printStackTrace();
            }
        }else{
            log.error("配置的Mongodb服务连接异常："+mongoserver);
        }

        return mongoClient;
    }
```



### 数据库操作

```java
//连接到数据库，如果不存在会自动创建
MongoDatabase mongodb = mongoClient.getDatabase("testDb");

//获取所有数据库
MongoIterable<String> dbs = mongoClient.listDatabaseNames();
for (String str : dbs) {
    System.out.println(str);
}

//删除数据库
mongoClient.dropDatabase("testDb");

//创建分片的集合和片键
db.runCommand( { enableSharding: "test" } );
db.runCommand( { shardCollection: "test.some_collection", key: { some_id:1 } } );

```

### 集合操作

```java
//连接到集合
MongoCollection<Document> collection = mongodb.getCollection("testCol");

//创建集合
mongoDatabase.createCollection("testCol");

//获取数据库集合列表
MongoIterable<String> dbCollection = mongodb.listCollectionNames();
for (String collname : dbCollection) {
    System.out.println(collname);;
}

//删除集合
mongoDatabase.getCollection("testCol").drop();

//创建索引
dbCollection.createIndex(new BasicDBObject("i",1));
```



### 插入数据

```java
            /**插入文档
             * 1. 创建文档 org.bson.Document 参数为key-value的格式
             * 2. 创建文档集合List<Document>
             * 3. 将文档集合插入数据库集合中 mongoCollection.insertMany(List<Document>) 插入单个文档可以用 mongoCollection.insertOne(Document)
             * */
            Document document = new Document("title", "MongoDB").
                    append("description", "database").
                    append("likes", 100).
                    append("by", "Fly");
            List<Document> documents = new ArrayList<Document>();
            documents.add(document);
            collection.insertMany(documents);
            System.out.println("文档插入成功");
```

### 查询数据

```java
			/**检索所有文档
             * 1. 获取迭代器FindIterable<Document>
             * 2. 获取游标MongoCursor<Document>
             * 3. 通过游标遍历检索出的文档集合
             * */
            FindIterable<Document> findIterable = collection.find();
            MongoCursor<Document> mongoCursor = findIterable.iterator();
            while (mongoCursor.hasNext()) {
                System.out.println(mongoCursor.next());
            }
            System.out.println("检索所有文档成功");
			

// 条件查询
Document document = new Document((Map) JSON.parse("{name:'zhangsan'}"));
Int rowsNum = 100;
FindIterable<Document> findIterable = collection.find(document).limit(rowsNum);



//扩展查询
 @Test
  public void test5(){
      MongoClient mongoClient = new MongoClient("192.168.128.156", 27017);

      // 获取指定数据库
      MongoDatabase database = mongoClient.getDatabase("test");

      // 获取指定集合
      MongoCollection<Document> collection = database.getCollection("users");

      // 查所有
      FindIterable<Document> documents = null;

      documents = collection.find();

      // 条件查询 select * fromt t_user where i = 20
      documents = collection.find(eq("i",20));

      // select * from t_user where i <= 20
      documents = collection.find(lte("i",30));

      // select * from t_user where i = 30 or i = 40 or i = 50
      documents = collection.find(in("i",30,40,50));

      // SELECT * FROM t_user WHERE i = 50 OR i < 25
      documents = collection.find(or(new Document("i",50),lt("i",25)));

      // 模糊查询 参数二：模糊条件
      documents = collection.find(regex("test","test"));

      // 排序 SELECT * FROM t_user WHERE i >= 90 order by i desc
      documents = collection.find(gt("i",90)).sort(Sorts.descending("i"));

      // 分页查询
      documents = collection.find().skip(50).limit(10);

      for (Document document : documents) {
          System.out.println(document);
      }

      mongoClient.close();
  }

```

### 获取数据库状态

```java
 /**
     * 获取数据库状态
     * @param param 要查询的数据库名，若为空则查询全部数据库
     * @return
     */
    @Override
    public  Result getDbStats(QueryMongoVO param){
        String dbname =  param.getDbname();
        log.info("开始获取Mongodb所有数据库的状态...");
        long stime = System.currentTimeMillis();
        JSONArray result = new JSONArray();
        MongoClient mongoClient  = initConn(param.getUrl());
        Document document = new Document();
        document.append("dbStats" ,1).append("scale", 1024*1024);
        if(dbname == null || dbname.length() == 0){
            MongoIterable<String> dbs = mongoClient.listDatabaseNames();
            for (String str : dbs) {
                MongoDatabase db = mongoClient.getDatabase(str);
                result.add(db.runCommand(document));
            }
        }else{
            MongoDatabase db = mongoClient.getDatabase(dbname);
            result.add(db.runCommand(document));
        }
        long etime = System.currentTimeMillis();
        mongoClient.close();
        return Result.ok("返回数据条数：" + result.size() + "，耗时:" + (etime - stime) + "ms",result);
    }
```

### MongoClientOptions 常用配置信息

```java
MongoClientOptions.Builder addClusterListener(ClusterListener clusterListener)
Adds the given cluster listener.//添加给定的集群监听器


MongoClientOptions.Builder addCommandListener(CommandListener commandListener)
Adds the given command listener.//添加给定的命令监听器


MongoClientOptions.Builder addConnectionPoolListener(ConnectionPoolListener connectionPoolListener)
Adds the given connection pool listener.//添加给定的连接池侦听器。


MongoClientOptions.Builder addServerListener(ServerListener serverListener)
Adds the given server listener.//添加给定的服务器侦听器。


MongoClientOptions.Builder addServerMonitorListener(ServerMonitorListener serverMonitorListener)
Adds the given server monitor listener.//添加给定的服务器监视器侦听器。


MongoClientOptions.Builder alwaysUseMBeans(boolean alwaysUseMBeans)
Sets whether JMX beans registered by the driver should always be MBeans, regardless of whether the VM is Java 6 or greater.
//设置由驱动程序注册的JMX bean是否应该始终是mbean，而不管VM是Java 6还是更大。


MongoClientOptions.Builder applicationName(String applicationName)
Sets the logical name of the application using this MongoClient.//设置MongoClient应用程序的逻辑名称。


MongoClientOptions build()
Build an instance of MongoClientOptions.//构建一个MongoClientOptions 实例


MongoClientOptions.Builder codecRegistry(CodecRegistry codecRegistry)
Sets the codec registry//设置注册表编解码器


MongoClientOptions.Builder connectionsPerHost(int connectionsPerHost)
Sets the maximum number of connections per host.//设置每个主机的最大连接数。


MongoClientOptions.Builder connectTimeout(int connectTimeout)
Sets the connection timeout.//设置连接超时。


MongoClientOptions.Builder cursorFinalizerEnabled(boolean cursorFinalizerEnabled)
Sets whether cursor finalizers are enabled.//设置是否启用游标终结器。


MongoClientOptions.Builder dbDecoderFactory(DBDecoderFactory dbDecoderFactory)
Sets the decoder factory.//设置集译码器工厂。


MongoClientOptions.Builder dbEncoderFactory(DBEncoderFactory dbEncoderFactory)
Sets the encoder factory.//设置编码器工厂。


MongoClientOptions.Builder description(String description)
Sets the description.//设置描述。


MongoClientOptions.Builder heartbeatConnectTimeout(int connectTimeout)
Sets the connect timeout for connections used for the cluster heartbeat.//为用于集群心跳的连接设置连接超时。


MongoClientOptions.Builder heartbeatFrequency(int heartbeatFrequency)
Sets the heartbeat frequency.//设置心跳频率。


MongoClientOptions.Builder heartbeatSocketTimeout(int socketTimeout)
Sets the socket timeout for connections used for the cluster heartbeat.//为用于集群心跳的连接设置套接字超时。


MongoClientOptions.Builder legacyDefaults()
Sets defaults to be what they are in MongoOptions.//设置默认设置为MongoOptions。


MongoClientOptions.Builder localThreshold(int localThreshold)
Sets the local threshold.//设置本地阈值。


MongoClientOptions.Builder maxConnectionIdleTime(int maxConnectionIdleTime)
Sets the maximum idle time for a pooled connection.//设置池连接的最大空闲时间。


MongoClientOptions.Builder maxConnectionLifeTime(int maxConnectionLifeTime)
Sets the maximum life time for a pooled connection.//设置池连接的最大生命时间。


MongoClientOptions.Builder maxWaitTime(int maxWaitTime)
Sets the maximum time that a thread will block waiting for a connection.//设置的最长时间，线程阻塞等待连接。


MongoClientOptions.Builder minConnectionsPerHost(int minConnectionsPerHost)
Sets the minimum number of connections per host.//设置每个主机的最小连接数。


MongoClientOptions.Builder minHeartbeatFrequency(int minHeartbeatFrequency)
Sets the minimum heartbeat frequency.//设置最小的心跳频率。


MongoClientOptions.Builder readConcern(ReadConcern readConcern)
Sets the read concern.


MongoClientOptions.Builder readPreference(ReadPreference readPreference)
Sets the read preference.


MongoClientOptions.Builder requiredReplicaSetName(String requiredReplicaSetName)
Sets the required replica set name for the cluster.//为集群设置所需的副本集名称。


MongoClientOptions.Builder serverSelectionTimeout(int serverSelectionTimeout)
Sets the server selection timeout in milliseconds, which defines how long the driver will wait for server selection to succeed before throwing an exception.//设置服务器选择超时以毫秒为间隔，这定义了在抛出异常之前，驱动程序等待服务器选择成功的时间。


MongoClientOptions.Builder socketFactory(SocketFactory socketFactory)
Sets the socket factory.//设置套接字工厂。


MongoClientOptions.Builder socketKeepAlive(boolean socketKeepAlive)
Deprecated. //默认为true
configuring keep-alive has been deprecated. It now defaults to true and disabling it is not recommended.


MongoClientOptions.Builder socketTimeout(int socketTimeout)
Sets the socket timeout.//设置套接字超时。


MongoClientOptions.Builder sslContext(SSLContext sslContext)
Sets the SSLContext to be used with SSL is enabled.//设置启用SSL的SSL上下文。


MongoClientOptions.Builder sslEnabled(boolean sslEnabled)
Sets whether to use SSL.//设置是否使用 SSL。


MongoClientOptions.Builder sslInvalidHostNameAllowed(boolean sslInvalidHostNameAllowed)
Define whether invalid host names should be allowed.//定义是否允许使用无效的主机名。


MongoClientOptions.Builder threadsAllowedToBlockForConnectionMultiplier(int threadsAllowedToBlockForConnectionMultiplier)
Sets the multiplier for number of threads allowed to block waiting for a connection.//设置允许阻塞等待连接的线程数量的倍数。


MongoClientOptions.Builder writeConcern(WriteConcern writeConcern)
Sets the write concern.

常用配置信息：

MongoClientOptions.Builder connectionsPerHost(int connectionsPerHost)
Sets the maximum number of connections per host.//设置每个主机的最大连接数。


MongoClientOptions.Builder connectTimeout(int connectTimeout)
Sets the connection timeout.//设置连接超时。


MongoClientOptions.Builder maxConnectionIdleTime(int maxConnectionIdleTime)
Sets the maximum idle time for a pooled connection.//设置池连接的最大空闲时间。


MongoClientOptions.Builder maxConnectionLifeTime(int maxConnectionLifeTime)
Sets the maximum life time for a pooled connection.//设置池连接的最大生命时间。


MongoClientOptions.Builder maxWaitTime(int maxWaitTime)
Sets the maximum time that a thread will block waiting for a connection.//设置的最长时间，线程阻塞等待连接。


MongoClientOptions.Builder minConnectionsPerHost(int minConnectionsPerHost)
Sets the minimum number of connections per host.//设置每个主机的最小连接数。


MongoClientOptions.Builder socketTimeout(int socketTimeout)
Sets the socket timeout.//设置套接字超时。


MongoClientOptions.Builder threadsAllowedToBlockForConnectionMultiplier(int threadsAllowedToBlockForConnectionMultiplier)
Sets the multiplier for number of threads allowed to block waiting for a connection.//设置允许阻塞等待连接的线程数量的倍数。
```



# 四、版本测试

## 低版本数据导入高版本

```shell
数据迁移，2.4不能直接迁到4.0需要从3.2过度一次

导出老版本里指定数据库
./bin/mongodump -o /data07/mongo/backup/dump`date +%Y%m%d` -d testwrite --port 27032

[root@HIHK-XNGL-VOLTE-DN40 mongo3.2]# ./bin/mongodump -o /data07/mongo/backup/dump_`date +%Y%m%d` -d testwrite --port 27032
2019-10-24T11:08:57.780+0800    writing testwrite.t2019102318 to 
2019-10-24T11:08:57.781+0800    writing testwrite.t2019102317 to 
2019-10-24T11:09:00.781+0800    [#######.................]  testwrite.t2019102318  572732/1771058  (32.3%)
2019-10-24T11:09:00.781+0800    [#######.................]  testwrite.t2019102317  573432/1737000  (33.0%)
2019-10-24T11:09:00.781+0800
2019-10-24T11:09:03.781+0800    [################........]  testwrite.t2019102318  1224423/1771058  (69.1%)
2019-10-24T11:09:03.781+0800    [################........]  testwrite.t2019102317  1226605/1737000  (70.6%)
2019-10-24T11:09:03.781+0800
2019-10-24T11:09:06.334+0800    [########################]  testwrite.t2019102317  1737000/1737000  (100.0%)
2019-10-24T11:09:06.334+0800    done dumping testwrite.t2019102317 (1737000 documents)
2019-10-24T11:09:06.483+0800    [########################]  testwrite.t2019102318  1771058/1771058  (100.0%)
2019-10-24T11:09:06.483+0800    done dumping testwrite.t2019102318 (1771058 documents)

导入新版本
./bin/mongorestore -h 10.209.178.240  --port 27032 --dir /data07/mongo/backup/dump20191024/

[root@HIHK-XNGL-VOLTE-DN40 mongo3.2]# ./bin/mongorestore -h 10.209.178.240  --port 27032 --dir /data07/mongo/backup/dump20191024/             
2019-10-24T10:48:13.811+0800    building a list of dbs and collections to restore from /data07/mongo/backup/dump20191024 dir
2019-10-24T10:48:13.812+0800    reading metadata for testwrite.t2019102317 from /data07/mongo/backup/dump20191024/testwrite/t2019102317.metadata.json
2019-10-24T10:48:13.812+0800    reading metadata for testwrite.t2019102318 from /data07/mongo/backup/dump20191024/testwrite/t2019102318.metadata.json
2019-10-24T10:48:13.820+0800    restoring testwrite.t2019102318 from /data07/mongo/backup/dump20191024/testwrite/t2019102318.bson
2019-10-24T10:48:13.826+0800    restoring testwrite.t2019102317 from /data07/mongo/backup/dump20191024/testwrite/t2019102317.bson
2019-10-24T10:48:16.812+0800    [##......................]  testwrite.t2019102318  125MB/1.02GB  (11.9%)
2019-10-24T10:48:16.812+0800    [##......................]  testwrite.t2019102317  123MB/1.02GB  (11.8%)
2019-10-24T10:48:16.812+0800
2019-10-24T10:48:19.812+0800    [#####...................]  testwrite.t2019102318  253MB/1.02GB  (24.1%)
2019-10-24T10:48:19.812+0800    [#####...................]  testwrite.t2019102317  250MB/1.02GB  (23.9%)
2019-10-24T10:48:19.812+0800
2019-10-24T10:48:22.812+0800    [########................]  testwrite.t2019102318  380MB/1.02GB  (36.3%)
2019-10-24T10:48:22.812+0800    [########................]  testwrite.t2019102317  377MB/1.02GB  (36.0%)
2019-10-24T10:48:22.812+0800
2019-10-24T10:48:25.812+0800    [###########.............]  testwrite.t2019102318  507MB/1.02GB  (48.4%)
2019-10-24T10:48:25.812+0800    [###########.............]  testwrite.t2019102317  494MB/1.02GB  (47.2%)
2019-10-24T10:48:25.812+0800
2019-10-24T10:48:28.812+0800    [##############..........]  testwrite.t2019102318  630MB/1.02GB  (60.2%)
2019-10-24T10:48:28.812+0800    [##############..........]  testwrite.t2019102317  617MB/1.02GB  (58.9%)
2019-10-24T10:48:28.812+0800
2019-10-24T10:48:31.812+0800    [#################.......]  testwrite.t2019102318  758MB/1.02GB  (72.4%)
2019-10-24T10:48:31.812+0800    [#################.......]  testwrite.t2019102317  744MB/1.02GB  (71.1%)
2019-10-24T10:48:31.812+0800
2019-10-24T10:48:34.812+0800    [####################....]  testwrite.t2019102318  878MB/1.02GB  (83.9%)
2019-10-24T10:48:34.812+0800    [###################.....]  testwrite.t2019102317  862MB/1.02GB  (82.3%)
2019-10-24T10:48:34.812+0800
2019-10-24T10:48:37.812+0800    [######################..]  testwrite.t2019102318  1000MB/1.02GB  (95.6%)
2019-10-24T10:48:37.812+0800    [######################..]  testwrite.t2019102317   980MB/1.02GB  (93.7%)
2019-10-24T10:48:37.812+0800
2019-10-24T10:48:39.004+0800    [########################]  testwrite.t2019102318  1.02GB/1.02GB  (100.0%)
2019-10-24T10:48:39.004+0800    restoring indexes for collection testwrite.t2019102318 from metadata
2019-10-24T10:48:39.084+0800    Failed: testwrite.t2019102318: error creating indexes for testwrite.t2019102318: createIndex error: Index with name: _id_ already exists with different options

```

## 高版本优势

|          | 特点                                                         |
| -------- | ------------------------------------------------------------ |
| 监控管理 | 目前网上流行的监控客户端都要求版本在2.6以上，我们现在使用的2.4没有客户端软件支持 |
| 性能     | MongoDB3.0较MongoDB2.x在性能上有很大提升  官方给出的测试结果显示：在YCSB测试中，MongoDB3.0在多线程、批量插入场景下较之于MongoDB2.6有大约7倍的增长  实测：并发量提升3-4倍，延迟降低了90%(来自网上) |

**4.0** **版本**

（1）跨文档事务支持 （ACID）

首个支持跨文档事务的NoSQL云数据库，将文档模型的速度，灵活性和功能与ACID保证相结合。现在，使用MongoDB解决各种用例变得更加容易。

（4.0 的事务存在最大修改 16MB、事务执行时间不能过长的限制）

（2）40%迁移速度提升

并发的读取和写入，使得新增分片shard迁移性能提升了约 40%， 新增节点能更快的承载业务压力。

（3）读性能大幅扩展

4.0版本借助事务特性，使得备节点不再因为同步日志而阻塞读取请求。

（4）Change Stream 增强

在MongoDB3.6之前，如果我们希望对MongoDB数据库中的数据变动进行监听，通常是通过 “监听并回放oplog”。从MongoDB3.6开始支持的 Change Streams打破了这个僵局。 Change Streams使得数据的变动监听变得简单易用。如果你只需要针对某一个collection进行变动监听，MongoDB3.6就可以满足你的需求。在4.0版本中我们可以针对若干个数据库或者整个实例（复制集或者sharding）进行变动监听。与watch()某一个collection不同，4.0中我们可以watch()某个数据库或者整个实例。

 

**4.2** **版本**

（1）分布式事务

4.2 支持分布式事务，MongoDB 采用二阶段提交的方式，实现在多个 Shard 间发生的修改，要么同时发生，要么都不发生，保证事务的 ACID 特性。

在使用上，4.2 的分布式事务跟 4.0 副本集事务使用方式完全一样，用户无需关心后端数据如何分布。

（2）高可用

在 4.0 提供了 Retryable Write 功能，在新的 4.2 版本，MongoDB 增加了 Retryable Read 功能，对于一些临时的网络问题，用户无需自己实现重试逻辑，MongoDB 会自动重试处理，保证用户业务的连续性。

（3）Index 增强，增加了Wildcard Index 类型

MongoDB 4.2 引入 Wildcard Index，可以针对一系列的字段自动建索引，满足丰富的查询需求。

（4）字段级别加密

MongoDB 除了支持 SSL、TDE 等安全机制，在 4.2 引入「字段级加密」的支持，实现对用户JSON文档的Value 进行自动加密。整个过程在 Driver 层完成，传输、存储到服务端的文档Value都是密文，MongoDB 4.2 Drvier 支持丰富的加密策略，可以针对集合、字段维度开启加密，加密过程对开发者完全透明。



 ## 写入测试

**高版本api写入mongodb4.0耗时会比较长 **

- 压缩：WiredTiger提供snappy和zlib两种数据压缩方式。snappy提供压缩比低但是性能损耗低，zlib压缩比高但性能损耗较高，通过storage.wiredTiger.collectionConfig.blockCompressor参数设置

* 同一台服务器上，非集群单机测试，插入2个共1.1GB数据
* 写入数据用的java高版本api是3.8，低版本api是2.4
* mongo2.4不支持高版本api

| mongo   | journal | block压缩 | 写入api | 未插入前 | 第一次 | 第二次 | 第三次 | 压缩率 | 耗时 |
| ------- | ------- | --------- | ------- | -------- | ------ | ------ | ------ | ------ | ---- |
| 4.0版本 | zlib    | zlib      | 3.8     | 301MB    | 657MB  | 950MB  | 1.4GB  | 71%    | 73s  |
| 4.0版本 | snappy  | zlib      | 3.8     | 301MB    | 699MB  |        |        | 70%    | 61s  |
| 4.0版本 | zlib    | zlib      | 2.4     | 301MB    | 685MB  | 1.1GB  | 1.5GB  | 70%    | 42s  |
| 4.0版本 | snappy  | zlib      | 2.4     | 301MB    | 677MB  | 1.1GB  | 1.4GB  | 71%    | 39s  |
| 4.0版本 | snappy  | snappy    | 3.8     | 301MB    | 972MB  | 1.7GB  | 2.4GB  | 40%    | 52s  |
| 4.0版本 | snappy  | snappy    | 2.4     | 301MB    | 1.6GB  |        |        | 不压   | 23s  |
| 2.4版本 |         |           | 2.4     | 108MB    | 486MB  | 899MB  | 1.2GB  | 71%    | 40s  |

用低版本2.4api的collection.insert(listdbo);每提交10万行耗时4s

用高版本3.8api的collection.insertMany(listdbo);每提交10万行耗时7s

## 遗留问题

**1、为什么高版本api写入mongodb4.0耗时会比较长？ **

2、为什么连接mongodb4.0，高版本api耗时会比低版本api长1秒多？



# 五、开启鉴权登陆

**1、创建root账户**

```shell
切换到admin数据库
> use admin
使用db.createUser()函数在admin数据库下创建用户
> db.createUser({user:"root",pwd:"123456",roles:[{role:"userAdminAnyDatabase",db:"admin"},{role:"readWriteAnyDatabase",db:"admin"}]})
```

**创建用户成功后，将mongodb.conf文件中的开启安全认证项修改为true**

```shell
vi /opt/mongodb3.6.11/mongodb.conf
修改auth = true，保存退出
```

**重启MongoDB**

2、创建普通用户，两种方式

 * 针对某个数据库(database)建立用户(权限:read,readWrite,write)

   a.创建用户(如果不选中对应的数据库use foobar,那么你创建的用户认证会不成功,也无法登陆mongo):

   ![img](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/MongoDB/919768-20160611112158668-375616792.png)

   b.认证db.auth("user","pwd")--也必须选中对应的数据库use foobar,因为我上面选中了,所以直接就认证,出现1表示认证成功

   ![img](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/MongoDB/919768-20160611112338840-261922593.png)

   c.接下来重启mongo,使用刚才创建的用户登陆,验证权限

   ![img](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/image/MongoDB/919768-20160611113115293-16483371.png)






# 单机部署



```sh
1、解压到opt下

cd /opt
tar -xf /data/soft/database/mongo/mongodb-linux-x86_64-rhel70-4.0.9.tar.gz -C /opt
ln -s mongodb-linux-x86_64-rhel70-4.0.9 mongodb

2、配置数据目录和日志目录
cd /opt/mongodb
mkdir -p  /opt/mongodb/data
mkdir -p  /opt/mongodb/log
mkdir -p  /opt/mongodb/conf
 
3、创建配置文件
vim  /opt/mongodb/conf/config.conf #添加一下内容
systemLog:
  logAppend: true
  path: "/opt/mongodb/log/mongodb.log"                        #存放日志的地方
  destination: file
 
processManagement:
   fork: true                                                   #允许后台运行
   pidFilePath: "/opt/mongodb/mongod.pid"
 
net:
   port: 27017                                                #端口，为安全起见，可以使用其他端口
   bindIp: "0.0.0.0"                                          #生产环境使用127.0.0.1
 
storage:
   dbPath: "/opt/mongodb/data"                                #存放数据的地方
   journal:
      enabled: true
      
      
      
4、配置环境变量
vim /etc/profile
export MONGODB_HOME=/opt/mongodb
export PATH=${MONGODB_HOME}/bin:$PATH
 
#生效
source /etc/profile

5、启动
[root@bgsbtsp0006-dqf:opt]# mongod -f /opt/mongodb/conf/config.conf
 
6、进入数据库
[root@bgsbtsp0006-dqf:opt]# mongo --port 27017

show dbs

```

配置systemctl自启动

```yaml
vim /usr/lib/systemd/system/mongod.service

[Unit]
Description=mongodb
After=network.target remote-fs.target nss-lookup.target  
  
[Service]  
Type=forking  
ExecStart=/opt/mongodb/bin/mongod --config /opt/mongodb/conf/config.conf
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/opt/mongodb/bin/mongod --shutdown --config /opt/mongodb/conf/config.conf
PrivateTmp=true  
    
[Install]  
WantedBy=multi-user.target
```

启动

systemctl start mongod
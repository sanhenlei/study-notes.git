# ELKstack日志收集系统

# 一、简介

ELKstack是由Elasticsearch、Logstash、Kibana三个开源软件组合而成的一款集分析、搜索、图形展示于一身的实施日志收集系统。 
各个组件的功能如下： 

* Elasticsearch：日志分布式存储、搜索工具，支持集群功能，可以将指定时间的日志生成一个索引，加快日志查询和访问。 
* Logstash:日志收集工具，可以从本地磁盘或网络环境的服务器中收集各种文件，然后进行过滤分析，并将日志输入到Elasticsearch中。 
* Kibana：web界面可视化日志展示工具，对Elasticsearch中存储的日志进行展示。

从各个组件的功能可以看出日志收集到展示的整个过程如: 

> 日志——>Logstash——>Elasticsearch——>Kibana 

在日志量相对较大的环境中，由于ELKstack天然支持集群功能及各个组件的灵活使用，我们还可以进行如下改进： 

> 日志——>Logstash——>redis/Kafaka——>Logstash——>Elasticsearch1+Elasticsearch2(集群)——>Kibana——>nginx

  通过ELKstack，我们就再也不需要给开发人员copy日志了，直接在web界面上开个账户即可进行各种查询工作，大大减轻了运维的工作。

在此我们只介绍在普通环境下的部署及使用。

# 二、单机部署

## 1、准备

### 1.1 服务器

192.168.17.121 Elasticsearch+Kibana+Logstash 

日志在那个服务器就把Logstash装在上面

### 1.2 软件

软件不断更新中：建议使用7.0以上

elasticsearch： wget https://download.elastic.co/elasticsearch/elasticsearch/elasticsearch-1.7.3.tar.gz 
logstash    ： wget https://download.elastic.co/logstash/logstash/logstash-2.0.0.tar.gz 
kibana     ： wget https://download.elastic.co/kibana/kibana/kibana-4.1.2-linux-x64.tar.gz  

软件放到移动硬盘的\soft\worksoft\linux\ELK

## 2、安装

### 2.1 Elasticsearch

#### 1.配置java环境，版本为8，如果为7出现告警信息无法启动 

 java -version 

#### 2、安装并创建相关目录 

```shell
cd /opt
tar -zxvf elasticsearch-1.7.3.tar.gz 
ln -s /opt/elasticsearch-1.7.3 /opt/elasticsearch
cd /opt/elasticsearch
mkdir {data,work,logs,plugins}
```

#### 3.修改配置文件 

```shell
vim /opt/elasticsearch/config/elasticsearch.yml

#集群名字
cluster.name: elasticsearch
#集群中节点名，不可重复
node.name: "es-node1"
#是否是master节点
node.master: true
#节点是否存储数据
node.data: true
#索引分片的个数
index.number_of_shards: 5
#分片的副本个数
index.number_of_replicas: 1
#配置文件路径
path.conf: /opt/elasticsearch/config
#数据存放路径
path.data: /opt/elasticsearch/data
#工作路径
path.work: /opt/elasticsearch/work
#日志路径
path.logs: /opt/elasticsearch/logs
#插件路径
path.plugins: /opt/elasticsearch-1.7.3/plugins
#内存不向swap交还
bootstrap.mlockall: true
```

#### 4. 安装启动脚本 

```sehll
git clone https://github.com/elastic/elasticsearch-servicewrapper.git
mv elasticsearch-servicewrapper/service /opt/elasticsearch-1.7.3/bin/
[root@test service]# /opt/elasticsearch-1.7.3/bin/service/elasticsearch install
Detected RHEL or Fedora:
Installing the Elasticsearch daemon..
```

#### 5.配置启动脚本并启动 

```shell
[root@test service]# vim /opt/elasticsearch/bin/service/elasticsearch.conf
#设置ES的安装路径，必须和安装路径保持一致
set.default.ES_HOME=/opt/elasticsearch
#设置分配jvm内存大小
set.default.ES_HEAP_SIZE=1024
[root@test service]# /etc/init.d/elasticsearch start
Starting Elasticsearch...
Waiting for Elasticsearch......
running: PID:29823
[root@test service]# netstat -lntp | grep -E "9200|9300"
tcp        0      0 :::9200                     :::*                        LISTEN      29825/java          
tcp        0      0 :::9300                     :::*                        LISTEN      29825/java 
```

 访问192.168.17.121:9200，检查elasticsearch是否启动正常。  

 **注意：** 
1.由于我们在此只用了一个节点，没有使用集群，若要使用集群请在另一台服务器上按上述配置，但是注意节点名称不能够重复，如**node.name: “es-node1”** 
2.官方还提供了一个ES集群管理插件，可以非常直观的查看ES状态信息，安装方法如下： 

/opt/elasticsearch/bin/plugin -i mobz/elasticsearch-head

 安装完成，访问安装head插件的ES服务器，即可查看集群信息: 
 其中：unassigned表示没有另一个集群节点，而nginx-2015-12和nginx-access-2015-12则是我通过logstash将nginx日志源写入ES的记录，下面我们再进行下一步吧。 

### 2.2  **Logstash** 

 #### 1.确认java环境 
logstash也需要1.8的java环境,安装如上。 

#### 2.安装并创建相关目录 

```shell
cd /opt/
tar -zxvf logstash-2.0.0.tar.gz
ln -sv logstash-2.0.0 logstash
cd /opt/logstash
#logs用于存放日志，etc用于存放配置文件
mkdir {logs,etc}
```

#### 3. 配置logstash启动脚本 

```shell
vim /etc/init.d/logstash


#!/bin/bash
#chkconfig: 2345 55 24
#description: logstash service manager
#logstash配置文件
FILE='/opt/logstash/etc/*.conf'
#指定logstash配置文件的命令
LOGBIN='/opt/logstash/bin/logstash agent --verbose --config'
#用锁文件配合服务启动与关闭
LOCK='/opt/logstash/locks'
#日志
LOGLOG='--log /opt/logstash/logs/stdou.log'

START() {
        if [ -f $LOCK ];then
                echo -e "Logstash is already \033[32mrunning\033[0m, do nothing."
        else
                echo -e "Start logstash service.\033[32mdone\033[m"
                nohup ${LOGBIN} ${FILE} ${LOGLOG} &
                touch $LOCK
        fi
}

STOP() {
        if [ ! -f $LOCK ];then
                echo -e "Logstash is already stop, do nothing."
        else
                echo -e "Stop logstash serivce \033[32mdone\033[m"
                rm -rf $LOCK
                ps -ef | grep logstash | grep -v "grep" | awk '{print $2}' | xargs kill -s 9 >/dev/null
        fi
}

STATUS() {
        ps aux | grep logstash | grep -v "grep" >/dev/null
        if [ -f $LOCK ] && [ $? -eq 0 ]; then
                echo -e "Logstash is: \033[32mrunning\033[0m..."
        else
                echo -e "Logstash is: \033[31mstopped\033[0m..."
        fi
}

TEST(){
        ${LOGBIN} ${FILE} --configtest
}

case "$1" in
  start)
        START
        ;;
  stop)
        STOP
        ;;
  status)
        STATUS
        ;;
  restart)
        STOP
        sleep 2
        START
        ;;
  test)
        TEST
        ;;
  *)
        echo "Usage: /etc/init.d/logstash (test|start|stop|status|restart)"
        ;;
esac

[root@test1 local]# chkconfig --add logstash
[root@test1 local]# chkconfig logstash on

```

####  4.编辑配置文件 

```shell
[root@test1 ~]# vim /opt/logstash/etc/logstash.conf
input {         #表示从标准输入中收集日志
  stdin {}
}

output {       
  elasticsearch  {  #表示将日志输出到ES中
    hosts => ["127.0.0.1:9200"]   #可以指定多台主机，也可以指定集群中的单台主机
  }
}
```



### 5.启动

```shell
#/etc/init.d/logstash或通过以下启动
[root@test1 ~]# /opt/logstash/bin/logstash -f /opt/logstash/etc/logstash.conf
Logstash startup completed
hello world    #这里是自己手动写入的内容
```

 上面我们只是简单通过hello world来简单演示了下，下面我们需要通过kibana来展示。 

logstash出现Unknown setting 'host' for elasticsearch

Unknown setting 'protocol' for elasticsearch

原因：2.0版本以上host修改为 hosts，并取消protocol。

### 2.3  **Kibana** 

 在此只部署单台的Kibana用于图形展示，当然我们还可以通过zookeeper来实现Kibana高可用集群。 

#### 1. 安装 

```shell
cd /opt
tar -zxvf kibana-4.1.2-linux-x64.tar.gz -C /opt/
ln -sv kibana-4.1.2-linux-x64 kibana
```

####  2.修改配置文件 

```shell
vim /opt/kibana/config/kibana.yml
#默认端口可以修改的
server.port: 5601 
#kibana监听的ip     
server.host: "0.0.0.0" 
#由于es在本地主机上面，所以这个选项打开注释即可
elasticsearch.url: "http://localhost:9200" 
```

#### 3.启动脚本 

```shell
[root@test ~]# vim /etc/init.d/kibana 
#!/bin/bash
#chkconfig: 2345 55 24
#description: kibana service manager

KIBBIN='/opt/kibana/bin/kibana'
LOCK='/opt/kibana/locks'

START() {
        if [ -f $LOCK ];then
                echo -e "kibana is already \033[32mrunning\033[0m, do nothing."
        else
                echo -e "Start kibana service.\033[32mdone\033[m"
                cd  /opt/kibana/bin
        nohup ./kibana & >/dev/null
                touch $LOCK
        fi
}

STOP() {
        if [ ! -f $LOCK ];then
                echo -e "kibana is already stop, do nothing."
        else
                echo -e "Stop kibana serivce \033[32mdone\033[m"
                rm -rf $LOCK
                ps -ef | grep kibana | grep -v "grep" | awk '{print $2}' | xargs kill -s 9 >/dev/null
        fi
}

STATUS() {
        Port=$(netstat -tunl | grep ":5602")
        if [ "$Port" != "" ] && [ -f $LOCK ];then
                echo -e "kibana is: \033[32mrunning\033[0m..."
        else
                echo -e "kibana is: \033[31mstopped\033[0m..."
        fi
}

case "$1" in
  start)
        START
        ;;
  stop)
        STOP
        ;;
  status)
        STATUS
        ;;
  restart)
        STOP
    sleep 2
    START
        ;;
  *)
        echo "Usage: /etc/init.d/kibana (|start|stop|status|restart)"
        ;;
esac

[root@test ~]# chmod 755 /etc/init.d/kibana
[root@test ~]# /etc/init.d/kibana start
[root@test ~]# netstat -ntlp |grep :5601
tcp        0      0 0.0.0.0:5601                0.0.0.0:*                   LISTEN      47145/./../node/bin

```

 访问192.168.17.121:5601，检查kibana是否启动 



 ok，经过以上我们的ELK基础环境就搭建完毕了，至于ES集群，Kibana集群及redis/kafaka队列等我们就先不介绍了。下面我们就拿nginx日志来做下展示吧。 

# 三、集群部署

参考文档：https://blog.csdn.net/bacteriumX/article/details/84859773

##  1.ELK工作流程图 

![1576808456105](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/ELK日志管理系统部署/1576808456105.png)



（1）首先最左侧这几个都是logstash agent，我们可以选择其中一种，logstash agent是运行在我们想要收集日志的机器上的，也就是我们想收集哪台机器的日志，我们就把logstash agent安装在哪台机器上。他的作用是收集日志，其中可以选择过滤掉一些日志的行，我们可以设置，下面有详细说明。

（2）logstash agent收集的数据要发送给kafka，这样为了防止在网络阻塞的时候数据丢失。相当于缓冲区，kafka具体内容不介绍，就当个黑盒就好了。logstash agent相当于kafka的producer。

（3）然后logstash从kafka中pull（拉）消息，当做kafka的consumer。

（4）logstash可以将日志解析，（这个解析很抽象，我们见下面的例子），然后将数据发送到elasticsearch，elasticsearch是负责存储数据的。

（5）kibana是展示端，仅仅负责展示ES中存储的数据。

**日志从filebeat 收集到Logstash输出的流程图：**

![1576811047679](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/ELK日志管理系统部署/1576811047679.png)

## 2.服务器和软件规划

| 10.199.155.171            | 10.199.155.183            | 10.199.155.210            | 版本  |
| ------------------------- | ------------------------- | ------------------------- | ----- |
| zookeeper集群（2181）     | zookeeper集群（2181）     | zookeeper集群（2181）     | 3.4.8 |
| kafka集群（9092）         | kafka集群（9092）         | kafka集群（9092）         | 2.4.0 |
| elasticsearch集群（9200） | elasticsearch集群（9200） | elasticsearch集群（9200） | 6.0.0 |
| logstash 收集web日志      | logstash 收集mongo日志    | logstash 收集解码器日志   | 6.0.0 |
| kibana（5601）            |                           |                           | 6.0.0 |

软件下载地址：或取百度网盘下载

`软件持续更新中，建议使用7.0以上`

 https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.0.0.tar.gz 

 https://artifacts.elastic.co/downloads/logstash/logstash-6.0.0.tar.gz

https://artifacts.elastic.co/downloads/kibana/kibana-6.0.0-linux-x86_64.tar.gz 

##  3.Kafka集群部署

### 3.1先安装 zookeeper 

```shell
#查看是否在机器上安装了java环境 
java -version

##三台服务器都创建
cd /opt
tar -zxvf zookeeper-3.4.8.tar.gz
ln -sv zookeeper-3.4.8 zookeeper


#添加环境变量
vim /etc/profile
export ZOOKEEPER_HOME=/opt/zookeeper
export PATH=$ZOOKEEPER_HOME/bin:$PATH

source /etc/profile

##添加启动配置文件
vim /opt/zookeeper/conf/zoo.cfg

tickTime=2000
initLimit=10
syncLimit=5
dataDir=/tmp/zookeeper
clientPort=2181
server.1=10.199.155.171:2888:3888
server.2=10.199.155.183:2888:3888
server.3=10.199.155.210:2888:3888

##主节点
mkdir /tmp/zookeeper
touch /tmp/zookeeper/myid
echo 1 > /tmp/zookeeper/myid

##副节点
mkdir /tmp/zookeeper
touch /tmp/zookeeper/myid
echo 2 > /tmp/zookeeper/myid

##副节点
mkdir /tmp/zookeeper
touch /tmp/zookeeper/myid
echo 3 > /tmp/zookeeper/myid


##进入bin目录，启动，重启，停止，
zkServer.sh start
zkServer.sh stop
zkServer.sh restart
zkServer.sh status

##查看状态命令如下，有QuorumPeerMain表示启动
jsp

##查看集群是否起来，看时候有leader和follower
zkServer.sh status
```

### 3.2 安装kafka

```shell
cd /opt
tar -zxf kafka_2.13-2.4.0.tgz
ln -sv kafka_2.13-2.4.0 kafka

#添加环境变量
vim /etc/profile
export KAFKA_HOME=/opt/kafka
export PATH=$KAFKA_HOME/bin:$PATH

source /etc/profile
```



三台文件分别设置server.properties文件

```shell
# master为0
vim /opt/kafka/config/server.properties


broker.id=0
listeners=PLAINTEXT://10.199.155.171:9092
advertised.listeners=PLAINTEXT://10.199.155.171:9092
num.network.threads=3
num.io.threads=8
socket.send.buffer.bytes=102400
socket.receive.buffer.bytes=102400
socket.request.max.bytes=104857600
log.dirs=/tmp/kafka-logs
num.partitions=5
num.recovery.threads.per.data.dir=1
offsets.topic.replication.factor=3
transaction.state.log.replication.factor=1
transaction.state.log.min.isr=1
log.retention.hours=24
log.segment.bytes=1073741824
log.retention.check.interval.ms=300000
# 连接
zookeeper.connect=10.199.155.171:2181,10.199.155.183:2181,10.199.155.210:2181
zookeeper.connection.timeout.ms=6000
group.initial.rebalance.delay.ms=0
# 可删除topic
delete.topic.enable=true
```

```shell
# master为1
vim /opt/kafka/config/server.properties


broker.id=1
listeners=PLAINTEXT://10.199.155.183:9092
advertised.listeners=PLAINTEXT://10.199.155.183:9092
num.network.threads=3
num.io.threads=8
socket.send.buffer.bytes=102400
socket.receive.buffer.bytes=102400
socket.request.max.bytes=104857600
log.dirs=/tmp/kafka-logs
num.partitions=5
num.recovery.threads.per.data.dir=1
offsets.topic.replication.factor=3
transaction.state.log.replication.factor=1
transaction.state.log.min.isr=1
log.retention.hours=24
log.segment.bytes=1073741824
log.retention.check.interval.ms=300000
# 连接
zookeeper.connect=10.199.155.171:2181,10.199.155.183:2181,10.199.155.210:2181
zookeeper.connection.timeout.ms=6000
group.initial.rebalance.delay.ms=0
# 可删除topic
delete.topic.enable=true
```

```shell
# master为2
vim /opt/kafka/config/server.properties

broker.id=2
listeners=PLAINTEXT://10.199.155.210:9092
advertised.listeners=PLAINTEXT://10.199.155.210:9092
num.network.threads=3
num.io.threads=8
socket.send.buffer.bytes=102400
socket.receive.buffer.bytes=102400
socket.request.max.bytes=104857600
log.dirs=/tmp/kafka-logs
num.partitions=5
num.recovery.threads.per.data.dir=1
offsets.topic.replication.factor=3
transaction.state.log.replication.factor=1
transaction.state.log.min.isr=1
log.retention.hours=24
log.segment.bytes=1073741824
log.retention.check.interval.ms=300000
# 连接
zookeeper.connect=10.199.155.171:2181,10.199.155.183:2181,10.199.155.210:2181
zookeeper.connection.timeout.ms=6000
group.initial.rebalance.delay.ms=0
# 可删除topic
delete.topic.enable=true
```

 ```shell
#启动
nohup kafka-server-start.sh  /opt/kafka/config/server.properties 1>/dev/null 2>&1 &
 ```



### 3.3  kafka操作

```shell
##创建一个topic 
kafka-topics.sh --create --zookeeper 127.0.0.1:2181 --replication-factor 3 --partitions 4 --topic catalina_218
#--partitions 4 　　　　　　　　 # 4个分区
#--replication-factor 3    　　      # 3个副本
# 将分区被复制到三个broker上

##查看有哪些topic
kafka-topics.sh --zookeeper 127.0.0.1:2181  --list
kafka-topics.sh --bootstrap-server 10.199.155.171:9092  --list 

##查看主题情况
kafka-topics.sh --zookeeper 127.0.0.1:2181 --topic catalina_218   --describe

##输入输出测试
# 输入
kafka-console-producer.sh --topic catalina_218 --broker-list 10.199.155.171:9092

# 输出
kafka-console-consumer.sh --bootstrap-server 10.199.155.171:9092 --topic catalina_218_new --from-beginning

# 增加分区数
kafka-topics.sh --zookeeper zk_host:port --alter --topic my_topic_name --partitions 10
# 增加配置
kafka-topics.sh --zookeeper zk_host:port --alter --topic my_topic_name --config flush.messages=1
# 删除配置
kafka-topics.sh --zookeeper zk_host:port --alter --topic my_topic_name --delete-config flush.messages

## 删除topic
#目前删除操作默认情况下只是打上一个删除的标记，再重新启动kafka后才删除。如果需要立即删除，则需要在server.properties中配置：delete.topic.enable=true

先删除kafka存储目录/tmp/kafka-logs/catalina_218-*

/opt/zookeeper/bin/zkCli.sh
rmr /brokers/topics/catalina_218
rmr /admin/delete_topics/catalina_218
rmr /config/topics/catalina_218

kafka-topics.sh --zookeeper 127.0.0.1:2181 --topic catalina_218 --delete 
```

![1576908126466](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/ELK日志管理系统部署/1576908126466.png)

##  4.filebeat的配置

 ### 4.1配置客户机（也就是我们想在这台机器上获得日志）

我们只需要安装一种logstash agent就可以了，我们选择filebeat，比较轻量级，对CPU负载不大，不占用太多资源。

filebeat    :   wget https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-5.5.2-linux-x86_64.tar.gz

比如要收集fast服务器上的mongodb日志，就把filebeat安装在10.199.155.171服务器上

配置文件参数详解：https://blog.csdn.net/chengxuyuanyonghu/article/details/54378778

### 4.2在171上收集mongodb

```shell
cd /opt/
tar -zxf filebeat-5.5.2-linux-x86_64.tar.gz
ln -sv filebeat-5.5.2-linux-x86_64 filebeat
cd filebeat
vim filebeat-kafka.yml

#如下配置
filebeat:
  prospectors:
    - input_type: log
      paths:
      ##要输入文件路径,支持Go Glab的所有模式
        - /data/mongodb/mongos/log/*.log
      ##下面这些可以不用配置
      close_inactive: 1m
      scan_frequency: 5s
      fields:
      file_name: mongoslog
      fields_under_root: true
      close_removed: true
      tail_files: true
      tags: 'fast-mongoslog'
  spool_size: 1024
  idle_timeout: 5s

########################下面选一种
##下面是输出到kafka
output.kafka:
  hosts: ["10.199.155.171:9092", "10.199.155.183:9092", "10.199.155.210:9092"]
  topic: 'mongodb_fast'
  #连接的存活时间.如果为0,表示短连,发送完就关闭.默认为0秒.
  keep_alive: 10s
  
  
##下面是输出到elasticsearch
output.elasticsearch:
  hosts: ["10.199.155.171:9200","10.199.155.210:9200"]
  worker: 4
  index: "kafka_catalina_218_log-%{+yyyy.MM.dd}"
setup.template.name: "filebeat_nginx"
setup.template.overwrite: false
setup.template.pattern: "filebeat_nginx*"


##下面是输出到logstash
output:
  logstash:
    enabled: true
    hosts: ["10.199.155.171:5044"]
    worker: 4
    bulk_max_size: 1024
    compression_level: 6
    loadbalance: false
    index: filebeat
    backoff.max: 120s
    
    

#####启动
cd /opt/filebeat
nohup ./filebeat -c filebeat-kafka.yml > logs/nohup.out 2>&1 &

##查看进程
ps -ef | grep filebeat | grep -v grep | awk '{print $1" | "$2" | "$10}'
```



### 4.3 在218上收集catalina.out日志

```shell
cd /opt/do/
tar -zxf filebeat-5.5.2-linux-x86_64.tar.gz
mv filebeat-5.5.2-linux-x86_64 filebeat
cd filebeat

vim filebeat-catalina.yml

#如下配置
filebeat.prospectors:
- type: log
  enabled: true
  close_rename: true
  close_remove: true
  close_inactive: 1m
  paths: ["/usr/local/apache-tomcat-8.5.31/logs/catalina.out"]
  scan_frequency: 5s
  backoff: 1s
  max_backoff: 10s
  backoff_factor: 2
  harvester_limit: 10
  multiline.pattern: '^\[\d{4}-\d{1,2}-\d{1,2}'
  multiline.negate: true
  multiline.match: after
  multiline.timeout: 5s
  tags: 'web-catalina-218'
  fields:
    filename: catalinalog218
  fields_under_root: true

########################下面选一种
##下面是输出到kafka
output.kafka:
  hosts: ["10.199.155.171:9092", "10.199.155.183:9092", "10.199.155.210:9092"]
  topic: 'catalina_218'
  #连接的存活时间.如果为0,表示短连,发送完就关闭.默认为0秒.
  keep_alive: 10s
  
#####启动
cd /opt/do/filebeat
chmod go-w filebeat-catalina.yml
mkdir logs
nohup ./filebeat -c filebeat-catalina.yml > logs/nohup.out 2>&1 &

##查看进程
ps -ef | grep filebeat

##在10.199.155.171的kafka集群上创建topic
kafka-topics.sh --zookeeper 127.0.0.1:2181 --create  --replication-factor 3 --partitions 4 --topic catalina_218
```

### 4.4收集多个日志源 多个topic输出

```shell
filebeat.inputs:
- type: log
  enabled: true
  paths:
    - /var/log/mylog/test1.log
  fields:
    log_topics: test1
- type: log
  enabled: true
  paths:
    - /var/log/mylog/test2.log
  fields:
    log_topics: test2
output.kafka:
  enabled: true
  hosts: ["10.112.101.90:9092"]
  topic: '%{[fields][log_topics]}'

```

## 5.Elasticsearch集群部署

### 5.1 安装

 wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.0.0.tar.gz 

参考文档：https://blog.csdn.net/laocaicode/article/details/78841053

```shell
##解压
需要非root用户安装
su hixngl
cd /opt
tar -zxf elasticsearch-6.0.0.tar.gz
ln -s elasticsearch-6.0.0 elasticsearch


##创建目录
cd /opt/elasticsearch
mkdir work

mkdir /data/elasticsearch
cd /data/elasticsearch
mkdir {data,logs}


##给三台服务器添加host
vim /etc/hosts
10.199.155.171 fast1
10.199.155.183 fast2
10.199.155.210 fast3

##配置ssh无密匙登录
ssh-keygen -t rsa //一路回车就好
ssh-copy-id -i ~/.ssh/id_rsa.pub fast1
ssh-copy-id -i ~/.ssh/id_rsa.pub fast2
ssh-copy-id -i ~/.ssh/id_rsa.pub fast3

##修改elasticsearch.yml配置文件
vim /opt/elasticsearch/config/elasticsearch.yml
```

6.0版本配置

```yaml
##>>>>>>>>> fast1 >>>>>>>>>>>>
path.data: /data/elasticsearch/data #数据
path.logs: /data/elasticsearch/logs #日志
path.work: /opt/elasticsearch/work  #工作路径
cluster.name: fast_es # 集群的名称
node.name: fast_node_1 # 该节点名称，与前面配置hosts保持一致
node.master: true # 意思是该节点是否可选举为主节点
node.data: true # 表示这不是数据节点
network.host: 0.0.0.0 # 监听全部ip，在实际环境中应为一个安全的ip
http.port: 9200 # es服务的端口号
http.cors.enabled: true
http.cors.allow-origin: "*"
discovery.zen.ping.unicast.hosts: ["10.199.155.171","10.199.155.183","10.199.155.210"] # 配置自动发现
discovery.zen.minimum_master_nodes: 2 #防止集群“脑裂”，需要配置集群最少主节点数目，通常为 (主节点数目/2) + 1


##>>>>>>>>> fast2 >>>>>>>>>>>>
path.data: /data/elasticsearch/data #数据
path.logs: /data/elasticsearch/logs #日志
path.work: /opt/elasticsearch/work  #工作路径
cluster.name: fast_es # 集群的名称
node.name: fast_node_2 # 该节点名称，与前面配置hosts保持一致
node.master: true # 意思是该节点是否可选举为主节点
node.data: true # 表示这不是数据节点
network.host: 0.0.0.0 # 监听全部ip，在实际环境中应为一个安全的ip
http.port: 9200 # es服务的端口号
http.cors.enabled: true
http.cors.allow-origin: "*"
discovery.zen.ping.unicast.hosts: ["10.199.155.171","10.199.155.183","10.199.155.210"] # 配置自动发现
discovery.zen.minimum_master_nodes: 2 #防止集群“脑裂”，需要配置集群最少主节点数目，通常为 (主节点数目/2) + 1

##>>>>>>>>> fast3 >>>>>>>>>>>>
path.data: /data/elasticsearch/data #数据
path.logs: /data/elasticsearch/logs #日志
path.work: /opt/elasticsearch/work  #工作路径
cluster.name: fast_es # 集群的名称
node.name: fast_node_3 # 该节点名称，与前面配置hosts保持一致
node.master: true # 意思是该节点是否可选举为主节点
node.data: true # 表示这不是数据节点
network.host: 0.0.0.0 # 监听全部ip，在实际环境中应为一个安全的ip
http.port: 9200 # es服务的端口号
http.cors.enabled: true
http.cors.allow-origin: "*"
discovery.zen.ping.unicast.hosts: ["10.199.155.171","10.199.155.183","10.199.155.210"] # 配置自动发现
discovery.zen.minimum_master_nodes: 2 #防止集群“脑裂”，需要配置集群最少主节点数目，通常为 (主节点数目/2) + 1
```



6.8版本配置

```yaml
##>>>>>>>>> fast1 >>>>>>>>>>>>
path.data: /data/elasticsearch/data #数据
path.logs: /data/elasticsearch/logs #日志
cluster.name: fast_es # 集群的名称
node.name: fast_node_1 # 该节点名称，与前面配置hosts保持一致
node.master: true # 意思是该节点是否可选举为主节点
node.data: true # 表示这不是数据节点
network.host: 0.0.0.0 # 监听全部ip，在实际环境中应为一个安全的ip
http.port: 9200 # es服务的端口号
http.cors.enabled: true
http.cors.allow-origin: "*"
http.cors.allow-headers: Authorization
xpack.security.enabled: true
xpack.security.transport.ssl.enabled: true
discovery.zen.ping.unicast.hosts: ["10.199.155.171","10.199.155.183","10.199.155.210"] # 配置自动发现
discovery.zen.minimum_master_nodes: 2 #防止集群“脑裂”，需要配置集群最少主节点数目，通常为 (主节点数目/2) + 1


##>>>>>>>>> fast2 >>>>>>>>>>>>
path.data: /data/elasticsearch/data #数据
path.logs: /data/elasticsearch/logs #日志
cluster.name: fast_es # 集群的名称
node.name: fast_node_2 # 该节点名称，与前面配置hosts保持一致
node.master: true # 意思是该节点是否可选举为主节点
node.data: true # 表示这不是数据节点
network.host: 0.0.0.0 # 监听全部ip，在实际环境中应为一个安全的ip
http.port: 9200 # es服务的端口号
http.cors.enabled: true
http.cors.allow-origin: "*"
http.cors.allow-headers: Authorization
xpack.security.enabled: true
xpack.security.transport.ssl.enabled: true
discovery.zen.ping.unicast.hosts: ["10.199.155.171","10.199.155.183","10.199.155.210"] # 配置自动发现
discovery.zen.minimum_master_nodes: 2 #防止集群“脑裂”，需要配置集群最少主节点数目，通常为 (主节点数目/2) + 1

##>>>>>>>>> fast3 >>>>>>>>>>>>
path.data: /data/elasticsearch/data #数据
path.logs: /data/elasticsearch/logs #日志
path.work: /opt/elasticsearch/work  #工作路径
cluster.name: fast_es # 集群的名称
node.name: fast_node_3 # 该节点名称，与前面配置hosts保持一致
node.master: true # 意思是该节点是否可选举为主节点
node.data: true # 表示这不是数据节点
network.host: 0.0.0.0 # 监听全部ip，在实际环境中应为一个安全的ip
http.port: 9200 # es服务的端口号
http.cors.enabled: true
http.cors.allow-origin: "*"
http.cors.allow-headers: Authorization
xpack.security.enabled: true
xpack.security.transport.ssl.enabled: true
discovery.zen.ping.unicast.hosts: ["10.199.155.171","10.199.155.183","10.199.155.210"] # 配置自动发现
discovery.zen.minimum_master_nodes: 2 #防止集群“脑裂”，需要配置集群最少主节点数目，通常为 (主节点数目/2) + 1

```



```sh
##启动
/opt/elasticsearch/bin/elasticsearch -d

##查看端口启动
netstat -lntp | grep -E "9200|9300"

##查看启动日志
tail -100 /data/elasticsearch/logs/fast_es.log

访问10.199.155.171:9200，检查elasticsearch是否启动正常,stats为200
```



### 5.2配置基本安全

**1.7版本**

https://blog.csdn.net/weixin_30298497/article/details/97319498?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522161130377916780265419774%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=161130377916780265419774&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduend~default-2-97319498.first_rank_v2_pc_rank_v29&utm_term=elasticsearch-http-basic&spm=1018.2226.3001.4187

**6.8版本** 

添加xpack配置，还是在elasticsearch.yml配置文件中，添加以下配置：

```yaml
http.cors.enabled: true
http.cors.allow-origin: "*"
http.cors.allow-headers: Authorization
xpack.security.enabled: true
xpack.security.transport.ssl.enabled: true
 
#以下这个可以在浏览器打开测试
#xpack.license.self_generated.type: basic
```

保存文件后，需要重新启动es，必须！

修改访问密码

然后进入bin目录，执行设置密码命令，如下：

```
bin/elasticsearch-setup-passwords interactive
```

指令交互过程中，会让设置4个用户的密码，设置完即可

| 用户                        | 角色                     |
| --------------------------- | ------------------------ |
| elastic                     | 超级管理员               |
| apm_system                  | 为 apm 创建的用户        |
| kibana                      | 为kinana创建的用户       |
| logstash_system             | 为logstash创建的用户     |
| beats_system                | 为 beats 创建的用户      |
| kiremote_monitoring_userana | 为 monitoring 创建的用户 |

xpack默认内置了这些用户，在设置xpack的时候，需要为这些用户设置一个默认值。

### 5.3 elasticsearch启动常见错误

1、max file descriptors [4096] for elasticsearch process is too low, increase to at least [65536]

　　每个进程最大同时打开文件数太小，可通过下面2个命令查看当前数量

```
ulimit -Hn
ulimit -Sn
```

　　修改vi /etc/security/limits.conf文件，增加配置，用户退出后重新登录生效

```
*               soft    nofile          65536
*               hard    nofile          65536
```

![img](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/ELK日志管理系统部署/1031555-20180228192703048-776330364.png)

2、max number of threads [3818] for user [es] is too low, increase to at least [4096]

　　问题同上，最大线程个数太低。修改配置文件/etc/security/limits.conf（和问题1是一个文件），增加配置

```
*               soft    nproc           4096
*               hard    nproc           4096
```

　　可通过命令查看

```
ulimit -Hu
ulimit -Su
```

![img](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/ELK日志管理系统部署/1031555-20180228193331496-1676615485.png)

修改后的文件：

![img](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/ELK日志管理系统部署/1031555-20190830103946644-1722042047.png)

 

 

3、max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]

　　修改/etc/sysctl.conf文件，增加配置vm.max_map_count=262144

```
vi /etc/sysctl.conf

vm.max_map_count=262144
```

　　执行命令sysctl -p生效

![img](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/ELK日志管理系统部署/1031555-20180228170034226-366544473.png)

 4、Exception in thread "main" java.nio.file.AccessDeniedException: /usr/local/elasticsearch/elasticsearch-6.2.2-1/config/jvm.options

　　elasticsearch用户没有该文件夹的权限，执行命令

```
chown -R es:es /usr/local/elasticsearch/
```

 



###  5.3 集群基本操作 

下面是在kibana里操作

```shell
#查看集群健康信息
GET /_cat/health?v

#查看集群中节点信息
GET /_cat/nodes?v

#查看集群中的索引信息
GET /_cat/indices?v

#创建索引
PUT /testindex

#删除索引
DELETE /testindex
```



下面在linux里操作

```shell
#查看集群的健康信息
curl '10.199.155.171:9200/_cluster/health?pretty'
 
#查看集群的详细信息
curl '10.199.155.171:9200/_cluster/state?pretty'
 
#查询索引列表
curl -XGET http://10.199.155.171:9200/_cat/indices?v
 
#创建索引
curl -XPUT http://10.199.155.171:9200/customer?pretty
 
#查询索引
curl -XGET http://10.199.155.171:9200/customer/external/1?pretty
 
#删除索引
curl -XDELETE http://10.199.155.171:9200/customer?pretty
 
#删除指定索引
curl -XDELETE 10.199.155.171:9200/nginx-log-2019.08
 
#删除多个索引
curl -XDELETE 10.199.155.171:9200/system-log-2019.0606,system-log-2019.0607
 
#删除所有索引
curl -XDELETE 10.199.155.171:9200/_all
#在删除数据时，通常不建议使用通配符，误删后果会很严重，所有的index都可能被删除,为了安全起见需要禁止通配符，可以在elasticsearch.yml配置文件中设置禁用_all和*通配符<br><strong>action.destructive_requires_name: true</strong>
```



##  6.logstash部署

 可以配置接入N多种log渠道，现状我配置的只是接入kafka渠道。 

### 6.1安装

下载地址： https://artifacts.elastic.co/downloads/logstash/logstash-6.0.0.tar.gz

参考文档：https://www.cnblogs.com/tonglin0325/p/9044674.html

```shell
cd /opt/
tar -zxvf logstash-6.0.0.tar.gz
ln -sv logstash-6.0.0 logstash
cd /opt/logstash
#logs用于存放日志，etc用于存放配置文件
mkdir logs
```

### 6.2配置启动脚本

```shell
vim /etc/init.d/logstash

#!/bin/bash
#chkconfig: 2345 55 24
#description: logstash service manager
#logstash配置文件
FILE='/opt/logstash/etc/*.conf'
#指定logstash配置文件的命令
LOGBIN='/opt/logstash/bin/logstash agent --verbose --config'
#用锁文件配合服务启动与关闭
LOCK='/opt/logstash/locks'
#日志
LOGLOG='--log /opt/logstash/logs/stdou.log'

START() {
        if [ -f $LOCK ];then
                echo -e "Logstash is already \033[32mrunning\033[0m, do nothing."
        else
                echo -e "Start logstash service.\033[32mdone\033[m"
                nohup ${LOGBIN} ${FILE} ${LOGLOG} &
                touch $LOCK
        fi
}

STOP() {
        if [ ! -f $LOCK ];then
                echo -e "Logstash is already stop, do nothing."
        else
                echo -e "Stop logstash serivce \033[32mdone\033[m"
                rm -rf $LOCK
                ps -ef | grep logstash | grep -v "grep" | awk '{print $2}' | xargs kill -s 9 >/dev/null
        fi
}

STATUS() {
        ps aux | grep logstash | grep -v "grep" >/dev/null
        if [ -f $LOCK ] && [ $? -eq 0 ]; then
                echo -e "Logstash is: \033[32mrunning\033[0m..."
        else
                echo -e "Logstash is: \033[31mstopped\033[0m..."
        fi
}

TEST(){
        ${LOGBIN} ${FILE} --configtest
}

case "$1" in
  start)
        START
        ;;
  stop)
        STOP
        ;;
  status)
        STATUS
        ;;
  restart)
        STOP
        sleep 2
        START
        ;;
  test)
        TEST
        ;;
  *)
        echo "Usage: /etc/init.d/logstash (test|start|stop|status|restart)"
        ;;
esac

[root@test1 local]# chkconfig --add logstash
[root@test1 local]# chkconfig logstash on
```



### 6.3配置取kafka脚本



注意logstash版本

```shell
####################下面格式是logstash2.0版本
###注意收集不同的日志，用不同的文件名
vim /opt/logstash/etc/logstash-kafka.conf


input {
  kafka {
        #Kafka topic
        topic_id => "catalina_218"
        #消费者组 
        group_id => "logstash_data_access_event"
        #zk的链接地址 
        zk_connect => "10.199.155.171:2181,10.199.155.183:2181,10.199.155.210:2181"
        queue_size => 80000
        consumer_threads => 4
        fetch_message_max_bytes => 10485760
  }
}
 
output {
  elasticsearch {
        hosts => ["10.199.155.171:9200","10.199.155.183:9200","10.199.155.210:9200"]
        workers => 4
        codec => "json"
        index => "kafka_catalina_218_log-%{+YYYY.MM.dd}"
  }
}
##########通过以下启动
cd /opt/logstash
./bin/logstash -f etc/logstash-kafka.conf 1>/dev/null 2>&1 &

```



```shell
####################下面格式是logstash5.0以上版本
###注意收集不同的日志，用不同的文件名
input {
    kafka{
		bootstrap_servers => ["10.199.155.171:9092,10.199.155.183:9092,10.199.155.210:9092"]
		group_id => "logstash_data_access_event"
		auto_offset_reset => "earliest"
		#最好与分区数一样多的线程，以实现完美的平衡-线程多于分区意味着某些线程将处于空闲状态
		consumer_threads => "4"
		decorate_events => "false"
		topics => ["catalina_218"]
		type => "catalina_log"
		#用于输入数据的编解码器
		codec => multiline {
    pattern => "^\[\d{4}-\d{1,2}-\d{1,2}\s\d{1,2}:\d{1,2}:\d{1,2}"
    negate => true
    max_lines => 2000
    what => "previous"
  }
	}
}
filter {
  
}

output {

  elasticsearch {
    action => "index"
    hosts  => ["10.199.155.171:9200","10.199.155.183:9200","10.199.155.210:9200"]
    index  => "kafka_catalina_218_log-%{+YYYY.MM.dd}"
    user => "logstash_system"
    password => "Uar_2016"
    codec => "json"
  }
}

##########通过以下启动
cd /opt/logstash
 bin/logstash -f config/logstash-kafka.conf 1>/dev/null 2>&1 &
 
###查看启动日志
tail -f /opt/logstash/logs/logstash-plain.log
```

这里需要注意的就四个地方

* bootstrap_servers 也就是kafka集群的地址，在filebeat端要求单个地址加引号，这里是集群地址放一起加引号。
* group_id 这里必须保证唯一，是你这个logstash集群消费kafka集群的身份标识。
* topics  filebeat和logstash使用的topic一致。
* codec => json  由于beat传输数据给kafka集群的时候，会附加很多tag，默认情况下，logstash就会将这串tag也认为是message的一部分。这样不利于后期的数据处理。所有需要添加codec处理。得到原本的message数据。

```shell
	kafka{
		bootstrap_servers => ["10.199.155.171,10.199.155.183,10.199.155.210"]
		group_id => "baicai"
		auto_offset_reset => "earliest"
		consumer_threads => "5"
		decorate_events => "false"
		topics => ["beattest"]
		type => "bbs_log"
		codec => json
	}
```

### 6.4 配置mongodb脚本

下面收集的是fast服务器上装的测试mongodb，这个logstash放到183下收集

先在kafka创建topic

```shell
kafka-topics.sh --create --zookeeper 127.0.0.1:2181 --replication-factor 3 --partitions 4 --topic mongodb_fast
```



```shell
###注意收集不同的日志，用不同的文件名
vim /opt/logstash/etc/logstash-mongodb-fast.conf

input {
  kafka {
        #Kafka topic
        topic_id => "mongodb_fast"
        #消费者组 
        group_id => "logstash_data_access_event"
        #zk的链接地址 
        zk_connect => "10.199.155.171:2181,10.199.155.183:2181,10.199.155.210:2181"
        queue_size => 80000
        consumer_threads => 4
        fetch_message_max_bytes => 10485760
  }
}

filter {
    if [type] == "mongoslog" {
        grok {
            match => ["message","%{TIMESTAMP_ISO8601:timestamp}\s+%{MONGO3_SEVERITY:severity}\s+%{MONGO3_COMPONENT:component}\s+(?:\[%{DATA:context}\])?\s+%{GREEDYDATA:body}"]
        }
        if [component] =~ "COMMAND" {
            grok {
                match => ["body","%{WORD:command_type}\s+%{DATA:db_name}\s+\w+\:\s+%{GREEDYDATA:command}\s+%{INT:spend_time}ms$"]
            }
        }
        date {
            match => [ "timestamp","UNIX", "YYYY-MM-dd HH:mm:ss", "ISO8601"]
            remove_field => [ "timestamp" ]
        }
    }
}

output {
 ####输出一份到屏幕便于观察，实际环境不需要
  #stdout {
  #  codec => rubydebug
  #}
  elasticsearch {
        hosts => ["10.199.155.171:9200","10.199.155.183:9200","10.199.155.210:9200"]
        workers => 4
        index => "kafka_mongodb_fast_log-%{+YYYY.MM.dd}"
  }
}


##########通过以下启动
cd /opt/logstash
./bin/logstash -f etc/logstash-mongodb-fast.conf 1>/opt/logstash/logs/nohup.out 2>&1 &

##查找进程
ps -ef | grep logstash | grep -v grep | awk '{print $1" | "$2" | "$34}'
```

### 6.5 Logstash配置详解

https://blog.csdn.net/hushukang/article/details/84423184



### 6.6  Logstash开启debug模式观察运行情况

> output {
>   stdout {
>         codec => rubydebug
>     }
> }
>
> 
>
> /opt/logstash/bin/logstash -f  /opt/logstash/config/logstash-deapOceanMT.conf --verbose --debug

## 7  **Kibana** 

 在此只部署单台的Kibana用于图形展示，当然我们还可以通过zookeeper来实现Kibana高可用集群。 

#### 7.1. 安装 

 wget https://artifacts.elastic.co/downloads/kibana/kibana-6.0.0-linux-x86_64.tar.gz 

```shell
cd /opt
tar -zxvf kibana-6.0.0-linux-x86_64.tar.gz
ln -sv kibana-6.0.0-linux-x86_64 kibana
```

####  7.2.修改配置文件 

```shell
vim /opt/kibana/config/kibana.yml
#默认端口可以修改的
server.port: 5601 
#kibana监听的ip     
server.host: "0.0.0.0" 
#由于es在本地主机上面，所以这个选项打开注释即可
elasticsearch.url: "http://0.0.0.0:9200" 
```

#### 7.3.启动脚本 

```shell
[root@test ~]# vim /etc/init.d/kibana 
#!/bin/bash
#chkconfig: 2345 55 24
#description: kibana service manager

KIBBIN='/opt/kibana/bin/kibana'
LOCK='/opt/kibana/locks'

START() {
        if [ -f $LOCK ];then
                echo -e "kibana is already \033[32mrunning\033[0m, do nothing."
        else
                echo -e "Start kibana service.\033[32mdone\033[m"
                cd  /opt/kibana/bin
        nohup ./kibana & >/dev/null
                touch $LOCK
        fi
}

STOP() {
        if [ ! -f $LOCK ];then
                echo -e "kibana is already stop, do nothing."
        else
                echo -e "Stop kibana serivce \033[32mdone\033[m"
                rm -rf $LOCK
                ps -ef | grep kibana | grep -v "grep" | awk '{print $2}' | xargs kill -s 9 >/dev/null
        fi
}

STATUS() {
        Port=$(netstat -tunl | grep ":5602")
        if [ "$Port" != "" ] && [ -f $LOCK ];then
                echo -e "kibana is: \033[32mrunning\033[0m..."
        else
                echo -e "kibana is: \033[31mstopped\033[0m..."
        fi
}

case "$1" in
  start)
        START
        ;;
  stop)
        STOP
        ;;
  status)
        STATUS
        ;;
  restart)
        STOP
    sleep 2
    START
        ;;
  *)
        echo "Usage: /etc/init.d/kibana (|start|stop|status|restart)"
        ;;
esac

[root@test ~]# chmod 755 /etc/init.d/kibana
[root@test ~]# /etc/init.d/kibana start
[root@test ~]# netstat -ntlp |grep :5601
tcp        0      0 0.0.0.0:5601                0.0.0.0:*                   LISTEN      47145/./../node/bin


nohup /opt/kibana/bin/kibana & >/dev/null
```

>进程不可能找不到，用fuser -n tcp 5601

 访问10.199.155.171:5601，检查kibana是否启动 



#### 7.4. kibana认证登陆

* 1、先安装httpd软件，

  在线安装：yum -y install httpd-tools

  离线安装下面四个包：

  > rpm -ivh --nodeps 包全名

  rpm -ivh --nodeps apr-1.4.8-5.el7.x86_64.rpm
  rpm -ivh --nodeps apr-util-1.5.2-6.el7.x86_64.rpm
  rpm -ivh --nodeps httpd-2.4.6-90.el7.centos.x86_64.rpm
  rpm -ivh --nodeps httpd-tools-2.4.6-90.el7.centos.x86_64.rpm

* 2、生成密码文件：

  mkdir -p /opt/nginx/passwd

  htpasswd -c -b /opt/nginx/passwd/kibana.passwd  hixngl Xngl@2020

* 3、安装nginx

  在线安装：yum -y install nginx

  离线先按照依赖包：

  > rpm -ivh --nodeps 包全名

  ![image-20200213162110264](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/ELK日志管理系统部署/image-20200213162110264.png)

  解压nginx-1.13.9.tar.gz

  > tar -zxvf nginx-1.13.9.tar.gz

  cd到解压后的nginx目录，使用默认配置

  > cd nginx-1.14.0
  > ./configure

  如果报错需要安装：

  ![image-20200217234536092](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/ELK日志管理系统部署/image-20200217234536092.png)

  yum -y install pcre-devel

  yum -y install openssl openssl-devel

  编译安装

  > make && make install

  启动、停止nginx

  > cd /usr/local/nginx/sbin/
  > ./nginx
  > ./nginx -s stop
  > ./nginx -s quit
  > ./nginx -s reload

  查询nginx进程：

  > ps aux|grep nginx

  开机自启动，编辑vi /etc/rc.local增加一行 /usr/local/nginx/sbin/nginx

* 配置nginx

  vim /usr/local/nginx/conf/nginx.conf

  > server {
  >     listen       192.168.17.121:5601;
  >     location / {
  >             auth_basic "Kibana Login Auth";
  >             auth_basic_user_file /opt/nginx/passwd/kibana.passwd;
  >             proxy_pass http://127.0.0.1:5601;
  >             proxy_redirect off;
  >     }

  /usr/local/nginx/sbin/nginx -s reload

* 配置

  vim /opt/kibana/config/kibana.yml

  > #host: "0.0.0.0"
  > host: "127.0.0.1"

  重启

  /etc/init.d/kibana stop

  /etc/init.d/kibana start

* 在浏览器输入http://192.168.17.121:5601

  ![image-20200213172109547](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/ELK日志管理系统部署/image-20200213172109547.png)

## 8.参考网站

集群搭建：https://www.cnblogs.com/heyongboke/p/11348325.html

Logstash 参考指南：https://segmentfault.com/a/1190000016615601?utm_source=tag-newest

ElasticSearch最全详细使用教程：http://blog.itpub.net/29715045/viewspace-2653369/

谈一谈Elasticsearch的集群：https://blog.csdn.net/zwgdft/article/details/54585644

## 9.登陆认证

参考https://blog.csdn.net/qq_41631365/article/details/109181240

# 四、常用日志采集配置

input类型很多，可以通过官网查看说明

https://www.elastic.co/guide/en/logstash/current/input-plugins.html

![1576909729878](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/ELK日志管理系统部署/1576909729878.png)

下面举几个常用的：

## 文件类型

```shell
input{
    file{
        #path属性接受的参数是一个数组，其含义是标明需要读取的文件位置
        path => [‘pathA’，‘pathB’]
        #表示多就去path路径下查看是够有新的文件产生。默认是15秒检查一次。
        discover_interval => 15
        #排除那些文件，也就是不去读取那些文件
        exclude => [‘fileName1’,‘fileNmae2’]
        #被监听的文件多久没更新后断开连接不在监听，默认是一个小时。
        close_older => 3600
        #在每次检查文件列 表的时候， 如果一个文件的最后 修改时间 超过这个值， 就忽略这个文件。 默认一天。
        ignore_older => 86400
        #logstash 每隔多 久检查一次被监听文件状态（ 是否有更新） ， 默认是 1 秒。
        stat_interval => 1
        #sincedb记录数据上一次的读取位置的一个index
        sincedb_path => ’$HOME/. sincedb‘
        #logstash 从什么 位置开始读取文件数据， 默认是结束位置 也可以设置为：beginning 从头开始
        start_position => ‘beginning’
        #注意：这里需要提醒大家的是，如果你需要每次都从同开始读取文件的话，关设置start_position => beginning是没有用的，你可以选择sincedb_path 定义为 /dev/null
    }            
}
```

## 数据库类型

```shell
input{
    jdbc{
    #jdbc sql server 驱动,各个数据库都有对应的驱动，需自己下载
    jdbc_driver_library => "/etc/logstash/driver.d/sqljdbc_2.0/enu/sqljdbc4.jar"
    #jdbc class 不同数据库有不同的 class 配置
    jdbc_driver_class => "com.microsoft.sqlserver.jdbc.SQLServerDriver"
    #配置数据库连接 ip 和端口，以及数据库    
    jdbc_connection_string => "jdbc:sqlserver://200.200.0.18:1433;databaseName=test_db"
    #配置数据库用户名
    jdbc_user =>   
    #配置数据库密码
    jdbc_password =>
    #上面这些都不重要，要是这些都看不懂的话，你的老板估计要考虑换人了。重要的是接下来的内容。
    # 定时器 多久执行一次SQL，默认是一分钟
    # schedule => 分 时 天 月 年  
    # schedule => * 22  *  *  * 表示每天22点执行一次
    schedule => "* * * * *"
    #是否清除 last_run_metadata_path 的记录,如果为真那么每次都相当于从头开始查询所有的数据库记录
    clean_run => false
    #是否需要记录某个column 的值,如果 record_last_run 为真,可以自定义我们需要表的字段名称，
    #此时该参数就要为 true. 否则默认 track 的是 timestamp 的值.
    use_column_value => true
    #如果 use_column_value 为真,需配置此参数. 这个参数就是数据库给出的一个字段名称。当然该字段必须是递增的，可以是 数据库的数据时间这类的
    tracking_column => create_time
    #是否记录上次执行结果, 如果为真,将会把上次执行到的 tracking_column 字段的值记录下来,保存到 last_run_metadata_path 指定的文件中
    record_last_run => true
    #们只需要在 SQL 语句中 WHERE MY_ID > :last_sql_value 即可. 其中 :last_sql_value 取得就是该文件中的值
    last_run_metadata_path => "/etc/logstash/run_metadata.d/my_info"
    #是否将字段名称转小写。
    #这里有个小的提示，如果你这前就处理过一次数据，并且在Kibana中有对应的搜索需求的话，还是改为true，
    #因为默认是true，并且Kibana是大小写区分的。准确的说应该是ES大小写区分
    lowercase_column_names => false
    #你的SQL的位置，当然，你的SQL也可以直接写在这里。
    #statement => SELECT * FROM tabeName t WHERE  t.creat_time > :last_sql_value
    statement_filepath => "/etc/logstash/statement_file.d/my_info.sql"
    #数据类型，标明你属于那一方势力。单了ES哪里好给你安排不同的山头。
    type => "my_info"
    }
    #注意：外载的SQL文件就是一个文本文件就可以了，还有需要注意的是，一个jdbc{}插件就只能处理一个SQL语句，
    #如果你有多个SQL需要处理的话，只能在重新建立一个jdbc{}插件。
}
```

## beats类型

```shell
input {
  beats {
    #接受数据端口
    port => 5044
    #数据类型
    type => "logs"
  }
  #这个插件需要和filebeat进行配很这里不做多讲，到时候结合起来一起介绍。
}
```



## tomcate的catalina.out日志

```shell
input {
  file {
    type => "erp_log"
    path => "/var/log/nginx/catalina.out"
    start_position => "beginning"
    codec => multiline {
      pattern => "^%{YEAR}-%{MONTHNUM}-%{MONTHDAY} %{HOUR}:?%{MINUTE}(?::?%{SECOND})"
      negate => true
      what => "previous"
    }
  }
}
output {
  redis {
    port => 6379
    host => ["192.168.10.214"]
    data_type => "list"
    key => "erp-%{type}"
  }
 stdout {
   codec => rubydebug
 }
}
```

## mongodb日志

```shell
input { 
    file { 
         path => "/opt/mongodb/mongos/log/mongos.log"
         type => "mongoslog"
    }
}

filter {
    if [type] == "mongoslog" {
        grok {
            match => ["message","%{TIMESTAMP_ISO8601:timestamp}\s+%{MONGO3_SEVERITY:severity}\s+%{MONGO3_COMPONENT:component}\s+(?:\[%{DATA:context}\])?\s+%{GREEDYDATA:body}"]
        }
        if [component] =~ "COMMAND" {
            grok {
                match => ["body","%{WORD:command_type}\s+%{DATA:db_name}\s+\w+\:\s+%{GREEDYDATA:command}\s+%{INT:spend_time}ms$"]
            }
        }
        date {
            match => [ "timestamp","UNIX", "YYYY-MM-dd HH:mm:ss", "ISO8601"]
            remove_field => [ "timestamp" ]
        }
    }
}

#指定 es 的 host 和索引。另外测试结果输出一份到屏幕便于观察。
output{
    elasticsearch {
        hosts => ["127.0.0.1:9200"]
        index => "mongoslog"
    }
    stdout {
        codec => rubydebug 
    }
}
```

## oracle脚本数据

```shell
input {
  jdbc {//每秒更新新增数据
    jdbc_driver_library => "/data/elasticsearch/logstash/ojdbc6-11.2.0.3.jar"
    jdbc_driver_class => "Java::oracle.jdbc.driver.OracleDriver"
    jdbc_connection_string => "jdbc:oracle:thin:@192.168.54.15:1521:ORCL"
    jdbc_user => "oracle"
    jdbc_password => "oracle"
    schedule => "* * * * * *"
    statement_filepath => "/data/elasticsearch/logstash/sql/test_oracle_one.sql"
    record_last_run => true
    use_column_value => true
    tracking_column => rn
    codec => plain { charset => "UTF-8"}
    tracking_column_type => numeric
    last_run_metadata_path => "/data/elasticsearch/logstash/metadata-path/test_oracle_one_last_id"
    clean_run => "false"
    jdbc_paging_enabled => true
    jdbc_page_size => 10000
  }
  jdbc {//每分钟进行全量更新
    jdbc_driver_library => "/data/elasticsearch/logstash/ojdbc6-11.2.0.3.jar"
    jdbc_driver_class => "Java::oracle.jdbc.driver.OracleDriver"
    jdbc_connection_string => "jdbc:oracle:thin:@192.168.54.15:1521:ORCL"
    jdbc_user => "oracle"
    jdbc_password => "oracle"
    schedule => "* * * * *"
    statement_filepath => "/data/elasticsearch/logstash/sql/test_oracle_one_all.sql"
    use_column_value => true
    tracking_column_type => numeric
    tracking_column => rn
    codec => plain { charset => "UTF-8"}
    jdbc_paging_enabled => true
    jdbc_page_size => 10000
  }
}
filter {
  date {
    match => [ "timestamp" , "yyyy/MM/dd HH:mm:ss Z" ]
  }
}
output {
  elasticsearch {
    hosts => ["192.168.54.11:9200"]
    index => "test_oracle_one"
    document_type => "_doc"
    document_id => "%{rn}"
  }
}
```



# 五、应用



## 日志收集步骤

### 1、分析日志组成

如：tomcate日志，配置tomcat日志为json格式

```xml
#注释原有日志
        <!-- Access log processes all example.
             Documentation at: /docs/config/valve.html
             Note: The pattern used is equivalent to using pattern="common" -->
<!--        <Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs"
               prefix="localhost_access_log." suffix=".txt"
               pattern="%h %l %u %t &quot;%r&quot; %s %b" />
-->
        <Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs"
               prefix="tomcat_access_log" suffix=".log"
               pattern="{&quot;clientip&quot;:&quot;%h&quot;,&quot;ClientUser&quot;:&quot;%l&quot;,&quot;authenticated&quot;:&quot;%u&quot;,&quot;AccessTime&quot;:&quot;%t&quot;,&quot;method&quot;:&quot;%r&quot;,&quot;status&quot;:&quot;%s&quot;,&quot;SendBytes&quot;:&quot;%b&quot;,&quot;Query?string&quot;:&quot;%q&quot;,&quot;partner&quot;:&quot;%{Referer}i&quot;,&quot;AgentVersion&quot;:&quot;%{User-Agent}i&quot;}"/>
```

### 2.编写logstash配置文件

```shell
vi /opt/logstash/config/tomcat.conf 

input {
    file {
      path => "/apps/tomcat/logs/tomcat_access_log*.log"
      type => "tomcat-access-log-ceshi"
      start_position => "beginning"
      stat_interval => "2"
    }
}
output {
    elasticsearch {
      hosts => ["192.168.17.121:9200"]
      index => "logstash-tomcat-access-log-ceshi-%{+YYYY.MM.dd}"
    }
}
```

### 3.启动监听

>cd /opt/logstash
>
>bin/logstash -f config/tomcat.conf 

## mongodb日志收集

说明：我们通过ELKstack环境收集mongodb日志，来实现相关图形展示。 

### 1. 数据源输入

这里直接指定 mongo 日志的路径， 如果有多种类型的日志，可以指定 type 标志来区分

```shell
input { 
    file {  //当输入源是文件时
         path => "/opt/mongodb/mongos/log/mongos.log"
         type => "mongoslog"
    }
}

filter {
    if [type] == "mongoslog" {
        grok {
            match => ["message","%{TIMESTAMP_ISO8601:timestamp}\s+%{MONGO3_SEVERITY:severity}\s+%{MONGO3_COMPONENT:component}\s+(?:\[%{DATA:context}\])?\s+%{GREEDYDATA:body}"]
        }
        if [component] =~ "COMMAND" {
            grok {
                match => ["body","%{WORD:command_type}\s+%{DATA:db_name}\s+\w+\:\s+%{GREEDYDATA:command}\s+%{INT:spend_time}ms$"]
            }
        }
        date {
            match => [ "timestamp","UNIX", "YYYY-MM-dd HH:mm:ss", "ISO8601"]
            remove_field => [ "timestamp" ]
        }
    }
}

#指定 es 的 host 和索引。另外测试结果输出一份到屏幕便于观察。
output{
    elasticsearch {
        hosts => ["127.0.0.1:9200"]
        index => "mongolog"
    }
    stdout {
        codec => rubydebug 
    }
}

```





###  2.日志分析和过滤

filter 将 input 的结果进行处理，并转为 es 中对应的字段。以下是对 type 为 mongolog 的日志进行处理。grok 是logstash 内置的正则解析引擎，包含丰富的正则表达式。

那么接下来是对于 mongo 日志的解析。根据 mongo 官网 （日志规范 ）对日志的描述，mongo3.0 以上版本的日志格式为：

><timestamp> <severity> <component> [<context>] <message>
>@timestamp : 时间
>@severity : 日志级别，分为: F(Fatal)/E(Error)/W(Warning)/I(Informational)/D(Debug)
>@component: 包括 /NETWORK/ACCESS/COMMMAND/GEO/INDEX/QUERY/ROLLBACK/SHARDING/JOURNAL/WRITE 等
>@context: 上下文，主要打印数据库连接
>@message: 具体的日志数据

 其中 compoent = COMMAND 基本涵盖了大部分的增删改查等数据库操作，这一部分的 message 是我们需要具体关注的。以下是部分有代表性的日志 

> // NETWROK 建立连接的日志
> 2018-01-02T10:47:02.253+0800 I NETWORK  [initandlisten] connection accepted from anonymous unix socket #179873 (349 connections now open)
>
> // ACCESS 接入失败的日志
> 2016-09-24T02:01:03.404+0800 I ACCESS   [conn2232] Failed to authenticate Opt_ALL@admin with mechanism MONGODB-CR: AuthenticationFailed UserNotFound Could not find user Opt_ALL@admin
>
> // COMMAND 日志
> 2016-08-14T19:08:54.569+0800 I COMMAND  [conn4521] insert im.user_msg ninserted:1 keyUpdates:0 writeConflicts:0 numYields:0 locks:{ Global: { acquireCount: { r: 1, w: 1 } }, Database: { acquireCount: { w: 1 } }, Collection: { acquireCount: { w: 1 } } } 492ms
> 2018-01-03T13:28:17.308+0800 I COMMAND  [conn9678] query task.opt_log query: { query: { did: 519390, taskid: 172 }, orderby: { opt_time: -1 } } planSummary: IXSCAN { did: 1.0, taskid: 1.0, opt_time: 1.0 } ntoreturn:3 ntoskip:0 nscanned:2 nscannedObjects:2 keyUpdates:0 writeConflicts:0 numYields:2 nreturned:2 reslen:429 locks:{ Global: { acquireCount: { r: 6 } }, Database: { acquireCount: { r: 3 } }, Collection: { acquireCount: { r: 3 } } } 120ms
> 2016-08-14T19:09:02.836+0800 I COMMAND  [conn4521] remove im.user_msg query: { did: 10000, pid: 8445, msg_id: { $lte: 510 } } ndeleted:100 keyUpdates:0 writeConflicts:0 numYields:1 locks:{ Global: { acquireCount: { r: 2, w: 2 } }, Database: { acquireCount: { w: 2 } }, Collection: { acquireCount: { w: 2 } } } 212ms

 以上述 COMMAND 类型的日志为例，其 message 部分又可以拆分出以下的格式 

><command_type> <databasename> <command_detail> <spend_time>
>@command_type : 操作类型，比如 query/insert/update/remove/getmore(迭代返回) 等等
>@databasename : 操作的数据库，比如 im.user_msg
>@command_detail : 会输出具体的 queryplan，如果是全表扫描会出现 COLL_SCAN 之类的关键词
>@spend_time : 花费时间，单位是 ms 

 通过对以上的mongo日志格式分析，我们现在就可以使用grok 解析出这些字段。 grok 内置了大部分的正则表达式，可以直接当做标签使用，比如用于匹配时间的 TIMESTAMP_ISO8601，MONGO3_SEVERITY 等等。每个标签后面可以指定这部分匹配结果的字段名。 

```shell
filter {
    if [type] == "mongoslog" {
        grok {
            match => ["message","%{TIMESTAMP_ISO8601:timestamp}\s+%{MONGO3_SEVERITY:severity}\s+%{MONGO3_COMPONENT:component}\s+(?:\[%{DATA:context}\])?\s+%{GREEDYDATA:body}"]
        }
        if [component] =~ "COMMAND" {
            grok {
                match => ["body","%{WORD:command_type}\s+%{DATA:db_name}\s+\w+\:\s+%{GREEDYDATA:command}\s+%{INT:spend_time}ms$"]
            }
        }
        date {
            match => [ "timestamp","UNIX", "YYYY-MM-dd HH:mm:ss", "ISO8601"]
            remove_field => [ "timestamp" ]
        }
    }
}
```



### 3.输出至 elastcisearch。

 指定 es 的 host 和索引。另外测试结果输出一份到屏幕便于观察。 

```shell
output{
    elasticsearch {
        hosts => ["127.0.0.1:9200"]
        index => "mongoslog"
    }
    stdout {
        codec => rubydebug 
    }
}
```



### 4.最终配置文件

> cd /opt/logstash/config
>
> vi mongos-logstash.conf 

```shell
input { 
    file { 
         path => "/opt/mongodb/mongos/log/mongos.log"
         type => "mongoslog"
    }
}

filter {
    if [type] == "mongoslog" {
        grok {
            match => ["message","%{TIMESTAMP_ISO8601:timestamp}\s+%{MONGO3_SEVERITY:severity}\s+%{MONGO3_COMPONENT:component}\s+(?:\[%{DATA:context}\])?\s+%{GREEDYDATA:body}"]
        }
        if [component] =~ "COMMAND" {
            grok {
                match => ["body","%{WORD:command_type}\s+%{DATA:db_name}\s+\w+\:\s+%{GREEDYDATA:command}\s+%{INT:spend_time}ms$"]
            }
        }
        date {
            match => [ "timestamp","UNIX", "YYYY-MM-dd HH:mm:ss", "ISO8601"]
            remove_field => [ "timestamp" ]
        }
    }
}

#指定 es 的 host 和索引。另外测试结果输出一份到屏幕便于观察。
output{
    elasticsearch {
        hosts => ["127.0.0.1:9200"]
        index => "mongoslog"
    }
    stdout {
        codec => rubydebug 
    }
}
```

 配置文件搞定之后就可以运行 

> cd /opt/logstash
>
> bin/logstash -f config/mongos-logstash.conf 

### 5.测试

#### 5.1 登陆mongos 产生登陆日志记录

```shell
[root@mongodb1 bin]# cd /opt/mongodb/bin   
[root@mongodb1 bin]# ./mongo 192.168.17.121:20000
MongoDB shell version v4.0.9
connecting to: mongodb://192.168.17.121:20000/test?gssapiServiceName=mongodb
Implicit session: session { "id" : UUID("49347def-0aa5-456a-833d-e2094685ad68") }
MongoDB server version: 4.0.9
Server has startup warnings: 
2019-11-19T11:14:49.396+0800 I CONTROL  [main] 
2019-11-19T11:14:49.396+0800 I CONTROL  [main] ** WARNING: Access control is not enabled for the database.
2019-11-19T11:14:49.396+0800 I CONTROL  [main] **          Read and write access to data and configuration is unrestricted.
2019-11-19T11:14:49.396+0800 I CONTROL  [main] ** WARNING: You are running this process as the root user, which is not recommended.
2019-11-19T11:14:49.396+0800 I CONTROL  [main] 
mongos> 
```

#### 5.2 查看logstash是否有数据

![1576744422152](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/ELK日志管理系统部署/1576744422152.png)

#### 5.3 在Kibana上查看

##### 1. 配置一个项目

![1576744934585](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/ELK日志管理系统部署/1576744934585.png)

##### 2. Discover 界面

看到的是es 元数据，默认只显示最近15 分钟的数据，如果需要查看更多，可以修改右上角的timefilter。可以用正则去搜索数据，并保存正则表达式用于多次搜索。 

![1576744980511](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/ELK日志管理系统部署/1576744980511.png)

向表里添加列

![1576747831850](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/ELK日志管理系统部署/1576747831850.png)

##### 3. Visualize界面 

 Visualize 可以生成多种类型的图表，并支持保存。 

比如需要查看一段时间内数据库所有表的查询次数，以直方图的形式展现出来。

根据一段时间内数据库查询生成所有表的查询次数直方图

![1576745747988](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/ELK日志管理系统部署/1576745747988.png)

##### 4. 应用场景发散
可以看到上述平台几乎可以非常轻松的搭建在任何服务器上，使得对文本型日志的监控运维更方便更高效。

如果线下专门搭建一个用于分析的 es 服务器，定时从线上数据库的副本同步数据到本地，可以进行各种分析。

比如现在领导们需要看一些统计报表，就可以不需要我们去写脚本运行了（当然建立合适的索引是es能否胜任报表分析维度的关键）。
比如现在的统计需求，如果产品那边不要求实时性的话，我们完全可以摒弃掉使用程序接口来统计数据的方式。这样极大提高生产力。

##### 5. 问题总结与优化
日志写入速率和 ES 导入数据速率不匹配的情况。如果用于线上大型的日志收集的话，这是可能出现的一种情况，logstash 作为一个管道，并没有缓存数据的功能，需要额外加入中间件来保证日志的写入速率和数据导入数据匹配。
线下测试环境发现，ES 服务器的单个进程可用文件描述符应开到2048以上，否则容易出现文件描述符不够用的情况，导致 logstash 无法写入数据。
对于日志采集端来说，logstash 其实是很重的组件，可以考虑使用更加轻量的 filebeats 来作为日志采集器，提高服务器性能的利用率。
当前没有使用大量的数据进行压力测试，因此对es 的性能问题无法全面的估计。
整个环境的部署可以进一步集成和简化，将相应版本的安装包和配置文件上传到 SVN，然后在任意服务器均可打包安装。

# 六、Logstash工作原理

 比较好的文档：https://doc.yonyoucloud.com/doc/logstash-best-practice-cn/index.html

Logstash事件处理管道有三个阶段：输入→过滤器→输出，输入生成事件，过滤器修改它们，然后输出将它们发送到其他地方。输入和输出支持编解码器，使你能够在数据进入或离开管道时对其进行编码或解码，而无需使用单独的过滤器。 

## 输入

你使用输入将数据获取到Logstash中，一些比较常用的输入是：

- **file**：从文件系统上的文件进行读取，非常类似于UNIX命令`tail -0F`。
- **syslog**：在众所周知的端口514上监听syslog消息并根据RFC3164格式进行解析。
- **redis**：从redis服务器读取数据，同时使用Redis通道和Redis列表，Redis通常被用作集中式Logstash安装中的“broker”，它将从远程Logstash “shipper”中的Logstash事件排队。
- **beats**：处理[Beats](https://www.elastic.co/downloads/beats)发送的事件。

有关可用输入的更多信息，请参见[输入插件](https://www.elastic.co/guide/en/logstash/current/input-plugins.html)。

## 过滤器

过滤器是Logstash管道中的中间处理设备，如果事件符合一定的条件，你可以将过滤器与条件语句组合在一起，对其执行操作，一些有用的过滤器包括：

- **grok**：解析和构造任意文本，Grok是目前Logstash中解析非结构化日志数据到结构化和可查询数据的最佳方式，使用内置的120种模式，你很可能会找到一个满足你的需要！
- **mutate**：对事件字段执行一般的转换，你可以重命名、删除、替换和修改事件中的字段。
- **drop**：完全删除事件，例如debug事件。
- **clone**：复制事件，可能添加或删除字段。
- **geoip**：添加关于IP地址地理位置的信息（在Kibana中还显示了令人惊叹的图表！）

有关可用过滤器的更多信息，请参见[过滤器插件](https://www.elastic.co/guide/en/logstash/current/filter-plugins.html)。

## 输出

输出是Logstash管道的最后阶段，事件可以通过多个输出，但是一旦所有的输出处理完成，事件就完成了它的执行，一些常用的输出包括：

- **elasticsearch**：发送事件数据到Elasticsearch，如果你打算以一种高效、方便、易于查询的格式保存数据，那么使用Elasticsearch是可行的。
- **file**：将事件数据写入磁盘上的文件。
- **graphite**：将事件数据发送到graphite，这是一种流行的用于存储和绘制指标的开源工具。http://graphite.readthedocs.io/en/latest/
- **statsd**：发送事件到statsd，“监听统计信息（如计数器和计时器）、通过UDP发送聚合并将聚合发送到一个或多个可插拔后端服务”的服务，如果你已经在使用statsd，这可能对你很有用！

有关可用输出的更多信息，请参见[输出插件](https://www.elastic.co/guide/en/logstash/current/output-plugins.html)。

## 编解码器

Codecs是基本的流过滤器，可以作为输入或输出的一部分进行操作，Codecs使你能够轻松地将消息的传输与序列化过程分开，流行的codecs包括`json`、`msgpack`和`plain`（text）。

- **json**：以JSON格式对数据进行编码或解码。
- **multiline**：将多行文本事件（如java异常和stacktrace消息）合并到单个事件中。

有关可用编解码器的更多信息，请参见[编解码器插件](https://www.elastic.co/guide/en/logstash/current/codec-plugins.html)。

## 执行模型

Logstash事件处理管道协调输入、过滤器和输出的执行。

Logstash管道中的每个输入阶段都在自己的线程中运行，输入将事件写入位于内存（默认）或磁盘上的中央队列，每个管道工作线程从这个队列中取出一批事件，通过配置的过滤器运行事件批处理，然后通过任何输出运行过滤的事件，可以配置批处理的大小和管道工作线程的数量（参见[调优和分析*Logstash*性能](https://www.elastic.co/guide/en/logstash/current/tuning-logstash.html)）。

默认情况下，Logstash使用内存有限队列在管道阶段之间（输入→过滤器和过滤器→输出）来缓冲事件，如果Logstash不安全的终止，则存储在内存中的任何事件都将丢失。为了防止数据丢失，你可以启用Logstash将运行中的事件持久化到磁盘上，有关更多信息，请参见[持久队列](https://www.elastic.co/guide/en/logstash/current/persistent-queues.html)。

## [logstash的各个场景应用](https://www.cnblogs.com/qingqing74647464/p/9378385.html)

**场景：**

**1）** **datasource->logstash->elasticsearch->kibana**

**2）** **datasource->filebeat->logstash-> elasticsearch->kibana**

**3）** **datasource->filebeat->logstash->redis/kafka->logstash-> elasticsearch->kibana**

**4）** **kafka->logstash-> elasticsearch->kibana**

**5）** **datasource->filebeat->kafka->logstash->elasticsearch->kibana(最常用)**

**6）** **filebeatSSL加密传输**

**7）** **datasource->logstash->redis/kafka->logstash->elasticsearch->kibana**

**8）** **mysql->logstash->elasticsearch->kibana**



# 六、问题

## 1.logstash 和filebeat 是什么关系

因为logstash是jvm跑的，资源消耗比较大，所以后来作者又用golang写了一个功能较少但是资源消耗也小的轻量级的logstash-forwarder。不过作者只是一个人，加入http://elastic.co公司以后，因为es公司本身还收购了另一个开源项目packetbeat，而这个项目专门就是用golang的，有整个团队，所以es公司干脆把logstash-forwarder的开发工作也合并到同一个golang团队来搞，于是新的项目就叫filebeat了。

logstash 和filebeat都具有日志收集功能，filebeat更轻量，占用资源更少，但logstash 具有filter功能，能过滤分析日志。一般结构都是filebeat采集日志，然后发送到消息队列，redis，kafaka。然后logstash去获取，利用filter功能过滤分析，然后存储到elasticsearch中

一个对比内存消耗的案例：https://blog.csdn.net/u010871982/article/details/79035317
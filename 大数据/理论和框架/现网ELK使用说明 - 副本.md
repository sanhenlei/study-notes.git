一、现网ELK使用

## 1、现网说明

现网搭建的集群是比较全面的，也可以搭建简单的：

下图是简单的系统：只需要Logstash、elasticsearch、kibana

 ![img](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/现网ELK使用说明/70.png) 

下面是复杂的系统：

![1576808456105](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/现网ELK使用说明/1576808456105.png)



现网采用的是：日志由filebeat 收集送到Kafka再送到Logstash再输出到ES数据库然后kibana界面调用

![1576811047679](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/现网ELK使用说明/1576811047679.png)

## 2、服务器和软件规划

核心软件都安装在了fast三台服务器上

| 10.199.155.171            | 10.199.155.183            | 10.199.155.210            | 版本  |
| ------------------------- | ------------------------- | ------------------------- | ----- |
| zookeeper集群（2181）     | zookeeper集群（2181）     | zookeeper集群（2181）     | 3.4.8 |
| kafka集群（9092）         | kafka集群（9092）         | kafka集群（9092）         | 2.4.0 |
| elasticsearch集群（9200） | elasticsearch集群（9200） | elasticsearch集群（9200） | 1.7.3 |
| logstash 收集web日志      | logstash 收集mongo日志    | logstash 收集解码器日志   | 2.0.0 |
| kibana（5601）            |                           |                           | 4.1.2 |

## 3、使用流程

### 3.1 配置filebeat

**如果没有filebeat就先安装**

3.1.1、需要在要收集日志的服务器上安装filebeat日志收集器，218我已经安装了在/opt/do/目录下

2、软件下载链接：https://pan.baidu.com/s/1wfXJwgjlyRUlgiHqvko4Dg    提取码：2kln

3、安装步骤

```shell
cd /opt/do/
tar -zxf filebeat-5.5.2-linux-x86_64.tar.gz
mv filebeat-5.5.2-linux-x86_64 filebeat
```

4、修改配置文件

**查看进程：**

```shell
ps -ef | grep filebeat | grep -v grep | awk '{print $1" | "$2" | "$10}'
```

**参数说明：**

* 文件名最好是filebeat-自定义.yml
* 配置文件参数详解：https://blog.csdn.net/chengxuyuanyonghu/article/details/54378778
* paths: 要输入文件路径,支持Go Glab的所有模式
* file_name： 自定义文件名
* tags：自定义标签，主要在界面过滤和查询 方便
* topic：自定义kafka主题名

> 下面是现网配置过的应用

#### 收集217web日志

```shell
view /home/fubocai/filebeat

filebeat.prospectors:
- type: log
  enabled: true
  close_rename: true
  close_remove: true
  close_inactive: 1m
  paths: ["/usr/local/apache-tomcat-8.5.57/logs/catalina.out"]
  scan_frequency: 5s
  backoff: 1s
  max_backoff: 10s
  backoff_factor: 2
  harvester_limit: 10
  multiline.pattern: '^(\d{4}-\d{1,2}-\d{1,2})'
  multiline.negate: true
  multiline.match: after
  multiline.timeout: 5s
  tags: 'web-catalina-217'
  fields:
    filename: catalinalog217
  fields_under_root: true
  
output.kafka:
  hosts: ["10.199.155.171:9092"]
  topic: 'catalina_218_new'
  keep_alive: 10s
  
#启动
nohup /home/fubocai/filebeat/filebeat -c /home/fubocai/filebeat/filebeat-catalina.yml 2>&1 &

##查看进程
ps -ef | grep filebeat
```



#### 收集218web日志

```shell
cd /opt/do/filebeat
view filebeat-catalina.yml

filebeat.prospectors:
- type: log
  enabled: true
  close_rename: true
  close_remove: true
  close_inactive: 1m
  paths: ["/usr/local/apache-tomcat-8.5.31/logs/catalina.out"]
  scan_frequency: 5s
  backoff: 1s
  max_backoff: 10s
  backoff_factor: 2
  harvester_limit: 10
  multiline.pattern: '^\[\d{4}-\d{1,2}-\d{1,2}'
  multiline.negate: true
  multiline.match: after
  multiline.timeout: 5s
  tags: 'web-catalina-218'
  fields:
    filename: catalinalog218
  fields_under_root: true
  
output.kafka:
  hosts: ["10.199.155.171:9092"]
  topic: 'catalina_218_new'
  keep_alive: 10s
 

##下面是输出到kafka,,,,停用了
#output.kafka:
#  hosts: ["10.199.155.171:9092", "10.199.155.183:9092", "10.199.155.210:9092"]
#  topic: 'catalina_218'
#  #连接的存活时间.如果为0,表示短连,发送完就关闭.默认为0秒.
#  keep_alive: 10s

output.elasticsearch:
  hosts: ["10.199.155.171:9200","10.199.155.210:9200"]
  worker: 4
  index: "kafka_catalina_218_log-%{+yyyy.MM.dd}"
  
  
  
#####启动
chmod go-w filebeat-catalina.yml
mkdir logs
nohup /opt/do/filebeat/filebeat -c /opt/do/filebeat/filebeat-catalina.yml > logs/nohup.out 2>&1 &

##查看进程
ps -ef | grep filebeat
```

#### 收集DO-mt日志

```shell
cd /opt/do/filebeat
vi filebeat-deapOceanMTlog.yml

filebeat.prospectors:
- type: log
  enabled: true
  close_rename: true
  close_remove: true
  close_inactive: 1m
  paths: ["/data01/mtlog/Local_Importer_Decoder01.log"]
  scan_frequency: 5s
  backoff: 1s
  max_backoff: 10s
  backoff_factor: 2
  harvester_limit: 10
  multiline.pattern: '^\d{4}-\d{1,2}-\d{1,2}'
  multiline.negate: true
  multiline.match: after
  multiline.timeout: 5s
  tags: 'deapOceanMTlog-218'
  fields:
    filename: catalinalog218
  fields_under_root: true
  

##下面是输出到kafka
output.kafka:
  hosts: ["10.199.155.171:9092", "10.199.155.183:9092", "10.199.155.210:9092"]
  topic: 'deapOceanMTlog'
  #连接的存活时间.如果为0,表示短连,发送完就关闭.默认为0秒.
  keep_alive: 10s
  
#####启动
chmod go-w filebeat-deapOceanMTlog.yml
nohup /opt/do/filebeat/filebeat -c filebeat-deapOceanMTlog.yml > logs/nohup.out 2>&1 &

##查看进程
ps -ef | grep filebeat
```

#### 收集发送短信日志

```shell
cd /opt/do/filebeat
vi filebeat-sendshortmsglog.yml

filebeat.prospectors:
- type: log
  enabled: true
  close_rename: true
  close_remove: true
  close_inactive: 1m
  paths: ["/opt/do/send_short_message/2*.log"]
  scan_frequency: 5s
  backoff: 1s
  max_backoff: 10s
  backoff_factor: 2
  harvester_limit: 10
  tags: 'send_short_message-218'
  fields:
    filename: sendshortmsglog
  fields_under_root: true

output.kafka:
  hosts: ["10.199.155.171:9092", "10.199.155.183:9092", "10.199.155.210:9092"]
  topic: 'sendshortmsglog'
  keep_alive: 3s
  
#####启动
chmod go-w filebeat-sendshortmsglog.yml
nohup /opt/do/filebeat/filebeat -c filebeat-sendshortmsglog.yml > logs/nohup.out 2>&1 &

##查看进程
ps -ef | grep filebeat
```

#### 收集mongodb日志

```shell
cd /opt/filebeat
vi filebeat-mongodblog.yml

filebeat:
  prospectors:
    - input_type: log
      paths:
      ##要输入文件路径,支持Go Glab的所有模式
        - /data/mongodb/mongos/log/*.log
      input_type: log
      ##下面这些可以不用配置
      document_type: mongodb
      close_inactive: 1m
      scan_frequency: 5s
      fields:
      file_name: mongoslog
      fields_under_root: true
      close_removed: true
      tail_files: true
      tags: 'mongoslog-224'
  spool_size: 1024
  idle_timeout: 5s

########################下面选一种
##下面是输出到kafka
output.kafka:
  hosts: ["10.199.155.171:9092", "10.199.155.183:9092", "10.199.155.210:9092"]
  topic: 'mongodblog'
  #连接的存活时间.如果为0,表示短连,发送完就关闭.默认为0秒.
  keep_alive: 10s

#####启动
cd /opt/filebeat
chmod go-w filebeat-mongodblog.yml
nohup /opt/filebeat/filebeat -c filebeat-mongodblog.yml > logs/nohup.out 2>&1 &
```

#### 收集7个应用日志

```shell
cd /opt/do/
tar -zxf filebeat-5.5.2-linux-x86_64.tar.gz
mv filebeat-5.5.2-linux-x86_64 filebeat
cd /opt/do/filebeat

vim filebeat-doweb.yml

#如下配置
filebeat.prospectors:
- type: log
  enabled: true
  close_older: 30m
  force_close_files: true
  paths: ["/opt/do/doweb_volte/log/debug.log"]
  scan_frequency: 5s
  multiline.pattern: '^\d{4}-\d{1,2}-\d{1,2}'
  multiline.negate: true
  multiline.match: after
  multiline.timeout: 5s
  tags: 'doweb-debug-218'
  fields:
    filename: debuglog
  fields_under_root: true
- type: log
  enabled: true
  close_older: 30m
  force_close_files: true
  paths: ["/opt/do/doweb_volte/log/error.log"]
  scan_frequency: 5s
  multiline.pattern: '^\d{4}-\d{1,2}-\d{1,2}'
  multiline.negate: true
  multiline.match: after
  multiline.timeout: 5s
  tags: 'doweb-error-218'
  fields:
    filename: errorlog
  fields_under_root: true
- type: log
  enabled: true
  close_older: 30m
  force_close_files: true
  paths: ["/opt/do/doweb_volte/start.log"]
  scan_frequency: 5s
  multiline.pattern: '^\d{4}-\d{1,2}-\d{1,2}'
  multiline.negate: true
  multiline.match: after
  multiline.timeout: 5s
  tags: 'doweb-start-218'
  fields:
    filename: startlog
  fields_under_root: true

##下面是输出到kafka
output.kafka:
  hosts: ["10.199.155.171:9092", "10.199.155.183:9092", "10.199.155.210:9092"]
  topic: 'topic1'
  #连接的存活时间.如果为0,表示短连,发送完就关闭.默认为0秒.
  keep_alive: 10s
  
#####启动
cd /data01/filebeat
chmod go-w /data01/filebeat/filebeat-doweb.yml

/data01/filebeat/filebeat -c filebeat-doweb.yml > logs/nohup.out 2>&1 &

##查看进程
ps -ef | grep filebeat

```



### 3.2 配置Kafka

目前只用了171一台，启动和关闭命令

```shell
#先关kafka
/opt/kafka/bin/kafka-server-stop.sh
#再关zookeeper
/opt/zookeeper/bin/zkServer.sh stop


#先启zookeeper
/opt/zookeeper/bin/zkServer.sh start
#再启kafka
nohup /opt/kafka/bin/kafka-server-start.sh /opt/kafka/config/server.properties &

#查看进程
[fubocai@HIHK-NPO-FAST-DN2 opt]$ jps
34086 Elasticsearch
40602 Kafka
36220 QuorumPeerMain
764 Jps
42621 Main
```



如果上面output里的topic主题在217上kafka集群还没有，需要创建，登陆10.199.155.171服务器执行下面命令：

> 注意修改topic

```shell
kafka-topics.sh --zookeeper 127.0.0.1:2181 --create  --replication-factor 3 --partitions 4 --topic catalina_218
kafka-topics.sh --zookeeper 127.0.0.1:2181 --create  --replication-factor 3 --partitions 4 --topic deapOceanMTlog

kafka-topics.sh --zookeeper 127.0.0.1:2181 --create  --replication-factor 3 --partitions 3 --topic sendshortmsglog

kafka-topics.sh --zookeeper 127.0.0.1:2181 --create  --replication-factor 3 --partitions 4 --topic mongodblog

kafka-topics.sh --zookeeper 127.0.0.1:2181 --create  --replication-factor 3 --partitions 3 --topic topic1log

kafka-topics.sh --zookeeper 127.0.0.1:2181 --create  --replication-factor 3 --partitions 3 --topic topic2log

kafka-topics.sh --zookeeper 127.0.0.1:2181 --create  --replication-factor 3 --partitions 3 --topic topic3log
```

#### *已建的topic如下：

| topic           | 说明                      |
| --------------- | ------------------------- |
| catalina_218    | 收集218的web—catalina-log |
| deapOceanMTlog  | 收集218的mt接口日志       |
| sendshortmsglog | 收集发送短信日志          |
| mongodblog      | 收集mongo库日志           |
| topic1log       | 公共1                     |
| topic2log       | 公共2                     |
| topic3log       | 公共3                     |
| topic4log       | 公共4                     |

### 3.3 配置Logstash

#### 查看进程：

```shell
ps -ef | grep logstash | grep -v grep | awk '{print $1" | "$2" | "$34}'
```

#### *已建的index如下：

| index                                    | 说明                      | 位置 |
| ---------------------------------------- | ------------------------- | ---- |
| kafka_catalina_218_log-%{+YYYY.MM.dd}    | 收集218的web—catalina-log | 171  |
| kafka_deapOceanMT_log-%{+YYYY.MM.dd}     | 收集218的mt接口日志       | 171  |
| kafka_sendshortmsglog_log-%{+YYYY.MM.dd} | 收集发送短信日志          | 171  |
| kafka_mongodb_log-%{+YYYY.MM.dd}         | 收集mongo库日志           | 183  |
| kafka_topic4_log-%{+YYYY.MM.dd}          |                           | 183  |
| kafka_topic1_log-%{+YYYY.MM.dd}          |                           | 210  |
| kafka_topic2_log-%{+YYYY.MM.dd}          |                           | 210  |
| kafka_topic3_log-%{+YYYY.MM.dd}          |                           | 210  |



1、如是新增的kafka主题，还需要配置logstash，如果有了不需要配置；

2、Logstash不是集群，可按下面规划选择一台配置：

10.199.155.171收集web日志等小日志 ， 10.199.155.183收集mongo日志，  10.199.155.210收集解码器日志

3、参数说明：

* topic_id：输入kafka主题名，跟上面保持一致
* index：输出ES数据库索引，kibana界面会用到，建议命名格式k1_k2_k3_k4-%{+YYYY.MM.dd}"，k1:输入类型如kafka、file、jdbc等，k2:日志名，k3:日志所在服务器IP，k4:数据类型如log、table等

##### logstash-catalina

```shell
##在171
###注意收集不同的日志，用不同的文件名
vim /opt/logstash/config/logstash-catalina.conf

input {
        kafka {
           bootstrap_servers => ["10.199.155.171:9092"]
           #group_id => "logstash_data_access_e"
           auto_offset_reset => "earliest"
           consumer_threads => "1"
           decorate_events => "false"
           topics => ["catalina_218_new"]
           codec => json
          }
}
filter {
    if "Executing SQL query" in [message] {
        grok{
            patterns_dir => ["/opt/logstash/patterns"]
            match => {
                "message" => "%{ExecutingSQL}"
            }
        }
        date {
            match => ["log_time", "yyyy-MM-dd HH:mm:ss.SSS", "ISO8601"]
            target => "@timestamp"
        }
    }else if "[JDBC-BRIDGE]" in [message] {
                grok{
            patterns_dir => ["/opt/logstash/patterns"]
            match => {
                "message" => "%{JDBCBRIDGE}"
            }
        }
        date {
            match => ["log_time", "yyyy-MM-dd HH:mm:ss,SSS", "ISO8601"]
            target => "@timestamp"
        }
    }else{
        grok{
            patterns_dir => ["/opt/logstash/patterns"]
            match => {
                "message" => "%{others}"
            }
        }
        date {
            match => ["log_time", "yyyy-MM-dd HH:mm:ss.SSS", "ISO8601"]
            target => "@timestamp"
        }
    }
}
output {
  elasticsearch {
    hosts  => ["10.199.155.171:9200"]
    index  => "kafka_catalina_218_log-%{+YYYY.MM.dd}"
    user => "elastic"
    password => "Uar_2016"
    codec => "json"
  }
}

cd /opt/logstash/
./bin/logstash -f config/logstash-catalina.conf --path.data=data/catalina  2>&1 &

ps -ef | grep logstash | grep -v grep | awk '{print $1" | "$2" | "$34}'
```

##### logstash-sendshortmsg

```shell
##在171
###注意收集不同的日志，用不同的文件名
vim /opt/logstash/config/logstash-sendshortmsg.conf

input {
    kafka{
                bootstrap_servers => ["10.199.155.171:9092,10.199.155.183:9092,10.199.155.210:9092"]
                group_id => "logstash_data_access_event"
                auto_offset_reset => "earliest"
                consumer_threads => "1"
                decorate_events => "false"
                topics => ["sendshortmsglog"]
                codec => json
    }
}
output {

  elasticsearch {
    hosts  => ["10.199.155.171:9200","10.199.155.183:9200","10.199.155.210:9200"]
    index  => "kafka_sendshortmsg_log-%{+YYYY.MM.dd}"
    codec => "json"
  }
}

cd /opt/logstash/
nohup ./bin/logstash -f config/logstash-sendshortmsg.conf --path.data=data/sendshortmsg  2>&1 &

ps -ef | grep logstash | grep -v grep | awk '{print $1" | "$2" | "$34}'
```

##### logstash-mongodb 

```shell
###在183
###注意收集不同的日志，用不同的文件名
vim /opt/logstash/config/logstash-mongodb.conf

input {
    kafka{
                bootstrap_servers => ["10.199.155.171:9092,10.199.155.183:9092,10.199.155.210:9092"]
                group_id => "logstash_data_access_event"
                auto_offset_reset => "earliest"
                consumer_threads => "3"
                decorate_events => "false"
                topics => ["mongodblog"]
                codec => json
    }
}
filter{
        grok {
            match => ["message","%{TIMESTAMP_ISO8601:timestamp}\s+%{MONGO3_SEVERITY:severity}\s+%{MONGO3_COMPONENT:component}\s+(?:\[%{DATA:context}\])?\s+%{GREEDYDATA:body}"]
        }
        if [component] =~ "COMMAND" {
            grok {
                match => ["body","%{WORD:command_type}\s+%{DATA:db_name}\s+\w+\:\s+%{GREEDYDATA:command}\s+%{INT:spend_time}ms$"]
            }
        }
        date {
            match => [ "timestamp","UNIX", "YYYY-MM-dd HH:mm:ss", "ISO8601"]
            remove_field => [ "timestamp" ]
        }
}
output {

  elasticsearch {
    hosts  => ["10.199.155.171:9200","10.199.155.183:9200","10.199.155.210:9200"]
    index  => "kafka_mongodb_log-%{+YYYY.MM.dd}"
    codec => "json"
  }
}

cd /opt/logstash/
nohup ./bin/logstash -f config/logstash-mongodb.conf --path.data=data/mongodb 2>&1 &

ps -ef | grep logstash | grep -v grep | awk '{print $1" | "$2" | "$34}'
```

##### logstash-deapOceanMT

```shell
##在171
###注意收集不同的日志，用不同的文件名
vim /opt/logstash/config/logstash-deapOceanMT.conf

input {
    kafka{
                bootstrap_servers => ["10.199.155.171:9092,10.199.155.183:9092,10.199.155.210:9092"]
                group_id => "logstash_data_access_event"
                auto_offset_reset => "earliest"
                consumer_threads => "3"
                decorate_events => "false"
                topics => ["deapOceanMTlog"]
                codec => json
    }
}
output {

  elasticsearch {
    hosts  => ["10.199.155.171:9200","10.199.155.183:9200","10.199.155.210:9200"]
    index  => "kafka_deapOceanMT_log-%{+YYYY.MM.dd}"
    codec => "json"
  }
}


cd /opt/logstash/
nohup ./bin/logstash -f config/logstash-deapOceanMT.conf --path.data=data/deapOceanMT 2>&1 &

ps -ef | grep logstash | grep -v grep | awk '{print $1" | "$2" | "$34}'
```

##### logstash-topic1

```shell
##在210
###注意收集不同的日志，用不同的文件名
vim /opt/logstash/config/logstash-topic1.conf

input {
    kafka{
                bootstrap_servers => ["10.199.155.171:9092,10.199.155.183:9092,10.199.155.210:9092"]
                group_id => "logstash_data_access_event"
                auto_offset_reset => "earliest"
                consumer_threads => "3"
                decorate_events => "false"
                topics => ["topic1"]
                codec => json
    }
}
output {

  elasticsearch {
    hosts  => ["10.199.155.171:9200","10.199.155.183:9200","10.199.155.210:9200"]
    index  => "kafka_topic1_log-%{+YYYY.MM.dd}"
    codec => "json"
  }
}

cd /opt/logstash/
nohup ./bin/logstash -f config/logstash-topic1.conf --path.data=data/topic1 2>&1 &

ps -ef | grep logstash | grep -v grep | awk '{print $1" | "$2" | "$34}'
```



##### logstash-topic2

```shell
##在210
###注意收集不同的日志，用不同的文件名
vim /opt/logstash/config/logstash-topic2.conf

input {
    kafka{
                bootstrap_servers => ["10.199.155.171:9092,10.199.155.183:9092,10.199.155.210:9092"]
                group_id => "logstash_data_access_event"
                auto_offset_reset => "earliest"
                consumer_threads => "3"
                decorate_events => "false"
                topics => ["topic2"]
                codec => json
    }
}
output {

  elasticsearch {
    hosts  => ["10.199.155.171:9200","10.199.155.183:9200","10.199.155.210:9200"]
    index  => "kafka_topic2_log-%{+YYYY.MM.dd}"
    codec => "json"
  }
}

cd /opt/logstash/
nohup ./bin/logstash -f config/logstash-topic2.conf --path.data=data/topic2  2>&1 &
```



##### logstash-topic3

```shell
##在210
###注意收集不同的日志，用不同的文件名
vim /opt/logstash/config/logstash-topic3.conf

input {
    kafka{
                bootstrap_servers => ["10.199.155.171:9092,10.199.155.183:9092,10.199.155.210:9092"]
                group_id => "logstash_data_access_event"
                auto_offset_reset => "earliest"
                consumer_threads => "3"
                decorate_events => "false"
                topics => ["topic3"]
                codec => json
    }
}
output {

  elasticsearch {
    hosts  => ["10.199.155.171:9200","10.199.155.183:9200","10.199.155.210:9200"]
    index  => "kafka_topic3_log-%{+YYYY.MM.dd}"
    codec => "json"
  }
}

cd /opt/logstash/
nohup ./bin/logstash -f config/logstash-topic3.conf --path.data=data/topic3 2>&1 &
```

##### logstash-topic4

```shell
##在171
###注意收集不同的日志，用不同的文件名
vim /opt/logstash/config/logstash-topic4.conf

input {
    kafka{
                bootstrap_servers => ["10.199.155.171:9092,10.199.155.183:9092,10.199.155.210:9092"]
                group_id => "logstash_data_access_event"
                auto_offset_reset => "earliest"
                consumer_threads => "3"
                decorate_events => "false"
                topics => ["topic4"]
                codec => json
    }
}
output {

  elasticsearch {
    hosts  => ["10.199.155.171:9200","10.199.155.183:9200","10.199.155.210:9200"]
    index  => "kafka_topic4_log-%{+YYYY.MM.dd}"
    codec => "json"
  }
}

cd /opt/logstash/
nohup ./bin/logstash -f config/logstash-topic4.conf --path.data=data/topic4  2>&1 &
```



#### filter grok使用方法：

```shell
#创建自定义过滤方法文件
cd /opt/logstash
mkdir patterns
vim patterns/webapplog.conf

#把下面内容放进去
ALLSTR ([\s\S]*)
ExecutingSQL \[%{TIMESTAMP_ISO8601:log_time} %{LOGLEVEL:log_level}( |)\] \- \[%{JAVACLASS:classname}:%{NUMBER:code_rownum}\] :Executing SQL query (\[ %{WORD:sql_id}\ ],|)
others \[%{TIMESTAMP_ISO8601:log_time} %{LOGLEVEL:log_level}( |)\] \- \[%{JAVACLASS:classname}:%{NUMBER:code_rownum}\]
JDBCBRIDGE %{TIMESTAMP_ISO8601:log_time} ([\s\S]*) %{LOGLEVEL:log_level}

#添加filter
vim /opt/logstash/etc/logstash-kafka.conf

filter{
    if "Executing SQL query" in [message] {
        grok{
            patterns_dir => ["/opt/logstash/patterns"]
            match => { 
                "message" => "%{ExecutingSQL}"
            }
        }
    }else{
        grok{
            patterns_dir => ["/opt/logstash/patterns"]
            match => { 
                "message" => "%{others}" 
            }
        }
    }
}

```



### 3.4 配置Kibana

1、在浏览器打开10.199.155.171:5601， elastic Uar_2016

2、在Settings里新增index

![1577069842000](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/现网ELK使用说明/1577069842000.png)

3、写入index名，跟上面logstash里保持一致

![1577069949207](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/现网ELK使用说明/1577069949207.png)

4、在主界面Discover里选在添加的index

![1577070132953](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/现网ELK使用说明/1577070132953.png)

5、主界面介绍

![](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/现网ELK使用说明/Image 444.png)

### 3.5 Kibana使用说明

kibana面板讲解和功能使用说明 https://blog.csdn.net/fxbin123/article/details/79983264

hixngl Xngl@2020

### 3.6 elasticsearch登陆配置

view /opt/elasticsearch/conf/elasticsearch.yml

```yaml
path.data: /data/elasticsearch/data #数据
path.logs: /data/elasticsearch/logs #日志
cluster.name: fast_es # 集群的名称
node.name: fast_node_1 # 该节点名称，与前面配置hosts保持一致
node.master: true # 意思是该节点是否可选举为主节点
node.data: true # 表示这不是数据节点
network.host: 0.0.0.0 # 监听全部ip，在实际环境中应为一个安全的ip
http.port: 9200 # es服务的端口号
http.cors.enabled: true
http.cors.allow-origin: "*"
http.cors.allow-headers: Authorization
xpack.security.enabled: true
xpack.security.transport.ssl.enabled: true
```

设置的用户名：elastic 密码：Uar_2016

## 4、验证

### 4.1 catalina.out日志情况

呈现延迟在1s内，数据量对的上

![1577408657859](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/现网ELK使用说明/1577408657859.png)

![1577408712222](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/理论和框架/image/现网ELK使用说明/1577408712222.png)
# 概述

官网：http://hadoop.apache.org/

![image-20210301164222786](image/Hadoop/image-20210301164222786.png)

- Hadoop是一个由Apache基金会开发的分布式系统基础架构。用以解决海量数据的存储和分析计算的问题。
- Hadoop三大发行版本：
  - Apache
  - Cloudera
  - Hortonworks
- 优点：
  - 高可靠性
  - 高扩展性
  - 高容错性
  - 高效性
- 组成：
  - MapReduce（计算）：
    - Map阶段并行处理输入数据；
    - Reduce阶段对Map结果进行汇总。
  - YARN（资源调度）：
    - ResourceManager（RM）：
      - 处理客户端请求；
      - 监控NodeManager；
      - 启动或监控ApplicationMaster；
      - 资源的分配与调度。
    - NodeManager（NM）：
      - 管理单个节点上的资源；
      - 处理来自ResourceManager的命令；
      - 处理来自ApplicationMaster的命令。
    - ApplicationMaster（AM）：
      - 负责数据的切分；
      - 为应用程序申请资源并分配给内部的任务；
      - 任务的监控与容错。
    - Container：
      - Container是YARN中的资源抽象，它封装了某个节点上的多维度资源：内存、CPU、磁盘、网络等。
  - HDFS（数据存储）：
    - NameNode（nn）：
      - 管理HDFS的名称空间；
      - 配置副本策略；
      - 管理数据块（Block）映射信息；
      - 处理客户端请求。
    - DataNode：
      - 存储实际的数据块；
      - 执行数据块的读写操作。
    - Client：
      - 文件切分。文件上传HDFS的时候，Client将文件切分成一个一个的Block，然后进行上传；
      - 与NameNode交互，获取文件的位置信息；
      - 与DataNode交互，读取或者写入数据；
      - Client可以管理和操作HDFS，格式化和增删改查操作。
    - Secondary NameNode：
      - 辅助NameNode，分担其工作量，定期合并Fsimage和Edits，并推送给NameNode；
      - 在紧急情况下，可辅助恢复NameNode。
  - Common（辅助工具）：

# 运行模式

1. 本地模式

2. 伪分布式模式

3. 完全分布式模式
   - 搭建Hadoop运行环境
   - 编写集群分发脚本
   - 配置ssh
   - 配置集群：
     - core-site.xml
     - hdfs-site.xml
     - yarn-site.xml
     - mapred-site.xml
   - 单点启动
   - 集群启动
     - 配置workers
4.  集群启动、停止命令总结
   - 各个服务组件逐一启动/停止：
     - hafs --daemon start/stop namenode/datanode/secondarynamenode
     - yarn --daemon start/stop resourcemanager/nodemanager
   - 各个模块分开启动/停止（配置ssh）：
     - start-dfs.sh/stop-dfs.sh
     - start-yarn.shh/stop-yarn.sh
5. 配置历史服务器
6. 配置日志聚集
7. 集群时间同步

​	

# 运行环境搭建

需要三台虚拟机

- 1、修改IP：
  \+ 虚拟机；
  \+ VMware网络编辑器；
  \+ VMware虚拟网卡。
- 2、修改主机名；
  \+ 修改虚拟机主机名；
  \+ Linux环境配置IP地址映射
  \+ Windows环境配置IP地址映射
- 3、关闭防火墙；
- 4、创建atguigu用户；
- 5、重启虚拟机；
- 6、配置atguigu用户具有root权限；
- 7、在opt/目录下创建module、software文件夹；
- 8、将安装包放在software下，安装JDK、Hadoop到module下。
- 9、配置环境变量，并检查环境是够搭建成功。



# 安装部署

官网下载：https://hadoop.apache.org/releases.html

![image-20210303151240249](image/Hadoop/image-20210303151240249.png)

## 本地模式

Hadoop本地模式只是用于本地开发调试，或者快速安装体验Hadoop

## 伪分布式模式

学习Hadoop一般是在伪分布式模式下进行。这种模式是在一台机器上各个进程上运行Hadoop的各个模块，伪分布式的意思是虽然各个模块是在各个进程上分开运行的，但是只是运行在一个操作系统上的，并不是真正的分布式。

## 完全分布式模式

参考：https://www.cnblogs.com/coolwxb/p/10975352.html

详细参考：https://blog.csdn.net/GitChat/article/details/77849331?utm_medium=distribute.pc_relevant.none-task-blog-OPENSEARCH-1.control&depth_1-utm_source=distribute.pc_relevant.none-task-blog-OPENSEARCH-1.control

生产环境采用的模式，Hadoop运行在服务器集群上，生产环境一般都会做HA，以实现高可用。

- 搭建Hadoop运行环境
- 编写集群分发脚本
- 配置ssh
- 配置集群：
  - core-stite.xml
  - hdfs-site.xml
  - yarn-site.xml
  - mapred-site.xml
- 单点启动
- 群起集群
  - 配置workers

### 集群规划

操作系统： CentOS 7.2 64位

网路设置:

| hostname       | IP         |
| -------------- | ---------- |
| cluster-master | 172.21.0.2 |
| cluster-slave1 | 172.21.0.3 |
| cluster-slave2 | 172.21.0.4 |
| cluster-slave3 | 172.21.0.5 |

### 免密登录

**`cluster-master`安装：**

```shell
#cluster-master需要修改配置文件（特殊）
#cluster-master

#安装openssh
[root@cluster-master /]# yum -y install openssh openssh-server openssh-clients

[root@cluster-master /]# systemctl start sshd
####ssh自动接受新的公钥
####master设置ssh登录自动添加kown_hosts
[root@cluster-master /]# vi /etc/ssh/ssh_config
#将原来的StrictHostKeyChecking ask
#设置StrictHostKeyChecking为no
#保存
[root@cluster-master /]# systemctl restart sshd
```

**分别对slaves安装OpenSSH**

```shell
#安装openssh
[root@cluster-slave1 /]#yum -y install openssh openssh-server openssh-clients

[root@cluster-slave1 /]# systemctl start sshd
```

**cluster-master公钥分发**

在master机上执行
ssh-keygen -t rsa
并一路回车，完成之后会生成~/.ssh目录，目录下有id_rsa（私钥文件）和id_rsa.pub（公钥文件），再将id_rsa.pub重定向到文件authorized_keys

```
ssh-keygen -t rsa
#一路回车

[root@cluster-master /]# cat ~/.ssh/id_rsa.pub > ~/.ssh/authorized_keys
```

文件生成之后用scp将公钥文件分发到集群slave主机

```
[root@cluster-master /]# ssh root@cluster-slave1 'mkdir ~/.ssh'
[root@cluster-master /]# scp ~/.ssh/authorized_keys root@cluster-slave1:~/.ssh
[root@cluster-master /]# ssh root@cluster-slave2 'mkdir ~/.ssh'
[root@cluster-master /]# scp ~/.ssh/authorized_keys root@cluster-slave2:~/.ssh
[root@cluster-master /]# ssh root@cluster-slave3 'mkdir ~/.ssh'
[root@cluster-master /]# scp ~/.ssh/authorized_keys root@cluster-slave3:~/.ssh
```

分发完成之后测试(ssh root@cluster-slave1)是否已经可以免输入密码登录



### Ansible安装

自动化管理的工具，用来传文件比较方便，可有可无

#### 

```
[root@cluster-master /]# yum -y install epel-release
[root@cluster-master /]# yum -y install ansible
#这样的话ansible会被安装到/etc/ansible目录下
```

此时我们再去编辑ansible的hosts文件

```
vi /etc/ansible/hosts
[cluster]
cluster-master
cluster-slave1
cluster-slave2
cluster-slave3

[master]
cluster-master

[slaves]
cluster-slave1
cluster-slave2
cluster-slave3
```

### 配置hosts

```sh
#添加下面内容
vim  /etc/hosts

127.0.0.1   localhost
172.31.0.2  cluster-master
172.31.0.3  cluster-slave1
172.31.0.4  cluster-slave2
172.31.0.5  cluster-slave3

#执行
source /etc/hosts
```

用ansible分发hosts至集群slave下

```
ansible cluster -m copy -a "src=/etc/hosts dest=/etc/"
```

### 软件安装

```shell
#上传jdk1.8和hadoop安装包，并解压，创建软连接

cd /opt
tar -xf jdk-8u201-linux-x64.tar.gz
tar -xf hadoop-3.2.1.tar.gz
ln -s hadoop-3.2.1 hadoop

#配置环境变量
vim /etc/profile

# hadoop
export HADOOP_HOME=/opt/hadoop
export PATH=$HADOOP_HOME/bin:$HADOOP_HOME/sbin:$PATH

#java
export JAVA_HOME=/opt/jdk1.8.0_201
export PATH=$JAVA_HOME/bin:$PATH


source /etc/profile
```

### 配置hadoop

cd $HADOOP_HOME/etc/hadoop/

修改vim core-site.xml

```xml
<configuration>
    <property>
        <name>hadoop.tmp.dir</name>
        <value>/home/hadoop/tmp</value>
        <description>A base for other temporary directories.</description>
    </property>
    <!-- file system properties -->
    <property>
        <name>fs.default.name</name>
        <value>hdfs://cluster-master:9000</value>
    </property>
    <property>
    <name>fs.trash.interval</name>
        <value>4320</value>
    </property>
</configuration>
```

修改vim hdfs-site.xml

```xml
<configuration>
<property>
   <name>dfs.namenode.name.dir</name>
   <value>/home/hadoop/tmp/dfs/name</value>
 </property>
 <property>
   <name>dfs.datanode.data.dir</name>
   <value>/home/hadoop/data</value>
 </property>
 <property>
   <name>dfs.replication</name>
   <value>3</value>
 </property>
 <property>
   <name>dfs.webhdfs.enabled</name>
   <value>true</value>
 </property>
 <property>
   <name>dfs.permissions.superusergroup</name>
   <value>staff</value>
 </property>
 <property>
   <name>dfs.permissions.enabled</name>
   <value>false</value>
 </property>
 </configuration>
```

修改vim mapred-site.xml

```xml
<configuration>
<property>
  <name>mapreduce.framework.name</name>
  <value>yarn</value>
</property>
<property>
    <name>mapred.job.tracker</name>
    <value>cluster-master:9001</value>
</property>
<property>
  <name>mapreduce.jobtracker.http.address</name>
  <value>cluster-master:50030</value>
</property>
<property>
  <name>mapreduce.jobhisotry.address</name>
  <value>cluster-master:10020</value>
</property>
<property>
  <name>mapreduce.jobhistory.webapp.address</name>
  <value>cluster-master:19888</value>
</property>
<property>
  <name>mapreduce.jobhistory.done-dir</name>
  <value>/jobhistory/done</value>
</property>
<property>
  <name>mapreduce.intermediate-done-dir</name>
  <value>/jobhisotry/done_intermediate</value>
</property>
<property>
  <name>mapreduce.job.ubertask.enable</name>
  <value>true</value>
</property>
</configuration>
```

修改vim yarn-site.xml

```xml
<configuration>
    <property>
   <name>yarn.resourcemanager.hostname</name>
   <value>cluster-master</value>
 </property>
 <property>
   <name>yarn.nodemanager.aux-services</name>
   <value>mapreduce_shuffle</value>
 </property>
 <property>
   <name>yarn.nodemanager.aux-services.mapreduce.shuffle.class</name>
   <value>org.apache.hadoop.mapred.ShuffleHandler</value>
 </property>
 <property>
   <name>yarn.resourcemanager.address</name>
   <value>cluster-master:18040</value>
 </property>
<property>
   <name>yarn.resourcemanager.scheduler.address</name>
   <value>cluster-master:18030</value>
 </property>
 <property>
   <name>yarn.resourcemanager.resource-tracker.address</name>
   <value>cluster-master:18025</value>
 </property> <property>
   <name>yarn.resourcemanager.admin.address</name>
   <value>cluster-master:18141</value>
 </property>
<property>
   <name>yarn.resourcemanager.webapp.address</name>
   <value>cluster-master:18088</value>
 </property>
<property>
   <name>yarn.log-aggregation-enable</name>
   <value>true</value>
 </property>
<property>
   <name>yarn.log-aggregation.retain-seconds</name>
   <value>86400</value>
 </property>
<property>
   <name>yarn.log-aggregation.retain-check-interval-seconds</name>
   <value>86400</value>
 </property>
<property>
   <name>yarn.nodemanager.remote-app-log-dir</name>
   <value>/tmp/logs</value>
 </property>
<property>
   <name>yarn.nodemanager.remote-app-log-dir-suffix</name>
   <value>logs</value>
 </property>
</configuration>
```

在Hadoop安装目录下找到sbin文件夹

cd $HADOOP_HOME/sbin

在里面修改四个文件

1、对于start-dfs.sh和stop-dfs.sh文件，添加下列参数：

```sh
#!/usr/bin/env bash

HDFS_DATANODE_USER=root
HADOOP_SECURE_DN_USER=hdfs
HDFS_NAMENODE_USER=root
HDFS_SECONDARYNAMENODE_USER=root
```


2、对于start-yarn.sh和stop-yarn.sh文件，添加下列参数：

```sh
#!/usr/bin/env bash
YARN_RESOURCEMANAGER_USER=root
HADOOP_SECURE_DN_USER=yarn
YARN_NODEMANAGER_USER=root
```



使用ansible-playbook分发配置文件至slave主机

```yaml
vim hadoop-dis.yaml

---
- hosts: cluster
  tasks:
    - name: copy core-site to slaves
      copy: src=/opt/hadoop/etc/hadoop/core-site.xml dest=/opt/hadoop/etc/hadoop
    - name: copy hdfs-site to slaves
      copy: src=/opt/hadoop/etc/hadoop/hdfs-site.xml dest=/opt/hadoop/etc/hadoop
    - name: copy mapred-site to slaves
      copy: src=/opt/hadoop/etc/hadoop/mapred-site.xml dest=/opt/hadoop/etc/hadoop
    - name: copy yarn-site to slaves
      copy: src=/opt/hadoop/etc/hadoop/yarn-site.xml dest=/opt/hadoop/etc/hadoop
    - name: copy start-dfs to slaves
      copy: src=/opt/hadoop/sbin/start-dfs.sh dest=/opt/hadoop/sbin
    - name: copy start-yarn to slaves
      copy: src=/opt/hadoop/sbin/start-yarn.sh dest=/opt/hadoop/sbin
    - name: copy stop-dfs to slaves
      copy: src=/opt/hadoop/sbin/stop-dfs.sh dest=/opt/hadoop/sbin
    - name: copy stop-yarn to slaves
      copy: src=/opt/hadoop/sbin/stop-yarn.sh dest=/opt/hadoop/sbin

  handlers:
    - name: exec source
      shell: source ~/.bashrc
```

执行

```shell
ansible-playbook /opt/hadoop/etc/hadoop/hadoop-dis.yaml
```

### 启动hadoop

每个节点都格式化namenode

```shell
mkdir /home/hadoop/data
hadoop namenode -format
```

如果看到storage format success等字样，即可格式化成功

启动每个节点

```shell
cd $HADOOP_HOME/sbin
start-all.sh
```

在主节点使用脚本启动所有节点

vim start-all-cluster.sh

```sh
#!/bin/bash
echo "===================正在启动所有集群节点，请稍等。。。======================="

echo "===================启动cluster-master节点==================================="
/opt/hadoop/sbin/start-all.sh
echo "===================启动cluster-slave1节点==================================="
ssh cluster-slave1 '/opt/hadoop/sbin/start-all.sh'
echo "===================启动cluster-slave2节点==================================="
ssh cluster-slave2 '/opt/hadoop/sbin/start-all.sh'
echo "===================启动cluster-slave3节点==================================="
ssh cluster-slave3 '/opt/hadoop/sbin/start-all.sh'
echo "===================启动完成================================================="
```

vim stop-all-cluster.sh

```sh
#!/bin/bash
echo "===================正在关闭所有集群节点，请稍等。。。======================="

echo "===================关闭cluster-master节点==================================="
/opt/hadoop/sbin/stop-all.sh
echo "===================关闭cluster-slave1节点=================================="
ssh cluster-slave1 '/opt/hadoop/sbin/stop-all.sh'
echo "===================关闭cluster-slave2节点==================================="
ssh cluster-slave2 '/opt/hadoop/sbin/stop-all.sh'
echo "===================关闭cluster-slave3节点==================================="
ssh cluster-slave3 '/opt/hadoop/sbin/stop-all.sh'
echo "===================关闭完成================================================="
```

vim status-all-cluster.sh

```sh
#!/bin/bash
echo "=========================cluster-master==================="
jps

for i in cluster-slave1 cluster-slave2 cluster-slave3
  do
    echo "========================="$i"==================="
    ssh $i jps
  done
```



启动后可使用jps命令查看是否启动成功，

![image-20210121152929513](image/Hadoop/image-20210121152929513.png)

![image-20210121154656377](image/Hadoop/image-20210121154656377.png)



### 验证服务



ResourceManager web管理 宿主机访问：http://192.168.17.130:18088/         直接访问：http://172.31.0.2:18088 

NameNode  web管理           宿主机访问：http://192.168.17.130:9870             直接访问：http://172.31.0.2:9870

hdfs访问                                        hdfs://172.31.0.2:9000/



## Docker快速部署

#### 部署

docker-compose.yml

```yaml
version: "2.2"

services:
  namenode:
    image: bde2020/hadoop-namenode:1.1.0-hadoop2.7.1-java8
    # 配置好 docker 内的假域名
    hostname: namenode
    container_name: namenode
    restart: always
    volumes:
      - /volume/hadoop/namenode:/hadoop/dfs/name
      - /volume/hadoop/input_files:/hadoop/input_files
    environment:
      - CLUSTER_NAME=test
      # 配置 hdfs 用户权限问题，不需要只允许 hadoop 用户访问
      - HDFS_CONF_dfs_permissions=false
    env_file:
      - ./hadoop.env
    ports:
      - 9000:9000
      - 50070:50070
    #network_mode: 'host'
  
  resourcemanager:
    image: bde2020/hadoop-resourcemanager:1.1.0-hadoop2.7.1-java8
    hostname: resourcemanager
    container_name: resourcemanager
    restart: always
    depends_on:
      - namenode
      - datanode1
      - datanode2
    env_file:
      - ./hadoop.env
    ports:
      - 8030:8030
      - 8031:8031
      - 8032:8032
      - 8033:8033
      - 8088:8088
    #network_mode: 'host'

  historyserver:
    image: bde2020/hadoop-historyserver:1.1.0-hadoop2.7.1-java8
    hostname: historyserver
    container_name: historyserver
    restart: always
    depends_on:
      - namenode
      - datanode1
      - datanode2
    volumes:
      - /volume/hadoop/historyserver:/hadoop/yarn/timeline
    env_file:
      - ./hadoop.env
    ports:
      - "8188:8188"
    #network_mode: 'host'

  nodemanager1:
    image: bde2020/hadoop-nodemanager:1.1.0-hadoop2.7.1-java8
    hostname: nodemanager1
    container_name: nodemanager1
    restart: always
    depends_on:
      - namenode
      - datanode1
      - datanode2
    env_file:
      - ./hadoop.env
    ports:
      - 8040:8040
      - 8041:8041
      - 8042:8042
    #network_mode: 'host'
  
  datanode1:
    image: bde2020/hadoop-datanode:1.1.0-hadoop2.7.1-java8
    hostname: datanode1
    container_name: datanode1
    restart: always
    depends_on:
      - namenode
    volumes:
      - /volume/hadoop/datanode1:/hadoop/dfs/data
    env_file:
      - ./hadoop.env
    environment:
      # 等价于在 hdfs-site.xml 中配置 dfs.datanode.address
      - HDFS_CONF_dfs_datanode_address=0.0.0.0:50011
      # dfs.datanode.ipc.address 不使用默认端口的意义是在同一机器起多个 datanode，暴露端口需要不同
      - HDFS_CONF_dfs_datanode_ipc_address=0.0.0.0:50021
      # dfs.datanode.http.address
      - HDFS_CONF_dfs_datanode_http_address=0.0.0.0:50071
    ports:
      - 50011:50011
      - 50021:50021
      - 50071:50071
    #network_mode: 'host'
  
  datanode2:
    image: bde2020/hadoop-datanode:1.1.0-hadoop2.7.1-java8
    hostname: datanode2
    container_name: datanode2
    restart: always
    depends_on:
      - namenode
    volumes:
      - /volume/hadoop/datanode2:/hadoop/dfs/data
    env_file:
      - ./hadoop.env
    environment:
      - HDFS_CONF_dfs_datanode_address=0.0.0.0:50012
      - HDFS_CONF_dfs_datanode_ipc_address=0.0.0.0:50022
      - HDFS_CONF_dfs_datanode_http_address=0.0.0.0:50072
    ports:
      - 50012:50012
      - 50022:50022
      - 50072:50072
    #network_mode: 'host'
  
  datanode3:
    image: bde2020/hadoop-datanode:1.1.0-hadoop2.7.1-java8
    hostname: datanode3
    container_name: datanode3
    restart: always
    depends_on:
      - namenode
    volumes:
      - /volume/hadoop/datanode3:/hadoop/dfs/data
    env_file:
      - ./hadoop.env
    environment:
      - HDFS_CONF_dfs_datanode_address=0.0.0.0:50013
      - HDFS_CONF_dfs_datanode_ipc_address=0.0.0.0:50023
      - HDFS_CONF_dfs_datanode_http_address=0.0.0.0:50073
    ports:
      - 50013:50013
      - 50023:50023
      - 50073:50073
    #network_mode: 'host'

```

在当前目录新建文件：hadoop.env

```cpp
CORE_CONF_fs_defaultFS=hdfs://namenode:9000
CORE_CONF_hadoop_http_staticuser_user=root
CORE_CONF_hadoop_proxyuser_hue_hosts=*
CORE_CONF_hadoop_proxyuser_hue_groups=*

HDFS_CONF_dfs_webhdfs_enabled=true
HDFS_CONF_dfs_permissions_enabled=false

YARN_CONF_yarn_log___aggregation___enable=true
YARN_CONF_yarn_resourcemanager_recovery_enabled=true
YARN_CONF_yarn_resourcemanager_store_class=org.apache.hadoop.yarn.server.resourcemanager.recovery.FileSystemRMStateStore
YARN_CONF_yarn_resourcemanager_fs_state___store_uri=/rmstate
YARN_CONF_yarn_nodemanager_remote___app___log___dir=/app-logs
YARN_CONF_yarn_log_server_url=http://historyserver:8188/applicationhistory/logs/
YARN_CONF_yarn_timeline___service_enabled=true
YARN_CONF_yarn_timeline___service_generic___application___history_enabled=true
YARN_CONF_yarn_resourcemanager_system___metrics___publisher_enabled=true
YARN_CONF_yarn_resourcemanager_hostname=resourcemanager
YARN_CONF_yarn_timeline___service_hostname=historyserver
YARN_CONF_yarn_resourcemanager_address=resourcemanager:8032
YARN_CONF_yarn_resourcemanager_scheduler_address=resourcemanager:8030
YARN_CONF_yarn_resourcemanager_resource___tracker_address=resourcemanager:8031
```

构建启动容器

docker-compose up -d

#### 集群说明

查看集群IP：docker inspect  `docker ps | grep hadoop | awk '{print $1}'` | grep IPAdd | grep 172

| 命令                                             | hostsname       | ip         | port                                                         | 作用                                                         |
| ------------------------------------------------ | --------------- | ---------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| docker inspect nodemanager1 \| grep IPAddress    | nodemanager1    | 172.21.0.6 | 8040:8040<br/>8041:8041<br/>8042:8042                        | 管理一个YARN集群中的每一个节点，比如监视资源使用情况         |
| docker inspect resourcemanager \| grep IPAddress | resourcemanager | 172.21.0.8 | 8030:8030<br/>8031:8031<br/>8032:8032<br/>8033:8033<br/>8088:8088 | 是Yarn集群主控节点，负责协调和管理整个集群（所有NodeManager）的资源 |
| docker inspect historyserver \| grep IPAddress   | historyserver   | 172.21.0.7 | 8188:8188                                                    | 可通过网页或RPC方式获取作业的信息                            |
| docker inspect datanode1 \| grep IPAddress       | datanode1       | 172.21.0.4 | 50011:50011<br/>50021:50021<br/>50071:50071                  | 储了实际的数据                                               |
| docker inspect datanode2  \| grep IPAddress      | datanode2       | 172.21.0.5 | 50012:50012<br/>50022:50022<br/>50072:50072                  | 储了实际的数据                                               |
| docker inspect datanode3 \| grep IPAddress       | datanode3       | 172.21.0.3 | 50013:50013<br/>50023:50023<br/>50073:50073                  | 储了实际的数据                                               |
| docker inspect namenode \| grep IPAddress        | namenode        | 172.21.0.2 | 9000:9000<br/>50070:50070                                    | 管理文件系统的元数据                                         |

一个HDFS集群主要由一个NameNode和很多个Datanode组成

在客户端host加入

192.168.17.130 resourcemanager
192.168.17.130 nodemanager1
192.168.17.130 namenode
192.168.17.130 historyserver
192.168.17.130 datanode1
192.168.17.130 datanode2
192.168.17.130 datanode3



在宿主机hosts里加入映射

vim /etc/hosts

172.21.0.2  namenode
172.21.0.3  datanode3
172.21.0.4  datanode1
172.21.0.5  datanode2
172.21.0.6  nodemanager1
172.21.0.7  historyserver
172.21.0.8  resourcemanager



#### 访问

```ruby
Namenode: http://192.168.17.130:50070/dfshealth.html#tab-overview
Datanode1: http://192.168.17.130:50071/
Datanode2: http://192.168.17.130:50072/
Nodemanager: http://192.168.17.130:8042/node
Resource manager: http://192.168.17.130:8088/cluster
History server: http://192.168.17.130:8188/applicationhistory
```

http://192.168.17.130:50070/dfshealth.html#tab-overview

![image-20200811161057953](image/Hadoop/image-20200811161057953.png)

http://192.168.17.130:8042/node

![image-20200811161250211](image/Hadoop/image-20200811161250211.png)

http://192.168.17.130:8088/cluster

![image-20200811161025113](image/Hadoop/image-20200811161025113.png)

http://192.168.17.130:8188/applicationhistory

![image-20200811161438199](image/Hadoop/image-20200811161438199.png)



#### 使用

```shell
# 进入容器
docker exec -it nodemanager1 bash
# 查看文件夹
hadoop fs -ls /
# 创建文件夹
hadoop fs -mkdir /xdr
# 删除文件夹
hadoop fs -rm -r /xdr


```

# 客户端配置

前面配置的每一个集群节点都可以做一个Hadoop客户端。但是我们一般都不会拿用来做集群的服务器来做客户端，需要单独的配置一个客户端。

配置hosts，vim /etc/hosts

172.21.0.2  namenode
172.21.0.3  datanode3
172.21.0.4  datanode1
172.21.0.5  datanode2
172.21.0.6  nodemanager1
172.21.0.7  historyserver
172.21.0.8  resourcemanager

1）安装JDK

2）安装Hadoop

tar -zxf hadoop-2.7.1.tar.gz -C /opt

ln -s hadoop-2.7.1 hadoop

vim /etc/profile

```sh
#set for hadoop
export HADOOP_HOME=/opt/hadoop
export PATH=$HADOOP_HOME/bin:$PATH
```

source /etc/profile

3）客户端配置子core-site.xml

cd /opt/hadoop/etc/hadoop

```xml
<configuration>
    <property>  
        <name>fs.defaultFS</name>  
        <value>hdfs://namenode:8020</value>  
    </property>  
    
    <property>
		<name>fs.hdfs.impl</name>
		<value>org.apache.hadoop.hdfs.DistributedFileSystem</value>
		<description>The FileSystem for hdfs: uris.</description>
    </property>
</configuration>
```

4）客户端配置之mapred-site.xml

```xml
<configuration>
    <!-- 指定MapReduce程序运行在Yarn上 -->
    <property>  
        <name>mapreduce.framework.name</name>  
        <value>yarn</value>  
    </property>  
</configuration>
```

5）客户端配置之yarn-site.xml

```xml
<configuration>
    <!-- 指定ResourceManager的地址 -->
    <property>  
        <name>yarn.resourcemanager.hostname</name>  
        <value>resourcemanager</value>  
    </property>  
</configuration>
```

6) 测试

[root@localhost hadoop]# hadoop fs -ls /
Found 3 items
drwxr-xr-x   - root supergroup          0 2021-01-26 04:25 /flume
drwxr-xr-x   - root supergroup          0 2020-08-11 05:00 /rmstate
drwxr-xr-x   - root supergroup          0 2021-01-31 22:09 /xdr



# MapReduce

## 介绍

- MapReduce是一个分布式运算程序的编程框架，是用户开发“基于Hadoop的数据分析应用”的核心框架。
- MapReduce核心功能是将用户编写的业务逻辑代码和自带默认组件整合成一个完整的分布式运算程序，并发运行在一个Hadoop集群上。
- MapReduce优点：
  - 易于编程；
  - 良好的扩展性；
  - 高容错性；
  - 适合PB级以上海量数据的离线处理，可以实现上千台服务器的并发工作。
- MapReduce缺点：
  - 不擅长实时计算；
  - 不擅长流失计算；
  - 不擅长DAG（有向图）计算。
- MapReduce核心思想：
  - 分布式的运算程序往往需要分成至少2个阶段；
  - 第一个阶段的 MapTask 并发实例，完全并行运行，互不相干；
  - 第二个阶段的 ReduceTask 并发实例互不相干，但是他们的数据依赖上一个阶段的所有 MapTask 并发实例的输出。
  - MapReduce编程模型只能包含一个Map阶段和一个Reduce阶段，如果用户的业务逻辑非常复杂，可以通过多个MapReduce程序串行解决。
- MapReduce程序在分布式运行时有三类实例进程：
  - MrAppMaster：负责整个程序的过程调度及状态协调。
  - MapTask：负责Map阶段的整个数据处理流程。
  - ReduceTask：负责Reduce阶段的整个数据处理流程。

## Hadoop序列化

- 因为Java的序列化附带信息较多，不便于在网络中高效传输。所以Hadoop自己开发了一套序列化机制。
- Hadoop序列化特点：
  - 紧凑：高效使用存储空间
  - 快速：读写数据的额外开销小；
  - 可扩展：随着通信协议的升级而可升级；
  - 互操作：支持多语言的交互。
- Hadoop的bean对象序列化步骤如下7步：
  - 1、必须实现Writable接口；
  - 2、必须有空参构造器，因为反序列化时，需要反射调用空参构造函数，；
  - 3、重写序列化方法；
  - 4、重写反序列化方法；
  - 5、反序列化的顺序必须和序列化的顺序一致；
  - 6、要想把结果显示在文件中，需要重写toString()，可有“\t”分开，方便后续使用。
  - 7、如果需要将自定义的bean放在key中传输，则还需要实现Comparable接口，因为MapReduce框架中的Shuffle过程要求对key必须能排序。

## MapReduce框架原理

### 切片与MapTask并行度决定机制

1、MapTask并行度决定机制

> - 数据块：Block是HDFS物理上把数据分成一块一块。
> - 数据切片：数据切片只是逻辑上对输入进行分片，并不会在磁盘上将其切分成片进行存储。
> - 一个Job的Map阶段并行度由客户端在提交Job时的切片数决定。
> - 每一个split切片分配一个MapTask并行实例处理。
> - 默认情况下，切片大小=BlockSize。
> - 切片时不考虑数据集整体，而是逐个针对每一个文件单独切片。

2、Job提交流程源码详解

3、FileInputFormat切片源码详解

# 数据压缩

- 压缩技术能够有效减少底层存储系统（HDFS）读写字节数。压缩提高了网络带宽和磁盘空间的效率。在运行MR程序时，网络数据传输、Shuffle和Merge要花大量的时间，尤其是数据规模很大和工作负载密集的情况下，因此，使用数据压缩显得非常重要。
- 鉴于磁盘I/O和网络带宽是Hadoop的宝贵资源，数据压缩对于节省资源、最小化磁盘I/O和网络传输非常有帮助。可以任意MapReduce阶段启用压缩。
- 压缩是提高Hadoop运行效率的一种优化策略。



# 常用的端口配置

## HDFS端口

| 参数                      | 描述                          | 默认  | 配置文件       | 例子值              |
| ------------------------- | ----------------------------- | ----- | -------------- | ------------------- |
| fs.default.name namenode  | namenode RPC交互端口          | 8020  | core-site.xml  | hdfs://master:8020/ |
| dfs.http.address          | NameNode web管理端口          | 50070 | hdfs- site.xml | 0.0.0.0:50070       |
| dfs.datanode.address      | datanode　控制端口            | 50010 | hdfs -site.xml | 0.0.0.0:50010       |
| dfs.datanode.ipc.address  | datanode的RPC服务器地址和端口 | 50020 | hdfs-site.xml  | 0.0.0.0:50020       |
| dfs.datanode.http.address | datanode的HTTP服务器和端口    | 50075 | hdfs-site.xml  | 0.0.0.0:50075       |

## MR端口

| 参数                             | 描述                   | 默认  | 配置文件        | 例子值              |
| -------------------------------- | ---------------------- | ----- | --------------- | ------------------- |
| mapred.job.tracker               | job-tracker交互端口    | 8021  | mapred-site.xml | hdfs://master:8021/ |
| job                              | tracker的web管理端口   | 50030 | mapred-site.xml | 0.0.0.0:50030       |
| mapred.task.tracker.http.address | task-tracker的HTTP端口 | 50060 | mapred-site.xml | 0.0.0.0:50060       |

## 其它端口

## MR端口

# 性能平台现网情况

## 版本

![image-20210302181608218](image/Hadoop/image-20210302181608218.png)

![image-20210302181623798](image/Hadoop/image-20210302181623798.png)

## Ambari

Apache Ambari是一种基于Web的工具，支持Apache Hadoop集群的创建、管理和监控。Ambari已支持大多数Hadoop组件，包括HDFS、MapReduce、Hive、Pig、 Hbase、Zookeeper、Sqoop和Hcatalog等；除此之外，Ambari还支持Spark、Storm等计算框架及资源调度平台YARN。

地址：http://10.209.179.1:8080/  用户名query密码query

![](image/Hadoop/clip_image001-1614678834722.png)

![image-20210302181405109](image/Hadoop/image-20210302181405109.png)

![image-20210302181441218](image/Hadoop/image-20210302181441218.png)

## HDFS

![](image/Hadoop/clip_image001-1614678890146.png)

### NameNode管理界面

http://mn01.lc2f.hi.ipm.nokia.com:50070/dfshealth.html#tab-overview

![image-20210302175602711](image/Hadoop/image-20210302175602711.png)

![image-20210302175703531](image/Hadoop/image-20210302175703531.png)

![image-20210302175717288](image/Hadoop/image-20210302175717288.png)

![image-20210302175732779](image/Hadoop/image-20210302175732779.png)

![image-20210303145858644](image/Hadoop/image-20210303145858644.png)

## YARN

![image-20210302180042133](image/Hadoop/image-20210302180042133.png)

### ResourceManager管理界面

http://mn04.lc2f.hi.ipm.nokia.com:8088/cluster

![image-20210302175902969](image/Hadoop/image-20210302175902969.png)

![image-20210302175952209](image/Hadoop/image-20210302175952209.png)

![image-20210302180009594](image/Hadoop/image-20210302180009594.png)

## HBase

![image-20210302180222615](image/Hadoop/image-20210302180222615.png)

### 管理界面

http://mn06.lc2f.hi.ipm.nokia.com:16010/master-status

![image-20210302180241738](image/Hadoop/image-20210302180241738.png)

![image-20210302180403022](image/Hadoop/image-20210302180403022.png)

![image-20210302180513004](image/Hadoop/image-20210302180513004.png)

![image-20210302180625409](image/Hadoop/image-20210302180625409.png)

![image-20210302180636172](image/Hadoop/image-20210302180636172.png)

## Spark

![image-20210302181147172](image/Hadoop/image-20210302181147172.png)

http://mn06.lc2f.hi.ipm.nokia.com:18080/?page=22&showIncomplete=false

![image-20210302181225864](image/Hadoop/image-20210302181225864.png)
# 原理介绍

官网：https://flink.apache.org/zh/

参考：https://www.jianshu.com/p/442521ddc28f

​				http://wuchong.me/blog/2016/05/25/flink-internals-window-mechanism/

​				https://www.jianshu.com/p/cdcdec05a249

Apache Flink 是近年来越来越流行的是一个框架和分布式处理引擎，它同时支持了`批处理`和`流处理`。用于对无界和有界数据流进行有状态计算。

![img](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/flink-home-graphic.png)

它和storm和spark的区别在哪里? storm是基于流计算的, 但是也可以模拟批处理, spark streaming也可以进行微批处理, 虽说在性能延迟上处于亚秒级别, 但也不足以说明Flink崛起如此迅速(毕竟从spark迁移到Flink是要成本的).

`图标`：松鼠，表示敏捷、轻快。

**处理无界和有界数据**

任何类型的数据都可以形成一种事件流。信用卡交易，传感器测量，机器日志或网站或移动应用程序上的用户交互，所有这些数据都作为流生成。 数据可以作为无界或有界流处理。

- `无界流`有定义流的开始但没有定义流的结束。它们会无休止地产生数据。无界流的数据必须持续处理，即数据被摄取后需要立刻处理。我们不能等到所有数据都到达再处理，因为输入是无限的，在任何时候输入都不会完成。处理无界数据通常要求以特定顺序摄取事件，例如事件发生的顺序，以便能够推断结果的完整性。
- 有定义流的开始，也有定义流的结束。有界流可以在摄取所有数据后再进行计算。有界流所有数据可以被排序，所以并不需要有序摄取。有界流处理通常被称为批处理

Apache Flink擅长处理无界和有界数据集。精确控制时间和状态使Flink的运行时能够在无界流上运行任何类型的应用程序。有界流由算法和数据结构内部处理，这些算法和数据结构专门针对固定大小的数据集而设计，从而产生出色的性能。

**部署应用程序在任何地方**

Apache Flink是一个分布式系统，需要计算资源来执行应用程序。Flink集成了所有常见的集群资源管理器，如Hadoop YARN、Apache Mesos和Kubernetes，但也可以设置为作为独立集群运行。

Flink被设计成能够很好地工作于前面列出的每个资源管理器。这是通过特定于资源管理器的部署模式实现的，这种部署模式允许Flink以其惯用的方式与每个资源管理器交互。

在部署Flink应用程序时，Flink根据应用程序的配置并行性自动识别所需的资源，并从资源管理器请求它们。如果发生故障，Flink将通过请求新的资源来替换失败的容器。提交或控制应用程序的所有通信都是通过REST调用进行的。这简化了Flink在许多环境中的集成。

**以任何规模运行应用程序**

Flink设计用于在任何规模上运行有状态流应用程序。应用程序被并行化成数千个任务，这些任务分布在一个集群中并发执行。因此，应用程序实际上可以利用无限数量的cpu、主内存、磁盘和网络IO。此外，Flink很容易保持非常大的应用状态。它的异步和增量检查点算法确保了对处理延迟的最小影响，同时保证了精确的一次状态一致性。

**利用内存中的性能**

有状态Flink应用程序针对本地状态访问进行了优化。任务状态总是在内存中维护，如果状态大小超过可用内存，则在具有访问效率的磁盘数据结构中维护。因此，任务通过访问本地(通常是在内存中)状态来执行所有计算，从而产生非常低的处理延迟。Flink通过定期和异步检查本地状态到持久存储，保证了在发生故障时的精确一次状态一致性。

### 特点

下面这两个特点加快编程效率，把本来需要程序员花大力气手动完成的工作交给框架：

#### Flink有灵活的窗口

在流处理应用中，数据是连续不断的，因此我们不可能等到所有数据都到了才开始处理。当然我们可以每来一个消息就处理一次，但是有时候我们需要做一些聚合类的处理，例如：在过去的1分钟内有多少用户点击了我们的网页，在这种情况下，我们必须定义一个窗口，用来收集最近一分钟内的数据，并对这个窗口内的数据进行计算。

窗口可以是`时间驱动`（Time Window，例如：每30秒钟），也可以是`数据驱动`（Count Window，例如：每一百个元素）。一种经典的窗口分类可以分成：翻滚窗口（Tumbling Window，无重叠），滚动窗口（Sliding Window，有重叠），和会话窗口（Session Window，活动间隙）。

假设，淘宝网会记录每个用户每次购买的商品个数，我们要做的是统计不同窗口中用户购买商品的总数。下图给出了几种经典的窗口切分概述图：

![img](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/1324594-2134a36e28355ef2.png)

上图中, raw data stream 代表用户的购买行为流, 圈中的数字代表该用户本次购买的商品个数, 事件是按时间分布的, 所以可以看出事件之间是有time gap的. Flink 提供了上图中所有的窗口类型. 关于Flink窗口的更多细节可以参考[Flink 原理与实现：Window 机制](http://wuchong.me/blog/2016/05/25/flink-internals-window-mechanism/)。

##### Time Window

就如名字所说的，Time Window 是根据时间对数据流进行分组的。这里我们涉及到了流处理中的时间问题，时间问题和消息乱序问题是紧密关联的，这是流处理中现存的难题之一，我们将在后续的 [EventTime 和消息乱序处理](http://wuchong.me/blog/2016/05/25/flink-internals-window-mechanism/#) 中对这部分问题进行深入探讨。这里我们只需要知道 Flink 提出了三种时间的概念，分别是event time（事件时间：事件发生时的时间），ingestion time（摄取时间：事件进入流处理系统的时间），processing time（处理时间：消息被计算处理的时间）。Flink 中窗口机制和时间类型是完全解耦的，也就是说当需要改变时间类型时不需要更改窗口逻辑相关的代码。

- **Tumbling Time Window**
  如上图，我们需要统计每一分钟中用户购买的商品的总数，需要将用户的行为事件按每一分钟进行切分，这种切分被成为翻滚时间窗口（Tumbling Time Window）。翻滚窗口能将数据流切分成不重叠的窗口，每一个事件只能属于一个窗口。通过使用 DataStream API，我们可以这样实现：

  ```scala
  // Stream of (userId, buyCnt)
  val buyCnts: DataStream[(Int, Int)] = ...
  
  val tumblingCnts: DataStream[(Int, Int)] = buyCnts
    // key stream by userId
    .keyBy(0) 
    // tumbling time window of 1 minute length
    .timeWindow(Time.minutes(1))
    // compute sum over buyCnt
    .sum(1)
  ```

- **Sliding Time Window**
  但是对于某些应用，它们需要的窗口是不间断的，需要平滑地进行窗口聚合。比如，我们可以每30秒计算一次最近一分钟用户购买的商品总数。这种窗口我们称为滑动时间窗口（Sliding Time Window）。在滑窗中，一个元素可以对应多个窗口。通过使用 DataStream API，我们可以这样实现：

  ```scala
  val slidingCnts: DataStream[(Int, Int)] = buyCnts
    .keyBy(0) 
    // sliding time window of 1 minute length and 30 secs trigger interval
    .timeWindow(Time.minutes(1), Time.seconds(30))
    .sum(1)
  ```

  

##### Count Window

Count Window 是根据元素个数对数据流进行分组的。

- **Tumbling Count Window**
  当我们想要每100个用户购买行为事件统计购买总数，那么每当窗口中填满100个元素了，就会对窗口进行计算，这种窗口我们称之为翻滚计数窗口（Tumbling Count Window），上图所示窗口大小为3个。通过使用 DataStream API，我们可以这样实现：

  ```scala
  // Stream of (userId, buyCnts)
  val buyCnts: DataStream[(Int, Int)] = ...
  
  val tumblingCnts: DataStream[(Int, Int)] = buyCnts
    // key stream by sensorId
    .keyBy(0)
    // tumbling count window of 100 elements size
    .countWindow(100)
    // compute the buyCnt sum 
    .sum(1)
  ```

  

- **Sliding Count Window**
  当然Count Window 也支持 Sliding Window，虽在上图中未描述出来，但和Sliding Time Window含义是类似的，例如计算每10个元素计算一次最近100个元素的总和，代码示例如下。

  ```scala
  val slidingCnts: DataStream[(Int, Int)] = vehicleCnts
    .keyBy(0)
    // sliding count window of 100 elements size and 10 elements trigger interval
    .countWindow(100, 10)
    .sum(1)
  ```

  

##### Session Window

在这种用户交互事件流中，我们首先想到的是将事件聚合到会话窗口中（一段用户持续活跃的周期），由非活跃的间隙分隔开。如上图所示，就是需要计算每个用户在活跃期间总共购买的商品数量，如果用户30秒没有活动则视为会话断开（假设raw data stream是单个用户的购买行为流）。Session Window 的示例代码如下：

```scala
// Stream of (userId, buyCnts)
val buyCnts: DataStream[(Int, Int)] = ...
  
val sessionCnts: DataStream[(Int, Int)] = vehicleCnts
  .keyBy(0)
  // session window based on a 30 seconds session gap interval 
  .window(ProcessingTimeSessionWindows.withGap(Time.seconds(30)))
  .sum(1)

```

##### Window的实现

窗口机制以及各组件之间是如何相互工作的：

![img](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/TB1swNgKXXXXXc4XpXXXXXXXXXX)

#### Exactly once语义保证

Flink的检查点和恢复算法保证了在发生故障时应用程序状态的一致性。因此，失败将被透明地处理，并且不会影响应用程序的正确性。——可以看出Exactly-once是为`有状态`的计算准备的。

换句话说，没有状态的算子操作（poerator），Flink无法也无需保证其只被处理Exactly-once！因为及时失败的情况下，无状态的operator（map，filter等）只需要数据重新计算一遍即可。

当机器(节点)等失败时, 只需从最近的一份快照开始, 利用可重发的数据源重发一次数据即可, 当数据经过filter算子时, 全部重新算一次即可，根本不需要区分哪个数据被计算过，哪个数据没有被计算过，因为没有状态的算子只有输入和输出，没有状态可以保存.

此外, Flink的Exactly-once需要从最近的一份快照开始重放数据, 因此这也和数据源的能力有关, 不是所有的数据源都可以提供Exactly-once语义的. 以下是apache官网列出的数据源和Exactly-once语义保障能力列表.

| Source                | Guarantees                                   | Notes                                                |
| :-------------------- | :------------------------------------------- | :--------------------------------------------------- |
| Apache Kafka          | exactly once                                 | Use the appropriate Kafka connector for your version |
| AWS Kinesis Streams   | exactly once                                 |                                                      |
| RabbitMQ              | at most once (v 0.10) / exactly once (v 1.0) |                                                      |
| Twitter Streaming API | at most once                                 |                                                      |
| Collections           | exactly once                                 |                                                      |
| Files                 | exactly once                                 |                                                      |
| Sockets               | at most once                                 |                                                      |

### 传统数据处理架构

#### 事务处理

![image-20200712161949344](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200712161949344.png)

#### 分析处理

将数据从业务数据库复制到数仓，再进行分析和查询。

![image-20200712162243057](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200712162243057.png)

### 有状态的流式处理

![image-20200712162746447](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200712162746447.png)

### 流处理的演变

`lambda架构`，用两套系统，同时保证低延迟和结果准确

![image-20200712162919768](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200712162919768.png)

![image-20200712163416443](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200712163416443.png)

### 应用

#### 概述

Apache Flink因其丰富的功能而成为开发和运行多种不同类型应用程序的绝佳选择。Flink的功能包括对流和批处理的支持，复杂的状态管理，事件时间处理语义以及状态的一致性保证。此外，Flink可以部署在各种资源管理器上（如YARN，Apache Mesos和Kubernetes），也可以作为裸机硬件上的独立群集。Flink配置为高可用性，没有单点故障。Flink提供高吞吐量和低延迟，并为世界上一些最苛刻的流处理应用程序提供支持。
 下面，我们将探讨由Flink提供支持的最常见类型的应用程序，并指出实际示例。

- 事件驱动型的应用程序
- 数据分析型的应用程序
- 数据管道型的应用程序

**事件驱动型的应用程序**

事件驱动型的应用程序是一个有状态的应用程序，它从一个或多个事件流中提取事件，并通过触发计算、状态更新或外部操作对传入事件做出反应。
 传统应用程序设计具有分离的计算和数据存储层，在此体系结构中，应用程序从远程事务数据库中读取数据并将数据持久化。
 相反，事件驱动的应用程序基于有状态流处理应用程序。在这种设计中，数据和计算是共同定位的，这产生了本地（内存或磁盘）数据访问。通过定期将检查点写入远程持久存储来实现容错。
 事件驱动的应用程序不是查询远程数据库，而是在本地访问其数据，从而在吞吐量和延迟方面产生更好的性能。远程持久存储的检查点可以异步和递增完成。因此，检查点对常规事件处理的影响非常小。

几种典型的事件驱动型应用程序：

- 欺诈识别
- 异常检测
- 基于规则的警报
- 业务流程监控
- Web应用程序（社交网络）

**数据分析型应用程序**

数据分析工作从原始数据中提取信息。传统上，数据分析是在记录事件的有界数据集上作为批量查询或应用程序执行。为了将最新数据合并到分析结果中，必须将其添加到分析的数据集中，并重新运行查询或应用程序。结果将写入存储系统或作为报告发出。
 借助先进的流处理引擎，还可以实时地执行分析。流式查询或应用程序不是读取有限数据集，而是摄取实时事件流，并在消耗事件时不断生成和更新结果。结果要么写入外部数据库，要么保持为内部状态。仪表板应用程序可以从外部数据库读取最新结果或直接查询应用程序的内部状态。Apache Flink支持流式和批量分析应用程序。
 与批量分析相比，连续流分析的优势不仅限于低延迟。与批量查询相比，流式查询不必处理输入数据中的人为边界，这些边界是由定期导入和输入的有界性质引起的。
 另一方面是更简单的应用程序架构。批量分析管道由若干独立组件组成，以定期调度数据提取和查询执行。可靠地操作这样的管道并非易事，因为一个组件的故障会影响管道的后续步骤。相比之下，在像Flink这样的复杂流处理器上运行的流分析应用程序包含从数据摄取到连续结果计算的所有步骤。

几种典型的数据分析型应用程序：

- 电信网络的质量监控
- 分析移动应用程序中的产品更新和实验评估
- 对消费者技术中的实时数据进行特别分析
- 大规模图分析

**数据管道型应用程序**

提取 - 转换 - 加载（ETL）是在存储系统之间转换和移动数据的常用方法。通常会定期触发ETL作业，以将数据从事务数据库系统复制到分析数据库或数据仓库。
 数据管道与ETL作业具有相似的用途。它们可以转换和丰富数据，并可以将数据从一个存储系统移动到另一个存储系统。但是，它们以连续流模式运行，而不是定期触发。因此，他们能够从连续生成数据的源中读取记录，并以低延迟将其移动到目的地。例如，数据管道可能会监视文件系统目录中的新文件并将其数据写入事件日志。
 连续数据流水线优于周期性ETL作业的原因是减少了将数据移动到目的地的延迟。此外，数据管道更通用，可用于更多用例，因为它们能够连续消耗和发送数据。

几种典型的数据管道型应用：

- 电子商务中的实时搜索索引构建
- 电子商务中持续的ETL



**电商和市场营销**

- 数据报表、广告投放、业务流量

**物联网**

- 传感器实时数据采集和显示、实时报警、交通运输

**电信业**

- 基站流量调配
- 信令关联

**银行和金融业**

- 实时结算和通知推送，实时监测异常行为

## 安装

### 下载地址：

最新版本：https://flink.apache.org/zh/downloads.html

历史版本：https://archive.apache.org/dist/flink

### 使用docker-compose安装Flink集群

参考：https://blog.csdn.net/zhanaolu4821/article/details/86001144

官网docker-compose.yaml文件内容

```yaml
version: "2.1"
services:
  jobmanager:
    image: ${FLINK_DOCKER_IMAGE_NAME:-flink}
    expose:
      - "6123"
    ports:
      - "8081:8081"
    command: jobmanager
    environment:
      - JOB_MANAGER_RPC_ADDRESS=jobmanager

  taskmanager:
    image: ${FLINK_DOCKER_IMAGE_NAME:-flink}
    expose:
      - "6121"
      - "6122"
    depends_on:
      - jobmanager
    command: taskmanager
    links:
      - "jobmanager:jobmanager"
    environment:
      - JOB_MANAGER_RPC_ADDRESS=jobmanager
```

#### 下载安装

```shell
wget https://codeload.github.com/melentye/flink-docker/zip/master

unzip master

cd flink-docker-master

#将进行相应的Flink的包的下载
docker-compose up -d

#复制task Managers  N代表要创建的数量
docker-compose scale flink-taskmanager=N

#查看运行的容器
docker ps
```

![image-20200713094658547](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200713094658547.png)

进入http://192.168.17.130:8081/

![image-20200713094404355](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200713094404355.png)

#### 部署和运行一个job

1. 将写好的Flink任务jar包部署在jobManager\

   docker cp /root/job.jar $(docker ps --filter name=flink-jobmanager --format={{.ID}}):/job.jar to

2. 复制数据到每个Flink Node 结点 如果是必须的

   for i in $(docker ps --filter name=flink --format={{.ID}}); do docker cp /path/to/data.csv $i:/data.csv done

3. 运行job

   docker exec -it $(docker ps --filter name=flink-jobmanager --format={{.ID}}) flink run -c <your_job_class> /job.jar

#### 停止Flink集群

docker-compose stop

#### 启动Flink集群

cd /opt/flink-docker-master

docker-compose start

### Standalone模式

参考：https://www.cnblogs.com/jake-jin/p/11435458.html

#### 使用docker部署集群

集群规划：

| 节点   | master | worker |
| ------ | ------ | ------ |
| master | master |        |
| slave1 |        | worker |
| slave2 |        | worker |



```shell
#创建3个容器
docker run --name master -id -h master -v /data:/data my_centos /usr/sbin/init
docker run --name slave1 -id -h slave1 -v /data:/data my_centos /usr/sbin/init
docker run --name slave2 -id -h slave2 -v /data:/data my_centos /usr/sbin/init

#进入master容器解压
docker exec -it master /bin/bash
docker exec -it slave1 /bin/bash
docker exec -it slave2 /bin/bash

#开启sshd服务
/usr/sbin/sshd

#配置hosts文件
echo "172.17.0.3      slave1" >> /etc/hosts
echo "172.17.0.4      slave2" >> /etc/hosts


#设置ssh无密钥登陆
cd ~/.ssh
ssh-keygen -t rsa
#然后敲（三个回车）

#将公钥拷贝到要免密登录的目标机器上
ssh-copy-id slave1
ssh-copy-id slave2

#测试登陆
ssh slave1

cd /opt
#从共享文件夹获取安装包
tar zxf /data/flink-1.7.2-bin-hadoop27-scala_2.11.tgz -C /opt/

#或下载
wget https://archive.apache.org/dist/flink/flink-1.7.2/flink-1.7.2-bin-hadoop27-scala_2.11.tgz
tar zxf flink-1.7.2-bin-hadoop27-scala_2.11.tgz 

mv flink-1.7.2 flink

```

修改配置文件：

```shell
#进入master容器
cd /opt/flink/conf

#编辑masters文件
vim masters
maste:8081

#编辑slaves文件
vim slaves
slave1
slave2

#修改flink-conf.yaml文件
vim flink-conf.yaml
jobmanager.rpc.address: master
taskmanager.numberOfTaskSlots: 1  #设置每个taskmanager里的taskslots个数，可根据cpu个数设置
```

配置环境变量：

```shell
vi /etc/profile
#set for java
export JAVA_HOME=/data/java
export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
export PATH=$PATH:$JAVA_HOME/bin

export FLINK_HOME=/opt/flink
export PATH=$PATH:$FLINK_HOME/bin

source /etc/profile
```

启动

```shell
[root@master ~]# /opt/flink/bin/start-cluster.sh 
Starting cluster.
Starting standalonesession daemon on host master.
Starting taskexecutor daemon on host slave1.
Starting taskexecutor daemon on host slave2.
[root@master ~]# jps
9225 StandaloneSessionClusterEntrypoint
9277 Jps
```

登陆web查看状态

http://172.17.0.2:8081

如果物理机无法访问，需要添加路由，192.168.124.8表示电脑的地址，在cmd里输入ipconfig查看

route add -p 172.17.0.0 mask 255.0.255.0 192.168.17.120

![image-20200715175522776](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200715175522776.png)

#### 提交任务

/opt/flink/bin/flink run -c com.wxl.wc.StreamWordCount -p 2 /data/FlinkStudy-1.0-SNAPSHOT-jar-with-dependencies.jar --host 172.17.0.4 --port 7777

--c：是jar包的main方法名

-p：设置使用的parallelism个数



### Yarn模式

### Kubernetes模式

参考：https://blog.csdn.net/zjerryj/article/details/100063858

Kubernetes 是目前非常流行的容器编排系统，在其之上可以运行 Web 服务、大数据处理等各类应用。这些应用被打包在一个个非常轻量的容器中，我们通过声明的方式来告知 Kubernetes 要如何部署和扩容这些程序，并对外提供服务。Flink 同样是非常流行的分布式处理框架，它也可以运行在 Kubernetes 之上。将两者相结合，我们就可以得到一个健壮和高可扩的数据处理应用，并且能够更安全地和其它服务共享一个 Kubernetes 集群。
在 Kubernetes 上部署 Flink 有两种方式：会话集群（Session Cluster）和脚本集群（Job Cluster）。会话集群和独立部署一个 Flink 集群类似，只是底层资源换成了 K8s 容器，而非直接运行在操作系统上。该集群可以提交多个脚本，因此适合运行那些短时脚本和即席查询。脚本集群则是为单个脚本部署一整套服务，包括 JobManager 和 TaskManager，运行结束后这些资源也随即释放。我们需要为每个脚本构建专门的容器镜像，分配独立的资源，因而这种方式可以更好地和其他脚本隔离开，同时便于扩容或缩容。文本将以脚本集群为例，演示如何在 K8s 上运行 Flink 实时处理程序，主要步骤如下：

- 编译并打包 Flink 脚本 Jar 文件；
- 构建 Docker 容器镜像，添加 Flink 运行时库和上述 Jar 包；
- 使用 Kubernetes Job 部署 Flink JobManager 组件；
- 使用 Kubernetes Service 将 JobManager 服务端口开放到集群中；
- 使用 Kubernetes Deployment 部署 Flink TaskManager；
- 配置 Flink JobManager 高可用，需使用 ZooKeeper 和 HDFS；
- 借助 Flink SavePoint 机制来停止和恢复脚本。



## Flink运行架构

### 运行时的组件

![image-20200715162059995](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200715162059995.png)

#### 作业管理器（JobManager）

控制一个应用程序执行的主进程，也就是说， 每个应用程序都会被一个不同的JobManager 所控制执行。JobManager 会先接收到要执行的应用程序，这个应用程序会包括： 作业图（JobGraph）、逻辑数据流图（logical dataflow graph）和打包了所有的类、库和其它资源的 JAR 包。JobManager 会把 JobGraph 转换成一个物理层面的数据流图，这个图被叫做“执行图”（ExecutionGraph），包含了所有可以并发执行的任务。JobManager 会向资源管理器（ResourceManager）请求执行任务必要的资源，也就是任务管理器（TaskManager）上的插槽（ slot）。一旦它获取到了足够的资源，就会将执行图分发到真正运行它们的TaskManager 上。而在运行过程中，JobManager 会负责所有需要中央协调的操作，比如说检查点（checkpoints）的协调。

#### 资源管理器（ResourceManager）

主要负责管理任务管理器（TaskManager）的插槽（slot），TaskManger 插槽是 Flink 中定义的处理资源单元。Flink 为不同的环境和资源管理工具提供了不同资源管理器，比如YARN、Mesos、K8s，以及 standalone 部署。当 JobManager 申请插槽资源时，ResourceManager 会将有空闲插槽的TaskManager 分配给 JobManager。如果 ResourceManager 没有足够的插槽来满足 JobManager 的请求，它还可以向资源提供平台发起会话，以提供启动 TaskManager 进程的容器。另外，ResourceManager 还负责终止空闲的 TaskManager，释放计算资源。

#### 任务管理器（TaskManager）

Flink 中的工作进程。通常在 Flink 中会有多个 TaskManager 运行，每一个 TaskManager 都包含了一定数量的插槽（slots）。插槽的数量限制了 TaskManager 能够执行的任务数量。启动之后， TaskManager 会向资源管理器注册它的插槽；收到资源管理器的指令后， TaskManager 就会将一个或者多个插槽提供给 JobManager 调用。JobManager 就可以向插槽分配任务（tasks）来执行了。在执行过程中，一个TaskManager 可以跟其它运行同一应用程序的 TaskManager 交换数据。

#### 分发器（Dispatcher）

可以跨作业运行，它为应用提交提供了 REST 接口。当一个应用被提交执行时，分发器就会启动并将应用移交给一个 JobManager。由于是 REST 接口，所以 Dispatcher 可以作为集群的一个 HTTP 接入点，这样就能够不受防火墙阻挡。Dispatcher 也会启动一个 Web UI，用来方便地展示和监控作业执行的信息。Dispatcher 在架构中可能并不是必需的，这取决于应用提交运行的方式。

### 任务提交流程

![image-20200715164343344](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200715164343344.png)

上图是从一个较为高层级的视角，来看应用中各组件的交互协作。如果部署的集群环境不同（例如 YARN，Mesos，Kubernetes，standalone 等），其中一些步骤可以被省略，或是有些组件会运行在同一个 JVM 进程中。

具体地，如果我们将 Flink 集群部署到 YARN 上，那么就会有如下的提交流程：

![image-20200715164310801](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200715164310801.png)

Flink 任务提交后， Client 向 HDFS 上传 Flink 的 Jar 包和配置， 之后向 Yarn ResourceManager 提交任务， ResourceManager 分配 Container 资源并通知对应的NodeManager 启动 ApplicationMaster， ApplicationMaster 启动后加载 Flink 的 Jar 包和配置构建环境，然后启动 JobManager，之后 ApplicationMaster 向 ResourceManager 申请资源启动 TaskManager ， ResourceManager 分配 Container 资 源 后 ， 由ApplicationMaster 通 知 资 源 所 在 节 点 的 NodeManager 启动 TaskManager ， NodeManager 加载 Flink 的 Jar 包和配置构建环境并启动 TaskManager，TaskManager 启动后向 JobManager 发送心跳包， 并等待 JobManager 向其分配任务。

### 任务调度原理

![image-20200715164937773](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200715164937773.png)

#### TaskManger 与 Slots

![image-20200715171843662](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200715171843662.png)

Flink 中每一个 worker(TaskManager)都是一个 **JVM** **进程**，它可能会在**独立的线程**上执行一个或多个 subtask。为了控制一个 worker 能接收多少个 task， worker 通过 task slot 来进行控制（一个 worker 至少有一个 task slot）。

每个 task slot 表示 TaskManager 拥有资源的**一个固定大小的子集**。假如一个

TaskManager 有三个 slot，那么它会将其管理的内存分成三份给各个 slot。资源 slot 化意味着一个 subtask 将不需要跟来自其他 job 的 subtask 竞争被管理的内存， 取而代之的是它将拥有一定数量的内存储备。需要注意的是，这里不会涉及到 CPU 的隔离， slot 目前仅仅用来隔离 task 的受管理的内存。

 通过调整 task slot 的数量，允许用户定义 subtask 之间如何互相隔离。如果一个TaskManager 一个 slot， 那将意味着每个 task group 运行在独立的 JVM 中（该 JVM 可能是通过一个特定的容器启动的），而一个 TaskManager 多个 slot 意味着更多的subtask 可以共享同一个 JVM。而在同一个 JVM 进程中的 task 将共享 TCP 连接（ 基于多路复用）和心跳消息。它们也可能共享数据集和数据结构，因此这减少了每个task 的负载

![image-20200715172727979](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200715172727979.png)

默认情况下，Flink 允许子任务共享 slot，即使它们是不同任务的子任务（前提是它们来自同一个 job）。 这样的结果是，一个 slot 可以保存作业的整个管道。

**Task Slot** **是静态的概念， 是指** **TaskManager** **具有的并发执行能力**， 可以通过参数 taskmanager.numberOfTaskSlots 进行配置；而**并行度** **parallelism** **是动态概念， 即** **TaskManager** **运行程序时实际使用的并发能力**，可以通过参数 parallelism.default

进行配置。

也就是说，假设一共有 3 个 TaskManager， 每一个 TaskManager 中的分配 3 个TaskSlot，也就是每个 TaskManager 可以接收 3 个 task，一共 9 个 TaskSlot，如果我们设置 parallelism.default=1，即运行程序默认的并行度为 1，9 个 TaskSlot 只用了 1 个，有 8 个空闲， 因此， 设置合适的并行度才能提高效率。

![image-20200715173537575](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200715173537575.png)![image-20200715173608596](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200715173608596.png)



#### 程序与数据流（DataFlow)

![image-20200715165528641](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200715165528641.png)

所有的 Flink 程序都是由三部分组成的： **Source** 、**Transformation** 和 **Sink**。

Source 负责读取数据源， Transformation 利用各种算子进行处理加工， Sink 负责输出。

在运行时，Flink上运行的程序会被映射成“逻辑数据流”（dataflows），它包含了这三个部分。`每一个dataflow以一个或多个sources开始以一个或多个sinks结束。`dataflow类似于任意的有向无环图（DAG）。在大部分情况下，程序中的转换运算（transformation）跟dataflow中的算子（operator）是一一对应的关系，但有时候，一个transformation可能对应多个operator。

![image-20200715170200667](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200715170200667.png)

#### 执行图（ExecutionGraph）

由 Flink 程序直接映射成的数据流图是 StreamGraph，也被称为逻辑流图，因为它们表示的是计算逻辑的高级视图。为了执行一个流处理程序，Flink 需要将逻辑流图转换为物理数据流图（也叫执行图）， 详细说明程序的执行方式。

Flink 中的执行图可以分成四层：StreamGraph -> JobGraph -> ExecutionGraph ->物理执行图。

- **StreamGraph**：是根据用户通过 Stream API 编写的代码生成的最初的图。用来表示程序的拓扑结构。

- **JobGraph**：StreamGraph 经过优化后生成了 JobGraph，提交给 JobManager 的数据结构。主要的优化为，将多个符合条件的节点 chain 在一起作为一个节点，这样可以减少数据在节点之间流动所需要的序列化/反序列化/传输消耗。

- **ExecutionGraph** ： JobManager  根据 JobGraph   生成 ExecutionGraph 。ExecutionGraph 是 JobGraph 的并行化版本， 是调度层最核心的数据结构。

- **物理执行图**： JobManager 根据 ExecutionGraph 对 Job 进行调度后， 在各个TaskManager 上部署 Task 后形成的“图”，并不是一个具体的数据结构。

![image-20200715170837616](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200715170837616.png)

#### 并行度（Parallelism）

Flink 程序的执行具有**并行、分布式**的特性。

在执行过程中，一个流（ stream） 包含一个或多个分区（ stream partition），而每一个算子（ operator）可以包含一个或多个子任务（ operator subtask），这些子任务在不同的线程、不同的物理机或不同的容器中彼此互不依赖地执行。

一个特定算子的子任务（ subtask） 的个数被称之为其并行度（ parallelism） 。

一般情况下， 一个流程序的并行度， 可以认为就是其所有算子中最大的并行度。一个程序中，不同的算子可能具有不同的并行度。

![image-20200715171145855](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200715171145855.png)

Stream 在算子之间传输数据的形式可以是 one-to-one(forwarding)的模式也可以是 redistributing 的模式，具体是哪一种形式，取决于算子的种类。

**One-to-one**：stream(比如在 source 和 map operator 之间)维护着分区以及元素的顺序。那意味着 map 算子的子任务看到的元素的个数以及顺序跟 source 算子的子任务生产的元素的个数、顺序相同， map、fliter、flatMap 等算子都是 one-to-one 的对应关系。

- 类似于 spark 中的**窄依赖**

**Redistributing**： stream(map()跟 keyBy/window 之间或者 keyBy/window 跟 sink 之间)的分区会发生改变。每一个算子的子任务依据所选择的 transformation 发送数据到不同的目标任务。例如，keyBy() 基于 hashCode 重分区、broadcast 和 rebalance 会随机重新分区，这些算子都会引起 redistribute 过程，而 redistribute 过程就类似于Spark 中的 shuffle 过程。

- 类似于 spark 中的**宽依赖**

一个例子：jobGraph里可知一共有A/B/C/D/E 5个算子，最大并行度为4，可以如下设置

taskmanager.numberOfTaskSlots: 2

parallelism.default: 4



![image-20200715180058049](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200715180058049.png)

#### 任务链（Operator Chains）

Flink采用了一种称为任务链的优化技术，可以在特定条件下减少本地通信的开销。为了满足任务链的要求，必须将两个或多个算子设为相同的并行度，并通过本地转发（local forward）的方式进行连接。

**相同并行度的** **one to one** **操作**，Flink 这样相连的算子链接在一起形成一个 task，原来的算子成为里面的一部分。将算子链接成 task 是非常有效的优化：它能减少线程之间的切换和基于缓存区的数据交换，在减少时延的同时提升吞吐量。链接的行为可以在编程 API 中进行指定。

![image-20200715171601552](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200715171601552.png)

默认都是打开任务链功能，如下

![image-20200716093848261](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200716093848261.png)

可在API中设置env.disableOperatorChaining();关闭全任务链，

![image-20200716093748703](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200716093748703.png)

也可以在指定操作的后面加设置.filter(_.nonEmpty).disableChaining()可以断开前和后的链

![image-20200716093721576](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200716093721576.png)

 设置.filter(_.nonEmpty).startNewChain()可以只断开前链

![image-20200716094146017](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200716094146017.png)



### 状态管理

大多数流应用程序都是有状态的。许多算子会不断地读取和更新状态，例如在窗口中收集的数据、读取输入源的位置，或者像机器学习模型那样的用户定制化的算子状态。 Flink用同样的方式处理所有的状态，无论是内置的还是用户自定义的算子。本节我们将会讨论Flink支持的不同类型的状态，并解释“状态后端”是如何存储和维护状态的。

一般来说，由一个任务维护，并且用来计算某个结果的所有数据，都属于这个任务的状态。你可以认为状态就是一个本地变量，可以被任务的业务逻辑访问。图3-10显示了任务与其状态之间的交互。

![image-20200728094617963](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200728094617963.png)

任务会接收一些输入数据。在处理数据时，任务可以读取和更新状态，并根据输入数据和状态计算结果。最简单的例子，就是统计接收到多少条数据的任务。当任务收到新数据时，它会访问状态以获取当前的计数，然后让计数递增，更新状态并发送出新的计数。

应用程序里，读取和写入状态的逻辑一般都很简单直接，而有效可靠的状态管理会复杂一些。这包括如何处理很大的状态——可能会超过内存，并且保证在发生故障时不会丢失任何状态。幸运的是，Flink会帮我们处理这相关的所有问题，包`括状态一致性、故障处理以及高效存储和访问`，以便开发人员可以专注于应用程序的逻辑。

在Flink中，状态始终与特定算子相关联。为了使运行时的Flink了解算子的状态，算子需要预先注册其状态。总的说来，有两种类型的状态：算子状态（operator state）和键控状态（keyed state），它们有着不同的范围访问，我们将在下面展开讨论。

#### 算子状态（Operator State）

算子状态的作用范围限定为算子任务。这意味着由同一并行任务所处理的所有数据都可以访问到相同的状态，状态对于同一任务而言是共享的。算子状态不能由相同或不同算子的另一个任务访问。图3-11显示了任务如何访问算子状态。

![image-20200728095415954](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200728095415954.png)

Flink为算子状态提供三种基本数据结构：

- **列表状态**（List State）：将状态表示为一组数据的列表
- **联合列表状态**（Union List State）：也将状态表示未数据的列表。它与常规列表状态的区别在于，发生故障时，或从保存点启动应用程序时如何恢复。
- **广播状态**（Broadcast State）：如果一个算子有多项任务，而它的每项任务状态又都相同，那么这种特殊情况最适合应用广播状态。在保存检查点和重新调整算子并行度时，会用到这个特性。这两部分内容将在本章后面讨论。

#### 键控状态（Keyed State）

顾名思义，键控状态是根据输入数据流中定义的键（key）来维护和访问的。Flink为每个键值维护一个状态实例，并将具有相同键的所有数据，都分区到同一个算子任务中，这个任务会维护和处理这个key对应的状态。当任务处理一条数据时，它会自动将状态的访问范围限定为当前数据的key。因此，具有相同key的所有数据都会访问相同的状态。图3-12显示了任务如何与键控状态进行交互。

![image-20200728100450576](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200728100450576.png)

我们可以将键控状态看成是在算子所有并行任务上，对键进行分区（或分片）之后的一个键值映射（key-value map）。 Flink为键控状态提供不同的数据结构，用于确定map中每个key存储的值的类型。我们简单了解一下最常见的键控状态。

##### 值状态（Value state）

为每个键存储一个任意类型的单个值。复杂数据结构也可以存储为值状态。

##### 列表状态（List state）

为每个键存储一个值的列表。列表里的每个数据可以是任意类型。

##### 映射状态（Map state）

为每个键存储一个键值映射（map）。map的key和value可以是任意类型。

状态的数据结构可以让Flink实现更有效的状态访问。我们将在“在运行时上下文（RuntimeContext）中声明键控状态”中做进一步讨论。

##### 聚合状态（ReducingState & Aggregating state）

将状态表示为一个用于聚合操作的列表

##### 代码演示

- 声明一个键控状态

  ```scala
  lazy val lastTemp: ValueState[Double] = getRuntimeContext.getState(
    new ValueStateDescriptor[Double]("lastTemp", Types.of[Double])
  )
  ```

- 读取状态

  ```scala
  val preTemp = lastTemp.value()
  ```

- 对状态赋值

  ```scala
  lastTemp.update(i.temperature)
  ```



#### 状态后端（State Backends）

每传入一条数据，**有状态的算子任务都会读取和更新状态**。由于有效的状态访问对于处理数据的低延迟至关重要，因此每个并行任务都会在本地维护其状态，以确保快速的状态访问。状态到底是如何被存储、访问以及维护的？这件事由一个可插入的组件决定，这个组件就叫做状态后端（state backend）。状态后端主要负责两件事：`本地的状态管理`，以及将`检查点（checkpoint）状态写入远程存储`。

对于本地状态管理，状态后端会存储所有键控状态，并确保所有的访问都被正确地限定在当前键范围。 Flink提供了默认的状态后端，会将键控状态作为内存中的对象进行管理，将它们存储在JVM堆上。另一种状态后端则会把状态对象进行序列化，并将它们放入RocksDB中，然后写入本地硬盘。第一种方式可以提供非常快速的状态访问，但它受内存大小的限制；而访问RocksDB状态后端存储的状态速度会较慢，但其状态可以增长到非常大。

状态检查点的写入也非常重要，这是因为Flink是一个分布式系统，而状态只能在本地维护。 TaskManager进程（所有任务在其上运行）可能在任何时间点挂掉。因此，它的本地存储只能被认为是不稳定的。状态后端负责将任务的状态检查点写入远程的持久存储。写入检查点的远程存储可以是分布式文件系统，也可以是数据库。不同的状态后端在状态检查点的写入机制方面有所不同。例如，RocksDB状态后端支持增量的检查点，这对于非常大的状态来说，可以显著减少状态检查点写入的开销。

我们将在“选择状态后端”一节中更详细地讨论不同的状态后端及其优缺点。

**选择一个状态后端**

* MemoryStateBackend 

  内存级的状态后端，将状态当作Java的对象(没有序列化操作)存储在TaskManager JVM进程的堆上。

  特点：快速、低延迟，但不稳定

* FsStateBackend

  将状态存储在本地的文件系统或者远程的文件系统如HDFS。

* RocksDBStateBackend

  将状态存储在RocksDB \footnote{Facebook开源的KV数据库} 中。

### 状态一致性

​		当在分布式系统中引入状态时，自然也引入了一致性问题。一致性实际上是"正确性级别"的另一种说法，也就是说在成功处理故障并恢复之后得到的结果，与没有发生任何故障时得到的结果相比，前者到底有多正确？举例来说，假设要对最近一小时登录的用户计数。在系统经历故障之后，计数结果是多少？如果有偏差，是有漏掉的计数还是重复计数？

#### 一致性级别

***AT-MOST-ONCE*** 最多一次

​		当任务故障时，最简单的做法是什么都不干，既不恢复丢失的状态，也不重播丢失的事件。At-most-once语义的含义是最多处理一次事件。换句话说，事件可以被丢弃掉，也没有任何操作来保证结果的准确性。这种类型的保证也叫“没有保证”，因为一个丢弃掉所有事件的系统其实也提供了这样的保障。没有保障听起来是一个糟糕的主意，但如果我们能接受近似的结果，并且希望尽可能低的延迟，那么这样也挺好。

***AT-LEAST-ONCE*** 至少一次

​		在大多数的真实应用场景，我们希望不丢失事件。这种类型的保障成为at-least-once，意思是所有的事件都得到了处理，而且一些事件还可能被处理多次。如果结果的正确性仅仅依赖于数据的完整性，那么重复处理是可以接受的。例如，判断一个事件是否在流中出现过，at-least-once这样的保证完全可以正确的实现。在最坏的情况下，我们多次遇到了这个事件。而如果我们要对一个特定的事件进行计数，计算结果就可能是错误的了。

​		为了保证在at-least-once语义的保证下，计算结果也能正确。我们还需要另一套系统来从数据源或者缓存中重新播放数据。持久化的事件日志系统将会把所有的事件写入到持久化存储中。所以如果任务发生故障，这些数据可以重新播放。还有一种方法可以获得同等的效果，就是使用结果承认机制。这种方法将会把每一条数据都保存在缓存中，直到数据的处理等到所有的任务的承认。一旦得到所有任务的承认，数据将被丢弃。

***EXACTLY-ONCE*** 精确一次

​		恰好处理一次是最严格的保证，也是最难实现的。恰好处理一次语义不仅仅意味着没有事件丢失，还意味着针对每一个数据，内部状态仅仅更新一次。本质上，恰好处理一次语义意味着我们的应用程序可以提供准确的结果，就好像从未发生过故障。

 		提供恰好处理一次语义的保证必须有至少处理一次语义的保证才行，同时还需要数据重放机制。另外，流处理器还需要保证内部状态的一致性。也就是说，在故障恢复以后，流处理器应该知道一个事件有没有在状态中更新。事务更新是达到这个目标的一种方法，但可能引入很大的性能问题。Flink使用了一种轻量级快照机制来保证恰好处理一次语义。

​		最先保证 exactly-once 的系统(Storm Trident 和 Spark Streaming)在性能和表现力这两个方面付出了很大的代价。为了保证 exactly-once，这些系统无法单独地对每条记录运用应用逻辑，而是同时处理多条(一批)记录，保证对每一批的处理要么全部成功，要么全部失败。这就导致在得到结果前，必须等待一批记录处理结束。因此，用户经常不得不使用两个流处理框架(一个用来保证 exactly-once，另一个用来对每个元素做低延迟处理)，结果使基础设施更加复杂。曾经，用户不得不在保证exactly-once 与获得低延迟和效率之间权衡利弊。Flink 避免了这种权衡。

Flink 的一个重大价值在于， 它既保证了 exactly-once ，也具有低延迟和高吞吐力 的处理能力。

从根本上说，Flink 通过使自身满足所有需求来避免权衡，它是业界的一次意义重大的技术飞跃。尽管这在外行看来很神奇，但是一旦了解，就会恍然大悟。

#### 端到端（end-to-end）状态一致性

​		目前我们看到的一致性保证都是由流处理器实现的，也就是说都是在 Flink 流处理器内部保证的；而在真实应用中，流处理应用除了流处理器以外还包含了数据源（例如 Kafka）和输出到持久化系统。
​		端到端的一致性保证，意味着结果的正确性贯穿了整个流处理应用的始终；每一个组件都保证了它自己的一致性，整个端到端的一致性级别取决于所有组件中一致性最弱的组件。具体可以划分如下：

- 内部保证 —— 依赖checkpoint

- source端 —— 需要外部源可重设数据的读取位置

- sink端 —— 需要保证从故障恢复时，数据不会重复写入外部系统

  而对于sink端，又有两种具体的实现方式：

  - `幂等写入`（Idempotent）

    所谓幂等操作，是说一个操作，可以重复执行很多次，但只导致一次结果更改，也就是说，后面再重复执行就不起作用了。

  - `事务写入`（Transactional）

    需要构建事务来写入外部系统，构建的事务对应着 checkpoint，等到 checkpoint真正完成的时候，才把所有对应的结果写入 sink 系统中。

    事务：应用程序中一系列严密的操作，所有操作必须成功完成，否则在每个操作中所做的所有更改都被撤销；具有原子性，一个事务中的一系列的操作要么全部成功，要么一个都不做

    具体又有两种实现方式：

    - **预写日志**（Write-Ahead-Log，WAL）

      把结果数据先当成状态保存，然后在收到checkpoint完成的通知时，一次性写入sink系统

      简单易于实现，由于数据提前在状态后端中做了缓存，所以无论什么sink系统，都能用这种方式一批搞定

      DataStream API提供了一个模板类：GenericWriteAheadSink，来实现这种事务性sink

    - **连接段提交**（2PC）

      对每个checkpoint，sink任务会启动一个事务，并将接下来所有接收的数据添加到事务里

      然后将这些数据写入外部sink系统，但不提交它们——这时只是”预提交“

      当它收到”checkpoint完成的通知时，它才正式提交事务，实现结果的真正写入

      > 这种方式真正实现了exactly-once，它需要一个提交事务支持的外部sink系统。Flink提供了TwoPhaseCommitSinkFunction接口。

      对外部sink系统的要求：

      - 外部sink系统必须提供事务支持，或者sink任务必须能够模拟外部系统上的事务
      - 在checkpoint的间隔期间里，必须能够开启一个事务并接受数据写入
      - 在收到checkpoint完成的通知之前，事务必须是“等待提交”的状态。在故障恢复的情况下，这可能需要一些时间。如果这个时候sink系统关闭事务（例如超时），那么未提交的数据就会丢失
      - sink任务必须能够在进行失败后恢复事务
      - 提交事务必须是幂等操作

|                | 不可重置的源 | 可重置的源                                               |
| -------------- | ------------ | -------------------------------------------------------- |
| any sink       | at-most-once | at-least-once                                            |
| 幂等性sink     | at-most-once | exactly-once（当从任务失败中恢复时，存在暂时的不一致性） |
| 预写式日志sink | at-most-once | at-least-once                                            |
| 2PC sink       | at-most-once | exactly-once                                             |

#### Flink+Kafka端到端状态一致性的保证

我们知道，端到端的状态一致性的实现，需要每一个组件都实现，对于 Flink + Kafka 的数据管道系统（Kafka 进、Kafka 出）而言，各组件怎样保证 exactly-once语义呢？

- 内部 —— 利用 checkpoint 机制，把状态存盘，发生故障的时候可以恢复，保证内部的状态一致性
- source —— kafka consumer 作为 source，可以将偏移量保存下来，如果后续任务出现了故障，恢复的时候可以由连接器重置偏移量，重新消费数据，保证一致性
- sink —— kafka producer 作为 sink，采用两阶段提交 sink，需要实现一个
  TwoPhaseCommitSinkFunction

内部的 checkpoint 机制我们已经有了了解，那 source 和 sink 具体又是怎样运行的呢？接下来我们逐步做一个分析。

我们知道 Flink 由 JobManager 协调各个 TaskManager 进行 checkpoint 存储，checkpoint 保存在 StateBackend 中，默认 StateBackend 是内存级的，也可以改为文件级的进行持久化保存。

![image-20200728172109876](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200728172109876.png)

当 checkpoint 启动时，JobManager 会将检查点分界线（barrier）注入数据流；barrier 会在算子间传递下去.

![image-20200728172208428](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200728172208428.png)

每个算子会对当前的状态做个快照，保存到状态后端。对于 source 任务而言，就会把当前的 offset 作为状态保存起来。下次从 checkpoint 恢复时，source 任务可以重新提交偏移量，从上次保存的位置开始重新消费数据。

![image-20200728172318319](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200728172318319.png)

每个内部的 transform 任务遇到 barrier 时，都会把状态存到 checkpoint 里。

sink 任务首先把数据写入外部 kafka，这些数据都属于预提交的事务（还不能被消费）；当遇到 barrier 时，把状态保存到状态后端，并开启新的预提交事务。

![image-20200728172417177](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200728172417177.png)

当所有算子任务的快照完成，也就是这次的 checkpoint 完成时，JobManager 会向所有任务发通知，确认这次 checkpoint 完成。

当 sink 任务收到确认通知，就会正式提交之前的事务，kafka 中未确认的数据就改为“已确认”，数据就真正可以被消费了。

![image-20200728175441025](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200728175441025.png)

所以我们看到，执行过程实际上是一个两段式提交，每个算子执行完成，会进行“预提交”，直到执行完 sink 操作，会发起“确认提交”，如果执行失败，预提交会放弃掉。

具体的两阶段提交步骤总结如下：

- 第一条数据来了之后，开启一个 kafka 的事务（transaction），正常写入kafka 分区日志但标记为未提交，这就是“预提交”

- jobmanager 触发 checkpoint 操作，barrier 从 source 开始向下传递，遇到barrier 的算子将状态存入状态后端，并通知 jobmanager

- sink 连接器收到 barrier，保存当前状态，存入 checkpoint，通知jobmanager，并开启下一阶段的事务，用于提交下个检查点的数据

- jobmanager 收到所有任务的通知，发出确认信息，表示 checkpoint 完成

- sink 任务收到 jobmanager 的确认信息，正式提交这段时间的数据

- 外部 kafka 关闭事务，提交的数据可以正常消费了。

所以我们也可以看到，如果宕机需要通过 StateBackend 进行恢复，只能恢复所有确认提交的操作。

### 容错机制

Flink是一个分布式数据处理系统，因此必须有一套机制处理各种故障，比如被杀掉的进程，故障的机器和中断的网络连接。任务都是在本地维护状态的，所以Flink必须确保状态不会丢失，并且在发生故障时能够保持一致。

在本节中，我们将介绍Flink的检查点（checkpoint）和恢复机制，这保证了“精确一次”（exactly-once）的状态一致性。我们还会讨论Flink独特的保存点（savepoint）功能，这是一个“瑞士军刀”式的工具，可以解决许多操作数据流时面对的问题。

#### 一致性检查点（Checkpoints）

Flink的恢复机制的核心，就是应用状态的一致检查点。有状态流应用的一致检查点，其实就是所有任务状态在某个时间点的一份拷贝，而这个时间点应该是所有任务都恰好处理完一个相同的输入数据的时候。这个过程可以通过一致检查点的一个简单算法步骤来解释。这个算法的步骤是：

* 暂停所有输入流的摄取，也就是不再接收新数据的输入。
* 等待所有正在处理的数据计算完毕，这意味着结束时，所有任务都已经处理了所有输入数据。
* 通过将每个任务的状态复制到远程持久存储，来得到一个检查点。所有任务完成拷贝操作后，检查点就完成了。
* 恢复所有输入流的摄取。

需要注意，Flink实现的并不是这种简单的机制。我们将在本节后面介绍Flink更精妙的检查点算法。

图3-17显示了一个简单应用中的一致检查点。

![image-20200728111703049](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200728111703049.png)

上面的应用程序中具有单一的输入源（source）任务，输入数据就是一组不断增长的数字的流——1,2,3等。数字流被划分为偶数流和奇数流。求和算子（sum）的两个任务会分别实时计算当前所有偶数和奇数的总和。源任务会将其输入流的当前偏移量存储为状态，而求和任务则将当前的总和值存储为状态。在图3-17中，Flink在输入偏移量为5时，将检查点写入了远程存储，当前的总和为6和9。

#### 故障恢复

在执行流应用程序期间，Flink会定期检查状态的一致检查点。如果发生故障，Flink将会使用最近的检查点来一致恢复应用程序的状态，并重新启动处理流程。图3-18显示了恢复过程。

![spaf_0318](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/spaf_0318.png)

应用程序从检查点的恢复分为三步：

* 重新启动整个应用程序。
* 将所有的有状态任务的状态重置为最近一次的检查点。
* 恢复所有任务的处理。

这种检查点的保存和恢复机制可以为应用程序状态提供“精确一次”（exactly-once）的一致性，因为所有算子都会保存检查点并恢复其所有状态，这样一来所有的输入流就都会被重置到检查点完成时的位置。至于数据源是否可以重置它的输入流，这取决于其实现方式和消费流数据的外部接口。例如，像Apache Kafka这样的事件日志系统可以提供流上之前偏移位置的数据，所以我们可以将源重置到之前的偏移量，重新消费数据。而从套接字（socket）消费数据的流就不能被重置了，因为套接字的数据一旦被消费就会丢弃掉。因此，对于应用程序而言，只有当所有的输入流消费的都是可重置的数据源时，才能确保在“精确一次”的状态一致性下运行。

从检查点重新启动应用程序后，其内部状态与检查点完成时的状态完全相同。然后它就会开始消费并处理检查点和发生故障之间的所有数据。尽管这意味着Flink会对一些数据处理两次（在故障之前和之后），我们仍然可以说这个机制实现了精确一次的一致性语义，因为所有算子的状态都已被重置，而重置后的状态下还不曾看到这些数据。

我们必须指出，Flink的检查点保存和恢复机制仅仅可以重置流应用程序的内部状态。对于应用中的一些的输出（sink）算子，在恢复期间，某些结果数据可能会多次发送到下游系统，比如事件日志、文件系统或数据库。对于某些存储系统，Flink提供了具有精确一次输出功​​能的sink函数，比如，可以在检查点完成时提交发出的记录。另一种适用于许多存储系统的方法是幂等更新。在“应用程序一致性保证”一节中，我们还会详细讨论如何解决应用程序端到端的精确一次一致性问题。

#### 检查点算法

Flink的恢复机制，基于它的一致性检查点。前面我们已经了解了从流应用中创建检查点的简单方法——先暂停应用，保存检查点，然后再恢复应用程序，这种方法很好理解，但它的理念是**“停止一切”**，这对于即使是中等延迟要求的应用程序而言也是不实用的。所以Flink没有这么简单粗暴，而是`基于Chandy-Lamport算法实现了分布式快照的检查点保存`。该算法并不会暂停整个应用程序，而是将检查点的保存与数据处理分离，这样就可以实现在其它任务做检查点状态保存状态时，让某些任务继续进行而不受影响。接下来我们将解释此算法的工作原理。

Flink的检查点算法用到了一种称为“检查点分界线”（checkpoint barrier）的特殊数据形式。与水位线（watermark）类似，检查点分界线由source算子注入到常规的数据流中，它的位置是限定好的，不能超过其他数据，也不能被后面的数据超过。检查点分界线带有检查点ID，用来标识它所属的检查点；这样，这个分界线就将一条流逻辑上分成了两部分。分界线之前到来的数据导致的状态更改，都会被包含在当前分界线所属的检查点中；而基于分界线之后的数据导致的所有更改，就会被包含在之后的检查点中。

我们用一个简单的流应用程序作为示例，来一步一步解释这个算法。该应用程序有两个源（source）任务，每个任务都消费一个增长的数字流。源任务的输出被划分为两部分：偶数和奇数的流。每个分区由一个任务处理，该任务计算所有收到的数字的总和，并将更新的总和转发给输出（sink）任务。这个应用程序的结构如图3-19所示。

![](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/spaf_0319.png){ width=100% }

JobManager会向每个数据源（source）任务发送一条带有新检查点ID的消息，通过这种方式来启动检查点，如图3-20所示。

![](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/spaf_0320.png){ width=100% }

当source任务收到消息时，它会暂停发出新的数据，在状态后端触发本地状态的检查点保存，并向所有传出的流分区广播带着检查点ID的分界线（barriers）。状态后端在状态检查点完成后会通知任务，而任务会向JobManager确认检查点完成。在发出所有分界线后，source任务就可以继续常规操作，发出新的数据了。通过将分界线注入到输出流中，源函数（source function）定义了检查点在流中所处的位置。图3-21显示了两个源任务将本地状态保存到检查点，并发出检查点分界线之后的流应用程序。

![](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/spaf_0321.png){ width=100% }

源任务发出的检查点分界线（barrier），将被传递给所连接的任务。与水位线（watermark）类似，barrier会被广播到所有连接的并行任务，以确保每个任务从它的每个输入流中都能接收到。当任务收到一个新检查点的barrier时，它会等待这个检查点的所有输入分区的barrier到达。在等待的过程中，任务并不会闲着，而是会继续处理尚未提供barrier的流分区中的数据。对于那些barrier已经到达的分区，如果继续有新的数据到达，它们就不会被立即处理，而是先缓存起来。这个等待所有分界线到达的过程，称为“分界线对齐”（barrier alignment），如图3-22所示。

![](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/spaf_0322.png){ width=100% }

当任务从所有输入分区都收到barrier时，它就会在状态后端启动一个检查点的保存，并继续向所有下游连接的任务广播检查点分界线，如图3-23所示。

![](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/spaf_0323.png){ width=100% }

所有的检查点barrier都发出后，任务就开始处理之前缓冲的数据。在处理并发出所有缓冲数据之后，任务就可以继续正常处理输入流了。图3-24显示了此时的应用程序。

![](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/spaf_0324.png){ width=100% }

最终，检查点分界线会到达输出（sink）任务。当sink任务接收到barrier时，它也会先执行“分界线对齐”，然后将自己的状态保存到检查点，并向JobManager确认已接收到barrier。一旦从应用程序的所有任务收到一个检查点的确认信息，JobManager就会将这个检查点记录为已完成。图3-25显示了检查点算法的最后一步。这样，当发生故障时，我们就可以用已完成的检查点恢复应用程序了。

![](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/spaf_0325.png){ width=100% }

#### 检查点的性能影响

Flink的检查点算法可以在不停止整个应用程序的情况下，生成一致的分布式检查点。但是，它可能会增加应用程序的处理延迟。Flink对此有一些调整措施，可以在某些场景下显得对性能的影响没那么大。

当任务将其状态保存到检查点时，它其实处于一个阻塞状态，而此时新的输入会被缓存起来。由于状态可能变得非常大，而且检查点需要通过网络将数据写入远程存储系统，检查点的写入很容易就会花费几秒到几分钟的时间——这对于要求低延迟的应用程序而言，显然是不可接受的。在Flink的设计中，真正负责执行检查点写入的，其实是状态后端。具体怎样复制任务的状态，取决于状态后端的实现方式。例如，文件系统（FileSystem）状态后端和RocksDB状态后端都支持了异步（asynchronous）检查点。触发检查点操作时，状态后端会先创建状态的本地副本。本地拷贝完成后，任务就将继续常规的数据处理，这往往并不会花费太多时间。一个后台线程会将本地快照异步复制到远程存储，并在完成检查点后再回来通知任务。异步检查点的机制，显著减少了任务继续处理数据之前的等待时间。此外，RocksDB状态后端还实现了增量的检查点，这样可以大大减少要传输的数据量。

为了减少检查点算法对处理延迟的影响，另一种技术是调整分界线对齐的步骤。对于需要非常低的延迟、并且可以容忍“至少一次”（at-least-once）状态保证的应用程序，Flink可以将检查点算法配置为，在等待barrier对齐期间处理所有到达的数据，而不是把barrier已经到达的那些分区的数据缓存起来。当检查点的所有barrier到达，算子任务就会将状态写入检查点——当然，现在的状态中，就可能包括了一些“提前”的更改，这些更改由本该属于下一个检查点的数据到来时触发。如果发生故障，从检查点恢复时，就将再次处理这些数据：这意味着检查点现在提供的是“至少一次”（at-least-once）而不是“精确一次”（exactly-once）的一致性保证。

#### 保存点（Savepoints）

Flink的恢复算法是基于状态检查点的。Flink根据可配置的策略，定期保存并自动丢弃检查点。检查点的目的是确保在发生故障时可以重新启动应用程序，所以当应用程序被显式地撤销（cancel）时，检查点会被删除掉。除此之外，应用程序状态的一致性快照还可用于除故障恢复之外的更多功能。

Flink中一个最有价值，也是最独特的功能是保存点（savepoints）。原则上，创建保存点使用的算法与检查点完全相同，因此保存点可以认为就是具有一些额外元数据的检查点。 Flink不会自动创建保存点，因此用户（或者外部调度程序）必须明确地触发创建操作。同样，Flink也不会自动清理保存点。第10章将会具体介绍如何触发和处理保存点。

#### 使用保存点

有了应用程序和与之兼容的保存点，我们就可以从保存点启动应用程序了。这会将应用程序的状态初始化为保存点的状态，并从保存点创建时的状态开始运行应用程序。虽然看起来这种行为似乎与用检查点从故障中恢复应用程序完全相同，但实际上故障恢复只是一种特殊情况，它只是在相同的集群上以相同的配置启动相同的应用程序。而从保存点启动应用程序会更加灵活，这就可以让我们做更多事情了。

* 可以从保存点启动不同但兼容的应用程序。这样一来，我们就可以及时修复应用程序中的逻辑bug，并让流式应用的源尽可能多地提供之前发生的事件，然后重新处理，以便修复之前的计算结果。修改后的应用程序还可用于运行A / B测试，或者具有不同业务逻辑的假设场景。这里要注意，应用程序和保存点必须兼容才可以这么做——也就是说，应用程序必须能够加载保存点的状态。
* 可以使用不同的并行度来启动相同的应用程序，可以将应用程序的并行度增大或减小。
* 可以在不同的集群上启动同样的应用程序。这非常有意义，意味着我们可以将应用程序迁移到较新的Flink版本或不同的集群上去。
* 可以使用保存点暂停应用程序，稍后再恢复。这样做的意义在于，可以为更高优先级的应用程序释放集群资源，或者在输入数据不连续生成时释放集群资源。
* 还可以将保存点设置为某一版本，并归档（archive）存储应用程序的状态。

保存点是非常强大的功能，所以许多用户会定期创建保存点以便能够及时退回之前的状态。我们见到的各种场景中，保存点一个最有趣的应用是不断将流应用程序迁移到更便宜的数据中心上去。

#### 从保存点启动应用程序

前面提到的保存点的所有用例，都遵循相同的模式。那就是首先创建正在运行的应用程序的保存点，然后在一个新启动的应用程序中用它来恢复状态。之前我们已经知道，保存点的创建和检查点非常相似，而接下来我们就将介绍对于一个从保存点启动的应用程序，Flink如何初始化其状态。

应用程序由多个算子组成。每个算子可以定义一个或多个键控状态和算子状态。算子由一个或多个算子任务并行执行。因此，一个典型的应用程序会包含多个状态，这些状态分布在多个算子任务中，这些任务可以运行在不同的TaskManager进程上。

图3-26显示了一个具有三个算子的应用程序，每个算子执行两个算子任务。一个算子（OP-1）具有单一的算子状态（OS-1），而另一个算子（OP-2）具有两个键控状态（KS-1和KS-2）。当保存点创建时，会将所有任务的状态复制到持久化的存储位置。

保存点中的状态拷贝会以算子标识符（operator ID）和状态名称（state name）组织起来。算子ID和状态名称必须能够将保存点的状态数据，映射到一个正在启动的应用程序的算子状态。从保存点启动应用程序时，Flink会将保存点的数据重新分配给相应的算子任务。

>请注意，保存点不包含有关算子任务的信息。这是因为当应用程序以不同的并行度启动时，任务数量可能会更改。

如果我们要从保存点启动一个修改过的应用程序，那么保存点中的状态只能映射到符合标准的应用程序——它里面的算子必须具有相应的ID和状态名称。默认情况下，Flink会自动分配唯一的算子ID。然而，一个算子的ID，是基于它之前算子的ID确定性地生成的。因此，算子的ID会在其前序算子改变时改变，比如，当我们添加了新的或移除掉一个算子时，前序算子ID改变，当前算子ID就会变化。所以对于具有默认算子ID的应用程序而言，如果想在不丢失状态的前提下升级，就会受到极大的限制。因此，我们强烈建议在程序中为算子手动分配唯一ID，而不是依靠Flink的默认分配。我们将在“指定唯一的算子标识符”一节中详细说明如何分配算子标识符。

## Window

### window概念

![image-20200724163907851](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200724163907851.png)

streaming流式计算是一种被设计用于处理无限数据集的数据处理引擎，而无限数据集是指一种不断增长的本质上无限的数据集，而window是一种`切割无限数据为有限块进行处理的手段`。

Window是无限数据流处理的核心，Window将一个无限的stream拆分成有限大小的`buckets`桶，我们可以在这些桶上做计算操作。

### window类型

可以分为两大类

- 时间窗口（Time Window）——按照时间生成window
  - 滚动时间窗口（Tumbling）
  - 滑动时间窗口（Sliding）
  - 会话窗口（Session）
- 计数窗口（Count Window）——按照指定的数据条数生成一个window，与时间无关。
  - 滚动计数窗口
  - 滑动技术窗口

#### 滚动窗口（Tumbling Windows）

将数据依据固定的窗口长度对数据进行切片。

**特点**：`时间对齐，窗口长度固定，没有重叠。`

**适用场景**：做BI统计等（做每个时间段的聚合计算）

滚动窗口分配器将每个元素分配到一个指定窗口大小的窗口中，滚动窗口有一个固定的大小，并且不会出现重叠。例如：如果你指定了一个5分钟大小的滚动窗口，窗口的创建如下图所示：

![image-20200724164728993](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200724164728993.png)

#### 滑动窗口（Sliding Windows）

滑动窗口是固定窗口的更广义的一种形式，滑动窗口由固定的窗口长度和滑动间隔组成。

**特点**：`窗口长度固定，可以有重叠`

**使用场景**：`对近一个时间段内的统计`

滑动窗口分配器将元素分配到固定长度的窗口中，与滚动窗口类似，窗口的大小有窗口大小参数配置 ，另一个窗口滑动参数控制滑动窗口开始的频率。因此，滑动窗口如果滑动参数小于窗口大小的话，窗口是可以重叠的，在这种情况下元素会被分配到多个窗口中。

例如：你有10分钟的窗口和5分钟的滑动，那么每个窗口中5分钟的窗口里包含着上个10分钟产生的数据，如下图所示：

![image-20200724165622494](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200724165622494.png)



#### 会话窗口（Session Windows）

由一系列事件组合一个指定时间长度的timeout间隙组成，类似于web应用的session，也就是一段时间没有接收到新数据就会生成新的窗口。

**特点**：`时间无对齐`

session窗口分配器通过session活动来对元素进行分组，session窗口跟滚动窗口和滑动窗口相比，不会有重叠和固定的开始时间和结束时间的情况，相反，当它在一个固定的时间周期内不再收到元素，即非活动间隔产生，那个窗口就会关闭，一个session窗口通过一个session间隔来配置，这个session间隔定一个了非活动周期的长度，当这个非活跃周期产生，那么当前的session将关闭并且后续的元素将被分配到新的session窗口中去。

![image-20200724170448991](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200724170448991.png)

### Window API

#### 窗口分配器（window assigner）

- 可以使用.window()来定义一个窗口，然后基于这个window去做一些聚合或者其他处理操作。注意window()方法必须在keyBy之后才能用。

- Flink提供了更加简单的.timeWindow()和.countWindow()方法，用于定义时间窗口和计数窗口。

- window()方法接收的输入参数是一个WindowAssigner

- WindowAssigner负责将每条输入的数据分发到正确的window中

- Flink提供了通用的WindowAssigner

  - 滚动窗口（tumbling window）

  - 滑动窗口（sliding window）
  - 会话窗口（session window）
  - 全局窗口（global window）

#### 创建不同类型的窗口

```scala
滚动时间窗口：
	.timeWindow(Time.seconds(15))
滑动时间窗口：
	.timeWindow(Time.seconds(15), Time.seconds(5))
会话窗口
	.window(EventTimeSessionWindows.withGap(Time.minutes(10)))
滚动计数窗口
	.countWindow(5)
滑动计数窗口
	.countWindow(10,2)

时间间隔有：Time.milliseconds(x)，Time.seconds(x)，Time.minutes(x)

```

#### 窗口函数

window function定义了要对窗口中收集的数据做的计算操作。可以分为两类：

- 增量聚合函数（incremental aggregation functions）
  - 每条数据到来就进行计算，保持一个简单的状态
  - ReduceFunction， AggregateFunction
- 全窗口函数（full window functions）
  - 先把窗口所有数据收集起来，等到计算的时候会遍历所有数据
  - ProcessWindowFunction

#### 其他可选API

.trigger() ——触发器，定义window什么时候关闭，触发计算并输出结果

.evitor() ——移除器，定义移除某些数据的逻辑

.allowedLateness() ——允许处理迟到的数据

.SideOutputLateData() ——将迟到的数据放入侧输出流

.getSideOutput() ——获取侧输出流

![image-20200724174832111](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200724174832111.png)

#### 开窗实例

```scala
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.watermark.Watermark
import org.apache.flink.streaming.api.windowing.time.Time

object windowTest {
  def main(args: Array[String]): Unit ={
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    env.setStreamTimeCharacteristic( TimeCharacteristic.EventTime)

    val stream = env.socketTextStream("192.168.17.130", 7777)

    val dataStream = stream.map( data => {
      val dataArray = data.split(",")
      SensorReading( dataArray(0).trim, dataArray(1).trim.toLong, dataArray(2).trim.toDouble)
    })
//      .assignAscendingTimestamps(_.time * 1000)    // 如果是升序简单实现
//      .assignTimestampsAndWatermarks(new MyAssigner)   // 乱序实现1
      .assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor[SensorReading](Time.seconds(1)) {
      override def extractTimestamp(t: SensorReading): Long = t.time * 1000
    })   // 乱序实现2， 延迟1秒输出


    val minTempWindowStream = dataStream.map( data => (data.id, data.temperature) )
      .keyBy(_._1)
//      .timeWindow( Time.seconds(10))    // 滚动统计10秒内的最小温度
//      .window(SlidingEventTimeWindows.of(Time.seconds(10), Time.seconds(5), Time.hours(-8)))  // 滑动统计，每隔5秒输出10秒内的最小温度
      .timeWindow( Time.seconds(10), Time.seconds(5))    // 滑动统计，每隔5秒输出10秒内的最小温度
      .reduce( (data1, data2) => (data1._1, data1._2.min(data2._2)))    // 用reduce做增量聚合

    minTempWindowStream.print("min temp")

    dataStream.print("input data")

    env.execute()
  }
}

class MyAssigner() extends AssignerWithPeriodicWatermarks[SensorReading]{
  val bound = 60000
  var maxTs = Long.MinValue
  override def getCurrentWatermark: Watermark = new Watermark(maxTs - bound)

  override def extractTimestamp(t: SensorReading, l: Long): Long = {
    maxTs = maxTs.max(t.time * 1000)
    t.time  * 1000
  }
}
```

起始点计算：offset是时区偏置，默认为0

![image-20200727115340209](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200727115340209.png)

## 时间语义与Wartermark

### Flink中的时间语义

![image-20200724175045275](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200724175045275.png)

**Event Time**：事件创建的时间

**Ingestion Timer**： 数据进入Flink的时间

**Processing Time**：执行操作算子的本地系统时间，与机器相关

> 不同的时间语义有不同的应用场合
>
> 往往更关系事件时间（Event Time）

如下场景，适合用事件时间

![image-20200726160733921](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200726160733921.png)

### 代码中设置Event Time

可以直接在代码中，对执行环境调用setStreamTimeCharacteristic方法，设置流的时间特性

具体的时间，还需要从数据中提取时间戳（timestamp）

```scala
val env = StreamExecutionEnvironment.getExecutionEnviroment
// 从调用时刻开始给env创建的每一个stream追加时间特征
env.setStreamTimeCharacteristic(TimeCharacteristic.EventTIme)
```

### 乱序数据的影响

流处理从事件产生，到流经source，再到operator，中间是有一个过程和时间的，虽然大部分情况下，流到operator的数据都是按照事件产生的时间顺序来的，但是不排除由于网络、分布式等原因，导致乱序的产生，所谓乱序，就是指Flink接收到的事件的先后顺序不是严格按照事件的Event Time顺序排列的。

- 当Flink以Event Time模式处理数据流时，他会根据数据里的时间戳来处理基于时间的算子

- 由于网络、分布式等原因，会导致乱序数据的产生

- 乱序数据会让窗口计算不准确 

![image-20200726165900230](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200726165900230.png)

### 水位线（Watermark）

一旦出现乱序，如果只根据eventTime决定window的运行，就不能明确数据是否全部到位，但又不能无限期的等下去，此时必须要有个机制来保证一个特定的时间后，必须触发window去进行计算了，这个特别的机制，就是Watermark。

- watermark是一种衡量Event Time进展的机制
- watermark是用于处理乱序事件的，而正确的处理乱序事件，通常用watermark机制结合window来实现
- 数据流中的watermark用于表示timestamp小于watermark的数据，都已经到达了；因此，window的执行也是由watermark触发的
- watermark可以理解成一个延迟触发机制，我们可以设置watermark的延时时长t，每次系统会校验已经到达的数据中最大的maxEventTime，然后认定eventTime小于maxEventTime - t 的所有数据都已经到达，如果有窗口的停止时间等于maxEventTime - t，那么这个窗口被触发执行。

#### 特点

![image-20200726181025879](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200726181025879.png)

- 是一条特殊的数据记录
- 必须单调递增，以确保任务的事件时间时钟在向前推进，而不是在后退
- 与数据的时间戳相关

#### watermark的传递

![image-20200726182022078](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200726182022078.png)

#### watermark的引入

- Event Time的使用一定要指定数据源中的时间戳

对于乱序数据，最常见的引用方式如下：

```scala
dataStream.assignTimestampsAndWatermarks(
	new BoundedOutOfOrdernessTimestampExtractor[SensorReading](Time.milliseconds(1000)){
    override def extractTimestamp(element: SensorReading): Long = {
      element.timestamp * 1000
    }
  }
)
```

Event Time 的使用一定要**指定数据源中的时间戳**。否则程序无法知道事件的事件时间是什么(数据源里的数据没有时间戳的话， 就只能使用 Processing Time 了)。

我们看到上面的例子中创建了一个看起来有点复杂的类， 这个类实现的其实就是分配时间戳的接口。Flink 暴露了 TimestampAssigner 接口供我们实现，使我们可以自定义如何从事件数据中抽取时间戳。

- 对于排好序的数据，不需要延迟触发，只需要指定时间戳就够了

  dataStream.assignAscendingTimestamps(_.timestamp * 1000)

- Flink暴露了TimestampAssigner接口供我们实现，使我们可以自定义如何从事件数据中抽取时间戳和生成watermark

  dataStream.assignTimestampsAndWatermarks(new MyAssigner())

  - MyAssigner可以有两种类型，都继承自TimestampAssigner

    - AssignerWithPeriodicWatermarks

      周期性的生成watermark：系统会周期性的将watermark插入到流中

      默认周期是200毫秒，可以使用ExecutionConfig.setAutoWatermarkInterval()方法进行设置

      升序和前面乱序的处理BoundedOutOfOrderness，都是基于周期性watermark的

    - AssignerWithPunctuatedWatermarks

      没有时间周期规律，可打断的生成watermark

#### watermark的设定

在Flink中，watermark由应用程序开发人员生成，这通常需要对相应的领域有一定的了解

如果watermark设置的延迟太久，收到结果的速度可能就会很慢，解决办法是在水位线到达之前输出一个近似结果

### 案例

参考本文：开窗实例 <a href="####开窗实例">点击调转</a>



## ProcessFunction API

已经学习的`转换算子`是无法访问事件的时间戳信息和水位线信息的。而这在一些应用场景下，极为重要。例如MapFunction这样的map转换算子就无法访问时间戳或者当前事件的事件时间。

基于此，DataStream API提供了一系列的Low-Level转换算子。可以访问时间戳、watermark以及注册定时时间。还可以输出特定的一些事件，例如超时事件等。ProcessFunction用来构建时间驱动的应用以及实现自定义的业务逻辑（使用之前的window函数和转换算子无法实现）。例如，FlinkSQL就是使用ProcessFunction实现的。



Flink提供了8个ProcessFunction：

- ProcessFunction
- KeyedProcessFunction
- CoProcessFunction
- ProcessJoinFunction
- BroadcastProcessFunction
- KeyedBroadcastProcessFunction
- ProcessWindowFunction
- ProcessAllWindowFunction



### KeyedProcessFunction

用来操作KeyedStream。会处理流的每一个元素，输出为0个、1个或多个元素。所有的ProcessFunction都继承自RichFunction接口，所以都有open()、close()和getRuntimeContext()等方法。而KeyedProcessFunction[KEY, IN, OUT]还额外提供了两个方法：

-  processElement(v: IN, ctx: Context, out: Collector[OUT])，流中的每一个元素都会调用这个方法，调用结果将会放在Collector数据类型中输出。Context可以访问元素的时间戳，元素的key，以及TimerService时间服务。Context还可以将结果输出到别的流。
- onTimer(timestamp: Long, ctx: OnTimerContext, out: Collector[OUT])是一个回调函数。当自强注册的定时器触发时调用。参数timestamp为定时器所设定的触发的时间戳。Collector为输出结果的集合。OnTimerContext和processElement的Context参数一样，提供了上下文的一些信息，例如定时器触发的时间信息（事件时间或处理时间）



### TimerService 和 定时器(Timers)

Context 和 OnTimerContext所持有的TimerService对象拥有以下方法：

* `currentProcessingTime(): Long` 返回当前处理时间
* `currentWatermark(): Long` 返回当前水位线的时间戳
* `registerProcessingTimeTimer(timestamp: Long): Unit` 会注册当前key的processing time的timer。当processing time到达定时时间时，触发timer。
* `registerEventTimeTimer(timestamp: Long): Unit` 会注册当前key的event time timer。当水位线大于等于定时器注册的时间时，触发定时器执行回调函数。
* `deleteProcessingTimeTimer(timestamp: Long): Unit` 删除之前注册处理时间定时器。如果没有这个时间戳的定时器，则不执行。
* `deleteEventTimeTimer(timestamp: Long): Unit` 删除之前注册的事件时间定时器，如果没有此时间戳的定时器，则不执行。

当定时器timer触发时，执行回调函数onTimer()。processElement()方法和onTimer()方法是同步方法，这样可以避免并发访问和操作状态。

> 定时器timer只能在KeyedStream上面使用。

针对每一个key和timestamp，只能注册一个定期器。也就是说，每一个key可以注册多个定时器，但在每一个时间戳只能注册一个定时器。KeyedProcessFunction默认将所有定时器的时间戳放在一个优先队列中。在Flink做检查点操作时，定时器也会被保存到状态后端中。

举个例子说明KeyedProcessFunction如何操作KeyedStream。

下面的程序展示了如何监控温度传感器的温度值，如果温度值在5秒钟之内(processing time)连续上升，报警。

```scala
val warnings = readings
  // key by sensor id
  .keyBy(_.id)
  // apply ProcessFunction to monitor temperatures
  .process(new TempIncreaseAlertFunction)
```

看一下TempIncreaseAlertFunction如何实现, 程序中使用了ValueState这样一个状态变量, 后面会详细讲解。

```scala
class TempIncreaseAlertFunction() extends KeyedProcessFunction[String, SensorReading, String]{
  // 定义一个状态用来保存上一个数据的温度值
  lazy val lastTemp: ValueState[Double] = getRuntimeContext.getState(
    new ValueStateDescriptor[Double]("lastTemp", Types.of[Double])
  )

  // 定义一个状态，用来保存定时器的时间戳
  lazy val currentTimer: ValueState[Long] = getRuntimeContext.getState(
    new ValueStateDescriptor[Long]("currentTimer", Types.of[Long])
  )

  override def processElement(i: SensorReading, context: KeyedProcessFunction[String, SensorReading, String]#Context, collector: Collector[String]): Unit = {

    // 先取上一个温度值
    val preTemp = lastTemp.value()

    // 更新温度值
     lastTemp.update(i.temperature)

    val curTimerTs = currentTimer.value()

    // 判断温度变化
    if( i.temperature > preTemp && curTimerTs == 0){
      // 如果温度上升，则注册定时器
      val timerTs = context.timerService().currentProcessingTime() + 5000L
      context.timerService().registerProcessingTimeTimer(timerTs)
      currentTimer.update(timerTs)
    }else if( preTemp > i.temperature || preTemp == 0.0){
      // 如果温度下降或者是第一条数据，删除定时器并清空状态
      context.timerService().deleteProcessingTimeTimer(curTimerTs)
      currentTimer.clear()
    }
  }

  override def onTimer(timestamp: Long, ctx: KeyedProcessFunction[String, SensorReading, String]#OnTimerContext, out: Collector[String]): Unit = {
    // 输出告警信息
    out.collect("传感器id为：" + ctx.getCurrentKey + "温度上升")
    currentTimer.clear()
  }
}
```

### 将事件发送到侧输出(Emitting to Side Outputs)

大部分的DataStream API的算子的输出是单一输出，也就是某种数据类型的流。除了split算子，可以将一条流分成多条流，这些流的数据类型也都相同。process function的side outputs功能可以产生多条流，并且这些流的数据类型可以不一样。一个side output可以定义为OutputTag[X]对象，X是输出流的数据类型。process function可以通过Context对象发射一个事件到一个或者多个side outputs。

例子

```scala
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.ProcessFunction
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.util.Collector

object SideOutputTest {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    env.setStreamTimeCharacteristic( TimeCharacteristic.EventTime)

    val stream = env.socketTextStream("192.168.17.130", 7777)

    val dataStream = stream.map( data => {
      val dataArray = data.split(",")
      SensorReading( dataArray(0).trim, dataArray(1).trim.toLong, dataArray(2).trim.toDouble)
    })
      .assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor[SensorReading](Time.seconds(1)) {
        override def extractTimestamp(t: SensorReading): Long = t.time * 1000
      })   // 乱序实现， 延迟1秒输出

    val processedStream = dataStream
      .process(new FreezingAlert())

//    dataStream.print("input data")

    processedStream.print("processed data")   // 只能输出主流
    processedStream.getSideOutput(new OutputTag[String]("freezing alert")).print("alert data")   // 侧输出流

    env.execute()

  }
}

// 冰点报警，如果小于32F，输出报警信息到侧输出流
class FreezingAlert() extends ProcessFunction[SensorReading, SensorReading]{

  lazy val alertOutput: OutputTag[String] = new OutputTag[String]("freezing alert")

  override def processElement(i: SensorReading, context: ProcessFunction[SensorReading, SensorReading]#Context, collector: Collector[SensorReading]): Unit = {
    if(i.temperature < 32.0){
      context.output(alertOutput, "freezing alert for " + i.id)
    }else {
      collector.collect(i)
    }
  }
```

### CoProcessFunction

对于两条输入流，DataStream API提供了CoProcessFunction这样的low-level操作。CoProcessFunction提供了操作每一个输入流的方法: processElement1()和processElement2()。类似于ProcessFunction，这两种方法都通过Context对象来调用。这个Context对象可以访问事件数据，定时器时间戳，TimerService，以及side outputs。CoProcessFunction也提供了onTimer()回调函数。下面的例子展示了如何使用CoProcessFunction来合并两条流。

```scala
// ingest sensor stream
val readings: DataStream[SensorReading] = ...

// filter switches enable forwarding of readings
val filterSwitches: DataStream[(String, Long)] = env
  .fromCollection(Seq(
    ("sensor_2", 10 * 1000L),
    ("sensor_7", 60 * 1000L)
  ))

val forwardedReadings = readings
  // connect readings and switches
  .connect(filterSwitches)
  // key by sensor ids
  .keyBy(_.id, _._1)
  // apply filtering CoProcessFunction
  .process(new ReadingFilter)
```

```scala
class ReadingFilter
  extends CoProcessFunction[SensorReading,
    (String, Long), SensorReading] {
  // switch to enable forwarding
  // 传送数据的开关
  lazy val forwardingEnabled: ValueState[Boolean] = getRuntimeContext
    .getState(
      new ValueStateDescriptor[Boolean]("filterSwitch", Types.of[Boolean])
    )

  // hold timestamp of currently active disable timer
  lazy val disableTimer: ValueState[Long] = getRuntimeContext
    .getState(
      new ValueStateDescriptor[Long]("timer", Types.of[Long])
    )

  override def processElement1(reading: SensorReading,
                               ctx: CoProcessFunction[SensorReading,
                                (String, Long), SensorReading]#Context,
                               out: Collector[SensorReading]): Unit = {
    // check if we may forward the reading
    // 决定我们是否要将数据继续传下去
    if (forwardingEnabled.value()) {
      out.collect(reading)
    }
  }

  override def processElement2(switch: (String, Long),
                               ctx: CoProcessFunction[SensorReading,
                                (String, Long), SensorReading]#Context,
                               out: Collector[SensorReading]): Unit = {
    // enable reading forwarding
    // 允许继续传输数据
    forwardingEnabled.update(true)
    // set disable forward timer
    val timerTimestamp = ctx.timerService().currentProcessingTime()
     + switch._2
  
    val curTimerTimestamp = disableTimer.value()

    if (timerTimestamp > curTimerTimestamp) {
      // remove current timer and register new timer
      ctx.timerService().deleteProcessingTimeTimer(curTimerTimestamp)
      ctx.timerService().registerProcessingTimeTimer(timerTimestamp)
      disableTimer.update(timerTimestamp)
    }
  }

  override def onTimer(ts: Long,
                       ctx: CoProcessFunction[SensorReading,
                        (String, Long), SensorReading]#OnTimerContext,
                       out: Collector[SensorReading]): Unit = {
     // remove all state; forward switch will be false by default
     forwardingEnabled.clear()
     disableTimer.clear()
  }
}
```



# Table API与SQL

## 概念

Flink本身是批流统一的处理框架，所以Table API和SQL，就是批流统一的上层处理API。目前功能尚未完善，处于活跃的开发阶段。

Table API是一套内嵌在Java和Scala语言中的查询API，它允许我们以非常直观的方式，组合来自一些关系运算符的查询（比如select、filter和join）。而对于Flink SQL，就是直接可以在代码中写SQL，来实现一些查询（Query）操作。Flink的SQL支持，基于实现了SQL标准的Apache Calcite（Apache开源SQL解析工具）。

无论输入是批输入还是流式输入，在这两套API中，指定的查询都具有相同的语义，得到相同的结果。



为什么要推出Table APIs和SQL？

首先，来看下Flink 的编程模型。如下图所示（图片来源于官网），DataStream API 和 DataSet API 是分开的，但是对于应用开发者来说，为什么要关注这一点？对于相同的数据，批处理与流计算居然要写两套代码，简直不可思议。Table APIs和SQL的推出，实现了流处理和批处理的统一。

![image-20200729175122828](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200729175122828.png)

其次，降低了学习和使用门槛，基于 DataStream/DataSet APIs 的 Scala 或 Java 程序开发，对于BI/分析师来说，还是有一定门槛的，而SQL 则简单太多了。

## 依赖

从1.9开始，Flink 提供了两个 Table Planner 实现来执行 Table API 和 SQL 程序：Blink Planner 和 Old Planner，Old Planner 在1.9之前就已经存在了。 Planner 的作用主要是把关系型的操作翻译成可执行的、经过优化的 Flink 任务。两种 Planner 所使用的优化规则以及运行时类都不一样。 它们在支持的功能上也有些差异。

注意 对于生产环境，我们建议使用在1.11版本之后已经变成默认的Blink Planner。

所有的 Table API 和 SQL 的代码都在 `flink-table` 或者 `flink-table-blink` Maven artifacts 下。

下面是各个依赖：

- `flink-table-common`: 公共模块，比如自定义函数、格式等需要依赖的。
- `flink-table-api-java`: Table 和 SQL API，使用 Java 语言编写的，给纯 table 程序使用（还在早期开发阶段，不建议使用）
- `flink-table-api-scala`: Table 和 SQL API，使用 Scala 语言编写的，给纯 table 程序使用（还在早期开发阶段，不建议使用）
- `flink-table-api-java-bridge`: Table 和 SQL API 结合 DataStream/DataSet API 一起使用，给 Java 语言使用。
- `flink-table-api-scala-bridge`: Table 和 SQL API 结合 DataStream/DataSet API 一起使用，给 Scala 语言使用。
- `flink-table-planner`: table Planner 和运行时。这是在1.9之前 Flink 的唯一的 Planner，但是从1.11版本开始我们不推荐继续使用。
- `flink-table-planner-blink`: 新的 Blink Planner，从1.11版本开始成为默认的 Planner。
- `flink-table-runtime-blink`: 新的 Blink 运行时。
- `flink-table-uber`: 把上述模块以及 Old Planner 打包到一起，可以在大部分 Table & SQL API 场景下使用。打包到一起的 jar 文件 `flink-table-*.jar` 默认会直接放到 Flink 发行版的 `/lib` 目录下。
- `flink-table-uber-blink`: 把上述模块以及 Blink Planner 打包到一起，可以在大部分 Table & SQL API 场景下使用。打包到一起的 jar 文件 `flink-table-blink-*.jar` 默认会放到 Flink 发行版的 `/lib` 目录下。

关于如何使用 Old Planner 以及 Blink Planner，可以参考[通用API](# 通用API调用)。

### Table 程序依赖

取决于你使用的编程语言，选择 Java 或者 Scala API 来构建你的 Table API 和 SQL 程序：

```xml
<!-- Either... -->
<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-table-api-java-bridge_2.11</artifactId>
  <version>1.11.0</version>
  <scope>provided</scope>
</dependency>
<!-- or... -->
<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-table-api-scala-bridge_2.11</artifactId>
  <version>1.11.0</version>
  <scope>provided</scope>
</dependency>
```

除此之外，如果你想在 IDE 本地运行你的程序，你需要添加下面的模块，具体用哪个取决于你使用哪个 Planner：

```xml
<!-- Either... (for the old planner that was available before Flink 1.9) -->
<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-table-planner_2.11</artifactId>
  <version>1.11.0</version>
  <scope>provided</scope>
</dependency>
<!-- or.. (for the new Blink planner) -->
<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-table-planner-blink_2.11</artifactId>
  <version>1.11.0</version>
  <scope>provided</scope>
</dependency>
```

内部实现上，部分 table 相关的代码是用 Scala 实现的。所以，下面的依赖也需要添加到你的程序里，不管是批式还是流式的程序：

```xml
<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-streaming-scala_2.11</artifactId>
  <version>1.11.0</version>
  <scope>provided</scope>
</dependency>
```

### 扩展依赖

如果你想实现[自定义格式](https://www.bookstack.cn/read/flink-1.11.1-zh/93e9c0980fa678ae.md#define-a-tablefactory)来解析 Kafka 数据，或者[自定义函数](https://www.bookstack.cn/read/flink-1.11.1-zh/54e977ab4e349c47.md)，下面的依赖就足够了，编译出来的 jar 文件可以直接给 SQL Client 使用：

```xml
<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-table-common</artifactId>
  <version>1.11.0</version>
  <scope>provided</scope>
</dependency>
```

当前，本模块包含以下可以扩展的接口：

- `SerializationSchemaFactory`
- `DeserializationSchemaFactory`
- `ScalarFunction`
- `TableFunction`
- `AggregateFunction`

## 通用API调用

### 两种planner（old & blink）的区别

1. 批流统一：Blink将批处理作业，视为流式处理的特殊情况。所以，**blink不支持表和DataSet之间的转换**，批处理作业将不转换为DataSet应用程序，而是跟流处理一样，转换为DataStream程序来处理。
2. 因为批流统一，Blink planner不支持BatchTableSource，使用有界的StreamTableSource代替。
3. Blink planner只支持全新的目录，不支持已弃用的ExternalCatalog。
4. 旧planner和Blink planner的FilterableTableSource实现不兼容。旧的planner会把PlannerExpressions下推到filterableTableSource中，而blink planner则会把Expressions下推。
5. 基于字符串的键值配置选项仅适用于Blink planner，参考：**[配置](# 配置)**。
6. PlannerConfig在两个planner中的实现不同。
7. Blink planner会将多个sink优化在一个DAG中（仅在TableEnvironment上受支持，而在StreamTableEnvironment上不受支持）。而旧planner的优化总是将每一个sink放在一个新的DAG中，其中所有DAG彼此独立。
8. 旧的planner不支持目录统计，而Blink planner支持。

### Table API和SQL程序结构

Table API 和 SQL 的程序结构，与流式处理的程序结构类似；也可以近似地认为有这么几步：首先创建执行环境，然后定义source、transform和sink。

具体操作流程如下：

##### **java**

```java
// create a TableEnvironment for specific planner batch or streaming
TableEnvironment tableEnv = ...; // see "Create a TableEnvironment" section
// create a Table
tableEnv.connect(...).createTemporaryTable("table1");
// register an output Table
tableEnv.connect(...).createTemporaryTable("outputTable");
// create a Table object from a Table API query
Table tapiResult = tableEnv.from("table1").select(...);
// create a Table object from a SQL query
Table sqlResult  = tableEnv.sqlQuery("SELECT ... FROM table1 ... ");
// emit a Table API result Table to a TableSink, same for SQL result
TableResult tableResult = tapiResult.executeInsert("outputTable");
tableResult...
// execute
tableEnv.execute("java_job");
```

##### **scala**

```scala
val tableEnv = ...  // 创建表的执行环境

// 创建一张表，用于读取数据
tableEnv.connect(...).createTemporaryTable("inputTable")
// 注册一张表，用于把计算结果输出
tableEnv.connect(...).createTemporaryTable("outputTable")
// 通过 Table API 查询算子，得到一张结果表
val result = tableEnv.from("inputTable").select(...)
// 通过 SQL查询语句，得到一张结果表
val sqlResult  = tableEnv.sqlQuery("SELECT ... FROM inputTable ...")

// 将结果表写入输出表中
result.insertInto("outputTable")
// execute
tableEnv.execute("scala_job")
```

##### **python**

```python
# create a TableEnvironment for specific planner batch or streaming
table_env = ... # see "Create a TableEnvironment" section
# register a Table
table_env.connect(...).create_temporary_table("table1")
# register an output Table
table_env.connect(...).create_temporary_table("outputTable")
# create a Table from a Table API query
tapi_result = table_env.from_path("table1").select(...)
# create a Table from a SQL query
sql_result  = table_env.sql_query("SELECT ... FROM table1 ...")
# emit a Table API result Table to a TableSink, same for SQL result
table_result = tapi_result.execute_insert("outputTable")
table_result...
# execute
table_env.execute("python_job")
```

**注意：** Table API 和 SQL 查询可以很容易地集成并嵌入到 DataStream 或 DataSet 程序中。 请参阅[与 DataStream 和 DataSet API 结合](https://www.bookstack.cn/read/flink-1.11.1-zh/432a56ede104b490.md#integration-with-datastream-and-dataset-api) 章节了解如何将 DataSet 和 DataStream 与表之间的相互转化。

### 创建TableEnvironment

官网：https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/table/common.html

创建表环境最简单的方式，就是基于流处理执行环境，调create方法直接创建：

```scala
val tableEnv = StreamTableEnvironment.create(env)
```

`TableEnvironment` 是 Table API 和 SQL 的核心概念。它负责:

- 在内部的 catalog 中注册 `Table`
- 注册外部的 catalog
- 加载可插拔模块
- 执行 SQL 查询
- 注册自定义函数 （scalar、table 或 aggregation）
- 将 `DataStream` 或 `DataSet` 转换成 `Table`
- 持有对 `ExecutionEnvironment` 或 `StreamExecutionEnvironment` 的引用

在创建TableEnv的时候，可以多传入一个EnvironmentSettings或者TableConfig参数，可以用来配置TableEnvironment的一些特性。

`Table` 总是与特定的 `TableEnvironment` 绑定。不能在同一条查询中使用不同 TableEnvironment 中的表，例如，对它们进行 join 或 union 操作。

`TableEnvironment` 可以通过静态方法 `BatchTableEnvironment.create()` 或者 `StreamTableEnvironment.create()` 在 `StreamExecutionEnvironment` 或者 `ExecutionEnvironment` 中创建，`TableConfig` 是可选项。`TableConfig`可用于配置`TableEnvironment`或定制的查询优化和转换过程(参见 [查询优化](https://www.bookstack.cn/read/flink-1.11.1-zh/432a56ede104b490.md#query-optimization))。

##### **java**

```java
// **********************
// FLINK STREAMING QUERY
// **********************
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

EnvironmentSettings fsSettings = EnvironmentSettings.newInstance().useOldPlanner().inStreamingMode().build();
StreamExecutionEnvironment fsEnv = StreamExecutionEnvironment.getExecutionEnvironment();
StreamTableEnvironment fsTableEnv = StreamTableEnvironment.create(fsEnv, fsSettings);
// or TableEnvironment fsTableEnv = TableEnvironment.create(fsSettings);

// ******************
// FLINK BATCH QUERY
// ******************
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.table.api.bridge.java.BatchTableEnvironment;

ExecutionEnvironment fbEnv = ExecutionEnvironment.getExecutionEnvironment();
BatchTableEnvironment fbTableEnv = BatchTableEnvironment.create(fbEnv);

// **********************
// BLINK STREAMING QUERY
// **********************
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

StreamExecutionEnvironment bsEnv = StreamExecutionEnvironment.getExecutionEnvironment();
EnvironmentSettings bsSettings = EnvironmentSettings.newInstance().useBlinkPlanner().inStreamingMode().build();
StreamTableEnvironment bsTableEnv = StreamTableEnvironment.create(bsEnv, bsSettings);
// or TableEnvironment bsTableEnv = TableEnvironment.create(bsSettings);

// ******************
// BLINK BATCH QUERY
// ******************
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.TableEnvironment;

EnvironmentSettings bbSettings = EnvironmentSettings.newInstance().useBlinkPlanner().inBatchMode().build();
TableEnvironment bbTableEnv = TableEnvironment.create(bbSettings);
```

##### **scala**

```scala
// **********************
// FLINK STREAMING QUERY
// **********************
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.table.api.EnvironmentSettings
import org.apache.flink.table.api.bridge.scala.StreamTableEnvironment

val fsSettings = EnvironmentSettings.newInstance().useOldPlanner().inStreamingMode().build()
val fsEnv = StreamExecutionEnvironment.getExecutionEnvironment
val fsTableEnv = StreamTableEnvironment.create(fsEnv, fsSettings)
// or val fsTableEnv = TableEnvironment.create(fsSettings)

// ******************
// FLINK BATCH QUERY
// ******************
import org.apache.flink.api.scala.ExecutionEnvironment
import org.apache.flink.table.api.bridge.scala.BatchTableEnvironment

val fbEnv = ExecutionEnvironment.getExecutionEnvironment
val fbTableEnv = BatchTableEnvironment.create(fbEnv)

// **********************
// BLINK STREAMING QUERY
// **********************
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.table.api.EnvironmentSettings
import org.apache.flink.table.api.bridge.scala.StreamTableEnvironment

val bsEnv = StreamExecutionEnvironment.getExecutionEnvironment
val bsSettings = EnvironmentSettings.newInstance().useBlinkPlanner().inStreamingMode().build()
val bsTableEnv = StreamTableEnvironment.create(bsEnv, bsSettings)
// or val bsTableEnv = TableEnvironment.create(bsSettings)

// ******************
// BLINK BATCH QUERY
// ******************
import org.apache.flink.table.api.{EnvironmentSettings, TableEnvironment}

val bbSettings = EnvironmentSettings.newInstance().useBlinkPlanner().inBatchMode().build()
val bbTableEnv = TableEnvironment.create(bbSettings)
```

##### **python**

```python
# **********************
# FLINK STREAMING QUERY
# **********************
from pyflink.datastream import StreamExecutionEnvironment
from pyflink.table import StreamTableEnvironment, EnvironmentSettings
f_s_env = StreamExecutionEnvironment.get_execution_environment()
f_s_settings = EnvironmentSettings.new_instance().use_old_planner().in_streaming_mode().build()
f_s_t_env = StreamTableEnvironment.create(f_s_env, environment_settings=f_s_settings)
# ******************
# FLINK BATCH QUERY
# ******************
from pyflink.dataset import ExecutionEnvironment
from pyflink.table import BatchTableEnvironment
f_b_env = ExecutionEnvironment.get_execution_environment()
f_b_t_env = BatchTableEnvironment.create(f_b_env, table_config)
# **********************
# BLINK STREAMING QUERY
# **********************
from pyflink.datastream import StreamExecutionEnvironment
from pyflink.table import StreamTableEnvironment, EnvironmentSettings
b_s_env = StreamExecutionEnvironment.get_execution_environment()
b_s_settings = EnvironmentSettings.new_instance().use_blink_planner().in_streaming_mode().build()
b_s_t_env = StreamTableEnvironment.create(b_s_env, environment_settings=b_s_settings)
# ******************
# BLINK BATCH QUERY
# ******************
from pyflink.table import EnvironmentSettings, BatchTableEnvironment
b_b_settings = EnvironmentSettings.new_instance().use_blink_planner().in_batch_mode().build()
b_b_t_env = BatchTableEnvironment.create(environment_settings=b_b_settings)
```



### 在Catalog中创建表

#### 表（Table）的概念

TableEnvironment可以注册目录Catalog，并可以基于Catalog注册表。它会维护一个Catalog-Table表之间的map。

表（Table）是由一个“标识符”来指定的，由3部分组成：Catalog名、数据库（database）名和对象名（表名）。如果没有指定目录或数据库，就使用当前的默认值。

表可以是常规的（Table，表），或者虚拟的（View，视图）。常规表（Table）一般可以用来描述外部数据，比如文件、数据库表或消息队列的数据，也可以直接从 DataStream转换而来。视图可以从现有的表中创建，通常是table API或者SQL查询的一个结果。

#### 临时表（Temporary Table）和永久表（Permanent Table）

表可以是临时的，并与单个 Flink 会话（session）的生命周期相关，也可以是永久的，并且在多个 Flink 会话和群集（cluster）中可见。

永久表需要 [catalog](https://www.bookstack.cn/read/flink-1.11.1-zh/3487c386592ca538.md)（例如 Hive Metastore）以维护表的元数据。一旦永久表被创建，它将对任何连接到 catalog 的 Flink 会话可见且持续存在，直至被明确删除。

另一方面，临时表通常保存于内存中并且仅在创建它们的 Flink 会话持续期间存在。这些表对于其它会话是不可见的。它们不与任何 catalog 或者数据库绑定但可以在一个命名空间（namespace）中创建。即使它们对应的数据库被删除，临时表也不会被删除。

##### 屏蔽（Shadowing）

可以使用与已存在的永久表相同的标识符去注册临时表。临时表会屏蔽永久表，并且只要临时表存在，永久表就无法访问。所有使用该标识符的查询都将作用于临时表。

这可能对实验（experimentation）有用。它允许先对一个临时表进行完全相同的查询，例如只有一个子集的数据，或者数据是不确定的。一旦验证了查询的正确性，就可以对实际的生产表进行查询。

#### 创建表

##### 虚拟表

在 SQL 的术语中，Table API 的对象对应于`视图`（虚拟表）。它封装了一个逻辑查询计划。它可以通过以下方法在 catalog 中创建：

**java**

```java
// get a TableEnvironment
TableEnvironment tableEnv = ...; // see "Create a TableEnvironment" section
// table is the result of a simple projection query 
Table projTable = tableEnv.from("X").select(...);
// register the Table projTable as table "projectedTable"
tableEnv.createTemporaryView("projectedTable", projTable);
```

**scala**

```scala
// get a TableEnvironment
val tableEnv = ... // see "Create a TableEnvironment" section
// table is the result of a simple projection query 
val projTable: Table = tableEnv.from("X").select(...)
// register the Table projTable as table "projectedTable"
tableEnv.createTemporaryView("projectedTable", projTable)
```

**python**

```python
# get a TableEnvironment
table_env = ... # see "Create a TableEnvironment" section
# table is the result of a simple projection query 
proj_table = table_env.from_path("X").select(...)
# register the Table projTable as table "projectedTable"
table_env.register_table("projectedTable", proj_table)
```

**注意：** 从传统数据库系统的角度来看，`Table` 对象与 `VIEW` 视图非常像。也就是，定义了 `Table` 的查询是没有被优化的， 而且会被内嵌到另一个引用了这个注册了的 `Table`的查询中。如果多个查询都引用了同一个注册了的`Table`，那么它会被内嵌每个查询中并被执行多次， 也就是说注册了的`Table`的结果**不会**被共享（注：Blink 计划器的`TableEnvironment`会优化成只执行一次）。



##### Connector Tables

另外一个方式去创建 `TABLE` 是通过 [connector](http://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/table/connect.html) 声明。Connector 描述了存储表数据的外部系统。存储系统例如 Apache Kafka 或者常规的文件系统都可以通过这种方式来声明。

TableEnvironment可以调用.connect()方法，连接外部系统，并调用.createTemporaryTable()方法在Catalog中注册表

**python**

```python
table_environment \
    .connect(...) \
    .with_format(...) \
    .with_schema(...) \
    .in_append_mode() \
    .create_temporary_table("MyTable")
```

**java** 和 **scala**

```scala
tableEnv
  .connect()  // 定义表数据来源，外部连接
  .withFormat()    // 定义从外部系统读取数据之后的格式化方法
  .withSchema()    // 定义表结构
  .createTemporaryTable("myTable")    // 创建临时表
```

```java
tableEnvironment.executeSql("CREATE [TEMPORARY] TABLE MyTable (...) WITH (...)")
```

#### 扩展表标识符

表总是通过三元标识符注册，包括 catalog 名、数据库名和表名。

用户可以指定一个 catalog 和数据库作为 “当前catalog” 和”当前数据库”。有了这些，那么刚刚提到的三元标识符的前两个部分就可以被省略了。如果前两部分的标识符没有指定， 那么会使用当前的 catalog 和当前数据库。用户也可以通过 Table API 或 SQL 切换当前的 catalog 和当前的数据库。

标识符遵循 SQL 标准，因此使用时需要用反引号（`` `）进行转义。

**java**

```java
TableEnvironment tEnv = ...;
tEnv.useCatalog("custom_catalog");
tEnv.useDatabase("custom_database");
Table table = ...;
// register the view named 'exampleView' in the catalog named 'custom_catalog'
// in the database named 'custom_database' 
tableEnv.createTemporaryView("exampleView", table);
// register the view named 'exampleView' in the catalog named 'custom_catalog'
// in the database named 'other_database' 
tableEnv.createTemporaryView("other_database.exampleView", table);
// register the view named 'example.View' in the catalog named 'custom_catalog'
// in the database named 'custom_database' 
tableEnv.createTemporaryView("`example.View`", table);
// register the view named 'exampleView' in the catalog named 'other_catalog'
// in the database named 'other_database' 
tableEnv.createTemporaryView("other_catalog.other_database.exampleView", table);
```

**scala**

```scala
// get a TableEnvironment
val tEnv: TableEnvironment = ...;
tEnv.useCatalog("custom_catalog")
tEnv.useDatabase("custom_database")
val table: Table = ...;
// register the view named 'exampleView' in the catalog named 'custom_catalog'
// in the database named 'custom_database' 
tableEnv.createTemporaryView("exampleView", table)
// register the view named 'exampleView' in the catalog named 'custom_catalog'
// in the database named 'other_database' 
tableEnv.createTemporaryView("other_database.exampleView", table)
// register the view named 'example.View' in the catalog named 'custom_catalog'
// in the database named 'custom_database' 
tableEnv.createTemporaryView("`example.View`", table)
// register the view named 'exampleView' in the catalog named 'other_catalog'
// in the database named 'other_database' 
tableEnv.createTemporaryView("other_catalog.other_database.exampleView", table)
```



### 表格式

#### CSV Format

这是旧版本的csv格式描述器。由于它是非标的，跟外部系统对接并不通用，所以将被弃用，以后会被一个符合RFC-4180标准的新format描述器取代。新的描述器就叫Csv()，但flink没有直接提供，需要引入依赖flink-csv：

```{.xml}
<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-csv</artifactId>
  <version>1.10.0</version>
</dependency>
```

代码非常类似，只需要把withFormat里的OldCsv改成Csv就可以了。

CSV格式旨在符合Internet工程任务组（IETF）提出的RFC-4180（“逗号分隔值（CSV）文件的通用格式和MIME类型”）。

该格式允许读取和写入与给定格式模式对应的CSV数据。 格式结构可以定义为Flink类型，也可以从所需的表结构派生。

如果格式模式等于表结构，则也可以自动派生该结构。 这仅允许定义一次结构信息。 格式的名称，类型和字段的顺序由表的结构确定。 如果时间属性的来源不是字段，则将忽略它们。 表模式中的from定义被解释为以该格式重命名的字段。

CSV格式可以如下使用：

```java
.withFormat(
  new Csv()

    .fieldDelimiter(';')         // optional: field delimiter character (',' by default)
    .lineDelimiter("\r\n")       // optional: line delimiter ("\n" by default;
                                 //   otherwise "\r", "\r\n", or "" are allowed)
    .disableQuoteCharacter()     // optional: disabled quote character for enclosing field values;
                                 //   cannot define a quote character and disabled quote character at the same time
    .quoteCharacter('\'')        // optional: quote character for enclosing field values ('"' by default)
    .allowComments()             // optional: ignores comment lines that start with '#' (disabled by default);
                                 //   if enabled, make sure to also ignore parse errors to allow empty rows
    .ignoreParseErrors()         // optional: skip fields and rows with parse errors instead of failing;
                                 //   fields are set to null in case of errors
    .arrayElementDelimiter("|")  // optional: the array element delimiter string for separating
                                 //   array and row element values (";" by default)
    .escapeCharacter('\\')       // optional: escape character for escaping values (disabled by default)
    .nullLiteral("n/a")          // optional: null literal string that is interpreted as a
                                 //   null value (disabled by default)
)
```

下表列出了可以读取和写入的受支持类型：

| Supported Flink SQL Types |
| ------------------------- |
| `ROW`                     |
| `VARCHAR`                 |
| `ARRAY[_]`                |
| `INT`                     |
| `BIGINT`                  |
| `FLOAT`                   |
| `DOUBLE`                  |
| `BOOLEAN`                 |
| `DATE`                    |
| `TIME`                    |
| `TIMESTAMP`               |
| `DECIMAL`                 |
| `NULL` (unsupported yet)  |

**Numeric types:** 值应该是数字，但字面量“ null”也可以理解。 空字符串被视为null。 值也被修剪（开头/结尾随空白）。 数字是使用Java的valueOf语义解析的。 其他非数字字符串可能会导致解析异常。

**String and time types:** 值未修剪。 文字“ null”也可以理解。 时间类型必须根据Java SQL时间格式进行格式化，且精度为毫秒。 例如：日期为2018-01-01，时间为20:43:59，时间戳为2018-01-01 20：43：59.999。

**Boolean type:** 值应为布尔值（“ true”，“ false”）字符串或“ null”。 空字符串被解释为false。 值被修剪（开头/结尾随空白）。 其他值导致异常。

**Nested types:** 使用数组元素定界符可以为一级嵌套支持数组和行类型。 

**Primitive byte arrays:** 基本字节数组以Base64编码表示形式处理。

**Line endings:** 对于行末未引号的字符串字段，即使对于基于行的连接器（如Kafka）也应忽略行尾



#### JSON Format

需要引入依赖flink-json：

```{.xml}
<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-json</artifactId>
  <version>1.10.0</version>
</dependency>
```

JSON格式允许读取和写入与给定格式结构相对应的JSON数据。 格式结构可以定义为Flink类型，JSON 结构或从所需的表结构派生。 Flink类型启用了更类似于SQL的定义并映射到相应的SQL数据类型。 JSON 格式允许更复杂和嵌套的结构。

如果格式结构等于表结构，则也可以自动派生该结构。 这仅允许定义一次结构信息。 格式的名称，类型和字段的顺序由表的结构确定。 如果时间属性的来源不是字段，则将忽略它们。 表结构中的from定义被解释为以该格式重命名的字段。 

JSON格式可以如下使用：

```java
.withFormat(
  new Json()
    .failOnMissingField(true)   // optional: flag whether to fail if a field is missing or not, false by default
    .ignoreParseErrors(true)    // optional: skip fields and rows with parse errors instead of failing;
                                //   fields are set to null in case of errors
    // deprecated: define the schema explicitly using JSON schema which parses to DECIMAL and TIMESTAMP.
    .jsonSchema(
      "{" +
      "  type: 'object'," +
      "  properties: {" +
      "    lon: {" +
      "      type: 'number'" +
      "    }," +
      "    rideTime: {" +
      "      type: 'string'," +
      "      format: 'date-time'" +
      "    }" +
      "  }" +
      "}"
    )
)
```

下表显示了JSON模式类型到Flink SQL类型的映射：

| JSON schema                       | Flink SQL                |
| --------------------------------- | ------------------------ |
| `object`                          | `ROW`                    |
| `boolean`                         | `BOOLEAN`                |
| `array`                           | `ARRAY[_]`               |
| `number`                          | `DECIMAL`                |
| `integer`                         | `DECIMAL`                |
| `string`                          | `VARCHAR`                |
| `string` with `format: date-time` | `TIMESTAMP`              |
| `string` with `format: date`      | `DATE`                   |
| `string` with `format: time`      | `TIME`                   |
| `string` with `encoding: base64`  | `ARRAY[TINYINT]`         |
| `null`                            | `NULL` (unsupported yet) |

当前，Flink仅支持JSON模式规范draft-07的子集。 尚不支持联合类型（以及allOf，anyOf和not）。 仅支持oneOf和类型数组用于指定可为空性。

#### 旧的CSV格式

旧的CSV格式允许使用文件系统连接器读取和写入以逗号分隔的行。

此格式描述了Flink的非标准CSV表源/接收器。 将来，该格式将被适当的RFC兼容版本取代。 写入Kafka时，请使用符合RFC的CSV格式。 现在，将旧版本用于流/批处理文件系统操作。

```java
.withFormat(
  new OldCsv()
    .fieldDelimiter(",")              // optional: string delimiter "," by default
    .lineDelimiter("\n")              // optional: string delimiter "\n" by default
    .quoteCharacter('"')              // optional: single character for string values, empty by default
    .commentPrefix('#')               // optional: string to indicate comments, empty by default
    .ignoreFirstLine()                // optional: ignore the first line, by default it is not skipped
    .ignoreParseErrors()              // optional: skip records with parse error instead of failing by default
)
```

旧的CSV格式包含在Flink中，不需要其他依赖项。

注意：目前，用于写入行的旧CSV格式受到限制。 仅支持将自定义字段定界符作为可选参数。



### 查询表

利用外部系统的连接器connector，我们可以读写数据，并在环境的Catalog中注册表。接下来就可以对表做查询转换了。

Flink给我们提供了两种查询方式：Table API和 SQL。

#### table   API

Table API 是关于 Scala 和 Java 的集成语言式查询 API。与 SQL 相反，Table API 的查询不是由字符串指定，而是在宿主语言中逐步构建。

Table API 是基于 `Table` 类的，该类表示一个表（流或批处理），并提供使用关系操作的方法。这些方法返回一个新的 Table 对象，该对象表示对输入 Table 进行关系操作的结果。 一些关系操作由多个方法调用组成，例如 `table.groupBy(...).select()`，其中 `groupBy(...)` 指定 `table` 的分组，而 `select(...)` 在 `table` 分组上的投影。

文档 [Table API](# Table-API) 说明了所有流处理和批处理表支持的 Table API 算子。

以下示例展示了一个简单的 Table API 聚合查询：

代码中的实现如下：

```{.scala}
val sensorTable: Table = tableEnv.from("inputTable")

val resultTable: Table = senorTable
  .select("id, temperature")
  .filter("id ='sensor_1'")
```

#### SQL查询

Flink的SQL集成，基于的是Apache Calcite，它实现了SQL标准。在Flink中，用常规字符串来定义SQL查询语句。SQL 查询的结果，是一个新的 Table。

代码实现如下：

```{.scala}
val resultSqlTable: Table = tableEnv
  .sqlQuery("select id, temperature from inputTable where id ='sensor_1'")
```

或者：

```{.scala}
val resultSqlTable: Table = tableEnv.sqlQuery(
  """
    |select id, temperature
    |from inputTable
    |where id = 'sensor_1'
  """.stripMargin)
```

当然，也可以加上聚合操作，比如我们统计每个sensor温度数据出现的个数，做个count统计：

```{.scala}
val aggResultTable = sensorTable
  .groupBy('id)
  .select('id, 'id.count as 'count)
```

SQL的实现：

```{.scala}
val aggResultSqlTable = tableEnv
  .sqlQuery("select id, count(id) as cnt from inputTable group by id")
```

这里Table API里指定的字段，前面加了一个单引号`'`，这是Table API中定义的Expression类型的写法，可以很方便地表示一个表中的字段。

字段可以直接全部用双引号引起来，也可以用半边单引号+字段名的方式。以后的代码中，一般都用后一种形式。

如下的示例展示了如何指定一个更新查询，将查询的结果插入到已注册的表中。

```java
// get a TableEnvironment
TableEnvironment tableEnv = ...; // see "Create a TableEnvironment" section

// register "Orders" table
// register "RevenueFrance" output table

// compute revenue for all customers from France and emit to "RevenueFrance"
tableEnv.executeSql(
    "INSERT INTO RevenueFrance " +
    "SELECT cID, cName, SUM(revenue) AS revSum " +
    "FROM Orders " +
    "WHERE cCountry = 'FRANCE' " +
    "GROUP BY cID, cName"
  );
```

#### 混用 Table API 和 SQL

Table API 和 SQL 查询的混用非常简单因为它们都返回 Table 对象：

- 可以在 SQL 查询返回的 Table 对象上定义 Table API 查询。
- 在 TableEnvironment 中注册的结果表可以在 SQL 查询的 FROM 子句中引用，通过这种方法就可以在 Table API 查询的结果上定义 SQL 查询。

#### 将DataStream转换成表

Flink允许我们把Table和DataStream做转换：我们可以基于一个DataStream，先流式地读取数据源，然后map成样例类，再把它转成Table。Table的列字段（column fields），就是样例类里的字段，这样就不用再麻烦地定义schema了。

#### 代码表达

代码中实现非常简单，直接用tableEnv.fromDataStream()就可以了。默认转换后的 Table schema 和 DataStream 中的字段定义一一对应，也可以单独指定出来。

这就允许我们更换字段的顺序、重命名，或者只选取某些字段出来，相当于做了一次map操作（或者Table API的 select操作）。

代码具体如下：

```{.scala}
val inputStream: DataStream[String] = env.readTextFile("sensor.txt")
val dataStream: DataStream[SensorReading] = inputStream
  .map(data => {
    val dataArray = data.split(",")
    SensorReading(dataArray(0), dataArray(1).toLong, dataArray(2).toDouble)
  })

val sensorTable: Table = tableEnv.fromDataStream(dataStream)

val sensorTable2 = tableEnv.fromDataStream(dataStream, 'id, 'timestamp as 'ts)
```

#### 数据类型与Table schema的对应

在上节的例子中，DataStream 中的数据类型，与表的 Schema 之间的对应关系，是按照样例类中的字段名来对应的（name-based mapping），所以还可以用as做重命名。

另外一种对应方式是，直接按照字段的位置来对应（position-based mapping），对应的过程中，就可以直接指定新的字段名了。

基于名称的对应：

```{.scala}
val sensorTable = tableEnv
  .fromDataStream(dataStream, 'timestamp as 'ts, 'id as 'myId, 'temperature)
```

基于位置的对应：

```{.scala}
val sensorTable = tableEnv
  .fromDataStream(dataStream, 'myId, 'ts)
```

Flink的DataStream和 DataSet API支持多种类型。

组合类型，比如元组（内置Scala和Java元组）、POJO、Scala case类和Flink的Row类型等，允许具有多个字段的嵌套数据结构，这些字段可以在Table的表达式中访问。其他类型，则被视为原子类型。

元组类型和原子类型，一般用位置对应会好一些；如果非要用名称对应，也是可以的：

元组类型，默认的名称是 “_1”, “_2”；而原子类型，默认名称是 ”f0”。

### 创建临时视图（Temporary View）

创建临时视图的第一种方式，就是直接从DataStream转换而来。同样，可以直接对应字段转换；也可以在转换的时候，指定相应的字段。

代码如下：

```{.scala}
tableEnv.createTemporaryView("sensorView", dataStream)
tableEnv.createTemporaryView("sensorView",
  dataStream, 'id, 'temperature, 'timestamp as 'ts)
```

另外，当然还可以基于Table创建视图：

```{.scala}
tableEnv.createTemporaryView("sensorView", sensorTable)
```

View和Table的Schema完全相同。事实上，在Table API中，可以认为View和Table是等价的。

### 输出

官网：https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/table/connect.html

Table 通过写入 TableSink 输出。TableSink 是一个通用接口，用于支持多种文件格式（如 CSV、Apache Parquet、Apache Avro）、存储系统（如 JDBC、Apache HBase、Apache Cassandra、Elasticsearch）或消息队列系统（如 Apache Kafka、RabbitMQ）。

批处理 Table 只能写入 BatchTableSink，而流处理 Table 需要指定写入 AppendStreamTableSink，RetractStreamTableSink 或者 UpsertStreamTableSink。

请参考文档 Table Sources & Sinks 以获取更多关于可用 Sink 的信息以及如何自定义 TableSink。

方法 Table.executeInsert(String tableName) 将 Table 发送至已注册的 TableSink。该方法通过名称在 catalog 中查找 TableSink 并确认Table schema 和 TableSink schema 一致。

具体实现，输出表最直接的方法，就是通过 Table.insertInto() 方法将一个 Table 写入注册过的 TableSink 中。

#### 输出到文件

代码如下：

```{.scala}
// 注册输出表
tableEnv.connect(
    new FileSystem().path("…\\resources\\out.txt")
  ) // 定义到文件系统的连接
  .withFormat(new Csv()) // 定义格式化方法，Csv格式
  .withSchema(
    new Schema()
      .field("id", DataTypes.STRING())
      .field("temp", DataTypes.DOUBLE())
  ) // 定义表结构
  .createTemporaryTable("outputTable") // 创建临时表

resultSqlTable.insertInto("outputTable")
```

#### 更新模式（Update Mode）

在流处理过程中，表的处理并不像传统定义的那样简单。

对于流式查询（Streaming Queries），需要声明如何在（动态）表和外部连接器之间执行转换。与外部系统交换的消息类型，由更新模式（update mode）指定。

Flink Table API中的更新模式有以下三种：

1. 追加模式（Append Mode）

在追加模式下，表（动态表）和外部连接器只交换插入（Insert）消息。

2. 撤回模式（Retract Mode）

在撤回模式下，表和外部连接器交换的是：添加（Add）和撤回（Retract）消息。

* 插入（Insert）会被编码为添加消息；
* 删除（Delete）则编码为撤回消息；
* 更新（Update）则会编码为已更新行（上一行）的撤回消息，和更新行（新行）的添加消息。

在此模式下，不能定义key，这一点跟upsert模式完全不同。

3. Upsert（更新插入）模式

在Upsert模式下，动态表和外部连接器交换Upsert和Delete消息。

这个模式需要一个唯一的key，通过这个key可以传递更新消息。为了正确应用消息，外部连接器需要知道这个唯一key的属性。

* 插入（Insert）和更新（Update）都被编码为Upsert消息；
* 删除（Delete）编码为Delete信息。

这种模式和Retract模式的主要区别在于，Update操作是用单个消息编码的，所以效率会更高。

#### 输出到Kafka

除了输出到文件，也可以输出到Kafka。我们可以结合前面Kafka作为输入数据，构建数据管道，kafka进，kafka出。

代码如下：

```{.scala}
// 输出到 kafka
tableEnv.connect(
  new Kafka()
    .version("0.11")
    .topic("sinkTest")
    .property("zookeeper.connect", "localhost:2181")
    .property("bootstrap.servers", "localhost:9092")
  )
  .withFormat(new Csv())
  .withSchema(
    new Schema()
      .field("id", DataTypes.STRING())
      .field("temp", DataTypes.DOUBLE())
  )
  .createTemporaryTable("kafkaOutputTable")
  
resultTable.insertInto("kafkaOutputTable")
```

#### 输出到ElasticSearch

ElasticSearch的connector可以在upsert（update+insert，更新插入）模式下操作，这样就可以使用Query定义的键（key）与外部系统交换UPSERT/DELETE消息。

另外，对于“仅追加”（append-only）的查询，connector还可以在append 模式下操作，这样就可以与外部系统只交换insert消息。

es目前支持的数据格式，只有Json，而flink本身并没有对应的支持，所以还需要引入依赖：

```{.xml}
<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-json</artifactId>
  <version>1.10.0</version>
</dependency>
```

代码实现如下：

```{.scala}
// 输出到es
tableEnv.connect(
  new Elasticsearch()
    .version("6")
    .host("localhost", 9200, "http")
    .index("sensor")
    .documentType("temp")
  )
  .inUpsertMode()           // 指定是 Upsert 模式
  .withFormat(new Json())
  .withSchema(
    new Schema()
      .field("id", DataTypes.STRING())
      .field("count", DataTypes.BIGINT())
  )
  .createTemporaryTable("esOutputTable")
  
aggResultTable.insertInto("esOutputTable")
```

#### 输出到MySql

Flink专门为Table API的jdbc连接提供了flink-jdbc连接器，我们需要先引入依赖：

```{.xml}
<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-jdbc_2.11</artifactId>
  <version>1.10.0</version>
</dependency>
```

jdbc连接的代码实现比较特殊，因为没有对应的java/scala类实现ConnectorDescriptor，所以不能直接tableEnv.connect()。不过Flink SQL留下了执行DDL的接口：tableEnv.sqlUpdate()。

对于jdbc的创建表操作，天生就适合直接写DDL来实现，所以我们的代码可以这样写：

```{.scala}
// 输出到 Mysql
val sinkDDL: String =
  """
    |create table jdbcOutputTable (
    |  id varchar(20) not null,
    |  cnt bigint not null
    |) with (
    |  'connector.type' = 'jdbc',
    |  'connector.url' = 'jdbc:mysql://localhost:3306/test',
    |  'connector.table' = 'sensor_count',
    |  'connector.driver' = 'com.mysql.jdbc.Driver',
    |  'connector.username' = 'root',
    |  'connector.password' = '123456'
    |)
  """.stripMargin

tableEnv.sqlUpdate(sinkDDL)
aggResultSqlTable.insertInto("jdbcOutputTable")
```

#### 将表转换成DataStream

表可以转换为DataStream或DataSet。这样，自定义流处理或批处理程序就可以继续在 Table API或SQL查询的结果上运行了。

将表转换为DataStream或DataSet时，需要指定生成的数据类型，即要将表的每一行转换成的数据类型。通常，最方便的转换类型就是Row。当然，因为结果的所有字段类型都是明确的，我们也经常会用元组类型来表示。

表作为流式查询的结果，是动态更新的。所以，将这种动态查询转换成的数据流，同样需要对表的更新操作进行编码，进而有不同的转换模式。

Table API中表到DataStream有两种模式：

* 追加模式（Append Mode）

用于表只会被插入（Insert）操作更改的场景。

* 撤回模式（Retract Mode）

用于任何场景。有些类似于更新模式中Retract模式，它只有Insert和Delete两类操作。

得到的数据会增加一个Boolean类型的标识位（返回的第一个字段），用它来表示到底是新增的数据（Insert），还是被删除的数据（老数据，Delete）。

代码实现如下：

```{.scala}
val resultStream: DataStream[Row] = tableEnv
  .toAppendStream[Row](resultTable)

val aggResultStream: DataStream[(Boolean, (String, Long))] = tableEnv
  .toRetractStream[(String, Long)](aggResultTable)

resultStream.print("result")
aggResultStream.print("aggResult")
```

所以，没有经过groupby之类聚合操作，可以直接用toAppendStream来转换；而如果经过了聚合，有更新操作，一般就必须用toRetractDstream。

#### 将表转换成DataSet

scala

```scala
resultTable.toDataSet[Row].print();
或
tableEnv.toDataSet[Row]{resultTable}.print();
```

java

```java
DataSet<Row> result = tableEnv.toDataSet(resultTable, Row.class);
```



## 流式概念

### 概览

### 动态表（Dynamic Table）

### 时间属性

### Joins in Continuous Queries

### 时态表（Temporal Tables）

### 模式监测

### Query Configuration



## 数据类型

由于历史原因，在 Flink 1.9 之前，Flink Table & SQL API 的数据类型与 Flink 的 TypeInformation 耦合紧密。TypeInformation 在 DataStream 和 DataSet API 中被使用，并且足以用来用于描述分布式环境中 JVM 对象的序列化和反序列化操作所需的全部信息。

然而,TypeInformation 并不是为独立于 JVM class 的逻辑类型而设计的。之前很难将 SQL 的标准类型映射到 TypeInformation 抽象。此外，有一些类型并不是兼容 SQL 的并且引入的时候没有长远规划过。

从 Flink 1.9 开始，Table & SQL API 开始启用一种新的类型系统作为长期解决方案，用来保持 API 稳定性和 SQL 标准的兼容性。

重新设计类型系统是一项涉及几乎所有的面向用户接口的重大工作。因此，它的引入跨越多个版本，社区的目标是在 Flink 1.12 完成这项工作。

同时由于为 Table 编程添加了新的 Planner 详见（FLINK-11439）, 并不是每种 Planner 都支持所有的数据类型。此外,Planner 对于数据类型的精度和c完整的。

注意 在使用数据类型之前请参阅 Planner 的兼容性表和局限性章节。

数据类型 描述 Table 编程环境中的值的逻辑类型。它可以被用来声明操作的输入输出类型。

Flink 的数据类型和 SQL 标准的 数据类型 术语类似，但也包含了可空属性，可以被用于标量表达式（scalar expression）的优化。

数据类型的示例:

- INT
- INT NOT NULL
- INTERVAL DAY TO SECOND(3)
- ROW<myField ARRAY<BOOLEAN>, myOtherField TIMESTAMP(3)>

全部的预定义数据类型见下面列表。

### Table API 的数据类型

JVM API 的用户可以在 Table API 中使用 org.apache.flink.table.types.DataType 的实例，以及定义连接器（Connector）、Catalog 或者用户自定义函数（User-Defined Function）。

一个 DataType 实例有两个作用：

- 逻辑类型的声明，它不表达具体物理类型的存储和转换，但是定义了基于 JVM 的语言和 Table 编程环境之间的边界。
- 可选的： 向 Planner 提供有关数据的物理表示的提示，这对于边界 API 很有用。
  对于基于 JVM 的语言，所有预定义的数据类型都在 org.apache.flink.table.api.DataTypes 里提供。

建议使用星号将全部的 API 导入到 Table 程序中以便于使用：

```java
import static org.apache.flink.table.api.DataTypes.*;

DataType t = INTERVAL(DAY(), SECOND(3));
```

#### 物理提示

在 Table 编程环境中，基于 SQL 的类型系统与程序指定的数据类型之间需要物理提示。该提示指出了实现预期的数据格式。

例如，Data Source 能够使用类 java.sql.Timestamp 来表达逻辑上的 TIMESTAMP 产生的值，而不是使用缺省的 java.time.LocalDateTime。有了这些信息，运行时就能够将产生的类转换为其内部数据格式。反过来，Data Sink 可以声明它从运行时消费的数据格式。

下面是一些如何声明桥接转换类的示例：

```java
// tell the runtime to not produce or consume java.time.LocalDateTime instances
// but java.sql.Timestamp
DataType t = DataTypes.TIMESTAMP(3).bridgedTo(java.sql.Timestamp.class);

// tell the runtime to not produce or consume boxed integer arrays
// but primitive int arraysc
DataType t = DataTypes.ARRAY(DataTypes.INT().notNull()).bridgedTo(int[].class);
```

注意 请注意，通常只有在扩展 API 时才需要物理提示。 预定义的 Source、Sink、Function 的用户不需要定义这样的提示。在 Table 编程中（例如 field.cast(TIMESTAMP(3).bridgedTo(Timestamp.class))）这些提示将被忽略。

### Planner 兼容性

正如简介里提到的，重新开发类型系统将跨越多个版本，每个数据类型的支持取决于使用的 Planner。本节旨在总结最重要的差异。

#### 旧的 Planner

Flink 1.9 之前引入的旧的 Planner 主要支持类型信息（Type Information），它只对数据类型提供有限的支持，可以声明能够转换为类型信息的数据类型，以便旧的 Planner 能够理解它们。

下表总结了数据类型和类型信息之间的区别。大多数简单类型以及 Row 类型保持不变。Time 类型、 Array 类型和 Decimal 类型需要特别注意。不允许使用其他的类型提示。

对于 类型信息 列，该表省略了前缀 org.apache.flink.table.api.Types。

对于 数据类型表示 列，该表省略了前缀 org.apache.flink.table.api.DataTypes。

类型信息	Java 表达式字符串	数据类型表示	数据类型备注
![image-20200816122238470](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200816122238470.png)
注意 如果对于新的类型系统有任何疑问，用户可以随时切换到 org.apache.flink.table.api.Types 中定义的 type information。

#### 新的 Blink Planner

新的 Blink Planner 支持旧的 Planner 的全部类型，尤其包括列出的 Java 表达式字符串和类型信息。

支持以下数据类型：

| 数据类型                         | 数据类型的备注                       |
| :------------------------------- | :----------------------------------- |
| `STRING`                         | `CHAR` 和 `VARCHAR` 暂不支持。       |
| `BOOLEAN`                        |                                      |
| `BYTES`                          | `BINARY` 和 `VARBINARY` 暂不支持。   |
| `DECIMAL`                        | 支持固定精度和小数位数。             |
| `TINYINT`                        |                                      |
| `SMALLINT`                       |                                      |
| `INTEGER`                        |                                      |
| `BIGINT`                         |                                      |
| `FLOAT`                          |                                      |
| `DOUBLE`                         |                                      |
| `DATE`                           |                                      |
| `TIME`                           | 支持的精度仅为 `0`。                 |
| `TIMESTAMP`                      |                                      |
| `TIMESTAMP WITH LOCAL TIME ZONE` |                                      |
| `INTERVAL`                       | 仅支持 `MONTH` 和 `SECOND(3)` 区间。 |
| `ARRAY`                          |                                      |
| `MULTISET`                       |                                      |
| `MAP`                            |                                      |
| `ROW`                            |                                      |
| `RAW`                            |                                      |
| stuctured types                  | 暂只能在用户自定义函数里使用。       |

### 数据类型列表

本节列出了所有预定义的数据类型。对于基于 JVM 的 Table API，这些类型也可以从 `org.apache.flink.table.api.DataTypes` 中找到。

#### 字符串

##### CHAR

固定长度字符串的数据类型。

**声明**

```
CHARCHAR(n)
DataTypes.CHAR(n)
```

此类型用 `CHAR(n)` 声明，其中 `n` 表示字符数量。`n` 的值必须在 `1` 和 `2,147,483,647` 之间（含边界值）。如果未指定长度，`n` 等于 `1`。

**JVM 类型**

| Java 类型                                | 输入 | 输出 | 备注                  |
| :--------------------------------------- | :--- | :--- | :-------------------- |
| `java.lang.String`                       | X    | X    | *缺省*                |
| `byte[]`                                 | X    | X    | 假设使用 UTF-8 编码。 |
| `org.apache.flink.table.data.StringData` | X    | X    | 内部数据结构。        |

##### VARCHAR / STRING

可变长度字符串的数据类型。

**声明**

```
VARCHARVARCHAR(n)STRING
DataTypes.VARCHAR(n)DataTypes.STRING()
```

此类型用 `VARCHAR(n)` 声明，其中 `n` 表示最大的字符数量。`n` 的值必须在 `1` 和 `2,147,483,647` 之间（含边界值）。如果未指定长度，`n` 等于 `1`。

`STRING` 等价于 `VARCHAR(2147483647)`.

**JVM 类型**

| Java 类型                                | 输入 | 输出 | 备注                  |
| :--------------------------------------- | :--- | :--- | :-------------------- |
| `java.lang.String`                       | X    | X    | *缺省*                |
| `byte[]`                                 | X    | X    | 假设使用 UTF-8 编码。 |
| `org.apache.flink.table.data.StringData` | X    | X    | 内部数据结构。        |

#### 二进制字符串

##### BINARY

固定长度二进制字符串的数据类型（=字节序列）。

**声明**

```
BINARYBINARY(n)
DataTypes.BINARY(n)
```

此类型用 `BINARY(n)` 声明，其中 `n` 是字节数量。`n` 的值必须在 `1` 和 `2,147,483,647` 之间（含边界值）。如果未指定长度，`n` 等于 `1`。

**JVM 类型**

| Java 类型 | 输入 | 输出 | 备注   |
| :-------- | :--- | :--- | :----- |
| `byte[]`  | X    | X    | *缺省* |

##### VARBINARY / BYTES

可变长度二进制字符串的数据类型（=字节序列）。

**声明**

```
VARBINARYVARBINARY(n)BYTES
DataTypes.VARBINARY(n)DataTypes.BYTES()
```

此类型用 `VARBINARY(n)` 声明，其中 `n` 是最大的字节数量。`n` 的值必须在 `1` 和 `2,147,483,647` 之间（含边界值）。如果未指定长度，`n` 等于 `1`。

`BYTES` 等价于 `VARBINARY(2147483647)`。

**JVM 类型**

| Java 类型 | 输入 | 输出 | 备注   |
| :-------- | :--- | :--- | :----- |
| `byte[]`  | X    | X    | *缺省* |

#### 精确数值

##### DECIMAL

精度和小数位数固定的十进制数字的数据类型。

**声明**

```
DECIMALDECIMAL(p)DECIMAL(p, s)DECDEC(p)DEC(p, s)NUMERICNUMERIC(p)NUMERIC(p, s)
DataTypes.DECIMAL(p, s)
```

此类型用 `DECIMAL(p, s)` 声明，其中 `p` 是数字的位数（*精度*），`s` 是数字中小数点右边的位数（*尾数*）。`p` 的值必须介于 `1` 和 `38` 之间（含边界值）。`s` 的值必须介于 `0` 和 `p` 之间（含边界值）。其中 `p` 的缺省值是 `10`，`s` 的缺省值是 `0`。

`NUMERIC(p, s)` 和 `DEC(p, s)` 都等价于这个类型。

**JVM 类型**

| Java 类型                                 | 输入 | 输出 | 备注           |
| :---------------------------------------- | :--- | :--- | :------------- |
| `java.math.BigDecimal`                    | X    | X    | *缺省*         |
| `org.apache.flink.table.data.DecimalData` | X    | X    | 内部数据结构。 |

##### TINYINT

1 字节有符号整数的数据类型，其值从 `-128` to `127`。

**声明**

```
TINYINT
DataTypes.TINYINT()
```

**JVM 类型**

| Java 类型        | 输入 | 输出 | 备注                       |
| :--------------- | :--- | :--- | :------------------------- |
| `java.lang.Byte` | X    | X    | *缺省*                     |
| `byte`           | X    | (X)  | 仅当类型不可为空时才输出。 |

##### SMALLINT

2 字节有符号整数的数据类型，其值从 `-32,768` 到 `32,767`。

**声明**

```
SMALLINT
DataTypes.SMALLINT()
```

**JVM 类型**

| Java 类型         | 输入 | 输出 | 备注                       |
| :---------------- | :--- | :--- | :------------------------- |
| `java.lang.Short` | X    | X    | *缺省*                     |
| `short`           | X    | (X)  | 仅当类型不可为空时才输出。 |

##### INT

4 字节有符号整数的数据类型，其值从 `-2,147,483,648` 到 `2,147,483,647`。

**声明**

```
INTINTEGER
DataTypes.INT()
```

`INTEGER` 等价于此类型。

**JVM 类型**

| Java 类型           | 输入 | 输出 | 备注                       |
| :------------------ | :--- | :--- | :------------------------- |
| `java.lang.Integer` | X    | X    | *缺省*                     |
| `int`               | X    | (X)  | 仅当类型不可为空时才输出。 |

##### BIGINT

8 字节有符号整数的数据类型，其值从 `-9,223,372,036,854,775,808` 到 `9,223,372,036,854,775,807`。

**声明**

```
BIGINT
DataTypes.BIGINT()
```

**JVM 类型**

| Java 类型        | 输入 | 输出 | 备注                       |
| :--------------- | :--- | :--- | :------------------------- |
| `java.lang.Long` | X    | X    | *缺省*                     |
| `long`           | X    | (X)  | 仅当类型不可为空时才输出。 |

#### 近似数值

##### FLOAT

4 字节单精度浮点数的数据类型。

与 SQL 标准相比，该类型不带参数。

**声明**

```
FLOAT
DataTypes.FLOAT()
```

**JVM 类型**

| Java 类型         | 输入 | 输出 | 备注                       |
| :---------------- | :--- | :--- | :------------------------- |
| `java.lang.Float` | X    | X    | *缺省*                     |
| `float`           | X    | (X)  | 仅当类型不可为空时才输出。 |

##### DOUBLE

8 字节双精度浮点数的数据类型。

**声明**

```
DOUBLEDOUBLE PRECISION
DataTypes.DOUBLE()
```

`DOUBLE PRECISION` 等价于此类型。

**JVM 类型**

| Java 类型          | 输入 | 输出 | 备注                       |
| :----------------- | :--- | :--- | :------------------------- |
| `java.lang.Double` | X    | X    | *缺省*                     |
| `double`           | X    | (X)  | 仅当类型不可为空时才输出。 |

#### 日期和时间

##### DATE

日期的数据类型由 `year-month-day` 组成，范围从 `0000-01-01` 到 `9999-12-31`。

与 SQL 标准相比，年的范围从 `0000` 开始。

**声明**

```
DATE
DataTypes.DATE()
```

**JVM 类型**

| Java 类型             | 输入 | 输出 | 备注                                                 |
| :-------------------- | :--- | :--- | :--------------------------------------------------- |
| `java.time.LocalDate` | X    | X    | *缺省*                                               |
| `java.sql.Date`       | X    | X    |                                                      |
| `java.lang.Integer`   | X    | X    | 描述从 Epoch 算起的天数。                            |
| `int`                 | X    | (X)  | 描述从 Epoch 算起的天数。 仅当类型不可为空时才输出。 |

##### TIME

*不带*时区的时间数据类型，由 `hour:minute:second[.fractional]` 组成，精度达到纳秒，范围从 `00:00:00.000000000` 到 `23:59:59.999999999`。

与 SQL 标准相比，不支持闰秒（`23:59:60` 和 `23:59:61`），语义上更接近于 `java.time.LocalTime`。没有提供*带有*时区的时间。

**声明**

```
TIMETIME(p)
DataTypes.TIME(p)
```

此类型用 `TIME(p)` 声明，其中 `p` 是秒的小数部分的位数（*精度*）。`p` 的值必须介于 `0` 和 `9` 之间（含边界值）。如果未指定精度，则 `p` 等于 `0`。

**JVM 类型**

| Java 类型             | 输入 | 输出 | 备注                                                |
| :-------------------- | :--- | :--- | :-------------------------------------------------- |
| `java.time.LocalTime` | X    | X    | *缺省*                                              |
| `java.sql.Time`       | X    | X    |                                                     |
| `java.lang.Integer`   | X    | X    | 描述自当天以来的毫秒数。                            |
| `int`                 | X    | (X)  | 描述自当天以来的毫秒数。 仅当类型不可为空时才输出。 |
| `java.lang.Long`      | X    | X    | 描述自当天以来的纳秒数。                            |
| `long`                | X    | (X)  | 描述自当天以来的纳秒数。 仅当类型不可为空时才输出。 |

##### TIMESTAMP

*不带*时区的时间戳数据类型，由 `year-month-day hour:minute:second[.fractional]` 组成，精度达到纳秒，范围从 `0000-01-01 00:00:00.000000000` 到 `9999-12-31 23:59:59.999999999`。

与 SQL 标准相比，不支持闰秒（`23:59:60` 和 `23:59:61`），语义上更接近于 `java.time.LocalDateTime`。

不支持和 `BIGINT`（JVM `long` 类型）互相转换，因为这意味着有时区，然而此类型是无时区的。对于语义上更接近于 `java.time.Instant` 的需求请使用 `TIMESTAMP WITH LOCAL TIME ZONE`。

**声明**

```
TIMESTAMPTIMESTAMP(p)TIMESTAMP WITHOUT TIME ZONETIMESTAMP(p) WITHOUT TIME ZONE
DataTypes.TIMESTAMP(p)
```

此类型用 `TIMESTAMP(p)` 声明，其中 `p` 是秒的小数部分的位数（*精度*）。`p` 的值必须介于 `0` 和 `9` 之间（含边界值）。如果未指定精度，则 `p` 等于 `6`。

`TIMESTAMP(p) WITHOUT TIME ZONE` 等价于此类型。

**JVM 类型**

| Java 类型                                   | 输入 | 输出 | 备注           |
| :------------------------------------------ | :--- | :--- | :------------- |
| `java.time.LocalDateTime`                   | X    | X    | *缺省*         |
| `java.sql.Timestamp`                        | X    | X    |                |
| `org.apache.flink.table.data.TimestampData` | X    | X    | 内部数据结构。 |

##### TIMESTAMP WITH TIME ZONE

*带有*时区的时间戳数据类型，由 `year-month-day hour:minute:second[.fractional] zone` 组成，精度达到纳秒，范围从 `0000-01-01 00:00:00.000000000 +14:59` 到 `9999-12-31 23:59:59.999999999 -14:59`。

与 SQL 标准相比，不支持闰秒（`23:59:60` 和 `23:59:61`），语义上更接近于 `java.time.OffsetDateTime`。

与 `TIMESTAMP WITH LOCAL TIME ZONE` 相比，时区偏移信息物理存储在每个数据中。它单独用于每次计算、可视化或者与外部系统的通信。

**声明**

```
TIMESTAMP WITH TIME ZONETIMESTAMP(p) WITH TIME ZONE
DataTypes.TIMESTAMP_WITH_TIME_ZONE(p)
```

此类型用 `TIMESTAMP(p) WITH TIME ZONE` 声明，其中 `p` 是秒的小数部分的位数（*精度*）。`p` 的值必须介于 `0` 和 `9` 之间（含边界值）。如果未指定精度，则 `p` 等于 `6`。

**JVM 类型**

| Java 类型                  | 输入 | 输出 | 备注          |
| :------------------------- | :--- | :--- | :------------ |
| `java.time.OffsetDateTime` | X    | X    | *缺省*        |
| `java.time.ZonedDateTime`  | X    |      | 忽略时区 ID。 |

##### TIMESTAMP WITH LOCAL TIME ZONE

*带有本地*时区的时间戳数据类型，由 `year-month-day hour:minute:second[.fractional] zone` 组成，精度达到纳秒，范围从 `0000-01-01 00:00:00.000000000 +14:59` 到 `9999-12-31 23:59:59.999999999 -14:59`。

不支持闰秒（`23:59:60` 和 `23:59:61`），语义上更接近于 `java.time.OffsetDateTime`。

与 `TIMESTAMP WITH TIME ZONE` 相比，时区偏移信息并非物理存储在每个数据中。相反，此类型在 Table 编程环境的 UTC 时区中采用 `java.time.Instant` 语义。每个数据都在当前会话中配置的本地时区中进行解释，以便用于计算和可视化。

此类型允许根据配置的会话时区来解释 UTC 时间戳，从而填补了时区无关和时区相关的时间戳类型之间的鸿沟。

**声明**

```
TIMESTAMP WITH LOCAL TIME ZONETIMESTAMP(p) WITH LOCAL TIME ZONE
DataTypes.TIMESTAMP_WITH_LOCAL_TIME_ZONE(p)
```

此类型用 `TIMESTAMP(p) WITH LOCAL TIME ZONE` 声明，其中 `p` 是秒的小数部分的位数（*精度*）。`p` 的值必须介于 `0` 和 `9` 之间（含边界值）。如果未指定精度，则 `p` 等于 `6`。

**JVM 类型**

| Java 类型                                   | 输入 | 输出 | 备注                                                 |
| :------------------------------------------ | :--- | :--- | :--------------------------------------------------- |
| `java.time.Instant`                         | X    | X    | *缺省*                                               |
| `java.lang.Integer`                         | X    | X    | 描述从 Epoch 算起的秒数。                            |
| `int`                                       | X    | (X)  | 描述从 Epoch 算起的秒数。 仅当类型不可为空时才输出。 |
| `java.lang.Long`                            | X    | X    | 描述从 Epoch 算起的毫秒数。                          |
| `long`                                      | X    | (X)  | 描述从 Epoch 算起的毫秒数。 仅当类型不可为空时才输出 |
| `org.apache.flink.table.data.TimestampData` | X    | X    | 内部数据结构。                                       |

##### INTERVAL YEAR TO MONTH

一组 Year-Month Interval 数据类型。

此类型必被参数化为以下情况中的一种：

- Year 时间间隔、
- Year-Month 时间间隔、
- Month 时间间隔。

Year-Month Interval 由 `+years-months` 组成，其范围从 `-9999-11` 到 `+9999-11`。

所有类型的表达能力均相同。例如，Month 时间间隔下的 `50` 等价于 Year-Month 时间间隔（缺省年份精度）下的 `+04-02`。

**声明**

```
INTERVAL YEARINTERVAL YEAR(p)INTERVAL YEAR(p) TO MONTHINTERVAL MONTH
DataTypes.INTERVAL(DataTypes.YEAR())DataTypes.INTERVAL(DataTypes.YEAR(p))DataTypes.INTERVAL(DataTypes.YEAR(p), DataTypes.MONTH())DataTypes.INTERVAL(DataTypes.MONTH())
```

可以使用以上组合来声明类型，其中 `p` 是年数（*年精度*）的位数。`p` 的值必须介于 `1` 和 `4` 之间（含边界值）。如果未指定年精度，`p` 则等于 `2`。

**JVM 类型**

| Java 类型           | 输入 | 输出 | 备注                                      |
| :------------------ | :--- | :--- | :---------------------------------------- |
| `java.time.Period`  | X    | X    | 忽略 `days` 部分。 *缺省*                 |
| `java.lang.Integer` | X    | X    | 描述月的数量。                            |
| `int`               | X    | (X)  | 描述月的数量。 仅当类型不可为空时才输出。 |

##### INTERVAL DAY TO MONTH

一组 Day-Time Interval 数据类型。

此类型达到纳秒精度，必被参数化为以下情况中的一种：

- Day 时间间隔、
- Day-Hour 时间间隔、
- Day-Minute 时间间隔、
- Day-Second 时间间隔、
- Hour 时间间隔、
- Hour-Minute 时间间隔、
- Hour-Second 时间间隔、
- Minute 时间间隔、
- Minute-Second 时间间隔、
- Second 时间间隔。

Day-Time 时间间隔由 `+days hours:months:seconds.fractional` 组成，其范围从 `-999999 23:59:59.999999999` 到 `+999999 23:59:59.999999999`。

所有类型的表达能力均相同。例如，Second 时间间隔下的 `70` 等价于 Day-Second 时间间隔（缺省精度）下的 `+00 00:01:10.000000`。

**声明**

```
INTERVAL DAYINTERVAL DAY(p1)INTERVAL DAY(p1) TO HOURINTERVAL DAY(p1) TO MINUTEINTERVAL DAY(p1) TO SECOND(p2)INTERVAL HOURINTERVAL HOUR TO MINUTEINTERVAL HOUR TO SECOND(p2)INTERVAL MINUTEINTERVAL MINUTE TO SECOND(p2)INTERVAL SECONDINTERVAL SECOND(p2)
DataTypes.INTERVAL(DataTypes.DAY())DataTypes.INTERVAL(DataTypes.DAY(p1))DataTypes.INTERVAL(DataTypes.DAY(p1), DataTypes.HOUR())DataTypes.INTERVAL(DataTypes.DAY(p1), DataTypes.MINUTE())DataTypes.INTERVAL(DataTypes.DAY(p1), DataTypes.SECOND(p2))DataTypes.INTERVAL(DataTypes.HOUR())DataTypes.INTERVAL(DataTypes.HOUR(), DataTypes.MINUTE())DataTypes.INTERVAL(DataTypes.HOUR(), DataTypes.SECOND(p2))DataTypes.INTERVAL(DataTypes.MINUTE())DataTypes.INTERVAL(DataTypes.MINUTE(), DataTypes.SECOND(p2))DataTypes.INTERVAL(DataTypes.SECOND())DataTypes.INTERVAL(DataTypes.SECOND(p2))
```

可以使用以上组合来声明类型，其中 `p1` 是天数（*天精度*）的位数，`p2` 是秒的小数部分的位数（*小数精度*）。`p1` 的值必须介于 `1` 和之间 `6`（含边界值），`p2` 的值必须介于 `0` 和之间 `9`（含边界值）。如果 `p1` 未指定值，则缺省等于 `2`，如果 `p2` 未指定值，则缺省等于 `6`。

**JVM 类型**

| Java 类型            | 输入 | 输出 | 备注                                    |
| :------------------- | :--- | :--- | :-------------------------------------- |
| `java.time.Duration` | X    | X    | *缺省*                                  |
| `java.lang.Long`     | X    | X    | 描述毫秒数。                            |
| `long`               | X    | (X)  | 描述毫秒数。 仅当类型不可为空时才输出。 |

#### 结构化的数据类型

##### ARRAY

具有相同子类型元素的数组的数据类型。

与 SQL 标准相比，无法指定数组的最大长度，而是被固定为 `2,147,483,647`。另外，任何有效类型都可以作为子类型。

**声明**

```
ARRAY<t>t ARRAY
DataTypes.ARRAY(t)
```

此类型用 `ARRAY<t>` 声明，其中 `t` 是所包含元素的数据类型。

`t ARRAY` 接近等价于 SQL 标准。例如，`INT ARRAY` 等价于 `ARRAY<INT>`。

**JVM 类型**

| Java 类型                               | 输入 | 输出 | 备注                  |
| :-------------------------------------- | :--- | :--- | :-------------------- |
| *t*`[]`                                 | (X)  | (X)  | 依赖于子类型。 *缺省* |
| `org.apache.flink.table.data.ArrayData` | X    | X    | 内部数据结构。        |

##### MAP

将键（包括 `NULL`）映射到值（包括 `NULL`）的关联数组的数据类型。映射不能包含重复的键；每个键最多可以映射到一个值。

元素类型没有限制；确保唯一性是用户的责任。

Map 类型是 SQL 标准的扩展。

**声明**

```
MAP<kt, vt>
DataTypes.MAP(kt, vt)
```

此类型用 `MAP<kt, vt>` 声明，其中 `kt` 是键的数据类型，`vt` 是值的数据类型。

**JVM 类型**

| Java 类型                             | 输入 | 输出 | 备注           |
| :------------------------------------ | :--- | :--- | :------------- |
| `java.util.Map<kt, vt>`               | X    | X    | *缺省*         |
| `java.util.Map<kt, vt>` 的*子类型*    | X    |      |                |
| `org.apache.flink.table.data.MapData` | X    | X    | 内部数据结构。 |

##### MULTISET

多重集合的数据类型（=bag）。与集合不同的是，它允许每个具有公共子类型的元素有多个实例。每个唯一值（包括 `NULL`）都映射到某种多重性。

元素类型没有限制；确保唯一性是用户的责任。

**声明**

```
MULTISET<t>t MULTISET
DataTypes.MULTISET(t)
```

此类型用 `MULTISET<t>` 声明，其中 `t` 是所包含元素的数据类型。

`t MULTISET` 接近等价于 SQL 标准。例如，`INT MULTISET` 等价于 `MULTISET<INT>`。

**JVM 类型**

| Java 类型                                        | 输入 | 输出 | 备注                                  |
| :----------------------------------------------- | :--- | :--- | :------------------------------------ |
| `java.util.Map<t, java.lang.Integer>`            | X    | X    | 将每个值可多重地分配给一个整数 *缺省* |
| `java.util.Map<t, java.lang.Integer>` 的*子类型* | X    |      |                                       |
| `org.apache.flink.table.data.MapData`            | X    | X    | 内部数据结构。                        |

##### ROW

字段序列的数据类型。

字段由字段名称、字段类型和可选的描述组成。表中的行的是最特殊的类型是 Row 类型。在这种情况下，行中的每一列对应于相同位置的列的 Row 类型的字段。

与 SQL 标准相比，可选的字段描述简化了复杂结构的处理。

Row 类型类似于其他非标准兼容框架中的 `STRUCT` 类型。

**声明**

```
ROW<n0 t0, n1 t1, ...>ROW<n0 t0 'd0', n1 t1 'd1', ...>ROW(n0 t0, n1 t1, ...>ROW(n0 t0 'd0', n1 t1 'd1', ...)
DataTypes.ROW(DataTypes.FIELD(n0, t0), DataTypes.FIELD(n1, t1), ...)DataTypes.ROW(DataTypes.FIELD(n0, t0, d0), DataTypes.FIELD(n1, t1, d1), ...)
```

此类型用 `ROW<n0 t0 'd0', n1 t1 'd1', ...>` 声明，其中 `n` 是唯一的字段名称，`t` 是字段的逻辑类型，`d` 是字段的描述。

`ROW(...)` 接近等价于 SQL 标准。例如，`ROW(myField INT, myOtherField BOOLEAN)` 等价于 `ROW<myField INT, myOtherField BOOLEAN>`。

**JVM 类型**

| Java 类型                             | 输入 | 输出 | 备注           |
| :------------------------------------ | :--- | :--- | :------------- |
| `org.apache.flink.types.Row`          | X    | X    | *缺省*         |
| `org.apache.flink.table.data.RowData` | X    | X    | 内部数据结构。 |

#### 用户自定义数据类型

注意 还未完全支持用户自定义数据类型，当前（从 Flink 1.11 开始）它们仅可作为函数参数和返回值的未注册的结构化类型。

结构化类型类似于面向对象编程语言中的对象，可包含零个、一个或多个属性，每个属性都包含一个名称和一个类型。

有两种结构化类型：

- 存储在 catalog 并由 *catatlog 标识符* 标识的类型（例如 `cat.db.MyType`），等价于 SQL 标准定义里的结构化类型。
- 由 *实现类* 标识，通常以反射方式匿名定义的未注册类型（例如 `com.myorg.model.MyType`）。当写代码定义表时，这些功能很有用。它们使你能够重用现有的JVM类，而无需重复手动定义数据类型。

##### 可注册的结构化类型

当前尚不支持，因此无法在 catalog 里保存或在 `CREATE TABLE` DDL 语句里引用它们。

##### 未注册的结构化类型

可以从常规 POJOs（Plain Old Java Objects）自动反射式提取出未注册的结构化类型。

结构化类型的实现类必须满足以下要求：

- 可被全局访问到，即必须声明为 `public`、`static`，不能用 `abstract`；
- 提供无参默认构造器，或可设置所有成员变量的构造器；
- 可访问类的所有成员变量，比如使用 `public` 声明成员变量，或遵循通用代码规范写 getter 比如 `getField()`、`isField()`、`field()`；
- 可设置类的所有成员变量，比如使用 `public` 声明成员变量，定义可设置所有成员变量的构造器，或遵循通用代码规范写 setter 比如 `setField(...)`、`field(...)`；
- 所有成员变量都要映射到某个数据类型，比如使用反射式提取进行隐式映射，或用 `@DataTypeHint` [注解](https://www.bookstack.cn/read/flink-1.11.1-zh/e6de5aef458994bc.md#data-type-annotations) 显式映射；
- 忽略 `static` 或 `transient` 修饰的成员变量；

只要字段不（递归地）指向自己，反射式提取支持字段的任意嵌套。

成员变量（比如 `public int age;`）的类型必须包含在本文为每种数据类型定义的受支持的 JVM 类型列表里（例如，`java.lang.Integer` 或 `int` 对应 `INT`）。

对于某些类，需要有注解才能将类映射到数据类型（例如， `@DataTypeHint("DECIMAL(10, 2)")` 为 `java.math.BigDecimal` 分配固定的精度和小数位）。

**声明**

```
class User {    // extract fields automatically    public int age;    public String name;    // enrich the extraction with precision information    public @DataTypeHint("DECIMAL(10, 2)") BigDecimal totalBalance;    // enrich the extraction with forcing using RAW types    public @DataTypeHint("RAW") Class<?> modelClass;}DataTypes.of(User.class);
case class User(    // extract fields automatically    age: Int,    name: String,    // enrich the extraction with precision information    @DataTypeHint("DECIMAL(10, 2)") totalBalance: java.math.BigDecimal,    // enrich the extraction with forcing using a RAW type    @DataTypeHint("RAW") modelClass: Class[_])DataTypes.of(classOf[User])
```

**JVM 类型**

| Java 类型                             | 输入 | 输出 | 备注                                             |
| :------------------------------------ | :--- | :--- | :----------------------------------------------- |
| *类型*                                | X    | X    | 原始类或子类（用于输入）或超类（用于输出）*缺省* |
| `org.apache.flink.types.Row`          | X    | X    | 代表一行数据的结构化类型。                       |
| `org.apache.flink.table.data.RowData` | X    | X    | 内部数据结构。                                   |

#### 其他数据类型

##### BOOLEAN

（可能）具有 `TRUE`、`FALSE` 和 `UNKNOWN` 三值逻辑的布尔数据类型。

**声明**

```
BOOLEAN
DataTypes.BOOLEAN()
```

**JVM 类型**

| Java 类型           | 输入 | 输出 | 备注                       |
| :------------------ | :--- | :--- | :------------------------- |
| `java.lang.Boolean` | X    | X    | *缺省*                     |
| `boolean`           | X    | (X)  | 仅当类型不可为空时才输出。 |

##### RAW

任意序列化类型的数据类型。此类型对于 Flink Table 来讲是一个黑盒子，仅在跟外部交互时被反序列化。

Raw 类型是 SQL 标准的扩展。

**声明**

```
RAW('class', 'snapshot')
DataTypes.RAW(class, serializer)DataTypes.RAW(class)
```

此类型用 `RAW('class', 'snapshot')` 声明，其中 `class` 是原始类，`snapshot` 是 Base64 编码的序列化的 `TypeSerializerSnapshot`。通常，类型字符串不是直接声明的，而是在持久化类型时生成的。

在 API 中，可以通过直接提供 `Class` + `TypeSerializer` 或通过传递 `TypeInformation` 并让框架从那里提取 `Class` + `TypeSerializer` 来声明 `RAW` 类型。

**JVM 类型**

| Java 类型                                  | 输入 | 输出 | 备注                                                |
| :----------------------------------------- | :--- | :--- | :-------------------------------------------------- |
| *类型*                                     | X    | X    | 原始类或子类（用于输入）或超类（用于输出）。 *缺省* |
| `byte[]`                                   |      | X    |                                                     |
| `org.apache.flink.table.data.RawValueData` | X    | X    | 内部数据结构。                                      |

##### NULL

表示空类型 `NULL` 值的数据类型。

NULL 类型是 SQL 标准的扩展。NULL 类型除 `NULL` 值以外没有其他值，因此可以将其强制转换为 JVM 里的任何可空类型。

此类型有助于使用 `NULL` 字面量表示 `API` 调用中的未知类型，以及桥接到定义该类型的 JSON 或 Avro 等格式。

这种类型在实践中不是很有用，为完整起见仅在此提及。

**声明**

```
NULL
DataTypes.NULL()
```

**JVM 类型**

| Java 类型          | 输入 | 输出 | 备注               |
| :----------------- | :--- | :--- | :----------------- |
| `java.lang.Object` | X    | X    | *缺省*             |
| *任何类型*         |      | (X)  | 任何非基本数据类型 |

### 数据类型注解

Flink API 经常尝试使用反射自动从类信息中提取数据类型，以避免重复的手动定义模式工作。然而以反射方式提取数据类型并不总是成功的，因为可能会丢失逻辑信息。因此，可能有必要在类或字段声明附近添加额外信息以支持提取逻辑。

下表列出了可以隐式映射到数据类型而无需额外信息的类：

| 类                         | 数据类型                            |
| :------------------------- | :---------------------------------- |
| `java.lang.String`         | `STRING`                            |
| `java.lang.Boolean`        | `BOOLEAN`                           |
| `boolean`                  | `BOOLEAN NOT NULL`                  |
| `java.lang.Byte`           | `TINYINT`                           |
| `byte`                     | `TINYINT NOT NULL`                  |
| `java.lang.Short`          | `SMALLINT`                          |
| `short`                    | `SMALLINT NOT NULL`                 |
| `java.lang.Integer`        | `INT`                               |
| `int`                      | `INT NOT NULL`                      |
| `java.lang.Long`           | `BIGINT`                            |
| `long`                     | `BIGINT NOT NULL`                   |
| `java.lang.Float`          | `FLOAT`                             |
| `float`                    | `FLOAT NOT NULL`                    |
| `java.lang.Double`         | `DOUBLE`                            |
| `double`                   | `DOUBLE NOT NULL`                   |
| `java.sql.Date`            | `DATE`                              |
| `java.time.LocalDate`      | `DATE`                              |
| `java.sql.Time`            | `TIME(0)`                           |
| `java.time.LocalTime`      | `TIME(9)`                           |
| `java.sql.Timestamp`       | `TIMESTAMP(9)`                      |
| `java.time.LocalDateTime`  | `TIMESTAMP(9)`                      |
| `java.time.OffsetDateTime` | `TIMESTAMP(9) WITH TIME ZONE`       |
| `java.time.Instant`        | `TIMESTAMP(9) WITH LOCAL TIME ZONE` |
| `java.time.Duration`       | `INVERVAL SECOND(9)`                |
| `java.time.Period`         | `INTERVAL YEAR(4) TO MONTH`         |
| `byte[]`                   | `BYTES`                             |
| `T[]`                      | `ARRAY<T>`                          |
| `java.lang.Map<K, V>`      | `MAP<K, V>`                         |
| structured type `T`        | anonymous structured type `T`       |

## Table API

`Table API`是用于流和批处理的统一的关系API。`Table API`查询可以在批处理或流输入上运行，无需修改。`Table API`是SQL语言的超集，是专门为与Apache Flink一起工作而设计的。表API是一个用于Scala和Java的语言集成API。与SQL中常见的将查询指定为字符串值不同，`Table API`查询在Java或Scala中以嵌入语言的样式定义，并具有IDE支持，如自动完成和语法验证。

`Table API`与Flink的SQL集成共享许多概念和部分API。看看[通用API调用](# 通用API调用)，了解如何注册表或创建表对象。[流式概念](# 流式概念)页面讨论流特定的概念，如动态表和时间属性。

下面的示例假设一个名为`Orders`的已注册表具有属性`(a、b、c、rowtime)`。`rowtime`字段可以是流中的逻辑[时间属性](# 时间属性)，也可以是批处理中的常规时间戳字段。

### 概述和例子

Table API可用于Scala和Java。Scala Table API利用了Scala表达式，Java Table API基于字符串，这些字符串被解析并转换为等价的表达式。

下面的示例显示了Scala和Java Table API之间的差异。表程序在批处理环境中执行。它扫描Orders表，按字段a进行分组，并计算每组的结果行数。表程序的结果被转换为`Row`类型的`DataSet`并打印出来。

通过导入`org.apache.flink.table.api.java.*`来启用Java表API。下面的示例展示了如何构造Java表API程序，以及如何将表达式指定为字符串。对于表达式DSL，还需要导入静态`org.apache.flink.table.api.Expressions.*`。

```java
import org.apache.flink.table.api.*
import static org.apache.flink.table.api.Expressions.*
// environment configuration
ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
BatchTableEnvironment tEnv = BatchTableEnvironment.create(env);
// register Orders table in table environment
// ...
// specify table program
Table orders = tEnv.from("Orders"); // schema (a, b, c, rowtime)
Table counts = orders
        .groupBy($("a"))
        .select($("a"), $("b").count().as("cnt"));
// conversion to DataSet
DataSet<Row> result = tEnv.toDataSet(counts, Row.class);
result.print();
```

通过导入`org.apache.flink.table.api._`,`org.apache.flink.api.scala._`, `org.apache.flink.table.api.bridge.scala._`来启用Scala Table API。(for bridging to/from DataStream)。

下面的示例展示了如何构造Scala Table API程序。表字段使用Scala的字符串插值引用，使用的是一个美元字符($)。

```scala
import org.apache.flink.api.scala._
import org.apache.flink.table.api._
import org.apache.flink.table.api.bridge.scala._
// environment configuration
val env = ExecutionEnvironment.getExecutionEnvironment
val tEnv = BatchTableEnvironment.create(env)
// register Orders table in table environment
// ...
// specify table program
val orders = tEnv.from("Orders") // schema (a, b, c, rowtime)
val result = orders
               .groupBy($"a")
               .select($"a", $"b".count as "cnt")
               .toDataSet[Row] // conversion to DataSet
               .print()
```

使用`from pyflink.table import *`来导入Python Table API。

下面这个例子演示了如何组织一个Python Table API程序，以及字符串形式的表达式用法。

```python
from pyflink.table import *
from pyflink.dataset import *
# environment configuration
env = ExecutionEnvironment.get_execution_environment()
t_env = TableEnvironment.create(env, TableConfig())
# register Orders table and Result table sink in table environment
# ...
# specify table program
orders = t_env.from_path("Orders")  # schema (a, b, c, rowtime)
orders.group_by("a").select("a, b.count as cnt").insert_into("result")
t_env.execute("python_job")
```

下一个示例展示了一个更复杂的表API程序。程序再次扫描Orders表。它过滤空值，规范化类型为String的字段a，并为每小时和产品a计算平均账单金额b。

**java**

```java
// environment configuration
// ...
// specify table program
Table orders = tEnv.from("Orders"); // schema (a, b, c, rowtime)
Table result = orders
        .filter(
            and(
                $("a").isNotNull(),
                $("b").isNotNull(),
                $("c").isNotNull()
            ))
        .select($("a").lowerCase().as("a"), $("b"), $("rowtime"))
        .window(Tumble.over(lit(1).hours()).on($("rowtime")).as("hourlyWindow"))
        .groupBy($("hourlyWindow"), $("a"))
        .select($("a"), $("hourlyWindow").end().as("hour"), $("b").avg().as("avgBillingAmount"));
```

**scala**

```scala
# environment configuration
# ...
# specify table program
orders = t_env.from_path("Orders")  # schema (a, b, c, rowtime)
result = orders.filter("a.isNotNull && b.isNotNull && c.isNotNull") \
               .select("a.lowerCase() as a, b, rowtime") \
               .window(Tumble.over("1.hour").on("rowtime").alias("hourlyWindow")) \
               .group_by("hourlyWindow, a") \
               .select("a, hourlyWindow.end as hour, b.avg as avgBillingAmount")
```

由于Table API是用于批处理和流数据的统一API，两个示例程序都可以在批处理和流输入上执行，而不需要修改表程序本身。在这两种情况下，程序产生相同的结果，只要流记录不延迟(参见[流式概念](# 流式概念)了解细节)。

### 操作Operations

Table API支持以下操作。请注意，并`不是所有的操作`都可以在批处理和流媒体;它们被相应地标记。

#### Scan, Projection, and Filter

##### **From**

> 支持：`Bath` `Streaming`
>
> Description:
>
> 类似于SQL查询中的FROM子句。对注册表执行扫描
>
> java:
>
> ```java
> Table orders = tableEnv.from(“Orders”);
> ```
>
> scala:
>
> ```scala
> val orders: Table = tableEnv.from("Orders")
> ```
>
> python:
>
> ```python
> orders = t_env.from_path("Orders")
> ```

##### **Values**

> 支持：`Bath` `Streaming`
>
> Description:
>
> 类似于SQL查询中的VALUES子句。从提供的行中生成内联表。
>
> 你可以使用' row(…)'表达式来创建复合行:
>
> java & scala:
>
> ```java
> Table table = tEnv.fromValues(
>    row(1, "ABC"),
>    row(2L, "ABCDE")
> );
> ```
>
> python:
>
> ```python
> table = t_env.from_elements([(1, 'ABC'), (2, 'ABCDE')])
> ```
>
>  
>
> 将生成一个具有如下模式的表:
>
> ```shell
> root
> |-- f0: BIGINT NOT NULL     // 原始类型INT和BIGINT被泛化为BIGINT
> |-- f1: VARCHAR(5) NOT NULL // 原始类型CHAR(3)和CHAR(5)被推广到VARCHAR(5)。使用VARCHAR代替CHAR，因此不应用填充
> ```
>
> 该方法将自动从输入表达式派生类型。如果某个位置的类型不同，该方法将尝试为所有类型找到一个通用的超类型。如果公共超类型不存在，则会抛出异常。
>
> 您还可以显式地指定所请求的类型。它可能有助于分配更多的泛型类型，如十进制或列命名。
>
> java & scala:
>
> ```java
> Table table = tEnv.fromValues(
>     DataTypes.ROW(
>         DataTypes.FIELD("id", DataTypes.DECIMAL(10, 2)),
>         DataTypes.FIELD("name", DataTypes.STRING())
>     ),
>     row(1, "ABC"),
>     row(2L, "ABCDE")
> );
> ```
>
> 将生成一个具有如下模式的表:
>
> ```
> root
> |-- id: DECIMAL(10, 2)
> |-- name: STRING
> ```
>
> 

##### **Select**

> 支持：`Bath` `Streaming`
>
> Description:
>
> 类似于SQL SELECT语句。执行选择操作。
>
> java:
>
> ```java
> Table orders = tableEnv.from("Orders");
> Table result = orders.select($("a"), $("c").as("d"));
> ```
>
> scala:
>
> ```scala
> val orders: Table = tableEnv.from("Orders")
> val result = orders.select($"a", $"c" as "d")
> ```
>
> python:
>
> ```python
> orders = t_env.from_path("Orders")
> ```

##### **As**

> 支持：`Bath` `Streaming`
>
> Description:
>
> 重命名字段
>
> java:
>
> ```java
> Table orders = tableEnv.from("Orders");
> Table result = orders.as("x, y, z, t");
> ```
>
> scala:
>
> ```scala
> val orders: Table = tableEnv.from("Orders").as("x", "y", "z", "t")
> ```
>
> python:
>
> ```python
> orders = t_env.from_path("Orders")
> result = orders.alias("x, y, z, t")
> ```

##### **Where / Filter**

> 支持：`Bath` `Streaming`
>
> Description:
>
> 类似于SQL的WHERE子句。过滤掉没有传递过滤器谓词的行。
>
> java:
>
> ```java
> Table orders = tableEnv.from("Orders");
> Table result = orders.where($("b").isEqual("red"));
> // or
> Table orders = tableEnv.from("Orders");
> Table result = orders.filter($("a").mod(2).isEqual(0));
> ```
>
> scala:
>
> ```scala
> val orders: Table = tableEnv.from("Orders")
> val result = orders.filter($"a" % 2 === 0)
> // or
> val orders: Table = tableEnv.from("Orders")
> val result = orders.where($"b" === "red")
> ```
>
> python:
>
> ```python
> orders = t_env.from_path("Orders")
> result = orders.where("a === 'red'")
> // or
> orders = t_env.from_path("Orders")
> result = orders.filter("b % 2 === 0")
> ```



#### Column Operations

##### **AddColumns**

> 支持：`Bath` `Streaming`
>
> Description:
>
> 执行字段添加操作。如果添加的字段已经存在，它将抛出异常。
>
> java:
>
> ```java
> Table orders = tableEnv.from("Orders");
> Table result = orders.addColumns(concat($("c"), "sunny"));
> ```
>
> scala:
>
> ```scala
> val orders = tableEnv.from("Orders");
> val result = orders.addColumns(concat($"c", "Sunny"))
> ```
>
> python:
>
> ```python
> orders = t_env.from_path("Orders")
> result = orders.add_columns("concat(c, 'sunny')")
> ```

##### **DropColumns**

> 支持：`Bath` `Streaming`
>
> Description:
>
> 执行字段删除操作。字段表达式应该是字段引用表达式，并且只能删除现有的字段。
>
> java:
>
> ```java
> Table orders = tableEnv.from("Orders");
> Table result = orders.dropColumns($("b"), $("c"));
> ```
>
> scala:
>
> ```scala
> val orders = tableEnv.from("Orders");
> val result = orders.dropColumns($"b", $"c")
> ```
>
> python:
>
> ```python
> orders = t_env.from_path("Orders")
> result = orders.drop_columns("b, c")
> ```

##### **AddOrReplaceColumns**

> 支持：`Bath` `Streaming`
>
> Description:
>
> 执行字段添加操作。如果添加列名与现有列名相同，则现有字段将被替换。此外，如果添加的字段具有重复的字段名称，则使用最后一个字段。
>
> java:
>
> ```java
> Table orders = tableEnv.from("Orders");
> Table result = orders.addOrReplaceColumns(concat($("c"), "sunny").as("desc"));
> ```
>
> scala:
>
> ```scala
> val orders = tableEnv.from("Orders");
> val result = orders.addOrReplaceColumns(concat($"c", "Sunny") as "desc")
> ```
>
> python:
>
> ```python
> orders = t_env.from_path("Orders")
> result = orders.add_or_replace_columns("concat(c, 'sunny') as desc")
> ```

##### **RenameColumns**

> 支持：`Bath` `Streaming`
>
> Description:
>
> 执行字段重命名操作。字段表达式应该是别名表达式，只有现有字段可以重命名。
>
> java:
>
> ```java
> Table orders = tableEnv.from("Orders");
> Table result = orders.renameColumns($("b").as("b2"), $("c").as("c2"));
> ```
>
> scala:
>
> ```scala
> val orders = tableEnv.from("Orders");
> val result = orders.renameColumns($"b" as "b2", $"c" as "c2")
> ```
>
> python:
>
> ```python
> orders = t_env.from_path("Orders")
> result = orders.rename_columns("b as b2, c as c2")
> ```



#### Aggregations

##### GroupBy Aggregation

> 支持：`Bath` `Streaming`
>
> Description:
>
> 类似于SQL GROUP BY子句。使用下面正在运行的聚合操作符对分组键上的行进行分组，以便按组对行进行聚合。
>
> java:
>
> ```java
> Table orders = tableEnv.from("Orders");
> Table result = orders.groupBy($("a")).select($("a"), $("b").sum().as("d"));
> ```
>
> scala:
>
> ```scala
> val orders: Table = tableEnv.from("Orders")
> val result = orders.groupBy($"a").select($"a", $"b".sum().as("d"))
> ```
>
> python:
>
> ```python
> orders = t_env.from_path("Orders")
> result = orders.group_by("a").select("a, b.sum as d")
> ```
>
> **注意：** 对于流式查询，计算查询结果所需的状态（state）可能会无限增长，具体情况取决于聚合操作的类型和分组的数量。您可能需要在查询配置中设置状态保留时间，以防止状态过大。详情请看[查询配置](https://www.bookstack.cn/read/flink-1.11.1-zh/36210bd6756300af.md)。

##### **GroupBy Window Aggregation**

> 支持：`Bath` `Streaming`
>
> Description:
>
> 在一个窗口上分组和聚合数据，可包含其它分组字段。
>
> java:
>
> ```java
> Table orders = tableEnv.from("Orders");
> Table result = orders
>     .window(Tumble.over(lit(5).minutes())).on($("rowtime")).as("w")) // define window
>     .groupBy($("a"), $("w")) // group by key and window
>     // access window properties and aggregate
>     .select(
>         $("a"),
>         $("w").start(),
>         $("w").end(),
>         $("w").rowtime(),
>         $("b").sum().as("d")
>     );
> ```
>
> scala:
>
> ```scala
> val orders: Table = tableEnv.from("Orders")
> val result: Table = orders
>     .window(Tumble over 5.minutes on $"rowtime" as "w") // define window
>     .groupBy($"a", $"w") // group by key and window
>     .select($"a", $"w".start, $"w".end, $"w".rowtime, $"b".sum as "d") // access window properties and aggregate
> ```
>
> python:
>
> ```python
> from pyflink.table.window import Tumble
> 
> orders = t_env.from_path("Orders")
> result = orders.window(Tumble.over("5.minutes").on("rowtime").alias("w")) \ 
>                .group_by("a, w") \
>                .select("a, w.start, w.end, b.sum as d")
> ```

##### **Over Window Aggregation**

> 支持： `Streaming`
>
> Description:
>
> 类似于SQL中的OVER开窗函数。Over窗口聚合对每一行都进行一次聚合计算，聚合的对象是以当前行的位置为基准，向前向后取一个区间范围内的所有数据。详情请见[Over窗口](https://www.bookstack.cn/read/flink-1.11.1-zh/3d19a702560621b1.md#over-windows)一节。
>
> java:
>
> ```java
> Table orders = tableEnv.from("Orders");
> Table result = orders
>     // define window
>     .window(
>         Over
>           .partitionBy($("a"))
>           .orderBy($("rowtime"))
>           .preceding(UNBOUNDED_RANGE)
>           .following(CURRENT_RANGE)
>           .as("w"))
>     // sliding aggregate
>     .select(
>         $("a"),
>         $("b").avg().over($("w")),
>         $("b").max().over($("w")),
>         $("b").min().over($("w"))
>     );
> ```
>
> scala:
>
> ```scala
> val orders: Table = tableEnv.from("Orders")
> val result: Table = orders
>     // define window
>     .window(
>         Over
>           partitionBy $"a"
>           orderBy $"rowtime"
>           preceding UNBOUNDED_RANGE
>           following CURRENT_RANGE
>           as "w")
>     .select($"a", $"b".avg over $"w", $"b".max().over($"w"), $"b".min().over($"w")) // sliding aggregate
> ```
>
> python:
>
> ```python
> from pyflink.table.window import Over
> 
> orders = t_env.from_path("Orders")
> result = orders.over_window(Over.partition_by("a").order_by("rowtime")
>                             .preceding("UNBOUNDED_RANGE").following("CURRENT_RANGE")
>                             .alias("w")) \
>     .select("a, b.avg over w, b.max over w, b.min over w")
> ```
>
> *注意*:所有聚合必须在同一个窗口上定义。，相同的分区、排序和范围。目前，只支持前面有当前行范围(无边界和有边界)的窗口。还不支持以下范围。ORDER BY必须在单个[time attribute](https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/table/streaming/time_attributes.html)上指定。

##### **Distinct Aggregation**

> 支持：`Bath` `Streaming`
>
> Description:
>
> 不同聚合声明聚合函数(内置或用户定义)只应用于不同的输入值。Distinct可以应用于**GroupBy Aggregation**, **GroupBy Window Aggregation** and **Over Window Aggregation**。
>
> java:
>
> ```java
> Table orders = tableEnv.from("Orders");
> // Distinct aggregation on group by
> Table groupByDistinctResult = orders
>     .groupBy($("a"))
>     .select($("a"), $("b").sum().distinct().as("d"));
> // Distinct aggregation on time window group by
> Table groupByWindowDistinctResult = orders
>     .window(Tumble
>             .over(lit(5).minutes()))
>             .on($("rowtime"))
>             .as("w")
>     )
>     .groupBy($("a"), $("w"))
>     .select($("a"), $("b").sum().distinct().as("d"));
> // Distinct aggregation on over window
> Table result = orders
>     .window(Over
>         .partitionBy($("a"))
>         .orderBy($("rowtime"))
>         .preceding(UNBOUNDED_RANGE)
>         .as("w"))
>     .select(
>         $("a"), $("b").avg().distinct().over($("w")),
>         $("b").max().over($("w")),
>         $("b").min().over($("w"))
>     );
> ```
>
> scala:
>
> ```scala
> val orders: Table = tableEnv.from("Orders")
> val result: Table = orders
>     .window(Tumble over 5.minutes on $"rowtime" as "w") // define window
>     .groupBy($"a", $"w") // group by key and window
>     .select($"a", $"w".start, $"w".end, $"w".rowtime, $"b".sum as "d") // access window properties and aggregate
> ```
>
> python:
>
> ```python
> orders = t_env.from_path("Orders")
> # Distinct aggregation on group by
> group_by_distinct_result = orders.group_by("a") \
>                                  .select("a, b.sum.distinct as d")
> # Distinct aggregation on time window group by
> group_by_window_distinct_result = orders.window(
>     Tumble.over("5.minutes").on("rowtime").alias("w")).group_by("a, w") \
>     .select("a, b.sum.distinct as d")
> # Distinct aggregation on over window
> result = orders.over_window(Over
>                        .partition_by("a")
>                        .order_by("rowtime")
>                        .preceding("UNBOUNDED_RANGE")
>                        .alias("w")) \
>                        .select(
>                        "a, b.avg.distinct over w, b.max over w, b.min over w")
> ```
>
> *注意*:对于流查询，计算查询结果所需的状态可能会无限增长，这取决于不同字段的数量。请提供具有有效保留间隔的查询配置，以防止状态过大。有关详细信息，请参阅[Query Configuration](https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/table/streaming/query_configuration.html) 。

##### **Distinct**

> 支持：`Bath` `Streaming`
>
> Description:
>
> 类似于SQL DISTINCT子句。返回具有不同值组合的记录。
>
> java:
>
> ```java
> Table orders = tableEnv.from("Orders");
> Table result = orders.distinct();
> ```
>
> scala:
>
> ```scala
> val orders: Table = tableEnv.from("Orders")
> val result = orders.distinct()
> ```
>
> python:
>
> ```python
> orders = t_env.from_path("Orders")
> result = orders.distinct()
> ```
>
> 注意:对于流查询，计算查询结果所需的状态可能会无限增长，这取决于不同字段的数量。请提供具有有效保留间隔的查询配置，以防止状态过大。如果启用了状态清理，则distinct必须发出消息，以防止下游操作符的过早状态清除，从而使distinct包含结果更新。有关详细信息，请参阅查询配置。

#### Joins

##### **Inner Join**

> **from**	`Bath` `Streaming`
>
> Description:
>
> 类似于SQL连接子句。连接两个表。两个表必须具有不同的字段名，并且必须通过join操作符或使用where或filter操作符定义至少一个相等连接谓词。
>
> java:
>
> ```java
> Table left = tableEnv.fromDataSet(ds1, "a, b, c");
> Table right = tableEnv.fromDataSet(ds2, "d, e, f");
> Table result = left.join(right)
>     .where($("a").isEqual($("d")))
>     .select($("a"), $("b"), $("e"));
> ```
>
> scala:
>
> ```scala
> val left = ds1.toTable(tableEnv, $"a", $"b", $"c")
> val right = ds2.toTable(tableEnv, $"d", $"e", $"f")
> val result = left.join(right).where($"a" === $"d").select($"a", $"b", $"e")
> ```
>
> python:
>
> ```python
> left = t_env.from_path("Source1").select("a, b, c")
> right = t_env.from_path("Source2").select("d, e, f")
> result = left.join(right).where("a = d").select("a, b, e")
> ```
>
> **Note:** For streaming queries the required state to compute the query result might grow infinitely depending on the number of distinct input rows. Please provide a query configuration with valid retention interval to prevent excessive state size. See [Query Configuration](https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/table/streaming/query_configuration.html) for details.

##### **Outer Join**

> 支持：`Bath` `Streaming`
>
> Description:
>
> 类似于SQL左/右/全外部连接子句。连接两个表。两个表必须具有不同的字段名，并且必须定义至少一个相等联接谓词。
>
> java:
>
> ```java
> Table left = tableEnv.fromDataSet(ds1, "a, b, c");
> Table right = tableEnv.fromDataSet(ds2, "d, e, f");
> 
> Table leftOuterResult = left.leftOuterJoin(right, $("a").isEqual($("d")))
>                             .select($("a"), $("b"), $("e"));
> Table rightOuterResult = left.rightOuterJoin(right, $("a").isEqual($("d")))
>                             .select($("a"), $("b"), $("e"));
> Table fullOuterResult = left.fullOuterJoin(right, $("a").isEqual($("d")))
>                             .select($("a"), $("b"), $("e"));
> ```
>
> scala:
>
> ```scala
> val left = tableEnv.fromDataSet(ds1, $"a", $"b", $"c")
> val right = tableEnv.fromDataSet(ds2, $"d", $"e", $"f")
> 
> val leftOuterResult = left.leftOuterJoin(right, $"a" === $"d").select($"a", $"b", $"e")
> val rightOuterResult = left.rightOuterJoin(right, $"a" === $"d").select($"a", $"b", $"e")
> val fullOuterResult = left.fullOuterJoin(right, $"a" === $"d").select($"a", $"b", $"e")
> ```
>
> python:
>
> ```python
> left = t_env.from_path("Source1").select("a, b, c")
> right = t_env.from_path("Source2").select("d, e, f")
> 
> left_outer_result = left.left_outer_join(right, "a = d").select("a, b, e")
> right_outer_result = left.right_outer_join(right, "a = d").select("a, b, e")
> full_outer_result = left.full_outer_join(right, "a = d").select("a, b, e")
> ```
>
> **Note:** For streaming queries the required state to compute the query result might grow infinitely depending on the number of distinct input rows. Please provide a query configuration with valid retention interval to prevent excessive state size. See [Query Configuration](https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/table/streaming/query_configuration.html) for details.

##### **Interval Join**

> 支持：`Bath` `Streaming`
>
> 注意:间隔连接是可以以流方式处理的常规连接的子集。
>
> Description:
>
> 间隔连接至少需要一个等连接谓词和一个限定双方时间的连接条件。这样的条件可以由两个适当的范围谓词(&lt;， &lt;=， &gt;=， &gt;)或一个比较相同类型的时间属性的相等谓词(即两个输入表的处理时间或事件时间)。
>
> 例如，下面的谓词是有效的间隔连接条件:
>
> - `ltime === rtime`
> - `ltime >= rtime && ltime < rtime + 10.minutes`
>
> java:
>
> ```java
> Table left = tableEnv.fromDataSet(ds1, $("a"), $("b"), $("c"), $("ltime").rowtime());
> Table right = tableEnv.fromDataSet(ds2, $("d"), $("e"), $("f"), $("rtime").rowtime()));
> 
> Table result = left.join(right)
>   .where(
>     and(
>         $("a").isEqual($("d")),
>         $("ltime").isGreaterEqual($("rtime").minus(lit(5).minutes())),
>         $("ltime").isLess($("rtime").plus(lit(10).minutes()))
>     ))
>   .select($("a"), $("b"), $("e"), $("ltime"));c
> ```
>
> scala:
>
> ```scala
> Table left = tableEnv.fromDataSet(ds1, $("a"), $("b"), $("c"), $("ltime").rowtime());
> Table right = tableEnv.fromDataSet(ds2, $("d"), $("e"), $("f"), $("rtime").rowtime()));
> 
> Table result = left.join(right)
>   .where(
>     and(
>         $("a").isEqual($("d")),
>         $("ltime").isGreaterEqual($("rtime").minus(lit(5).minutes())),
>         $("ltime").isLess($("rtime").plus(lit(10).minutes()))
>     ))
>   .select($("a"), $("b"), $("e"), $("ltime"));
> ```
>
> python:
>
> ```python
> left = t_env.from_path("Source1").select("a, b, c, rowtime1")
> right = t_env.from_path("Source2").select("d, e, f, rowtime2")
>   
> result = left.join(right).where("a = d && rowtime1 >= rowtime2 - 1.second 
>                        && rowtime1 <= rowtime2 + 2.second").select("a, b, e, rowtime1")
> ```

##### **Inner Join with Table Function (UDTF)**

> 支持：`Bath` `Streaming`
>
> Description:
>
> 将表与表函数的结果连接起来。左边(外部)表的每一行都与相应调用表函数产生的所有行连接。如果左(外部)表的表函数调用返回空结果，则删除该表的一行。
>
> java:
>
> ```java
> // register User-Defined Table Function
> TableFunction<String> split = new MySplitUDTF();
> tableEnv.registerFunction("split", split);
> 
> // join
> Table orders = tableEnv.from("Orders");
> Table result = orders
>     .joinLateral(call("split", $("c")).as("s", "t", "v"))
>     .select($("a"), $("b"), $("s"), $("t"), $("v"));
> ```
>
> scala:
>
> ```scala
> // instantiate User-Defined Table Function
> val split: TableFunction[_] = new MySplitUDTF()
> 
> // join
> val result: Table = table
>     .joinLateral(split($"c") as ("s", "t", "v"))
>     .select($"a", $"b", $"s", $"t", $"v")
> ```
>
> python:
>
> ```python
> # register User-Defined Table Function
> @udtf(input_types=[DataTypes.BIGINT()],
>       result_types=[DataTypes.BIGINT(), DataTypes.BIGINT(), DataTypes.BIGINT()])
> def split(x):
>     return [Row(1, 2, 3)]
> t_env.register_function("split", split)
> 
> # join
> orders = t_env.from_path("Orders")
> result = orders.join_lateral("split(c).as(s, t, v)").select("a, b, s, t, v")
> ```

##### **Left Outer Join with Table Function (UDTF)**

> 支持：`Bath` `Streaming`
>
> Description:
>
> 将表与表函数的结果连接起来。左边(外部)表的每一行都与相应调用表函数产生的所有行连接。如果表函数调用返回空结果，则保留相应的外部行，并用空值填充结果。
>
> 注意:目前，表函数左外连接的谓词只能为空或文字为真。
>
> java:
>
> ```java
> // register User-Defined Table Function
> TableFunction<String> split = new MySplitUDTF();
> tableEnv.registerFunction("split", split);
> 
> // join
> Table orders = tableEnv.from("Orders");
> Table result = orders
>     .leftOuterJoinLateral(call("split", $("c")).as("s", "t", "v"))
>     .select($("a"), $("b"), $("s"), $("t"), $("v"));
> ```
>
> scala:
>
> ```scala
> // instantiate User-Defined Table Function
> val split: TableFunction[_] = new MySplitUDTF()
> 
> // join
> val result: Table = table
>     .leftOuterJoinLateral(split($"c") as ("s", "t", "v"))
>     .select($"a", $"b", $"s", $"t", $"v")
> ```
>
> python:
>
> ```python
> # register User-Defined Table Function
> @udtf(input_types=[DataTypes.BIGINT()],
>       result_types=[DataTypes.BIGINT(), DataTypes.BIGINT(), DataTypes.BIGINT()])
> def split(x):
>     return [Row(1, 2, 3)]
> t_env.register_function("split", split)
> 
> # join
> orders = t_env.from_path("Orders")
> result = orders.left_outer_join_lateral("split(c).as(s, t, v)").select("a, b, s, t, v")
> ```

##### **Join with Temporal Table**

> 支持：`Bath` `Streaming`
>
> Description:
>
> 时态表是跟踪随时间变化的表。
>
> 时态表函数提供对时态表在特定时间点的状态的访问。使用时态表函数连接表的语法与使用表函数连接表的内部连接相同。
>
> 目前只支持与时态表的内部连接。
>
> java:
>
> ```java
> Table ratesHistory = tableEnv.from("RatesHistory");
> 
> // register temporal table function with a time attribute and primary key
> TemporalTableFunction rates = ratesHistory.createTemporalTableFunction(
>     "r_proctime",
>     "r_currency");
> tableEnv.registerFunction("rates", rates);
> 
> // join with "Orders" based on the time attribute and key
> Table orders = tableEnv.from("Orders");
> Table result = orders
>     .joinLateral(call("rates", $("o_proctime")), $("o_currency").isEqual($("r_currency")))
> ```
>
> scala:
>
> ```scala
> val ratesHistory = tableEnv.from("RatesHistory")
> 
> // register temporal table function with a time attribute and primary key
> val rates = ratesHistory.createTemporalTableFunction($"r_proctime", $"r_currency")
> 
> // join with "Orders" based on the time attribute and key
> val orders = tableEnv.from("Orders")
> val result = orders
>     .joinLateral(rates($"o_rowtime"), $"r_currency" === $"o_currency")
> ```
>
> python:
>
> ```python
> Currently not supported in Python Table API.
> ```

#### Set Operations

##### **Union**

> 支持：`Bath` 
>
> Description: 类似于SQL的UNION子句。将两张表组合成一张表，这张表拥有二者去除重复后的全部数据。两张表的字段和类型必须完全一致。
>
> ```java
>Table left = tableEnv.fromDataSet(ds1, "a, b, c");
> Table right = tableEnv.fromDataSet(ds2, "a, b, c");
>Table result = left.union(right);
> ```
> 

##### **UnionAll**

> 支持：`Bath` `Streaming`
>
> Description: 类似于SQL的UNION ALL子句。将两张表组合成一张表，这张表拥有二者的全部数据。两张表的字段和类型必须完全一致。
>
> ```java
>Table left = tableEnv.fromDataSet(ds1, "a, b, c");
> Table right = tableEnv.fromDataSet(ds2, "a, b, c");
>Table result = left.unionAll(right);
> ```
> 

##### **Intersect**

> 支持：`Bath` 
>
> Description: 类似于SQL的INTERSECT子句。Intersect返回在两张表中都存在的数据。如果一个记录在两张表中不止出现一次，则只返回一次，即结果表没有重复记录。两张表的字段和类型必须完全一致。
>
> ```java
> Table left = tableEnv.fromDataSet(ds1, "a, b, c");
> Table right = tableEnv.fromDataSet(ds2, "d, e, f");
> Table result = left.intersect(right);
> ```

##### **IntersectAll**

> 支持：`Bath` 
>
> Description: 类似于SQL的INTERSECT ALL子句。IntersectAll返回在两张表中都存在的数据。如果一个记录在两张表中不止出现一次，则按照它在两张表中都出现的次数返回，即结果表可能包含重复数据。两张表的字段和类型必须完全一致。
>
> ```java
> Table left = tableEnv.fromDataSet(ds1, "a, b, c");
> Table right = tableEnv.fromDataSet(ds2, "d, e, f");
> Table result = left.intersectAll(right);
> ```

##### **Minus**

> 支持：`Bath` 
>
> Description: 类似于SQL的EXCEPT子句。Minus返回仅存在于左表，不存在于右表中的数据。左表中的相同数据只会返回一次，即数据会被去重。两张表的字段和类型必须完全一致。
>
> ```java
> Table left = tableEnv.fromDataSet(ds1, "a, b, c");
> Table right = tableEnv.fromDataSet(ds2, "a, b, c");
> Table result = left.minus(right);
> ```

##### **MinusAll**

> 支持：`Bath` 
>
> Description: 类似于SQL的EXCEPT ALL子句。MinusAll返回仅存在于左表，不存在于右表中的数据。如果一条数据在左表中出现了n次，在右表中出现了m次，最终这条数据将会被返回(n - m)次，即按右表中出现的次数来移除数据。两张表的字段和类型必须完全一致。
>
> ```java
> Table left = tableEnv.fromDataSet(ds1, "a, b, c");
> Table right = tableEnv.fromDataSet(ds2, "a, b, c");
> Table result = left.minusAll(right);
> ```

##### **In**

> 支持：`Bath` `Streaming`
>
> Description: 类似于SQL的IN子句。如果In左边表达式的值在给定的子查询结果中则返回true。子查询的结果必须为单列。此列数据类型必须和表达式一致。
>
> ```java
> Table left = ds1.toTable(tableEnv, "a, b, c");
> Table right = ds2.toTable(tableEnv, "a");
> 
> Table result = left.select($("a"), $("b"), $("c")).where($("a").in(right));
> ```
>
> **注意：** 对于流式查询，这个操作会被替换成一个连接操作和一个分组操作。计算查询结果所需的状态（state）可能会无限增长，具体取决于不重复的输入行的数量。您可能需要在查询配置中设置状态保留时间，以防止状态过大。详情请看[查询配置](https://www.bookstack.cn/read/flink-1.11.1-zh/36210bd6756300af.md)。

#### OrderBy, Offset & Fetch

##### **Order By**

> 支持：`Bath` 
>
> Description: 类似于SQL的ORDER BY子句。返回包括所有子并发分区内所有数据的全局排序结果。
>
> ```java
>Table in = tableEnv.fromDataSet(ds, "a, b, c");
> Table result = in.orderBy($("a").asc()");
>```
> 

##### **Offset & Fetch**

> 支持：`Bath` 
>
> Description: 类似于SQL的OFFSET和FETCH子句。Offset和Fetch从已排序的结果中返回指定数量的数据。Offset和Fetch在技术上是Order By操作的一部分，因此必须紧跟其后出现。
>
> ```java
>Table in = tableEnv.fromDataSet(ds, "a, b, c");
> 
>// returns the first 5 records from the sorted result
> Table result1 = in.orderBy($("a").asc()).fetch(5);
> 
> // skips the first 3 records and returns all following records from the sorted result
>Table result2 = in.orderBy($("a").asc()).offset(3);
> 
>// skips the first 10 records and returns the next 5 records from the sorted result
> Table result3 = in.orderBy($("a").asc()).offset(10).fetch(5);
> ```
> 



#### Insert

##### **Insert Into**

> 支持：`Bath` `Streaming`
>
> Description:
>
> 类似于SQL请求中的INSERT INTO子句。将数据输出到一个已注册的输出表中。`execute_insert` 方法会立即提交一个 Flink 作业，触发插入操作。
>
> 输出表必须先在TableEnvironment中注册（详见[注册一个TableSink](https://www.bookstack.cn/read/flink-1.11.1-zh/432a56ede104b490.md#register-a-tablesink)）。此外，注册的表的模式（schema）必须和请求的结果的模式（schema）相匹配。
>
> java:
>
> ```java
> Table orders = tableEnv.from("Orders");
> orders.executeInsert("OutOrders");
> ```
>
> scala:
>
> ```scala
> val orders: Table = tableEnv.from("Orders")
> orders.executeInsert("OutOrders")
> ```
>
> python:
>
> ```python
> orders = t_env.from_path("Orders")
> orders.execute_insert("OutOrders")
> ```

#### Group Windows

Group window根据时间或行数间隔将分组行聚合为有限组，并对每组计算一次聚合函数。对于批处理表，窗口是按时间间隔对记录进行分组的方便快捷方式。

窗口是使用window(GroupWindow w)子句定义的，需要使用as子句指定别名。为了根据窗口对表进行分组，必须在groupBy(…)子句中引用窗口别名，就像引用常规的分组属性一样。下面的示例演示如何在表上定义窗口聚合。

> java:
>
> ```java
> Table table = input
>   .window([GroupWindow w].as("w"))  // define window with alias w
>   .groupBy($("w"))  // group the table by window w
>   .select($("b").sum());  // aggregate
> ```
>
> scala:
>
> ```scala
> val table = input
>   .window([w: GroupWindow] as $"w")  // define window with alias w
>   .groupBy($"w")   // group the table by window w
>   .select($"b".sum)  // aggregate
> ```
>
> python:
>
> ```python
> # define window with alias w, group the table by window w, then aggregate
> table = input.window([w: GroupWindow].alias("w")) \
>              .group_by("w").select("b.sum")
> ```

在流环境中，只有当窗口在窗口之外的一个或多个属性上分组时，才能并行计算窗口聚合。， groupBy(…)子句引用一个窗口别名和至少一个附加属性。groupBy(…)子句仅引用窗口别名(如上面的示例)，只能由单个非并行任务计算。下面的示例显示如何定义具有附加分组属性的窗口聚合。

> java:
>
> ```java
> Table table = input
>   .window([GroupWindow w].as("w"))  // define window with alias w
>   .groupBy($("w"), $("a"))  // group the table by attribute a and window w
>   .select($("a"), $("b").sum());  // aggregate
> ```
>
> scala:
>
> ```scala
> val table = input
>   .window([w: GroupWindow] as $"w") // define window with alias w
>   .groupBy($"w", $"a")  // group the table by attribute a and window w
>   .select($"a", $"b".sum)  // aggregatec
> ```
>
> python:
>
> ```python
> # define window with alias w, group the table by attribute a and window w,
> # then aggregate
> table = input.window([w: GroupWindow].alias("w")) \
>              .group_by("w, a").select("b.sum")
> ```

窗口属性，如时间窗口的开始、结束或rowtime时间戳，可以作为窗口别名w的属性添加到select语句中。首先,w.start、w.end和w.rowtime。窗口启动和行时时间戳是包含上下窗口边界的。相反，窗口结束时间戳是独占的上窗口边界。例如，从下午2点开始的30分钟滚动窗口将有14:00:00.000作为开始时间戳，14:29:59.999作为rowtime时间戳，14:30:00.000作为结束时间戳。

> java:
>
> ```java
> Table table = input
>   .window([GroupWindow w].as("w"))  // define window with alias w
>   .groupBy($("w"), $("a"))  // group the table by attribute a and window w
>   .select($("a"), $("w").start(), $("w").end(), $("w").rowtime(), $("b").count()); // aggregate and add window start, end, and rowtime timestamps
> ```
>
> ```java
> val table = input
>   .window([w: GroupWindow] as $"w")  // define window with alias w
>   .groupBy($"w", $"a")  // group the table by attribute a and window w
>   .select($"a", $"w".start, $"w".end, $"w".rowtime, $"b".count) // aggregate and add window start, end, and rowtime timestamps
> ```
>
> scala:
>
> ```scala
> val table = input
>   .window([w: GroupWindow] as $"w") // define window with alias w
>   .groupBy($"w", $"a")  // group the table by attribute a and window w
>   .select($"a", $"b".sum)  // aggregatec
> ```
>
> python:
>
> ```python
> # define window with alias w, group the table by attribute a and window w,
> # then aggregate and add window start, end, and rowtime timestamps
> table = input.window([w: GroupWindow].alias("w")) \
>              .group_by("w, a") \
>              .select("a, w.start, w.end, w.rowtime, b.count")
> ```

Window参数定义了行如何映射到窗口。Window不是用户可以实现的接口。相反，表API提供了一组具有特定语义的预定义窗口类，这些语义被转换为底层的DataStream或数据集操作。下面列出了支持的窗口定义。

##### Tumble (Tumbling Windows)

滚动窗口将行分配给不重叠的、固定长度的连续窗口。例如，5分钟的滚动窗口以5分钟的间隔对行进行分组。滚动窗口可以在事件时间、处理时间或行计数上定义。

| Method | Description                                                  |
| :----- | :----------------------------------------------------------- |
| `over` | Defines the length the window, either as time or row-count interval. |
| `on`   | The time attribute to group (time interval) or sort (row count) on. For batch queries this might be any Long or Timestamp attribute. For streaming queries this must be a [declared event-time or processing-time time attribute](https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/table/streaming/time_attributes.html). |
| `as`   | Assigns an alias to the window. The alias is used to reference the window in the following `groupBy()` clause and optionally to select window properties such as window start, end, or rowtime timestamps in the `select()` clause. |

```java
// Tumbling Event-time Window
.window(Tumble.over(lit(10).minutes()).on($("rowtime")).as("w"));

// Tumbling Processing-time Window (assuming a processing-time attribute "proctime")
.window(Tumble.over(lit(10).minutes()).on($("proctime")).as("w"));

// Tumbling Row-count Window (assuming a processing-time attribute "proctime")
.window(Tumble.over(rowInterval(10)).on($("proctime")).as("w"));
```

```scala
// Tumbling Event-time Window
.window(Tumble over 10.minutes on $"rowtime" as $"w")

// Tumbling Processing-time Window (assuming a processing-time attribute "proctime")
.window(Tumble over 10.minutes on $"proctime" as $"w")

// Tumbling Row-count Window (assuming a processing-time attribute "proctime")
.window(Tumble over 10.rows on $"proctime" as $"w")
```

##### Slide (Sliding Windows)

滑动窗口具有固定的大小并按指定的滑动间隔滑动。如果滑动间隔小于窗口大小，则滑动窗口是重叠的。因此，行可以分配给多个窗口。例如，一个大小为15分钟、间隔为5分钟的滑动窗口将每行分配给3个大小为15分钟的不同窗口，时间间隔为5分钟。滑动窗口可以在事件时间、处理时间或行计数上定义。

| Method  | Description                                                  |
| :------ | :----------------------------------------------------------- |
| `over`  | Defines the length of the window, either as time or row-count interval. |
| `every` | Defines the slide interval, either as time or row-count interval. The slide interval must be of the same type as the size interval. |
| `on`    | The time attribute to group (time interval) or sort (row count) on. For batch queries this might be any Long or Timestamp attribute. For streaming queries this must be a [declared event-time or processing-time time attribute](https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/table/streaming/time_attributes.html). |
| `as`    | Assigns an alias to the window. The alias is used to reference the window in the following `groupBy()` clause and optionally to select window properties such as window start, end, or rowtime timestamps in the `select()` clause. |

```java
// Sliding Event-time Window
.window(Slide.over(lit(10).minutes())
            .every(lit(5).minutes())
            .on($("rowtime"))
            .as("w"));

// Sliding Processing-time window (assuming a processing-time attribute "proctime")
.window(Slide.over(lit(10).minutes())
            .every(lit(5).minutes())
            .on($("proctime"))
            .as("w"));

// Sliding Row-count window (assuming a processing-time attribute "proctime")
.window(Slide.over(rowInterval(10)).every(rowInterval(5)).on($("proctime")).as("w"));
```

```scala
// Sliding Event-time Window
.window(Slide over 10.minutes every 5.minutes on $"rowtime" as $"w")

// Sliding Processing-time window (assuming a processing-time attribute "proctime")
.window(Slide over 10.minutes every 5.minutes on $"proctime" as $"w")

// Sliding Row-count window (assuming a processing-time attribute "proctime")
.window(Slide over 10.rows every 5.rows on $"proctime" as $"w")
```

##### Session (Session Windows)

会话窗口没有固定的大小，但是它们的边界是由不活动的时间间隔定义的。，如果在定义的间隔期间内没有事件出现，则关闭会话窗口。例如，一个有30分钟间隔的会话窗口在30分钟不活动后观察到一行时开始(否则该行将添加到现有窗口)，如果30分钟内没有添加行，则关闭该会话窗口。会话窗口可以在事件时或处理时工作。

| Method    | Description                                                  |
| :-------- | :----------------------------------------------------------- |
| `withGap` | Defines the gap between two windows as time interval.        |
| `on`      | The time attribute to group (time interval) or sort (row count) on. For batch queries this might be any Long or Timestamp attribute. For streaming queries this must be a [declared event-time or processing-time time attribute](https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/table/streaming/time_attributes.html). |
| `as`      | Assigns an alias to the window. The alias is used to reference the window in the following `groupBy()` clause and optionally to select window properties such as window start, end, or rowtime timestamps in the `select()` clause. |

```java
// Session Event-time Window
.window(Session.withGap(lit(10).minutes()).on($("rowtime")).as("w"));

// Session Processing-time Window (assuming a processing-time attribute "proctime")
.window(Session.withGap(lit(10).minutes()).on($("proctime")).as("w"));
```

```scala
// Session Event-time Window
.window(Session withGap 10.minutes on $"rowtime" as $"w")

// Session Processing-time Window (assuming a processing-time attribute "proctime")
.window(Session withGap 10.minutes on $"proctime" as $"w")
```

#### Over Windows

窗口聚合来自标准SQL (Over子句)，并在查询的SELECT子句中定义。与group BY子句中指定的组窗口不同，在窗口上不折叠行。而不是通过窗口聚合计算每个输入行在其相邻行范围内的聚合。

在windows使用窗口定义(w: OverWindow *)条款(在Python API中为over_window (* OverWindow)并通过别名引用在select()方法。下面的示例演示如何在表上定义窗口上方聚合。

```java
Table table = input
  .window([OverWindow w].as("w"))           // define over window with alias w
  .select($("a"), $("b").sum().over($("w")), $("c").min().over($("w"))); // aggregate over the over window w
```

```scala
val table = input
  .window([w: OverWindow] as $"w")              // define over window with alias w
  .select($"a", $"b".sum over $"w", $"c".min over $"w") // aggregate over the over window w
```

OverWindow定义了计算聚合的行范围。OverWindow不是一个用户可以实现的界面。相反，表API提供了Over类来配置Over窗口的属性。可以在事件时间或处理时间以及指定为时间间隔或行计数的范围上定义窗口。支持的over窗口定义作为over(和其他类)上的方法公开，如下所示:

| Method      | Required | Description |
| ----------- | -------- | ----------- |
| partitionBy |          |             |
| orderBy     |          |             |
| preceding   |          |             |
| following   |          |             |
| as          |          |             |

##### Unbounded Over Windows

```java
// Unbounded Event-time over window (assuming an event-time attribute "rowtime")
.window(Over.partitionBy($("a")).orderBy($("rowtime")).preceding(UNBOUNDED_RANGE).as("w"));

// Unbounded Processing-time over window (assuming a processing-time attribute "proctime")
.window(Over.partitionBy($("a")).orderBy("proctime").preceding(UNBOUNDED_RANGE).as("w"));

// Unbounded Event-time Row-count over window (assuming an event-time attribute "rowtime")
.window(Over.partitionBy($("a")).orderBy($("rowtime")).preceding(UNBOUNDED_ROW).as("w"));
 
// Unbounded Processing-time Row-count over window (assuming a processing-time attribute "proctime")
.window(Over.partitionBy($("a")).orderBy($("proctime")).preceding(UNBOUNDED_ROW).as("w"));
```

##### Bounded Over Windows

```java
// Bounded Event-time over window (assuming an event-time attribute "rowtime")
.window(Over.partitionBy($("a")).orderBy($("rowtime")).preceding(lit(1).minutes()).as("w"))

// Bounded Processing-time over window (assuming a processing-time attribute "proctime")
.window(Over.partitionBy($("a")).orderBy($("proctime")).preceding(lit(1).minutes()).as("w"))

// Bounded Event-time Row-count over window (assuming an event-time attribute "rowtime")
.window(Over.partitionBy($("a")).orderBy($("rowtime")).preceding(rowInterval(10)).as("w"))
 
// Bounded Processing-time Row-count over window (assuming a processing-time attribute "proctime")
.window(Over.partitionBy($("a")).orderBy($("proctime")).preceding(rowInterval(10)).as("w"))
```

#### Row-based Operations

##### Map

> 支持：`Bath` `Streaming`
>
> Description:
>
> Performs a map operation with a user-defined scalar function or built-in scalar function. The output will be flattened if the output type is a composite type.
>
> java:
>
> ```java
> public class MyMapFunction extends ScalarFunction {
>     public Row eval(String a) {
>         return Row.of(a, "pre-" + a);
>     }
> 
>     @Override
>     public TypeInformation<?> getResultType(Class<?>[] signature) {
>         return Types.ROW(Types.STRING(), Types.STRING());
>     }
> }
> 
> ScalarFunction func = new MyMapFunction();
> tableEnv.registerFunction("func", func);
> 
> Table table = input
>   .map(call("func", $("c")).as("a", "b"))
> ```
>
> scala:
>
> ```scala
> public class MyMapFunction extends ScalarFunction {
>     public Row eval(String a) {
>         return Row.of(a, "pre-" + a);
>     }
> 
>     @Override
>     public TypeInformation<?> getResultType(Class<?>[] signature) {
>         return Types.ROW(Types.STRING(), Types.STRING());
>     }
> }
> 
> ScalarFunction func = new MyMapFunction();
> tableEnv.registerFunction("func", func);
> 
> Table table = input
>   .map(call("func", $("c")).as("a", "b"))
> ```

##### FlatMap

> 支持：`Bath` `Streaming`
>
> Description:
>
> Performs a flatMap operation with a table function.
>
> java:
>
> ```java
> public class MyFlatMapFunction extends TableFunction<Row> {
> 
>     public void eval(String str) {
>         if (str.contains("#")) {
>             String[] array = str.split("#");
>             for (int i = 0; i < array.length; ++i) {
>                 collect(Row.of(array[i], array[i].length()));
>             }
>         }
>     }
> 
>     @Override
>     public TypeInformation<Row> getResultType() {
>         return Types.ROW(Types.STRING(), Types.INT());
>     }
> }
> 
> TableFunction func = new MyFlatMapFunction();
> tableEnv.registerFunction("func", func);
> 
> Table table = input
>   .flatMap(call("func", $("c")).as("a", "b"))
> ```
>
> scala:
>
> ```scala
> class MyFlatMapFunction extends TableFunction[Row] {
>   def eval(str: String): Unit = {
>     if (str.contains("#")) {
>       str.split("#").foreach({ s =>
>         val row = new Row(2)
>         row.setField(0, s)
>         row.setField(1, s.length)
>         collect(row)
>       })
>     }
>   }
> 
>   override def getResultType: TypeInformation[Row] = {
>     Types.ROW(Types.STRING, Types.INT)
>   }
> }
> 
> val func = new MyFlatMapFunction
> val table = input
>   .flatMap(func($"c")).as("a", "b")
> ```

##### Aggregate

> 支持：`Bath` `Streaming`
>
> Description:
>
> Performs an aggregate operation with an aggregate function. You have to close the "aggregate" with a select statement and the select statement does not support aggregate functions. The output of aggregate will be flattened if the output type is a composite type.
>
> java:
>
> ```java
> public class MyMinMaxAcc {
>     public int min = 0;
>     public int max = 0;
> }
> 
> public class MyMinMax extends AggregateFunction<Row, MyMinMaxAcc> {
> 
>     public void accumulate(MyMinMaxAcc acc, int value) {
>         if (value < acc.min) {
>             acc.min = value;
>         }
>         if (value > acc.max) {
>             acc.max = value;
>         }
>     }
> 
>     @Override
>     public MyMinMaxAcc createAccumulator() {
>         return new MyMinMaxAcc();
>     }
> 
>     public void resetAccumulator(MyMinMaxAcc acc) {
>         acc.min = 0;
>         acc.max = 0;
>     }
> 
>     @Override
>     public Row getValue(MyMinMaxAcc acc) {
>         return Row.of(acc.min, acc.max);
>     }
> 
>     @Override
>     public TypeInformation<Row> getResultType() {
>         return new RowTypeInfo(Types.INT, Types.INT);
>     }
> }
> 
> AggregateFunction myAggFunc = new MyMinMax();
> tableEnv.registerFunction("myAggFunc", myAggFunc);
> Table table = input
>   .groupBy($("key"))
>   .aggregate(call("myAggFunc", $("a")).as("x", "y"))
>   .select($("key"), $("x"), $("y"))
> ```
>
> scala:
>
> ```scala
> case class MyMinMaxAcc(var min: Int, var max: Int)
> 
> class MyMinMax extends AggregateFunction[Row, MyMinMaxAcc] {
> 
>   def accumulate(acc: MyMinMaxAcc, value: Int): Unit = {
>     if (value < acc.min) {
>       acc.min = value
>     }
>     if (value > acc.max) {
>       acc.max = value
>     }
>   }
> 
>   override def createAccumulator(): MyMinMaxAcc = MyMinMaxAcc(0, 0)
> 
>   def resetAccumulator(acc: MyMinMaxAcc): Unit = {
>     acc.min = 0
>     acc.max = 0
>   }
> 
>   override def getValue(acc: MyMinMaxAcc): Row = {
>     Row.of(Integer.valueOf(acc.min), Integer.valueOf(acc.max))
>   }
> 
>   override def getResultType: TypeInformation[Row] = {
>     new RowTypeInfo(Types.INT, Types.INT)
>   }
> }
> 
> val myAggFunc = new MyMinMax
> val table = input
>   .groupBy($"key")
>   .aggregate(myAggFunc($"a") as ("x", "y"))
>   .select($"key", $"x", $"y")c
> ```

##### Group Window Aggregatec

> 支持：`Bath` `Streaming`
>
> Description:
>
> Groups and aggregates a table on a group window and possibly one or more grouping keys. You have to close the "aggregate" with a select statement. And the select statement does not support "*" or aggregate functions.
>
> java:
>
> ```java
> AggregateFunction myAggFunc = new MyMinMax();
> tableEnv.registerFunction("myAggFunc", myAggFunc);
> 
> Table table = input
>     .window(Tumble.over(lit(5).minutes())
>                   .on($("rowtime"))
>                   .as("w")) // define window
>     .groupBy($("key"), $("w")) // group by key and window
>     .aggregate(call("myAggFunc", $("a")).as("x", "y"))
>     .select($("key"), $("x"), $("y"), $("w").start(), $("w").end()); // access window properties and aggregate results
> ```
>
> scala:
>
> ```scala
> val myAggFunc = new MyMinMax
> val table = input
>     .window(Tumble over 5.minutes on $"rowtime" as "w") // define window
>     .groupBy($"key", $"w") // group by key and window
>     .aggregate(myAggFunc($"a") as ("x", "y"))
>     .select($"key", $"x", $"y", $"w".start, $"w".end) // access window properties and aggregate results
> ```

##### FlatAggregate

> 支持：`Bath` `Streaming`
>
> Description:
>
> 
>
> java:
>
> ```java
> /**
>  * Accumulator for Top2.
>  */
> public class Top2Accum {
>     public Integer first;
>     public Integer second;
> }
> 
> /**
>  * The top2 user-defined table aggregate function.
>  */
> public class Top2 extends TableAggregateFunction<Tuple2<Integer, Integer>, Top2Accum> {
> 
>     @Override
>     public Top2Accum createAccumulator() {
>         Top2Accum acc = new Top2Accum();
>         acc.first = Integer.MIN_VALUE;
>         acc.second = Integer.MIN_VALUE;
>         return acc;
>     }
> 
> 
>     public void accumulate(Top2Accum acc, Integer v) {
>         if (v > acc.first) {
>             acc.second = acc.first;
>             acc.first = v;
>         } else if (v > acc.second) {
>             acc.second = v;
>         }
>     }
> 
>     public void merge(Top2Accum acc, java.lang.Iterable<Top2Accum> iterable) {
>         for (Top2Accum otherAcc : iterable) {
>             accumulate(acc, otherAcc.first);
>             accumulate(acc, otherAcc.second);
>         }
>     }
> 
>     public void emitValue(Top2Accum acc, Collector<Tuple2<Integer, Integer>> out) {
>         // emit the value and rank
>         if (acc.first != Integer.MIN_VALUE) {
>             out.collect(Tuple2.of(acc.first, 1));
>         }
>         if (acc.second != Integer.MIN_VALUE) {
>             out.collect(Tuple2.of(acc.second, 2));
>         }
>     }
> }
> 
> tEnv.registerFunction("top2", new Top2());
> Table orders = tableEnv.from("Orders");
> Table result = orders
>     .groupBy($("key"))
>     .flatAggregate(call("top2", $("a")).as("v", "rank"))
>     .select($("key"), $("v"), $("rank");
> ```
>
> scala:
>
> ```scala
> import java.lang.{Integer => JInteger}
> import org.apache.flink.table.api.Types
> import org.apache.flink.table.functions.TableAggregateFunction
> 
> /**
>  * Accumulator for top2.
>  */
> class Top2Accum {
>   var first: JInteger = _
>   var second: JInteger = _
> }
> 
> /**
>  * The top2 user-defined table aggregate function.
>  */
> class Top2 extends TableAggregateFunction[JTuple2[JInteger, JInteger], Top2Accum] {
> 
>   override def createAccumulator(): Top2Accum = {
>     val acc = new Top2Accum
>     acc.first = Int.MinValue
>     acc.second = Int.MinValue
>     acc
>   }
> 
>   def accumulate(acc: Top2Accum, v: Int) {
>     if (v > acc.first) {
>       acc.second = acc.first
>       acc.first = v
>     } else if (v > acc.second) {
>       acc.second = v
>     }
>   }
> 
>   def merge(acc: Top2Accum, its: JIterable[Top2Accum]): Unit = {
>     val iter = its.iterator()
>     while (iter.hasNext) {
>       val top2 = iter.next()
>       accumulate(acc, top2.first)
>       accumulate(acc, top2.second)
>     }
>   }
> 
>   def emitValue(acc: Top2Accum, out: Collector[JTuple2[JInteger, JInteger]]): Unit = {
>     // emit the value and rank
>     if (acc.first != Int.MinValue) {
>       out.collect(JTuple2.of(acc.first, 1))
>     }
>     if (acc.second != Int.MinValue) {
>       out.collect(JTuple2.of(acc.second, 2))
>     }
>   }
> }
> 
> val top2 = new Top2
> val orders: Table = tableEnv.from("Orders")
> val result = orders
>     .groupBy($"key")
>     .flatAggregate(top2($"a") as ($"v", $"rank"))
>     .select($"key", $"v", $"rank")
> ```

##### Group Window FlatAggregate

> 支持：`Bath` `Streaming`
>
> Description:
>
> Groups and aggregates a table on a group window and possibly one or more grouping keys. You have to close the "flatAggregate" with a select statement. And the select statement does not support aggregate functions.
>
> java:c
>
> ```java
> tableEnv.registerFunction("top2", new Top2());
> Table orders = tableEnv.from("Orders");
> Table result = orders
>     .window(Tumble.over(lit(5).minutes())
>                   .on($("rowtime"))
>                   .as("w")) // define window
>     .groupBy($("a"), $("w")) // group by key and window
>     .flatAggregate(call("top2", $("b").as("v", "rank"))
>     .select($("a"), $("w").start(), $("w").end(), $("w").rowtime(), $("v"), $("rank")); // access window properties and aggregate results
> ```
>
> scala:
>
> ```scala
> val top2 = new Top2
> val orders: Table = tableEnv.from("Orders")
> val result = orders
>     .window(Tumble over 5.minutes on $"rowtime" as "w") // define window
>     .groupBy($"a", $"w") // group by key and window
>     .flatAggregate(top2($"b") as ($"v", $"rank"))
>     .select($"a", w.start, $"w".end, $"w".rowtime, $"v", $"rank") // access window properties and aggregate results
> ```



## SQL

## 函数（Functions）

Flink Table 和 SQL内置了很多SQL中支持的函数；如果有无法满足的需要，则可以实现用户自定义的函数（UDF）来解决。

#### 系统内置函数

Flink Table API 和 SQL为用户提供了一组用于数据转换的内置函数。SQL中支持的很多函数，Table API和SQL都已经做了实现，其它还在快速开发扩展中。

以下是一些典型函数的举例，全部的内置函数，可以参考官网介绍。

![image-20200803093121021](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200803093121021.png)

![image-20200803093237092](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200803093237092.png)



#### 日期函数

| 函数名         | 返回类型  | 解释                                                         | 例子                                                      |
| -------------- | --------- | ------------------------------------------------------------ | --------------------------------------------------------- |
| LOCALTIMESTAMP | timestamp | 返回当前系统的时间戳                                         | 2018-07-27 14:04:38.998                                   |
| TO_TIMESTAMP   | timestamp | 将BIGINT类型的日期或者VARCHAR类型的日期转换成TIMESTAMP类型。 | 见下图                                                    |
| MONTH          | BIGINT    | 返回输入时间参数中的“月”，范围1～12。                        | BIGINT MONTH(TIMESTAMP timestamp) BIGINT MONTH(DATE date) |
| DATE_FORMAT    | VARCHAR   | 将字符串类型的日期从源格式转换至目标格式。如果有参数为NULL或解析错误，则返回NULL | 见下图                                                    |
|                |           |                                                              |                                                           |
|                |           |                                                              |                                                           |
|                |           |                                                              |                                                           |
|                |           |                                                              |                                                           |

##### TO_TIMESTAMP

![image-20200805171643852](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200805171643852.png)

##### DATE_FORMAT

![image-20200805170701934](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200805170701934.png)

#### 用户定义函数

用户定义函数（User-defined Functions，UDF）是一个重要的特性，因为它们显著地扩展了查询（Query）的表达能力。一些系统内置函数无法解决的需求，我们可以用UDF来自定义实现。

##### 注册用户自定义函数UDF

在大多数情况下，用户定义的函数必须先注册，然后才能在查询中使用。不需要专门为Scala 的Table API注册函数。

函数通过调用registerFunction（）方法在TableEnvironment中注册。当用户定义的函数被注册时，它被插入到TableEnvironment的函数目录中，这样Table API或SQL解析器就可以识别并正确地解释它。

##### 标量函数（Scalar Functions）

用户定义的标量函数，可以将0、1或多个标量值，映射到新的标量值。

为了定义标量函数，必须在org.apache.flink.table.functions中扩展基类Scalar Function，并实现（一个或多个）求值（evaluation，eval）方法。标量函数的行为由求值方法决定，求值方法必须公开声明并命名为eval（直接def声明，没有override）。求值方法的参数类型和返回类型，确定了标量函数的参数和返回类型。

在下面的代码中，我们定义自己的HashCode函数，在TableEnvironment中注册它，并在查询中调用它。

```{.scala}
// 自定义一个标量函数
class HashCode( factor: Int ) extends ScalarFunction {
  def eval( s: String ): Int = {
    s.hashCode * factor
  }
}
```

主函数中调用，计算sensor id的哈希值（前面部分照抄，流环境、表环境、读取source、建表）：

```{.scala .numberLines}
package com.atguigu.course

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.api.scala._
import org.apache.flink.table.api.{EnvironmentSettings, Tumble}
import org.apache.flink.table.api.scala._
import org.apache.flink.table.functions.ScalarFunction

object TableUDFExample1 {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    val settings = EnvironmentSettings.newInstance()
      .useBlinkPlanner()
      .inStreamingMode()
      .build()
    val tEnv = StreamTableEnvironment.create(env, settings)
    env.setParallelism(1)
    val stream = env.addSource(new SensorSource)
    val hashCode = new HashCode(10)
    tEnv.registerFunction("hashCode", new HashCode(10))
    val table = tEnv.fromDataStream(stream, 'id)
    // table api 写法
    table
      .select('id, hashCode('id))
      .toAppendStream[(String, Int)]
      .print()

    // sql 写法
    tEnv.createTemporaryView("t", table, 'id)
    tEnv
      .sqlQuery("SELECT id, hashCode(id) FROM t")
      .toAppendStream[(String, Int)]
      .print()

    env.execute()
  }

  class HashCode(factor: Int) extends ScalarFunction {
    def eval(s: String): Int = {
      s.hashCode() * factor
    }
  }
  
}
```

##### 表函数（Table Functions）

与用户定义的标量函数类似，用户定义的表函数，可以将0、1或多个标量值作为输入参数；与标量函数不同的是，它可以返回任意数量的行作为输出，而不是单个值。

为了定义一个表函数，必须扩展org.apache.flink.table.functions中的基类TableFunction并实现（一个或多个）求值方法。表函数的行为由其求值方法决定，求值方法必须是public的，并命名为eval。求值方法的参数类型，决定表函数的所有有效参数。

返回表的类型由TableFunction的泛型类型确定。求值方法使用protected collect（T）方法发出输出行。

在Table API中，Table函数需要与.joinLateral或.leftOuterJoinLateral一起使用。

joinLateral算子，会将外部表中的每一行，与表函数（TableFunction，算子的参数是它的表达式）计算得到的所有行连接起来。

而leftOuterJoinLateral算子，则是左外连接，它同样会将外部表中的每一行与表函数计算生成的所有行连接起来；并且，对于表函数返回的是空表的外部行，也要保留下来。

在SQL中，则需要使用Lateral Table（<TableFunction>），或者带有ON TRUE条件的左连接。

下面的代码中，我们将定义一个表函数，在表环境中注册它，并在查询中调用它。

自定义TableFunction：

```{.scala}
// 自定义TableFunction
class Split(separator: String) extends TableFunction[(String, Int)]{
  def eval(str: String): Unit = {
    str.split(separator).foreach(
      word => collect((word, word.length))
    )
  }
}
```

接下来，就是在代码中调用。首先是Table API的方式：

```{.scala}
// Table API中调用，需要用joinLateral
val resultTable = sensorTable
  .joinLateral(split('id) as ('word, 'length))   // as对输出行的字段重命名
  .select('id, 'word, 'length)
  
// 或者用leftOuterJoinLateral
val resultTable2 = sensorTable
  .leftOuterJoinLateral(split('id) as ('word, 'length))
  .select('id, 'word, 'length)
  
// 转换成流打印输出
resultTable.toAppendStream[Row].print("1")
resultTable2.toAppendStream[Row].print("2")
```

然后是SQL的方式：

```{.scala}
tableEnv.createTemporaryView("sensor", sensorTable)
tableEnv.registerFunction("split", split)

val resultSqlTable = tableEnv.sqlQuery(
  """
    |select id, word, length
    |from
    |sensor, LATERAL TABLE(split(id)) AS newsensor(word, length)
  """.stripMargin)
    
// 或者用左连接的方式
val resultSqlTable2 = tableEnv.sqlQuery(
  """
    |SELECT id, word, length
    |FROM
    |sensor
    |  LEFT JOIN 
    |  LATERAL TABLE(split(id)) AS newsensor(word, length) 
    |  ON TRUE
  """.stripMargin)

// 转换成流打印输出
resultSqlTable.toAppendStream[Row].print("1")
resultSqlTable2.toAppendStream[Row].print("2")
```

##### 聚合函数（Aggregate Functions）

用户自定义聚合函数（User-Defined Aggregate Functions，UDAGGs）可以把一个表中的数据，聚合成一个标量值。用户定义的聚合函数，是通过继承AggregateFunction抽象类实现的。

![](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/udagg-mechanism.png){ width=100% }

上图中显示了一个聚合的例子。

假设现在有一张表，包含了各种饮料的数据。该表由三列（id、name和price）、五行组成数据。现在我们需要找到表中所有饮料的最高价格，即执行max（）聚合，结果将是一个数值。

AggregateFunction的工作原理如下。

* 首先，它需要一个累加器，用来保存聚合中间结果的数据结构（状态）。可以通过调用AggregateFunction的createAccumulator（）方法创建空累加器。
* 随后，对每个输入行调用函数的accumulate（）方法来更新累加器。
* 处理完所有行后，将调用函数的getValue（）方法来计算并返回最终结果。

AggregationFunction要求必须实现的方法：

* createAccumulator()
* accumulate()
* getValue()

除了上述方法之外，还有一些可选择实现的方法。其中一些方法，可以让系统执行查询更有效率，而另一些方法，对于某些场景是必需的。例如，如果聚合函数应用在会话窗口（session group window）的上下文中，则merge（）方法是必需的。

* retract() 
* merge() 
* resetAccumulator()

接下来我们写一个自定义AggregateFunction，计算一下每个sensor的平均温度值。

```{.scala}
// 定义AggregateFunction的Accumulator
class AvgTempAcc {
  var sum: Double = 0.0
  var count: Int = 0
}

class AvgTemp extends AggregateFunction[Double, AvgTempAcc] {
  override def getValue(accumulator: AvgTempAcc): Double = accumulator.sum / accumulator.count

  override def createAccumulator(): AvgTempAcc = new AvgTempAcc
  
  def accumulate(accumulator: AvgTempAcc, temp: Double): Unit ={
    accumulator.sum += temp
    accumulator.count += 1
  }
}
```

接下来就可以在代码中调用了。

```{.scala}
// 创建一个聚合函数实例
val avgTemp = new AvgTemp()
// Table API的调用
val resultTable = sensorTable
  .groupBy('id)
  .aggregate(avgTemp('temperature) as 'avgTemp)
  .select('id, 'avgTemp)
  
// SQL的实现
tableEnv.createTemporaryView("sensor", sensorTable)
tableEnv.registerFunction("avgTemp", avgTemp)
val resultSqlTable = tableEnv.sqlQuery(
  """
    |SELECT
    |id, avgTemp(temperature)
    |FROM
    |sensor
    |GROUP BY id
  """.stripMargin)
  
// 转换成流打印输出
resultTable.toRetractStream[(String, Double)].print("agg temp")
resultSqlTable.toRetractStream[Row].print("agg temp sql")
```

##### 表聚合函数（Table Aggregate Functions）

用户定义的表聚合函数（User-Defined Table Aggregate Functions，UDTAGGs），可以把一个表中数据，聚合为具有多行和多列的结果表。这跟AggregateFunction非常类似，只是之前聚合结果是一个标量值，现在变成了一张表。

![](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/udtagg-mechanism.png){ width=100% }

比如现在我们需要找到表中所有饮料的前2个最高价格，即执行top2()表聚合。我们需要检查5行中的每一行，得到的结果将是一个具有排序后前2个值的表。

用户定义的表聚合函数，是通过继承TableAggregateFunction抽象类来实现的。

TableAggregateFunction的工作原理如下。

* 首先，它同样需要一个累加器（Accumulator），它是保存聚合中间结果的数据结构。通过调用TableAggregateFunction的createAccumulator()方法可以创建空累加器。
* 随后，对每个输入行调用函数的accumulate()方法来更新累加器。
* 处理完所有行后，将调用函数的emitValue()方法来计算并返回最终结果。

AggregationFunction要求必须实现的方法：

* createAccumulator()
* accumulate()

除了上述方法之外，还有一些可选择实现的方法。

* retract() 
* merge()  
* resetAccumulator() 
* emitValue() 
* emitUpdateWithRetract()

接下来我们写一个自定义TableAggregateFunction，用来提取每个sensor最高的两个温度值。

```{.scala}
// 先定义一个 Accumulator
class Top2TempAcc{
  var highestTemp: Double = Int.MinValue
  var secondHighestTemp: Double = Int.MinValue
}

// 自定义 TableAggregateFunction
class Top2Temp extends TableAggregateFunction[(Double, Int), Top2TempAcc]{
  
  override def createAccumulator(): Top2TempAcc = new Top2TempAcc
  
  def accumulate(acc: Top2TempAcc, temp: Double): Unit ={
    if( temp > acc.highestTemp ){
      acc.secondHighestTemp = acc.highestTemp
      acc.highestTemp = temp
    } else if( temp > acc.secondHighestTemp ){
      acc.secondHighestTemp = temp
    }
  }
  
  def emitValue(acc: Top2TempAcc, out: Collector[(Double, Int)]): Unit ={
    out.collect(acc.highestTemp, 1)
    out.collect(acc.secondHighestTemp, 2)
  }
}
```

接下来就可以在代码中调用了。

```{.scala}
// 创建一个表聚合函数实例
val top2Temp = new Top2Temp()
// Table API的调用
val resultTable = sensorTable
  .groupBy('id)
  .flatAggregate( top2Temp('temperature) as ('temp, 'rank) )
  .select('id, 'temp, 'rank)
  
// 转换成流打印输出
resultTable.toRetractStream[(String, Double, Int)].print("agg temp")
resultSqlTable.toRetractStream[Row].print("agg temp sql")
```



## scala案例代码

#### Example.scala   

从文件获取数据，用sql处理数据，转成流打印到控制台

```scala
import com.wxl.apitest.SensorReading
import org.apache.flink.streaming.api.scala._
import org.apache.flink.table.api.Table
import org.apache.flink.table.api.scala._

object Example {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)

    // 从文件读取数据
    val inputStream: DataStream[String]  = env.readTextFile("E:\\Projects\\FlinkStudy\\src\\main\\resources\\sensor.txt")

    val dataStream = inputStream.map(data => {
      val dataArray = data.split(",")
      SensorReading(dataArray(0), dataArray(1).trim.toLong, dataArray(2).trim.toDouble)
    })

    // 基于env创建表环境
    val tableEnv = StreamTableEnvironment.create(env)

    // 将流转换为表
    val dataTable: Table = tableEnv.fromDataStream(dataStream)

    // 调用table api做转换操作
    val resultTable: Table = dataTable.select("id, temperature").filter("id == 'sensor_1'")

    // 写sql
//    tableEnv.registerTable("dataTable", dataTable)    // 已经弃用
    tableEnv.createTemporaryView("dataTable", dataTable)
    val resulteSqlTable: Table = tableEnv.sqlQuery(
      """
        |select id, temperature
        |from dataTable
        |where id = 'sensor_1'
      """.stripMargin)

    // 把表转换成流打印
    val resultStream: DataStream[(String, Double)] = resulteSqlTable.toAppendStream[(String, Double)]

    resultStream.print()

    env.execute("table api")
  }
}
```

#### TableApiTest.scala

```scala
package com.wxl.tabletest

import org.apache.flink.streaming.api.scala._
import org.apache.flink.table.api.{DataTypes, EnvironmentSettings, Table, TableEnvironment}
import org.apache.flink.table.api.scala._
import org.apache.flink.table.descriptors._

object TableApiTest {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    // 1.创建表执行环境
    val tableEnv = StreamTableEnvironment.create(env)

//    // 1.1老版本planner的流式查询
//    val settings: EnvironmentSettings = EnvironmentSettings.newInstance()
//      .useOldPlanner()   // 用老版本
//      .inStreamingMode() // 流处理模式
//      .build()
//    val oldStreamTableEnv = StreamTableEnvironment.create(env, settings)
//
//    // 1.2老版本批处理环境
//    val batchEnv: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment
//    val batchTableEnv: BatchTableEnvironment = BatchTableEnvironment.create(batchEnv)
//
//    // 1.3 blink版本流式查询
//    val blinkSettings = EnvironmentSettings.newInstance()
//      .useBlinkPlanner()
//      .inStreamingMode()
//      .build()
//    val blinkStreamTableEnv = StreamTableEnvironment.create(env, blinkSettings)
//
//    // 1.4 blink版本批式查询
//    val blinkBatchSettings = EnvironmentSettings.newInstance()
//      .useBlinkPlanner()
//      .inBatchMode()
//      .build()
//    val blinkBatchTableEnv = TableEnvironment.create(blinkBatchSettings)

    // 2. 连接外部系统，读取数据
    // 2.1 读取文件数据
    val filePath = "E:\\Projects\\FlinkStudy\\src\\main\\resources\\sensor.txt"

    tableEnv.connect(new FileSystem().path(filePath))
      .withFormat(new OldCsv())   // 定义从外部文件读取数据之后的格式化方法
      .withSchema(new Schema()
        .field("id", DataTypes.STRING())
        .field("time", DataTypes.BIGINT())
        .field("temperature", DataTypes.DOUBLE())
      ) // 定义表的结果
      .createTemporaryTable("inputTable")   // 在表环境里注册一个表

    // 2.2 消费kafka数据
    tableEnv.connect(new Kafka()
      .version("0.11")  // 定义版本
      .topic("sensor")  // 定义主题
      .property("zookeeper.connect", "192.168.17.130:2181")
      .property("bootstrap.servers", "192.168.17.130:9092")
    )
      .withFormat( new Csv()
        .fieldDelimiter(',')         // optional: field delimiter character (',' by default)
        .lineDelimiter("\r\n")       // optional: line delimiter ("\n" by default; otherwise "\r", "\r\n", or "" are allowed)
      ) // 定义从外部文件读取数据之后的格式化方法
      .withSchema(new Schema()
      .field("id", DataTypes.STRING())
      .field("time", DataTypes.BIGINT())
      .field("temperature", DataTypes.DOUBLE())
    ) // 定义表的结果
      .createTemporaryTable("kafkaInputTable")


    // 3. 表的查询转换
    val sensorTable: Table = tableEnv.from("inputTable")
    // 3.1 简单查询转换
    val resultTable: Table = sensorTable
      .select('id, 'temperature)
      .filter('id === "sensor_1")

    // 3.2.1 聚合转换
    val aggResultTable: Table = sensorTable
      .groupBy('id)
      .select('id, 'id.count as 'count)

    // 3.2.2 用sql实现
    val aggResultSqlTable: Table = tableEnv
        .sqlQuery("select id, count(id) as cnt from inputTable group by id")

    // 转成流输出

    resultTable.toAppendStream[(String, Double)].print("result")
    aggResultSqlTable.toRetractStream[(String, Long)].print("agg result")

    env.execute("table api test job")
  }
}
```

#### FsOutputTest.scala

```scala
package com.wxl.tabletest

import com.wxl.apitest.SensorReading
import org.apache.flink.streaming.api.scala._
import org.apache.flink.table.api.{DataTypes, Table}
import org.apache.flink.table.api.scala._
import org.apache.flink.table.descriptors.{Csv, FileSystem, Schema}

object FsOutputTest {
  def main(args: Array[String]): Unit = {
    // 1.创建数据流环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    val tableEnv = StreamTableEnvironment.create(env)

    // 2.从文件读取数据到流，map成样例类
    val inputStream: DataStream[String]  = env.readTextFile("E:\\Projects\\FlinkStudy\\src\\main\\resources\\sensor.txt")

    val dataStream = inputStream.map(data => {
      val dataArray = data.split(",")
      SensorReading(dataArray(0), dataArray(1).trim.toLong, dataArray(2).trim.toDouble)
    })

    // 3. 把流转换成表
    val sensorTable: Table = tableEnv
      .fromDataStream(dataStream, 'id, 'temperature as 'temp, 'time as 'ts)

    // 4. 进行表的转换操作
    tableEnv.createTemporaryView("sensorTable", sensorTable)
    val resultTable: Table = tableEnv.sqlQuery("select id, temp from sensorTable where id = 'sensor_1'")
//    val resultTable2: Table = tableEnv.sqlQuery("select id, count(id) as cnt from sensorTable where id = 'sensor_1' group by id")

    // 5. 将结果输出到文件中
    tableEnv.connect(new FileSystem().path("E:\\Projects\\FlinkStudy\\src\\main\\resources\\output.txt"))
      .withFormat( new Csv)
      .withSchema( new Schema()
        .field("id", DataTypes.STRING())
        .field("temp", DataTypes.DOUBLE())
      )
      .createTemporaryTable("outputTable")

    resultTable.insertInto("outputTable")

    env.execute("fs output test job")
  }
}
```



## Table API的窗口聚合操作

### 引入依赖

```xml
<dependency>
<groupId>org.apache.flink</groupId>
<artifactId>flink-table_2.11</artifactId>
<version>1.7.2</version>
</dependency>
```



#### 案例

```scala
// 每 10 秒中渠道为 appstore 的个数
def main(args: Array[String]): Unit = {
  //sparkcontext
  val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
  
  // 时间特性改为 eventTime
  env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
  
  val myKafkaConsumer: FlinkKafkaConsumer011[String] = MyKafkaUtil.getConsumer("ECOMMERCE")
  val dstream: DataStream[String] = env.addSource(myKafkaConsumer)
  val ecommerceLogDstream: DataStream[EcommerceLog] = dstream.map{ jsonString =>JSON.parseObject(jsonString,classOf[EcommerceLog]) }
  
  // 告知 watermark  和 eventTime 如何提取
  val ecommerceLogWithEventTimeDStream: DataStream[EcommerceLog] = ecommerceLogDstream.assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor[EcommerceLog](Time.seconds(0L)) {
    override def extractTimestamp(element: EcommerceLog): Long = {
      element.ts
    }
  }).setParallelism(1)
  val tableEnv: StreamTableEnvironment = TableEnvironment.getTableEnvironment(env)
  
  // 把数据流转化成 Table
  val ecommerceTable: Table = tableEnv.fromDataStream(ecommerceLogWithEventTimeDStream , 'mid,'uid,'appid,'area,'os,'ch,'logType,'vs,'logDate,'logHour,'logHourMinute,'ts.rowtime)
  
  // 通过 table api  进行操作
  //  每 10 秒 统计一次各个渠道的个数 table api  解决
  //1 groupby 2  要用 window 3  用 eventtime 来确定开窗时间
  val resultTable: Table = ecommerceTable.window(Tumble over 10000.millis on 'ts as 'tt).groupBy('ch,'tt ).select( 'ch, 'ch.count)
  
  // 把 Table 转化成数据流
  val resultDstream: DataStream[(Boolean, (String, Long))] = resultSQLTable.toRetractStream[(String,Long)]
  resultDstream.filter(_._1).print()
  env.execute()
}
```

####  关于 group by

1. 如果了使用 groupby，table 转换为流的时候只能用 toRetractDstream

```scala
val rDstream: DataStream[(Boolean, (String, Long))] = table
			.toRetractStream[(String,Long)]
```

2. toRetractDstream 得到的第一个 boolean 型字段标识 true 就是最新的数据(Insert)，false 表示过期老数据(Delete)

```scala
val rDstream: DataStream[(Boolean, (String, Long))] = table
			.toRetractStream[(String,Long)]
			rDstream.filter(_._1).print()
```

3. 如果使用的 api 包括时间窗口，那么窗口的字段必须出现在 groupBy 中

```scala
val table: Table = ecommerceLogTable
      .filter("ch ='appstore'")
      .window(Tumble over 10000.millis on 'ts as 'tt)
      .groupBy('ch ,'tt)
      .select("ch,ch.count ")
```

#### 关于时间窗口

1. 用到时间窗口，必须提前声明时间字段，如果是 processTime 直接在创建动态表时进行追加就可以

   ```scala
   val ecommerceLogTable: Table = tableEnv
       .fromDataStream( ecommerceLogWithEtDstream, 'mid,'uid,'appid,'area,'os,'ch,'logType,'vs,'logDate,'logHour,'logHourMinute,'ps.proctime)
   ```

2. 如果是 EventTime 要在创建动态表时声明

   ```scala
   val ecommerceLogTable: Table = tableEnv
       .fromDataStream( ecommerceLogWithEtDstream, 'mid,'uid,'appid,'area,'os,'ch,'logType,'vs,'logDate,'logHour,'logHourMinute,'ps.rowtime)
   ```

3. 滚动窗口可以使用 Tumble over 10000.millis on 来表示

   ```scala
   val table: Table = ecommerceLogTable.filter("ch ='appstore'")
         .window(Tumble over 10000.millis on 'ts as 'tt)
         .groupBy('ch ,'tt)
         .select("ch,ch.count ")
   ```



### SQL编写

```scala
val resultSQLTable : Table = tableEnv.sqlQuery( "select ch ,count(ch) from"+ecommerceTable+" group by ch ,Tumble(ts,interval '10' SECOND )")
```

## 时间特性

基于时间的操作（比如Table API和SQL中窗口操作），需要定义相关的时间语义和时间数据来源的信息。所以，Table可以提供一个逻辑上的时间字段，用于在表处理程序中，指示时间和访问相应的时间戳。

时间属性，可以是每个表schema的一部分。一旦定义了时间属性，它就可以作为一个字段引用，并且可以在基于时间的操作中使用。

时间属性的行为类似于常规时间戳，可以访问，并且进行计算。

### 处理时间（Processing Time）

处理时间语义下，允许表处理程序根据机器的本地时间生成结果。它是时间的最简单概念。它既不需要提取时间戳，也不需要生成watermark。

定义处理时间属性有三种方法：在DataStream转化时直接指定；在定义Table Schema时指定；在创建表的DDL中指定。

1. DataStream转化成Table时指定

由DataStream转换成表时，可以在后面指定字段名来定义Schema。在定义Schema期间，可以使用.proctime，定义处理时间字段。

注意，这个proctime属性只能通过附加逻辑字段，来扩展物理schema。因此，只能在schema定义的末尾定义它。

代码如下：

```{.scala}
// 定义好 DataStream
val inputStream: DataStream[String] = env.readTextFile("\\sensor.txt")
val dataStream: DataStream[SensorReading] = inputStream
  .map(data => {
    val dataArray = data.split(",")
    SensorReading(dataArray(0), dataArray(1).toLong, dataArray(2).toDouble)
  })

// 将 DataStream转换为 Table，并指定时间字段
val sensorTable = tableEnv
  .fromDataStream(dataStream, 'id, 'temperature, 'timestamp, 'pt.proctime)
```

2. 定义Table Schema时指定

这种方法其实也很简单，只要在定义Schema的时候，加上一个新的字段，并指定成proctime就可以了。

代码如下：

```{.scala}
tableEnv
  .connect(
    new FileSystem().path("..\\sensor.txt"))
  .withFormat(new Csv())
  .withSchema(
    new Schema()
      .field("id", DataTypes.STRING())
      .field("timestamp", DataTypes.BIGINT())
      .field("temperature", DataTypes.DOUBLE())
      .field("pt", DataTypes.TIMESTAMP(3))
      .proctime()    // 指定 pt字段为处理时间
  ) // 定义表结构
  .createTemporaryTable("inputTable") // 创建临时表
```

3. 创建表的DDL中指定

在创建表的DDL中，增加一个字段并指定成proctime，也可以指定当前的时间字段。

代码如下：

```{.scala}
val sinkDDL: String =
  """
    |create table dataTable (
    |  id varchar(20) not null,
    |  ts bigint,
    |  temperature double,
    |  pt AS PROCTIME()
    |) with (
    |  'connector.type' = 'filesystem',
    |  'connector.path' = 'file:///D:\\..\\sensor.txt',
    |  'format.type' = 'csv'
    |)
  """.stripMargin

tableEnv.sqlUpdate(sinkDDL) // 执行 DDL
```

注意：运行这段DDL，必须使用Blink Planner。

### 事件时间（Event Time）

事件时间语义，允许表处理程序根据每个记录中包含的时间生成结果。这样即使在有乱序事件或者延迟事件时，也可以获得正确的结果。

为了处理无序事件，并区分流中的准时和迟到事件；Flink需要从事件数据中，提取时间戳，并用来推进事件时间的进展（watermark）。

1. DataStream转化成Table时指定

在DataStream转换成Table，schema的定义期间，使用.rowtime可以定义事件时间属性。注意，必须在转换的数据流中分配时间戳和watermark。

在将数据流转换为表时，有两种定义时间属性的方法。根据指定的.rowtime字段名是否存在于数据流的架构中，timestamp字段可以：

* 作为新字段追加到schema
* 替换现有字段

在这两种情况下，定义的事件时间戳字段，都将保存DataStream中事件时间戳的值。

代码如下：

```{.scala}
val inputStream: DataStream[String] = env.readTextFile("\\sensor.txt")
val dataStream: DataStream[SensorReading] = inputStream
  .map(data => {
    val dataArray = data.split(",")
    SensorReading(dataArray(0), dataArray(1).toLong, dataArray(2).toDouble)
  })
  .assignAscendingTimestamps(_.timestamp * 1000L)

// 将 DataStream转换为 Table，并指定时间字段
val sensorTable = tableEnv
  .fromDataStream(dataStream, 'id, 'timestamp.rowtime, 'temperature)
// 或者，直接追加字段
val sensorTable2 = tableEnv
  .fromDataStream(dataStream, 'id, 'temperature, 'timestamp, 'rt.rowtime)
```

2. 定义Table Schema时指定

这种方法只要在定义Schema的时候，将事件时间字段，并指定成rowtime就可以了。

代码如下：

```{.scala}
tableEnv
  .connect(new FileSystem().path("sensor.txt"))
  .withFormat(new Csv())
  .withSchema(
    new Schema()
      .field("id", DataTypes.STRING())
      .field("timestamp", DataTypes.BIGINT())
      .rowtime(
        new Rowtime()
          .timestampsFromField("timestamp")    // 从字段中提取时间戳
          .watermarksPeriodicBounded(1000)    // watermark延迟1秒
      )
      .field("temperature", DataTypes.DOUBLE())
  ) // 定义表结构
  .createTemporaryTable("inputTable") // 创建临时表
```

3. 创建表的DDL中指定

事件时间属性，是使用CREATE TABLE DDL中的WARDMARK语句定义的。watermark语句，定义现有事件时间字段上的watermark生成表达式，该表达式将事件时间字段标记为事件时间属性。

代码如下：

```{.scala}
val sinkDDL: String =
  """
    |create table dataTable (
    |  id varchar(20) not null,
    |  ts bigint,
    |  temperature double,
    |  rt AS TO_TIMESTAMP( FROM_UNIXTIME(ts) ),
    |  watermark for rt as rt - interval '1' second
    |) with (
    |  'connector.type' = 'filesystem',
    |  'connector.path' = 'file:///D:\\..\\sensor.txt',
    |  'format.type' = 'csv'
    |)
  """.stripMargin

tableEnv.sqlUpdate(sinkDDL) // 执行 DDL
```

这里FROM_UNIXTIME是系统内置的时间函数，用来将一个整数（秒数）转换成“YYYY-MM-DD hh:mm:ss”格式（默认，也可以作为第二个String参数传入）的日期时间字符串（date time string）；然后再用TO_TIMESTAMP将其转换成Timestamp。



## 窗口（Windows）

时间语义，要配合窗口操作才能发挥作用。最主要的用途，当然就是开窗口、根据时间段做计算了。下面我们就来看看Table API和SQL中，怎么利用时间字段做窗口操作。

在Table API和SQL中，主要有两种窗口：Group Windows和Over Windows

### Group Windows

分组窗口（Group Windows）会根据时间或行计数间隔，将行聚合到有限的组（Group）中，并对每个组的数据执行一次聚合函数。

Table API中的Group Windows都是使用.window（w:GroupWindow）子句定义的，并且必须由as子句指定一个别名。为了按窗口对表进行分组，窗口的别名必须在group by子句中，像常规的分组字段一样引用。

```{.scala}
val table = input
  .window([w: GroupWindow] as 'w) // 定义窗口，别名 w
  .groupBy('w, 'a)  // 以属性a和窗口w作为分组的key
  .select('a, 'b.sum)  // 聚合字段b的值，求和
```

或者，还可以把窗口的相关信息，作为字段添加到结果表中：

```{.scala}
val table = input
  .window([w: GroupWindow] as 'w)
  .groupBy('w, 'a) 
  .select('a, 'w.start, 'w.end, 'w.rowtime, 'b.count)
```

Table API提供了一组具有特定语义的预定义Window类，这些类会被转换为底层DataStream或DataSet的窗口操作。

Table API支持的窗口定义，和我们熟悉的一样，主要也是三种：滚动（Tumbling）、滑动（Sliding）和会话（Session）。

### 滚动窗口

滚动窗口（Tumbling windows）要用Tumble类来定义，另外还有三个方法：

* over：定义窗口长度
* on：用来分组（按时间间隔）或者排序（按行数）的时间字段
* as：别名，必须出现在后面的groupBy中

代码如下：

```{.scala}
// Tumbling Event-time Window（事件时间字段rowtime
.window(Tumble over 10.minutes on 'rowtime as 'w)
// Tumbling Processing-time Window（处理时间字段proctime）
.window(Tumble over 10.minutes on 'proctime as 'w)
// Tumbling Row-count Window (类似于计数窗口，按处理时间排序，10行一组)
.window(Tumble over 10.rows on 'proctime as 'w)
```

### 滑动窗口

滑动窗口（Sliding windows）要用Slide类来定义，另外还有四个方法：

* over：定义窗口长度
* every：定义滑动步长
* on：用来分组（按时间间隔）或者排序（按行数）的时间字段
* as：别名，必须出现在后面的groupBy中

代码如下：

```{.scala}
// Sliding Event-time Window
.window(Slide over 10.minutes every 5.minutes on 'rowtime as 'w)
// Sliding Processing-time window
.window(Slide over 10.minutes every 5.minutes on 'proctime as 'w)
// Sliding Row-count window
.window(Slide over 10.rows every 5.rows on 'proctime as 'w)
```

### 会话窗口

会话窗口（Session windows）要用Session类来定义，另外还有三个方法：

* withGap：会话时间间隔
* on：用来分组（按时间间隔）或者排序（按行数）的时间字段
* as：别名，必须出现在后面的groupBy中

代码如下：

```{scala}
// Session Event-time Window
.window(Session withGap 10.minutes on 'rowtime as 'w)
// Session Processing-time Window
.window(Session withGap 10.minutes on 'proctime as 'w)
```

### Over Windows

Over window聚合是标准SQL中已有的（Over子句），可以在查询的SELECT子句中定义。Over window 聚合，会针对每个输入行，计算相邻行范围内的聚合。Over windows使用.window（w:overwindows*）子句定义，并在select()方法中通过别名来引用。

比如这样：

```{.scala}
val table = input
  .window([w: OverWindow] as 'w)
  .select('a, 'b.sum over 'w, 'c.min over 'w)
```

Table API提供了Over类，来配置Over窗口的属性。可以在事件时间或处理时间，以及指定为时间间隔、或行计数的范围内，定义Over windows。

无界的over window是使用常量指定的。也就是说，时间间隔要指定UNBOUNDED_RANGE，或者行计数间隔要指定UNBOUNDED_ROW。而有界的over window是用间隔的大小指定的。

实际代码应用如下：

1. 无界的 over window

```{.scala}
// 无界的事件时间over window (时间字段 "rowtime")
.window(Over partitionBy 'a orderBy 'rowtime preceding UNBOUNDED_RANGE as 'w)
//无界的处理时间over window (时间字段"proctime")
.window(Over partitionBy 'a orderBy 'proctime preceding UNBOUNDED_RANGE as 'w)
// 无界的事件时间Row-count over window (时间字段 "rowtime")
.window(Over partitionBy 'a orderBy 'rowtime preceding UNBOUNDED_ROW as 'w)
//无界的处理时间Row-count over window (时间字段 "rowtime")
.window(Over partitionBy 'a orderBy 'proctime preceding UNBOUNDED_ROW as 'w)
```

2. 有界的over window

```{.scala}
// 有界的事件时间over window (时间字段 "rowtime"，之前1分钟)
.window(Over partitionBy 'a orderBy 'rowtime preceding 1.minutes as 'w)
// 有界的处理时间over window (时间字段 "rowtime"，之前1分钟)
.window(Over partitionBy 'a orderBy 'proctime preceding 1.minutes as 'w)
// 有界的事件时间Row-count over window (时间字段 "rowtime"，之前10行)
.window(Over partitionBy 'a orderBy 'rowtime preceding 10.rows as 'w)
// 有界的处理时间Row-count over window (时间字段 "rowtime"，之前10行)
.window(Over partitionBy 'a orderBy 'proctime preceding 10.rows as 'w)
```

### SQL中窗口的定义

我们已经了解了在Table API里window的调用方式，同样，我们也可以在SQL中直接加入窗口的定义和使用。

#### Group Windows

Group Windows在SQL查询的Group BY子句中定义。与使用常规GROUP BY子句的查询一样，使用GROUP BY子句的查询会计算每个组的单个结果行。

SQL支持以下Group窗口函数:

* TUMBLE(time_attr, interval)

定义一个滚动窗口，第一个参数是时间字段，第二个参数是窗口长度。

* HOP(time_attr, interval, interval)

定义一个滑动窗口，第一个参数是时间字段，第二个参数是窗口滑动步长，第三个是窗口长度。

* SESSION(time_attr, interval)

定义一个会话窗口，第一个参数是时间字段，第二个参数是窗口间隔（Gap）。

另外还有一些辅助函数，可以用来选择Group Window的开始和结束时间戳，以及时间属性。

这里只写TUMBLE_*，滑动和会话窗口是类似的（HOP_*，SESSION_*）。

* TUMBLE_START(time_attr, interval)
* TUMBLE_END(time_attr, interval)
* TUMBLE_ROWTIME(time_attr, interval)
* TUMBLE_PROCTIME(time_attr, interval)

#### Over Windows

由于Over本来就是SQL内置支持的语法，所以这在SQL中属于基本的聚合操作。所有聚合必须在同一窗口上定义，也就是说，必须是相同的分区、排序和范围。目前仅支持在当前行范围之前的窗口（无边界和有边界）。

注意，ORDER BY必须在单一的时间属性上指定。

代码如下：

```{.sql}
SELECT COUNT(amount) OVER (
  PARTITION BY user
  ORDER BY proctime
  ROWS BETWEEN 2 PRECEDING AND CURRENT ROW)
FROM Orders

// 也可以做多个聚合
SELECT COUNT(amount) OVER w, SUM(amount) OVER w
FROM Orders
WINDOW w AS (
  PARTITION BY user
  ORDER BY proctime
  ROWS BETWEEN 2 PRECEDING AND CURRENT ROW)
```



### 案例

TimeAndWindowTest.scala

```scala
package com.wxl.tabletest

import com.wxl.apitest.SensorReading
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.table.api.{Over, Tumble}
import org.apache.flink.table.api.scala._
import org.apache.flink.types.Row

object TimeAndWindowTest {
  def main(args: Array[String]): Unit = {
    // 1.创建数据流环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    val tableEnv = StreamTableEnvironment.create(env)

    // 2.从文件读取数据到流，map成样例类
    val inputStream: DataStream[String]  = env.readTextFile("E:\\Projects\\FlinkStudy\\src\\main\\resources\\sensor.txt")

    val dataStream = inputStream.map(data => {
      val dataArray = data.split(",")
      SensorReading(dataArray(0), dataArray(1).trim.toLong, dataArray(2).trim.toDouble)
    })
      .assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor[SensorReading](Time.seconds(1)) {
        override def extractTimestamp(t: SensorReading): Long = t.time * 1000L
      })

    // 将DataStream转换为Table, 并指定时间字段
//    val sensorTable = tableEnv.fromDataStream(dataStream, 'id, 'temperature, 'time, 'pt.proctime)
    val sensorTable = tableEnv.fromDataStream(dataStream, 'id, 'temperature, 'time, 'rt.rowtime)

//    sensorTable.printSchema()

    // 1 table api
    // 1.1 Group Window聚合操作
    val resultTable = sensorTable
      .window(Tumble over 10.seconds on 'rt as 'tw)
      .groupBy('id, 'tw)
      .select('id, 'id.count, 'tw.end )

//    resultTable.toAppendStream[Row].print("agg")

    // 1.2 Over Window操作
    val overResultTable = sensorTable
        .window(Over partitionBy 'id orderBy 'rt preceding 2.rows as 'ow )
        .select('id, 'rt, 'id.count over 'ow, 'temperature.avg over 'ow)

//    overResultTable.toAppendStream[Row].print("over result")

    // 2 sql实现
    // 2.1 Group Window
    tableEnv.createTemporaryView("sensor", sensorTable)
    val resultSqlTable = tableEnv.sqlQuery(
      """
        |select id, count(id), hop_end(rt, interval '4' second, interval '10' second)
        |from sensor
        |group by id, hop(rt, interval '4' second, interval '10' second)
      """.stripMargin)
//    resultSqlTable.toAppendStream[Row].print("agg sql")

    // 2.2 Over Window
    val orderSqlTable = tableEnv.sqlQuery(
      """
        |select id, rt, count(id) over w, avg(temperature) over w
        |from sensor
        |window w as (
        | partition by id
        | order by rt
        | rows between 2 preceding and current row
        |)
      """.stripMargin)
    orderSqlTable.toAppendStream[Row].print("order sql")
    env.execute("Time And Window test api")
  }
}
```



## Catalogs

Catalog 提供了元数据信息，例如数据库、表、分区、视图以及数据库或其他外部系统中存储的函数和信息。

数据处理最关键的方面之一是管理元数据。 元数据可以是临时的，例如临时表、或者通过 TableEnvironment 注册的 UDF。 元数据也可以是持久化的，例如 Hive Metastore 中的元数据。Catalog 提供了一个统一的API，用于管理元数据，并使其可以从 Table API 和 SQL 查询语句中来访问。

### Catalog 类型

#### GenericInMemoryCatalog

`GenericInMemoryCatalog` 是基于内存实现的 Catalog，所有元数据只在 session 的生命周期内可用。

#### JdbcCatalog

`JdbcCatalog` 使得用户可以将 Flink 通过 JDBC 协议连接到关系数据库。`PostgresCatalog` 是当前实现的唯一一种 JDBC Catalog。 参考 [JdbcCatalog 文档](https://www.bookstack.cn/read/flink-1.11.1-zh/28a2e931458de229.md) 获取关于配置 JDBC catalog 的详细信息。

#### HiveCatalog

`HiveCatalog` 有两个用途：作为原生 Flink 元数据的持久化存储，以及作为读写现有 Hive 元数据的接口。 Flink 的 [Hive 文档](http://ci.apache.org/projects/flink/flink-docs-release-1.11/zh/dev/table/hive/index.html) 提供了有关设置 `HiveCatalog` 以及访问现有 Hive 元数据的详细信息。

警告 Hive Metastore 以小写形式存储所有元数据对象名称。而 `GenericInMemoryCatalog` 区分大小写。

#### 用户自定义 Catalog

Catalog 是可扩展的，用户可以通过实现 `Catalog` 接口来开发自定义 Catalog。 想要在 SQL CLI 中使用自定义 Catalog，用户除了需要实现自定义的 Catalog 之外，还需要为这个 Catalog 实现对应的 `CatalogFactory` 接口。

`CatalogFactory` 定义了一组属性，用于 SQL CLI 启动时配置 Catalog。 这组属性集将传递给发现服务，在该服务中，服务会尝试将属性关联到 `CatalogFactory` 并初始化相应的 Catalog 实例。

### 创建表并将其注册到 Catalog

#### 使用 SQL DDL

用户可以使用 DDL 通过 Table API 或者 SQL Client 在 Catalog 中创建表。

##### **java**

```java
TableEnvironment tableEnv = ...
// Create a HiveCatalog 
Catalog catalog = new HiveCatalog("myhive", null, "<path_of_hive_conf>", "<hive_version>");
// Register the catalog
tableEnv.registerCatalog("myhive", catalog);
// Create a catalog database
tableEnv.executeSql("CREATE DATABASE mydb WITH (...)");
// Create a catalog table
tableEnv.executeSql("CREATE TABLE mytable (name STRING, age INT) WITH (...)");
tableEnv.listTables(); // should return the tables in current catalog and database.
```

##### **scala**

```scala
val tableEnv = ...
// Create a HiveCatalog 
val catalog = new HiveCatalog("myhive", null, "<path_of_hive_conf>", "<hive_version>");
// Register the catalog
tableEnv.registerCatalog("myhive", catalog);
// Create a catalog database
tableEnv.executeSql("CREATE DATABASE mydb WITH (...)");
// Create a catalog table
tableEnv.executeSql("CREATE TABLE mytable (name STRING, age INT) WITH (...)");
tableEnv.listTables(); // should return the tables in current catalog and database.
```

##### **flink client**

```sh
// the catalog should have been registered via yaml file
Flink SQL> CREATE DATABASE mydb WITH (...);
Flink SQL> CREATE TABLE mytable (name STRING, age INT) WITH (...);
Flink SQL> SHOW TABLES;
mytable
```

更多详细信息，请参考[Flink SQL CREATE DDL](https://www.bookstack.cn/read/flink-1.11.1-zh/1053d2b69932523c.md)。

#### 使用 Java/Scala

用户可以用编程的方式使用Java 或者 Scala 来创建 Catalog 表。

```java
simport org.apache.flink.table.api.*;
import org.apache.flink.table.catalog.*;
import org.apache.flink.table.catalog.hive.HiveCatalog;
import org.apache.flink.table.descriptors.Kafka;
TableEnvironment tableEnv = TableEnvironment.create(EnvironmentSettings.newInstance().build());
// Create a HiveCatalog
Catalog catalog = new HiveCatalog("myhive", null, "<path_of_hive_conf>", "<hive_version>");
// Register the catalog
tableEnv.registerCatalog("myhive", catalog);
// Create a catalog database
catalog.createDatabase("mydb", new CatalogDatabaseImpl(...));
// Create a catalog table
TableSchema schema = TableSchema.builder()
    .field("name", DataTypes.STRING())
    .field("age", DataTypes.INT())
    .build();
catalog.createTable(
        new ObjectPath("mydb", "mytable"),
        new CatalogTableImpl(
            schema,
            new Kafka()
                .version("0.11")
                ....
                .startFromEarlist()
                .toProperties(),
            "my comment"
        ),
        false
    );
List<String> tables = catalog.listTables("mydb"); // tables should contain "mytable"
```

##### **scala**

```scala
import org.apache.flink.table.api._
import org.apache.flink.table.catalog._
import org.apache.flink.table.catalog.hive.HiveCatalog
import org.apache.flink.table.descriptors.Kafka
val tableEnv = TableEnvironment.create(EnvironmentSettings.newInstance.build)
// Create a HiveCatalog
val catalog = new HiveCatalog("myhive", null, "<path_of_hive_conf>", "<hive_version>")
// Register the catalog
tableEnv.registerCatalog("myhive", catalog)
// Create a catalog database
catalog.createDatabase("mydb", new CatalogDatabaseImpl(...))
// Create a catalog table
val schema = TableSchema.builder()
    .field("name", DataTypes.STRING())
    .field("age", DataTypes.INT())
    .build()
catalog.createTable(
        new ObjectPath("mydb", "mytable"),
        new CatalogTableImpl(
            schema,
            new Kafka()
                .version("0.11")
                ....
                .startFromEarlist()
                .toProperties(),
            "my comment"
        ),
        false
    )
val tables = catalog.listTables("mydb") // tables should contain "mytable"
```

### Catalog API

注意：这里只列出了编程方式的 Catalog API，用户可以使用 SQL DDL 实现许多相同的功能。 关于 DDL 的详细信息请参考 [SQL CREATE DDL](https://www.bookstack.cn/read/flink-1.11.1-zh/1053d2b69932523c.md)。

#### 数据库操作

```java
// create database
catalog.createDatabase("mydb", new CatalogDatabaseImpl(...), false);
// drop database
catalog.dropDatabase("mydb", false);
// alter database
catalog.alterDatabase("mydb", new CatalogDatabaseImpl(...), false);
// get databse
catalog.getDatabase("mydb");
// check if a database exist
catalog.databaseExists("mydb");
// list databases in a catalog
catalog.listDatabases("mycatalog");
```

#### 表操作

```java
// create table
catalog.createTable(new ObjectPath("mydb", "mytable"), new CatalogTableImpl(...), false);
// drop table
catalog.dropTable(new ObjectPath("mydb", "mytable"), false);
// alter table
catalog.alterTable(new ObjectPath("mydb", "mytable"), new CatalogTableImpl(...), false);
// rename table
catalog.renameTable(new ObjectPath("mydb", "mytable"), "my_new_table");
// get table
catalog.getTable("mytable");
// check if a table exist or not
catalog.tableExists("mytable");
// list tables in a database
catalog.listTables("mydb");
```

#### 视图操作

```java
// create view
catalog.createTable(new ObjectPath("mydb", "myview"), new CatalogViewImpl(...), false);
// drop view
catalog.dropTable(new ObjectPath("mydb", "myview"), false);
// alter view
catalog.alterTable(new ObjectPath("mydb", "mytable"), new CatalogViewImpl(...), false);
// rename view
catalog.renameTable(new ObjectPath("mydb", "myview"), "my_new_view");
// get view
catalog.getTable("myview");
// check if a view exist or not
catalog.tableExists("mytable");
// list views in a database
catalog.listViews("mydb");
```

#### 分区操作

```java
// create view
catalog.createPartition(
    new ObjectPath("mydb", "mytable"),
    new CatalogPartitionSpec(...),
    new CatalogPartitionImpl(...),
    false);
// drop partition
catalog.dropPartition(new ObjectPath("mydb", "mytable"), new CatalogPartitionSpec(...), false);
// alter partition
catalog.alterPartition(
    new ObjectPath("mydb", "mytable"),
    new CatalogPartitionSpec(...),
    new CatalogPartitionImpl(...),
    false);
// get partition
catalog.getPartition(new ObjectPath("mydb", "mytable"), new CatalogPartitionSpec(...));
// check if a partition exist or not
catalog.partitionExists(new ObjectPath("mydb", "mytable"), new CatalogPartitionSpec(...));
// list partitions of a table
catalog.listPartitions(new ObjectPath("mydb", "mytable"));
// list partitions of a table under a give partition spec
catalog.listPartitions(new ObjectPath("mydb", "mytable"), new CatalogPartitionSpec(...));
// list partitions of a table by expression filter
catalog.listPartitions(new ObjectPath("mydb", "mytable"), Arrays.asList(epr1, ...));
```

#### 函数操作

```java
// create function
catalog.createFunction(new ObjectPath("mydb", "myfunc"), new CatalogFunctionImpl(...), false);
// drop function
catalog.dropFunction(new ObjectPath("mydb", "myfunc"), false);
// alter function
catalog.alterFunction(new ObjectPath("mydb", "myfunc"), new CatalogFunctionImpl(...), false);
// get function
catalog.getFunction("myfunc");
// check if a function exist or not
catalog.functionExists("myfunc");
// list functions in a database
catalog.listFunctions("mydb");
```

### 通过 Table API 和 SQL Client 操作 Catalog

#### 注册 Catalog

用户可以访问默认创建的内存 Catalog `default_catalog`，这个 Catalog 默认拥有一个默认数据库 `default_database`。 用户也可以注册其他的 Catalog 到现有的 Flink 会话中。

```java
tableEnv.registerCatalog(new CustomCatalog("myCatalog"));
```

使用 YAML 定义的 Catalog 必须提供 `type` 属性，以表示指定的 Catalog 类型。 以下几种类型可以直接使用。

| Catalog         | Type Value        |
| :-------------- | :---------------- |
| GenericInMemory | generic_in_memory |
| Hive            | hive              |

```yaml
catalogs:
   - name: myCatalog
     type: custom_catalog
     hive-conf-dir: ...
```

### 修改当前的 Catalog 和数据库

Flink 始终在当前的 Catalog 和数据库中寻找表、视图和 UDF。

```java
tableEnv.useCatalog("myCatalog");
tableEnv.useDatabase("myDb");
```

```sh
Flink SQL> USE CATALOG myCatalog;
Flink SQL> USE myDB;
```

通过提供全限定名 `catalog.database.object` 来访问不在当前 Catalog 中的元数据信息。

```java
tableEnv.from("not_the_current_catalog.not_the_current_db.my_table");
```

```sh
Flink SQL> SELECT * FROM not_the_current_catalog.not_the_current_db.my_table;
```

#### 列出可用的 Catalog

```java
tableEnv.listCatalogs();tableEnv.listCatalogs();
```

```sh
Flink SQL> show databases;
```

#### 列出可用的表

```java
tableEnv.listTables();
```

```sh
Flink SQL> show tables;
```

## 配置

Table 和 SQL API 的默认配置能够确保结果准确，同时也提供可接受的性能。

根据 Table 程序的需求，可能需要调整特定的参数用于优化。例如，无界流程序可能需要保证所需的状态是有限的(请参阅 [流式概念](https://www.bookstack.cn/read/flink-1.11.1-zh/36210bd6756300af.md)).

#### 概览

在每个 TableEnvironment 中，`TableConfig` 提供用于当前会话的配置项。

对于常见或者重要的配置项，`TableConfig` 提供带有详细注释的 `getters` 和 `setters` 方法。

对于更加高级的配置，用户可以直接访问底层的 key-value 配置项。以下章节列举了所有可用于调整 Flink Table 和 SQL API 程序的配置项。

注意 因为配置项会在执行操作的不同时间点被读取，所以推荐在实例化 TableEnvironment 后尽早地设置配置项。

```java
// instantiate table environment
TableEnvironment tEnv = ...
// access flink configuration
Configuration configuration = tEnv.getConfig().getConfiguration();
// set low-level key-value options
configuration.setString("table.exec.mini-batch.enabled", "true");
configuration.setString("table.exec.mini-batch.allow-latency", "5 s");
configuration.setString("table.exec.mini-batch.size", "5000");
```

```scala
// instantiate table environment
val tEnv: TableEnvironment = ...
// access flink configuration
val configuration = tEnv.getConfig().getConfiguration()
// set low-level key-value options
configuration.setString("table.exec.mini-batch.enabled", "true")
configuration.setString("table.exec.mini-batch.allow-latency", "5 s")
configuration.setString("table.exec.mini-batch.size", "5000")
```

```pyth
# instantiate table environment
t_env = ...
# access flink configuration
configuration = t_env.get_config().get_configuration();
# set low-level key-value options
configuration.set_string("table.exec.mini-batch.enabled", "true");
configuration.set_string("table.exec.mini-batch.allow-latency", "5 s");
configuration.set_string("table.exec.mini-batch.size", "5000");
```

**注意** 目前，key-value 配置项仅被 Blink planner 支持。

#### 执行配置

以下选项可用于优化查询执行的性能。

| Key                                                     | Default              | Type                               | Description                                                  |
| :------------------------------------------------------ | :------------------- | :--------------------------------- | :----------------------------------------------------------- |
| table.exec.async-lookup.buffer-capacity Batch Streaming | 100                  | Integer                            | The max number of async i/o operation that the async lookup join can trigger. |
| table.exec.async-lookup.timeout Batch Streaming         | “3 min”              | String                             | The async timeout for the asynchronous operation to complete. |
| table.exec.disabled-operators Batch                     | (none)               | String                             | Mainly for testing. A comma-separated list of operator names, each name represents a kind of disabled operator. Operators that can be disabled include “NestedLoopJoin”, “ShuffleHashJoin”, “BroadcastHashJoin”, “SortMergeJoin”, “HashAgg”, “SortAgg”. By default no operator is disabled. |
| table.exec.mini-batch.allow-latency Streaming           | “-1 ms”              | String                             | The maximum latency can be used for MiniBatch to buffer input records. MiniBatch is an optimization to buffer input records to reduce state access. MiniBatch is triggered with the allowed latency interval and when the maximum number of buffered records reached. NOTE: If table.exec.mini-batch.enabled is set true, its value must be greater than zero. |
| table.exec.mini-batch.enabled Streaming                 | false                | Boolean                            | Specifies whether to enable MiniBatch optimization. MiniBatch is an optimization to buffer input records to reduce state access. This is disabled by default. To enable this, users should set this config to true. NOTE: If mini-batch is enabled, ‘table.exec.mini-batch.allow-latency’ and ‘table.exec.mini-batch.size’ must be set. |
| table.exec.mini-batch.size Streaming                    | -1                   | Long                               | The maximum number of input records can be buffered for MiniBatch. MiniBatch is an optimization to buffer input records to reduce state access. MiniBatch is triggered with the allowed latency interval and when the maximum number of buffered records reached. NOTE: MiniBatch only works for non-windowed aggregations currently. If table.exec.mini-batch.enabled is set true, its value must be positive. |
| table.exec.resource.default-parallelism Batch Streaming | -1                   | Integer                            | Sets default parallelism for all operators (such as aggregate, join, filter) to run with parallel instances. This config has a higher priority than parallelism of StreamExecutionEnvironment (actually, this config overrides the parallelism of StreamExecutionEnvironment). A value of -1 indicates that no default parallelism is set, then it will fallback to use the parallelism of StreamExecutionEnvironment. |
| table.exec.shuffle-mode Batch                           | “ALL_EDGES_BLOCKING” | String                             | Sets exec shuffle mode. Accepted values are:`ALL_EDGES_BLOCKING`: All edges will use blocking shuffle.`FORWARD_EDGES_PIPELINED`: Forward edges will use pipelined shuffle, others blocking.`POINTWISE_EDGES_PIPELINED`: Pointwise edges will use pipelined shuffle, others blocking. Pointwise edges include forward and rescale edges.`ALL_EDGES_PIPELINED`: All edges will use pipelined shuffle.`batch`: the same as `ALL_EDGES_BLOCKING`. Deprecated.`pipelined`: the same as `ALL_EDGES_PIPELINED`. Deprecated.Note: Blocking shuffle means data will be fully produced before sent to consumer tasks. Pipelined shuffle means data will be sent to consumer tasks once produced. |
| table.exec.sink.not-null-enforcer Batch Streaming       | ERROR                | EnumPossible values: [ERROR, DROP] | The NOT NULL column constraint on a table enforces that null values can’t be inserted into the table. Flink supports ‘error’ (default) and ‘drop’ enforcement behavior. By default, Flink will check values and throw runtime exception when null values writing into NOT NULL columns. Users can change the behavior to ‘drop’ to silently drop such records without throwing exception. |
| table.exec.sort.async-merge-enabled Batch               | true                 | Boolean                            | Whether to asynchronously merge sorted spill files.          |
| table.exec.sort.default-limit Batch                     | -1                   | Integer                            | Default limit when user don’t set a limit after order by. -1 indicates that this configuration is ignored. |
| table.exec.sort.max-num-file-handles Batch              | 128                  | Integer                            | The maximal fan-in for external merge sort. It limits the number of file handles per operator. If it is too small, may cause intermediate merging. But if it is too large, it will cause too many files opened at the same time, consume memory and lead to random reading. |
| table.exec.source.idle-timeout Streaming                | “-1 ms”              | String                             | When a source do not receive any elements for the timeout time, it will be marked as temporarily idle. This allows downstream tasks to advance their watermarks without the need to wait for watermarks from this source while it is idle. |
| table.exec.spill-compression.block-size Batch           | “64 kb”              | String                             | The memory size used to do compress when spilling data. The larger the memory, the higher the compression ratio, but more memory resource will be consumed by the job. |
| table.exec.spill-compression.enabled Batch              | true                 | Boolean                            | Whether to compress spilled data. Currently we only support compress spilled data for sort and hash-agg and hash-join operators. |
| table.exec.window-agg.buffer-size-limit Batch           | 100000               | Integer                            | Sets the window elements buffer size limit used in group window agg operator. |

#### 优化器配置

以下配置可以用于调整查询优化器的行为以获得更好的执行计划。

| Key                                                          | Default | Type    | Description                                                  |
| :----------------------------------------------------------- | :------ | :------ | :----------------------------------------------------------- |
| table.optimizer.agg-phase-strategy Batch Streaming           | “AUTO”  | String  | Strategy for aggregate phase. Only AUTO, TWO_PHASE or ONE_PHASE can be set. AUTO: No special enforcer for aggregate stage. Whether to choose two stage aggregate or one stage aggregate depends on cost. TWO_PHASE: Enforce to use two stage aggregate which has localAggregate and globalAggregate. Note that if aggregate call does not support optimize into two phase, we will still use one stage aggregate. ONE_PHASE: Enforce to use one stage aggregate which only has CompleteGlobalAggregate. |
| table.optimizer.distinct-agg.split.bucket-num Streaming      | 1024    | Integer | Configure the number of buckets when splitting distinct aggregation. The number is used in the first level aggregation to calculate a bucket key ‘hash_code(distinct_key) % BUCKET_NUM’ which is used as an additional group key after splitting. |
| table.optimizer.distinct-agg.split.enabled Streaming         | false   | Boolean | Tells the optimizer whether to split distinct aggregation (e.g. COUNT(DISTINCT col), SUM(DISTINCT col)) into two level. The first aggregation is shuffled by an additional key which is calculated using the hashcode of distinct_key and number of buckets. This optimization is very useful when there is data skew in distinct aggregation and gives the ability to scale-up the job. Default is false. |
| table.optimizer.join-reorder-enabled Batch Streaming         | false   | Boolean | Enables join reorder in optimizer. Default is disabled.      |
| table.optimizer.join.broadcast-threshold Batch               | 1048576 | Long    | Configures the maximum size in bytes for a table that will be broadcast to all worker nodes when performing a join. By setting this value to -1 to disable broadcasting. |
| table.optimizer.reuse-source-enabled Batch Streaming         | true    | Boolean | When it is true, the optimizer will try to find out duplicated table sources and reuse them. This works only when table.optimizer.reuse-sub-plan-enabled is true. |
| table.optimizer.reuse-sub-plan-enabled Batch Streaming       | true    | Boolean | When it is true, the optimizer will try to find out duplicated sub-plans and reuse them. |
| table.optimizer.source.predicate-pushdown-enabled Batch Streaming | true    | Boolean | When it is true, the optimizer will push down predicates into the FilterableTableSource. Default value is true. |

#### Planner 配置

以下配置可以用于调整 planner 的行为。

| Key                                                 | Default   | Type    | Description                                                  |
| :-------------------------------------------------- | :-------- | :------ | :----------------------------------------------------------- |
| table.dynamic-table-options.enabled Batch Streaming | false     | Boolean | Enable or disable the OPTIONS hint used to specify table optionsdynamically, if disabled, an exception would be thrown if any OPTIONS hint is specified |
| table.sql-dialect Batch Streaming                   | “default” | String  | The SQL dialect defines how to parse a SQL query. A different SQL dialect may support different SQL grammar. Currently supported dialects are: default and hive |



## 查询优化

Apache Flink 使用并扩展了 Apache Calcite 来执行复杂的查询优化。 这包括一系列基于规则和成本的优化，例如：

- 基于 Apache Calcite 的子查询解相关
- 投影剪裁
- 分区剪裁
- 过滤器下推
- 子计划消除重复数据以避免重复计算
- 特殊子查询重写，包括两部分：
  - 将 IN 和 EXISTS 转换为 left semi-joins
  - 将 NOT IN 和 NOT EXISTS 转换为 left anti-join
- 可选 join 重新排序
  - 通过 `table.optimizer.join-reorder-enabled` 启用

**注意：** 当前仅在子查询重写的结合条件下支持 IN / EXISTS / NOT IN / NOT EXISTS。

优化器不仅基于计划，而且还基于可从数据源获得的丰富统计信息以及每个算子（例如 io，cpu，网络和内存）的细粒度成本来做出明智的决策。

高级用户可以通过 `CalciteConfig` 对象提供自定义优化，可以通过调用 `TableEnvironment＃getConfig＃setPlannerConfig` 将其提供给 TableEnvironment。

Apache Flink 利用 Apache Calcite 来优化和翻译查询。当前执行的优化包括投影和过滤器下推，子查询消除以及其他类型的查询重写。原版计划程序尚未优化 join 的顺序，而是按照查询中定义的顺序执行它们（FROM 子句中的表顺序和/或 WHERE 子句中的 join 谓词顺序）。

通过提供一个 `CalciteConfig` 对象，可以调整在不同阶段应用的优化规则集合。这个对象可以通过调用构造器 `CalciteConfig.createBuilder()` 创建，并通过调用 `tableEnv.getConfig.setPlannerConfig(calciteConfig)` 提供给 TableEnvironment。

## 解释表

Table API 提供了一种机制来解释计算 `Table` 的逻辑和优化查询计划。 这是通过 `Table.explain()` 方法或者 `StatementSet.explain()` 方法来完成的。`Table.explain()` 返回一个 Table 的计划。`StatementSet.explain()` 返回多 sink 计划的结果。它返回一个描述三种计划的字符串：

1. 关系查询的抽象语法树（the Abstract Syntax Tree），即未优化的逻辑查询计划，
2. 优化的逻辑查询计划，以及
3. 物理执行计划。

可以用 `TableEnvironment.explainSql()` 方法和 `TableEnvironment.executeSql()` 方法支持执行一个 `EXPLAIN` 语句获取逻辑和优化查询计划，请参阅 [EXPLAIN](https://www.bookstack.cn/read/flink-1.11.1-zh/2dcca6a705a28972.md) 页面.

以下代码展示了一个示例以及对给定 `Table` 使用 `Table.explain()` 方法的相应输出：

##### java

```java
StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
StreamTableEnvironment tEnv = StreamTableEnvironment.create(env);
DataStream<Tuple2<Integer, String>> stream1 = env.fromElements(new Tuple2<>(1, "hello"));
DataStream<Tuple2<Integer, String>> stream2 = env.fromElements(new Tuple2<>(1, "hello"));
// explain Table API
Table table1 = tEnv.fromDataStream(stream1, $("count"), $("word"));
Table table2 = tEnv.fromDataStream(stream2, $("count"), $("word"));
Table table = table1
  .where($("word").like("F%"))
  .unionAll(table2);
System.out.println(table.explain());
```

##### scala

```scala
val env = StreamExecutionEnvironment.getExecutionEnvironment
val tEnv = StreamTableEnvironment.create(env)
val table1 = env.fromElements((1, "hello")).toTable(tEnv, $"count", $"word")
val table2 = env.fromElements((1, "hello")).toTable(tEnv, $"count", $"word")
val table = table1
  .where($"word".like("F%"))
  .unionAll(table2)
println(table.explain())
```

##### python

```python
env = StreamExecutionEnvironment.get_execution_environment()
t_env = StreamTableEnvironment.create(env)
table1 = t_env.from_elements([(1, "hello")], ["count", "word"])
table2 = t_env.from_elements([(1, "hello")], ["count", "word"])
table = table1 \
    .where("LIKE(word, 'F%')") \
    .union_all(table2)
print(table.explain())
```

上述例子的结果是：

```
== Abstract Syntax Tree ==
LogicalUnion(all=[true])
  LogicalFilter(condition=[LIKE($1, _UTF-16LE'F%')])
    FlinkLogicalDataStreamScan(id=[1], fields=[count, word])
  FlinkLogicalDataStreamScan(id=[2], fields=[count, word])
== Optimized Logical Plan ==
DataStreamUnion(all=[true], union all=[count, word])
  DataStreamCalc(select=[count, word], where=[LIKE(word, _UTF-16LE'F%')])
    DataStreamScan(id=[1], fields=[count, word])
  DataStreamScan(id=[2], fields=[count, word])
== Physical Execution Plan ==
Stage 1 : Data Source
    content : collect elements with CollectionInputFormat
Stage 2 : Data Source
    content : collect elements with CollectionInputFormat
    Stage 3 : Operator
        content : from: (count, word)
        ship_strategy : REBALANCE
        Stage 4 : Operator
            content : where: (LIKE(word, _UTF-16LE'F%')), select: (count, word)
            ship_strategy : FORWARD
            Stage 5 : Operator
                content : from: (count, word)
                ship_strategy : REBALANCE
```

以下代码展示了一个示例以及使用 `StatementSet.explain()` 的多 sink 计划的相应输出：

##### java

```java
EnvironmentSettings settings = EnvironmentSettings.newInstance().useBlinkPlanner().inStreamingMode().build();
TableEnvironment tEnv = TableEnvironment.create(settings);
final Schema schema = new Schema()
    .field("count", DataTypes.INT())
    .field("word", DataTypes.STRING());
tEnv.connect(new FileSystem("/source/path1"))
    .withFormat(new Csv().deriveSchema())
    .withSchema(schema)
    .createTemporaryTable("MySource1");
tEnv.connect(new FileSystem("/source/path2"))
    .withFormat(new Csv().deriveSchema())
    .withSchema(schema)
    .createTemporaryTable("MySource2");
tEnv.connect(new FileSystem("/sink/path1"))
    .withFormat(new Csv().deriveSchema())
    .withSchema(schema)
    .createTemporaryTable("MySink1");
tEnv.connect(new FileSystem("/sink/path2"))
    .withFormat(new Csv().deriveSchema())
    .withSchema(schema)
    .createTemporaryTable("MySink2");
StatementSet stmtSet = tEnv.createStatementSet();
Table table1 = tEnv.from("MySource1").where($("word").like("F%"));
stmtSet.addInsert("MySink1", table1);
Table table2 = table1.unionAll(tEnv.from("MySource2"));
stmtSet.addInsert("MySink2", table2);
String explanation = stmtSet.explain();
System.out.println(explanation);
```

##### scala

```scala
val settings = EnvironmentSettings.newInstance.useBlinkPlanner.inStreamingMode.build
val tEnv = TableEnvironment.create(settings)
val schema = new Schema()
    .field("count", DataTypes.INT())
    .field("word", DataTypes.STRING())
tEnv.connect(new FileSystem("/source/path1"))
    .withFormat(new Csv().deriveSchema())
    .withSchema(schema)
    .createTemporaryTable("MySource1")
tEnv.connect(new FileSystem("/source/path2"))
    .withFormat(new Csv().deriveSchema())
    .withSchema(schema)
    .createTemporaryTable("MySource2")
tEnv.connect(new FileSystem("/sink/path1"))
    .withFormat(new Csv().deriveSchema())
    .withSchema(schema)
    .createTemporaryTable("MySink1")
tEnv.connect(new FileSystem("/sink/path2"))
    .withFormat(new Csv().deriveSchema())
    .withSchema(schema)
    .createTemporaryTable("MySink2")
val stmtSet = tEnv.createStatementSet()
val table1 = tEnv.from("MySource1").where($"word".like("F%"))
stmtSet.addInsert("MySink1", table1)
val table2 = table1.unionAll(tEnv.from("MySource2"))
stmtSet.addInsert("MySink2", table2)
val explanation = stmtSet.explain()
println(explanation)
```

##### python

```python
settings = EnvironmentSettings.new_instance().use_blink_planner().in_streaming_mode().build()
t_env = TableEnvironment.create(environment_settings=settings)
schema = Schema()
    .field("count", DataTypes.INT())
    .field("word", DataTypes.STRING())
t_env.connect(FileSystem().path("/source/path1")))
    .with_format(Csv().deriveSchema())
    .with_schema(schema)
    .create_temporary_table("MySource1")
t_env.connect(FileSystem().path("/source/path2")))
    .with_format(Csv().deriveSchema())
    .with_schema(schema)
    .create_temporary_table("MySource2")
t_env.connect(FileSystem().path("/sink/path1")))
    .with_format(Csv().deriveSchema())
    .with_schema(schema)
    .create_temporary_table("MySink1")
t_env.connect(FileSystem().path("/sink/path2")))
    .with_format(Csv().deriveSchema())
    .with_schema(schema)
    .create_temporary_table("MySink2")
stmt_set = t_env.create_statement_set()
table1 = t_env.from_path("MySource1").where("LIKE(word, 'F%')")
stmt_set.add_insert("MySink1", table1)
table2 = table1.union_all(t_env.from_path("MySource2"))
stmt_set.add_insert("MySink2", table2)
explanation = stmt_set.explain()
print(explanation)
```

多 sink 计划的结果是：

```plain
== Abstract Syntax Tree ==
LogicalLegacySink(name=[MySink1], fields=[count, word])
+- LogicalFilter(condition=[LIKE($1, _UTF-16LE'F%')])
   +- LogicalTableScan(table=[[default_catalog, default_database, MySource1, source: [CsvTableSource(read fields: count, word)]]])
LogicalLegacySink(name=[MySink2], fields=[count, word])
+- LogicalUnion(all=[true])
   :- LogicalFilter(condition=[LIKE($1, _UTF-16LE'F%')])
   :  +- LogicalTableScan(table=[[default_catalog, default_database, MySource1, source: [CsvTableSource(read fields: count, word)]]])
   +- LogicalTableScan(table=[[default_catalog, default_database, MySource2, source: [CsvTableSource(read fields: count, word)]]])
== Optimized Logical Plan ==
Calc(select=[count, word], where=[LIKE(word, _UTF-16LE'F%')], reuse_id=[1])
+- TableSourceScan(table=[[default_catalog, default_database, MySource1, source: [CsvTableSource(read fields: count, word)]]], fields=[count, word])
LegacySink(name=[MySink1], fields=[count, word])
+- Reused(reference_id=[1])
LegacySink(name=[MySink2], fields=[count, word])
+- Union(all=[true], union=[count, word])
   :- Reused(reference_id=[1])
   +- TableSourceScan(table=[[default_catalog, default_database, MySource2, source: [CsvTableSource(read fields: count, word)]]], fields=[count, word])
== Physical Execution Plan ==
Stage 1 : Data Source
    content : collect elements with CollectionInputFormat
    Stage 2 : Operator
        content : CsvTableSource(read fields: count, word)
        ship_strategy : REBALANCE
        Stage 3 : Operator
            content : SourceConversion(table:Buffer(default_catalog, default_database, MySource1, source: [CsvTableSource(read fields: count, word)]), fields:(count, word))
            ship_strategy : FORWARD
            Stage 4 : Operator
                content : Calc(where: (word LIKE _UTF-16LE'F%'), select: (count, word))
                ship_strategy : FORWARD
                Stage 5 : Operator
                    content : SinkConversionToRow
                    ship_strategy : FORWARD
                    Stage 6 : Operator
                        content : Map
                        ship_strategy : FORWARD
Stage 8 : Data Source
    content : collect elements with CollectionInputFormat
    Stage 9 : Operator
        content : CsvTableSource(read fields: count, word)
        ship_strategy : REBALANCE
        Stage 10 : Operator
            content : SourceConversion(table:Buffer(default_catalog, default_database, MySource2, source: [CsvTableSource(read fields: count, word)]), fields:(count, word))
            ship_strategy : FORWARD
            Stage 12 : Operator
                content : SinkConversionToRow
                ship_strategy : FORWARD
                Stage 13 : Operator
                    content : Map
                    ship_strategy : FORWARD
                    Stage 7 : Data Sink
                        content : Sink: CsvTableSink(count, word)
                        ship_strategy : FORWARD
                        Stage 14 : Data Sink
                            content : Sink: CsvTableSink(count, word)
                            ship_strategy : FORWARD
```



# DataStream API

```mermaid
graph LR
A[environment] --> B(source)
B --> C(transform)
C --> D(sink)
style A fill:#f9f
```

## Maven依赖

你只要将以下依赖项添加到 `pom.xml` 中，就能在项目中引入 Apache Flink 。这些依赖项包含了本地执行环境，因此支持本地测试。

- **Scala API**: 为了使用 Scala API，将 `flink-java` 的 artifact id 替换为 `flink-scala_2.11`，同时将 `flink-streaming-java_2.11` 替换为 `flink-streaming-scala_2.11`。

```xml
<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-java</artifactId>
  <version>1.10.0</version>
</dependency>
<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-streaming-java_2.11</artifactId>
  <version>1.10.0</version>
</dependency>
<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-clients_2.11</artifactId>
  <version>1.10.0</version>
</dependency>

<dependency>
    <groupId>org.apache.flink</groupId>
    <artifactId>flink-connector-kafka-0.11_2.11</artifactId>
    <version>1.10.0</version>
</dependency>
```

## build打包

```xml
<build>
    <plugins>
        <!-- 该插件用于将Scala代码编译成class文件 -->
        <plugin>
            <groupId>net.alchim31.maven</groupId>
            <artifactId>scala-maven-plugin</artifactId>
            <version>3.2.2</version>
            <executions>
                <execution>
                    <!-- 声明绑定到maven的compile阶段 -->
                    <goals>
                        <goal>testCompile</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-assembly-plugin</artifactId>
            <version>3.0.0</version>
            <configuration>
                <descriptorRefs>
                    <descriptorRef>jar-with-dependencies</descriptorRef>
                </descriptorRefs>
            </configuration>
            <executions>
                <execution>
                    <id>make-assembly</id>
                    <phase>package</phase>
                    <goals>
                        <goal>single</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

## 1.执行环境 Environment

##### getExecutionEnvironment

创建一个执行环境，表示当前执行程序的上下文。 如果程序是独立调用的，则此方法返回本地执行环境；如果从命令行客户端调用程序以提交到集群，则此方法返回此集群的执行环境，也就是说， getExecutionEnvironment 会根据查询运行的方式决定返回什么样的运行环境，`不需要再考虑是用LocalEnvironment还是RemoterEnvironment`,是最常用的一种创建执行环境的方式。

```scala
val env = StreamExecutionEnvironment.getExecutionEnvironment
```

##### createLocalEnvironment

返回本地执行环境，需要在调用时指定默认的并行度

```scala
val env = StreamExecutionEnvironment.createLocalEnvironment(1)
```

##### createRemoteEnvironment

返回集群执行环境，将 Jar 提交到远程服务器。需要在调用时指定 JobManager的 IP 和端口号，并指定要在集群中运行的 Jar 包.

```scala
var e = StreamExecutionEnvironment.createRemoteEnvironment("JobManager-hostname", 6123, "YOURPATH//wordcount.jar")
```

## 2. DataSource

四种方式

```scala
import java.util.Properties
import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.streaming.api.functions.source.SourceFunction
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011

import scala.util.Random

/**
  * 温度传感器读数样例类
  *
  * @param id   设备id
  * @param time 时间
  * @param temperature  温度
  */
case class SensorReading(id: String, time: Long, temperature: Double)

object SourceTest {
  def main(args: Array[String]): Unit ={
    val env = StreamExecutionEnvironment.getExecutionEnvironment;

    // 1. 从自定义的集合中读取数据
    val stream1 = env.fromCollection( List(
      SensorReading("sensor_1", 1547718199, 35.80018327300259),
      SensorReading("sensor_6", 1547718201, 15.402984393403084),
      SensorReading("sensor_7", 1547718202, 6.720945201171228),
      SensorReading("sensor_10", 1547718205, 38.101067604893444)
    ) )
    stream1.print("stream1").setParallelism(1)

    // 2. 从文件中读取数据
    val stream2 = env.readTextFile("E:\\Projects\\FlinkStudy\\src\\main\\resources\\sensor.txt")
    stream2.print("stream2").setParallelism(1)

    // 3. 从kafka中读取数据
    val properties = new Properties()
    properties.setProperty("bootstrap.servers", "192.168.17.130:9092")
    properties.setProperty("group.id", "consumer-group")
    properties.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    properties.setProperty("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    properties.setProperty("auto.offset.reset", "latest")
		val kafkaStream = new FlinkKafkaConsumer011[String]("flinkTest",new SimpleStringSchema(), properties)
    //  指定消费策略
    // kafkaStream.setStartFromEarliest() // - 从最早的记录开始；
    // kafkaStream.setStartFromTimestamp(null); // 从指定的epoch时间戳（毫秒）开始；
    // kafkaStream.setStartFromGroupOffsets(); // 默认行为，从上次消费的偏移量进行继续消费。
    kafkaStream.setStartFromLatest() //- 从最新记录开始；
    kafkaStream.setCommitOffsetsOnCheckpoints(true); //设置在检查点提交偏移量
    
    val stream3 = env.addSource(kafkaStream)

    stream3.print("stream3").setParallelism(1)


    // 4. 自定义source
    val stream4 = env.addSource(new SensorSource())

    stream4.print("stream4").setParallelism(1)

    env.execute("source test")
  }
}


class SensorSource() extends SourceFunction[SensorReading] {

  // 定义一个flag，表示数据源是否正常运行
  var running: Boolean = true

  // 取消数据源的生成
  override def cancel(): Unit = {
    running = false
  }

  // 正常生成数据源
  override def run(sourceContext: SourceFunction.SourceContext[SensorReading]): Unit = {
    // 初始化一个随机数发生器
    val rand = new Random()

    // 定义一组传感器温度数据
    var curTemp = 1.to(10).map(
      i => ( "sensor_" + i ,60 + rand.nextGaussian() * 20)
    )

    // 用无线循环产生数据流
    while(running){
      // 在前一次温度的基础上更新温度值
      curTemp = curTemp.map(
        t => (t._1, t._2 + rand.nextGaussian())
      )

      // 获取当前时间戳
      val curTime = System.currentTimeMillis()
      curTemp.foreach(
        t => sourceContext.collect(SensorReading(t._1, curTime, t._2))
      )
      // 设置时间间隔
      Thread.sleep(500)
    }
  }
}
```

## 3. 转换算子(Transform)

一旦我们有一条DataStream，我们就可以在这条数据流上面使用转换算子了。转换算子有很多种。一些转换算子可以产生一条新的DataStream，当然这个DataStream的类型可能是新类型。还有一些转换算子不会改变原有DataStream的数据，但会将数据流分区或者分组。业务逻辑就是由转换算子串起来组合而成的。

转换算子

- ##### map

  ![image-20200718210157261](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200718210157261.png)

  ​	例子：val streamMap = stream.map { x => x * 2 }

- ##### flatMap

  flatMap 的函数签名： def flatMap[A,B](as: List[A])(f: A ⇒ List[B]): List[B]

  例如: flatMap(List(1,2,3))(i ⇒ List(i,i))

  结果是 List(1,1,2,2,3,3),

  而 List("a b", "c d").flatMap(line ⇒ line.split(" "))

  结果是 List(a, b, c, d)。

  实例：

  val streamFlatMap = stream.flatMap{ x => x.split(" ")

- ##### filter

  ![image-20200719102008346](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200719102008346.png)

  val streamFilter = stream.filter{ x => x == 1}

- ##### keyBy

  ![image-20200719102517400](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200719102517400.png)

  **DataStream → KeyedStream**：逻辑地将一个流拆分成不相交的分区，每个分区包含具有相同 key 的元素， 在内部以 hash 的形式实现的。

- ##### 滚动聚合算子（Rolling Aggregation）

  这些算子可以针对KeyedStream的每一个支流做聚合

  sum(), min(), max(), minBy(), maxBy()

- ##### reduce

  **KeyedStream → DataStream**：一个分组数据流的聚合操作，合并当前的元素和上次聚合的结果，产生一个新的值，返回的流中包含每一次聚合的结果，而不是只返回最后一次聚合的最终结果

  ```scala
      val streamFromFile = env.readTextFile("E:\\Projects\\FlinkStudy\\src\\main\\resources\\sensor.txt")
  
      val dataStream: DataStream[SensorReading] = streamFromFile.map( data => {
        val dataArray = data.split(",")
        SensorReading( dataArray(0).trim, dataArray(1).trim.toLong, dataArray(2).trim.toDouble)
      })
          .keyBy(0)
  //        .sum(2)
          // 输出当前传感器最新的温度+10，而时间戳是上一次数据的时间+1
          .reduce( (x, y) => SensorReading(x.id, x.time + 1, y.temperature +10) )
      dataStream.print()
      env.execute("transform test")
  ```

- ##### split和select

  **split方法**

  ![image-20200719161907409](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200719161907409.png)

  **DataStream → SplitStream**：根据某些特征把一个 DataStream 拆分成两个或者多个 DataStream

  **select方法**

  ![image-20200719162143026](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200719162143026.png)

  **SplitStream→DataStream**：从一个 SplitStream 中获取一个或者多个 DataStream。需求： 传感器数据按照温度高低（ 以 30 度为界）， 拆分成两个流

  ```scala
  val splitStream = dataStream.split( data => {
    if (data.temperature > 30) Seq("hight") else Seq("low")
  })
  
  val high = splitStream.select("hight")
  val low = splitStream.select("low")
  val all = splitStream.select("hight", "low")
  
  high.print("high")
  low.print("low")
  all.print("all")
  ```

- ##### connect和coMap

  **connect方法**

  ![image-20200719164701806](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200719164701806.png)

  **DataStream,DataStream** **→  ConnectedStreams**：连接两个保持他们类型的数据流，两个数据流被 Connect 之后，只是被放在了一个同一个流中，内部依然保持各自的数据和形式不发生任何变化，两个流相互独立。

  **coMap方法**

  ![image-20200719164859275](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200719164859275.png)

  **ConnectedStreams → DataStream**：作用于 ConnectedStreams 上，功能与 map 和 flatMap 一样，对 ConnectedStreams 中的每一个 Stream 分别进行 map 和 flatMap 处理。

  ```scala
  // 3. 多流转换算子--合并两条流
  val warning = high.map( data => (data.id, data.temperature))
  val connectedStream = warning.connect(low)
  
  val coMapDataStream = connectedStream.map(
    warningData => (warningData._1, warningData._2, "warning"),
    lowData => (lowData.id, "healthy")
  )
  coMapDataStream.print()
  ```

- ##### union

  ![image-20200719165856934](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200719165856934.png)

  **DataStream → DataStream**：对两个或者两个以上的 DataStream 进行 union 操作，产生一个包含所有 DataStream 元素的新 DataStream。

  ```scala
  val unionStream = high.union(low)
  unionStream.print("union")
  ```

- ##### connect和union区别

  1. Union 之前两个流的类型必须是一样，Connect 可以不一样，在之后的 coMap中再去调整成为一样的。

  2. Connect 只能操作两个流， Union 可以操作多个。

## 4. 输出结果

流处理程序经常将它们的计算结果发送到一些外部系统中去，例如：Apache Kafka，文件系统，或者数据库中。Flink提供了一个维护的很好的sink算子的集合，这些sink算子可以用来将数据写入到不同的系统中去。我们也可以实现自己的sink算子。也有一些Flink程序并不会向第三方外部系统发送数据，而是将数据存储到Flink系统内部，然后可以使用Flink的可查询状态的特性来查询数据。

在我们的例子中，计算结果是一个`DataStream[SensorReading]`数据记录。每一条数据记录包含了一个传感器在5秒钟的周期里面的平均温度。计算结果组成的数据流将会调用`print()`将计算结果写到标准输出。

```{.scala}
avgTemp.print()
```

>要注意一点，流的Sink算子的选择将会影响应用程序端到端(`end-to-end`)的一致性，具体就是应用程序的计算提供的到底是`at-least-once`还是`exactly-once`的一致性语义。应用程序端到端的一致性依赖于所选择的流的Sink算子和Flink的检查点算法的集成使用。



## 5.  执行

当应用程序完全写好时，我们可以调用`StreamExecutionEnvironment.execute()`来执行应用程序。在我们的例子中就是我们的最后一行调用：

```{.scala}
env.execute("Compute average sensor temperature")
```

Flink程序是惰性执行的。也就是说创建数据源和转换算子的API调用并不会立刻触发任何数据处理逻辑。API调用仅仅是在执行环境中构建了一个执行计划，这个执行计划包含了执行环境创建的数据源和所有的将要用在数据源上的转换算子。只有当`execute()`被调用时，系统才会触发程序的执行。

构建好的执行计划将被翻译成一个`JobGraph`并提交到`JobManager`上面去执行。根据执行环境的种类，一个`JobManager`将会运行在一个本地线程中（如果是本地执行环境的化）或者`JobGraph`将会被发送到一个远程的`JobManager`上面去。如果`JobManager`远程运行，那么`JobGraph`必须和一个包含有所有类和应用程序的依赖的JAR包一起发送到远程`JobManager`。

## 6. Sink

Flink 没有类似于 spark 中 foreach 方法， 让用户进行迭代的操作。虽有对外的输出操作都要利用 Sink 完成。最后通过类似如下方式完成整个任务最终输出操作。

> stream.addSink(new MySink(xxxx))

官方提供了一部分的框架的 sink。除此以外，需要用户自定义实现 sink

![image-20200719211631658](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200719211631658.png)

#### kafka

```xml
<dependency>
    <groupId>org.apache.flink</groupId>
    <artifactId>flink-connector-kafka-0.11_2.11</artifactId>
    <version>1.7.2</version>
</dependency>
```

```scala
import java.util.Properties
import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.connectors.kafka.{FlinkKafkaConsumer011, FlinkKafkaProducer011}

object KafkaSinkTest {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)

    // source
    val properties = new Properties()
    properties.setProperty("bootstrap.servers", "192.168.17.130:9092")
    properties.setProperty("group.id", "consumer-group")
    properties.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    properties.setProperty("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    properties.setProperty("auto.offset.reset", "latest")

    val inputStream = env.addSource(new FlinkKafkaConsumer011[String]("flinkTest",new SimpleStringSchema(), properties))

    // 1. 基本转换算子和简单聚合算子
    val dataStream = inputStream.map( data => {
      val dataArray = data.split(",")
      SensorReading( dataArray(0).trim, dataArray(1).trim.toLong, dataArray(2).trim.toDouble).toString
    })

    // sink
    dataStream.addSink( new FlinkKafkaProducer011[String]("192.168.17.130:9092", "sinkTest", new SimpleStringSchema()))
    dataStream.print()
    env.execute("kafka sink test")
  }
}
```

#### redis

```xml
<dependency>
    <groupId>org.apache.bahir</groupId>
    <artifactId>flink-connector-redis_2.11</artifactId>
    <version>1.0</version>
</dependency>
```

```scala
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.connectors.redis.RedisSink
import org.apache.flink.streaming.connectors.redis.common.config.FlinkJedisPoolConfig
import org.apache.flink.streaming.connectors.redis.common.mapper.{RedisCommand, RedisCommandDescription, RedisMapper}

object RedisSinkTest {
  def main(args: Array[String]): Unit ={
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    // source
    val inputStream = env.readTextFile("E:\\Projects\\FlinkStudy\\src\\main\\resources\\sensor.txt")

    val dataStream = inputStream.map( data => {
      val dataArray = data.split(",")
      SensorReading( dataArray(0).trim, dataArray(1).trim.toLong, dataArray(2).trim.toDouble)
    })

    val conf = new FlinkJedisPoolConfig.Builder()
        .setHost("192.168.17.130")
        .setPort(6379)
        .build()
    // sink
    dataStream.addSink( new RedisSink(conf, new MyRedisMapper()) )

    env.execute("redis sink test")
  }
}

class MyRedisMapper() extends RedisMapper[SensorReading]{
  override def getCommandDescription: RedisCommandDescription = {
    // 把传感器数据保存成hashMap
    new RedisCommandDescription( RedisCommand.HSET, "sensor_temperature")
  }

  override def getKeyFromData(t: SensorReading): String = t.id

  override def getValueFromData(t: SensorReading): String = t.temperature.toString
}
```

#### Elasticsearch

```xml
<dependency>
    <groupId>org.apache.flink</groupId>
    <artifactId>flink-connector-elasticsearch6_2.11</artifactId>
    <version>1.7.2</version>
</dependency>
```

```scala
import java.util
import org.apache.flink.api.common.functions.RuntimeContext
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.connectors.elasticsearch.{ElasticsearchSinkFunction, RequestIndexer}
import org.apache.flink.streaming.connectors.elasticsearch6.ElasticsearchSink
import org.apache.http.HttpHost
import org.elasticsearch.client.Requests

object ElasticsearchSinkTest {
  def main(args: Array[String]): Unit ={
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    val streamFromFile = env.readTextFile("E:\\Projects\\FlinkStudy\\src\\main\\resources\\sensor.txt")

    val dataStream: DataStream[SensorReading] = streamFromFile.map( data => {
      val dataArray = data.split(",")
      SensorReading( dataArray(0).trim, dataArray(1).trim.toLong, dataArray(2).trim.toDouble)
    })

    val httpHosts = new util.ArrayList[HttpHost]()
    httpHosts.add( new HttpHost("192.168.17.130",9201))

    // 创建esSink的builder
    val esSinkBuilder = new ElasticsearchSink.Builder[SensorReading](httpHosts, new ElasticsearchSinkFunction[SensorReading] {
      override def process(t: SensorReading, runtimeContext: RuntimeContext, requestIndexer: RequestIndexer): Unit = {
        println("save data: " + t)

        val json = new util.HashMap[String,String]()
        json.put("data", t.toString)
        val indexRequest = Requests.indexRequest().index("sensor").`type`("readingData").source(json)

        requestIndexer.add(indexRequest)
        println("saved successfully")
      }
    })

    dataStream.addSink(esSinkBuilder.build())

    env.execute("es sink test")
  }
}
```

#### Mysql

```xml
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>5.1.44</version>
</dependency>
```



```sql
CREATE TABLE `temperatures` (
  `sensor` varchar(255) DEFAULT NULL,
  `temp` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```



```scala
import java.sql.{Connection, DriverManager, PreparedStatement}
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.functions.sink.{RichSinkFunction, SinkFunction}
import org.apache.flink.streaming.api.scala._

object JdbcSinkTest {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)

    val streamFromFile = env.readTextFile("E:\\Projects\\FlinkStudy\\src\\main\\resources\\sensor.txt")

    val dataStream: DataStream[SensorReading] = streamFromFile.map( data => {
      val dataArray = data.split(",")
      SensorReading( dataArray(0).trim, dataArray(1).trim.toLong, dataArray(2).trim.toDouble)
    })

    // sink
    dataStream.addSink( new MyJdbcSink())

    env.execute("jdbc sink test")
  }
}


class MyJdbcSink() extends RichSinkFunction[SensorReading]{
  // 定义链接、预编译器
  var conn: Connection = _
  var insertStmt: PreparedStatement = _
  var updateStmt: PreparedStatement = _

  // 初始化，创建连接和预编译语句
  override def open(parameters: Configuration): Unit = {
    super.open(parameters)
    conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "Wangxl@123")
    insertStmt = conn.prepareStatement("insert into temperatures (sensor, temp) values (?,?)")
    updateStmt = conn.prepareStatement("update temperatures set temp = ? where sensor = ?")
  }

  // 调用连接，执行sql
  override def invoke(value: SensorReading, context: SinkFunction.Context[_]): Unit = {
    // 执行更新语句
    updateStmt.setDouble(1, value.temperature)
    updateStmt.setString(2, value.id)
    updateStmt.execute()
    // 如果没有数据，执行插入
    if (updateStmt.getUpdateCount ==0){
      insertStmt.setString(1, value.id)
      insertStmt.setDouble(2, value.temperature)
      insertStmt.execute()
    }
  }

  override def close(): Unit = {
    insertStmt.close()
    updateStmt.close()
    conn.close()
  }
}
```



## 设置并行度

Flink应用程序在一个像集群这样的分布式环境中并行执行。当一个数据流程序提交到JobManager执行时，系统将会创建一个数据流图，然后准备执行需要的操作符。每一个操作符将会并行化到一个或者多个任务中去。每个算子的并行任务都会处理这个算子的输入流中的一份子集。一个算子并行任务的个数叫做算子的并行度。它决定了算子执行的并行化程度，以及这个算子能处理多少数据量。

算子的并行度可以在执行环境这个层级来控制，也可以针对每个不同的算子设置不同的并行度。默认情况下，应用程序中所有算子的并行度都将设置为执行环境的并行度。执行环境的并行度（也就是所有算子的默认并行度）将在程序开始运行时自动初始化。如果应用程序在本地执行环境中运行，并行度将被设置为CPU的核数。当我们把应用程序提交到一个处于运行中的Flink集群时，执行环境的并行度将被设置为集群默认的并行度，除非我们在客户端提交应用程序时显式的设置好并行度。

通常情况下，将算子的并行度定义为和执行环境并行度相关的数值会是个好主意。这允许我们通过在客户端调整应用程序的并行度就可以将程序水平扩展了。我们可以使用以下代码来访问执行环境的默认并行度。

我们还可以重写执行环境的默认并行度，但这样的话我们将再也不能通过客户端来控制应用程序的并行度了。

算子默认的并行度也可以通过重写来明确指定。在下面的例子里面，数据源的操作符将会按照环境默认的并行度来并行执行，map操作符的并行度将会是默认并行度的2倍，sink操作符的并行度为2。

```{.scala}
val env = StreamExecutionEnvironment.getExecutionEnvironment
val defaultP = env.getParallelism
val result = env.addSource(new CustomSource)
  .map(new MyMapper).setParallelism(defaultP * 2)
  .print().setParallelism(2)
```

当我们通过客户端将应用程序的并行度设置为16并提交执行时，source操作符的并行度为16，mapper并行度为32，sink并行度为2。如果我们在本地环境运行应用程序的话，例如在IDE中运行，机器是8核，那么source任务将会并行执行在8个任务上面，mapper运行在16个任务上面，sink运行在2个任务上面。

>并行度是动态概念，任务槽数量是静态概念。并行度<=任务槽数量。一个任务槽最多运行一个并行度。



## 支持的数据类型

Flink 流应用程序处理的是以数据对象表示的事件流。所以在 Flink 内部， 我们需要能够处理这些对象。它们需要被序列化和反序列化， 以便通过网络传送它们； 或者从状态后端、检查点和保存点读取它们。为了有效地做到这一点，Flink 需要明确知道应用程序所处理的数据类型。Flink 使用类型信息的概念来表示数据类型，并为每个数据类型生成特定的序列化器、反序列化器和比较器。

Flink 还具有一个类型提取系统，该系统分析函数的输入和返回类型，以自动获取类型信息，从而获得序列化器和反序列化器。但是，在某些情况下，例如 lambda 函数或泛型类型，需要显式地提供类型信息，才能使应用程序正常工作或提高其性能。

Flink 支持 Java 和 Scala 中所有常见数据类型。使用最广泛的类型有以下几种。

##### 基础数据类型

​	Flink 支持所有的 Java 和 Scala 基础数据类型， Int, Double, Long, String, …

```scala
val numbers**:** DataStream[Long] **=** env.fromElements(1L, 2L, 3L, 4L)
numbers.map( n **=>** n + 1 )
```

##### Java 和 Scala 元组（Tuples）

##### Scala样例类（case classe）

```scala
    val persons: DataStream[Person] = 
      env.fromElements( Person("Adam", 17)),
      Person("Sarah", 23) )
    persons.filter(p => p.age > 18)
```

##### Java简单对象（POJOs）

```scala
public class Person { 
  public String name; 
  public int age;
  public Person() {}
  public Person(String name, int age) { 
    this.name = name;
    this.age = age;
  }
}
DataStream<Person> persons = env.fromElements( Person("Adam", 17) )
```

##### 其它（Arrays, Lists, Maps, Enums, 等等）



## 实现UDF函数

——更细粒度的控制流

##### 函数类（Function Classes）

Flink 暴露了所有 udf 函数的接口(实现方式为接口或者抽象类)。例如 MapFunction, FilterFunction, ProcessFunction 等等。下面例子实现了 FilterFunction 接口：

```scala
object Test{
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    val streamFromFile = env.readTextFile("E:\\Projects\\FlinkStudy\\src\\main\\resources\\sensor.txt")
	val dataStream: DataStream[SensorReading] = streamFromFile.map( data => {
      val dataArray = data.split(",")
      SensorReading( dataArray(0).trim, dataArray(1).trim.toLong, dataArray(2).trim.toDouble)
    })
    dataStream.filter( new MyFilter() ).print()
  }
}

class MyFilter() extends FilterFunction[SensorReading]{
  override def filter(value: SensorReading): Boolean = {
    value.id.startsWith("sensor_1")
  }
}
```

##### 匿名函数（Lambda Functions）

```scala
dataStream.filter( _.id.contains("sensor_10")).print()
相当于
dataStream.filter( data => data.id.contains("sensor_10")).print()
```

##### 富函数（Rich Functions）

“富函数”是 DataStream API 提供的一个函数类的接口， 所有 Flink 函数类都有其 Rich 版本。它与常规函数的不同在于，可以获取运行环境的上下文，并拥有一些生命周期方法，所以可以实现更复杂的功能

- RichMapFunction
- RichFlatMapFunction
- RichFilterFunction
- ...

Rich Function 有一个生命周期的概念。典型的生命周期方法有：

- open()方法是 rich function 的初始化方法，当一个算子例如 map 或者 filter被调用之前 open()会被调用。

- close()方法是生命周期中的最后一个调用的方法，做一些清理工作。

- getRuntimeContext()方法提供了函数的 RuntimeContext 的一些信息，例如函数执行的并行度，任务的名字，以及 state 状态

```scala
class MyFlatMap extends RichFlatMapFunction[Int, (Int, Int)] {
  var subTaskIndex = 0
  
  override def open(configuration: Configuration): Unit = { 
    subTaskIndex =
    getRuntimeContext.getIndexOfThisSubtask
    // 以下可以做一些初始化工作，例如建立一个和 HDFS 的连接
  }
  
  override def flatMap(in: Int, out: Collector[(Int, Int)]): Unit = {
    if (in % 2 == subTaskIndex) { 
      out.collect((subTaskIndex, in))
    }
  }
  
  override def close(): Unit = {
    // 以下做一些清理工作，例如断开和 HDFS 的连接
  }
}
```



## 一个简单的单词统计

#### 创建一个Object对象

```scala
import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.streaming.api.scala._

object StreamWordCount {
  def main(args: Array[String]): Unit = {

    val params = ParameterTool.fromArgs(args)

    val host: String  = params.get("host")
    val port: Int = params.getInt("port")

    // 创建一个流处理的执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    // 接收socket数据流
    val textDataStream = env.socketTextStream(host, port)

    // 逐一读取数据，打散之后进行count
    val wordCountDataStream = textDataStream.flatMap(_.split(" "))
      .filter(_.nonEmpty)
      .map( (_, 1))
      .keyBy(0)
      .sum(1)

    // 打印输出， 多线程输出com.wxl.wc.StreamWordCount
    wordCountDataStream.print()

    // 可以设置单线程输出
//    wordCountDataStream.print().setParallelism(1)

    // 执行任务
    env.execute("my job")
  }
}
```

#### 打jar包

![image-20200715150403285](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200715150403285.png)

#### 运行jar包

在一台服务器上使用下面命令开启一个测试端口

nc -lk 7777

![image-20200715151025856](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200715151025856.png)

用flink run运行jar包

/opt/flink/bin/flink run -c com.wxl.wc.StreamWordCount -p 2 /data/FlinkStudy-1.0-SNAPSHOT-jar-with-dependencies.jar --host 172.17.0.4 --port 7777

通过flink web界面查看job是否正常运行

![image-20200715150807464](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200715150807464.png)

从Task Managers查看运行job的打印情况

![image-20200715150950636](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200715150950636.png)

![image-20200715151003063](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200715151003063.png)



# Connectors

## DataStream Connectors

### 预定义的 Source 和 Sink

一些比较基本的 Source 和 Sink 已经内置在 Flink 里。 [预定义 data sources](# 2. DataSource) 支持从文件、目录、socket，以及 collections 和 iterators 中读取数据。 [预定义 data sinks](# 6. Sink) 支持把数据写入文件、标准输出（stdout）、标准错误输出（stderr）和 socket。

### 附带的连接器

连接器可以和多种多样的第三方系统进行交互。目前支持以下系统:

- [Apache Kafka](https://www.bookstack.cn/read/flink-1.11.1-zh/f4ce98c60273831b.md) (source/sink)
- [Apache Cassandra](https://www.bookstack.cn/read/flink-1.11.1-zh/93a8cccb61fb96a3.md) (sink)
- [Amazon Kinesis Streams](https://www.bookstack.cn/read/flink-1.11.1-zh/ae18cc13bdfbb4bc.md) (source/sink)
- [Elasticsearch](https://www.bookstack.cn/read/flink-1.11.1-zh/bfd7b3d885c8c729.md) (sink)
- [Hadoop FileSystem](https://www.bookstack.cn/read/flink-1.11.1-zh/ea30942a6274f2c7.md) (sink)
- [RabbitMQ](https://www.bookstack.cn/read/flink-1.11.1-zh/bb57a8e5f4aadbea.md) (source/sink)
- [Apache NiFi](https://www.bookstack.cn/read/flink-1.11.1-zh/6d01e533d2394f1c.md) (source/sink)
- [Twitter Streaming API](https://www.bookstack.cn/read/flink-1.11.1-zh/e89f5cfd8a468131.md) (source)
- [Google PubSub](https://www.bookstack.cn/read/flink-1.11.1-zh/21df7dcf4e21b624.md) (source/sink)
- [JDBC](https://www.bookstack.cn/read/flink-1.11.1-zh/f2e7a9a8c7737daa.md) (sink)

请记住，在使用一种连接器时，通常需要额外的第三方组件，比如：数据存储服务器或者消息队列。 要注意这些列举的连接器是 Flink 工程的一部分，包含在发布的源码中，但是不包含在二进制发行版中。 更多说明可以参考对应的子部分。

### Apache Bahir 中的连接器

Flink 还有些一些额外的连接器通过 [Apache Bahir](https://bahir.apache.org/) 发布, 包括:

- [Apache ActiveMQ](https://bahir.apache.org/docs/flink/current/flink-streaming-activemq/) (source/sink)
- [Apache Flume](https://bahir.apache.org/docs/flink/current/flink-streaming-flume/) (sink)
- [Redis](https://bahir.apache.org/docs/flink/current/flink-streaming-redis/) (sink)
- [Akka](https://bahir.apache.org/docs/flink/current/flink-streaming-akka/) (sink)
- [Netty](https://bahir.apache.org/docs/flink/current/flink-streaming-netty/) (source)

### 连接Fink的其他方法

#### 异步 I/O

使用connector并不是唯一可以使数据进入或者流出Flink的方式。 一种常见的模式是从外部数据库或者 Web 服务查询数据得到初始数据流，然后通过 `Map` 或者 `FlatMap` 对初始数据流进行丰富和增强。 Flink 提供了[异步 I/O](https://www.bookstack.cn/read/flink-1.11.1-zh/dca32aa5989aec16.md) API 来让这个过程更加简单、高效和稳定。

#### 可查询状态

当 Flink 应用程序需要向外部存储推送大量数据时会导致 I/O 瓶颈问题出现。在这种场景下，如果对数据的读操作远少于写操作，那么让外部应用从 Flink 拉取所需的数据会是一种更好的方式。 [可查询状态](https://www.bookstack.cn/read/flink-1.11.1-zh/59f9506bbddc90a3.md) 接口可以实现这个功能，该接口允许被 Flink 托管的状态可以被按需查询。

### Kafka 连接器

此连接器提供了访问 [Apache Kafka](https://kafka.apache.org/) 事件流的服务。

Flink 提供了专门的 Kafka 连接器，向 Kafka topic 中读取或者写入数据。Flink Kafka Consumer 集成了 Flink 的 Checkpoint 机制，可提供 exactly-once 的处理语义。为此，Flink 并不完全依赖于跟踪 Kafka 消费组的偏移量，而是在内部跟踪和检查偏移量。

根据你的用例和环境选择相应的包（maven artifact id）和类名。对于大多数用户来说，使用 `FlinkKafkaConsumer010`（ `flink-connector-kafka` 的一部分）是比较合适的。

| Maven 依赖                      | 自从哪个版本 开始支持 | 消费者和 生产者的类名称                     | Kafka 版本 | 注意                                                         |
| :------------------------------ | :-------------------- | :------------------------------------------ | :--------- | :----------------------------------------------------------- |
| flink-connector-kafka-0.10_2.11 | 1.2.0                 | FlinkKafkaConsumer010 FlinkKafkaProducer010 | 0.10.x     | 这个连接器支持 [带有时间戳的 Kafka 消息](https://cwiki.apache.org/confluence/display/KAFKA/KIP-32+-+Add+timestamps+to+Kafka+message)，用于生产和消费。 |
| flink-connector-kafka-0.11_2.11 | 1.4.0                 | FlinkKafkaConsumer011 FlinkKafkaProducer011 | 0.11.x     | Kafka 从 0.11.x 版本开始不支持 Scala 2.10。此连接器支持了 [Kafka 事务性的消息传递](https://cwiki.apache.org/confluence/display/KAFKA/KIP-98+-+Exactly+Once+Delivery+and+Transactional+Messaging)来为生产者提供 Exactly once 语义。 |
| flink-connector-kafka_2.11      | 1.7.0                 | FlinkKafkaConsumer FlinkKafkaProducer       | >= 1.0.0   | 这个通用的 Kafka 连接器尽力与 Kafka client 的最新版本保持同步。该连接器使用的 Kafka client 版本可能会在 Flink 版本之间发生变化。从 Flink 1.9 版本开始，它使用 Kafka 2.2.0 client。当前 Kafka 客户端向后兼容 0.10.0 或更高版本的 Kafka broker。 但是对于 Kafka 0.11.x 和 0.10.x 版本，我们建议你分别使用专用的 flink-connector-kafka-0.11_2.11 和 flink-connector-kafka-0.10_2.11 连接器。 |

接着，在你的 maven 项目中导入连接器：

```xml
<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-connector-kafka_2.11</artifactId>
  <version>1.11.0</version>
</dependency>
```

请注意：目前流连接器还不是二进制分发的一部分。 [在此处](https://www.bookstack.cn/read/flink-1.11.1-zh/859b23aab8a2b37c.md)可以了解到如何链接它们以实现在集群中执行。

#### 安装 Apache Kafka

- 按照 [Kafka 快速入门](https://kafka.apache.org/documentation.html#quickstart)的说明下载代码并启动 Kafka 服务器（每次启动应用程序之前都需要启动 Zookeeper 和 Kafka server）。
- 如果 Kafka 和 Zookeeper 服务器运行在远端机器上，那么必须要将 `config/server.properties` 文件中的 `advertised.host.name`属性设置为远端设备的 IP 地址。

#### Kafka 1.0.0+ 连接器

从 Flink 1.7 开始，有一个新的通用 Kafka 连接器，它不跟踪特定的 Kafka 主版本。相反，它是在 Flink 发布时跟踪最新版本的 Kafka。 如果你的 Kafka broker 版本是 1.0.0 或 更新的版本，你应该使用这个 Kafka 连接器。 如果你使用的是 Kafka 的旧版本( 0.11 或 0.10 )，那么你应该使用与 Kafka broker 版本相对应的连接器。

##### 兼容性

通过 Kafka client API 和 broker 的兼容性保证，通用的 Kafka 连接器兼容较旧和较新的 Kafka broker。 它兼容 Kafka broker 0.11.0 或者更高版本，具体兼容性取决于所使用的功能。有关 Kafka 兼容性的详细信息，请参考 [Kafka 文档](https://kafka.apache.org/protocol.html#protocol_compatibility)。

##### 将 Kafka Connector 从 0.11 迁移到通用版本

以便执行迁移，请参考 [升级 Jobs 和 Flink 版本指南](https://www.bookstack.cn/read/flink-1.11.1-zh/550dffe899aeb3c2.md)：

- 在全程中使用 Flink 1.9 或更新版本。
- 不要同时升级 Flink 和 Operator。
- 确保你的 Job 中所使用的 Kafka Consumer 和 Kafka Producer 分配了唯一的标识符（uid）。
- 使用 stop with savepoint 的特性来执行 savepoint（例如，使用 `stop --withSavepoint`）[CLI 命令](https://www.bookstack.cn/read/flink-1.11.1-zh/78b52bbc8bf80d4f.md)。

##### 用法

要使用通用的 Kafka 连接器，请为它添加依赖关系：

```xml
<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-connector-kafka_2.11</artifactId>
  <version>1.11.0</version>
</dependency>
```

然后，实例化 source（ `FlinkKafkaConsumer`）和 sink（ `FlinkKafkaProducer`）。除了从模块和类名中删除了特定的 Kafka 版本外，这个 API 向后兼容 Kafka 0.11 版本的 connector。

#### Kafka Consumer

Flink 的 Kafka consumer 称为 `FlinkKafkaConsumer010`（或适用于 Kafka 0.11.0.x 版本的 `FlinkKafkaConsumer011`，或适用于 Kafka >= 1.0.0 的版本的 `FlinkKafkaConsumer`）。它提供对一个或多个 Kafka topics 的访问。

构造函数接受以下参数：

1. Topic 名称或者名称列表
2. 用于反序列化 Kafka 数据的 DeserializationSchema 或者 KafkaDeserializationSchema
3. Kafka 消费者的属性。需要以下属性：
   - “bootstrap.servers”（以逗号分隔的 Kafka broker 列表）
   - “group.id” 消费组 ID

示例：

```java
Properties properties = new Properties();
properties.setProperty("bootstrap.servers", "localhost:9092");
properties.setProperty("group.id", "test");
DataStream<String> stream = env
  .addSource(new FlinkKafkaConsumer010<>("topic", new SimpleStringSchema(), properties));
```

##### DeserializationSchema

Flink Kafka Consumer 需要知道如何将 Kafka 中的二进制数据转换为 Java 或者 Scala 对象。`DeserializationSchema` 允许用户指定这样的 schema，为每条 Kafka 消息调用 `T deserialize(byte[] message)` 方法，传递来自 Kafka 的值。

从 `AbstractDeserializationSchema` 开始通常很有帮助，它负责将生成的 Java 或 Scala 类型描述为 Flink 的类型系统。 用户如果要自己去实现一个`DeserializationSchema`，需要自己去实现 `getProducedType(...)`方法。

为了访问 Kafka 消息的 key、value 和元数据，`KafkaDeserializationSchema` 具有以下反序列化方法 `T deserialize(ConsumerRecord<byte[], byte[]> record)`。

为了方便使用，Flink 提供了以下几种 schemas：

1. `TypeInformationSerializationSchema`（和 `TypeInformationKeyValueSerializationSchema`) 基于 Flink 的 `TypeInformation` 创建 `schema`。 如果该数据的读和写都发生在 Flink 中，那么这将是非常有用的。此 schema 是其他通用序列化方法的高性能 Flink 替代方案。
2. `JsonDeserializationSchema`（和 `JSONKeyValueDeserializationSchema`）将序列化的 JSON 转化为 ObjectNode 对象，可以使用 `objectNode.get("field").as(Int/String/...)()` 来访问某个字段。 KeyValue objectNode 包含一个含所有字段的 key 和 values 字段，以及一个可选的”metadata”字段，可以访问到消息的 offset、partition、topic 等信息。
3. `AvroDeserializationSchema` 使用静态提供的 schema 读取 Avro 格式的序列化数据。 它能够从 Avro 生成的类（`AvroDeserializationSchema.forSpecific(...)`）中推断出 schema，或者可以与 `GenericRecords` 一起使用手动提供的 schema（用 `AvroDeserializationSchema.forGeneric(...)`）。此反序列化 schema 要求序列化记录不能包含嵌入式架构！
   - 此模式还有一个版本，可以在 [Confluent Schema Registry](https://docs.confluent.io/current/schema-registry/docs/index.html) 中查找编写器的 schema（用于编写记录的 schema）。
   - 使用这些反序列化 schema 记录将读取从 schema 注册表检索到的 schema 转换为静态提供的 schema（或者通过 `ConfluentRegistryAvroDeserializationSchema.forGeneric(...)` 或 `ConfluentRegistryAvroDeserializationSchema.forSpecific(...)`）。

```xml
要使用此反序列化 schema 必须添加以下依赖：

<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-avro</artifactId>
  <version>1.11.0</version>
</dependency>
```

当遇到因一些原因而无法反序列化的损坏消息时，这里有两个选项 - 从 `deserialize（...）` 方法抛出异常会导致作业失败并重新启动，或返回 `null`，以允许 Flink Kafka 消费者悄悄地跳过损坏的消息。请注意，由于 Consumer 的容错能力（请参阅下面的部分以获取更多详细信息），在损坏的消息上失败作业将使 consumer 尝试再次反序列化消息。因此，如果反序列化仍然失败，则 consumer 将在该损坏的消息上进入不间断重启和失败的循环。

##### 配置 Kafka Consumer 开始消费的位置

Flink Kafka Consumer 允许通过配置来确定 Kafka 分区的起始位置。

例如：

```java
final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
FlinkKafkaConsumer010<String> myConsumer = new FlinkKafkaConsumer010<>(...);
myConsumer.setStartFromEarliest();     // 尽可能从最早的记录开始
myConsumer.setStartFromLatest();       // 从最新的记录开始
myConsumer.setStartFromTimestamp(...); // 从指定的时间开始（毫秒）
myConsumer.setStartFromGroupOffsets(); // 默认的方法
DataStream<String> stream = env.addSource(myConsumer);
...
```

Flink Kafka Consumer 的所有版本都具有上述明确的起始位置配置方法。

- `setStartFromGroupOffsets`（默认方法）：从 Kafka brokers 中的 consumer 组（consumer 属性中的 `group.id` 设置）提交的偏移量中开始读取分区。 如果找不到分区的偏移量，那么将会使用配置中的 `auto.offset.reset` 设置。
- `setStartFromEarliest()` 或者 `setStartFromLatest()`：从最早或者最新的记录开始消费，在这些模式下，Kafka 中的 committed offset 将被忽略，不会用作起始位置。
- `setStartFromTimestamp(long)`：从指定的时间戳开始。对于每个分区，其时间戳大于或等于指定时间戳的记录将用作起始位置。如果一个分区的最新记录早于指定的时间戳，则只从最新记录读取该分区数据。在这种模式下，Kafka 中的已提交 offset 将被忽略，不会用作起始位置。

你也可以为每个分区指定 consumer 应该开始消费的具体 offset：

```java
Map<KafkaTopicPartition, Long> specificStartOffsets = new HashMap<>();
specificStartOffsets.put(new KafkaTopicPartition("myTopic", 0), 23L);
specificStartOffsets.put(new KafkaTopicPartition("myTopic", 1), 31L);
specificStartOffsets.put(new KafkaTopicPartition("myTopic", 2), 43L);
myConsumer.setStartFromSpecificOffsets(specificStartOffsets);
```

上面的例子中使用的配置是指定从 `myTopic` 主题的 0 、1 和 2 分区的指定偏移量开始消费。offset 值是 consumer 应该为每个分区读取的下一条消息。请注意：如果 consumer 需要读取在提供的 offset 映射中没有指定 offset 的分区，那么它将回退到该特定分区的默认组偏移行为（即 `setStartFromGroupOffsets()`）。

请注意：当 Job 从故障中自动恢复或使用 savepoint 手动恢复时，这些起始位置配置方法不会影响消费的起始位置。在恢复时，每个 Kafka 分区的起始位置由存储在 savepoint 或 checkpoint 中的 offset 确定（有关 checkpointing 的信息，请参阅下一节，以便为 consumer 启用容错功能）。

##### Kafka Consumer 和容错

伴随着启用 Flink 的 checkpointing 后，Flink Kafka Consumer 将使用 topic 中的记录，并以一致的方式定期检查其所有 Kafka offset 和其他算子的状态。如果 Job 失败，Flink 会将流式程序恢复到最新 checkpoint 的状态，并从存储在 checkpoint 中的 offset 开始重新消费 Kafka 中的消息。

因此，设置 checkpoint 的间隔定义了程序在发生故障时最多需要返回多少。

要使用容错的 Kafka Consumer，需要在执行环境中启用拓扑的 checkpointing。

```java
final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
env.enableCheckpointing(5000); // 每隔 5000 毫秒 执行一次 checkpoint
```

另请注意，只有当可用的 slots 足够时，Flink 才能重新启动。因此，如果拓扑由于丢失了 TaskManager 而失败，那么之后必须要一直有足够可用的 solt。Flink on YARN 支持自动重启丢失的 YARN 容器。

如果未启用 checkpoint，那么 Kafka consumer 将定期向 Zookeeper 提交 offset。

##### Kafka Consumer Topic 和分区发现

###### 分区发现

Flink Kafka Consumer 支持发现动态创建的 Kafka 分区，并使用精准一次的语义保证去消耗它们。在初始检索分区元数据之后（即，当 Job 开始运行时）发现的所有分区将从最早可能的 offset 中消费。

默认情况下，是禁用了分区发现的。若要启用它，请在提供的属性配置中为 `flink.partition-discovery.interval-millis` 设置大于 0 的值，表示发现分区的间隔是以毫秒为单位的。

局限性 当从 Flink 1.3.x 之前的 Flink 版本的 savepoint 恢复 consumer 时，分区发现无法在恢复运行时启用。如果启用了，那么还原将会失败并且出现异常。在这种情况下，为了使用分区发现，请首先在 Flink 1.3.x 中使用 savepoint，然后再从 savepoint 中恢复。

###### Topic 发现

在更高的级别上，Flink Kafka Consumer 还能够使用正则表达式基于 Topic 名称的模式匹配来发现 Topic。请看下面的例子：

```java
final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
Properties properties = new Properties();
properties.setProperty("bootstrap.servers", "localhost:9092");
properties.setProperty("group.id", "test");
FlinkKafkaConsumer011<String> myConsumer = new FlinkKafkaConsumer011<>(
    java.util.regex.Pattern.compile("test-topic-[0-9]"),
    new SimpleStringSchema(),
    properties);
DataStream<String> stream = env.addSource(myConsumer);
...
```

在上面的例子中，当 Job 开始运行时，Consumer 将订阅名称与指定正则表达式匹配的所有主题（以 `test-topic` 开头并以单个数字结尾）。

要允许 consumer 在作业开始运行后发现动态创建的主题，那么请为 `flink.partition-discovery.interval-millis` 设置非负值。这允许 consumer 发现名称与指定模式匹配的新主题的分区。

##### Kafka Consumer 提交 Offset 的行为配置

Flink Kafka Consumer 允许有配置如何将 offset 提交回 Kafka broker 的行为。请注意：Flink Kafka Consumer 不依赖于提交的 offset 来实现容错保证。提交的 offset 只是一种方法，用于公开 consumer 的进度以便进行监控。

配置 offset 提交行为的方法是否相同，取决于是否为 job 启用了 checkpointing。

- *禁用 Checkpointing：* 如果禁用了 checkpointing，则 Flink Kafka Consumer 依赖于内部使用的 Kafka client 自动定期 offset 提交功能。 因此，要禁用或启用 offset 的提交，只需将 `enable.auto.commit` 或者 `auto.commit.interval.ms` 的Key 值设置为提供的 `Properties` 配置中的适当值。
- *启用 Checkpointing：* 如果启用了 checkpointing，那么当 checkpointing 完成时，Flink Kafka Consumer 将提交的 offset 存储在 checkpoint 状态中。 这确保 Kafka broker 中提交的 offset 与 checkpoint 状态中的 offset 一致。 用户可以通过调用 consumer 上的 `setCommitOffsetsOnCheckpoints(boolean)` 方法来禁用或启用 offset 的提交(默认情况下，这个值是 true )。 注意，在这个场景中，`Properties` 中的自动定期 offset 提交设置会被完全忽略。

##### Kafka Consumer 和 时间戳抽取以及 watermark 发送

在许多场景中，记录的时间戳是(显式或隐式)嵌入到记录本身中。此外，用户可能希望定期或以不规则的方式 Watermark，例如基于 Kafka 流中包含当前事件时间的 watermark 的特殊记录。对于这些情况，Flink Kafka Consumer 允许指定 `AssignerWithPeriodicWatermarks` 或 `AssignerWithPunctuatedWatermarks`。

你可以按照[此处](https://www.bookstack.cn/read/flink-1.11.1-zh/1272ccb5c5426490.md)的说明指定自定义时间戳抽取器或者 Watermark 发送器，或者使用 [内置的](https://www.bookstack.cn/read/flink-1.11.1-zh/6fd539a505bf888a.md)。你也可以通过以下方式将其传递给你的 consumer：

```java
Properties properties = new Properties();
properties.setProperty("bootstrap.servers", "localhost:9092");
properties.setProperty("group.id", "test");
FlinkKafkaConsumer010<String> myConsumer =
    new FlinkKafkaConsumer010<>("topic", new SimpleStringSchema(), properties);
myConsumer.assignTimestampsAndWatermarks(new CustomWatermarkEmitter());
DataStream<String> stream = env
  .addSource(myConsumer)
  .print();
```

在内部，每个 Kafka 分区执行一个 assigner 实例。当指定了这样的 assigner 时，对于从 Kafka 读取的每条消息，调用 `extractTimestamp(T element, long previousElementTimestamp)` 来为记录分配时间戳，并为 `Watermark getCurrentWatermark()`（定期形式）或 `Watermark checkAndGetNextWatermark(T lastElement, long extractedTimestamp)`（打点形式）以确定是否应该发出新的 watermark 以及使用哪个时间戳。

**请注意**：如果 watermark assigner 依赖于从 Kafka 读取的消息来上涨其 watermark (通常就是这种情况)，那么所有主题和分区都需要有连续的消息流。否则，整个应用程序的 watermark 将无法上涨，所有基于时间的算子(例如时间窗口或带有计时器的函数)也无法运行。单个的 Kafka 分区也会导致这种反应。这是一个已在计划中的 Flink 改进，目的是为了防止这种情况发生（请见[FLINK-5479: Per-partition watermarks in FlinkKafkaConsumer should consider idle partitions](https://issues.apache.org/jira/browse/FLINK-5479)）。同时，可能的解决方法是将*心跳消息*发送到所有 consumer 的分区里，从而上涨空闲分区的 watermark。

#### Kafka Producer

Flink Kafka Producer 被称为 `FlinkKafkaProducer011`（或适用于 Kafka 0.10.0.x 版本的 `FlinkKafkaProducer010`，或适用于 Kafka >= 1.0.0 版本的 `FlinkKafkaProducer`）。它允许将消息流写入一个或多个 Kafka topic。

示例：

```java
DataStream<String> stream = ...;
FlinkKafkaProducer011<String> myProducer = new FlinkKafkaProducer011<String>(
        "localhost:9092",            // broker 列表
        "my-topic",                  // 目标 topic
        new SimpleStringSchema());   // 序列化 schema
// 0.10+ 版本的 Kafka 允许在将记录写入 Kafka 时附加记录的事件时间戳；
// 此方法不适用于早期版本的 Kafka
myProducer.setWriteTimestampToKafka(true);
stream.addSink(myProducer);
```

上面的例子演示了创建 Flink Kafka Producer 来将流消息写入单个 Kafka 目标 topic 的基本用法。 对于更高级的用法，这还有其他构造函数变体允许提供以下内容：

- *提供自定义属性*：producer 允许为内部 `KafkaProducer` 提供自定义属性配置。有关如何配置 Kafka Producer 的详细信息，请参阅 [Apache Kafka 文档](https://kafka.apache.org/documentation.html)。
- *自定义分区器*：要将消息分配给特定的分区，可以向构造函数提供一个 `FlinkKafkaPartitioner` 的实现。这个分区器将被流中的每条记录调用，以确定消息应该发送到目标 topic 的哪个具体分区里。有关详细信息，请参阅 [Kafka Producer 分区方案](https://www.bookstack.cn/read/flink-1.11.1-zh/f4ce98c60273831b.md#kafka-producer-分区方案)。
- *高级的序列化 schema*：与 consumer 类似，producer 还允许使用名为 `KeyedSerializationSchema` 的高级序列化 schema，该 schema 允许单独序列化 key 和 value。它还允许覆盖目标 topic，以便 producer 实例可以将数据发送到多个 topic。

##### Kafka Producer 分区方案

默认情况下，如果没有为 Flink Kafka Producer 指定自定义分区程序，则 producer 将使用 `FlinkFixedPartitioner` 为每个 Flink Kafka Producer 并行子任务映射到单个 Kafka 分区（即，接收子任务接收到的所有消息都将位于同一个 Kafka 分区中）。

可以通过扩展 `FlinkKafkaPartitioner` 类来实现自定义分区程序。所有 Kafka 版本的构造函数都允许在实例化 producer 时提供自定义分区程序。 注意：分区器实现必须是可序列化的，因为它们将在 Flink 节点之间传输。此外，请记住分区器中的任何状态都将在作业失败时丢失，因为分区器不是 producer 的 checkpoint 状态的一部分。

也可以完全避免使用分区器，并简单地让 Kafka 通过其附加 key 写入的消息进行分区（使用提供的序列化 schema 为每条记录确定分区）。 为此，在实例化 producer 时提供 `null` 自定义分区程序，提供 `null` 作为自定义分区器是很重要的; 如上所述，如果未指定自定义分区程序，则默认使用 `FlinkFixedPartitioner`。

##### Kafka Producer 和容错

###### Kafka 0.10

启用 Flink 的 checkpointing 后，`FlinkKafkaProducer010` 可以提供至少一次的语义。

除了启用 Flink 的 checkpointing 之外，还应该适当地配置 setter 方法，`setLogFailuresOnly(boolean)` 和 `setFlushOnCheckpoint(boolean)`。

- `setLogFailuresOnly(boolean)`：默认情况下，此值设置为 `false`。启用这个选项将使 producer 仅记录失败而不是捕获和重新抛出它们。这基本上是记录了成功的记录，即使它从未写入目标 Kafka topic。对 at-least-once 的语义，这个方法必须禁用。
- `setFlushOnCheckpoint(boolean)`：默认情况下，此值设置为 `true`。启用此功能后，Flink 的 checkpoint 将在 checkpoint 成功之前等待 Kafka 确认 checkpoint 时的任意即时记录。这样可确保 checkpoint 之前的所有记录都已写入 Kafka。对 at-least-once 的语义，这个方法必须启用。

总之，默认情况下，Kafka producer 中，`setLogFailureOnly` 设置为 `false` 及 `setFlushOnCheckpoint` 设置为 `true` 会为 0.10 版本提供 at-least-once 语义。

**注意**：默认情况下，重试次数设置为 0。这意味着当 `setLogFailuresOnly` 设置为 `false` 时，producer 会立即失败，包括 leader 更改。该值默认设置为 0，以避免重试导致目标 topic 中出现重复的消息。对于大多数频繁更改 broker 的生产环境，我们建议将重试次数设置为更高的值。

**注意**：目前还没有 Kafka 的事务 producer，所以 Flink 不能保证写入 Kafka topic 的精准一次语义。

###### Kafka 0.11 和更新的版本

启用 Flink 的 checkpointing 后，`FlinkKafkaProducer011`（适用于 Kafka >= 1.0.0 版本的 `FlinkKafkaProducer`）可以提供精准一次的语义保证。

除了启用 Flink 的 checkpointing，还可以通过将适当的 `semantic` 参数传递给 `FlinkKafkaProducer011`（适用于 Kafka >= 1.0.0 版本的 `FlinkKafkaProducer`）来选择三种不同的操作模式：

- `Semantic.NONE`：Flink 不会有任何语义的保证，产生的记录可能会丢失或重复。
- `Semantic.AT_LEAST_ONCE`（默认设置）：类似 `FlinkKafkaProducer010` 中的 `setFlushOnCheckpoint(true)`，这可以保证不会丢失任何记录（虽然记录可能会重复）。
- `Semantic.EXACTLY_ONCE`：使用 Kafka 事务提供精准一次的语义。无论何时，在使用事务写入 Kafka 时，都要记得为所有消费 Kafka 消息的应用程序设置所需的 `isolation.level`（ `read_committed` 或 `read_uncommitted` - 后者是默认值）。

###### 注意事项

`Semantic.EXACTLY_ONCE` 模式依赖于事务提交的能力。事务提交发生于触发 checkpoint 之前，以及从 checkpoint 恢复之后。如果从 Flink 应用程序崩溃到完全重启的时间超过了 Kafka 的事务超时时间，那么将会有数据丢失（Kafka 会自动丢弃超出超时时间的事务）。考虑到这一点，请根据预期的宕机时间来合理地配置事务超时时间。

默认情况下，Kafka broker 将 `transaction.max.timeout.ms` 设置为 15 分钟。此属性不允许为大于其值的 producer 设置事务超时时间。 默认情况下，`FlinkKafkaProducer011` 将 producer config 中的 `transaction.timeout.ms` 属性设置为 1 小时，因此在使用 `Semantic.EXACTLY_ONCE` 模式之前应该增加 `transaction.max.timeout.ms` 的值。

在 `KafkaConsumer` 的 `read_committed` 模式中，任何未结束（既未中止也未完成）的事务将阻塞来自给定 Kafka topic 的未结束事务之后的所有读取数据。 换句话说，在遵循如下一系列事件之后：

1. 用户启动了 `transaction1` 并使用它写了一些记录
2. 用户启动了 `transaction2` 并使用它编写了一些其他记录
3. 用户提交了 `transaction2`

即使 `transaction2` 中的记录已提交，在提交或中止 `transaction1` 之前，消费者也不会看到这些记录。这有 2 层含义：

- 首先，在 Flink 应用程序的正常工作期间，用户可以预料 Kafka 主题中生成的记录的可见性会延迟，相当于已完成 checkpoint 之间的平均时间。
- 其次，在 Flink 应用程序失败的情况下，此应用程序正在写入的供消费者读取的主题将被阻塞，直到应用程序重新启动或配置的事务超时时间过去后，才恢复正常。此标注仅适用于有多个 agent 或者应用程序写入同一 Kafka 主题的情况。

**注意**：`Semantic.EXACTLY_ONCE` 模式为每个 `FlinkKafkaProducer011` 实例使用固定大小的 KafkaProducer 池。每个 checkpoint 使用其中一个 producer。如果并发 checkpoint 的数量超过池的大小，`FlinkKafkaProducer011` 将抛出异常，并导致整个应用程序失败。请合理地配置最大池大小和最大并发 checkpoint 数量。

**注意**：`Semantic.EXACTLY_ONCE` 会尽一切可能不留下任何逗留的事务，否则会阻塞其他消费者从这个 Kafka topic 中读取数据。但是，如果 Flink 应用程序在第一次 checkpoint 之前就失败了，那么在重新启动此类应用程序后，系统中不会有先前池大小（pool size）相关的信息。因此，在第一次 checkpoint 完成前对 Flink 应用程序进行缩容，且并发数缩容倍数大于安全系数 `FlinkKafkaProducer011.SAFE_SCALE_DOWN_FACTOR` 的值的话，是不安全的。

#### 在 Kafka 0.10.x 中使用 Kafka 时间戳和 Flink 事件时间

自 Apache Kafka 0.10+ 以来，Kafka 的消息可以携带[时间戳](https://cwiki.apache.org/confluence/display/KAFKA/KIP-32+-+Add+timestamps+to+Kafka+message)，指示事件发生的时间（请参阅 [Apache Flink 中的”事件时间”](https://www.bookstack.cn/read/flink-1.11.1-zh/2b6c152db3715058.md)）或消息写入 Kafka broker 的时间。

如果 Flink 中的时间特性设置为 `TimeCharacteristic.EventTime`（ `StreamExecutionEnvironment.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)`），则 `FlinkKafkaConsumer010` 将发出附加时间戳的记录。

Kafka consumer 不会发出 watermark。为了发出 watermark，可采用上文 “Kafka Consumer 和时间戳抽取以及 watermark 发送” 章节中描述的 `assignTimestampsAndWatermarks` 方法。

使用 Kafka 的时间戳时，无需定义时间戳提取器。`extractTimestamp()` 方法的 `previousElementTimestamp` 参数包含 `Kafka` 消息携带的时间戳。

Kafka consumer 的时间戳提取器应该是这样的：

```java
public long extractTimestamp(Long element, long previousElementTimestamp) {
    return previousElementTimestamp;
}
```

只有设置了 `setWriteTimestampToKafka(true)`，则 `FlinkKafkaProducer010` 才会发出记录时间戳。

```java
FlinkKafkaProducer010.FlinkKafkaProducer010Configuration config = FlinkKafkaProducer010.writeToKafkaWithTimestamps(streamWithTimestamps, topic, new SimpleStringSchema(), standardProps);
config.setWriteTimestampToKafka(true);
```

#### Kafka 连接器指标

Flink 的 Kafka 连接器通过 Flink 的 [metric 系统](https://www.bookstack.cn/read/flink-1.11.1-zh/fc8e043486b5e026.md) 提供一些指标来分析 Kafka Connector 的状况。Producer 通过 Flink 的 metrics 系统为所有支持的版本导出 Kafka 的内部指标。consumer 从 Kafka 0.10 版本开始导出所有指标。Kafka 文档在其[文档](http://kafka.apache.org/documentation/#selector_monitoring)中列出了所有导出的指标。

除了这些指标之外，所有 consumer 都暴露了每个主题分区的 `current-offsets` 和 `committed-offsets`。`current-offsets` 是指分区中的当前偏移量。指的是我们成功检索和发出的最后一个元素的偏移量。`committed-offsets` 是最后提交的偏移量。这为用户提供了 at-least-once 语义，用于提交给 Zookeeper 或 broker 的偏移量。对于 Flink 的偏移检查点，系统提供精准一次语义。

提交给 ZK 或 broker 的偏移量也可以用来跟踪 Kafka consumer 的读取进度。每个分区中提交的偏移量和最近偏移量之间的差异称为 *consumer lag*。如果 Flink 拓扑消耗来自 topic 的数据的速度比添加新数据的速度慢，那么延迟将会增加，consumer 将会滞后。对于大型生产部署，我们建议监视该指标，以避免增加延迟。

#### 启用 Kerberos 身份验证

Flink 通过 Kafka 连接器提供了一流的支持，可以对 Kerberos 配置的 Kafka 安装进行身份验证。只需在 `flink-conf.yaml` 中配置 Flink。像这样为 Kafka 启用 Kerberos 身份验证：

1. 通过设置以下内容配置 Kerberos 票据
   - `security.kerberos.login.use-ticket-cache`：默认情况下，这个值是 `true`，Flink 将尝试在 `kinit` 管理的票据缓存中使用 Kerberos 票据。注意！在 YARN 上部署的 Flink jobs 中使用 Kafka 连接器时，使用票据缓存的 Kerberos 授权将不起作用。使用 Mesos 进行部署时也是如此，因为 Mesos 部署不支持使用票据缓存进行授权。
   - `security.kerberos.login.keytab` 和 `security.kerberos.login.principal`：要使用 Kerberos keytabs，需为这两个属性设置值。
2. 将 `KafkaClient` 追加到 `security.kerberos.login.contexts`：这告诉 Flink 将配置的 Kerberos 票据提供给 Kafka 登录上下文以用于 Kafka 身份验证。

一旦启用了基于 Kerberos 的 Flink 安全性后，只需在提供的属性配置中包含以下两个设置（通过传递给内部 Kafka 客户端），即可使用 Flink Kafka Consumer 或 Producer 向 Kafk a进行身份验证：

- 将 `security.protocol` 设置为 `SASL_PLAINTEXT`（默认为 `NONE`）：用于与 Kafka broker 进行通信的协议。使用独立 Flink 部署时，也可以使用 `SASL_SSL`;请在[此处](https://kafka.apache.org/documentation/#security_configclients)查看如何为 SSL 配置 Kafka 客户端。
- 将 `sasl.kerberos.service.name` 设置为 `kafka`（默认为 `kafka`）：此值应与用于 Kafka broker 配置的 `sasl.kerberos.service.name` 相匹配。客户端和服务器配置之间的服务名称不匹配将导致身份验证失败。

有关 Kerberos 安全性 Flink 配置的更多信息，请参见[这里](https://www.bookstack.cn/read/flink-1.11.1-zh/32b496b3c89e8f90.md)。你也可以在[这里](https://www.bookstack.cn/read/flink-1.11.1-zh/af7ad5161f88091c.md)进一步了解 Flink 如何在内部设置基于 kerberos 的安全性。

#### 问题排查

如果你在使用 Flink 时对 Kafka 有问题，请记住，Flink 只封装 [KafkaConsumer](https://kafka.apache.org/documentation/#consumerapi) 或 [KafkaProducer](https://kafka.apache.org/documentation/#producerapi)，你的问题可能独立于 Flink，有时可以通过升级 Kafka broker 程序、重新配置 Kafka broker 程序或在 Flink 中重新配置 KafkaConsumer 或 KafkaProducer 来解决。下面列出了一些常见问题的示例。

##### 数据丢失

根据你的 Kafka 配置，即使在 Kafka 确认写入后，你仍然可能会遇到数据丢失。特别要记住在 Kafka 的配置中设置以下属性：

- `acks`
- `log.flush.interval.messages`
- `log.flush.interval.ms`
- `log.flush.*`

上述选项的默认值是很容易导致数据丢失的。请参考 Kafka 文档以获得更多的解释。

##### UnknownTopicOrPartitionException

导致此错误的一个可能原因是正在进行新的 leader 选举，例如在重新启动 Kafka broker 之后或期间。这是一个可重试的异常，因此 Flink job 应该能够重启并恢复正常运行。也可以通过更改 producer 设置中的 `retries` 属性来规避。但是，这可能会导致重新排序消息，反过来可以通过将 `max.in.flight.requests.per.connection` 设置为 1 来避免不需要的消息。

### Kafka测试代码

```java
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011;

import java.util.Properties;

public class Kafka {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        // 1.kafka配置
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "192.168.17.130:9092");
        properties.setProperty("group.id", "consumer-group");

        // 2.实例化一个消费者
        FlinkKafkaConsumer011 kafkaConsumer = new FlinkKafkaConsumer011("t1", new SimpleStringSchema(), properties);
        //  指定消费策略
        // kafkaConsumer.setStartFromEarliest(); // - 从最早的记录开始；
        // kafkaConsumer.setStartFromTimestamp(null); // 从指定的epoch时间戳（毫秒）开始；
        // kafkaConsumer.setStartFromGroupOffsets(); // 默认行为，从上次消费的偏移量进行继续消费。
        kafkaConsumer.setStartFromLatest(); //- 从最新记录开始；
        kafkaConsumer.setCommitOffsetsOnCheckpoints(true); //设置在检查点提交偏移量

        // 3.消费kafka数据
        DataStreamSource data  = env.addSource(kafkaConsumer);

        // 4.输出数据到kafka
        FlinkKafkaProducer011<String> myProducer = new FlinkKafkaProducer011<String>("192.168.17.130:9092","sinkTest",new SimpleStringSchema());
        myProducer.setWriteTimestampToKafka(true);
        data.addSink(myProducer);

        env.execute("java stream connectors kafka job");
    }
}
```

## DataSet Connectors



## Table & SQL Connectors

### 概览

Flink本地支持各种连接器。下表列出了所有可用的连接器。

| Name                                                         | Version   | Source                             | Sink                       |
| :----------------------------------------------------------- | :-------- | :--------------------------------- | :------------------------- |
| [Filesystem](https://www.bookstack.cn/read/flink-1.11.1-zh/614907ebd286edea.md) |           | Bounded and Unbounded Scan, Lookup | Streaming Sink, Batch Sink |
| [Elasticsearch](https://www.bookstack.cn/read/flink-1.11.1-zh/3db3ec87159f9067.md) | 6.x & 7.x | Not supported                      | Streaming Sink, Batch Sink |
| [Apache Kafka](https://www.bookstack.cn/read/flink-1.11.1-zh/3be15c65e6c19c0c.md) | 0.10+     | Unbounded Scan                     | Streaming Sink, Batch Sink |
| [JDBC](https://www.bookstack.cn/read/flink-1.11.1-zh/28a2e931458de229.md) |           | Bounded Scan, Lookup               | Streaming Sink, Batch Sink |
| [Apache HBase](https://www.bookstack.cn/read/flink-1.11.1-zh/087b7ed635207cf6.md) | 1.4.x     | Bounded Scan, Lookup               | Streaming Sink, Batch Sink |

### Formats

#### csv

官网：https://ci.apache.org/projects/flink/flink-docs-stable/zh/dev/table/connectors/formats/csv.html

[CSV](https://zh.wikipedia.org/wiki/逗号分隔值) Format 允许我们基于 CSV schema 进行解析和生成 CSV 数据。 目前 CSV schema 是基于 table schema 推断而来的。

##### 依赖

为了建立CSV格式，下列的表格提供了为项目使用自动化工具（例如Maven或者SBT）以及SQL客户端使用SQL JAR包的依赖信息。

| Maven依赖   | SQL 客户端 JAR |
| :---------- | :------------- |
| `flink-csv` | 内置           |

##### 如何创建使用 CSV 格式的表

以下是一个使用 Kafka 连接器和 CSV 格式创建表的示例。

```
CREATE TABLE user_behavior (
  user_id BIGINT,
  item_id BIGINT,
  category_id BIGINT,
  behavior STRING,
  ts TIMESTAMP(3)
) WITH (
 'connector' = 'kafka',
 'topic' = 'user_behavior',
 'properties.bootstrap.servers' = 'localhost:9092',
 'properties.group.id' = 'testGroup',
 'format' = 'csv',
 'csv.ignore-parse-errors' = 'true',
 'csv.allow-comments' = 'true'
)
```

##### Format 参数

| 参数                        | 是否必选 | 默认值 |  类型   |                             描述                             |
| :-------------------------- | :------: | :----: | :-----: | :----------------------------------------------------------: |
| format                      |   必选   | (none) | String  |            指定要使用的格式，这里应该是 `'csv'`。            |
| csv.field-delimiter         |   可选   |  `,`   | String  |                   字段分隔符 (默认`','`)。                   |
| csv.line-delimiter          |   可选   |  `\n`  | String  | 行分隔符, 默认`\n`。注意 `\n` 和 `\r` 是不可见的特殊符号, 在显式的 SQL 语句中必须使用 unicode 编码。例如 `'csv.line-delimiter' = U&'\\000D'` 使用换行符号 `\r` 作为行分隔符。例如 `'csv.line-delimiter' = U&'\\000A'` 使用换行符号 `\n` 作为行分隔符。 |
| csv.disable-quote-character |   可选   | false  | Boolean | 是否禁止对引用的值使用引号 (默认是 false). 如果禁止，选项 `'csv.quote-character'` 不能设置。 |
| csv.quote-character         |   可选   |  `"`   | String  |             用于围住字段值的引号字符 (默认`"`).              |
| csv.allow-comments          |   可选   | false  | Boolean | 是否允许忽略注释行（默认不允许），注释行以 `'#'` 作为起始字符。 如果允许注释行，请确保 `csv.ignore-parse-errors` 也开启了从而允许空行。 |
| csv.ignore-parse-errors     |   可选   | false  | Boolean | 当解析异常时，是跳过当前字段或行，还是抛出错误失败（默认为 false，即抛出错误失败）。如果忽略字段的解析异常，则会将该字段值设置为`null`。 |
| csv.array-element-delimiter |   可选   |  `;`   | String  |             分隔数组和行元素的字符串(默认`';'`).             |
| csv.escape-character        |   可选   | (none) | String  |                     转义字符(默认关闭).                      |
| csv.null-literal            |   可选   | (none) | String  |             是否将 "null" 字符串转化为 null 值。             |

##### 数据类型映射

目前 CSV 的 schema 都是从 table schema 推断而来的。显式地定义 CSV schema 暂不支持。 Flink 的 CSV Format 数据使用 [jackson databind API](https://github.com/FasterXML/jackson-databind) 去解析 CSV 字符串。

下面的表格列出了flink数据和CSV数据的对应关系。

| Flink SQL 类型            | CSV 类型                        |
| :------------------------ | :------------------------------ |
| `CHAR / VARCHAR / STRING` | `string`                        |
| `BOOLEAN`                 | `boolean`                       |
| `BINARY / VARBINARY`      | `string with encoding: base64`  |
| `DECIMAL`                 | `number`                        |
| `TINYINT`                 | `number`                        |
| `SMALLINT`                | `number`                        |
| `INT`                     | `number`                        |
| `BIGINT`                  | `number`                        |
| `FLOAT`                   | `number`                        |
| `DOUBLE`                  | `number`                        |
| `DATE`                    | `string with format: date`      |
| `TIME`                    | `string with format: time`      |
| `TIMESTAMP`               | `string with format: date-time` |
| `INTERVAL`                | `number`                        |
| `ARRAY`                   | `array`                         |
| `ROW`                     | `object`                        |





### 连接Json字符串

##### xdr数据读取

```java
String json = "{\"success\":0,\"rows\":43,\"spend\":2271,\"data\":{\"mwsipflvolte_byimsi\":[\"3937590040570684385|460021209319788|121680130|8615120952053|||10.184.199.135|10.191.94.7|||1596505509581729|1596505518236993|1596505509581729|1596505518236993||1596505511226737|1596505510597740|||||||||||||90|69|8613158932826|8615120952053||460021209319788||861997044960350|2|1|INVITE|UDP|10.184.198.228|5060||||0|0|sbc-domain=hksbc01.0898.101.hi.chinamobile.com|4600075EB740B102|200|200|||RTP/AVP|10.184.207.218|33412|10.184.204.25|15096|29|600|2000||||||||||||0|0|||||||||||4955|1573|0|0|0|0|1|4|1|1596505509587545|6ttzmzbt2xk75zk9588p9oksxsmpzo6p@10.18.5.64|46000121680130|30187|||sendrecv|1596505511226737||2|||||2|||1|0|0|0|0||||1596505518236993|1||||||INVITE|1||||||||ab8c6e43fa63320200804094509|||10.184.198.228|scscf2.hi.chinamobile.com|sip:+8615120952053@10.191.94.4;user=phone|tel:13158932826;cpc=ordinary|sip:13158932826@10.184.198.228:5060;user=phone|||100rel,timer,histinfo,precondition|2409:880a:8088:6b8b:cd89:4a62:97a0:d3a|||precondition,100rel||\"]}}";
JSONObject jsonObject = JSONObject.parseObject(json);
JSONObject data = jsonObject.getJSONObject("data");

for(String tablename : tblString.split(",")){
            JSONArray tableArr = data.getJSONArray(tablename + "_byimsi");
            TableDefinition tableDefinition = new TableDefinition();
            tableDefinition.init(tablename);
            List<String> fieldsTypes = tableDefinition.getFieldTypes();
            TypeInformation[] fieldTypeInfo = new TypeInformation[fieldsTypes.size()];
            int fieldNum = fieldsTypes.size();
            for (int i = 0; i < fieldNum; i++) {
                TypeInformation<?> ti = null;
                String fieldType = fieldsTypes.get(i);
                if(fieldType.equals("bigint") || fieldType.equals("int") || fieldType.equals("tinyint") || fieldType.equals("smallint")){
                    ti = Types.LONG;
                }else if(fieldType.equals("bit")){
                    ti = Types.SHORT;
                }else if(fieldType.equals("real")){
                    ti = Types.FLOAT;
                }else if(fieldType.equals("datetime")){
                    ti = Types.LONG;
                }else if(fieldType.equals("varchar") || fieldType.equals("nvarchar") || fieldType.equals("ipv4") || fieldType.equals("ipv6") || fieldType.equals("varbinary") ){
                    ti = Types.STRING;
                }else{
                    System.out.println("没有找到类型：" + fieldType);
                }
                
                fieldTypeInfo[i] = ti;
            }

            // 把数据放到Row对象里
            List<Row> list = new ArrayList<>();
            if( tableArr != null && tableArr.size() > 0){

                for (int i =0; i < tableArr.size() ;i++){
                    Row row = mySplit(tableArr.getString(i),fieldsTypes.size(), fieldsTypes);
                    list.add(row);
                }

            }else{
                Row row = new Row(fieldNum);
                for (int i = 0; i < fieldNum; i++) {
                    row.setField(i, null);
                }
                list.add(row);
            }

            TypeInformation<Row> tye = new RowTypeInfo(fieldTypeInfo);
            DataSet dataSet = env.fromCollection(list, tye);
            tableEnv.createTemporaryView(tablename, dataSet, tableDefinition.toFieldNamesString());

        }
```



### 连接到文件系统

连接外部系统在Catalog中注册表，直接调用tableEnv.connect()就可以，里面参数要传入一个ConnectorDescriptor，也就是connector描述器。对于文件系统的connector而言，flink内部已经提供了，就叫做FileSystem()。

代码如下：

```{.scala}
tableEnv
  .connect(new FileSystem().path("sensor.txt"))  // 定义表数据来源，外部连接
  .withFormat( new Csv()
      .fieldDelimiter(',')         // optional: field delimiter character (',' by default)
      .lineDelimiter("\r\n")       // optional: line delimiter ("\n" by default; otherwise "\r", "\r\n", or "" are allowed)
  ) // 定义从外部文件读取数据之后的格式化方法
  .withSchema(
    new Schema()
      .field("id", DataTypes.STRING())
      .field("timestamp", DataTypes.BIGINT())
      .field("temperature", DataTypes.DOUBLE())
  )    // 定义表结构
  .createTemporaryTable("inputTable")    // 创建临时表
```

### 连接kafka

kafka的连接器flink-kafka-connector中，1.10版本的已经提供了Table API的支持。我们可以在 connect方法中直接传入一个叫做Kafka的类，这就是kafka连接器的描述器ConnectorDescriptor。

```{.scala}
tableEnv
  .connect(
    new Kafka()
      .version("0.11") // 定义kafka的版本
      .topic("sensor") // 定义主题
      .property("zookeeper.connect", "192.158.17.130:2181")
      .property("bootstrap.servers", "192.158.17.130:9092")
  )
      .withFormat( new Csv()
        .fieldDelimiter(',')         // optional: field delimiter character (',' by default)
        .lineDelimiter("\r\n")       // optional: line delimiter ("\n" by default; otherwise "\r", "\r\n", or "" are allowed)
      ) // 定义从外部文件读取数据之后的格式化方法
  .withSchema(
    new Schema()
      .field("id", DataTypes.STRING())
      .field("timestamp", DataTypes.BIGINT())
      .field("temperature", DataTypes.DOUBLE())
  )
  .createTemporaryTable("kafkaInputTable")
```

当然也可以连接到ElasticSearch、MySql、HBase、Hive等外部系统，实现方式基本上是类似的。

### 连接MySql

```sql
CREATE TABLE MyUserTable (
  ...
) WITH (
  'connector.type' = 'jdbc', -- required: specify this table type is jdbc
  
  'connector.url' = 'jdbc:mysql://localhost:3306/flink-test', -- required: JDBC DB url
  
  'connector.table' = 'jdbc_table_name',  -- required: jdbc table name

  -- optional: the class name of the JDBC driver to use to connect to this URL.
  -- If not set, it will automatically be derived from the URL.
  'connector.driver' = 'com.mysql.jdbc.Driver',

  -- optional: jdbc user name and password
  'connector.username' = 'name',
  'connector.password' = 'password',
  
  -- **followings are scan options, optional, used when reading from a table**

  -- optional: SQL query / prepared statement.
  -- If set, this will take precedence over the 'connector.table' setting
  'connector.read.query' = 'SELECT * FROM sometable',

  -- These options must all be specified if any of them is specified. In addition,
  -- partition.num must be specified. They describe how to partition the table when
  -- reading in parallel from multiple tasks. partition.column must be a numeric,
  -- date, or timestamp column from the table in question. Notice that lowerBound and
  -- upperBound are just used to decide the partition stride, not for filtering the
  -- rows in table. So all rows in the table will be partitioned and returned.

  'connector.read.partition.column' = 'column_name', -- optional: the column name used for partitioning the input.
  'connector.read.partition.num' = '50', -- optional: the number of partitions.
  'connector.read.partition.lower-bound' = '500', -- optional: the smallest value of the first partition.
  'connector.read.partition.upper-bound' = '1000', -- optional: the largest value of the last partition.

  -- optional, Gives the reader a hint as to the number of rows that should be fetched
  -- from the database when reading per round trip. If the value specified is zero, then
  -- the hint is ignored. The default value is zero.
  'connector.read.fetch-size' = '100',

  -- **followings are lookup options, optional, used in temporary join**

  -- optional, max number of rows of lookup cache, over this value, the oldest rows will
  -- be eliminated. "cache.max-rows" and "cache.ttl" options must all be specified if any
  -- of them is specified. Cache is not enabled as default.
  'connector.lookup.cache.max-rows' = '5000',

  -- optional, the max time to live for each rows in lookup cache, over this time, the oldest rows
  -- will be expired. "cache.max-rows" and "cache.ttl" options must all be specified if any of
  -- them is specified. Cache is not enabled as default.
  'connector.lookup.cache.ttl' = '10s',

  'connector.lookup.max-retries' = '3', -- optional, max retry times if lookup database failed

  -- **followings are sink options, optional, used when writing into table**

  -- optional, flush max size (includes all append, upsert and delete records),
  -- over this number of records, will flush data. The default value is "5000".
  'connector.write.flush.max-rows' = '5000',

  -- optional, flush interval mills, over this time, asynchronous threads will flush data.
  -- The default value is "0s", which means no asynchronous flush thread will be scheduled.
  'connector.write.flush.interval' = '2s',

  -- optional, max retry times if writing records to database failed
  'connector.write.max-retries' = '3'
)
```

**Upsert sink:** Flink自动从查询中提取有效键。 例如，查询SELECT a，b，c FROM t GROUP BY a，b定义了字段a和b的组合键。 如果将JDBC表用作upsert接收器，请确保查询的键是基础数据库的唯一键集或主键之一。 这样可以保证输出结果符合预期。

**Temporary Join:** JDBC连接器可以在临时联接中用作查找源。 当前，仅支持同步查找模式。 如果指定了查找缓存选项（connector.lookup.cache.max-rows和connector.lookup.cache.ttl），则必须全部指定它们。 查找缓存用于通过首先查询缓存而不是将所有请求发送到远程数据库来提高临时连接JDBC连接器的性能。 但是，如果来自缓存，则返回的值可能不是最新的。 因此，这是吞吐量和正确性之间的平衡。

**Writing:** 默认情况下，connector.write.flush.interval为0s，connector.write.flush.max-rows为5000，这意味着对于低流量查询，缓冲的输出行可能不会长时间刷新到数据库。 因此，建议设置间隔配置。

### 连接Oracle

![image-20200813184005934](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200813184005934.png)

### 连接Hbase

HBase连接器允许读取和写入HBase群集。

连接器可以在upsert模式下运行，以使用查询定义的密钥与外部系统交换UPSERT / DELETE消息。

对于 append-only 查询，连接器还可以在追加模式下操作，以仅与外部系统交换INSERT消息。

```java
.connect(
  new HBase()
    .version("1.4.3")                      // required: currently only support "1.4.3"
    .tableName("hbase_table_name")         // required: HBase table name
    .zookeeperQuorum("localhost:2181")     // required: HBase Zookeeper quorum configuration
    .zookeeperNodeParent("/test")          // optional: the root dir in Zookeeper for HBase cluster.
                                           // The default value is "/hbase".
    .writeBufferFlushMaxSize("10mb")       // optional: writing option, determines how many size in memory of buffered
                                           // rows to insert per round trip. This can help performance on writing to JDBC
                                           // database. The default value is "2mb".
    .writeBufferFlushMaxRows(1000)         // optional: writing option, determines how many rows to insert per round trip.
                                           // This can help performance on writing to JDBC database. No default value,
                                           // i.e. the default flushing is not depends on the number of buffered rows.
    .writeBufferFlushInterval("2s")        // optional: writing option, sets a flush interval flushing buffered requesting
                                           // if the interval passes, in milliseconds. Default value is "0s", which means
                                           // no asynchronous flush thread will be scheduled.
)
```

Columns: HBase表中的所有列系列必须声明为ROW类型，字段名称映射到列 family 名称，而嵌套的字段名称映射到列 qualifier 名称。 无需在结构中声明所有族和限定符，用户可以声明必要的内容。 除ROW type字段外，原子类型的唯一一个字段（例如STRING，BIGINT）将被识别为表的行键。 行键字段的名称没有任何限制。

Temporary join: 针对HBase的查找联接不使用任何缓存； 始终总是通过HBase客户端直接查询数据。



### 用户自定义Source和Sink

动态表是Flink的Table & SQL API的核心概念，用于以统一的方式处理有界和无界数据。

因为动态表只是一个逻辑概念，所以Flink本身并不拥有数据。相反，动态表的内容存储在外部系统(如数据库、键值存储、消息队列)或文件中。

*Dynamic sources* and *dynamic sinks* 可用于从外部系统读写数据。在文档中，源和汇通常用术语连接器来概括。

Flink为Kafka、Hive和不同的文件系统提供了预定义的连接器。有关内置表源和接收器的更多信息，请参阅 [connector section](https://www.bookstack.cn/read/flink-1.11.1-zh/7238acdfc9dd016c.md)。

本页面主要介绍如何开发自定义的、用户定义的连接器。

注意，作为FLIP-95的一部分，Flink 1.11中引入了新的表源和表接收接口。工厂接口也被重新设计了。flip95还没有完全实现。许多功能接口还不被支持(例如过滤器或分区下推)。如果有必要，也请看看[old table sources and sinks page](http://ci.apache.org/projects/flink/flink-docs-release-1.11/zh/dev/table/legacySourceSinks.html)。这些接口仍然支持向后兼容性。

#### 概览

在许多情况下，实现者不需要从头创建新的连接器，而是需要稍微修改现有连接器或挂钩到现有堆栈中。
在其他情况下，实现者希望创建专用的连接器。

本部分对这两种用例都有帮助。
它解释了表连接器的一般架构，从API中的纯声明到将在集群上执行的运行时代码。

填充的箭头显示了在转换过程中对象如何从一个阶段转换到下一个阶段的其他对象。

![Translation of table connectors](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/2b9de163d9c25f02f77aa63a2a5dcbe1.svg)

##### Metadata

表API和SQL都是声明式API。这包括表的声明。因此，执行`CREATE TABLE`语句会导致更新目标目录中的元数据。

对于大多数目录实现，外部系统中的物理数据不会因此操作而被修改。特定于连接器的依赖项还不必出现在类路径中。`WITH`子句中声明的选项既不进行验证，也不进行其他解释。

动态表的元数据(通过DDL创建或由目录提供)表示为`CatalogTable`的实例。必要时，表名将在内部解析为`CatalogTable`。

##### Planning

当涉及到表程序的规划和优化时，需要将`CatalogTable`解析为`DynamicTableSource`(用于读取SELECT查询)和`DynamicTableSink`(用于写入INSERT into语句)。

`DynamicTableSourceFactory`和`DynamicTableSinkFactory`提供了特定于连接器的逻辑，用于将编目表的元数据转换为`DynamicTableSource`和`DynamicTableSink`的实例。在大多数情况下，工厂的目的是验证选项(例如示例中的'port' = '5022')，配置编码/解码格式(如果需要)，并创建表连接器的参数化实例。

默认情况下，`DynamicTableSourceFactory`和`DynamicTableSinkFactory`的实例是使用Java的服务提供程序接口(SPI)发现的。连接器选项(例如示例中的'connector' = 'custom')必须与有效的工厂标识符相对应。

虽然在类命名中可能不明显，但`DynamicTableSource`和`DynamicTableSink`也可以被看作是有状态的工厂，它们最终生成具体的运行时实现来读写实际数据。

规划器使用源和接收器实例执行特定于连接器的双向通信，直到找到最佳逻辑计划为止。根据可选声明的能力接口(例如`SupportsProjectionPushDown`或`SupportsOverwrite`)，计划者可能会对实例应用更改，从而改变产生的运行时实现。

##### Runtime

逻辑计划完成后，计划者将从表连接器获得运行时实现。运行时逻辑是在Flink的核心连接器接口(如`InputFormat`或`SourceFunction`)中实现的。

这些接口按另一个抽象级别分组，作为`ScanRuntimeProvider`、`LookupRuntimeProvider`和`SinkRuntimeProvider`的子类。

例如，`OutputFormatProvider`(提供`org.apache. plugin .common.io. outputformat`)和`SinkFunctionProvider`(提供`org.apache. plugin . streams .api.functions.sink. sinkfunction`)都是策划者可以处理的`SinkRuntimeProvider`的具体实例。

#### Extension Points

This section explains the available interfaces for extending Flink’s table connectors.

##### Dynamic Table Factories

Dynamic table factories are used to configure a dynamic table connector for an external storage system from catalog and session information.

`org.apache.flink.table.factories.DynamicTableSourceFactory` can be implemented to construct a `DynamicTableSource`.

`org.apache.flink.table.factories.DynamicTableSinkFactory` can be implemented to construct a `DynamicTableSink`.

By default, the factory is discovered using the value of the `connector` option as the factory identifier and Java’s Service Provider Interface.

In JAR files, references to new implementations can be added to the service file:

```
META-INF/services/org.apache.flink.table.factories.Factory
```

The framework will check for a single matching factory that is uniquely identified by factory identifier and requested base class (e.g. `DynamicTableSourceFactory`).

The factory discovery process can be bypassed by the catalog implementation if necessary. For this, a catalog needs to return an instance that implements the requested base class in `org.apache.flink.table.catalog.Catalog#getFactory`.

##### Dynamic Table Source

By definition, a dynamic table can change over time.

When reading a dynamic table, the content can either be considered as:

- A changelog (finite or infinite) for which all changes are consumed continuously until the changelog is exhausted. This is represented by the `ScanTableSource` interface.
- A continuously changing or very large external table whose content is usually never read entirely but queried for individual values when necessary. This is represented by the `LookupTableSource` interface.

A class can implement both of these interfaces at the same time. The planner decides about their usage depending on the specified query.

###### Scan Table Source

A `ScanTableSource` scans all rows from an external storage system during runtime.

The scanned rows don’t have to contain only insertions but can also contain updates and deletions. Thus, the table source can be used to read a (finite or infinite) changelog. The returned *changelog mode* indicates the set of changes that the planner can expect during runtime.

For regular batch scenarios, the source can emit a bounded stream of insert-only rows.

For regular streaming scenarios, the source can emit an unbounded stream of insert-only rows.

For change data capture (CDC) scenarios, the source can emit bounded or unbounded streams with insert, update, and delete rows.

A table source can implement further abilitiy interfaces such as `SupportsProjectionPushDown` that might mutate an instance during planning. All abilities are listed in the `org.apache.flink.table.connector.source.abilities` package and in the documentation of `org.apache.flink.table.connector.source.ScanTableSource`.

The runtime implementation of a `ScanTableSource` must produce internal data structures. Thus, records must be emitted as `org.apache.flink.table.data.RowData`. The framework provides runtime converters such that a source can still work on common data structures and perform a conversion at the end.

###### Lookup Table Source

A `LookupTableSource` looks up rows of an external storage system by one or more keys during runtime.

Compared to `ScanTableSource`, the source does not have to read the entire table and can lazily fetch individual values from a (possibly continuously changing) external table when necessary.

Compared to `ScanTableSource`, a `LookupTableSource` does only support emitting insert-only changes currently.

Further abilities are not supported. See the documentation of `org.apache.flink.table.connector.source.LookupTableSource` for more information.

The runtime implementation of a `LookupTableSource` is a `TableFunction` or `AsyncTableFunction`. The function will be called with values for the given lookup keys during runtime.

##### Dynamic Table Sink

By definition, a dynamic table can change over time.

When writing a dynamic table, the content can always be considered as a changelog (finite or infinite) for which all changes are written out continuously until the changelog is exhausted. The returned *changelog mode* indicates the set of changes that the sink accepts during runtime.

For regular batch scenarios, the sink can solely accept insert-only rows and write out bounded streams.

For regular streaming scenarios, the sink can solely accept insert-only rows and can write out unbounded streams.

For change data capture (CDC) scenarios, the sink can write out bounded or unbounded streams with insert, update, and delete rows.

A table sink can implement further abilitiy interfaces such as `SupportsOverwrite` that might mutate an instance during planning. All abilities are listed in the `org.apache.flink.table.connector.sink.abilities` package and in the documentation of `org.apache.flink.table.connector.sink.DynamicTableSink`.

The runtime implementation of a `DynamicTableSink` must consume internal data structures. Thus, records must be accepted as `org.apache.flink.table.data.RowData`. The framework provides runtime converters such that a sink can still work on common data structures and perform a conversion at the beginning.

##### Encoding / Decoding Formats

Some table connectors accept different formats that encode and decode keys and/or values.

Formats work similar to the pattern `DynamicTableSourceFactory -> DynamicTableSource -> ScanRuntimeProvider`, where the factory is responsible for translating options and the source is responsible for creating runtime logic.

Because formats might be located in different modules, they are discovered using Java’s Service Provider Interface similar to [table factories](https://www.bookstack.cn/read/flink-1.11.1-zh/93e9c0980fa678ae.md#dynamic-table-factories). In order to discover a format factory, the dynamic table factory searches for a factory that corresponds to a factory identifier and connector-specific base class.

For example, the Kafka table source requires a `DeserializationSchema` as runtime interface for a decoding format. Therefore, the Kafka table source factory uses the value of the `value.format` option to discover a `DeserializationFormatFactory`.

The following format factories are currently supported:

```
org.apache.flink.table.factories.DeserializationFormatFactoryorg.apache.flink.table.factories.SerializationFormatFactory
```

The format factory translates the options into an `EncodingFormat` or a `DecodingFormat`. Those interfaces are another kind of factory that produce specialized format runtime logic for the given data type.

For example, for a Kafka table source factory, the `DeserializationFormatFactory` would return an `EncodingFormat<DeserializationSchema>` that can be passed into the Kafka table source.

Kafka案例

![image-20200820180332722](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200820180332722.png)

#### Socket案例

本节概述如何使用支持changelog语义的解码格式实现扫描表源。该示例说明了上述所有组件是如何一起工作的。它可以作为参考实现。

特别地，它展示了如何

创建解析和验证选项的工厂，

连接器,实现表

实现和发现自定义格式，

并使用提供的工具，如数据结构转换器和FactoryUtil。

表source使用一个简单的单线程SourceFunction打开一个套接字，该套接字侦听传入的字节。原始字节按可插拔格式解码成行。该格式需要一个changelog标志作为第一列。

![image-20200820180159708](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200820180159708.png)

参考官网：https://www.bookstack.cn/read/flink-1.11.1-zh/93e9c0980fa678ae.md



# 功能笔记

## 支持的数据类型

### 1、Java Tuple和 Scala Case类

Tuple是包含固定数量各种类型字段的复合类。Flink Java API提供了Tuple1-Tuple25。Tuple的字段可以是Flink的任意类型，甚至嵌套Tuple。

访问Tuple属性的方式有以下两种：

- 1.属性名(f0,f1…fn)

- 2.getField(int pos)

![image-20200812150735128](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200812150735128.png)

Scala的Case类（以及Scala的Tuple,实际是Case class的特殊类型）是包含了一定数量多种类型字段的组合类型。Tuple字段通过他们的1-offset名称定位，例如 _1代表第一个字段。Case class 通过字段名称获得：

```scala
case class WordCount(word: String, count: Int){
  var env = ExecutionEnvironment.getExecutionEnvironment
  val input = env.fromElements(
    WordCount("hello", 1),
    WordCount("world", 2)) // Case Class Data Set
   input.keyBy("word")// key by field expression "word"
  val input2 = env.fromElements(("hello", 1), ("world", 2)) // Tuple2 Data Set
  input2.keyBy(0, 1) // key by field positions 0 and 1
}
```



### 2、Java POJO

Java和Scala的类在满足下列条件时，将会被Flink视作特殊的POJO数据类型专门进行处理：

1.是公共类；

2.无参构造是公共的；

3.所有的属性都是可获得的（声明为公共的，或提供get,set方法）；

4.字段的类型必须是Flink支持的。Flink会用Avro来序列化任意的对象。

Flink会分析POJO类型的结构获知POJO的字段。POJO类型要比一般类型好用。此外，`Flink访问POJO要比一般类型更高效`。

```java
public class WordWithCount {
    public String word;
    public int count;
    public WordWithCount() {}
    public WordWithCount(String word, int count) { this.word = word; this.count = count; }
}

DataStream<WordWithCount> wordCounts = env.fromElements(
            new WordWithCount("hello", 1),
            new WordWithCount("world", 2));
```

### 3、基本类型

Flink支持Java和Scala所有的基本数据类型，比如 Integer，String,和Double。

### 4、通用类

Flink支持大多数的Java,Scala类（API和自定义）。包含不能序列化字段的类在增加一些限制后也可支持。遵循Java Bean规范的类一般都可以使用。

所有不能视为POJO的类Flink都会当做一般类处理。这些数据类型被视作黑箱，其内容是不可见的。通用类使用Kryo进行序列/反序列化。

### 5、值

通过实现org.apache.flinktypes.Value接口的read和write方法提供自定义代码来进行序列化/反序列化，而不是使用通用的序列化框架。

Flink预定义的值类型与原生数据类型是一一对应的(例如:ByteValue, ShortValue, IntValue, LongValue, FloatValue, DoubleValue, StringValue, CharValue, BooleanValue)。这些值类型作为原生数据类型的可变变体，他们的值是可以改变的，允许程序重用对象从而缓解GC的压力。

### 6、Hadoop Writables

它实现org.apache.hadoop.Writable接口的类型，该类型的序列化逻辑在write()和readFields()方法中实现。

### 7、特殊类型

Flink比较特殊的类型有以下两种：

1.Scala的 Either、Option和Try。

2.Java ApI有自己的Either实现。

## 旧版本的更新

## Flink和Spark Streaming

### 流（stream）和微批（micro-batching）

spark可以做到秒级延迟，flink可以做到毫秒级延迟

![image-20200712164332675](https://gitee.com/sanhenlei/study-notes/raw/master/大数据/数据处理/image/Flink笔记/image-20200712164332675.png)

### 数据模型

- spark采用RDD模型，spark streaming的DStream实际上也就是一组组小批数据RDD的集合
- flink基本数据模型是数据流，以及事件序列

### 运行时架构

- spark是批处理，将DAG划分为不同的stage，一个完成后才可以计算下一个
- flink是标准的流执行模式，一个事件在一个节点处理完后可以直接发往下一个环节上进行处理
# 概述

官网：

- 基于Hadoop的一个数据仓库工具，用来进行数据提取、转化、加载。一种可以存储、查询和分析存储在Hadoop中的大规模数据的机制

- 1、Hive中所有的数据都存储在 HDFS 中，没有专门的数据存储格式（可支持Text，SequenceFile，ParquetFile，RCFILE，ORCFile等）

- 2、只需要在创建表的时候告诉 Hive 数据中的列分隔符和行分隔符，Hive 就可以解析数据。

- 3、Hive 中包含以下数据模型：DB、Table，External Table，Partition，Bucket。

- - db：在hdfs中表现为${hive.metastore.warehouse.dir}目录下一个文件夹
  - table：在hdfs中表现所属db目录下一个文件夹
  - external table：与table类似，不过其数据存放位置可以在任意指定路径
  - partition：在hdfs中表现为table目录下的子目录
  - bucket：在hdfs中表现为同一个表目录下根据hash散列之后的多个文件

-  

- **怎么做**：将结构化的数据文件映射为一张数据库表，提供SQL查询功能，能将SQL语句转变成MapReduce任务来执行

- **为什么**：直接使用mapreduce学习成本高，复杂查询开发难度大。Hive提供类sql语法，减少学习成本且拓展方便。

- **优点**：

- ● 支持创建索引，优化数据查询。 [3] 

- ● 不同的存储类型，例如，纯文本文件、HBase 中的文件。 [3] 

- ● 将元数据保存在关系数据库中，大大减少了在查询过程中执行语义检查的时间。 [3] 

- ● 可以直接使用存储在Hadoop 文件系统中的数据。 [3] 

- ● 内置大量用户函数UDF 来操作时间、字符串和其他的数据挖掘工具，支持用户扩展UDF 函数来完成内置函数无法实现的操作。 [3] 

- ● 类SQL 的查询方式，将SQL 查询转换为MapReduce 的job 在Hadoop集群上执行。



# 安装

参考：https://blog.csdn.net/pucao_cug/article/details/71773665?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522161485074316780264057764%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=161485074316780264057764&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_v2~rank_v29-2-71773665.first_rank_v2_pc_rank_v29&utm_term=hive%E5%AE%89%E8%A3%85%E5%9C%A8hadoop

# 详解

参考：https://www.jianshu.com/p/3129d17ec012

## 一起学Hive系列

[Hive概述，Hive是什么](http://lxw1234.com/archives/2015/06/238.htm)

[Hive函数大全-完整版](http://lxw1234.com/archives/2015/06/251.htm)

[Hive中的数据库(Database)和表(Table)](http://lxw1234.com/archives/2015/06/265.htm)

[Hive的安装配置](http://lxw1234.com/archives/2015/06/269.htm)

[Hive的视图和分区](http://lxw1234.com/archives/2015/06/284.htm)

[Hive的动态分区](http://lxw1234.com/archives/2015/06/286.htm)

[向Hive表中加载数据](http://lxw1234.com/archives/2015/06/290.htm)

[使用Hive命令行](http://lxw1234.com/archives/2015/06/292.htm)

[Hive的查询语句SELECT](http://lxw1234.com/archives/2015/06/307.htm)

[Hive中Join的原理和机制](http://lxw1234.com/archives/2015/06/313.htm)

[Hive中Join的类型和用法](http://lxw1234.com/archives/2015/06/315.htm)

[Hive SQL的优化](http://lxw1234.com/archives/2015/06/317.htm)

[Hive整合HBase，操作HBase表](http://lxw1234.com/archives/2015/06/319.htm)

[Hive的元数据表结构详解](http://lxw1234.com/archives/2015/07/378.htm)

[分析Hive表和分区的统计信息(Statistics)](http://lxw1234.com/archives/2015/07/413.htm)

[Hive的WEB页面接口-HWI](http://lxw1234.com/archives/2015/07/419.htm)

[从Hive表中进行数据抽样-Sampling](http://lxw1234.com/archives/2015/08/444.htm)

[Hive UDF开发](http://lxw1234.com/archives/2015/08/454.htm)

[使用Hive API分析HQL的执行计划、Job数量和表的血缘关系](http://lxw1234.com/archives/2015/09/476.htm)

[自定义HiveServer2的用户安全认证](http://lxw1234.com/archives/2016/01/600.htm)

## 架构

![image-20210304171203360](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E5%A4%84%E7%90%86/image/Untitled/image-20210304171203360.png)

如图中所示，Hive通过给用户提供的一系列交互接口，接收到用户的指令(SQL)，使用自己的Driver，结合元数据(MetaStore)，将这些指令翻译成MapReduce，提交到Hadoop中执行，最后，将执行返回的结果输出到用户交互接口。
 在使用过程中，至需要将Hive看做是一个数据库就行，本身Hive也具备了数据库的很多特性和功能。

## 擅长什么

Hive可以使用HQL(Hive SQL)很方便的完成对海量数据的统计汇总，即席查询和分析，除了很多内置的函数，还支持开发人员使用其他编程语言和脚本语言来自定义函数。

但是，由于Hadoop本身是一个批处理，高延迟的计算框架，Hive使用Hadoop作为执行引擎，自然也就有了批处理，高延迟的特点，在数据量很小的时候，Hive执行也需要消耗较长时间来完成，这时候，就显示不出它与Oracle，Mysql等传统数据库的优势。

此外，Hive对事物的支持不够好，原因是HDFS本身就设计为一次写入，多次读取的分布式存储系统，因此，不能使用Hive来完成诸如DELETE、UPDATE等在线事务处理的需求。

因此，Hive擅长的是非实时的、离线的、对响应及时性要求不高的海量数据批量计算，即席查询，统计分析。

## 数据单元

- **Databases**：数据库。概念等同于关系型数据库的Schema，不多解释；
- **Tables**：表。概念等同于关系型数据库的表，不多解释；
- **Partitions**：分区。概念类似于关系型数据库的表分区，没有那么多分区类型，只支持固定分区，将同一组数据存放至一个固定的分区中。
- **Buckets** (or **Clusters**):分桶。同一个分区内的数据还可以细分，将相同的KEY再划分至一个桶中，这个有点类似于HASH分区，只不过这里是HASH分桶，也有点类似子分区吧。

## 数据类型

既然是被当做数据库来使用，除了数据单元，Hive当然也得有一些列的数据类型。这里先简单描述下，后续章节会有详细的介绍。

### 4.1 原始数据类型

- 整型
  - TINYINT — 微整型，只占用1个字节，只能存储0-255的整数。
  - SMALLINT– 小整型，占用2个字节，存储范围–32768 到 32767。
  - INT– 整型，占用4个字节，存储范围-2147483648到2147483647。
  - BIGINT– 长整型，占用8个字节，存储范围-263到263-1。
- 布尔型
  - BOOLEAN — TRUE/FALSE
- 浮点型
  - FLOAT– 单精度浮点数。
  - DOUBLE– 双精度浮点数。
- 字符串型
  - STRING– 不设定长度。

### 4.2 复合数据类型

- Structs：一组由任意数据类型组成的结构。比如，定义一个字段C的类型为STRUCT {a INT; b STRING}，则可以使用a和C.b来获取其中的元素值；
- Maps：和Java中的Map没什么区别，就是存储K-V对的；
- Arrays：就是数组而已；

## Hive窗口函数/分析函数详解

原创 园陌 [五分钟学大数据](javascript:void(0);)

在sql中有一类函数叫做聚合函数,例如sum()、avg()、max()等等,这类函数可以将多行数据按照规则聚集为一行,一般来讲聚集后的行数是要少于聚集前的行数的。但是有时我们想要既显示聚集前的数据,又要显示聚集后的数据,这时我们便引入了窗口函数。窗口函数又叫OLAP函数/分析函数，窗口函数兼具分组和排序功能。

窗口函数最重要的关键字是 **partition by** 和 **order by。**

具体语法如下：**over (partition by xxx order by xxx)**

### sum,avg,min,max 函数

准备数据

```sql
--建表语句:
create table bigdata_t1(
cookieid string,
createtime string,   --day 
pv int
) row format delimited 
fields terminated by ',';
 
--加载数据：
load data local inpath '/root/hivedata/bigdata_t1.dat' into table bigdata_t1;

cookie1,2018-04-10,1
cookie1,2018-04-11,5
cookie1,2018-04-12,7
cookie1,2018-04-13,3
cookie1,2018-04-14,2
cookie1,2018-04-15,4
cookie1,2018-04-16,4

--开启智能本地模式
SET hive.exec.mode.local.auto=true;
```

SUM函数和窗口函数的配合使用：结果和ORDER BY相关,默认为升序。

```sql
#pv1
select cookieid,createtime,pv,
sum(pv) over(partition by cookieid order by createtime) as pv1 
from bigdata_t1;

#pv2
select cookieid,createtime,pv,
sum(pv) over(partition by cookieid order by createtime rows between unbounded preceding and current row) as pv2
from bigdata_t1;

#pv3
select cookieid,createtime,pv,
sum(pv) over(partition by cookieid) as pv3
from bigdata_t1;

#pv4
select cookieid,createtime,pv,
sum(pv) over(partition by cookieid order by createtime rows between 3 preceding and current row) as pv4
from bigdata_t1;

#pv5
select cookieid,createtime,pv,
sum(pv) over(partition by cookieid order by createtime rows between 3 preceding and 1 following) as pv5
from bigdata_t1;

#pv6
select cookieid,createtime,pv,
sum(pv) over(partition by cookieid order by createtime rows between current row and unbounded following) as pv6
from bigdata_t1;


pv1: 分组内从起点到当前行的pv累积，如，11号的pv1=10号的pv+11号的pv, 12号=10号+11号+12号
pv2: 同pv1
pv3: 分组内(cookie1)所有的pv累加
pv4: 分组内当前行+往前3行，如，11号=10号+11号， 12号=10号+11号+12号，
                           13号=10号+11号+12号+13号， 14号=11号+12号+13号+14号
pv5: 分组内当前行+往前3行+往后1行，如，14号=11号+12号+13号+14号+15号=5+7+3+2+4=21
pv6: 分组内当前行+往后所有行，如，13号=13号+14号+15号+16号=3+2+4+4=13，
                             14号=14号+15号+16号=2+4+4=10
```

如果不指定rows between,默认为从起点到当前行;

如果不指定order by，则将分组内所有值累加;

关键是理解rows between含义,也叫做**window子句**：

preceding：往前

following：往后

current row：当前行

unbounded：起点

unbounded preceding 表示从前面的起点

unbounded following：表示到后面的终点

AVG，MIN，MAX，和SUM用法一样。

### row_number,rank,dense_rank,ntile函数

准备数据

```sql
cookie1,2018-04-10,1
cookie1,2018-04-11,5
cookie1,2018-04-12,7
cookie1,2018-04-13,3
cookie1,2018-04-14,2
cookie1,2018-04-15,4
cookie1,2018-04-16,4
cookie2,2018-04-10,2
cookie2,2018-04-11,3
cookie2,2018-04-12,5
cookie2,2018-04-13,6
cookie2,2018-04-14,3
cookie2,2018-04-15,9
cookie2,2018-04-16,7

CREATE TABLE bigdata_t2 (
cookieid string,
createtime string,   --day 
pv INT
) ROW FORMAT DELIMITED 
FIELDS TERMINATED BY ',' 
stored as textfile;

加载数据：
load data local inpath '/root/hivedata/bigdata_t2.dat' into table bigdata_t2;
```

- ROW_NUMBER()使用

  ROW_NUMBER()从1开始，按照顺序，生成分组内记录的序列。

```sql
SELECT 
cookieid,
createtime,
pv,
ROW_NUMBER() OVER(PARTITION BY cookieid ORDER BY pv desc) AS rn 
FROM bigdata_t2;
```

- RANK 和 DENSE_RANK使用

  RANK() 生成数据项在分组中的排名，排名相等会在名次中留下空位 。

  DENSE_RANK()生成数据项在分组中的排名，排名相等会在名次中不会留下空位。

```sql
SELECT 
cookieid,
createtime,
pv,
RANK() OVER(PARTITION BY cookieid ORDER BY pv desc) AS rn1,
DENSE_RANK() OVER(PARTITION BY cookieid ORDER BY pv desc) AS rn2,
ROW_NUMBER() OVER(PARTITION BY cookieid ORDER BY pv DESC) AS rn3 
FROM bigdata_t2 
WHERE cookieid = 'cookie1';
```

- NTILE

  有时会有这样的需求:如果数据排序后分为三部分，业务人员只关心其中的一部分，如何将这中间的三分之一数据拿出来呢?NTILE函数即可以满足。

  ntile可以看成是：把有序的数据集合平均分配到指定的数量（num）个桶中, 将桶号分配给每一行。如果不能平均分配，则优先分配较小编号的桶，并且各个桶中能放的行数最多相差1。

  然后可以根据桶号，选取前或后 n分之几的数据。数据会完整展示出来，只是给相应的数据打标签；具体要取几分之几的数据，需要再嵌套一层根据标签取出。

```
SELECT 
cookieid,
createtime,
pv,
NTILE(2) OVER(PARTITION BY cookieid ORDER BY createtime) AS rn1,
NTILE(3) OVER(PARTITION BY cookieid ORDER BY createtime) AS rn2,
NTILE(4) OVER(ORDER BY createtime) AS rn3
FROM bigdata_t2 
ORDER BY cookieid,createtime;
```

### lag,lead,first_value,last_value 函数

- LAG  
  **LAG(col,n,DEFAULT) 用于统计窗口内往上第n行值**第一个参数为列名，第二个参数为往上第n行（可选，默认为1），第三个参数为默认值（当往上第n行为NULL时候，取默认值，如不指定，则为NULL）

```sql
SELECT cookieid,
createtime,
url,
ROW_NUMBER() OVER(PARTITION BY cookieid ORDER BY createtime) AS rn,
LAG(createtime,1,'1970-01-01 00:00:00') OVER(PARTITION BY cookieid ORDER BY createtime) AS last_1_time,
LAG(createtime,2) OVER(PARTITION BY cookieid ORDER BY createtime) AS last_2_time 
FROM bigdata_t4;


last_1_time: 指定了往上第1行的值，default为'1970-01-01 00:00:00'  
                          cookie1第一行，往上1行为NULL,因此取默认值 1970-01-01 00:00:00
                          cookie1第三行，往上1行值为第二行值，2015-04-10 10:00:02
                          cookie1第六行，往上1行值为第五行值，2015-04-10 10:50:01
last_2_time: 指定了往上第2行的值，为指定默认值
                         cookie1第一行，往上2行为NULL
                         cookie1第二行，往上2行为NULL
                         cookie1第四行，往上2行为第二行值，2015-04-10 10:00:02
                         cookie1第七行，往上2行为第五行值，2015-04-10 10:50:01
```

- LEAD

  与LAG相反
  **LEAD(col,n,DEFAULT) 用于统计窗口内往下第n行值**
  第一个参数为列名，第二个参数为往下第n行（可选，默认为1），第三个参数为默认值（当往下第n行为NULL时候，取默认值，如不指定，则为NULL）

```sql
SELECT cookieid,
createtime,
url,
ROW_NUMBER() OVER(PARTITION BY cookieid ORDER BY createtime) AS rn,
LEAD(createtime,1,'1970-01-01 00:00:00') OVER(PARTITION BY cookieid ORDER BY createtime) AS next_1_time,
LEAD(createtime,2) OVER(PARTITION BY cookieid ORDER BY createtime) AS next_2_time 
FROM bigdata_t4;
```

- FIRST_VALUE

  取分组内排序后，截止到当前行，第一个值

```sql
SELECT cookieid,
createtime,
url,
ROW_NUMBER() OVER(PARTITION BY cookieid ORDER BY createtime) AS rn,
FIRST_VALUE(url) OVER(PARTITION BY cookieid ORDER BY createtime) AS first1 
FROM bigdata_t4;
```

- LAST_VALUE

  取分组内排序后，截止到当前行，最后一个值

```sql
SELECT cookieid,
createtime,
url,
ROW_NUMBER() OVER(PARTITION BY cookieid ORDER BY createtime) AS rn,
LAST_VALUE(url) OVER(PARTITION BY cookieid ORDER BY createtime) AS last1 
FROM bigdata_t4;
```

如果想要取分组内排序后最后一个值，则需要变通一下：

```sql
 SELECT cookieid,
 createtime,
 url,
 ROW_NUMBER() OVER(PARTITION BY cookieid ORDER BY createtime) AS rn,
 LAST_VALUE(url) OVER(PARTITION BY cookieid ORDER BY createtime) AS last1,
 FIRST_VALUE(url) OVER(PARTITION BY cookieid ORDER BY createtime DESC) AS last2 
 FROM bigdata_t4 
 ORDER BY cookieid,createtime;
```

**特别注意order  by**

如果不指定ORDER BY，则进行排序混乱，会出现错误的结果

```sql
 SELECT cookieid,
 createtime,
 url,
 FIRST_VALUE(url) OVER(PARTITION BY cookieid) AS first2  
 FROM bigdata_t4;
```

### cume_dist,percent_rank 函数

这两个序列分析函数不是很常用，**注意： 序列函数不支持WINDOW子句**

- 数据准备

```sql
 d1,user1,1000
 d1,user2,2000
 d1,user3,3000
 d2,user4,4000
 d2,user5,5000
 
 CREATE EXTERNAL TABLE bigdata_t3 (
 dept STRING,
 userid string,
 sal INT
 ) ROW FORMAT DELIMITED 
 FIELDS TERMINATED BY ',' 
 stored as textfile;
 
 加载数据：
 load data local inpath '/root/hivedata/bigdata_t3.dat' into table bigdata_t3;
```

- CUME_DIST  和order by的排序顺序有关系

  CUME_DIST 小于等于当前值的行数/分组内总行数  order 默认顺序 正序 升序
  比如，统计小于等于当前薪水的人数，所占总人数的比例

```sql
 SELECT 
 dept,
 userid,
 sal,
 CUME_DIST() OVER(ORDER BY sal) AS rn1,
 CUME_DIST() OVER(PARTITION BY dept ORDER BY sal) AS rn2 
 FROM bigdata_t3;
 
 rn1: 没有partition,所有数据均为1组，总行数为5，
      第一行：小于等于1000的行数为1，因此，1/5=0.2
      第三行：小于等于3000的行数为3，因此，3/5=0.6
 rn2: 按照部门分组，dpet=d1的行数为3,
      第二行：小于等于2000的行数为2，因此，2/3=0.6666666666666666
```

- PERCENT_RANK

  PERCENT_RANK 分组内当前行的RANK值-1/分组内总行数-1

```sql
 SELECT 
 dept,
 userid,
 sal,
 PERCENT_RANK() OVER(ORDER BY sal) AS rn1,   --分组内
 RANK() OVER(ORDER BY sal) AS rn11,          --分组内RANK值
 SUM(1) OVER(PARTITION BY NULL) AS rn12,     --分组内总行数
 PERCENT_RANK() OVER(PARTITION BY dept ORDER BY sal) AS rn2 
 FROM bigdata_t3;
 
 rn1: rn1 = (rn11-1) / (rn12-1) 
        第一行,(1-1)/(5-1)=0/4=0
        第二行,(2-1)/(5-1)=1/4=0.25
        第四行,(4-1)/(5-1)=3/4=0.75
 rn2: 按照dept分组，
      dept=d1的总行数为3
      第一行，(1-1)/(3-1)=0
      第三行，(3-1)/(3-1)=1
```

### grouping sets,grouping__id,cube,rollup 函数

这几个分析函数通常用于OLAP中，不能累加，而且需要根据不同维度上钻和下钻的指标统计，比如，分小时、天、月的UV数。

- 数据准备

```sql
 2018-03,2018-03-10,cookie1
 2018-03,2018-03-10,cookie5
 2018-03,2018-03-12,cookie7
 2018-04,2018-04-12,cookie3
 2018-04,2018-04-13,cookie2
 2018-04,2018-04-13,cookie4
 2018-04,2018-04-16,cookie4
 2018-03,2018-03-10,cookie2
 2018-03,2018-03-10,cookie3
 2018-04,2018-04-12,cookie5
 2018-04,2018-04-13,cookie6
 2018-04,2018-04-15,cookie3
 2018-04,2018-04-15,cookie2
 2018-04,2018-04-16,cookie1
 
 CREATE TABLE bigdata_t5 (
 month STRING,
 day STRING, 
 cookieid STRING 
 ) ROW FORMAT DELIMITED 
 FIELDS TERMINATED BY ',' 
 stored as textfile;
 
 加载数据：
 load data local inpath '/root/hivedata/bigdata_t5.dat' into table bigdata_t5;
```

- GROUPING SETS

  grouping sets是一种将多个group by 逻辑写在一个sql语句中的便利写法。

  等价于将不同维度的GROUP BY结果集进行UNION ALL。

  **GROUPING__ID**，表示结果属于哪一个分组集合。

```sql
 SELECT 
 month,
 day,
 COUNT(DISTINCT cookieid) AS uv,
 GROUPING__ID 
 FROM bigdata_t5 
 GROUP BY month,day 
 GROUPING SETS (month,day) 
 ORDER BY GROUPING__ID;
 
 grouping_id表示这一组结果属于哪个分组集合，
 根据grouping sets中的分组条件month，day，1是代表month，2是代表day
 
 等价于 
 SELECT month,NULL,COUNT(DISTINCT cookieid) AS uv,1 AS GROUPING__ID FROM bigdata_t5 GROUP BY month UNION ALL 
 SELECT NULL as month,day,COUNT(DISTINCT cookieid) AS uv,2 AS GROUPING__ID FROM bigdata_t5 GROUP BY day;
```

再如：

```sql
 SELECT 
 month,
 day,
 COUNT(DISTINCT cookieid) AS uv,
 GROUPING__ID 
 FROM bigdata_t5 
 GROUP BY month,day 
 GROUPING SETS (month,day,(month,day)) 
 ORDER BY GROUPING__ID;
 
 等价于
 SELECT month,NULL,COUNT(DISTINCT cookieid) AS uv,1 AS GROUPING__ID FROM bigdata_t5 GROUP BY month 
 UNION ALL 
 SELECT NULL,day,COUNT(DISTINCT cookieid) AS uv,2 AS GROUPING__ID FROM bigdata_t5 GROUP BY day
 UNION ALL 
 SELECT month,day,COUNT(DISTINCT cookieid) AS uv,3 AS GROUPING__ID FROM bigdata_t5 GROUP BY month,day;
```

- CUBE

  根据GROUP BY的维度的所有组合进行聚合。

```sql
 SELECT 
 month,
 day,
 COUNT(DISTINCT cookieid) AS uv,
 GROUPING__ID 
 FROM bigdata_t5 
 GROUP BY month,day 
 WITH CUBE 
 ORDER BY GROUPING__ID;
 
 等价于
 SELECT NULL,NULL,COUNT(DISTINCT cookieid) AS uv,0 AS GROUPING__ID FROM bigdata_t5
 UNION ALL 
 SELECT month,NULL,COUNT(DISTINCT cookieid) AS uv,1 AS GROUPING__ID FROM bigdata_t5 GROUP BY month 
 UNION ALL 
 SELECT NULL,day,COUNT(DISTINCT cookieid) AS uv,2 AS GROUPING__ID FROM bigdata_t5 GROUP BY day
 UNION ALL 
 SELECT month,day,COUNT(DISTINCT cookieid) AS uv,3 AS GROUPING__ID FROM bigdata_t5 GROUP BY month,day;
```

- ROLLUP

  是CUBE的子集，以最左侧的维度为主，从该维度进行层级聚合。

```sql
 比如，以month维度进行层级聚合：
 SELECT 
 month,
 day,
 COUNT(DISTINCT cookieid) AS uv,
 GROUPING__ID  
 FROM bigdata_t5 
 GROUP BY month,day
 WITH ROLLUP 
 ORDER BY GROUPING__ID;
 
 --把month和day调换顺序，则以day维度进行层级聚合：
 
 SELECT 
 day,
 month,
 COUNT(DISTINCT cookieid) AS uv,
 GROUPING__ID  
 FROM bigdata_t5 
 GROUP BY day,month 
 WITH ROLLUP 
 ORDER BY GROUPING__ID;
 （这里，根据天和月进行聚合，和根据天聚合结果一样，因为有父子关系，如果是其他维度组合的话，就会不一样）
```



# 问题

## 容易出错的 Hive sql 

原创 园陌 [五分钟学大数据](javascript:void(0);)

#### 前言

在进行数仓搭建和数据分析时最常用的就是 sql，其语法简洁明了，易于理解，目前大数据领域的几大主流框架全部都支持sql语法，包括 hive，spark，flink等，所以sql在大数据领域有着不可替代的作用，需要我们重点掌握。

在使用sql时如果不熟悉或不仔细，那么在进行查询分析时极容易出错，接下来我们就来看下几个容易出错的sql语句及使用注意事项。

#### 正文开始

#### 1. decimal

hive 除了支持 int,double,string等常用类型，也支持 decimal 类型，用于在数据库中存储精确的数值，常用在表示金额的字段上

**注意事项：**

如：decimal(11,2) 代表最多有11位数字，其中后2位是小数，整数部分是9位； 
如果**整数部分超过9位，则这个字段就会变成null，如果整数部分不超过9位，则原字段显示**； 
如果**小数部分不足2位，则后面用0补齐两位，如果小数部分超过两位，则超出部分四舍五入**； 
也可直接写 decimal，后面不指定位数，默认是 decimal(10,0) 整数10位，没有小数

#### 2. location

```sql
表创建的时候可以用 location 指定一个文件或者文件夹
create  table stu(id int ,name string)  location '/user/stu2';
```

**注意事项：**

创建表时使用location，
当**指定文件夹时，hive会加载文件夹下的所有文件，当表中无分区时，这个文件夹下不能再有文件夹，否则报错。**    
当表是分区表时，比如 partitioned by (day string)， 则这个文件夹下的每一个文件夹就是一个分区，且文件夹名为 day=20201123
这种格式，然后使用：**msck  repair  table  score**; 修复表结构，成功之后即可看到数据已经全部加载到表当中去了

#### 3. load data 和 load data local

```sql
从hdfs上加载文件
load data inpath '/hivedatas/techer.csv' into table techer;

从本地系统加载文件
load data local inpath '/user/test/techer.csv' into table techer;
```

**注意事项：**

1. 使用 load data local 表示**从本地文件系统加载，文件会拷贝到hdfs上**
2. 使用 load data 表示**从hdfs文件系统加载，文件会直接移动到hive相关目录下**，注意不是拷贝过去，因为hive认为hdfs文件已经有3副本了，没必要再次拷贝了
3. 如果表是分区表，load 时不指定分区会报错  
4. 如果加载相同文件名的文件，会被自动重命名

#### 4. drop 和 truncate

```sql
删除表操作
drop table score1;

清空表操作
truncate table score2;
```

**注意事项：**

如果 **hdfs 开启了回收站，drop 删除的表数据是可以从回收站恢复的**，表结构恢复不了，需要自己重新创建；**truncate 清空的表是不进回收站的，所以无法恢复truncate清空的表。**  
所以 truncate 一定慎用，一旦清空除物理恢复外将无力回天

#### 5. join 连接

```sql
INNER JOIN 内连接：只有进行连接的两个表中都存在与连接条件相匹配的数据才会被保留下来
select * from techer t [inner] join course c on t.t_id = c.t_id; -- inner 可省略

LEFT OUTER JOIN 左外连接：左边所有数据会被返回，右边符合条件的被返回
select * from techer t left join course c on t.t_id = c.t_id; -- outer可省略

RIGHT OUTER JOIN 右外连接：右边所有数据会被返回，左边符合条件的被返回、
select * from techer t right join course c on t.t_id = c.t_id;

FULL OUTER JOIN 满外(全外)连接: 将会返回所有表中符合条件的所有记录。如果任一表的指定字段没有符合条件的值的话，那么就使用NULL值替代。
SELECT * FROM techer t FULL JOIN course c ON t.t_id = c.t_id ;
```

**注意事项：**

1. hive2版本已经支持不等值连接，就是 **join on条件后面可以使用大于小于符号;并且也支持 join on 条件后跟or** (早前版本 on 后只支持 = 和 and，不支持 > < 和 or)
2. 如hive执行引擎使用MapReduce，一个join就会启动一个job，一条sql语句中如有多个join，则会启动多个job



**注意**：表之间用逗号(,)连接和 inner join 是一样的，例：

```
select tableA.id, tableB.name from tableA , tableB where tableA.id=tableB.id;   
和   
select tableA.id, tableB.name from tableA join tableB on tableA.id=tableB.id;   
```

**它们的执行效率没有区别，只是书写方式不同**，用逗号是sql 89标准，join 是sql 92标准。用逗号连接后面过滤条件用 where ，用 join 连接后面过滤条件是 on。

#### 6. left semi join

```sql
为什么把这个单独拿出来说，因为它和其他的 join 语句不太一样，
这个语句的作用和 in/exists 作用是一样的，是 in/exists 更高效的实现
SELECT A.* FROM A where id in (select id from B)

SELECT A.* FROM A left semi join B ON A.id=B.id

上述两个 sql 语句执行结果完全一样，只不过第二个执行效率高
```

**注意事项：**

1. left semi join 的限制是：join 子句中右边的表**只能在 on 子句中设置过滤条件**，在 where 子句、select 子句或其他地方过滤都不行。
2. left semi join 中 on 后面的过滤条件**只能是等于号**，不能是其他的。
3. left semi join 是只传递表的 join key 给 map 阶段，因此left semi join 中最后 select 的**结果只许出现左表**。
4. 因为 left semi join 是 in(keySet) 的关系，遇到**右表重复记录，左表会跳过**

#### 7. 聚合函数中 null 值

```
hive支持 count(),max(),min(),sum(),avg() 等常用的聚合函数
```

**注意事项：**

**聚合操作时要注意 null 值**：

count(*) 包含 null 值，统计所有行数； 
count(id) 不包含id为 null 的值； 
min **求最小值是不包含 null**，除非所有值都是 null； 
avg **求平均值也是不包含 null**。

**以上需要特别注意，null 值最容易导致算出错误的结果**

#### 8. 运算符中 null 值

```
hive 中支持常用的算术运算符(+,-,*,/)  
比较运算符(>, <, =)
逻辑运算符(in, not in)

以上运算符计算时要特别注意 null 值
```

**注意事项：**

1. **每行中的列字段相加或相减，如果含有 null 值，则结果为 null**  
   例：有一张商品表（product）

| id   | price | dis_amount |
| :--- | :---- | :--------- |
| 1    | 100   | 20         |
| 2    | 120   | null       |

各字段含义：id (商品id)、price (价格)、dis_amount (优惠金额)

我想算**每个商品优惠后实际的价格**，sql如下：

```sql
select id, price - dis_amount as real_amount from product;
```

得到结果如下：

| id   | real_amount |
| :--- | :---------- |
| 1    | 80          |
| 2    | null        |

id=2的商品价格为 null，结果是错误的。

我们可以**对 null 值进行处理**，sql如下：

```sql
select id, price - coalesce(dis_amount,0) as real_amount from product;

使用 coalesce 函数进行 null 值处理下，得到的结果就是准确的

coalesce 函数是返回第一个不为空的值
如上sql：如果dis_amount不为空，则返回dis_amount，如果为空，则返回0
```

1. **小于是不包含 null 值**，如 id \< 10；是不包含 id 为 null 值的。
2. **not in 是不包含 null 值的**，如 city not in ('北京','上海')，这个条件得出的结果是 city 中不包含 北京，上海和 null 的城市。

#### 9. and 和 or

在sql语句的过滤条件或运算中，如果有多个条件或多个运算，我们都会考虑优先级，如乘除优先级高于加减，乘除或者加减它们之间优先级平等，谁在前就先算谁。那 and 和 or 呢，看似 and 和 or 优先级平等，谁在前先算谁，但是，**and 的优先级高于 or**。

**注意事项：**

例： 
还是一张商品表（product）

| id   | classify | price |
| :--- | :------- | :---- |
| 1    | 电器     | 70    |
| 2    | 电器     | 130   |
| 3    | 电器     | 80    |
| 4    | 家具     | 150   |
| 5    | 家具     | 60    |
| 6    | 食品     | 120   |

我想要统计下电器或者家具这两类中价格大于100的商品，sql如下：

```sql
select * from product where classify = '电器' or classify = '家具' and price>100
```

得到结果

| id   | classify | price |
| :--- | :------- | :---- |
| 1    | 电器     | 70    |
| 2    | 电器     | 130   |
| 3    | 电器     | 80    |
| 4    | 家具     | 150   |

结果是错误的，把所有的电器类型都查询出来了，原因就是 and 优先级高于 or，上面的sql语句实际执行的是，先找出 classify = '家具' and price>100 的，然后在找出 classify = '电器' 的

正确的 sql 就是加个括号，先计算括号里面的：

```sql
select * from product where (classify = '电器' or classify = '家具') and price>100
```

## 解决hive小文件过多问题

原创 园陌 [五分钟学大数据](javascript:void(0);)

### 小文件产生原因

hive 中的小文件肯定是向 hive 表中导入数据时产生，所以先看下向 hive 中导入数据的几种方式

1. 直接向表中插入数据

```sql
insert into table A values (1,'zhangsan',88),(2,'lisi',61);
```

这种方式每次插入时都会产生一个文件，多次插入少量数据就会出现多个小文件，但是这种方式生产环境很少使用，可以说基本没有使用的

1. 通过load方式加载数据

```sql
load data local inpath '/export/score.csv' overwrite into table A  -- 导入文件

load data local inpath '/export/score' overwrite into table A   -- 导入文件夹
```

使用 load 方式可以导入文件或文件夹，当导入一个文件时，hive表就有一个文件，当导入文件夹时，hive表的文件数量为文件夹下所有文件的数量

1. 通过查询方式加载数据

```sql
insert overwrite table A  select s_id,c_name,s_score from B;
```

这种方式是生产环境中常用的，也是最容易产生小文件的方式

insert 导入数据时会启动 MR 任务，MR中 reduce 有多少个就输出多少个文件

所以， 文件数量=ReduceTask数量*分区数

也有很多简单任务没有reduce，只有map阶段，则

文件数量=MapTask数量*分区数

每执行一次 insert 时hive中至少产生一个文件，因为 insert 导入时至少会有一个MapTask。
像有的业务需要每10分钟就要把数据同步到 hive 中，这样产生的文件就会很多。

### 小文件过多产生的影响

1. 首先对底层存储HDFS来说，HDFS本身就不适合存储大量小文件，小文件过多会导致namenode元数据特别大, 占用太多内存，严重影响HDFS的性能
2. 对 hive 来说，在进行查询时，每个小文件都会当成一个块，启动一个Map任务来完成，而一个Map任务启动和初始化的时间远远大于逻辑处理的时间，就会造成很大的资源浪费。而且，同时可执行的Map数量是受限的。

### 怎么解决小文件过多

#### 1. 使用 hive 自带的 concatenate 命令，自动合并小文件

使用方法：

```sql
#对于非分区表
alter table A concatenate;

#对于分区表
alter table B partition(day=20201224) concatenate;
```

举例：

```sh
#向 A 表中插入数据
hive (default)> insert into table A values (1,'aa',67),(2,'bb',87);
hive (default)> insert into table A values (3,'cc',67),(4,'dd',87);
hive (default)> insert into table A values (5,'ee',67),(6,'ff',87);

#执行以上三条语句，则A表下就会有三个小文件,在hive命令行执行如下语句
#查看A表下文件数量
hive (default)> dfs -ls /user/hive/warehouse/A;
Found 3 items
-rwxr-xr-x   3 root supergroup        378 2020-12-24 14:46 /user/hive/warehouse/A/000000_0
-rwxr-xr-x   3 root supergroup        378 2020-12-24 14:47 /user/hive/warehouse/A/000000_0_copy_1
-rwxr-xr-x   3 root supergroup        378 2020-12-24 14:48 /user/hive/warehouse/A/000000_0_copy_2

#可以看到有三个小文件，然后使用 concatenate 进行合并
hive (default)> alter table A concatenate;

#再次查看A表下文件数量
hive (default)> dfs -ls /user/hive/warehouse/A;
Found 1 items
-rwxr-xr-x   3 root supergroup        778 2020-12-24 14:59 /user/hive/warehouse/A/000000_0

#已合并成一个文件
```

> 注意： 
> 1、concatenate 命令只支持 RCFILE 和 ORC 文件类型。 
> 2、使用concatenate命令合并小文件时不能指定合并后的文件数量，但可以多次执行该命令。 
> 3、当多次使用concatenate后文件数量不在变化，这个跟参数 mapreduce.input.fileinputformat.split.minsize=256mb 的设置有关，可设定每个文件的最小size。

#### 2. 调整参数减少Map数量

- **设置map输入合并小文件的相关参数**：

```
#执行Map前进行小文件合并
#CombineHiveInputFormat底层是 Hadoop的 CombineFileInputFormat 方法
#此方法是在mapper中将多个文件合成一个split作为输入
set hive.input.format=org.apache.hadoop.hive.ql.io.CombineHiveInputFormat; -- 默认

#每个Map最大输入大小(这个值决定了合并后文件的数量)
set mapred.max.split.size=256000000;   -- 256M

#一个节点上split的至少的大小(这个值决定了多个DataNode上的文件是否需要合并)
set mapred.min.split.size.per.node=100000000;  -- 100M

#一个交换机下split的至少的大小(这个值决定了多个交换机上的文件是否需要合并)
set mapred.min.split.size.per.rack=100000000;  -- 100M
```

- **设置map输出和reduce输出进行合并的相关参数**:

```
#设置map端输出进行合并，默认为true
set hive.merge.mapfiles = true;

#设置reduce端输出进行合并，默认为false
set hive.merge.mapredfiles = true;

#设置合并文件的大小
set hive.merge.size.per.task = 256*1000*1000;   -- 256M

#当输出文件的平均大小小于该值时，启动一个独立的MapReduce任务进行文件merge
set hive.merge.smallfiles.avgsize=16000000;   -- 16M 
```

- **启用压缩**

```
# hive的查询结果输出是否进行压缩
set hive.exec.compress.output=true;

# MapReduce Job的结果输出是否使用压缩
set mapreduce.output.fileoutputformat.compress=true;
```

#### 3. 减少Reduce的数量

```
#reduce 的个数决定了输出的文件的个数，所以可以调整reduce的个数控制hive表的文件数量，
#hive中的分区函数 distribute by 正好是控制MR中partition分区的，
#然后通过设置reduce的数量，结合分区函数让数据均衡的进入每个reduce即可。

#设置reduce的数量有两种方式，第一种是直接设置reduce个数
set mapreduce.job.reduces=10;

#第二种是设置每个reduce的大小，Hive会根据数据总大小猜测确定一个reduce个数
set hive.exec.reducers.bytes.per.reducer=5120000000; -- 默认是1G，设置为5G

#执行以下语句，将数据均衡的分配到reduce中
set mapreduce.job.reduces=10;
insert overwrite table A partition(dt)
select * from B
distribute by rand();

解释：如设置reduce数量为10，则使用 rand()， 随机生成一个数 x % 10 ，
这样数据就会随机进入 reduce 中，防止出现有的文件过大或过小
```

#### 4. 使用hadoop的archive将小文件归档

Hadoop Archive简称HAR，是一个高效地将小文件放入HDFS块中的文件存档工具，它能够将多个小文件打包成一个HAR文件，这样在减少namenode内存使用的同时，仍然允许对文件进行透明的访问

```
#用来控制归档是否可用
set hive.archive.enabled=true;
#通知Hive在创建归档时是否可以设置父目录
set hive.archive.har.parentdir.settable=true;
#控制需要归档文件的大小
set har.partfile.size=1099511627776;

#使用以下命令进行归档
ALTER TABLE A ARCHIVE PARTITION(dt='2020-12-24', hr='12');

#对已归档的分区恢复为原文件
ALTER TABLE A UNARCHIVE PARTITION(dt='2020-12-24', hr='12');
```

> 注意:  
> **归档的分区可以查看不能 insert overwrite，必须先 unarchive**

#### 最后

如果是新集群，没有历史遗留问题的话，建议hive使用 orc 文件格式，以及启用 lzo 压缩。
这样小文件过多可以使用hive自带命令 concatenate 快速合并。
#  第1章 Kafka概述

## 1.1 定义

​		kafka是一个分布式的基于发布/订阅模式的消息队列（Message Queue），主要应用于大数据实时处理领域。

## 1.2 消息队列

### 1.2.1 传统消息队列的应用场景

![](image\Kafka\Image 1.png)

**使用消息队列的好处**

* 解耦

  允许你独立的扩展或修改两边的处理过程，只要确保它们遵守同样的接口约束。

* 可恢复性

  系统的一部分组件失效时，不会影响到整个系统。消息队列降低了进程间的耦合度，所以即使一个处理消息的进程挂掉，加入队列中的消息仍然可以在系统恢复后被处理。

* 缓冲

  有助于控制和优化数据流经过系统的速度，解决生产消息和消费消息的处理速度不一致的情况。

* 灵活性 & 峰值处理能力

  在访问量剧增的情况下，应用仍然需要继续发挥左营，但是这样的突发流量并不常见。如果为以能处理这类峰值访问为标准来投入资源随时待命无疑是巨大的浪费，使用消息队列能够使关键组件顶住突发的访问压力，而不会因为突发的超负荷的请求而完全崩溃。

* 异步通信

  很多时候，用户不想也不需要立即处理消息，消息队列提供了异步处理机制，允许用户把一个消息放入队列，但并立即处理它。想向队列中放入多少消息就放多少，然后在需要的时候再去处理它们。

### 1.2.2 消息队列的两种模式

**（1）点对点模式（一对一，消费者主动拉取数据，消息收到后消息清楚）**

​			消息生产者生产消息发送到Queue中，然后消息消费者从Queue中取出并且消费消息。消息被消费后，queue中不再有存储，所以消息消费者不可能消费到已经被消费的消息。Queue支持存在多个消费者，但是对一个消息而言，只会有一个消费者可以消费。

![](image\Kafka\Image 12.png)

**（2）发布/订阅模式（一对多，消费者消费数据之后不会清除消息）**

​			消息生产者（发布）将消息发布到topic中，同时有多个消息消费者（订阅）消费该消息。和点对点方式不同，发布到topic的消息会被所有订阅者消费。

​			该模式又分为两种：

​					1、<u>消费者主动拉去</u>（缺点：需要不断的去消息队列询问）kafka模式

​					2、<u>topic主动推数据</u>（缺点：由于消费者的能力同，可能导致服务器负荷高或资源浪费）

![](image\Kafka\Image 13.png)



## 1.3 Kafka架构

 ![](image\Kafka\Image 14.png)

* **Producer：**消息生产者，就是向kafka broker发消息的客户端；
* **Consumer：**消息消费者，向kafka broker取消息的客户端；
* **Consumer Group (CG)：**消费者组，由多个consumer组成，***消费者组内每个消费者负责消费不同分区的数据，一个分区只能由一个组内消费者消费；消费者组之间互不影响***。所有的消费者都属于某个消费组，即**消费者组是逻辑上的一个订阅者**。
* **Broker：**一台kafka服务器就是一个broker，一个集群由多个broker组成，一个broker可以容纳多个topic。
* **Topic：**可以理解为一个队列，***生产者和消费者面向的都是一个topic***。
* **Patiticon：**为了实现扩展性，一个非常大的topic可以分布到多个broker上，**一个topic可以分为多个partition，**每个partition是一个有序的队列；
* **Replica：**副本，为保证集群中某个节点发生故障时，该节点上的partition数据不丢失，且kafka仍然能继续工作，kafka提供了副本机制，一个topic的每个分区都有若干个副本，一个leader和若干个follower。
* **leader：**每个分区多个副本的“主”，生产者发送数据的的对象，以及消费者消费数据的对象都是leader；
* **follower：**每个分区多个副本中的“从”，实时从leader中同步数据，保持和leader数据的同步。leader发生故障时，某个follower会成为新的follower。

# 第2章 Kafka部署

## 2.1 集群安装部署

### 2.1.1 集群规划

| 192.168.17.121 | 192.168.17.122 | 192.168.17.123 |
| -------------- | -------------- | -------------- |
| zookeeper      | zookeeper      | zookeeper      |
| kafka          | kafka          | kafka          |
|                |                |                |

### 2.1.2 安装包下载

Apache软件国内下载 http://mirrors.hust.edu.cn/apache/

**zookeeper :** http://mirrors.hust.edu.cn/apache/zookeeper/zookeeper-3.4.14/zookeeper-3.4.14.tar.gz

**kafka :** wget http://mirrors.hust.edu.cn/apache/kafka/2.1.1/kafka_2.12-2.1.1.tgz

服务器还需要安装jak

### 2.1.3 安装zookeeper

kafka需要配合zookeeper使用，在安装kafka前，需要先安装zookeeper集群

```shell
cd /opt
tar -zxf zookeeper-3.4.14.tar.gz
mv zookeeper-3.4.14 zookeeper

#修改环境变量
vi /etc/profile

#添加内容
#set for zookeeper
export ZOOKEEPER_HOME=/opt/zookeeper
export PATH=$ZOOKEEPER_HOME/bin:$PATH


#刷新环境变量
source /etc/profile

#修改配置文件
cd /opt/zookeeper/conf

vim zoo.cfg
#添加内容
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
maxClientCnxns=60
tickTime=2000
initLimit=10
syncLimit=5
dataDir=/data/zookeeper/data
dataLogDir=/data/zookeeper/logs

#（客户端连接端口）
clientPort=2181
# (主机名, 心跳端口、数据端口)
server.1=192.168.17.121:2888:3888
# (都是默认端口）
server.2=192.168.17.122:2888:3888
# (2888表示zookeeper程序监听端口，3888表示zookeeper选举通信端口）
server.3=192.168.17.123:2888:3888
<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

#创建文件夹
mkdir -p /data/zookeeper/data
mkdir -p /data/zookeeper/logs

#在data文件夹中新建myid文件，myid文件的内容为1（一句话创建：echo 1 > myid）
touch /data/zookeeper/data/myid
echo 1 > /data/zookeeper/data/myid

touch /data/zookeeper/data/myid
echo 2 > /data/zookeeper/data/myid

touch /data/zookeeper/data/myid
echo 3 > /data/zookeeper/data/myid

#将三台机器的防火墙关闭掉
service iptables stop
service iptables status

#启动，在三个节点上分别都要执行命令zkServer.sh start
/opt/zookeeper/bin/zkServer.sh start

#检验，jps查看进程，会出现进程QuorumPeerMain
[root@mongodb1 conf]# jps
65993 QuorumPeerMain
66057 Jps
[root@mongodb1 conf]# 

[root@mongodb1 conf]# /opt/zookeeper/bin/zkServer.sh status
ZooKeeper JMX enabled by default
Using config: /opt/zookeeper/bin/../conf/zoo.cfg
Mode: follower
[root@mongodb1 conf]# 

[root@mongodb2 conf]# /opt/zookeeper/bin/zkServer.sh status
ZooKeeper JMX enabled by default
Using config: /opt/zookeeper/bin/../conf/zoo.cfg
Mode: leader
[root@mongodb2 conf]# 

[root@mongodb3 conf]# /opt/zookeeper/bin/zkServer.sh status
ZooKeeper JMX enabled by default
Using config: /opt/zookeeper/bin/../conf/zoo.cfg
Mode: follower
[root@mongodb3 conf]# 
```

zookeeper命令行客户端

执行/opt/zookeeper/bin/zkCli.sh，客户端连接上服务器。

```shell
#查找根目录
ls  /

#创建节点并赋值
create /test abc

#获取指定节点的值
get /test

#设置已存在节点的值
set /test cb

#递归删除节点
rmr /test

#删除不存在子节点的节点
delete /test/test01
```

### 2.1.4 安装kafka

```shell
cd /opt/
tar zxf kafka_2.12-2.1.1.tgz
mv kafka_2.12-2.1.1 kafka

#修改配置文件
vim /opt/kafka/config/server.properties
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
###每台服务器的broker.id和host.name都不能相同
broker.id=1
host.name=192.168.17.121
port=9092
#是否可以删除topic
delete.topic.enable=true
#具体一些参数
log.dirs=/opt/kafka/data
log.retention.hours=168 
message.max.byte=5242880
default.replication.factor=3
replica.fetch.max.bytes=5242880
#设置zookeeper集群地址与端口如下：
zookeeper.connect= 192.168.17.121:2181,192.168.17.122:2181,192.168.17.123:2181
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


#添加环境变量
vim /etc/profile

#set for kafka
export KAFKA_HOME=/opt/kafka
export PATH=$KAFKA_HOME/bin:$PATH

#加载
source /etc/profile


#启动kafka（三台）
cd /opt/kafka/bin
kafka-server-start.sh -daemon ../config/server.properties &

#查看是否起来
jps


#创建topic
kafka-topics.sh --create --replication-factor 2 --partitions 1 --topic tttt --zookeeper 192.168.17.121:2181,192.168.17.122:2181,192.168.17.123:2181 

参数解释:

复制两份
　　--replication-factor 2

创建1个分区
　　--partitions 1

topic 名称
　　--topic tttt
　　
　　
#查看已经存在的topic
kafka-topics.sh --list --zookeeper 192.168.17.121:2181,192.168.17.122:2181,192.168.17.123:2181

#删除topic
kafka-topics.sh --delete --zookeeper 192.168.17.121:2181,192.168.17.122:2181,192.168.17.123:2181 --topic tttt


#查询集群描述
kafka-topics.sh --describe --zookeeper 192.168.17.121:2181,192.168.17.122:2181,192.168.17.123:2181

#消费者列表查询
kafka-topics.sh --zookeeper 192.168.17.121:2181,192.168.17.122:2181,192.168.17.123:2181 --list

#新消费者列表查询（支持0.9版本+）
kafka-consumer-groups.sh --new-consumer --bootstrap-server localhost:9092 --list

#显示某个消费组的消费详情（仅支持offset存储在zookeeper上的）
kafka-run-class.sh kafka.tools.ConsumerOffsetChecker --zookeeper localhost:2181 --group test

#显示某个消费组的消费详情（支持0.9版本+）
kafka-consumer-groups.sh --new-consumer --bootstrap-server localhost:9092 --describe --group test-consumer-group


#启一个生产者
kafka-console-producer.sh --topic tttt --broker-list 192.168.17.121:9092

#启一个消费者
kafka-console-consumer.sh --topic tttt --bootstrap-server 192.168.17.121:9092 --from-beginning
```

## 2.2 单机安装部署

### 2.2.1 配置文件

使用Kafka自带的Zookeeper，不需要修改zookeeper.properties

Kafka的配置文件server.properties也可以使用默认的不需要修改

```yaml
log.dirs=/tmp/kafka-logs

zookeeper.connect=localhost:2181
#单机的必须要设置：
offsets.topic.replication.factor=1

log.retention.hours=168
```



### 2.2.2启动和关闭

1.先启动zookeeper：

cd /opt/kafka/bin

nohup ./zookeeper-server-start.sh ../config/zookeeper.properties &
2.启动Kafka：
nohup ./kafka-server-start.sh ../config/server.properties &

3.用jps查看进程

[root@localhost bin]# jps
33250 Jps
32342 QuorumPeerMain
32827 Kafka

关闭进程

./zookeeper-server-stop.sh

./kafka-server-stop.sh

### 2.2.3测试

```shell
cd /opt/kafka/bin

#查看topic列表
./kafka-topics.sh --list --bootstrap-server localhost:9092

#创建一个topic
./kafka-topics.sh --create --replication-factor 1 --partitions 1 --topic test --bootstrap-server localhost:9092

#删除一个topic
./kafka-topics.sh --delete --topic test --bootstrap-server localhost:9092

#启动一个生产者
./kafka-console-producer.sh --topic test --broker-list localhost:9092

#启动一个消费者
./kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test --from-beginning
```



# 第3章 Kafka架构深入

# 第4章 Kafka API

## 生产者读取文件

```java
import io.netty.handler.codec.string.StringEncoder;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Properties;

public class KafkaProducerTest {
    static Producer<String, String> producer = null;
    public static void main(String[] args) {
        Properties props = new Properties();
        //kafka brokerlist
        props.put("bootstrap.servers", "192.168.17.130:9092,192.168.17.130:9093,192.168.17.130:9094,192.168.17.130:9095");
        //ack "0,1,-1 all"四种，1为文件patition leader完成写入就算完成
        props.put("acks", "1");
        props.put("retries", 0);
        props.put("max.request.size", 33554432);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        //必须设置(k,v)的序列化  详情见kafkaProducer 的构造函数
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        producer = new KafkaProducer<>(props);
        try {
            BufferedReader bf = new BufferedReader(
                    new FileReader(
                            new File("E:\\vmware\\data\\docker-compose-file\\kafka-zookeeper\\test.txt")));
            String line = null;
            int count = 0;
            while((line=bf.readLine())!=null){
                count++;
                System.out.println(line.getBytes().length);
                producer.send(new ProducerRecord<>("t1","key_" + count,  line));
                // producer.send(new ProducerRecord<>("t1",  line));
            }
            bf.close();
            producer.close();
            System.out.println("已经发送完毕");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```

# 第5章 Kafka监控

## Kafka监控系统Kafka Eagle剖析

参考文档：https://www.cnblogs.com/smartloli/p/9371904.html

### 1、介绍

Kafka Eagle监控系统是一款用来监控Kafka集群的工具，目前更新的版本是v1.2.3，支持管理多个Kafka集群、管理Kafka主题（包含查看、删除、创建等）、消费者组合消费者实例监控、消息阻塞告警、Kafka集群健康状态查看等。目前Kafka Eagle v1.2.3整个系统所包含的功能，这里笔者给绘制成了一个图，结果如下图所示： 

![1577347401482](image\Kafka\1577347401482.png)

### 2、下载

可以直接访问Kafka Eagle安装包下载地址：http://download.smartloli.org/,然后点击下载按钮，等待下载完成即可。下载界面如下图所示： 

 ![img](image\Kafka\666745-20180726234757083-1491281273.png) 

### 3、安装

```shell 
cd /opt
tar -zxf kafka-eagle-bin-1.3.0.tar.gz
ln -sv kafka-eagle-bin-1.3.0 kafkaeagle
cd kafkaeagle

#配置system-config.properties文件
vi conf/system-config.properties

######################################
# 配置多个Kafka集群所对应的Zookeeper
######################################
kafka.eagle.zk.cluster.alias=cluster1,cluster2
cluster1.zk.list=10.209.178.228:2181,10.209.178.229:2181,10.209.178.230:2181
cluster2.zk.list=10.199.155.171:2181,10.199.155.183:2181,10.199.155.210:2181

######################################
# 设置Zookeeper线程数
######################################
kafka.zk.limit.size=25

######################################
# 设置Kafka Eagle浏览器访问端口
######################################
kafka.eagle.webui.port=8089

######################################
# 如果你的offsets存储在Kafka中，这里就配置
# 属性值为kafka，如果是在Zookeeper中，可以
# 注释该属性。一般情况下，Offsets的也和你消
# 费者API有关系，如果你使用的Kafka版本为0.10.x
# 以后的版本，但是，你的消费API使用的是0.8.2.x
# 时的API，此时消费者依然是在Zookeeper中
######################################
cluster1.kafka.eagle.offset.storage=kafka
cluster2.kafka.eagle.offset.storage=kafka

######################################
# 是否启动监控图表，默认是不启动的
######################################
kafka.eagle.metrics.charts=true

######################################
# 在使用Kafka SQL查询主题时，如果遇到错误，
# 可以尝试开启这个属性，默认情况下，不开启
######################################
kafka.eagle.sql.fix.error=true

######################################
# kafka sql topic records max
######################################
kafka.eagle.sql.topic.records.max=5000

######################################
# 邮件服务器设置，用来告警
######################################
kafka.eagle.mail.enable=false
kafka.eagle.mail.sa=alert_sa
kafka.eagle.mail.username=alert_sa@163.com
kafka.eagle.mail.password=mqslimczkdqabbbh
kafka.eagle.mail.server.host=smtp.163.com
kafka.eagle.mail.server.port=25

######################################
# alarm im configure
######################################
#kafka.eagle.im.dingding.enable=true
#kafka.eagle.im.dingding.url=https://oapi.dingtalk.com/robot/send?access_token=

#kafka.eagle.im.wechat.enable=true
#kafka.eagle.im.wechat.url=https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=
#kafka.eagle.im.wechat.touser=
#kafka.eagle.im.wechat.toparty=
#kafka.eagle.im.wechat.totag=
#kafka.eagle.im.wechat.agentid=

######################################
# delete kafka topic token
######################################
kafka.eagle.topic.token=keadmin

######################################
# kafka sasl authenticate
######################################
kafka.eagle.sasl.enable=false
kafka.eagle.sasl.protocol=SASL_PLAINTEXT
kafka.eagle.sasl.mechanism=PLAIN

######################################
# kafka jdbc driver address
######################################
kafka.eagle.driver=org.sqlite.JDBC
kafka.eagle.url=jdbc:sqlite:/opt/kafka-eagle-web-1.3.0/db/ke.db
kafka.eagle.username=root
kafka.eagle.password=smartloli
```

### 4、启动

 配置完成后，可以执行Kafka Eagle脚本ke.sh。如果首次执行，需要给该脚本赋予执行权限，命令如下： 

```shell
cd bin
chmod +x bin/ke.sh
```

在ke.sh脚本中，支持以下命令：

| 命令                   | 说明                                 |
| ---------------------- | ------------------------------------ |
| ke.sh start            | 启动Kafka Eagle系统                  |
| ke.sh stop             | 停止Kafka Eagle系统                  |
| ke.sh restart          | 重启Kafka Eagle系统                  |
| ke.sh status           | 查看Kafka Eagle系统运行状态          |
| ke.sh stats            | 统计Kafka Eagle系统占用Linux资源情况 |
| ke.sh find [ClassName] | 查看Kafka Eagle系统中的类是否存在    |

## 预览

### 1.Consumer模块展示

 启动一个消费者程序，然后进入到Consumer模块，截图如下： 

![img](image\Kafka\666745-20180727003501567-792685122.png)

![img](image\Kafka\666745-20180727003557494-955457367.png)

这里需要注意的时，Kafka在0.10.x之后的版本和之前的版本底层设计有了变化，在之前的版本消费者信息是存储在Zookeeper中的，在0.10.x版本之后，默认存储到了Kafka内部主题中，只保留了元数据信息存储在Zookeeper中，例如：Kafka Broker地址、Topic名称、分区等信息。

是不是我使用的是Kafka 0.10.x之后的版本（如0.10.0、1.0.x、1.x等），然后配置属性kafka.eagle.offset.storage=kafka，启动消费者，就可以看到消费者信息呢？不一定的，还有一个关键因素决定Kafka Eagle系统是否可以展示你消费者程序信息，那就是消费者API的使用。

如果你使用的Kafka 0.10.x之后的版本，然后消费者API也是使用的最新的写法，那么自然你的消费者信息会被记录到Kafka内部主题中，那么此时你设置kafka.eagle.offset.storage=kafka这个属性，Kafka Eagle系统可以完美展示你的消费者使用情况。

但是，如果你虽然使用的是Kafka 0.10.x之后的版本，但是你使用的消费者API还是0.8.2.x或是0.9.x时的写法，此时的消费者信息是会被记录到Zookeeper中进行存储，那么此时你需要设置kafka.eagle.offset.storage=zookeeper或者注释掉该属性，在访问Kafka Eagle系统就可以查看到你的消费者详情了。

### 2. 监控趋势图

 Kafka系统默认是没有开启JMX端口的，所以Kafka Eagle的监控趋势图默认采用不启用的方式，即kafka.eagle.metrics.charts=false。如果需要查看监控趋势图，需要开启Kafka系统的JMX端口，设置该端口在$KAFKA_HOME/bin/kafka-server-start.sh脚本中，设置内容如下：

```shell
vi kafka-server-start.sh

if [ "x$KAFKA_HEAP_OPTS" = "x" ]; then
    export KAFKA_HEAP_OPTS="-server -Xms2G -Xmx2G -XX:PermSize=128m -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -XX:ParallelGCThreads=8 -XX:ConcGCThreads=5 -XX:InitiatingHeapOccupancyPercent=70"
    export JMX_PORT="9999"
    #export KAFKA_HEAP_OPTS="-Xmx1G -Xms1G"
fi
```

需要注意的是，这里的端口不一定非要设置成9999，端口只有可用，均可。Kafka Eagle系统会自动获取这个JMX端口，采集结果如下：

![img](image\Kafka\666745-20180727004948478-1062696178.png)

### 3 Kafka SQL查询Topic

还有一部分同学在Kafka Eagle系统的SQL查询Topic时，会出现查询不到数据的情况。这里查询不到数据可能情况有多种，首先需要排除Kafka集群因素，确保Kafka集群运行正常，Topic能够正常访问，并且Topic中是有数据的。

在排除一些主观因素后，回到Kafka Eagle系统应用层面，如果出现这种问题，可以尝试开启属性kafka.eagle.sql.fix.error=true，这个属性默认是不开启的。正常情况下使用Kafka SQL查询Topic，返回结果如下图所示：

![img](image\Kafka\666745-20180727005710817-1885036162.png)

 

 ![img](image\Kafka\666745-20180727005735726-1157971488.png)

## 总结

 另外，如果在使用Kafka Eagle系统中遇到其他问题，可以查看$KE_HOME/logs/ke_console.out日志来分析具体的异常信息，一般都会提示具体的错误，大家可以根据错误提示来进行解决。

# Kafka报错

## 超大消息发送失败

两个参数会影响

1、生产者(producer)的参数max.request.size，默认1M，大于1M的生产者会报错，消费者没有接收也没有报错

![image-20200812094038549](image\Kafka\image-20200812094038549.png)

可以在代码里发送消息时设置KafkaProducer参数的参数props.put("max.request.size", 33554432);（30MB）

![image-20200812095810382](image\Kafka\image-20200812095810382.png)

2、经营者(broker)的参数max.message.bytes，默认1M，大于1M的生产者会报错，消费者没有接收也没有报错

<img src="image\Kafka\image-20200812095227865.png" alt="image-20200812095227865"  />

或者这个错误，这个把大小门限改1M后再测试的错

![image-20200812095240515](image\Kafka\image-20200812095240515.png)

可以在启动配置文件server.properties里添加后重启，或者用下面命令修改指定topic

kafka-configs.sh --zookeeper 192.168.17.130:2181 --entity-type topics --entity-name t1 --alter --add-config max.message.bytes=10485760


# 介绍

官方文档：https://www.elastic.co/guide/en/beats/filebeat/current/index.html

Filebeat是Beat平台的工具，以libbeat框架为基础，轻量级日志采集工具，经常与ELK搭配使用，作为数据采集源头使用。

是Beats家族的一员，Beats是用于单用途数据托运人的平台。它们以轻量级代理的形式安装，并将来自成百上千台机器的数据发送到Logstash或Elasticsearch。

![image-20201229110033645](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86/image/Filebeat/image-20201229110033645.png)

Beats可以直接（或者通过Logstash）将数据发送到Elasticsearch，在那里你可以进一步处理和增强数据，然后在Kibana中将其可视化。

![image-20201230102243327](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86/image/Filebeat/image-20201230102243327.png)



**为什么不直接使用Logstash**，因为filebeat比较轻量级，对CPU负载不大，不占用太多资源

Filebeat是一个日志文件托运工具。可作为一个客户端安装在你的服务器上，Filebeat可以监控日志的目录或者指定的日志文件，实时读取文件，并将其上传到Elasticsearch或Logstash进行索引等处理。

**Filebeat工作流程是这样的**：开启Filebeat时，它会启动一个或多个探测器（prospectors）去检测你设置的日志路径或日志文件，在定位到每一个日志文件以后，Filebeat启动一个收割进程（harvester）。每一个收割进程读取一个日志文件的新内容并把数据发送到libbeat ，libbeat会集合这些事件并将汇总的数据发送到你设置的外部接收程序中。

![image-20201230101234867](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86/image/Filebeat/image-20201230101234867.png)

# 安装

## 下载

官方下载地址https://www.elastic.co/cn/downloads/beats/filebeat

- tar

  低版本：wget https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-5.5.2-linux-x86_64.tar.gz

  新版本：wget https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.10.1-linux-x86_64.tar.gz

  tar -xzvf filebeat-7.10.1-linux-x86_64.tar.gz

- rpm: 

  curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-5.1.1-x86_64.rpm

  sudo rpm -vi filebeat-5.1.1-x86_64.rpm

## 配置

编辑配置文件来配置filebeat，对于rpm或者deb来说，配置文件是/etc/filebeat/filebeat.yml这个文件，对于MAC或者win来说，请查看你的解压文件中。

```yaml
filebeat:

##############老版本###########
  # List of prospectors to fetch data.
  prospectors:
  	  #指定文件的输入类型log(默认)或者stdin。
    - input_type: log
 ##############老版本###########
  
 ##############新版本###########
  inputs:
    - type: log
 ##############新版本###########
 
      # paths指定要监控的日志文件路径,支持Go Glab的所有模式
      paths:
        - /var/log/*.log

      #指定被监控的文件的编码类型使用plain和utf-8都是可以处理中文日志的。
      # Some sample encodings:
      #   plain, utf-8, utf-16be-bom, utf-16be, utf-16le, big5, gb18030, gbk,
      #    hz-gb-2312, euc-kr, euc-jp, iso-2022-jp, shift-jis, ...
      #encoding: plain
      
      # 在输入中排除符合正则表达式列表的那些行
      # exclude_lines: ["^DBG"]

      # 包含输入中符合正则表达式列表的那些行默认包含所有行include_lines执行完毕之后会执行exclude_lines。
      # include_lines: ["^ERR", "^WARN"]
      
      # 忽略掉符合正则表达式列表的文件默认为每一个符合paths定义的文件都创建一个harvester。
      # exclude_files: [".gz$"]

      # 向输出的每一条日志添加额外的信息比如“level:debug”方便后续对日志进行分组统计。默认情况下会在输出信息的fields子目录下以指定的新增fields建立子目录例如fields.level。
      #fields:
      #  level: debug
      #  review: 1

      # 如果该选项设置为true则新增fields成为顶级目录而不是将其放在fields目录下。自定义的field会覆盖filebeat默认的field。
      #fields_under_root: false

      # 可以指定Filebeat忽略指定时间段以外修改的日志内容比如2h两个小时或者5m(5分钟)。
      #ignore_older: 0

      # 如果一个文件在某个时间段内没有发生过更新则关闭监控的文件handle。默认1h,change只会在下一次scan才会被发现
      #close_older: 1h

      # 设定Elasticsearch输出时的document的type字段也可以用来给日志进行分类。Default: log
      #document_type: log

      # Filebeat以多快的频率去prospector指定的目录下面检测文件更新比如是否有新增文件如果设置为0s则Filebeat会尽可能快地感知更新占用的CPU会变高。默认是10s。
      #scan_frequency: 10s

      # 每个harvester监控文件时使用的buffer的大小。
      #harvester_buffer_size: 16384

      # 日志文件中增加一行算一个日志事件max_bytes限制在一次日志事件中最多上传的字节数多出的字节会被丢弃。The default is 10MB.
      #max_bytes: 10485760

      # 适用于日志中每一条日志占据多行的情况比如各种语言的报错信息调用栈。这个配置的下面包含如下配置
      #multiline:

        # The regexp Pattern that has to be matched. The example pattern matches all lines starting with [
        #pattern: ^\[

        # Defines if the pattern set under pattern should be negated or not. Default is false.
        #negate: false

        # Match can be set to "after" or "before". It is used to define if lines should be append to a pattern
        # that was (not) matched before or after or as long as a pattern is not matched based on negate.
        # Note: After is the equivalent to previous and before is the equivalent to to next in Logstash
        #match: after

        # The maximum number of lines that are combined to one event.
        # In case there are more the max_lines the additional lines are discarded.
        # Default is 500
        #max_lines: 500

        # After the defined timeout, an multiline event is sent even if no new pattern was found to start a new event
        # Default is 5s.
        #timeout: 5s

      # 如果设置为trueFilebeat从文件尾开始监控文件新增内容把新增的每一行文件作为一个事件依次发送而不是从文件开始处重新发送所有内容。
      #tail_files: false

      # Filebeat检测到某个文件到了EOF之后每次等待多久再去检测文件是否有更新默认为1s。
      #backoff: 1s

      # Filebeat检测到某个文件到了EOF之后等待检测文件更新的最大时间默认是10秒。
      #max_backoff: 10s

      # 定义到达max_backoff的速度默认因子是2到达max_backoff后变成每次等待max_backoff那么长的时间才backoff一次直到文件有更新才会重置为backoff。
      #backoff_factor: 2

      # 这个选项关闭一个文件,当文件名称的变化。#该配置选项建议只在windows。
      #force_close_files: false

    # Additional prospector
    #-
      # Configuration to use stdin input
      #input_type: stdin

  # spooler的大小spooler中的事件数量超过这个阈值的时候会清空发送出去不论是否到达超时时间。
  #spool_size: 2048

  # 是否采用异步发送模式(实验!)
  #publish_async: false

  # spooler的超时时间如果到了超时时间spooler也会清空发送出去不论是否到达容量的阈值。
  #idle_timeout: 5s

  # 记录filebeat处理日志文件的位置的文件
  registry_file: /var/lib/filebeat/registry

  # 如果要在本配置文件中引入其他位置的配置文件可以写在这里需要写完整路径但是只处理prospector的部分。
  #config_dir:
  
  
  ############################# Output ##########################################

# 输出到数据配置.单个实例数据可以输出到elasticsearch或者logstash选择其中一种注释掉另外一组输出配置。
output:

可参考下面案例
```

## 启动

```sh
#####启动
nohup ./filebeat -c filebeat.yml > logs/nohup.out 2>&1 &

##查看进程
ps -ef | grep filebeat | grep -v grep | awk '{print $1" | "$2" | "$10}'
```



# input

## 下面是官方提供的连接

You can configure Filebeat to use the following inputs:

- [Azure Event Hub](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-azure-eventhub.html)
- [Cloud Foundry](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-cloudfoundry.html)
- [Container](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-container.html)
- [Docker](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-docker.html)
- [Google Pub/Sub](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-google-pubsub.html)
- [HTTP Endpoint](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-http_endpoint.html)
- [HTTP JSON](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-httpjson.html)
- [Kafka](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-kafka.html)
- [Log](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-log.html)
- [MQTT](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-mqtt.html)
- [NetFlow](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-netflow.html)
- [Office 365 Management Activity API](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-o365audit.html)
- [Redis](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-redis.html)
- [S3](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-s3.html)
- [Stdin](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-stdin.html)
- [Syslog](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-syslog.html)
- [TCP](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-tcp.html)
- [UDP](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-udp.html)

## Log

用的最多的一个，使用日志输入从日志文件中读取行。

```yaml
filebeat.inputs:
- type: log
  paths:
    - /var/log/messages
    - /var/log/*.log
    
#为了对不同的文件应用不同的配置设置，你需要定义多个输入部分:
- type: log 
  paths:
    - "/var/log/apache2/*"
  fields:
    apache: true
  fields_under_root: true

  #下面的示例将 Filebeat 配置为删除任何以 DBG 开头的行。
  exclude_lines: ['^DBG']
  
  #下面的示例将 Filebeat 配置为导出任何以 ERR 或 WARN 开头的行:
  include_lines: ['^ERR', '^WARN']
  
  #忽略所有扩展名为 gz 的文件
  exclude_files: ['\.gz$']
  
  #添加多行设置，可参考下面Multiline messages文档说明
  multiline.pattern: '^\[\d{4}-\d{1,2}-\d{1,2}'
  multiline.negate: true
  multiline.match: after
  multiline.timeout: 5s
  
  #标签可以很容易地在 Kibana 选择特定的事件或者在 Logstash 应用条件过滤
  tags: 'web-catalina-218'
```

**paths**

将被抓取的基于 glob 的路径列表。这里也支持 Go Glob 支持的所有模式。例如，要从预定义的子目录级别获取所有文件，可以使用以下模式:/var/log/*/\*.log。这样就可以取出所有的东西。从 / var / log 的子文件夹中获取日志文件。它不从 / var / log 文件夹本身获取日志文件。可以使用可选的 recursive _ glob 设置递归地获取目录的所有子目录中的所有文件。

**recursive_glob.enabled**

允许将 * * 扩展到递归的 glob 模式。启用此特性后，每个路径中最右边的 * * 将扩展为固定数量的球状模式。例如:/foo/* * 扩展为/foo,/foo/* ,/foo/*/* ，等等。如果启用，它将单个 * * 扩展为8级深度 * 模式。

此特性默认启用。将 recursive _ glob. 设置为 false 以禁用它。

**encoding**

用于读取包含国际字符的数据的文件编码。请参阅 W3C 推荐的用于 HTML5的编码名称。

有效编码:

- `plain`: plain ASCII encoding : 纯 ASCII 编码

- `utf-8` or 或`utf8`: UTF-8 encoding : UTF-8编码

- `gbk`: simplified Chinese charaters 简体中文

  等

**close_renamed**

启用此选项后，当重命名文件时，Filebeat 将关闭文件处理程序。例如，在旋转文件时就会发生这种情况。默认情况下，由于文件处理程序不依赖于文件名，因此收割机保持打开状态并继续读取文件。如果启用了 `close _ renamed` 选项，并且该文件被重命名或移动的方式使其不再与为。Filebeat 无法读完文件。

配置基于路径的`file_identity`时，不要使用此选项。启用该选项没有意义，因为 Filebeat 无法使用路径名作为唯一标识符检测重命名。

**close_removed**

当启用此选项时，Filebeat 将在删除文件时关闭收割器。通常，文件只有在关闭 _ inactive 指定的持续时间内处于非活动状态之后才应该被删除。但是，如果一个文件提前被删除，而且你不能启用 close _ removed，Filebeat 会保持文件打开状态，以确保收割机已经完成。如果此设置导致文件由于过早从磁盘删除而未完全读取，请禁用此选项。

## Multiline messages

Filebeat 获取的文件可能包含跨多行文本的消息。例如，多行消息在包含 Java 堆栈跟踪的文件中很常见。为了正确处理这些多行事件，需要在 filebeat.yml 文件中配置多行设置，以指定哪些行是单个事件的一部分。

下面的示例演示如何配置 Filebeat 来处理多行消息，其中消息的第一行以括号([)开头。

```yaml
#可以在 Filebeat.yml 配置文件的 Filebeat.input 部分指定以下选项，以控制 Filebeat 处理跨多行消息的方式。

# 另外一个参数是count,按照固定行数汇聚
multiline.type: pattern
multiline.pattern: '^\['
multiline.negate: true
multiline.match: after
```

Filebeat 获取所有不以[开头的行，并将它们与以前以[开头的行组合。例如，你可以使用这个配置将多行消息的下列行连接到一个事件中:

![image-20201230121250791](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86/image/Filebeat/image-20201230121250791.png)



### Java日志跟踪

**案例1：**要将这些线路合并为 Filebeat 的一个事件，请使用以下多线路配置:

![image-20201230121308754](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86/image/Filebeat/image-20201230121308754.png)

```yaml
multiline.type: pattern
multiline.pattern: '^[[:space:]]'
multiline.negate: false
multiline.match: after
```

此配置将以空格开始的任何行合并到前一行。

**案例2：**以空格开头，后跟单词的一行`at` or 或`...`

![image-20201230121505369](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86/image/Filebeat/image-20201230121505369.png)

```yaml
multiline.type: pattern
multiline.pattern: '^[[:space:]]+(at|\.{3})[[:space:]]+\b|^Caused by:'
multiline.negate: false
multiline.match: after
```

### 时间戳

![image-20201230121629519](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86/image/Filebeat/image-20201230121629519.png)

```yaml
multiline.type: pattern
multiline.pattern: '^\[[0-9]{4}-[0-9]{2}-[0-9]{2}'
multiline.negate: true
multiline.match: after
```

### 应用程序事件

![image-20201230121741467](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86/image/Filebeat/image-20201230121741467.png)

```yaml
multiline.type: pattern
multiline.pattern: 'Start new event'
multiline.negate: true
multiline.match: after
multiline.flush_pattern: 'End event'
```

## Docker

从 Docker 容器读取日志

```yaml
filebeat.inputs:
- type: docker
  containers.ids: 
    - '8b6fe7dc9e067b58476dc57d6986dd96d7100430c5de3b109a99cd56ac655347'
```

从中读取日志的路径列表。对于使用与 docker 相同日志记录格式但将日志放置在不同路径的其他运行时，这可以作为 containers.ids 的替代方案。举例来说，在 Kubernetes 使用中区填海区填海计划运行时，可使用以下配置:

```yaml
filebeat.inputs:
- type: docker
  containers.paths:
    - /var/log/pods/${data.kubernetes.pod.uid}/${data.kubernetes.container.name}/*.log
```

## Filestream

使用文件列表输入从活动日志文件中读取行。它是对日志输入的一种新的、改进的替代方法。但是，它缺少一些特性，例如多行或其他特殊的解析功能。这些缺失的选项可能会被再次添加。如果可能的话，我们努力实现功能平等。

```yaml
filebeat.inputs:
- type: filestream
  paths:
    - /var/log/messages
    - /var/log/*.log
    
  #下面的示例将 Filebeat 配置为删除任何以 DBG 开头的行。
  exclude_lines: ['^DBG']
  
  #下面的示例将 Filebeat 配置为导出任何以 ERR 或 WARN 开头的行:
  include_lines: ['^ERR', '^WARN']
```

可以对从这些文件获取的行应用其他配置设置(例如字段、 include _ lines、 exclude _ lines 等)。指定的选项将应用于此输入获取的所有文件。

为了对不同的文件应用不同的配置设置，你需要定义多个输入部分:

```yaml
filebeat.inputs:
#从两个文件获取行: system.log 和 wifi.log。
- type: filestream 
  paths:
    - /var/log/system.log
    - /var/log/wifi.log
#从目录中的每个文件获取行，并使用 fields configuration 选项向输出中添加一个名为 apache 的字段
- type: filestream 
  paths:
    - "/var/log/apache2/*"
  fields:
    apache: true
```

## HTTP JSON

https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-httpjson.html

```yaml
filebeat.inputs:
# Fetch your public IP every minute.
- type: httpjson
  url: https://api.ipify.org/?format=json
  interval: 1m
  processors:
    - decode_json_fields
        fields: [message]
        target: json
```

## Kafka

```yaml
filebeat.inputs:
- type: kafka
  hosts:
    - kafka-broker-1:9092
    - kafka-broker-2:9092
  topics: ["my-topic"]
  group_id: "filebeat"
  #要使用的 Kafka 协议版本(默认为“1.0.0”)
  version: 1.0.0
  #开始读取的初始偏移量，“oldest”或“newest”。默认为“最老”。
  initial_offset: "newest"
  #可以指定这些字段向输出添加附加信息
  fields:
    app_id: query_engine_12
```



# output

## Console

一般测试用。

控制台输出以JSON格式将事件写入stdout。

控制台输出应该只用于调试问题，因为它可以产生大量的日志记录数据。

```yaml
output.console:
  pretty: true
```

## Elasticsearch

```yaml
output:

  ### 输出数据到Elasticsearch
  elasticsearch:
    # IPv6 addresses should always be defined as: https://[2001:db8::1]:9200
    hosts: ["localhost:9200"]

    # 输出认证.
    #protocol: "https"
    #username: "admin"
    #password: "s3cr3t"

    # 启动进程数.
    #worker: 1

    # 输出数据到指定index default is "filebeat"  可以使用变量[filebeat-]YYYY.MM.DD keys.
    #index: "filebeat"

    # 一个模板用于设置在Elasticsearch映射默认模板加载是禁用的,没有加载模板这些设置可以调整或者覆盖现有的加载自己的模板
    #template:

      # Template name. default is filebeat.
      #name: "filebeat"

      # Path to template file
      #path: "filebeat.template.json"

      # Overwrite existing template
      #overwrite: false

    # Optional HTTP Path
    #path: "/elasticsearch"

    # Proxy server url
    #proxy_url: http://proxy:3128

    # 发送重试的次数取决于max_retries的设置默认为3
    #max_retries: 3

    # 单个elasticsearch批量API索引请求的最大事件数。默认是50。
    #bulk_max_size: 50

    # elasticsearch请求超时事件。默认90秒.
    #timeout: 90

    # 新事件两个批量API索引请求之间需要等待的秒数。如果bulk_max_size在该值之前到达额外的批量索引请求生效。
    #flush_interval: 1

    # elasticsearch是否保持拓扑。默认false。该值只支持Packetbeat。
    #save_topology: false

    # elasticsearch保存拓扑信息的有效时间。默认15秒。
    #topology_expire: 15

    # 配置TLS参数选项如证书颁发机构等用于基于https的连接。如果tls丢失主机的CAs用于https连接elasticsearch。
    #tls:
      # List of root certificates for HTTPS server verifications
      #certificate_authorities: ["/etc/pki/root/ca.pem"]

      # Certificate for TLS client authentication
      #certificate: "/etc/pki/client/cert.pem"

      # Client Certificate Key
      #certificate_key: "/etc/pki/client/cert.key"

      # Controls whether the client verifies server certificates and host name.
      # If insecure is set to true, all server host names and certificates will be
      # accepted. In this mode TLS based connections are susceptible to
      # man-in-the-middle attacks. Use only for testing.
      #insecure: true

      # Configure cipher suites to be used for TLS connections
      #cipher_suites: []

      # Configure curve types for ECDHE based cipher suites
      #curve_types: []

      # Configure minimum TLS version allowed for connection to logstash
      #min_version: 1.0

      # Configure maximum TLS version allowed for connection to logstash
      #max_version: 1.2

```

## Elasticsearch集群

```yaml
##下面是输出到elasticsearch
output.elasticsearch:
  hosts: ["10.199.155.171:9200","10.199.155.183:9200","10.199.155.210:9200"]
  worker: 4
  index: "filebeat_nginx_turn.%{+yyyy.MM.dd.HH}"
setup.template.name: "filebeat_nginx"
setup.template.overwrite: false
setup.template.pattern: "filebeat_nginx*"
```

## Kafka集群

```yaml
##下面是输出到kafka
output.kafka:
  hosts: ["10.199.155.171:9092", "10.199.155.183:9092", "10.199.155.210:9092"]
  topic: 'mongodb_fast'
  #连接的存活时间.如果为0,表示短连,发送完就关闭.默认为0秒.
  keep_alive: 10s
```

```yaml
output.kafka:
  # initial brokers for reading cluster metadata
  hosts: ["kafka1:9092", "kafka2:9092", "kafka3:9092"]

  # message topic selection + partitioning
  topic: '%{[fields.log_topic]}'
  partition.round_robin:
    reachable_only: false

  #主题选择器规则的数组。每个规则都指定了用于匹配该规则的事件的主题。在发布期间，Filebeat根据数组中的第一个匹配规则为每个事件设置主题。规则可以包含条件、基于格式字符串的字段和名称映射。如果主题设置缺失或没有规则匹配，则使用topic字段。
  topics:
  - topic: "critical-%{[agent.version]}"
    when.contains:
      message: "CRITICAL"
  - topic: "error-%{[agent.version]}"
    when.contains:
      message: "ERR"
      
  required_acks: 1
  compression: gzip
  max_message_bytes: 1000000
```

## Logstash

```yaml
##下面是输出到logstash
output:
  logstash:
    enabled: true
    hosts: ["10.199.155.171:5044"]
    index: filebeat



##############################logstash参数详解#################

#logstash:
    # Logstash 主机地址
    #hosts: ["localhost:5044"]

    # 配置每个主机发布事件的worker数量。在负载均衡模式下最好启用。
    #worker: 1

    # #发送数据压缩级别
    #compression_level: 3

    # 如果设置为TRUE和配置了多台logstash主机输出插件将负载均衡的发布事件到所有logstash主机。
    #如果设置为false输出插件发送所有事件到随机的一台主机上如果选择的不可达将切换到另一台主机。默认是false。
    #loadbalance: true

    # 输出数据到指定index default is "filebeat"  可以使用变量[filebeat-]YYYY.MM.DD keys.
    #index: filebeat
    #例如filebeat-7.10.1.：
    #index => "%{[@metadata][beat]}-%{[@metadata][version]}" 
    
    # Optional TLS. By default is off.
    #配置TLS参数选项如证书颁发机构等用于基于https的连接。如果tls丢失主机的CAs用于https连接elasticsearch。
    #tls:
      # List of root certificates for HTTPS server verifications
      #certificate_authorities: ["/etc/pki/root/ca.pem"]

      # Certificate for TLS client authentication
      #certificate: "/etc/pki/client/cert.pem"

      # Client Certificate Key
      #certificate_key: "/etc/pki/client/cert.key"

      # Controls whether the client verifies server certificates and host name.
      # If insecure is set to true, all server host names and certificates will be
      # accepted. In this mode TLS based connections are susceptible to
      # man-in-the-middle attacks. Use only for testing.
      #insecure: true

      # Configure cipher suites to be used for TLS connections
      #cipher_suites: []

      # Configure curve types for ECDHE based cipher suites
      #curve_types: []

```

## Logstash集群

```yaml
output.logstash:
  hosts: ["localhost:5044", "localhost:5045"]
  loadbalance: true
  index: filebeat
```

## Redis

```yaml
output.redis:
  hosts: ["localhost"]
  password: "my_password"
  #key: "%{[fields.list]:fallback}"
  key: "filebeat"
  db: 0
  timeout: 5
  keys:
    - key: "info_list"   # send to info_list if `message` field contains INFO
      when.contains:
        message: "INFO"
    - key: "debug_list"  # send to debug_list if `message` field contains DEBUG
      when.contains:
        message: "DEBUG"
    - key: "%{[fields.list]}"
      mappings:
        http: "frontend_list"
        nginx: "frontend_list"
        mysql: "backend_list"
```

## File

```yaml
output.file:
  path: "/tmp/filebeat"
  filename: filebeat
  
  #每个文件的最大大小(单位为千字节)。当达到这个大小时，文件将被旋转。缺省值是10240kb
  #rotate_every_kb: 10000
  
  #路径下要保存的最大文件数。当达到这个数量的文件时，最老的文件将被删除，其余的文件将从最后一个转移到第一个。文件个数必须在2 ~ 1024之间。默认值为7。
  #number_of_files: 7
  
  #用于创建文件的权限。默认值为0600。
  #permissions: 0600
```

# 实例

## 读取日志文件到Kafka

读取多个不同路径日志，并按照不同topic送到Kafka

```yaml
#新建
vim filebeat-test.yaml

#复制内容
filebeat.inputs:
  - type: log
      paths:
        - /root/testData.log
      fields:
        log_topics: t1
    - type: log
      paths:
        - /root/testData1.log
      fields:
        log_topics: t2
output.kafka:
  hosts: ["192.168.17.130:9092"]
  topic: '%{[fields][log_topics]}'
  
  
#启动
nohup ./filebeat -c filebeat-test.yml  &
```


# 概述

参考：https://www.cnblogs.com/cjsblog/p/9459781.html

Logstash是一个开源数据收集引擎，具有实时管道功能。Logstash可以动态地将来自不同数据源的数据统一起来，并将数据标准化到你所选择的目的地。

![image-20210107111226192](image/Logstash/image-20210107111226192.png)



## 工作流程

### 输入

**采集各种样式、大小和来源的数据**

数据往往以各种各样的形式，或分散或集中地存在于很多系统中。Logstash 支持各种输入选择 ，可以在同一时间从众多常用来源捕捉事件。能够以连续的流式传输方式，轻松地从您的日志、指标、Web 应用、数据存储以及各种 AWS 服务采集数据。

![image-20210107112057741](image/Logstash/image-20210107112057741.png)



### 过滤器

**实时解析和转换数据**

数据从源传输到存储库的过程中，Logstash 过滤器能够解析各个事件，识别已命名的字段以构建结构，并将它们转换成通用格式，以便更轻松、更快速地分析和实现商业价值。

Logstash 能够动态地转换和解析数据，不受格式或复杂度的影响：

- 利用 Grok 从非结构化数据中派生出结构
- 从 IP 地址破译出地理坐标
- 将 PII 数据匿名化，完全排除敏感字段
- 整体处理不受数据源、格式或架构的影响

![image-20210107112330228](image/Logstash/image-20210107112330228.png)

### 输出

**选择你的存储，导出你的数据**

尽管 Elasticsearch 是我们的首选输出方向，能够为我们的搜索和分析带来无限可能，但它并非唯一选择。

Logstash 提供众多输出选择，您可以将数据发送到您要指定的地方，并且能够灵活地解锁众多下游用例。

![image-20210107112427503](image/Logstash/image-20210107112427503.png)



## Logstash与Filebeat的对比

因为logstash是jvm跑的，资源消耗比较大，所以后来作者又用golang写了一个功能较少但是资源消耗也小的轻量级的logstash-forwarder。不过作者只是一个人，加入http://elastic.co公司以后，因为es公司本身还收购了另一个开源项目packetbeat，而这个项目专门就是用golang的，有整个团队，所以es公司干脆把logstash-forwarder的开发工作也合并到同一个golang团队来搞，于是新的项目就叫filebeat了。

logstash 和filebeat都具有日志收集功能，filebeat更轻量，占用资源更少，但logstash 具有filter功能，能过滤分析日志。一般结构都是filebeat采集日志，然后发送到消息队列，redis，kafaka。然后logstash去获取，利用filter功能过滤分析，然后存储到elasticsearch中

|                  | Filebeat | Logstash |
| ---------------- | -------- | -------- |
| 采集不同数据源   | 支持     | 支持     |
| 对数据进行转换   | `不支持` | 支持     |
| 输出到不同目的地 | 支持     | 支持     |
| 消耗资源         | 小       | `大`     |

### 测试对比

参考：https://blog.csdn.net/m0_38120325/article/details/79072921

| 测试环境             | CPU                                                          | 内存                                                         | 系统版本                                                     | 硬盘大小 | 网卡     |
| -------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | -------- | -------- |
| 192.168.145.101      | 4个 每个4核                                                  | 24G                                                          | Linux version 2.6.32-642.el6.x86_64--Red Hat Enterprise Linux Server release 6.8 | 256G     | 万兆网卡 |

1. 5分钟的持续输入日志,Log4J2打印日志,beat扫描文件将日志传输给LogStash,LogStash使用logstash-input-beats插件,接收日志输送给ElasticSearch,最后展示到Kibana 
2. 5分钟的持续输入日志,Log4J2打印日志,LogStash使用logstash-input-file插件扫描文件将日志输送给ElasticSearch,最后展示到Kibana 以上测试全部采用单节点

| ...................... | LogStash                                                     | FileBeat                                                     | 备注                                                       |
| ---------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ---------------------------------------------------------- |
| 内存                   | 400左右                                                      | 20左右(目前所有日志文件夹扫描255左右)                        |                                                            |
| CPU                    | 8.5左右                                                      | (0.2 内存占用不超过1)                                        |                                                            |
| 最后一行处理           | 否                                                           | 是                                                           |                                                            |
| 安全传输               | 否                                                           | 是                                                           | filebeat实现ssl加密验证                                    |
| 背压敏感协议           | 否                                                           | 是                                                           | 当接收端繁忙时,会通知收集端降低传输速率,并在正常后恢复速度 |
| 插件                   | 多                                                           | 多                                                           |                                                            |
| 适用场景               | 宽                                                           | 窄                                                           |                                                            |
| 功能                   | 从多种输入端采集并实时解析和转换数据并输出到多种输出端       | 传输                                                         |                                                            |
| 应急                   | 记忆上次读取节点                                             | 记忆上次读取节点                                             |                                                            |
| 文档                   | 多                                                           | 少                                                           |                                                            |
| 轻重                   | 轻量级框架(相对较重)                                         | 轻量级二进制文件                                             |                                                            |
| 过滤能力               | 强大的过滤能力                                               | 有过滤能力但是弱                                             |                                                            |
| 进程                   | 一台服务器只允许一个logstash进程,挂掉之后需要手动拉起        | 十分稳定                                                     | 都可以通过X-Pack的monitoring进行监控,5.1版本适配           |
| 原理                   | Logstash使用管道的方式进行日志的搜集和输出,分为输入input --> 处理filter（不是必须的） --> 输出output,每个阶段都有不同的替代方式 | 开启进程后会启动一个或多个探测器（prospectors）去检测指定的日志目录或文件，对于探测器找出的每一个日志文件，filebeat启动收割进程（harvester），每一个收割进程读取一个日志文件的新内容，并发送这些新的日志数据到处理程序（spooler），处理程序会集合这些事件，最后filebeat会发送集合的数据到你指定的地点。 |                                                            |
| 编写语言               | Jruby                                                        | go语言                                                       |                                                            |
| 集群                   | 单节点                                                       | 单节点                                                       |                                                            |
| 输出到多个接收方       | 支持                                                         | 6.0之前支持                                                  | 为保持管道的简单性,明确beat的输出范围                      |
| 二次开发或者扩展开发   | 难                                                           | 易                                                           |                                                            |
| 支持异步发送           | 支持                                                         | 支持                                                         |                                                            |

# 安装

## 下载解压

各种版本下载路径：

https://www.newbe.pro/Mirrors/Mirrors-Logstash/

> logstash需要1.8的java环境

```shell
cd /opt/
tar -zxvf logstash-2.0.0.tar.gz
ln -sv logstash-2.0.0 logstash
```

简单测试：

```shell
cd /opt/logstash
#选项 -e 的意思是允许你从命令行指定配置
bin/logstash -e 'input { stdin {} } output { stdout {codec=>rubydebug}}'
```

![image-20210107160948211](image/Logstash/image-20210107160948211.png)

## 低版本启动脚本 

```shell
cd /opt/logstash
#logs用于存放日志，etc用于存放配置文件
mkdir {logs,etc}

vim /etc/init.d/logstash


#!/bin/bash
#chkconfig: 2345 55 24
#description: logstash service manager
#logstash配置文件
FILE='/opt/logstash/etc/*.conf'
#指定logstash配置文件的命令
LOGBIN='/opt/logstash/bin/logstash agent --verbose --config'
#用锁文件配合服务启动与关闭
LOCK='/opt/logstash/locks'
#日志
LOGLOG='--log /opt/logstash/logs/stdou.log'

START() {
        if [ -f $LOCK ];then
                echo -e "Logstash is already \033[32mrunning\033[0m, do nothing."
        else
                echo -e "Start logstash service.\033[32mdone\033[m"
                nohup ${LOGBIN} ${FILE} ${LOGLOG} &
                touch $LOCK
        fi
}

STOP() {
        if [ ! -f $LOCK ];then
                echo -e "Logstash is already stop, do nothing."
        else
                echo -e "Stop logstash serivce \033[32mdone\033[m"
                rm -rf $LOCK
                ps -ef | grep logstash | grep -v "grep" | awk '{print $2}' | xargs kill -s 9 >/dev/null
        fi
}

STATUS() {
        ps aux | grep logstash | grep -v "grep" >/dev/null
        if [ -f $LOCK ] && [ $? -eq 0 ]; then
                echo -e "Logstash is: \033[32mrunning\033[0m..."
        else
                echo -e "Logstash is: \033[31mstopped\033[0m..."
        fi
}

TEST(){
        ${LOGBIN} ${FILE} --configtest
}

case "$1" in
  start)
        START
        ;;
  stop)
        STOP
        ;;
  status)
        STATUS
        ;;
  restart)
        STOP
        sleep 2
        START
        ;;
  test)
        TEST
        ;;
  *)
        echo "Usage: /etc/init.d/logstash (test|start|stop|status|restart)"
        ;;
esac

[root@test1 local]# chkconfig --add logstash
[root@test1 local]# chkconfig logstash on

```

## 高版本配置文件

```sh
vim /opt/logstash/config/logstash.yml

#默认全部注释，添加下面2个即可
path.data: /opt/logstash/data
path.logs: /opt/logstash/logs

```



##  编辑采集文件 

```shell
[root@test1 ~]# vim /opt/logstash/etc/test.conf
input {         #表示从标准输入中收集日志
  stdin {}
}

output {       
  elasticsearch  {  #表示将日志输出到ES中
    hosts => ["192.168.17.130:9201","192.168.17.130:9202","192.168.17.130:9203"]   #可以指定多台主机，也可以指定集群中的单台主机
    index  => "test-%{+YYYY.MM.dd}"
    codec => "json"
  }
}
```

## 启动

```shell
#/etc/init.d/logstash或通过以下启动
[root@test1 ~]# /opt/logstash/bin/logstash -f /opt/logstash/etc/test.conf

#等待进入控制台后，手动写入的内容
hello world  
```

![image-20210107164508598](image/Logstash/image-20210107164508598.png) 

logstash出现Unknown setting 'host' for elasticsearch

Unknown setting 'protocol' for elasticsearch

原因：2.0版本以上host修改为 hosts，并取消protocol。



# 采集文件详解

参考：https://blog.csdn.net/zjcjava/article/details/99258682?utm_medium=distribute.pc_relevant_t0.none-task-blog-BlogCommendFromMachineLearnPai2-1.control&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-BlogCommendFromMachineLearnPai2-1.control

组成部分是数据输入(input)，数据源过滤(filter)，数据输出(output)三部分

## input

官方文档：https://www.elastic.co/guide/en/logstash/current/plugins-inputs-file.html#plugins-inputs-file-sincedb_path

input 及输入是指日志数据传输到Logstash中。其中常见的配置如下：

- `file`：从文件系统中读取一个文件，很像UNIX命令 “tail -0a”
- `kafka`：消费Kafka的数据
- `syslog`：监听514端口，按照RFC3164标准解析日志数据
- `redis`：从redis服务器读取数据，支持channel(发布订阅)和list模式。redis一般在Logstash消费集群中作为"broker"角色，保存events队列共Logstash消费。
- `lumberjack`：使用lumberjack协议来接收数据，目前已经改为 logstash-forwarder。

```yaml
stdin { } # 从控制台中输入来源


file { # 从文件中来
        path => "E:/software/logstash-1.5.4/logstash-1.5.4/data/*" #单一文件
         #监听文件的多个路径
        path => ["E:/software/logstash-1.5.4/logstash-1.5.4/data/*.log","F:/*.log"]
        #排除不想监听的文件
        exclude => "1.log"
        
        #添加自定义的字段
        add_field => {"test"=>"test"}
        #增加标签
        tags => "tag1"

        #设置新事件的标志
        delimiter => "\n"

        #设置多长时间扫描目录，发现新文件
        discover_interval => 15
        #设置多长时间检测文件是否修改
        stat_interval => 1

         #监听文件的起始位置，默认是end
        start_position => beginning

        #监听文件读取信息记录的位置
        sincedb_path => "E:/software/logstash-1.5.4/logstash-1.5.4/test.txt"
        #设置多长时间会写入读取的位置信息
        sincedb_write_interval => 15
    }

  kafka{
         bootstrap_servers => ["10.199.155.171:9092,10.199.155.183:9092,10.199.155.210:9092"]
         
         #此消费者所属组的标识符。使用者组是碰巧由多个处理器组成的单个逻辑订阅者。主题中的消息将被分发到具有相同组 _ id 的所有 Logstash 实例
         #如果在单个管道中使用了多个输入，读取不同的主题，那么必须为每个输入设置不同的 group _ id
         group_id => "logstash_data_access_event"
         
         #当 Kafka 中没有初始偏移量或偏移量超出范围时起作用，earliest、latest、none
         auto_offset_reset => "earliest"
         
         #消费线程数：线程数应该与分区数一样多——线程数多于分区意味着某些线程将处于空闲状态，默认为1
         consumer_threads => 3
         
         #要订阅的主题列表，默认为[“ logstash”]。
         topics => ["mongodblog"]
         
         #要订阅的主题正则表达式。使用此配置时，主题配置将被忽略。
         topics_pattern => ""
         
         #默认值为"plain"
         codec => json
    }


 syslog { # 系统日志方式
  type => "system-syslog"  # 定义类型
   port => 10514    # 定义监听端口
 }


 beats { # filebeats方式
  port => 5044
 }

```

> 以上文件来源file，syslog,beats 只能选择其中一种

`注意：`

- 文件的路径名需要时绝对路径
- 支持globs写法
- 如果想要监听多个目标文件可以改成数组

1、path
path是file中唯一必需的参数。其他都是可选参数
2、exclude
是不想监听的文件，logstash会自动忽略该文件的监听。配置的规则与path类似，支持字符串或者数组，但是要求必须是绝对路径。
3、start_position
是监听的位置，默认是end，即一个文件如果没有记录它的读取信息，则从文件的末尾开始读取，也就是说，仅仅读取新添加的内容。对于一些更新的日志类型的监听，通常直接使用end就可以了；相反，beginning就会从一个文件的头开始读取。但是如果记录过文件的读取信息，这个配置也就失去作用了。
4、sincedb_path
这个选项配置了默认的读取文件信息记录在哪个文件中，默认是按照文件的inode等信息自动生成。其中记录了inode、主设备号、次设备号以及读取的位置。因此，如果一个文件仅仅是重命名，那么它的inode以及其他信息就不会改变，因此也不会重新读取文件的任何信息。类似的，如果复制了一个文件，就相当于创建了一个新的inode，如果监听的是一个目录，就会读取该文件的所有信息。
5、关于扫描和检测的时间
按照默认的来就好了，如果频繁创建新的文件，想要快速监听，那么可以考虑缩短检测的时间。
6、add_field
就是增加一个字段，例如：
7、tags
用于增加一些标签，这个标签可能在后续的处理中起到标志的作用

## filter

Fillters 在Logstash处理链中担任中间处理组件。他们经常被组合起来实现一些特定的行为来，处理匹配特定规则的事件流。常见的filters如下：

- grok：解析无规则的文字并转化为有结构的格式。Grok 是目前最好的方式来将无结构的数据转换为有结构可查询的数据。有120多种匹配规则，会有一种满足你的需要。
- mutate：mutate filter 允许改变输入的文档，你可以从命名，删除，移动或者修改字段在处理事件的过程中。
- drop：丢弃一部分events不进行处理，例如：debug events。
- clone：拷贝 event，这个过程中也可以添加或移除字段。
- geoip：添加地理信息(为前台kibana图形化展示使用)

它的主要作用就是把数据解析成规则的json键值对格式便于输出到其他组件中使用。
logstash自带的文件正则支持grok,date,geoip

> 详情看另一篇文章：grok正则

```yml
filter {

  #定义数据的格式
  grok {
    match => { "message" => "%{DATA:timestamp}\|%{IP:serverIp}\|%{IP:clientIp}\|%{DATA:logSource}\|%{DATA:userId}\|%{DATA:reqUrl}\|%{DATA:reqUri}\|%{DATA:refer}\|%{DATA:device}\|%{DATA:textDuring}\|%{DATA:duringTime:int}\|\|"}
  }

 #定义时间戳的格式
  date {
    match => [ "timestamp", "yyyy-MM-dd-HH:mm:ss" ]
    locale => "cn"
  }

  #定义客户端的IP是哪个字段（上面定义的数据格式）
  geoip {
    source => "clientIp"
  }
}

```

```yaml
  #需要进行转换的字段，这里是将访问的时间转成int，再传给Elasticsearch
  mutate {
    convert => ["duringTime", "integer"]
  }

```



## output

官网：https://www.elastic.co/guide/en/logstash/current/output-plugins.html

outputs是logstash处理管道的最末端组件。一个event可以在处理过程中经过多重输出，但是一旦所有的outputs都执行结束，这个event也就完成生命周期。一些常用的outputs包括：

- `elasticsearch`：如果你计划将高效的保存数据，并且能够方便和简单的进行查询.

- `file`：将event数据保存到文件中。

- `csv`：将event数据保存到csv文件中。

- `redis`：将event数据保存到readis

- `graphite`：将event数据发送到图形化组件中，一个很流行的开源存储图形化展示的组件。http://graphite.wikidot.com/。

- `statsd：statsd`是一个统计服务，比如技术和时间统计，通过udp通讯，聚合一个或者多个后台服务，如果你已经开始使用statsd，该选项对你应该很有用。

  默认情况下将过滤扣的数据输出到elasticsearch，当我们不需要输出到ES时需要特别声明输出的方式是哪一种，同时支持配置多个输出源

```yaml
output {
  #将输出保存到elasticsearch，如果没有匹配到时间就不保存，因为日志里的网址参数有些带有换行
  if [timestamp] =~ /^\d{4}-\d{2}-\d{2}/ {
        elasticsearch { host => localhost }
  }

  #输出到stdout
  stdout { codec => rubydebug }

#定义访问数据的用户名和密码
#  user => webService
#  password => 1q2w3e4r
}

```

### stdout

一般用来测试用

```yaml
output {
  stdout { codec => rubydebug }
}
```

### elasticsearch

```yaml
output {
    elasticsearch {
        #可以指定多台主机，也可以指定集群中的单台主机
        hosts => ["172.17.107.187:9203","172.17.107.187:9201","172.17.107.187:9202"]
        
        #要写入elasticsearch的索引,可以使用变量进行动态调用，%{+YYYY.MM.dd}按照天来分区
        index => "filebeat_%{[fields][log_source]}-%{+YYYY.MM.dd}"
        
        #用于验证到安全 Elasticsearch 集群的用户名和密码
        user => "elastic"
        password => "escluter123456"
    }
}
```

**测试控制台到ES**

```sh
/opt/logstash/binlogstash -e 'input {  stdin {} } output {   elasticsearch  {  hosts => ["192.168.17.130:9201","192.168.17.130:9202","192.168.17.130:9203"]  index  => "test-%{+YYYY.MM.dd}"  codec => "json"  } }'

#等待一会进入控制台

随意输出字符串后，查看ES数据
```

![image-20210107161611194](image/Logstash/image-20210107161611194.png)

http://192.168.17.130:9201/_cat/indices?v

![image-20210107161639324](image/Logstash/image-20210107161639324.png)

### file

输出到文件,实现将分散在多地的文件统一到一处的需求,比如将所有web机器的web日志收集到1个文件中,从而方便查阅信息

```yaml
output {
  file { 
  	path => "/val/log/web.log"
  	#默认输出json格式，通过formate可以输出原始格式
  	codec => line{ format => "%{message}"}
  }
}
```

### redis

```yaml
output {
    redis{
        batch => false
        batch_events => 50
        batch_timeout => 5
        codec => plain
        congestion_interval => 1
        congestion_threshold => 0
        data_type => list
        db => 0
        host => ["127.0.0.1:6379"]
        key => xxx
        password => xxx
        port => 6379
        reconnect_interval => 1
        shuffle_hosts => true
        timeout => 5
        workers => 1
    }
}

```

以上所有配置项都是可选的，**没有必须的**。

- 批处理类（仅用于data_type为list）

  - batch：设为true，通过发送一条rpush命令，存储一批的数据
    - 默认为false：1条rpush命令，存储1条数据
    - 设为true之后，1条rpush会发送batch_events条数据或发送batch_timeout秒（取决于哪一个先到达）
  - batch_events：一次rpush多少条
    - 默认50条
  - batch_timeout：一次rpush最多消耗多少s
    - 默认5s

- 编码类

  - **codec**：对输出数据进行codec，避免使用logstash的separate filter

- 拥塞保护（仅用于data_type为list）

  - congestion_interval：每多长时间进行一次拥塞检查
    - 默认1s
    - 设为0，表示对每rpush一个，都进行检测
  - congestion_threshold：list中最多可以存在多少个item数据
    - 默认是0：表示禁用拥塞检测
    - 当list中的数据量达到congestion_threshold，会阻塞直到有其他消费者消费list中的数据
    - 作用：防止OOM

- data_type

  - **list**：使用rpush
  - channel：使用publish

- db：使用redis的数据库，默认使用0号

- host

  ：数组

  - eg.["127.0.0.1:6380", "127.0.0.1"]
  - 可以指定port，会覆盖全局port

- port：全局port，默认6379

- key

  ：list或channel的名字

  - 支持动态key，例如：logstash-%{type}

- password：redis密码，默认不使用密码

- reconnect_interval：失败重连的间隔，默认为1s

- timeout：连接超时，默认5s

### hdfs

**安装插件**

logstash默认不支持数据直接写入HDFS，官方推荐的output插件是`webhdfs`，webhdfs使用HDFS提供的API将数据写入HDFS集群

```sh
cd /opt/logstash
./bin/logstash-plugin install logstash-output-webhdfs
```

**配置hosts**

HDFS集群内通过主机名进行通信所以logstash所在的主机需要配置hadoop集群的hosts信息

```sh
# cat /etc/hosts
192.168.107.154 master01
192.168.107.155 slave01
192.168.107.156 slave02
192.168.107.157 slave03
```

配置logstash

```yaml
# cat config/indexer_rsyslog_nginx.conf
input {
    kafka {
        bootstrap_servers => "10.82.9.202:9092,10.82.9.203:9092,10.82.9.204:9092"
        topics => ["rsyslog_nginx"]
        codec => "json"
    }
}
 
filter {
	# 匹配原始日志中的time_local字段并设置为时间字段
    # time_local字段为本地时间字段，没有8小时的时间差
    date {
        match => ["time_local","dd/MMM/yyyy:HH:mm:ss Z"]
        target => "time_local"
    }
 
 	# 添加一个index.date字段，值设置为time_local的日期
    ruby {
        code => "event.set('index.date', event.get('time_local').time.localtime.strftime('%Y%m%d'))"
    }
 
 	# 添加一个index.hour字段，值设置为time_local的小时
    ruby {
        code => "event.set('index.hour', event.get('time_local').time.localtime.strftime('%H'))"
    }
}

#不指定codec，logstash会给每条日志前添加时间和host字段
codec => line {
    format => "%{message}"
}
 
output {
    webhdfs {
        host => "master01"
        port => 50070
        user => "hadmin"
        path => "/logs/test/%{index.date}/%{index.hour}.log"
        codec => "json"
    }
    #调试打印
    stdout { codec => rubydebug }
}
```

查看hdfs发现数据已经按照定义好的路径正常写入

```

$ hadoop fs -ls /logs/test/20190318/19.log
```



# 编解码器

Codecs是基本的流过滤器，可以作为输入或输出的一部分进行操作，Codecs使你能够轻松地将消息的传输与序列化过程分开，流行的codecs包括`json`、`msgpack`和`plain`（text）。

- **json**：以JSON格式对数据进行编码或解码。
- **multiline**：将多行文本事件（如java异常和stacktrace消息）合并到单个事件中。

有关可用编解码器的更多信息，请参见[编解码器插件](https://www.elastic.co/guide/en/logstash/current/codec-plugins.html)。

# 场景应用

**场景：**

**1）** **datasource->logstash->elasticsearch->kibana**

**2）** **datasource->filebeat->logstash-> elasticsearch->kibana**

**3）** **datasource->filebeat->logstash->redis/kafka->logstash-> elasticsearch->kibana**

**4）** **kafka->logstash-> elasticsearch->kibana**

**5）** **datasource->filebeat->kafka->logstash->elasticsearch->kibana(最常用)**

**6）** **filebeatSSL加密传输**

**7）** **datasource->logstash->redis/kafka->logstash->elasticsearch->kibana**

**8）** **mysql->logstash->elasticsearch->kibana**


# 概述

flume 是由 cloudera 软件公司产出的可分布式日志收集系统，后与 2009 年被捐赠了 apache 软件基金会， 为hadoop 相关组件之一。
Flume 是一种分布式 ， 可靠且可用的服务 ， 用于高效地收集 ， 汇总和移动大量日志数据 。 它具有基于流式数据流的简单而灵活的架构 。 它具有可靠的可靠性机制以及许多故障转移和恢复机制 ， 具有强大的容错性和容错能力。它使用一个简单的可扩展数据模型，允许在线分析应用程序。

## 为什么需要flume?

1、当大量的数据在同一个时间要写入HDFS时，每次一个文件被创建或者分配一个新的块，都会在namenode发生很复杂的操作，主节点压力很大，会造成很多问题，比如写入时间严重延迟、写入失败等。
2、flume是一个灵活的分布式系统，易扩展，高度可定制化。
3、flume中的核心组件Agent。一个Agent可以连接一个或者多个Agent，可以从一个或者多个Agent上收集数据。多个Agent相互连接，可以建立流作业，在Agent链上，就能将数据从一个位置移动到另一个地方（HDFS、HBase等）。

## flume特性

Flume 是一个分布式、可靠、和高可用的海量日志采集、聚合和传输的系统。

Flume 可以采集文件，socket 数据包、文件、文件夹、kafka 等各种形式源数据，又可以将采集到的数据( 下沉sink) 输出到 HDFS 、hbase 、hive 、kafka 等众多外部存储系统中

对 一般的采集需求，通过对 flume 的简单配置即可实现

Flume可以高效率的将多个网站服务器中收集的日志信息存入HDFS/HBase中

Flume可以将从多个服务器中获取的数据迅速的移交给Hadoop中

除了日志信息，Flume同时也可以用来接入收集规模宏大的社交网络节点事件数据，比如facebook,twitter,电商网站如亚马逊，flipkart等

Flume 针对特殊场景也具备良好的自定义扩展能力，因此，flume 可以适用于大部分的日常数据采集场景

Flume 的管道是基 于事务，保证了数据在传送和接收时的一致性

Flume 是可靠的，容错性高的，可升级的，易管理的, 并且可定制的。

当收集数据的速度超过将写入数据的时候，也就是当收集信息遇到峰值时，这时候收集的信息非常大，甚至超过了系统的写入数据能力，这时候，Flume会在数据生产者和数据收容器间做出调整，保证其能够在两者之间提供一共平稳的数据.

## Flume架构

![简单](https://img-blog.csdnimg.cn/20190313232915169.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2YxNTUwODA0,size_16,color_FFFFFF,t_70)

**Agent：Agent是Flume中的核心组件，用来收集数据。一个Agent就是一个JVM进程，它是Flume中最小的独立运行的单元。**

在Agent中有三个组件，分别为Source，Channel，Sink

> **Flume Event**
> Event：在flume中，event是最小的数据单元，由header和body组成 event 是 是 flume
> 中处理消息的基本单元，个 由零个或者多个 header 和正文 body 组成 。 Header 是 是 key/value 形式的
> ， 可以用来制造路由决策或携带其他结构化信息( 如事件的时间戳或 事件来源的服务器主机名)和 。你可以把它想象成和 HTTP
> 头一样提供相同的功能—— 通过该方法 来传输正文之外的额外信息。 Body 是一 个字节数组 ，包含了实际的内容。 flume
> 提供的同 不同 source 会给其生成的 event 添加不同的 header

**Source**：数据源。负责将数据捕获后进行特殊的格式化，然后再封装在Event中，再将数据推入到Channel中
**常见类型**： ：avro 、exec、 jms、spooling directory、source 、kafka 、netcat 等

**Channel**：连接source和sink的组件，可以理解为数据缓冲区（数据队列），可以将event暂存在内存上，也可以持久化到本地磁盘上，直到sink消费完。
**常见类型**：Memory、JDBC、File等

**Sink**：数据下沉。从channel中取出数据后，分发到别的地方，比如HDFS或者HBase等，也可以是其他Agent的source。当日志数据量小的时候，可以将数据存在文件系统中，并设定一定的时间间隔来存储数据。
**常见类型**：HDFS、Logger、File　Roll、Avro、Thrift、HBａｓｅ等
[Source、Channel和Sink的类型具体说明参照官网用户指南](http://flume.apache.org/releases/content/1.9.0/FlumeUserGuide.html)

它的组合形式举例:

![image-20210111152449861](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86/image/Flume/image-20210111152449861.png)

![image-20210111152501660](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86/image/Flume/image-20210111152501660.png)

## 多个Agent连接

![多个Agent](https://img-blog.csdnimg.cn/20190313232959285.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2YxNTUwODA0,size_16,color_FFFFFF,t_70)

如图，３个Agent连接一个Agent时，要注意的是，３个Agent中Sinks的类型需要统一，因为另一个Agent的Source类型需要统一的数据源类型来接收。

一个或者多个Agent连接就形成了**流**，流将数据推送到了另一个Agent，最终将数据推送到存储或者索引系统。

## Flume插件

1. **Interceptors拦截器**

   用于source和channel之间,用来更改或者检查Flume的events数据

2. **管道选择器 channels Selectors**

   在多管道是被用来选择使用那一条管道来传递数据(events). 管道选择器又分为如下两种:

    	`默认管道选择器`:  每一个管道传递的都是相同的events

    	`多路复用通道选择器`:  依据每一个event的头部header的地址选择管道.

3. **sink线程**

   用于激活被选择的sinks群中特定的sink,用于负载均衡.

## 应用场景

   比如我们在做一个电子商务网站，然后我们想从消费用户中访问点特定的节点区域来分析消费者的行为或者购买意图. 这样我们就可以更加快速的将他想要的推送到界面上，实现这一点，我们需要将获取到的她访问的页面以及点击的产品数据等日志数据信息收集并移交给Hadoop平台上去分析.而Flume正是帮我们做到这一点。现在流行的内容推送，比如广告定点投放以及新闻私人定制也是基于次，不过不一定是使用FLume,毕竟优秀的产品很多，比如facebook的Scribe，还有Apache新出的另一个明星项目chukwa，还有淘宝Time Tunnel。

# 安装

官网下载：http://flume.apache.org/download.html

1. 下载Flume：https://downloads.apache.org/flume/1.9.0/apache-flume-1.9.0-bin.tar.gz

2. 解压

3. 配置`/opt/flume/conf/flume-env.sh` 配置Java环境

4. 编写测试配置文件test.conf

   ```yml
   vim /opt/flume/conf/test.conf
   
   
   #a1是agent别名，a1中定义了一个r1的source,如果有多个，使用空格隔开
   a1.sources=r1
   a1.sinks=k1
   a1.channels=c1
   
   #组名.属性名=属性值
   a1.sources.r1.type=netcat
   a1.sources.r1.bind=192.168.17.130
   a1.sources.r1.port=44444
   
   #定义sink
   a1.sinks.k1.type=logger
   a1.sinks.k1.maxBytesToLog=100
   
   #定义chanel
   a1.channels.c1.type=memory
   a1.channels.c1.capacity=1000
   
   #连接组件 同一个source可以对接多个channel，一个sink只能从一个channel拿数据！
   a1.sources.r1.channels=c1
   a1.sinks.k1.channel=c1 
   ```

5. 运行Flume，在${FLUME_HOME}/bin 下运行

   ```shell
   /opt/flume/bin/flume-ng agent -n a1 -c /opt/flume/conf -f /opt/flume/conf/test.conf -Dflume.root.logger=INFO,console
   
   # agent 必须写的没有疑问
   # -n a1 ：指定agent的名字
   # -c conf 指定配置目录
   # -f 执行agent具体的的配置文件
   # Dflume.root.logger=INFO,console：接收数据打印到控制台
   # example.conf：配置文件。需要自己根据flume的语法创建
   ```

6. 在终端进行侧是

   telnet 192.168.17.130 44444

   ![image-20210111173016028](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86/image/Flume/image-20210111173016028.png)

   ![image-20210111173139076](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86/image/Flume/image-20210111173139076.png)

# Source

其中红框里的最常用也是最重要的

![image-20210111160747976](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86/image/Flume/image-20210111160747976.png)

## Exec Soufce

通过linux的指令来监控数据，以下是基本配置，想看详细配置请点击该链接http://flume.apache.org/FlumeUserGuide.html#exec-source

```yaml
a1.sources = r1 #定义source的名字，多个用逗号隔开
a1.channels = c1 #定义channel的名字，多个用逗号隔开
a1.sources.r1.type = exec #定义source的类型
a1.sources.r1.command = tail -F /var/log/secure #利用指令监控日志
a1.sources.r1.channels = c1
```

|                 |             |                                                              |
| --------------- | ----------- | ------------------------------------------------------------ |
| 属性名          | 默认值      | 属性名解释                                                   |
| **channels**    | 无          | 连接channel                                                  |
| **type**        | 无          | 该组件的类型名称，该值必须是exec                             |
| **command**     | 无          | 执行的命令，例如：tail -F /var/log/secure                    |
| shell           | 无          | 用于运行命令的shell调用                                      |
| restartThrottle | 10000       | 在尝试重新启动之前等待的时间(以millis为单位)                 |
| restart         | false       | 如果执行的cmd死了，是否应该重新启动它                        |
| logStdErr       | false       | 是否记录命令的标准输出                                       |
| batchSize       | 20          | 每次读取和发送给channel的最大行数                            |
| batchTimeout    | 3000        | 如果没有达到缓冲区的大小，等待多久将数据push到channel里      |
| selector.type   | replicating | 复制或多路复用。复制就是把一个完整的事件发送到不同的channel中，多路复用就是根据不同的条件将同一个事件拆分成多个条目发送到channel中 |
| selector.*      | 无          | 取决于selector.type的值                                      |
| interceptors    | 无          | 空格分隔的列表的拦截器                                       |
| interceptors.*  |             |                                                              |

## SpoolDirectorySource

这个源支持从磁盘中某文件夹获取文件数据。不同于其他异步源，这个源能够避免重启或者发送失败后数据丢失。flume可以监控文件夹，当出现新文件时会读取该文件并获取数据。当一个给定的文件被全部读入到通道中时，该文件会被重命名以标志已经完成。同时，该源需要一个清理进程来定期移除完成的文件，一下是最基本的配置，想看详细配置请点击该链接http://flume.apache.org/FlumeUserGuide.html#spooling-directory-source

```yaml
a1.channels = ch-1
a1.sources = src-1

a1.sources.src-1.type = spooldir
a1.sources.src-1.channels = ch-1
a1.sources.src-1.spoolDir = /var/log/apache/flumeSpool
a1.sources.src-1.fileHeader = true
```

| 属性名                   | 默认值      | 属性名解释                                                   |
| ------------------------ | ----------- | ------------------------------------------------------------ |
| **channels**             | 无          | 连接channel                                                  |
| **type**                 | 无          | 该组件的类型名称，必须是spooldir                             |
| **spoolDir**             | 无          | 自己定义的spooldir的文件目录                                 |
| fileSuffix               | .COMPLETED  | 文件读取完毕以后给完成文件添加的标记后缀                     |
| deletePolicy             | never       | 是否删除读取完毕的文件，默认是”never”，就是不删除，目前只支持”never”和“IMMEDIATE”； |
| fileHeader               | false       | 是否在event的Header中添加文件名，boolean类型                 |
| fileHeaderKey            | file        | 这是event的Header中的key,value是文件名                       |
| basenameHeader           | false       | 是否添加一个存储文件的basename的Header                       |
| basenameHeaderKey        | basename    | 将文件的basename添加到事件头时使用的头文件键。               |
| includePattern           | ^.*$        | 指定要包含哪些文件的正则表达式。它可以与ignorePattern一起使用。如果一个文件同时匹配ignorePattern和includePattern regex，该文件将被忽略。 |
| ignorePattern            | ^$          | 正则表达式指定被忽略的文件，如果一个文件同时匹配ignorePattern和includePattern regex，该文件将被忽略。 |
| trackerDir               | .flumespool | 目录来存储与文件处理相关的元数据。如果该路径不是绝对路径，则将指定spoolDir。 |
| pollDelay                | 500         | 轮询新文件的时候使用的延迟时间                               |
| recursiveDirectorySearch | false       | 是否监视要读取的新文件的子目录                               |
| maxBackoff               | 4000        | 如果通道满了以后，连续尝试向通道里写入数据的最大的等待时间，毫秒为单位 |
| batchSize                | 100         | 批量向channle传输event的大小                                 |
| inputCharset             | UTF-8       | 编码方式，默认是”UTF-8”                                      |
| decodeErrorPolicy        | FAIL        | FAIL:抛出异常并解析文件失败。REPLACE:将不可解析字符替换为“替换字符”字符，通常是Unicode U+FFFD。IGNORE:删除不可解析的字符序列。 |
| deserializer             | LINE        | 指定用于将文件解析为事件的反序列化器。默认情况下，将每一行解析为一个事件。指定的类必须实现eventdeserizer.builder。 |
| selector.type            | replicating | 复制或多路复用。复制就是把一个完整的事件发送到不同的channel中，多路复用就是根据不同的条件将同一个事件拆分成多个条目发送到channel中 |
| selector.*               | 无          | 取决于selector.type的值                                      |
| interceptors             | 无          | 空格分隔的列表的拦截器                                       |
| interceptors.*           |             |                                                              |

## Kafka Source

是一个从Kafka的 Topic中读取消息的Apache Kafka消费者。 如果您有多个Kafka source运行，您可以使用相同的Consumer Group配置它们，因此每个将读取topic中一组唯一的分区。以下是基本配置，想看详细配置请点击该链接http://flume.apache.org/FlumeUserGuide.html#kafka-source

```yaml
tier1.sources.source1.type = org.apache.flume.source.kafka.KafkaSource #指定source的类型为kafka
tier1.sources.source1.channels = channel1
tier1.sources.source1.batchSize = 5000 #在一个批处理中写入通道的最大消息数
tier1.sources.source1.batchDurationMillis = 2000 #在将批处理写入通道之前的最大时间(以ms为单位)
tier1.sources.source1.kafka.bootstrap.servers = localhost:9092 #kafka地址
tier1.sources.source1.kafka.topics = test1, test2 #指定消费哪些主题，多个用逗号隔开
tier1.sources.source1.kafka.consumer.group.id = custom.g.id 
```

| 属性名                      | 默认值 | 属性名解释                                                   |
| --------------------------- | ------ | ------------------------------------------------------------ |
| **channels**                | 无     | 连接channel                                                  |
| **type**                    | 无     | 该组件的类型名称，必须是org.apache.flume.source.kafka.KafkaSource |
| **kafka.bootstrap.servers** | 无     | kafka集群的地址，如localhost:9092                            |
| **kafka.topics**            | 无     | Kafka集群的主题，多个用逗号隔开                              |
| **kafka.topics.regex**      | 无     | 正则匹配的主题集，他比kafka.topics优先级高，如果kafka.topics存在，那么就会覆盖kafka.topics的值 |
| kafka.consumer.group.id     | flume  | 消费者群体的唯一标识。在多个源或代理中设置相同的id表明它们属于同一个消费者组 |
| batchSize                   | 1000   | 一个批次向channel中写入的最大的消息的数量                    |
| batchDurationMillis         | 1000   | 在将批处理写入通道之前的最大时间(以ms为单位)                 |
| backoffSleepIncrement       | 1000   | 当Kafka主题为空时触发的初始和增量等待时间。这个值尽可能的低  |
| maxBackoffSleep             | 5000   | 当Kafka主题看起来为空时触发的最大等待时间，这个值尽可能的低  |
| useFlumeEventFormat         | false  | 默认情况下，事件作为字节从Kafka主题直接带到事件主体中。设置为true以Avro二进制格式读取事件。与KafkaSink上的相同属性或Kafka通道上的parseAsFlumeEvent属性一起使用，这将保留在生产端发送的所有Flume头。 |
| setTopicHeader              | true   | 当设置为true时，将检索到的消息的主题存储到一个header中，由topicHeader属性定义。 |
| topicHeader                 | topic  | 如果setTopicHeader属性设置为true，则定义用于存储接收消息的主题的标题的名称。如果与Kafka Sink topicHeader属性结合，应该要小心，以免在循环中将消息发送回相同的主题。 |
| migrateZookeeperOffsets     | true   |                                                              |

## Syslog Sources

读取Syslog数据转转换成Event

### 1、Syslog TCP Source

通过单个监听端口来接受数据转换成Event

```yaml
a1.sources = r1   #source名称
a1.channels = c1   #channel的名称
a1.sources.r1.type = syslogtcp   #source类型为syslogtcp
a1.sources.r1.port = 5140  #source监控的端口号
a1.sources.r1.host = localhost   #监控的机器地址
a1.sources.r1.channels = c1  #定义source与channel的连接
```

| 属性名         | 默认值      | 属性名解释                                                   |
| -------------- | ----------- | ------------------------------------------------------------ |
| **channels**   | 无          | 连接channel                                                  |
| **type**       | 无          | 该组件的类型名称，必须是syslogtcp                            |
| **host**       | 无          | 主机名或IP地址                                               |
| **port**       | 无          | 监听的端口号                                                 |
| eventSize      | 2500        | 单个事件行的最大大小，以字节为单位                           |
| selector.type  | replicating | 复制或多路复用。复制就是把一个完整的事件发送到不同的channel中，多路复用就是根据不同的条件将同一个事件拆分成多个条目发送到channel中 |
| selector.*     | 无          | 取决于selector.type的值                                      |
| interceptors   | 无          | 空格分隔的列表的拦截器                                       |
| interceptors.* |             |                                                              |

### 2、Multiport Syslog TCP Source

通过监听多个端口开始先数据转换成Event

```yaml
a1.sources = r1  #定义的source
a1.channels = c1 #定义channel
a1.sources.r1.type = multiport_syslogtcp  #指定source类型为多端口监控
a1.sources.r1.channels = c1
a1.sources.r1.host = 0.0.0.0 #指定监控端口的所在的机器
a1.sources.r1.ports = 10001 10002 10003  #监控多个端口用空格开
a1.sources.r1.portHeader = port  #监控的是端口
```

| 属性名                | 默认值      | 属性名解释                                                   |
| --------------------- | ----------- | ------------------------------------------------------------ |
| **channels**          | 无          | 连接channel                                                  |
| **type**              | 无          | 该组件的类型名称，必须是multiport_syslogtcp                  |
| **host**              | 无          | 主机名或IP地址                                               |
| **ports**             | 无          | 监听的端口号，多个用空格隔开                                 |
| portHeader            | 无          | 如果指定，端口号将使用这里指定的头名存储在每个事件的头中。这允许拦截器和通道选择器根据传入的端口自定义路由逻辑。 |
| eventSize             | 2500        | 单个事件行的最大大小，以字节为单位                           |
| charset.default       | UTF-8       | 将syslog事件解析为字符串时使用的默认字符集。                 |
| charset.port.《port》 | 无          | 设置对应端口的字符集                                         |
| batchSize             | 100         | 尝试处理每个请求循环的最大事件数。使用默认值通常是可以的。   |
| readBufferSize        | 1024        | 内部Mina读取缓冲区的大小。提供性能调优。使用默认值通常是可以的。 |
| selector.type         | replicating | 复制或多路复用。复制就是把一个完整的事件发送到不同的channel中，多路复用就是根据不同的条件将同一个事件拆分成多个条目发送到channel中 |
| selector.*            | 无          | 取决于selector.type的值                                      |
| interceptors          | 无          | 空格分隔的列表的拦截器                                       |
| interceptors.*        |             |                                                              |

### 3、Syslog UDP Source

通过监听UDP协议的端口来实现数据转换成Event，以下是具体的必须配置项，如果想查看更加详细的配置请点击该链接http://flume.apache.org/FlumeUserGuide.html#syslog-udp-source

```yaml
a1.sources = r1
a1.channels = c1
a1.sources.r1.type = syslogudp #指定source类型为syslogUdp
a1.sources.r1.port = 5140  #指定监控的端口
a1.sources.r1.host = localhost
a1.sources.r1.channels = c1
```

|                |             |                                                              |
| -------------- | ----------- | ------------------------------------------------------------ |
| 属性名         | 默认值      | 属性名解释                                                   |
| **channels**   | 无          | 连接channel                                                  |
| **type**       | 无          | 该组件的类型名称，必须是syslogudp                            |
| **host**       | 无          | 主机名或IP地址                                               |
| **port**       | 无          | 监听的端口号                                                 |
| selector.type  | replicating | 复制或多路复用。复制就是把一个完整的事件发送到不同的channel中，多路复用就是根据不同的条件将同一个事件拆分成多个条目发送到channel中 |
| selector.*     | 无          | 取决于selector.type的值                                      |
| interceptors   | 无          | 空格分隔的列表的拦截器                                       |
| interceptors.* |             |                                                              |

## HTTP Source

用于接受Http的Post或者Get请求，但是Get请求一般用在实验环境中，以下是最基本的配置，如果想查看更加详细的配置请点击该链接http://flume.apache.org/FlumeUserGuide.html#http-source

a1.sources = r1
a1.channels = c1
a1.sources.r1.type = http #指定source的类型为http
a1.sources.r1.port = 5140 #指定监控的端口
a1.sources.r1.channels = c1 
a1.sources.r1.handler = org.example.rest.RestHandler   #指定绑定的处理程序，如果自己定义这个处理程序需要需要定义一个实现了HTTPSourceHandler接口的类，然后打包放到Flume的lib目录下。
a1.sources.r1.handler.nickname = random props #配置处理程序的参数

| 属性名                             | 默认值                                   | 属性名解释                                                   |
| ---------------------------------- | ---------------------------------------- | ------------------------------------------------------------ |
| **channels**                       | 无                                       | 连接channel                                                  |
| **type**                           | 无                                       | 该组件的类型名称，必须是http                                 |
| **port**                           | 无                                       | 监听的端口号                                                 |
| bind                               | 0.0.0.0                                  | 监听的主机名或者ip地址                                       |
| handler                            | org.apache.flume.source.http.JSONHandler | 处理程序类                                                   |
| handler.*                          | 无                                       | 处理程序类的配置参数                                         |
| selector.type                      | replicating                              | 复制或多路复用。复制就是把一个完整的事件发送到不同的channel中，多路复用就是根据不同的条件将同一个事件拆分成多个条目发送到channel中 |
| selector.*                         | 无                                       | 取决于selector.type的值                                      |
| interceptors                       | 无                                       | 空格分隔的列表的拦截器                                       |
| interceptors.*                     |                                          |                                                              |
| enableSSL                          | false                                    | 将属性设置为true，以启用SSL。HTTP源不支持SSLv3。             |
| excludeProtocols                   | SSLv3                                    | 要排除的SSL/TLS协议的空格分隔列表。SSLv3总是被排除在外。     |
| keystore                           |                                          | 密钥存储库的位置包括密钥存储库文件名                         |
| keystorePassword Keystore password | 无                                       | 密钥密码                                                     |

# Channel

![image-20210111165605232](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86/image/Flume/image-20210111165605232.png)

## Memory Channel

事件存储在内存当中

```yaml
a1.channels = c1
a1.channels.c1.type = memory  #指定channel的类型为内存
a1.channels.c1.capacity = 10000  #存储事件的最大数量
a1.channels.c1.transactionCapacity = 10000  #接受的最大数量
a1.channels.c1.byteCapacityBufferPercentage = 20
a1.channels.c1.byteCapacity = 800000
```

| 属性名              | 默认值 | 属性名解释                     |
| ------------------- | ------ | ------------------------------ |
| **channels**        | 无     | 连接channel                    |
| **type**            | 无     | 该组件的类型名称，必须是memory |
| capacity            | 100    | 存储在channel中的最大事件数    |
| transactionCapacity | 100    | 接受最大的事件数               |
| keep-alive          | 3      | 增加或者删除一个事件的超时时间 |

## JDBC Channel

channel数据存储在数据库中

```yaml
a1.channels = c1
a1.channels.c1.type = jdbc
```

|                            |                                      |                                                              |
| -------------------------- | ------------------------------------ | ------------------------------------------------------------ |
| 属性名                     | 默认值                               | 属性名解释                                                   |
| type                       | 无                                   | 该组件的类型名称，必须是jdbc                                 |
| db.type                    | DERBY                                | 数据库类型                                                   |
| driver.class               | org.apache.derby.jdbc.EmbeddedDriver | jdbc驱动                                                     |
| driver.url                 | (constructed from other properties)  | jdbc连接url                                                  |
| db.username                | “sa”                                 | 用户名                                                       |
| db.password                | –                                    | 用户密码                                                     |
| connection.properties.file | –                                    | jdbc连接属性文件的路径                                       |
| create.schema              | true                                 | (如果为真)，则创建db模式(如果不为真不创建)                   |
| create.index               | true                                 | 创建索引加快查找速度                                         |
| create.foreignkey          | true                                 |                                                              |
| transaction.isolation      | “READ_COMMITTED”                     | I数据隔离级别 READ_UNCOMMITTED, READ_COMMITTED, SERIALIZABLE, REPEATABLE_READ |
| maximum.connections        | 10                                   | 最大连接数                                                   |
| maximum.capacity           | 0 (unlimited)                        | channel中最大的事件数                                        |
| sysprop.*                  | DB Vendor specific properties        |                                                              |
| sysprop.user.home          | Home路径来存储嵌入式Derby数据库      |                                                              |



# Sink

红框内的比较常用

![image-20210111160944283](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86/image/Flume/image-20210111160944283.png)

## HDFS Sink

输出时间到HDFS中，以下是基本配置，如果想查看详细配置请点击该链接http://flume.apache.org/FlumeUserGuide.html#flume-sinks

```yaml
a1.channels = c1
a1.sinks = k1
a1.sinks.k1.type = hdfs  #执行sink输出的类型为HDFS
a1.sinks.k1.channel = c1
a1.sinks.k1.hdfs.path = hdfs://hadoop001:8020/flume/events/%y-%m-%d/%H%M/%S  #指定Sink写入HDFS的路径
a1.sinks.k1.hdfs.filePrefix = events-  #指定sink写入HDFS文件的前缀名
a1.sinks.k1.hdfs.round = true  #是否开启时间戳的四舍五入
a1.sinks.k1.hdfs.roundValue = 10  #舍弃十分钟，也就是该目录每十分钟生成一个
a1.sinks.k1.hdfs.roundUnit = minute  #四舍五入的最小单位
```

| 属性名                 | 默认值       | 属性名解释                                                   |
| ---------------------- | ------------ | ------------------------------------------------------------ |
| **channels**           | 无           | 连接channel                                                  |
| **type**               | 无           | 该组件的类型名称，必须是hdfs                                 |
| **hdfs.path**          | 无           | HDFS目录路径                                                 |
| hdfs.filePrefix        | FlumeData    | HDFS上前缀标识的为Flume创建的文件                            |
| hdfs.fileSuffix        | 无           | HDFS上后缀标识的为Flume创建的文件                            |
| hdfs.inUsePrefix       | 无           | 用于flume主动写入的临时文件的前缀                            |
| hdfs.inUseSuffix       | .tmp         | 用于flume主动写入的临时文件的后缀                            |
| hdfs.rollInterval      | 30           | 间隔多少秒数触发滚动当前文件(0 =从不基于时间间隔滚动)        |
| hdfs.rollSize          | 1024         | 文件的触发滚动当前文件 单位为bytes（0=从不基于大小滚动）     |
| hdfs.rollCount         | 10           | 事件的条目数触发滚动当前文件(0 =从不滚动基于事件数)          |
| hdfs.idleTimeout       | 0            | 超时后关闭非活跃文件(0 =禁用自动关闭空闲文件)                |
| hdfs.batchSize         | 100          | 一个批次向HDFS写入的事件数                                   |
| hdfs.codeC             | 无           | 指定压缩格式。gzip, bzip2, lzo, lzop, snappy                 |
| hdfs.fileType          | SequenceFile | 文件格式:当前SequenceFile、DataStream或CompressedStream (1)DataStream不会压缩输出文件，请不要设置压缩格式(2)CompressedStream必须设置hdfs.codeC的压缩格式 |
| hdfs.maxOpenFiles      | 5000         | 只允许打开这个数目的文件。如果超过这个数字，就会关闭最老的文件 |
| hdfs.minBlockReplicas  | 无           | 指定每个HDFS块的最小复制数。如果没有指定，它来自类路径中的默认Hadoop配置。 |
| hdfs.writeFormat       | Writable     | 向DFS文件里写的格式，要么是Text或者Writable，在使用Flume创建数据文件之前，将这些文件设置为Text，否则Apache Impala或Apache Hive都无法读取这些文件 |
| hdfs.callTimeout       | 10000        | 用于HDFS操作的毫秒数，如打开、写入、刷新、关闭。如果发生许多HDFS超时操作，则应该增加这个数字。 |
| hdfs.threadsPoolSize   | 10           | HDFS IO操作的每个HDFS接收器的线程数                          |
| hdfs.rollTimerPoolSize | 1            | 用于调度定时文件滚动的每个HDFS接收器的线程数                 |
| hdfs.kerberosPrincipal | 无           | 用于访问安全HDFS的Kerberos用户主体                           |
| hdfs.kerberosKeytab    | 无           | 用于访问安全HDFS的Kerberos keytab                            |
| hdfs.round             | false        | 是否时间戳被四舍五入                                         |
| hdfs.roundValue        | 1            | 四舍五入到最高倍数(在使用hdfs.roundUnit配置的单元中)，小于当前时间 |
| hdfs.roundUnit         | second       | 事件四舍五入的最小单位- second, minute or hour.              |
| hdfs.timeZone          | Local Time   | 用于解析目录路径的时区的名称，例如America/Los_Angeles。      |
| hdfs.useLocalTimeStamp | false        | 在替换转义序列时，使用本地时间(而不是事件头部的时间戳)。     |

## Hive Sink

该接收器将包含分隔文本或JSON数据的事件直接写到到Hive表或分区中。事件是使用Hive事务来编写的。一旦一组事件被提交到Hive中，它们就会立即出现在Hive查询中。flume要写入到的分区可以是预先创建好的，也可以是没创建好的，如果没有创建好这些分区，flume可以创建它们。事件数据的字段被映射到Hive表中的相应列。以下是基本的配置，如果想要查看更详细的配置，请查看下边对应的表格

```yaml
a1.channels = c1 
a1.channels.c1.type = memory
a1.sinks = k1  #定义sink的名字，多个可以用逗号隔开
a1.sinks.k1.type = hive  #定义sink类型为Hive
a1.sinks.k1.channel = c1
a1.sinks.k1.hive.metastore = thrift://127.0.0.1:9083  #连接MetaStore服务的地址
a1.sinks.k1.hive.database = logsdb  #写入的Hive的数据库
a1.sinks.k1.hive.table = weblogs  #写入的hive表
a1.sinks.k1.hive.partition = asia,%{country},%y-%m-%d-%H-%M  #指定分区
a1.sinks.k1.useLocalTimeStamp = #false 是否使用本地时间戳
a1.sinks.k1.round = true  #是否开启时间的四舍五入
a1.sinks.k1.roundValue = 10  #时间的四舍五入的最高倍数
a1.sinks.k1.roundUnit = minute  #时间的四舍五入的最小单位
a1.sinks.k1.serializer = DELIMITED #设定序列化器为分割
a1.sinks.k1.serializer.delimiter = "\t" #设定分割符号
a1.sinks.k1.serializer.serdeSeparator = '\t'
a1.sinks.k1.serializer.fieldnames =id,msg  # #字段名称，","分隔，不能有空格
```

| 属性名                 | 默认值     | 属性名解释                                                   |
| ---------------------- | ---------- | ------------------------------------------------------------ |
| **channels**           | 无         | 连接channel                                                  |
| **type**               | 无         | 该组件的类型名称，必须是hive                                 |
| **hive.metastore**     | 无         | MetaStore服务，例如thrift://localhost:9083                   |
| **hive.database**      | 无         | hive数据库                                                   |
| **hive.table**         | 无         | hive表                                                       |
| hive.partition         | 无         | 逗号分隔的分区值列表，标识要写入的分区                       |
| hive.txnsPerBatchAsk   | 100        | Hive授予一批事务，而不是像Flume这样的流客户端的单个事务      |
| heartBeatInterval      | 240        | 心跳间隔，防止hive的事物过期，将此值设置为0以禁用心跳。秒为单位 |
| autoCreatePartitions   | true       | 是否开启自动创建分区                                         |
| batchSize              | 15000      | 一个单独的hive事物一次的批处理的条数                         |
| maxOpenConnections     | 500        | 只允许打开这个数目的连接。如果超过这个数字，则关闭最近最少使用的连接。 |
| hdfs.callTimeout       | 10000      | (以毫秒为单位)Hive & HDFS I/O操作的超时，比如openTxn、write、commit、abort。 |
| serializer             | 无         | 序列化器解根据什么规则解析事件映射到hive表中                 |
| hdfs.round             | false      | 是否时间戳被四舍五入                                         |
| hdfs.roundValue        | 1          | 四舍五入到最高倍数(在使用hdfs.roundUnit配置的单元中)，小于当前时间 |
| hdfs.roundUnit         | second     | 事件四舍五入的最小单位- second, minute or hour.              |
| hdfs.timeZone          | Local Time | 用于解析目录路径的时区的名称，例如America/Los_Angeles。      |
| hdfs.useLocalTimeStamp | false      | 在替换转义序列时，使用本地时间(而不是事件头部的时间戳)。     |

## Hbase Sinks

将数据写到Hbase中

### HBase Sink

```yaml
a1.channels = c1
a1.sinks = k1
a1.sinks.k1.type = hbase  #sink类型 ，必须为hbase
a1.sinks.k1.table = foo_table  #Hbase的表名
a1.sinks.k1.columnFamily = bar_cf  #Hbase列簇
a1.sinks.k1.serializer = org.apache.flume.sink.hbase.RegexHbaseEventSerializer  #序列化解析器解析事件映射成Bhase中的字段
a1.sinks.k1.channel = c1
```

| 属性名             | 默认值                                                       | 属性名解释                                                   |
| ------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **channels**       | 无                                                           | 连接channel                                                  |
| **type**           | 无                                                           | 该组件的类型名称，必须是hbase                                |
| **table**          | 无                                                           | 指定将需要虚入数据的hbase的表                                |
| columnFamily       | 无                                                           | 指定数据写入的列簇                                           |
| zookeeperQuorum    | 无                                                           | 指定zookeeper的链接地址，需要跟hbase.site.xml里的值一致      |
| znodeParent        | /hbase                                                       | Hbase数据存储的根路径，跟hbase.site.xml里的值一致            |
| batchSize          | 100                                                          | 一次写入的条数                                               |
| coalesceIncrements | false                                                        | 如果sink合并多个增量到一个Cell中，如果多个增量合并到一个限定数量的cell中，那么就会有更好的性能 |
| serializer         | org.apache.flume.sink.hbase.<br />SimpleHbaseEventSerializer | Default increment column = “iCol”, payload column = “pCol”.  |
| serializer.*       | 无                                                           | 要传递给序列化器的属性。                                     |
| kerberosPrincipal  | 无                                                           | 用于访问安全HBase的Kerberos用户                              |
| kerberosKeytab     | 无                                                           | 用于访问安全HBase的Kerberos keytab                           |

### AsyncHBaseSink

异步写入数据到Hbase中的sink

```yaml
a1.channels = c1
a1.sinks = k1
a1.sinks.k1.type = asynchbase #指定异步类型
a1.sinks.k1.table = foo_table 
a1.sinks.k1.columnFamily = bar_cf
a1.sinks.k1.serializer = org.apache.flume.sink.hbase.SimpleAsyncHbaseEventSerializer  #异步序列化器
a1.sinks.k1.channel = c1
```

| 属性名             | 默认值                                                       | 属性名解释                                                   |
| ------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **channels**       | 无                                                           | 连接channel                                                  |
| **type**           | 无                                                           | 该组件的类型名称，必须是hbase                                |
| **table**          | 无                                                           | 指定将需要虚入数据的hbase的表                                |
| columnFamily       | 无                                                           | 指定数据写入的列簇                                           |
| zookeeperQuorum    | 无                                                           | 指定zookeeper的链接地址，需要跟hbase.site.xml里的值一致      |
| znodeParent        | /hbase                                                       | Hbase数据存储的根路径，跟hbase.site.xml里的值一致            |
| batchSize          | 100                                                          | 一次写入的条数                                               |
| timeout            | 60000                                                        |                                                              |
| coalesceIncrements | false                                                        | 如果sink合并多个增量到一个Cell中，如果多个增量合并到一个限定数量的cell中，那么就会有更好的性能 |
| serializer         | org.apache.flume.sink.hbase.<br />SimpleHbaseEventSerializer | Default increment column = “iCol”, payload column = “pCol”.  |
| serializer.*       | 无                                                           | 要传递给序列化器的属性。                                     |

## ElasticSearchSink

```yaml
a1.channels = c1
a1.sinks = k1
a1.sinks.k1.type = elasticsearch  #指定sink类型为elasticsearch  
a1.sinks.k1.hostNames = 127.0.0.1:9200,127.0.0.2:9300  #es集群地址
a1.sinks.k1.indexName = foo_index  #索引名
a1.sinks.k1.indexType = bar_type  #索引类型
    a1.sinks.k1.clusterName = foobar_cluster   #集群的名字
a1.sinks.k1.batchSize = 500  #批处理的大小
a1.sinks.k1.ttl = 5d  
a1.sinks.k1.serializer = org.apache.flume.sink.elasticsearch.ElasticSearchDynamicSerializer  #使用的序列化器
a1.sinks.k1.channel = c1
```

| 属性名        | 默认值                                                 | 属性名解释                                                   |
| ------------- | ------------------------------------------------------ | ------------------------------------------------------------ |
| **channels**  | 无                                                     | 连接channel                                                  |
| **type**      | 无                                                     | 该组件的类型名称，必须是elasticsearch                        |
| **indexName** | flume                                                  | 索引名称，默认是flume                                        |
| indexType     | logs                                                   | 索引类型，默认是logs                                         |
| clusterName   | elasticsearch                                          | 集群的名称，默认是elasticsearch                              |
| batchSize     | 100                                                    | 批处理的大小                                                 |
| ttl           | 无                                                     | 设置过期时间以后文档将会删除，ms(毫秒)，s(秒)，m(分钟)，h(小时)，d(天)和w(周) |
| serializer    | org.apache.flume.sink.hbase.SimpleHbaseEventSerializer | Default increment column = “iCol”, payload column = “pCol”.  |
| serializer.*  | 无                                                     | 要传递给序列化器的属性。                                     |

# 实例

## 采集目录到HDFS

采集需求：**服务器的某特定目录下，会不断产生新的文件，每当有新文件出现，就需要把文件采集到HDFS中去**

根据需求，首先定义以下3大要素

- 采集源，即source——监控文件目录 : spooldir
- 下沉目标，即sink——HDFS文件系统 : hdfs sink
- source和sink之间的传递通道——channel，可用file channel 也可以用内存channel

### 1.配置文件

```sh
#创建一个要读取目录
mkdir -p /root/logs

#创建flume配置文件
vim /opt/flume/conf/spooldir.conf
```

```yaml
# Name the components on this agent
a1.sources = r1
a1.sinks = k1
a1.channels = c1

# Describe/configure the source
##注意：不能往监控目中重复丢同名文件
a1.sources.r1.type = spooldir
a1.sources.r1.spoolDir = /root/logs
a1.sources.r1.fileHeader = true

# Describe the sink
a1.sinks.k1.type = hdfs
a1.sinks.k1.hdfs.path = hdfs://172.21.0.2:8020/flume/logs/%Y%m%d%H/
a1.sinks.k1.hdfs.filePrefix = logs
a1.sinks.k1.hdfs.round = true
a1.sinks.k1.hdfs.roundValue = 10
a1.sinks.k1.hdfs.roundUnit = minute
a1.sinks.k1.hdfs.rollInterval = 0
# 1024000是1MB
a1.sinks.k1.hdfs.rollSize = 1024000
a1.sinks.k1.hdfs.rollCount = 0
a1.sinks.k1.hdfs.batchSize = 100
a1.sinks.k1.hdfs.useLocalTimeStamp = true
#生成的文件类型，默认是Sequencefile，可用DataStream，则为普通文本
a1.sinks.k1.hdfs.fileType = DataStream

# Use a channel which buffers events in memory
a1.channels.c1.type = memory
a1.channels.c1.capacity = 1000
a1.channels.c1.transactionCapacity = 100

# Bind the source and sink to the channel
a1.sources.r1.channels = c1
a1.sinks.k1.channel = c1

```

### 2.拷贝jar包

cp ${HADOOP_HOME}share/hadoop/hdfs/*.jar /opt/flume/lib/

cp ${HADOOP_HOME}share/hadoop/common/*.jar /opt/flume/lib/

cp ${HADOOP_HOME}share/hadoop/common/lib/*.jar /opt/flume/lib/

ll /opt/flume/lib/ | grep guava   #删除低版本的

![image-20210126095035122](https://gitee.com/sanhenlei/study-notes/raw/master/%E5%A4%A7%E6%95%B0%E6%8D%AE/%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86/image/Flume/image-20210126095035122.png)

### 3.启动flume

/opt/flume/bin/flume-ng agent -c /opt/flume/conf -f /opt/flume/conf/spooldir.conf -n a1 -Dflume.root.logger=INFO,console

### 4.制作数据

上传文件到指定目录

**将不同的文件上传到下面目录里面去，注意文件不能重名**
重名后，程序会中断！！！！

## 采集文件到HDFS

监控文件内容的变化！！

采集需求：比如业务系统使用log4j生成的日志，日志内容不断增加，需要把追加到日志文件中的数据实时采集到hdfs

根据需求，首先定义以下3大要素

- 采集源，即source——监控文件内容更新 : exec ‘tail -F file’
- 下沉目标，即sink——HDFS文件系统 : hdfs sink
- Source和sink之间的传递通道——channel，可用file channel 也可以用 内存channel

### 1.配置文件

```yaml
vim /opt/flume/conf/tail-file.conf
```

```yaml
# Name the components on this agent
a1.sources = r1
a1.sinks = k1
a1.channels = c1

# Describe/configure the source
a1.sources.r1.type = exec
a1.sources.r1.command = tail -F /root/logs/test.log
a1.sources.r1.channels = c1

# Describe the sink
a1.sinks.k1.type = hdfs
a1.sinks.k1.hdfs.path = hdfs://172.21.0.2:8020/flume/tailout/%Y-%m-%d/%H%M/
a1.sinks.k1.hdfs.filePrefix = events-
a1.sinks.k1.hdfs.round = true
a1.sinks.k1.hdfs.roundValue = 10
a1.sinks.k1.hdfs.roundUnit = minute
a1.sinks.k1.hdfs.rollInterval = 0
a1.sinks.k1.hdfs.rollSize = 1024
a1.sinks.k1.hdfs.rollCount = 0
a1.sinks.k1.hdfs.batchSize = 1
a1.sinks.k1.hdfs.useLocalTimeStamp = true
#生成的文件类型，默认是Sequencefile，可用DataStream，则为普通文本
a1.sinks.k1.hdfs.fileType = DataStream

# Use a channel which buffers events in memory
a1.channels.c1.type = memory
a1.channels.c1.capacity = 1000
a1.channels.c1.transactionCapacity = 100

# Bind the source and sink to the channel
a1.sources.r1.channels = c1
a1.sinks.k1.channel = c1
```

参数解析：
• rollInterval
默认值：30
hdfs sink间隔多长将临时文件滚动成最终目标文件，单位：秒；
如果设置成0，则表示不根据时间来滚动文件；
注：滚动（roll）指的是，hdfs sink将临时文件重命名成最终目标文件，并新打开一个临时文件来写入数据；

• rollSize
默认值：1024
当临时文件达到该大小（单位：bytes）时，滚动成目标文件；
如果设置成0，则表示不根据临时文件大小来滚动文件；

• rollCount
默认值：10
当events数据达到该数量时候，将临时文件滚动成目标文件；
如果设置成0，则表示不根据events数据来滚动文件；

• round
默认值：false
是否启用时间上的“舍弃”，这里的“舍弃”，类似于“四舍五入”。

• roundValue
默认值：1
时间上进行“舍弃”的值；

• roundUnit
默认值：seconds
时间上进行“舍弃”的单位，包含：second,minute,hour
示例：

a1.sinks.k1.hdfs.path = /flume/events/%y-%m-%d/%H%M/%S
a1.sinks.k1.hdfs.round = true
a1.sinks.k1.hdfs.roundValue = 10
a1.sinks.k1.hdfs.roundUnit = minute
当时间为2015-10-16 17:38:59时候，hdfs.path依然会被解析为：
/flume/events/20151016/17:30/00
因为设置的是舍弃10分钟内的时间，因此，该目录每10分钟新生成一个。

### 2.制作数据

模拟，本地文件，动态写入：

将时间动态写入test.log的文件：

```sh
while true; do date >> /root/logs/test.log;done
```

查看是否动态写入：

```bash
[root@node01 ~]# tail -f /root/logs/test.log
```

### 3.启动flume

```bash
cd /opt/flume
./bin/flume-ng agent -c conf -f conf/tail-file.conf -n a1  -Dflume.root.logger=INFO,console
```

注意： 这里的配置文件名为： tail-file.conf



# flume 与 logstash 的比较

参考：https://blog.csdn.net/bzq629/article/details/90609996?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522161033633416780257478742%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=161033633416780257478742&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_v2~rank_v29-5-90609996.first_rank_v2_pc_rank_v29&utm_term=logstash%20%E5%92%8CFlume&spm=1018.2226.3001.4187

**1、flume的介绍**

flume是一个高可用的，高可靠的，分布式的海量日志采集、聚合和传输的系统。flume的一个Agent主要由source、channel、sink三个组件组成。
source负责从数据发生器接收数据；
channel相当于持久层，用于暂时存储数据；
sink负责将数据传送到目的地（hdfs、es等）。
flume支持开发者定制，可以自己进行开发。

**2、logstash的介绍**

logstash是一个接收、处理、转发日志的工具，与elasticsearch与kibana同属一个公司，ELK的组合兼容性强。logstash由input、Filter、output三个部分组成。
input负责数据的输入（产生或搜集，以及解码decode）；
filter负责对采集的日志进行分析，提取字段（一般都是提取关键的字段，存储到elasticsearch中进行检索分析）；
output负责把数据输出到指定的存储位置（kafka、es等）。

**3、功能上的比较**

flume比较注重于数据的传输，数据的产生后封装成event进行传输。传输的时候数据会持久化在channel中（一般有两种可以选择，memoryChannel存在内存中,FileChannel存储在文件中）。
数据只有sink完成后，才会从当前的channel中删除。这个过程是通过事务控制的，这样就保证了数据的可靠性。
但是这里也会产生一个问题：内存如果超过一定的量，也会造成数据丢失；如果使用文件存储，性能会变差。
logstash比较注重于字段的预处理，logstash有几十个插件，配置灵活。相比较而言，flume则是强调用户的自定义开发（source和sink的种类不多，功能也比较单一）。

**4、操作上的比较**

结合实际业务，我简单尝试了一下flume的几种配置：

1）flume使用avro source 可以实时读到控制台输出的日志，暂未尝试加上拦截器之后读取速度是否会变慢。
需要使用Apache下对应的log包以及配置一个log4j.properties文件：

2）flume使用spool source可以读取.log日志文件，但是被读取过的文件会有complete的标记，并不适用现有的日志滚动生成机制。

3）flume使用taildir source可以读取指定文件夹下的日志文件，这个是1.8版本中的实验性功能，目前只支持linux系统。
（原因：taildir会生成一个json文件来记录每个日志的消费偏移量，使得宕机后能从偏移量继续消费，解决数据重复发送的问题，但是这个偏移量在windows系统中无法使用）。

4）flume连接es(sink.type = elasticsearch)报错。目前问题定位在flume1.8版本无法连接到es5.4版本。
（网上给出的解决方案：1、将es版本降低至1.7；2、修改源码）

**5、部署的比较**

flume与logstash都需要单独安装在服务器上，通过配置文件控制其采集的数据源与数据接收方。

**6、现阶段给出的方案**

目前logstash存在的问题：
1、读取速度跟不上日志产生速度时会出现数据丢失问题——已有解决方案
2、需要单独安装——flume目前的资料来看也是需要单独安装的

若要使用flume需要：
定制source，定义日志过滤的逻辑等，出jar包，放置lib目录下；
修改es sink的源码，使其能够连接成功，出jar包，放置lib目录下；
channel的逻辑是否需要定制、选择内存or文件的持久化方式需结合实际场景。

# Flume与Kafka对比

1. kafka和flume都是日志系统，kafka是分布式消息中间件，自带存储，提供push和pull存取数据功能。flume分为agent（数据采集器）,collector（数据简单处理和写入）,storage（存储器）三部分，每一部分都是可以定制的。比如agent采用RPC（Thrift-RPC）、text（文件）等，storage指定用hdfs做。
2. kafka做日志缓存应该是更为合适的，但是 flume的数据采集部分做的很好，可以定制很多数据源，减少开发量。所以比较流行flume+kafka模式，如果为了利用flume写hdfs的能力，也可以采用kafka+flume的方式。

采集层 主要可以使用Flume, Kafka两种技术。

Flume：Flume 是管道流方式，提供了很多的默认实现，让用户通过参数部署，及扩展API.

Kafka：Kafka是一个可持久化的分布式的消息队列。

- Kafka 是一个非常通用的系统。你可以有许多生产者和很多的消费者共享多个主题Topics。相比之下,Flume是一个专用工具被设计为旨在往HDFS,HBase发送数据。它对HDFS有特殊的优化，并且集成了Hadoop的安全特性。所以，Cloudera 建议如果数据被多个系统消费的话，使用kafka；如果数据被设计给Hadoop使用，使用Flume。

 

- 正如你们所知Flume内置很多的source和sink组件。然而，Kafka明显有一个更小的生产消费者生态系统，并且Kafka的社区支持不好。希望将来这种情况会得到改善，但是目前：使用Kafka意味着你准备好了编写你自己的生产者和消费者代码。如果已经存在的Flume Sources和Sinks满足你的需求，并且你更喜欢不需要任何开发的系统，请使用Flume。

 

- Flume可以使用拦截器实时处理数据。这些对数据屏蔽或者过量是很有用的。Kafka需要外部的流处理系统才能做到。

 

- Kafka和Flume都是可靠的系统,通过适当的配置能保证零数据丢失。然而，Flume不支持副本事件。于是，如果Flume代理的一个节点崩溃了，即使使用了可靠的文件管道方式，你也将丢失这些事件直到你恢复这些磁盘。如果你需要一个高可靠行的管道，那么使用Kafka是个更好的选择。

 

- Flume和Kafka可以很好地结合起来使用。如果你的设计需要从Kafka到Hadoop的流数据，使用Flume代理并配置Kafka的Source读取数据也是可行的：你没有必要实现自己的消费者。你可以直接利用Flume与HDFS及HBase的结合的所有好处。你可以使用Cloudera Manager对消费者的监控，并且你甚至可以添加拦截器进行一些流处理。

Flume和Kafka可以结合起来使用。通常会使用Flume + Kafka的方式。其实如果为了利用Flume已有的写HDFS功能，也可以使用Kafka + Flume的方式。
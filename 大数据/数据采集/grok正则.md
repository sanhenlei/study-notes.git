# 系统自带正则

常用的有：

| INT          | int类型                        |
| ------------ | ------------------------------ |
| NUMBER       | 数字                           |
| DATA         | 数据，可以对应字符串           |
| GREEDYDATA   | 数据，可以对应字符串，贪婪匹配 |
| QUOTEDSTRING | 带引号的字符串，可以简写为QS   |
| WORD         | 一个词                         |
| IP           | ip地址，v4 或 v6               |
| DATE         | 日期                           |
| TIME         | 时间                           |
| DATESTAMP    | 日期+时间                      |
| PATH         | 系统路径                       |
| HOSTNAME     | 计算机名                       |
| MAC          | mac地址                        |
| UUID         | uuid                           |
| LOGLEVEL     | 日志等级                       |
| EMAILADDRESS | email地址                      |

```shell
USERNAME [a-zA-Z0-9._-]+

USER %{USERNAME}

INT (?:[+-]?(?:[0-9]+))

BASE10NUM (?<![0-9.+-])(?>[+-]?(?:(?:[0-9]+(?:\.[0-9]+)?)|(?:\.[0-9]+)))

NUMBER (?:%{BASE10NUM})

BASE16NUM (?<![0-9A-Fa-f])(?:[+-]?(?:0x)?(?:[0-9A-Fa-f]+))

BASE16FLOAT \b(?<![0-9A-Fa-f.])(?:[+-]?(?:0x)?(?:(?:[0-9A-Fa-f]+(?:\.[0-9A-Fa-f]*)?)|(?:\.[0-9A-Fa-f]+)))\b

 

POSINT \b(?:[1-9][0-9]*)\b

NONNEGINT \b(?:[0-9]+)\b

WORD \b\w+\b

NOTSPACE \S+

SPACE \s*

DATA .*?

GREEDYDATA .*

QUOTEDSTRING (?>(?<!\\)(?>"(?>\\.|[^\\"]+)+"|""|(?>'(?>\\.|[^\\']+)+')|''|(?>`(?>\\.|[^\\`]+)+`)|``))

UUID [A-Fa-f0-9]{8}-(?:[A-Fa-f0-9]{4}-){3}[A-Fa-f0-9]{12}

 

# Networking

MAC (?:%{CISCOMAC}|%{WINDOWSMAC}|%{COMMONMAC})

CISCOMAC (?:(?:[A-Fa-f0-9]{4}\.){2}[A-Fa-f0-9]{4})

WINDOWSMAC (?:(?:[A-Fa-f0-9]{2}-){5}[A-Fa-f0-9]{2})

COMMONMAC (?:(?:[A-Fa-f0-9]{2}:){5}[A-Fa-f0-9]{2})

IPV6 ((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?

IPV4 (?<![0-9])(?:(?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2})[.](?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2})[.](?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2})[.](?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2}))(?![0-9])

IP (?:%{IPV6}|%{IPV4})

HOSTNAME \b(?:[0-9A-Za-z][0-9A-Za-z-]{0,62})(?:\.(?:[0-9A-Za-z][0-9A-Za-z-]{0,62}))*(\.?|\b)

HOST %{HOSTNAME}

IPORHOST (?:%{HOSTNAME}|%{IP})

HOSTPORT %{IPORHOST}:%{POSINT}

 

# paths

PATH (?:%{UNIXPATH}|%{WINPATH})

UNIXPATH (?>/(?>[\w_%!$@:.,-]+|\\.)*)+

TTY (?:/dev/(pts|tty([pq])?)(\w+)?/?(?:[0-9]+))

WINPATH (?>[A-Za-z]+:|\\)(?:\\[^\\?*]*)+

URIPROTO [A-Za-z]+(\+[A-Za-z+]+)?

URIHOST %{IPORHOST}(?::%{POSINT:port})?

# uripath comes loosely from RFC1738, but mostly from what Firefox

# doesn't turn into %XX

URIPATH (?:/[A-Za-z0-9$.+!*'(){},~:;=@#%_\-]*)+

#URIPARAM \?(?:[A-Za-z0-9]+(?:=(?:[^&]*))?(?:&(?:[A-Za-z0-9]+(?:=(?:[^&]*))?)?)*)?

URIPARAM \?[A-Za-z0-9$.+!*'|(){},~@#%&/=:;_?\-\[\]]*

URIPATHPARAM %{URIPATH}(?:%{URIPARAM})?

URI %{URIPROTO}://(?:%{USER}(?::[^@]*)?@)?(?:%{URIHOST})?(?:%{URIPATHPARAM})?

 

# Months: January, Feb, 3, 03, 12, December

MONTH \b(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)\b

MONTHNUM (?:0?[1-9]|1[0-2])

MONTHNUM2 (?:0[1-9]|1[0-2])

MONTHDAY (?:(?:0[1-9])|(?:[12][0-9])|(?:3[01])|[1-9])

 

# Days: Monday, Tue, Thu, etc...

DAY (?:Mon(?:day)?|Tue(?:sday)?|Wed(?:nesday)?|Thu(?:rsday)?|Fri(?:day)?|Sat(?:urday)?|Sun(?:day)?)

 

# Years?

YEAR (?>\d\d){1,2}

HOUR (?:2[0123]|[01]?[0-9])

MINUTE (?:[0-5][0-9])

# '60' is a leap second in most time standards and thus is valid.

SECOND (?:(?:[0-5]?[0-9]|60)(?:[:.,][0-9]+)?)

TIME (?!<[0-9])%{HOUR}:%{MINUTE}(?::%{SECOND})(?![0-9])

# datestamp is YYYY/MM/DD-HH:MM:SS.UUUU (or something like it)

DATE_US %{MONTHNUM}[/-]%{MONTHDAY}[/-]%{YEAR}

DATE_EU %{MONTHDAY}[./-]%{MONTHNUM}[./-]%{YEAR}

ISO8601_TIMEZONE (?:Z|[+-]%{HOUR}(?::?%{MINUTE}))

ISO8601_SECOND (?:%{SECOND}|60)

TIMESTAMP_ISO8601 %{YEAR}-%{MONTHNUM}-%{MONTHDAY}[T ]%{HOUR}:?%{MINUTE}(?::?%{SECOND})?%{ISO8601_TIMEZONE}?

DATE %{DATE_US}|%{DATE_EU}

DATESTAMP %{DATE}[- ]%{TIME}

TZ (?:[PMCE][SD]T|UTC)

DATESTAMP_RFC822 %{DAY} %{MONTH} %{MONTHDAY} %{YEAR} %{TIME} %{TZ}

DATESTAMP_RFC2822 %{DAY}, %{MONTHDAY} %{MONTH} %{YEAR} %{TIME} %{ISO8601_TIMEZONE}

DATESTAMP_OTHER %{DAY} %{MONTH} %{MONTHDAY} %{TIME} %{TZ} %{YEAR}

DATESTAMP_EVENTLOG %{YEAR}%{MONTHNUM2}%{MONTHDAY}%{HOUR}%{MINUTE}%{SECOND}

 

# Syslog Dates: Month Day HH:MM:SS

SYSLOGTIMESTAMP %{MONTH} +%{MONTHDAY} %{TIME}

PROG (?:[\w._/%-]+)

SYSLOGPROG %{PROG:program}(?:\[%{POSINT:pid}\])?

SYSLOGHOST %{IPORHOST}

SYSLOGFACILITY <%{NONNEGINT:facility}.%{NONNEGINT:priority}>

HTTPDATE %{MONTHDAY}/%{MONTH}/%{YEAR}:%{TIME} %{INT}

 

# Shortcuts

QS %{QUOTEDSTRING}

 

# Log formats

SYSLOGBASE %{SYSLOGTIMESTAMP:timestamp} (?:%{SYSLOGFACILITY} )?%{SYSLOGHOST:logsource} %{SYSLOGPROG}:

COMMONAPACHELOG %{IPORHOST:clientip} %{USER:ident} %{USER:auth} \[%{HTTPDATE:timestamp}\] "(?:%{WORD:verb} %{NOTSPACE:request}(?: HTTP/%{NUMBER:httpversion})?|%{DATA:rawrequest})" %{NUMBER:response} (?:%{NUMBER:bytes}|-)

COMBINEDAPACHELOG %{COMMONAPACHELOG} %{QS:referrer} %{QS:agent}

 

# Log Levels

LOGLEVEL ([Aa]lert|ALERT|[Tt]race|TRACE|[Dd]ebug|DEBUG|[Nn]otice|NOTICE|[Ii]nfo|INFO|[Ww]arn?(?:ing)?|WARN?(?:ING)?|[Ee]rr?(?:or)?|ERR?(?:OR)?|[Cc]rit?(?:ical)?|CRIT?(?:ICAL)?|[Ff]atal|FATAL|[Ss]evere|SEVERE|EMERG(?:ENCY)?|[Ee]merg(?:ency)?)
```



# 测试网站

http://grok.51vagaa.com/





# 常用例子

| Item                                 | Comment                                                      |
| ------------------------------------ | ------------------------------------------------------------ |
| %{USER:user}                         | 以 USER 模式进行正则匹配，结果放在user中                     |
| [[^]]+]                              | 以 [ 开头 以]结尾，内容是由一个或多个不是 ] 的字符填充而成   |
| %{NUMBER: id:int}                    | 以 NUMBER 模式进行正则匹配，为整数型，结果放在id中           |
| \n                                   | 匹配换行符                                                   |
| %{NUMBER:query_time:float}           | 以 NUMBER 模式进行正则匹配，为浮点型，结果放在query_time中   |
| (?:use\s+%{USER:usedatabase};\s*\n)? | 这个匹配可能有，也可能无，如果有，就是以use开头，若干空字符，以 USER  模式进行正则匹配，结果放在usedatabase中，然后紧接着 ; ，后面是0个或多个空字符，然后是换行，注意：如果有是整体有，如果无，是整体无 |
| \b                                   | 代表字单词边界不占位置，只用来指示位置                       |
| .*                                   | 尽可能多的任意匹配                                           |
| (?<  query>(?< action>\w+)\b.*)      | 整体匹配，存到query中，以一个或多个字符开头组成的单词，结果存到action中 |
| (?:\n#\s+Time)?                      | 内容可能有，也可能无，如果有，是接在一个换行之后，以 #  开头，隔着一个或多个空字符，然后是Time |
| .*$                                  | 任意匹配直到结尾                                             |
| [\s\S]*                              | 任意匹配包括换行\n                                           |

```shell
grok正则表达式：(?<temMsg>(.*)(?=Report)/?) 获取Report之前的字符
grok正则表达式：(?<temMsg>(?=Report)(.*)/?) 获取Report之后的字符
grok{
		match => { 
				 #截取<Report>之前的字符作为temMsg字段的值
				"message" => "(?<temMsg>(.*)(?=Report)/?)" 
			}
	}
这个是截取特定的字符集日志，要日志中包含了【Report】关键字
（注：表达式中(?=Report)中的等于【=】符号如果换成【<=】这表示就不包含本身了，例如(?<temMsg>(.*)(?=Report)/?)可以写成(?<temMsg>(.*)(?<=Report)/?)这样输出的结果就不包含Report了，同理下面的一样）

```

```shell
grok正则表达式：(?<temMsg>(?<=report).*?(?=msg)) 截取report和msg之间的值 不包含report和msg本身
grok正则表达式：(?<temMsg>(report).*?(?=msg)) 截取 包含report但不包含msg
grok正则表达式：(?<temMsg>(?<=report).*?(msg))截取  不包含report但包含msg
grok正则表达式：(?<temMsg>(report).*?(msg|request))输出以report开头,以msg或者以request结尾的所有包含头尾信息
grok正则表达式：(?<temMsg>(report).*?(?=(msg|request)))输出以report开头,以msg或者以request结尾的不包含头尾信息
grok{
		match => { 
				 #截取<Report>之后的和<msg>之前的值作为temMsg字段的值
				"message" => "(?<temMsg>(?<=report).*?(?=msg))" 
			}
	}
这个是截取特定的字符集日志，要日志中包含了【report和msg和request】关键字
之间的表达式只要替换一下就可以使用了
（注过个表达式中出现异常，在单个的字符串中可以将小括号【()】去掉，例如：(report).*?(?=msg) 可以写成report.*?(?=msg)）
```

```shell
grok正则表达式：(?<MYELF>([\s\S]{500}))
 grok{
       match => {
              #截取日志500个字符 作为MYELF的值
              "message" => "(?<MYELF>([\s\S]{500}))"  
             }
     }
    对有所日志截取500个字符，可以加入if()做为判断条件，根据自身项目来
```

```shell
grok正则表达式：%{LOGLEVEL:level}
grok {
		#这个patterns_dir大家都应该正对 单独写表达式的地方
		#patterns_dir => "/usr/local/nlp/logstash-6.0.1/config/patterns"
                match => [
                        "message","%{LOGLEVEL:level}"         
                ]
        }
  这个比较简单 就不多说了
```

```shell
结合上面的 这个是对level级别的日志做判断 如果日志中含有DEBUG的，就drop掉
if [level] == "DEBUG" {
       drop { }
}
这个其实和上面差不多，加了一个【~】表示对单条的前后日志做匹配
if[message]=~"ASPECT"{
       drop { }
}
```

```shell
这个是说对temMsg赋值的所有的日志从新命名打印message
mutate {
		#重命名字段temMsg为message
		rename => {"temMsg" => "message"} 
}
```

```shell
#logstash过滤器切割
filter {                                      
    if [type] == "simple" {
        mutate{
                 split => ["message","|"]     #按 | 进行split切割message
                        add_field =>   {
                                "requestId" => "%{[message][0]}"
                        }
                        add_field =>   {
                                "timeCost" => "%{[message][1]}"
                        }
                        add_field =>   {
                                "responseStatus" => "%{[message][2]}"
                        }
						add_field =>   {
                                "channelCode" => "%{[message][3]}"
                        }
						add_field =>   {
                                "transCode" => "%{[message][4]}"
                        }
        }
		mutate {
			convert => ["timeCost", "integer"]  #修改timeCost字段类型为整型
		}
    } else if [type] == "detail" {
		grok{
			match => {             
				#将message里面 TJParam后面的内容，分隔并新增为ES字段和值
				"message" => ".*TJParam %{PROG:requestId} %{PROG:channelCode} %{PROG:transCode}"
			}
		}
		grok{
			match => { 
			 	#截取TJParam之前的字符作为temMsg字段的值
				"message" => "(?<temMsg>(.*)(?=TJParam)/?)" 
				#删除字段message
				remove_field => ["message"]		     
			}
		}
		mutate {
			 #重命名字段temMsg为message
			rename => {"temMsg" => "message"}		    
		}
    }
}

```

## 一个完整的

```shell
input {
    redis {
        data_type => "list"
        host => "localhost1"
        port => "5100"
        key => "nlp_log_file"
	    db => 0                                         
	    threads => 1                                    #线程数量
	    codec => "json"
    }
    redis {
        data_type => "list"
        host => "localhost2"
        port => "5101"
        key => "nlp_log_file"
        db => 0                                     
        threads => 1                                    #线程数量
        codec => "json"
    }
}

filter {
       grok {
		#patterns_dir => "/usr/local/nlp/logstash-6.0.1/config/patterns"
                match => [
                        "message","%{LOGLEVEL:level}"         
                ]
        }
	grok{
		match => { 
				#截取<ReportPdf>之前的字符作为temMsg字段的值
				"message" => "(?<temMsg>(.*)(?=<ReportPdf>)/?)"  
			}
	}
	mutate {
		 #重命名字段temMsg为message
		rename => {"temMsg" => "message"}		    
	}
        if [level] == "DEBUG" {
                drop { }
        }
        if[message]=~"ASPECT"{
                drop { }
        }
     #获取日志文件带RAWT关键字的
	if[message]=~"[RAW]"{   
             grok{
                match => {
             			#截取带RAW关键字的日志500个字符 作为MYELF的值
                        "message" => "(?<MYELF>([\s\S]{500}))"  
                        }
       		 }
       	      mutate {
                rename => {"MYELF" => "message"} #重命名字段MYELF为message
       	      }
        }
}

output {
    elasticsearch {
        hosts => ["localhost:9200"]
        index => "logstash-%{+YYYY.MM.dd}"  
		action => "index"
		template_overwrite => true
		#user => "elastic"
 		#password => "admins-1"
    }
	stdout{codec => dots}
}
```

 太多使用DATA和GREEDYDAYA会导致性能cpu负载严重。建议多使用正则匹配，或者ruby代码块 

```shell
filter {
     grok {
        match => [
               "message", "(?<hostname>[a-zA-Z0-9._-]+)\|%{DATA:tag}\|%{NUMBER:types}\|(?<uid>[0-9]+)\|%{GREEDYDATA:msg}",
               "message", "(?<hostname>[a-zA-Z0-9._-]+)\|%{DATA:tag}\|%{GREEDYDATA:msg}",
        ]
       remove_field => ['type','_id','input_type','tags','message','beat','offset']
    }
}
```

```shell
filter {
    ruby {
        code =>'
        arr = event["message"].split("|")
        if arr.length == 5
            event["hostname"] = arr[0]
            event["tag"] = arr[1]
            event["types"] = arr[2]
            event["uid"] = arr[3]
            event["msg"] = arr[4]
        elsif arr.length == 3
            event["hostname"] = arr[0]
            event["tag"] = arr[1]
            event["msg"] = arr[2]
        end'
       remove_field => ['type','_id','input_type','tags','message','beat','offset']
    }
}
```

## 自定义类型

第一种
直接使用Oniguruma语法来命名捕获，它可以让你匹配一段文本并保存为一个字段：

> (?<field_name>the pattern here)

例如，日志有一个queue_id 为一个长度为10或11个字符的十六进制值。使用下列语法可以获取该片段，并把值赋予queue_id

> (?<queue_id>[0-9A-F]{10,11})



第二种
创建自定义 patterns 文件。 
①创建一个名为patterns其中创建一个文件postfix （文件名无关紧要,随便起）,在该文件中，将需要的模式写为模式名称，空格，然后是该模式的正则表达式。例如：

>#contents of ./patterns/postfix:
POSTFIX_QUEUEID [0-9A-F]{10,11}

②然后使用这个插件中的patterns_dir设置告诉logstash目录是你的自定义模式。这是一个完整的示例的示例日志:

> Jan  1 06:25:43 mailserver14 postfix/cleanup[21403]: BEF25A72965: message-id=<20130101142543.5828399CCAF@mailserver14.example.com>

```shell
filter {
  grok {
    patterns_dir => ["./patterns"]
    match => { "message" => "%{SYSLOGBASE} %{POSTFIX_QUEUEID:queue_id}: %{GREEDYDATA:syslog_message}" }
  }
}
```

匹配结果如下：

  ● timestamp: Jan 1 06:25:43
  ● logsource: mailserver14
  ● program: postfix/cleanup
  ● pid: 21403
  ● queue_id: BEF25A72965
  ● syslog_message: message-id=<20130101142543.5828399CCAF@mailserver14.example.com>

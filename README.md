# 学习笔记

个人学习笔记，涉及前端、后端、大数据

前端知识图谱：https://www.processon.com/view/link/601ab40d1e08535a7bcff4f3
![输入图片说明](https://images.gitee.com/uploads/images/2021/0111/103631_76a70db5_5054511.png "屏幕截图.png")

后端知识图谱：https://www.processon.com/view/link/601ab3241e08535a7bcff386
![输入图片说明](https://images.gitee.com/uploads/images/2021/0203/223425_18397dea_5054511.png "屏幕截图.png")

大数据只是图谱：https://www.processon.com/view/link/601ab3fa7d9c0858d76fe125
![输入图片说明](https://images.gitee.com/uploads/images/2021/0111/103514_ef963902_5054511.png "屏幕截图.png")